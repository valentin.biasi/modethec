Specific settings
#################

.. _advanced_numerical_parameters:

Advanced numerical parameters
*****************************

.. _advanced_parameters:
.. figure:: img/modethec_gui/advanced_parameters.png
    :width: 90%
    :align: center

    Advanced numerical parameters

The **Show advanced numerical parameters** checkbox shows a list of parameters used for the numerical configuration of a Modethec case, that are:

* **Linear solver**: For implicit methods, a linear solver is used, especially for Newton-Raphson methods. At this moment, only the PARDISO solver is available and maintained in Modethec. PARDISO is a direct linear solver for sparse systems, using shared-memory functionalities. PARDISO is part of the Intel MKL library.

* **Theta**: The theta parameter defines the time integration method in the theta-implicit scheme. If theta is equal to 1, the integration method is similar to an Euler implicit method and if theta is equal to 0.5, the method is similar to an Crank-Nicolson method. More technical references are available in :doc:`/numerical_methods`.

* **Relative variation limit of conservative variables**: This value is used to limit formation and extinction of species due to reactions. For instance, a species concentration can not increase or decrease of more than 50% when this parameter is set to 0.5. This parameter is generally to a value close to 1.

* **Absolute and relative convergence criterion**: These combined values set the stop criterion of Newton-Raphson solvers. The relative criterion defines the stop criterion relatively to the first iteration error of the solver whereas the absolute criterion sets the absolute minimal error of the solver. See :doc:`/numerical_methods` for more references.

* **Maximum number of non-linear iterations**: If the convergence criterion is not reached after this number of iteration for one time step, the time integration continues even if the Newton-Raphson solver has not properly ended.

* **Flux limiter type**: It sets the TVD flux limiter for the advective terms of the gas transport solver. Flux limiters can be chosen in a list (Upwind, Van Leer, OSPRE, etc.). This parameter is important for the precision and the robustness of the gas transport solver. For most computations, the Van Leer limiter is a good compromise between precision and robustness.




.. _restart_and_backup:

Restart and backup computation
******************************

Modethec has a computation restart functionality, that could be extremely useful for long computations with risks of data loss that can be due various problems (divergence, memory, power supply, ...). To enable this functionality, the backup output needs to be activated. In the History tab, users have to activate the **Activate backup output** checkbox and set the **Time step between backup outputs** to the desired value.

.. _restart:
.. figure:: img/modethec_gui/restart.png
    :width: 90%
    :align: center

    Backup configuration


When a computation is run with the backup output functionality, a HDF5 file is created, named ``backup_<PARAM_FILE>.hdf``, where ``<PARAM_FILE>`` is the filename of the parameter file. This HDF5 file contains different arrays of mass, energy, reaction rates of the last backup.

To restart a computation with a backup data file, the **Restart from last backup computation** needs to be activated in the Numerical tab. If so, Modethec will read the backup data file and compute heterogeneous initial conditions according to this file.

.. note:: Only the last backup can be restarted. At each backup time step, the data file is overwritten. Moreover, the time step values are not saved in the backup process. A restart computation uses a new time chronology.


.. _user-specified_conditions:


User-specified boundary conditions
**********************************

Some special boundary conditions (Gaussian heat flux, scattered heat flux) can be configured in Modethec using user-specified boundary conditions files.

.. warning:: No user interface is available at this moment for user-specified boundary conditions. Files has to be created and edited with a text editor.

User-specified boundary conditions are written in separate files (one for each special condition) using the ``.cls`` extension. In the Modethec user interface, the user-specified boundary functionality has to be activated with the dedicated checkbox and the associated ``.cls`` file has to be defined. All ``.cls`` files have to be stored in the same directory as the main ``.prm`` file.

.. _cls_parameters:
.. figure:: img/modethec_gui/cls_parameters.png
    :width: 90%
    :align: center

    User-specified boundary conditions configuration

.. note:: Please note that user-specified boundary conditions only overwrite specific data and it has to be filled according to the boundary conditions type. For instance, if a ``.cls`` affects the impinging heat flux and the thermal boundary type is `Imposed temperature`, the boundary condition will not be modified.

The following code is an example of a 3D user-specified boundary conditions for a Gaussian heat flux.


.. code-block:: fortran
    :linenos:

    !--- 3D GAUSSIAN HEAT FLUX --------------------------------------------!
    !
    ener_typ_cls = 'gaussien'
    Fmax = 38.23e3
    omega = 10.9e-3
    tf_gauss = 240.0
    AXE_3D = 0.00 0.00 0.0 0.0 0.0 -1.0   


This example is a scattered heat flux boundary condition.


.. code-block:: fortran
    :linenos:

    !--- 2D SCATTERED HEAT FLUX -------------------------------------------!
    !
    ener_typ_cls = 'nuage'
    npt_nuage = 40
    val_nuage =
    0.000e+00      1.540e-03      5.964e+04
    1.410e-03      1.540e-03      6.083e+04
    2.821e-03      1.540e-03      6.189e+04
    4.231e-03      1.540e-03      6.282e+04
    5.641e-03      1.540e-03      6.346e+04
    7.051e-03      1.540e-03      6.376e+04
    8.462e-03      1.540e-03      6.410e+04
    9.872e-03      1.540e-03      6.348e+04
    1.128e-02      1.540e-03      5.939e+04
    1.269e-02      1.540e-03      5.639e+04
    1.410e-02      1.540e-03      5.102e+04
    1.551e-02      1.540e-03      4.688e+04
    1.692e-02      1.540e-03      4.410e+04
    1.833e-02      1.540e-03      4.014e+04
    1.974e-02      1.540e-03      3.653e+04



Available parameters in ``.cls`` files are:


* **ener_typ_cls**: It specifies the type of boundary condition. 2 options are possible: ``gaussien`` for a Gaussian heat flux and ``nuage`` for a scattered heat flux.

* **Fmax**: The peak heat flux (along the axis).

* **omega**: The variance parameter of the Gaussian function. The impinging heat flux :math:`F` can be expressed as a function of the distance from the axis :math:`r` as:

.. math::
   F(r) = F_{max} . exp \left( \frac{- r^2 }{ 2 \omega^2} \right)
   :label: gaussian_heat_flux_function


* **tf_gauss**: The impinging time. This parameter is optional.

* **AXE_3D**: The Gaussian axis. This value doesn't need to be specified in axisymmetric 2D as it has been already specified in the domain definition.

* **npt_nuage**: Number of scattered points for scattered heat fluxes definition.

* **val_nuage**: Values of the scattered data. The first and the second column are the X and Y coordinates. In 3D, the third column is the Z coordinates. The last column is the heat flux values.

.. note:: Face-centered values are linearly interpolated from scattered data the 2 or 3 nearest points.

.. _sensors_history_file:

Sensors and variables' history file
***********************************

History files can be created to export numerical data, surface and volume integrals and sensors data in a separate HDF5 format (named with the export filename). The XY plot function of Modethec can be used to plot these values as a function of time, as shown in the :ref:`interface_overview` section.

.. _history_variables:
.. figure:: img/modethec_gui/history_variables.png
    :width: 90%
    :align: center

    Sensors and variables' history file configuration


The necessary parameters used to define a history file are:

* **Export integrated variables**: It activates the history file export in HDF5. The created file is named using the export filename (Export options) and the ``.hdf`` extension.

* **Time step of HDF5 export history**: History records are done on each time step specified by this variable. This time step must be a multiple of the global time step.

* **Export variables at specific coordinates**: This option activates the sensors functionality. Output variables are written at each history time step for all sensors in the history file.

* **Number of sensors for specific outputs**: It specifies the number of desired sensors.

* **List of sensor coordinates to be extracted**: The location of sensors is set with this parameter. X, Y and Z coordinates has to be completed in the associated table. In 2D simulations, the Z coordinate has to be set to 0.


.. note:: Sensors data are linearly interpolated from the nearest cell center value and its gradient.
