General settings
################

This section details the general settings of a case. These parameters are common to all solvers.

General
*******

.. _general_settings:
.. figure:: img/modethec_gui/general_settings.png
    :width: 80%
    :align: center

    General settings overview


The general tab, as shown on :numref:`general_settings`, allows to select the following parameters:

* **Solver activation**: each solver can be activated by clicking on the desired solver. 

    *  The **heat diffusion** solver deals with the transient energy conservation law with only the thermal diffusion term on the right hand side of the equation. The heat diffusion solver is so far a required solver and can not be deactivated. Technical references are available at :ref:`energy_conservation_law`.

    * The **gas transport** solver evaluates the solution of the heat and mass transport equation in porous media. To activate this solver, the parameter file should contain at least 2 species, one solid and one gaseous. Technical references are available at :ref:`energy_conservation_law` and :ref:`mass_conservation_law`.

    * The **kinetic** solver deals with the effect of thermo-chemical reactions, defined as Arrhenius laws, and its effects on the heat and mass transport equation. 2 species are required to activate this solver, with at least one solid species to set the reactant. Technical references are available at :ref:`chemical_reactions`.

    * The **mechanical** solver evaluates the steady state stress and displacement of a material. It can deal with pressure terms and thermal-expansion. Please note that this solver is only valid in 1D and 2D. See references at :doc:`/thermo-mechanical_model`.

    * The **ablation** solver handles the displacement of a domain due to surface recession, based on ALE approach. See references at :doc:`/ablation_model`.

    * The **external coupling** is not strictly speaking a solver, but the activation of the CWIPI interface in order to exchange data with an external code. Technical references are available at :doc:`/external_coupling`.

* **Mesh filename**: the filename, in ``.msh`` or ``.dat`` format (see :doc:`/meshing`) is specified in this field. The filename must contain the extension.

* **Simulation dimension**: The dimension of the simulation is set by this selection box (1D, 2D planar, 2D axisymmetric or 3D). When a 2D axisymmetric dimension is set, a symmetry axis has to be defined.

* **Symmetry axis**: The symmetry axis is only required when the dimension is 2D axisymmetric. It is defined by a reference point (x0, y0, z0) and a direction (u0, v0, w0).

* **Author**: The author of the case can be specified in this field, that is purely informative.


Physical
********

Few parameters are available in the Physical tab when no solver is activated. The common parameters are:

* **Activate properties saturation**: It activate a threshold of temperature dependent properties (:math:`\mathbf{k}^{*}`, :math:`C_P`, :math:`\mu`). Below this temperature, properties evolves as specified by the physical laws but above, properties are saturated. This parameter activation can be useful when properties are characterized in a specified range of temperature or when the above threshold temperature properties are inconsistent.

* **Threshold temperature**: It specifies the threshold temperature for thermo-physical properties.

Species definition
******************

The species tab allows to specify the number of species and their names, as shown on :numref:`species_settings`. Users can double-click on the species name table to edit the species names. The parameter tree will be modified in consequence in a sub-tree, containing the list of specified species.

.. _species_settings:
.. figure:: img/modethec_gui/species_settings.png
    :width: 80%
    :align: center

    Species settings overview

Species settings
****************

For each defined species, parameters can be defined by clicking on the right species row in the parameter tree, as shown on :numref:`ispecies_settings`.

.. _ispecies_settings:
.. figure:: img/modethec_gui/ispecies_settings.png
    :width: 80%
    :align: center

    Per species settings overview

Common parameters for species definition are:

* **Species phase**: A species can be **solid** or **gas**. 

* **Gas species molar mass**: If a species is gaseous, its molar mass has to be defined.

* **Species density**: If a species is solid, its density is required. It can be defined **constant** or **polynomial**, depending on the species density model option. In case of a polynomial definition, the number of polynomial coefficients (up to 6) edits the number of editable rows.


Numerical
*********

Common parameters of the numerical tab are the **initial time**, the **time step** and the **final time**. If the initial time is not equal to 0, the simulation starts at this time with the initial condition and stops when the final time is reached. Technical references about the main time step and CFL-based time step in :doc:`/numerical_methods`.

.. _numerical_settings:
.. figure:: img/modethec_gui/numerical_settings.png
    :width: 80%
    :align: center

    Numerical settings overview

Details about the advanced numerical parameters and computation's restart can be found at :ref:`advanced_numerical_parameters` and :ref:`restart_and_backup`.

Initial conditions
******************

To define the initial conditions in a domain, the following parameters have to be set:

* **Initial temperature**: The initial temperature is set set for the whole domain.

* **Initial species volume fraction**: A table has to be filled with the initial volume fraction of each species in the order defined in the species name definition table. The sum of all volume fraction has to be equal to 1. If not, an error will raised at the beginning of the computation.

.. _initial_conditions:
.. figure:: img/modethec_gui/initial_conditions.png
    :width: 80%
    :align: center

    Initial conditions settings overview


Boundary conditions
*******************

In the boundary condition tab, the number of boundaries and their names have to be specified, as shown on :numref:`boundary_conditions`. The mechanism is similar to the species definition, as users can double-click on the boundary name table to edit their names. The parameter tree will be modified in consequence in a sub-tree, containing the list of specified boundaries.

.. _boundary_conditions:
.. figure:: img/modethec_gui/boundary_conditions.png
    :width: 80%
    :align: center

    Boundary conditions overview

The number and names of boundary conditions has to reach the mesh filename (see :doc:`/meshing` for more information), but the order of boundaries has no importance.

No common parameters are available in the boundary conditions definition when no solver is activated. Only the user-specified conditions can be specified, whose parameters are detailed in :ref:`user-specified_conditions`.

History
*******

As shown on :numref:`history_settings`, the history tab contains the following parameters:

.. _history_settings:
.. figure:: img/modethec_gui/history_settings.png
    :width: 80%
    :align: center

    History settings overview

* **Activate terminal history**: It activates the output of modethec in the standard terminal output. A sum-up of the computation is printed for initial and final phases, and on each history time step. Only errors and warnings are printed when this option is deactivated.

* **Time step of on-screen prints**: It is the history time step. It has to be a multiple of the global time step.

* **Record log file**: It activates the record of terminal history in a file named ``mdtc_output.log`` and errors and warnings in a file named ``mdtc_error.log``. These files are located in the same folder as parameters file.

* **Export integrated variables**: It activates the export of history files in HDF5 format. Details are available at :ref:`sensors_history_file`.

* **Activate backup output**: It activates the export of backup files in HDF5 format. Details are available at :ref:`restart_and_backup`.

Export
******

The export tab allows to define the results files format (exported variables, locations, time step, ...). As shown on :numref:`export_settings`, the export tab contains the following parameters:

.. _export_settings:
.. figure:: img/modethec_gui/export_settings.png
    :width: 90%
    :align: center

    Export settings overview

* **Save export file**: It activates the export of results files. When this option is deactivated, no file will be written. History files are independent of this option.

* **Export filename prefix**: Users specify the output filename used for exports. This field must not contain a prefix.

* **Time step of export file**: Exports are done on each time step specified by this variable. This time step must be a multiple of the global time step.

* **Export initial state**: This option activates the export of the variables' fields at the initial time.

* **Mesh deformation for mechanical solver**: When the mechanical solver is activated, this option deforms the mesh depending on the displacement field and the magnitude factor. This mesh deformation is only for visualization purposes.

* **Mesh deformation magnitude factor**: The magnitude factor is the multiplication factor of the real displacement field. This value is set for visualization purposes and has no influence on the computation.

* **Cell connectivity**: It exports the cell connectivity in the exported files. Otherwise, the data are scattered and the viewer (Tecplot, Paraview, ...) won't be able to recover the cells and faces connectivity.

* **Output format**: Different outputs are available in Modethec (Tecplot ASCII, Tecplot binary, VTK ASCII). Multiples formats can be exported with the same run.

* **Export location**: Users have to choose the location of exported data with radio buttons (at cell centers or at node centers).

* **Number of variables to be exported**: Users specify the number of exported variables. The exported variables' table is re-dimensioned according to this number.

* **List of exported variables**: The table of exported variables is composed of 3 columns. The first one sets the internal variable name, as detailed in the :numref:`list_export_variables`. The second row sets the index of the associated internal variable. When the variable is a vector, it corresponds to the Cartesian direction. When the variable is specified for each species, reaction, etc., it corresponds to the desired value according to its index. When the variable is a scalar, this value is not relative and is set by default to 1. The last column is the exported name. It is only important for visualization purposes. Special characters and spaces can be included in exported names.

.. warning:: The export location have an influence on results, as Modethec is a cell centered solver. The node centered data are interpolated by Modethec.


.. _list_export_variables:

.. csv-table:: List of exported variables
   :header: Internal variable name, Type, Description
   :widths: 25, 15, 60
   :stub-columns: 1
   :header-rows: 0

    VOLUME , Scalar , Cells' volume *
    T , Scalar , Temperature
    grad_T , Vector , Temperature gradient
    P , Scalar , Pressure
    grad_P , Vector , Pressure gradient
    Y , Species nb. , Species mass fraction
    Y_g , Scalar , Gaseous mass fraction
    Y_s , Scalar , Solid mass fraction
    phi , Species nb. , Species volume fraction
    phi_g , Scalar , Gaseous volume fraction
    phi_s , Scalar , Solid volume fraction
    rho , Scalar , Mean density
    rho_e , Species nb. , Species density
    rho_g , Scalar , Gaseous density
    v_g , Vector , Darcian gas velocity
    w_e , Species nb. , Per species reaction source term
    w_g , Scalar , Gaseous reaction source term
    wQ , Scalar , Energy reaction source term
    alpha_reac , Reaction nb. , Reaction rate
    E , Scalar , Internal cells' energy *
    dE , Scalar , Internal cells' energy derivate *
    m , Scalar , Internal cells' mass
    m_e , Species nb. , Per species internal cells' mass *
    dm_e , Species nb. , Per species internal cells' mass derivate *
    k , Vector , Thermal conductivity tensor (3 main directions)
    Cp , Scalar , Heat capacity
    h , Scalar , Sensible enthalpy
    Kp , Vector , Permeability tensor (3 main directions)
    G_XYZ , Vector , Cells' centers positions *
    U , Vector , Mechanical displacement
    eps_strain , Tensor , Mechanical strain (Voigt notation)
    sig_stress , Tensor , Mechanical stress (Voigt notation)


.. note:: Variables with a "*" notation are only expressed at cells centers location. For these variables, the node centered variables are not relevant.
