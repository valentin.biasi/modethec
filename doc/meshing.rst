Mesh definition
###############

Modethec supports two kind of mesh formats:

* The ``.msh`` mesh format exported by the GMSH mesh generator
* The ``.dat`` mesh format, proprietary format of the CEDRE CFD solver (ONERA |copy|)

.. |copy| unicode:: 0xA9 .. copyright sign


``.msh`` mesh format
******************** 

GMSH is an open source three-dimensional mesh generator with a built-in CAD engine and post-processor. The specification of any input in GMSH is done either interactively using the graphical user interface, or in ASCII text files using Gmsh's own scripting language (.geo files), or using the C++, C, Python or Julia API.

.. _gmsh_interface:
.. figure:: img/gmsh_interface.png
   :width: 100%
   :align: center

   Screenshot of the GMSH interface

Modethec developers encourages to read the following links that refer to tutorials and documentations for mesh generation with GMSH:

* http://gmsh.info/doc/texinfo/gmsh.html
* https://albertsk.files.wordpress.com/2012/12/gmsh_tutorial.pdf
* https://openfoamwiki.net/index.php/2D_Mesh_Tutorial_using_GMSH
* https://community.dur.ac.uk/g.l.ingram/download/gmshtutorial2012.pdf



.. note:: Before exporting a ``.msh`` file with GMSH, please ensure that your domain and boundary conditions are tagged with physical tags, named in agreement with the parameter file of modethec. For instance, in your ``.geo`` file, your entities has to be linked with physical tag with:
    
    .. code-block:: ruby
        :linenos:
        
        Physical Line("RIGHT") = {1};
        Physical Line("TOP") = {2};
        Physical Line("LEFT") = {3};
        Physical Line("BOTTOM") = {4};
        Physical Surface("DOMAIN") = {5};

    Then, you could check that your exported ``.msh`` file takes in account your physical tags in the header with a block like:

    .. code-block:: ruby
        :linenos:
    
        $PhysicalNames
        5
        1 1 "DROITE"
        1 2 "HAUT"
        1 3 "GAUCHE"
        1 4 "BASE"
        2 5 "INTERIEUR"
        $EndPhysicalNames

    You could also open the exported ``.msh`` file with GMSH and see if all boundary faces and cells are well recognized and correctly tagged.



``.dat`` mesh format
******************** 

The ``.dat`` mesh format is a proprietary format used in the CFD solver CEDRE. Details about the ``.dat`` format are available in the CEDRE documentation. Centaur and IcemCFD mesh generators are natively able to export ``.dat`` mesh files.

**Centaur** exports ``.dat`` mesh files simply with the selection of the output format "Cedre solver format" in the Mesh Generator window, as shown below:

.. _centaur_export_cedre:
.. figure:: img/centaur_export_cedre.png
   :width: 70%
   :align: center

   Centaur ``.dat`` export option

**IcemCFD** exports ``.dat`` mesh files when selecting the CEDRE output solver in Output mesh >> Select solver >> Output solver, as shown below:

.. _icem_export_cedre:
.. figure:: img/icem_export_cedre.png
   :width: 50%
   :align: center

   IcemCFD ``.dat`` export option

.. note:: The integrity of exported ``.dat`` mesh files can be checked with the ``epinette`` utility, in the toolbox of the CEDRE solver.

Mesh viewer
***********

To view a mesh file in the modethec interface, the ``Mesh filename`` has to be set up in the "General" tab and the parameter file has to be saved. Then, when the "Plot mesh" button is clicked, the plot mesh interface should open, as shown below:


.. _mesh_plot_view:
.. figure:: img/modethec_gui/mesh_plot_view.png
   :width: 100%
   :align: center

   Mesh viewer of the modethec interface

Internally, modethec converts the mesh file to a ``.vtk`` file and open it with the mayavi plugin. All functionalities of the mayavi viewer are available, such as:

* Rotate, slide, zoom in/out with the mouse
* Mesh trim
* Line or plane extraction.

To learn more about the mayavi plugin, please refer to the `documentation <https://docs.enthought.com/mayavi/mayavi/application.html>`_.

.. note:: To use the internal viewer of modethec, the mesh file format has to be ``.msh``. The ``.dat`` mesh format is not yet supported by the viewer.
