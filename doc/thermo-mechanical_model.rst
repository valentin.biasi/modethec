﻿Thermo-mechanical model
#######################


Mechanical response
-------------------

The modeling of the mechanical stresses of the material is deliberately limited to the equilibrium between the solid phase :math:`s` and the gaseous phase :math:`g`. In every point of the material, the momentum conservation law is translated by the system of equations:

.. math::
    -\overrightarrow{\nabla} \cdot \mathbf{\sigma}_s = \mathbf{0}
    :label: eq_solide

.. math::
    -\overrightarrow{\nabla} \cdot \mathbf{\sigma}_g = \mathbf{0}
    :label: eq_gaz

where :math:`\mathbf{\sigma}_s` and :math:`\mathbf{\sigma}_g` are the stress tensors of the solid and gaseous phase. The terms of acceleration are neglected. We also neglect gravity in the phases of the material, which would introduce a volume stress term in the equations :eq:`eq_solide` and :eq:`eq_gaz`.

The equation :eq:`eq_gaz` is homogenized in the thermochemical model taking the form of Darcy's law. The objective is to homogenize the momentum equilibrium equation of the solid phase using a volume-averaging method on the equation :eq:`eq_solide`. In the representative elementary volume :math:`V`, one can write:

.. math::
   - \langle \overrightarrow{\nabla} \cdot \mathbf{\sigma}_s \rangle = -  \overrightarrow{\nabla} \cdot \langle \mathbf{\sigma}_s \rangle - \frac{1}{V} \int_{A_{sg}} \mathbf{\sigma}_s \mathbf{n}_{sg} dA =\mathbf{0}
   :label: moyvtens_solide

where :math:`A_{sg}` is the surface between the solid and gaseous phases on the volume :math:`V` and :math:`\mathbf{n}_{sg}` is the normal vector in any point of :math:`A_{sg}` oriented towards the outside of the solid phase.

At the surface of the two considered phases, one can write the stress equilibrium:

.. math::
   \mathbf{\sigma}_s \mathbf{n}_{sg} = \mathbf{\sigma}_g \mathbf{n}_{gs}
   :label: equ_contrainte_Asg

and at every point of this surface, the stress tensor of the gas phase is:

.. math::
   \mathbf{\sigma}_g = P_g \mathbf{I}
   :label: equ_contrainte_sigmag

The equation :eq:`moyvtens_solide` can therefore transforms into:

.. math::
   -  \overrightarrow{\nabla} \cdot \langle \mathbf{\sigma}_s \rangle = \frac{1}{V} \int_{A_{sg}} P_g \mathbf{n}_{gs} dA
   :label: moyvtens_solide2

The integral term of pressure in the gaseous phase evaluated at the surface :math:`A_{sg}` also appears by applying a volume average operator to the pressure gradient in :math:`V`:

.. math::
   \langle \overrightarrow{\nabla} P_g \rangle =  \overrightarrow{\nabla} \cdot \langle P_g \rangle + \frac{1}{V} \int_{A_{gs}} P_g \mathbf{n}_{gs} dA
   :label: moypression

The equation :eq:`moyvtens_solide2` becomes:

.. math::
   -  \overrightarrow{\nabla} \cdot \langle \mathbf{\sigma}_s \rangle =  \langle \overrightarrow{\nabla} P_g \rangle -  \overrightarrow{\nabla} \cdot \langle P_g \rangle
   :label: moyvtens_solide3

Whitaker [WIT1986]_ indicates that one can consider:

.. math::

   \overrightarrow{\nabla} \cdot \langle P_g \rangle \simeq - \frac{1}{V} \int_{A_{gs}} P_g \mathbf{n}_{gs} dA \text{~~~~~, c'est-à-dire~~~~}  \langle \overrightarrow{\nabla} P_g \rangle \simeq \mathbf{0}
   \label{eq:moypression2}

if the ratio between the characteristic size of the microscopic scale and that of the macroscopic scale is very small. The momentum equation of the solid phase is then:


.. math::
   -  \overrightarrow{\nabla} \cdot \langle \mathbf{\sigma}_s \rangle =  -  \overrightarrow{\nabla} \cdot \langle P_g \rangle
   :label: moyvtens_solide4

that we can express in light notations:

.. math::
    -  \overrightarrow{\nabla} \cdot \widetilde{ \mathbf{\sigma}_s }  =  -  \overrightarrow{\nabla} \cdot \left( \varphi_g \widehat{P_g} \right)
    :label: moyvtens_solide5


Constitutive equations
----------------------

The assumption of an elastic behavior of the material is made and we suppose that, for every representative elementary volume :math:`V`, there exists an tensor :math:`\mathbf{S^*_s}` linking the averaged stress tensor :math:`\widetilde{ \mathbf{\sigma}_s }` to the averaged strain tensor :math:`\widetilde{\mathbf{\epsilon}_s}` in :math:`V`:

.. math::
   \widetilde{ \mathbf{\epsilon_s} } =  \mathbf{S^*_s} : \widetilde{ \mathbf{\sigma_s}} + \widetilde{ \mathbf{\epsilon^T_s} }
   :label: loidehookegen

where :math:`\widetilde{ \mathbf{\epsilon^T_s} }` is the thermoelastic strain tensor. A classical model of thermoelastic strain is choosen, such as:

.. math::
   \widetilde{ \mathbf{\epsilon^T_s} } = \beta_s \Delta \widehat{T} \mathbf{I}
   :label: def_thermoelast

where :math:`\beta_s`  is the thermoelastic expansion coefficient and :math:`\Delta \widehat{T} = \widehat{T} - \widehat{T_0}` is the temperature variation between the initial and the current state.

:math:`\mathbf{S^*_s}` is the compliance tensor and :math:`\mathbf{C^*_s}` is the stiffness tensor. These two fourth-order tensors are directly linked by the relation:

.. math::
   \mathbf{C^*_s} = \mathbf{S^*_s}^{-1}
   :label: def_tenscomplaisance

By applying the stiffness tensor relation to the equation :eq:`loidehookegen`, it becomes:

.. math::
   \widetilde{ \mathbf{\sigma_s}} = \mathbf{C^*_s} : \left( \widetilde{ \mathbf{\epsilon_s} }-\widetilde{ \mathbf{\epsilon^T_s} }  \right)
   :label: loidehookegen2

We are now interested in the expression of the local strain tensor :math:`\mathbf{\epsilon_s}`, function of the displecement vector of the solid phase :math:`\mathbf{u_s}`:

.. math::
   \mathbf{\epsilon_s} = \frac{1}{2} \left( \left(\overrightarrow{\nabla} \otimes \mathbf{u_s} \right) + ~ ^t\left(\overrightarrow{\nabla} \otimes \mathbf{u_s} \right) \right)
   :label: tens_deform

An average volume operator is used to express the averaged strain tensor :math:`\langle \mathbf{\epsilon_s} \rangle` as a function of averaged displacements vector :math:`\langle \mathbf{u_s} \rangle`:

.. math::
   \langle \mathbf{\epsilon_s} \rangle = \frac{1}{2} \left( \left(\overrightarrow{\nabla} \otimes\langle \mathbf{u_s} \rangle \right) + ~ ^t\left(\overrightarrow{\nabla} \otimes \langle \mathbf{u_s} \rangle \right) \right) + \frac{1}{2V} \int_{A_{sg}} \left( \left(\mathbf{u_s} \otimes \mathbf{n_{sg}} \right) +  ^t\left(\mathbf{u_s} \otimes \mathbf{n_{sg}} \right) \right) dA
   :label: tens_deform2

The integral term shows the effect of surface displacements due to the mechanical stress between the solid phase and the gas phase. Luo and DesJardin [LUO2012]_ develop the expression of this term by making particular assumptions on the geometry of the representative elementary volume. As a first approximation, we will neglect the effects of these surface displacements and summarize the strain tensor  :math:`\langle \mathbf{\epsilon_s} \rangle` to:

.. math::
   \langle \mathbf{\epsilon_s} \rangle = \frac{1}{2} \left( \left(\overrightarrow{\nabla} \otimes\langle \mathbf{u_s} \rangle \right) + ~ ^t\left(\overrightarrow{\nabla} \otimes \langle \mathbf{u_s} \rangle \right) \right)
   :label: tens_deform3

or in lightened notation:

.. math::
   \widetilde{ \mathbf{\epsilon_s} } = \frac{1}{2} \left( \left(\overrightarrow{\nabla} \otimes \widetilde{ \mathbf{u_s} } \right) + ~ ^t\left(\overrightarrow{\nabla} \otimes \widetilde{ \mathbf{u_s} } \right) \right)
   :label: tens_deform4

Thus, the expression of the momentum equilibrium equation on the solid phase is:

.. math::
   \overrightarrow{\nabla} \cdot \left[ \mathbf{C^*_s} : \left( \frac{1}{2} \left( \left( \overrightarrow{\nabla} \otimes \widetilde{ \mathbf{u_s} } \right) + ~ ^t\left(\overrightarrow{\nabla} \otimes \widetilde{ \mathbf{u_s} } \right) \right) - \beta_s \Delta \widehat{T} \mathbf{I} \right) - \varphi_g \widehat{P_g} \right]  = \mathbf{0}
   :label: moyvtens_solide6



Stiffness tensor
----------------

The stiffness tensor :math:`\mathbf{C^*_s}` is the elastic linear application that links the stain of the material to its stress from a mesoscopic point of view. It is a tensor of order 4 (in 3D). It contains theoretically :math:`3^4=81` coefficients. However, :math:`\widetilde{ \mathbf{\epsilon_s} }` and :math:`\widetilde{\mathbf{\sigma_s}}` are both symmetrical, so this number of coefficients can be reduced  to :math:`6^2=36`.

In Voigt notation, :math:`\widetilde{ \mathbf{\epsilon_s} }` and :math:`\widetilde{ \mathbf{\sigma_s}}` tensors can be written as:

.. math::
   \mathbf{ \widetilde{\sigma_s} }  = \begin{bmatrix}
    \sigma_{11} \\ \sigma_{22} \\\sigma_{33} \\\sigma_{23} \\\sigma_{13} \\\sigma_{12} 
    \end{bmatrix} \text{~~~~~~~et~~~~~~} 
     \mathbf{\widetilde{\epsilon_s} }  = \begin{bmatrix}
    \epsilon_{11} \\ \epsilon_{22} \\\epsilon_{33} \\2\epsilon_{23} \\2\epsilon_{13} \\2\epsilon_{12} 
    \end{bmatrix}
   :label: not_allege_sgma

The exponent :math:`V` is used to recall the contracted notation (or Voigt notation). The contracted expression of the stiffness tensor then reduces to a second-order tensor.

A medium is said to be **orthotropic** if all the properties of the material are invariant by a direction change obtained by symmetry around one of the three orthogonal symmetry planes.

A material is said **transverse** if its properties are invariant by a direction change obtained by symmetry around a given axis. It can be noticed that a transverse material is then orthotropic by definition.

The case of a unidirectional fiber composite material is, from a mesoscopic point of view, transverse. Taking into account the properties of
symmetry of this kind of material, we can reduce the stiffness tensor :math:`\mathbf{C}_{UD}^V` to:

.. math::
   \mathbf{C}_{UD}^V  = 
    \begin{bmatrix}
    C_{11} & C_{12} & C_{23} & 0 & 0 & 0\\
    C_{12} & C_{11} & C_{23} & 0 & 0 & 0\\
    C_{23} & C_{23} & C_{33} & 0 & 0 & 0\\
    0 & 0 & 0 & C_{44} & 0 & 0 \\
    0 & 0 & 0 & 0 & C_{44} & 0 \\
    0 & 0 & 0 & 0 & 0 & \frac{C_{11} - C_{12}}{2} \\
    \end{bmatrix}
   :label: not_allege_C2

having made the assumption that the symmetry axis is :math:`\mathbf{e_3}`. :math:`\mathbf{C}_{UD}^V` depends on 5 independent coefficients that can be expressed as:


.. math::
    C_{11} &= \frac{\left( 1-\nu_{13}^2 \frac{E_1}{E_3} \right) }{ \left( 1 +\nu_{12} \right) \delta }  E_1 \\
    C_{33} &=  \frac{  1-\nu_{12} }{ \delta } E_3 \\
    C_{12} &=  \frac{\left( \nu_{12}+\nu_{13}^2 \frac{E_1}{E_3} \right) }{ \left( 1 +\nu_{12} \right) \delta }  E_1 \\
    C_{23} &=  \frac{  \nu_{23} }{ \delta } E_1 \\
    C_{44} &= G_{13}
    :label: not_allege_C3

where:

*  :math:`E_1` is the Young's modulus in the :math:`\mathbf{e_1}` direction

*  :math:`E_3` is the Young's modulus in the :math:`\mathbf{e_3}` direction

*  :math:`\nu_{12}` is the Poisson's coefficient between :math:`\mathbf{e_1}` and :math:`\mathbf{e_2}`

*  :math:`\nu_{13}` is the Poisson's coefficient between :math:`\mathbf{e_1}` and :math:`\mathbf{e_3}`

*  :math:`G_{13}` is the shear modulus between :math:`\mathbf{e_1}` and :math:`\mathbf{e_3}`


Stiffness tensor rotation
-------------------------

We have defined momentum conservation system in the solid phase :eq:`moyvtens_solide6` valid in any reference in the space. The expression of the stiffness tensor of a unidirectional fiber material defined in the previous section is valid in the local coordinate system :math:`\left(\mathbf{e_1},\mathbf{e_2},\mathbf{e_3}\right)` where :math:`\mathbf{e_3}` is the fibers' main direction.

For the resolution of the system of mechanical equations, it is appropriate to express this tensor in the reference of the material :math:`\left(\mathbf{x},\mathbf{y},\mathbf{z}\right)`. Any tensor expressed in the reference of the material is indexed with :math:`xyz`  and any tensor expressed in the reference internal to the composite is indexed with :math:`123` in this section.

We make the assumption that any field in :math:`\left(\mathbf{e_1},\mathbf{e_2},\mathbf{e_3}\right)` can be described in the fixed coordinate system :math:`\left(\mathbf{x},\mathbf{y},\mathbf{z}\right)` by a rotation around the axis :math:`\mathbf{z} = \mathbf{e_3}` with an angle :math:`\theta` using a matrix :math:`R`, such as:

.. math::
   R  = 
    \begin{bmatrix}
    \cos(\theta) & \sin(\theta) & 0\\
    - \sin(\theta) &  \cos(\theta) & 0\\
    0 & 0 & 1
    \end{bmatrix}
   :label: mat_rotation

It is thus possible to define for any stress tensor and any strain tensor between the reference of the fibers and the fixed reference according to the relations:

.. math::
    \mathbf{\sigma}_{xyz} =  R ~\mathbf{\sigma}_{123} R^{-1} \\
    \mathbf{\epsilon}_{xyz} =  R ~\mathbf{\epsilon}_{123} R^{-1}
   :label: mat_rotation2
   
We can develop all the terms of the stress tensor and express them in Voigt notation:

.. math::
   \begin{bmatrix}
    \sigma_{xx} \\ \sigma_{yy} \\\sigma_{zz} \\\sigma_{yz} \\\sigma_{xz} \\\sigma_{xy} 
    \end{bmatrix}
    = 
    J_z
    \begin{bmatrix}
    \sigma_{11} \\ \sigma_{22} \\\sigma_{33} \\\sigma_{23} \\\sigma_{13} \\\sigma_{12} 
    \end{bmatrix}
   :label: mat_rotation3

and similarly for the strain tensor:

.. math::
   \begin{bmatrix}
    \epsilon_{xx} \\ \epsilon_{yy} \\\epsilon_{zz} \\2\epsilon_{yz} \\2\epsilon_{xz} \\2\epsilon_{xy} 
    \end{bmatrix}
    = 
    J_z
    \begin{bmatrix}
    \epsilon_{11} \\ \epsilon_{22} \\\epsilon_{33} \\2\epsilon_{23} \\2\epsilon_{13} \\2\epsilon_{12} 
    \end{bmatrix}
   :label: mat_rotation4

The matrix :math:`J_z` is the rotation matrix of strain and stress tensors in their contracted form. Its development leads to the following expression:

.. math::
    J_z = \begin{bmatrix} \\
    \cos(\theta)^2 & \sin(\theta)^2 &0 &0 &0&2 \cos(\theta)\sin(\theta) \\ 
    \sin(\theta)^2 & \cos(\theta)^2 &0 &0 &0& -2 \cos(\theta)\sin(\theta) \\ 
    0&0&1&0&0&0 \\
    0&0&0&\cos(\theta)&\sin(\theta)&0\\
    0&0&0&-\sin(\theta)&\cos(\theta)&0\\
    -\cos(\theta)\sin(\theta) & \cos(\theta)\sin(\theta) &0&0&0& \cos(\theta)^2 -  \sin(\theta)^2 \\
    \end{bmatrix}
    :label: mat_rotation5

It is possible to develop the tensor :math:`{\sigma}_{xyz}^V` in Voigt notation by:

.. math::
    {\sigma}_{xyz}^V &= J_z ~{\sigma}_{123}^V \\
    {\sigma}_{xyz}^V &= J_z ~ \left( \mathbf{C}_{123}^V ~ {\epsilon}_{123}^V \right) \\
    {\sigma}_{xyz}^V &= J_z ~ \left( \mathbf{C}_{123}^V ~ \left( J_z^{-1} {\epsilon}_{xyz}^V \right)\right)
    :label: mat_rotation6

Thus, there is a direct relationship between the stiffness tensor in the fibers' coordinate system and the material coordinate system, such as:

.. math:: 
    {\sigma}_{xyz}^V = \mathbf{C}_{xyz}^V ~ {\epsilon}_{xyz}^V
    :label: mat_rotation7

where

.. math::
    \mathbf{C}_{xyz}^V = J_z ~ \mathbf{C}_{123}^V ~J_z^{-1}
    :label: mat_rotation8

