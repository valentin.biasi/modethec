﻿External coupling with CWIPI
############################

Introduction 
************ 

The CWIPI library (Coupling With Interpolation Parallel Interface)  is a chaining and coupling library between two existing solvers. This one is developed at ONERA, notably by Eric Quémerais, and is used for many applications (fluid / structure interactions, aeroacoustics, plasma flows, ...). In addition, CWIPI is the standard coupling library for CEDRE and ElsA solvers. For more information, please visit the official web page of the library https://w3.onera.fr/cwipi/.

CWIPI has been defined as the reference coupling library between Modethec and an external solver with regard to the following possibilities:

* Interpolated field exchanges through a non-conforming interface.
* Independence from spatial discretization and temporal interpolation of coupled solvers.
* Interfaces in Fortran, C ++ and Python
* CWIPI interfaces already present and actively maintained in CEDRE
* Parallel exchanges via the MPI library.


CWIPI library link in Modethec
******************************

The CWIPI library integration into Modethec is done according to version 0.9.8, compiled with Intel MPI version 17. The Modethec git repository contains this compiled version, but Modethec users are free to compile and use their own version of the CWIPI library, available for download at the following address: https://w3.onera.fr/cwipi/telechargement.

All of CWIPI's communications are provided by the MPI protocol which is currently the standard for massively parallel computing. The link of CWIPI using the cmake tool can be done with the following lines in the ``CMakeLists.txt``:


::

    link_directories(<MODETHEC_DIR>/cwipi/lib)
    include_directories(<MODETHEC_DIR>/cwipi/include)
    target_link_libraries(modethec fvmc bftc cwipi cwipi_f)

with ``<MODETHEC_DIR>`` the source folder of Modethec. A classic compilation using a MakeFile is also possible by adding the following options in compilation phase:

::

    -I<MODETHEC_DIR>/cwipi/include

and in the linking phase:

::

    -L<MODETHEC_DIR>/cwipi/lib –lfvmc –lbftc –lcwipi –lcwipi_f


.. note:: Coupled applications must be launched in the same MPI environment. As a result, the compilation of the CWIPI library as well as that of the Modethec solver must be done with the same implementation of MPI, but also with that of the code with which it will have to communicate.


External coupling algorithm
***************************

In order to allow the coupling of Modethec with an external solver, the exchange algorithm that have been implemented is based on a chaining data principle with fixed time steps, as shown in :numref:`cwipi_phases`. For all the coupled codes, a first initialization phase is necessary in order to declare the name of the couplings, the coupled codes and the communicating domains or borders. Then different phases of exchanges follow one another at fixed time steps :math:`\Delta t_{COUPLING}` during which the exchanged fields are sent, interpolated, received by the coupled codes.


It is important to note that this type of chaining does not ensure the continuity and the differentiability of the thermal and mass fluxes at the interfaces. An iterative coupling system would make possible to converge the flows to the interfaces according to a fixed point method, but this type of algorithm must be suited for the third-party code.

.. _cwipi_phases:
.. figure:: img/cwipi_phases.png
    :width: 100%
    :align: center

    CWIPI coupling outline


The first coupling phase via CWIPI is related to the initialization of the interfaces between the solvers used. This phase is necessary for all the solvers using the CWIPI library and must be done before any exchange request, which otherwise would lead to a program error. The main steps of the initialization phase of the Modethec solver, as shown in :numref:`cwipi_initialization`, are:

* A step of coupling declaration, via the routine ``CWIPI_CREATE_COUPLING_F`` where are declared the name of the coupling (known in advance by the two coupled solvers), the name of the distant application and the various options like the type of mesh (static or mobile) or archiving options.

* A step of mesh declaration of the coupled interface of the local application according to the CWIPI formalism. This formalism uses multiple node location tables and 1D, 2D or 3D connectivity tables. In the framework of the Modethec coupling, the coupled interface mesh is of dimension ``n-1`` with respect to the dimension ``n`` of the computational domain. The mesh of the coupled interface is transmitted and stored in MPI buffer thanks to the ``CWIPI_DEFINE_MESH_F`` routine and will be used to build the interpolation functions during CWIPI exchanges.

* Finally, a node location test step is performed through the routine ``CWIPI_LOCATE_F``. This step makes sure that all the nodes of the local mesh are well located on the remote mesh. If some nodes can not be located, a warning message is sent to the user.


.. _cwipi_initialization:
.. figure:: img/cwipi_initialization.png
    :width: 40%
    :align: center

    Main steps of the initialization phase


At each :math:`\Delta t_{COUPLING}` but also at :math:`t = 0`, exchange phases are operated, as shown in :numref:`cwipi_exchanges`. These exchanges are made using non-blocking communications in order to allow a greater interoperability of the solvers. Thus for each boundary requiring a coupling with a third-party application:

* A call to the ``CWIPI_IRECEIVE_F`` subroutine is used to initiate the reception of the interpolated data in an intermediate array.

* The data to be sent is interlaced and stored in a send table.

* A call to the ``CWIPI_ISSEND_F`` subroutine sends the data in the CWIPI swap space.


Following this first step, the received data are verified and restructured for each boundary according to the following steps:

* A call to the subroutines ``CWIPI_WAIT_ISSEND_F`` and ``WIPI_WAIT_IRECV_F`` forces the process to go in "blocking mode", and thus to ensure the good sending and the good reception of the exchanged data before going on to the following iterations.

* The received data is then de-interlaced and re-implanted into the Modethec heterogeneous boundary condition tables.


.. _cwipi_exchanges:
.. figure:: img/cwipi_exchanges.png
    :width: 60%
    :align: center

    Main steps of the exchanges phases
