﻿Solver settings
###############

This section details specific parameters for each solver (heat diffusion, gas transport, ...). For each solver, settings are detailed for each parameter tab.

Heat diffusion
**************

Physical settings
-----------------

* **Thermal conductivity homogenization method**: It specifies the homogenization method used for thermal conductivity (law of mixture or Mori-Tanaka). The law of mixture is the simplest homogenization method where species' thermal conductivities are weighted with volume fraction. The Mori-Tanaka homogenization method uses structure dependent parameters (ellipsoidal definition of species) to evaluate the homogenized thermal conductivity. Technical references are available at :ref:`mori-tanaka_homogenization`.

Species settings
----------------

* **Species specific heat**: The specific heat :math:`C_P` of each species can be defined **constant** or **polynomial**, depending on the species specific heat model option. In case of a polynomial definition, the number of polynomial coefficients (up to 6) edits the number of editable rows.

* **Species thermal conductivity**: The thermal conductivity :math:`\mathbf{k}^{*}` of each species can be defined **constant**, **isotropic polynomial**  or **orthotropic polynomial**. The constant option defines an isotropic and temperature-independent thermal conductivity, the isotropic polynomial defines a temperature-dependent thermal conductivity and the orthotropic polynomial defines a directional and temperature-dependent thermal conductivity. For each polynomial function, up to 6 coefficients can be set up.


* **Eshelby tensor**: Eshelby tensors are defined for each species when the Mori-Tanaka homogenization method is used. Exx, Eyy and Ezz are the diagonal values of the Eshelby tensors. For any inclusion-shaped species, the sum of the Exx, Eyy and Ezz must be equal to 1. For any continuum-like species, all values of the Eshelby tensors must be set to 0. Only orthotropic definition of the Eshelby tensors are available so far. Technical references are available at :ref:`mori-tanaka_homogenization`.

* **Surface emissivity and absorptivity**: Surface emissivity and absorptivity are set for each solid species. Homogenized values of emissivity and absorptivity are calculated using solid volume averaging.


Numerical settings
------------------

* **Time integration numerical method**: Users can choose the time integration method for the heat diffusion between the Euler-explicit method and a general theta-implicit method. The Euler-explicit method has to comply with the Fourier stability criterion:

.. math::
   Fo= \frac{ \mathbf{k}^{*} \Delta t }{ \rho C_P \Delta L ^2} \leq 1/2
   :label: fourier_condition

where :math:`\Delta L` is the local space step. The general theta-implicit method has no explicit stability criterion and its theta parameters can be modified in the advanced numerical parameters (see :ref:`advanced_numerical_parameters`).

Boundary conditions settings
----------------------------

* **Thermal boundary condition type**: 3 types of boundary conditions types are available: **Imposed heat flux**, **Imposed temperature** and **Combined heat flux**. The combined heat flux type is the most general type, combination of an impinging heat flux, a radiative and a convective heat transfer condition.

* **Imposed temperature**: It is the boundary temperature when the **Imposed temperature** type is applied.

* **Imposed heat flux**: It is the boundary net heat flux when the **Imposed heat applied** type is applied, or the raw heat flux when the **Combined heat flux** is applied.

* **Radiative heat transfer temperature**: It defines the radiative environment temperature.

* **Convective heat transfer temperature and coefficient**: It defines the convective environment temperature and the associated heat transfer coefficient.


Gas transport
*************

Physical settings
-----------------

* **Permeability definition**: 4 types of homogenization methods are available for the permeability parameter. It can be defined as constant, using the Henderson model (linear evolution between a virgin and a charred value) or using an isotropic or orthotropic Kozeny-Carman model (power law as a function of porosity) as:


.. math::
   \mathbf{K_P} = \mathbf{K_{P_0}} \frac{ \varphi_g^3 }{ (1- \varphi_g)^2}
   :label: kozeny_carman

* **Maximum permeability value**: Whatever the permeability homogenization method is, the permeability can be set to a maximum value using this parameter.

* **Gas phase dynamic viscosity**: The gas phase dynamic viscosity :math:`\mu_g` can be defined **constant** or **polynomial**. In case of a polynomial definition, the number of polynomial coefficients (up to 6) edits the number of editable rows.


Numerical settings
------------------

* **Time integration numerical method**: Users can choose the time integration method for the gas transport between the Euler-explicit method, the general theta-implicit method and the CFL-based theta-implicit method. The Euler-explicit method has to comply with the CFL stability criterion, as:

.. math::
   CFL = \frac{ \mathbf{v_g} \Delta t }{ \Delta L} \leq 1
   :label: CFL_condition

For best stability results, maximum CFL values must not exceed 0.5. The general theta-implicit method is similar to the heat diffusion integration method and uses the global time step. The CFL-based theta-implicit method is a sub-stepped theta-implicit method, in accordance with the maximum CFL value in the domain. The maximum CFL is specified by the user. This integration method is recommended when the porosity and/or the permeability of the domain evolves due to chemical reactions, using a maximum CFL value of 0.5.

Boundary conditions settings
----------------------------

* **Mass flow boundary condition type**: 2 types of boundary conditions types are available: **Imposed mass flow rate** and **Imposed external pressure**.

* **Imposed external pressure**: The external pressure of a domain is set with this parameter.

* **Imposed mass flow rate**: The mass flow rate can be imposed with this parameter. For stability reasons, this parameter has to be positive. Gas suction can only be set using an external pressure condition.



Kinetic reactions
*****************

Reactions definition
--------------------

The reactions tab activates only if the reaction solver is checked in the general tab. It allows to specify the number of reactions and their names, as shown on :numref:`reaction_definition`. Users can double-click on the reactions name table to edit the reactions names. The parameter tree will be modified in consequence in a sub-tree, containing the list of specified reactions.

.. _reaction_definition:
.. figure:: img/modethec_gui/reaction_definition.png
    :width: 90%
    :align: center

    Reactions definition overview


Reactions settings
------------------

For each defined reaction, parameters can be defined by clicking on the right reaction row in the parameter tree, as shown on :numref:`reaction_settings`.

.. _reaction_settings:
.. figure:: img/modethec_gui/reaction_settings.png
    :width: 90%
    :align: center

    Per Reaction settings overview


Common parameters for reactions definition are:

* **Species ID**: For each reaction, reactants and products has to be define using the species ID, as configured in the species definition table. At least one solid reaction has to be defined. If no solid product is set, the parameter has to remain at 0. Multiple gaseous products can be defined for each reaction using the `Number of gas phase products` spinbox.

* **Stoichiometric mass coefficients**: For each reactant or product of the selected reaction, a stoichiometric mass coefficient has to be defined. The sum of mass coefficient of reactants must be equal to the mass coefficient of products.

* **Oxidative reaction**: A reaction can be activated or deactivated in presence of :math:`O_2` depending of the surrounding atmosphere. If a reaction is defined as oxidative, it will only be activated if the atmosphere is set to air or :math:`O_2`.

* **Heat of reaction**: If defines the total consumed or released energy per quantity of solid reactant consumed (in :math:`J/kg`).

* **Arrhenius kinetic parameters**: :math:`A`, :math:`E_A` and :math:`n` defines the kinetic parameters of an Arrhnius kinetic reaction. Technical references are available at :ref:`chemical_reactions`.

Mechanics
*********

.. warning:: The mechanical solver is only available in 1D or 2D. You can not use this solver in 3D. Moreover, this solver has never been evaluated against classical finite elements mechanical solvers.

Physical settings
-----------------

* **Mechanical properties model**: 2 types of properties models are available for mechanical solver. It can be isotropic or transverse orthotropic. Only elastic properties of a material can be defined.

* **Elastic properties**: The Young modulus (isotropic or orthotropic), the Poisson coefficient and the transverse shear modulus (only for transverse orthotropic model) can be defined in the Physical tab.

* **Thermal expansion properties**: Depending of the mechanical properties model, the thermal expansion coefficient :math:`\beta` can be defined as isotropic or orthotropic.

Boundary conditions settings
----------------------------


* **Mechanical boundary type**: 3 types of mechanical boundary types are available: **Stress** for imposed stress condition, **Constrained** to avoid any displacement of the boundary, or **Symmetry axis** to avoid any displacement out of the axis.

* **Normal and tangential stress**: For an imposed stress condition, users has to define the normal and tangential stress of the boundary.



Ablation
********

To be filled


.. _external_coupling:

External coupling
*****************

To define an external coupling in Modethec, the **External coupling** solver has to be activated in the General tab. Then the external coupling time step has to be defined in the Numerical tab. This time step has to be a multiple of the global time step.

For each boundary condition coupled to an external application, exchanged data must be defined directly in the parameters tab of the selected boundary. :numref:`cwipi_coupling` shows the external coupling settings of a boundary condition. The external coupling is activated with the checkbox "Activate external coupling", which makes it possible to display the definition zone of the coupled data. For each coupled boundary, it is necessary to define:

* **Application name to be coupled via CWIPI**: It is the name of the remote application with which Modethec will communicate. The name of the local MoDeTheC application is simply declared as modethec or as the name of the mesh filename in the case of a Modethec / Modethec coupling.


* **Number of variables to be sent and received**: Changing this setting will change the size of the array of sent and received variables.


* **List of variables to be sent and received**: In the first column, users have to define the names of the internal variables according to a choice of drop-down list whose possibilities are given in :numref:`coupling_variables`. In the second column, the index of the exchanged table is given, i.e. the dimension in the case of a vector or the species number in the case of a volume or mass fraction. For exchanges of scalar quantities, this index should remain at 1. In the third column, the name of the variables for the CWIPI data export is entered. This name has no impact on the computation.


.. _cwipi_coupling:
.. figure:: img/modethec_gui/cwipi_coupling.png
    :width: 90%
    :align: center

    External coupling settings overview



.. _coupling_variables:

.. csv-table:: List of exchanged variables for external coupling
   :header: Internal variable name, Description
   :widths: 30, 70
   :stub-columns: 1
   :header-rows: 0

    Fimp , Heat flux
    Timp, Wall temperature
    T_conv , Convective environment temperature
    h_conv , Convective heat transfer coefficient
    T_rad , Radiative environment temperature
    Dimp , Wall mass flow rate
    Pimp , Wall pressure


Data exchanges with Modethec are designed to be modular. There is no specific order on exchanged data, minimum or maximum sending number or number of external applications. However, some additional options could be implemented to allow more possibilities, such as volumetric coupling to allow a thermomechanical coupling, the coupling with several solvers on the same limit to allow the interaction of several types of physics on the basis of the same solver or the possibility of having a temporal loopback to allow strong coupling.


Each variable received during exchanges with an external solver overwrites the value already stored in the parameter tables of the boundary conditions. Thus, the condition type has an importance on how the received data are interpreted. For example, a wall temperature received by Modethec via a CWIPI exchange can only affect the system if the chosen condition is of the **Imposed temperature** type. If this is not the case, the temperature will be received by Modethec but will have no influence on the computation. For exchanges of heat quantity at the interfaces, it is generally chosen that one of the solvers sends a temperature information and receives a heat flow while the remote solver performs the symmetrical operation. It is similar for mass exchanges, where the first solver sends a pressure information and receives a mass flow rate. From a more formal point of view, the first application must be constrained by a Neumann condition while the second must be constrained by a Dirichlet condition.


Launching two applications coupled with CWIPI must be done in the same MPI instance. Thus, for two coupled Modethec applications, the following command line will be used:

::

    mpirun –np 1 modethec file1.prm : -np 1 modethec file2.prm


External coupling with CHARME
-----------------------------

In the specific case of a Modethec coupling with the CEDRE/CHARME solver, some specificities must be taken into account. To perform a heat flu / temperature coupling, the ``Type 10`` wall model can be used provided that the associated CHARME boundary condition is considered at imposed temperature and the external coupling checkbox is activated. In addition, an additional parameter file named ``cwipi.dat``, must be stored in the CEDRE computation folder. This file defines the set of parameters used for all coupling of the CEDRE computation.

The following code block shows an example of a ``cwipi.dat`` file configured for an heat flux / temperature coupling of a wall named ``Plaque_Haut_Met`` in the CEDRE parameters file and ``BOTTOM`` in the Modethec parameters file. This file consists of two main blocks:

* A first block that defines the couplings, with for each coupling, the name of the CEDRE boundary, the name of the coupling that corresponds to the name of the Modethec boundary, the name of the local application and the name of the remote application.

* A second block which defines the exchanges modes (send or receive), the coupling time steps of each exchange, and the labels of the internal quantities exchanged. The label ``Tparoi`` is used to denote the wall temperature and ``Qchaleur`` for the wall heat flux.

.. code-block:: fortran
    :linenos:

    !------------------------------------------------------------------------------!
    ! Nombre de domaines utilisateurs concernes par l'echange volumique            !
    !------------------------------------------------------------------------------!
    0                                                                              !
    !                                                                              !
    !                                                                              !
    !------------------------------------------------------------------------------!
    ! Nombre de limites concernees par l'echange surfacique                        !
    !------------------------------------------------------------------------------!
    1                                                                              !
    !                                                                              !
    ! Liste des limites couplees                                                   !
    !-------------------------                                                     !
    Plaque_Haut_Met   BOTTOM   charme  mlg_plaque.msh ! nom limite, couplage, solveur!
        1     0                 ! periode d'archivage CWIPI, actu connexion        !
    !                                                                              !
    ! Nombre de modes d'echange et ordre sur les limites                           !
    !---------------------------------------------------                           !
    !                                                                              !
        2                ! nombre de modes d'echange et ordre                      !
    !                                                                              !
        isend     BOTTOM  ! mode echange, nom de la limite concernee               !
        0.000  2.e-2     ! dt avant premier couplage, dt entre 2 couplages         !
        1  0             ! action it_deb, action it_fin                            !
        1                ! nombre de variables surfaciques à envoyer               !
        Qchaleur                                                                   !
        irecv     BOTTOM  ! mode echange, nom de la limite concernee               !
        0.000  2.e-2     ! dt avant premier couplage, dt entre 2 couplages         !
        1  0             ! action it_deb, action it_fin                            !
        1                ! nombre de variables surfaciques à recevoir              !
        Tparoi                                                                     !
    !                                                                              !
    !------------------------------------------------------------------------------!


For a heat flux / temperature and gas flow rate coupling, it is necessary to modify the type of boundary condition to ``type 11`` with the ``Imposed blow / flow`` option in the CHARME parameters. The ``type 11`` boundary must be temperature-constrained and have a porosity equal to 1. The following code block shows an example of a ``cwipi.dat`` file for a gas flow rate coupling. The first part of the file remains unchanged compared to the case of heat flux /temperature coupling. However the exchanged variables are modified:

* A mass flow rate sent by Modethec, named ``Debit_t11``

* A gas flow temperature sent by Modethec, named ``Tgaz_t11``

* A wall temperature sent by Modethec, named ``Tparoi_t11``

* A heat flux sent by CHARME, named ``Qchaleur``


.. code-block:: fortran
    :linenos:
    
    !------------------------------------------------------------------------------!
    ! Nombre de domaines utilisateurs concernes par l'echange volumique            !
    !------------------------------------------------------------------------------!
    0                                                                              !
    !                                                                              !
    !                                                                              !
    !------------------------------------------------------------------------------!
    ! Nombre de limites concernees par l'echange surfacique                        !
    !------------------------------------------------------------------------------!
    1                                                                              !
    !                                                                              !
    ! Liste des limites couplees                                                   !
    !-------------------------                                                     !
    Plaque_Haut_Met   BOTTOM   charme  modethec_plaque.msh   ! nom limite, nom couplage, nom solveur!
        1     0                 ! periode d'archivage CWIPI, actu connexion        !
    !                                                                              !
    ! Nombre de modes d'echange et ordre sur les limites                           !
    !---------------------------------------------------                           !
    !                                                                              !
        2                ! nombre de modes d'echange et ordre                      !
    !                                                                              !
        irecv     BOTTOM  ! mode echange, nom de la limite concernee               !
        0.000  2.e-2     ! dt avant premier couplage, dt entre 2 couplages         !
        1  0             ! action it_deb, action it_fin                            !
        3                ! nombre de variables surfaciques à envoyer               !
        Debit_t11                                                                  !
        Tgaz_t11                                                                   !
        Tparoi_t11															       !
        isend     BOTTOM  ! mode echange, nom de la limite concernee               !
        0.000  2.e-2     ! dt avant premier couplage, dt entre 2 couplages         !
        1  0             ! action it_deb, action it_fin                            !
        1                ! nombre de variables surfaciques à envoyer               !
        Qchaleur                                                                   !
    !                                                                              !
    !------------------------------------------------------------------------------!


A CHARME-Modethec coupled computation is launched by a command like:

::

    mpirun –np 63 cedre -cwipi -dir=CHARME -lc=charme : -np 1 modethec fichier1.prm


The ``-cwipi`` command-line option specifies the use of CWIPI as an external coupling library, the ``-lc`` option specifies the name of the local application, and the ``-dir`` option specifies the folder in which CEDRE will be launched. The use of a separate folder for CEDRE computations is advisable because it allows to clarify the working files and to avoid possible files recoveries.



