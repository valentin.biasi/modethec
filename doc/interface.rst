.. _interface_overview:

Interface overview
##################

The modethec interface can be launched in any terminal with the command:
::

    modethec_gui

If any problem is encountered, please refer to the :doc:`/install` section.

The main window of the modethec interface looks like:


.. _general_view:
.. figure:: img/modethec_gui/general_view.png
   :width: 100%
   :align: center

   General view of the modethec interface

The different zones in the modethec interface are:


**1. Main menu**

The main menu is dedicated to the management of parameter files. In the **File** menu, the following options are available:

* **New**: to create a new empty parameter file 
* **Open**: to open an existing parameter file. Some checks are done at each opening to verify the integrity of existing parameter files.
* **Save** and **Save as**: to save a file with the ``.prm`` extension. **Save** become **Save as** if no file has been save yet.
* **Revert**: to revert the file in the opening state or the last saved version. If the file has never been saved, modethec create a new empty parameter file.
* **Close**: to close the modethec interface.

.. note:: Please note that modethec proposes to save the parameter file before closing if any modification is encountered based on the opening state.


**2. Parameter tree**

The parameter tree is dedicated to the selection of the grid view. By default, the selected grid view is the "General" tab. Details about the parameter tree are given in the :doc:`/general_settings` section.


**3. Grid view**

The grid view is the main zone of the modethec interface. After selecting a parameter tab in the parameter tree, the grid view exhibits the options linked to it.


**4. Plot mesh**

The plot mesh button exhibits the mesh given in the ``Mesh filename`` entry of the "General" tab. The mesh file is converted to a ``.vtk`` file and plotted with the 2D/3D viewer.

**5. Run**

A modethec solver instance can be launched with the "Run" button as soon as the parameter file is saved. When running modethec with the "Run" button, it can also be stopped with the same button as shown below.


.. _in_progress_modethec:
.. figure:: img/modethec_gui/in_progress_modethec.png
   :width: 100%
   :align: center

   In progress computation in the modethec interface

**6. Run console**

All outputs of modethec instances are printed in the run console.


**7. Plot XY**

This button shows the plot section of the modethec interface. To view the XY plot section, modethec has to be run at least once with the ``Export integrated variables (HDF5 format)`` option in the "History" tab. Then a HDF5 file is created (named with the export filename). The XY plot function loads this file and plot the selected variables. The plot section looks like the :numref:`general_plot_view`. 

.. Functionalities of the plot XY are detailed in the :doc:`/results` section.


.. _general_plot_view:
.. figure:: img/modethec_gui/general_plot_view.png
   :width: 90%
   :align: center

   Plot XY viewer of the modethec interface


**8. Plot 2D/3D**

This button shows the 2D/3D contour viewer of the modethec interface. To view the output data directly in the modethec interface, ``VTK`` output format has to be selected in the "Export" tab. The plot section looks like the :numref:`general_2D3D_viewer`.

.. Functionalities of the 2D/3D contour viewer are detailed in the :doc:`/results` section.

.. _general_2D3D_viewer:
.. figure:: img/modethec_gui/general_2D3D_viewer.png
   :width: 90%
   :align: center

   2D/3D viewer of the modethec interface

