Installation
############


Requirements 
************ 

MoDeTheC is a multi-platform solver that has been tested and validated under Linux and Windows. MoDeTheC uses two independent programs, one for the user interface ``modethec_gui`` coded in Python and one for the solver ``modethec`` (or ``modethec.exe`` under Windows) coded in Fortran.

For the user interface, a version of Python 2 (minimal Python 2.7.8) is required.

For the solver, a recent Fortran compiler is needed. MoDeTheC has been tested and validated with Intel Fortran compiler, but with minimal modifications of the compilation flags, GNU compilers can also be used. 

The required components are:

* ``git``
* ``cmake`` (minimal 3.2)
* ``ifort`` (minimal 15, with MPI and openMP)
* ``python`` (minimal 2.7.8)

To grab all the modethec project, including the source code, the documentation, and the tutorials, you are encouraged to use ``git`` to clone the project and stay up-to-date of the future developments.

::

    git clone http://login@git-dri.onecert.fr/modethec

where ``login`` has to be replaced by your own login.

.. note:: If you are not able to access to the git server, please send a email to the project administrator at gillian.leplat@onera.fr

.. warning:: If you can not resolve the git URL, it may be a proxy problem. If you are under a ONERA workstation, please set your environment variable ``http_proxy`` to ``http://proxy.onera``


To get access to the modethec executables from any directory, you have to add the following lines to the ``.bashrc`` file in your ``$HOME`` directory:
::

    export PATH=~/modethec:$PATH
    export PATH=~/modethec/gui:$PATH



Compilation
***********

Once you pulled all the modethec project in your ``$HOME`` directory, you are able to create a MakeFile using cmake in the ``make/`` folder and run the compilation process. We encourage you to create subdirectories in order to handle multiple configurations.

To generate the MakeFile, please follow the instructions:
::

    cd modethec/make
    mkdir release
    cd release
    cmake ..

The ``release`` directory is just an example and different directories can be used depending of your different configurations.

.. note:: Please note that if your directory name contains the ``debug`` keyword, debugging flags will be automatically activated. To force the debugging flags, you can use the following command:
    ::

        cmake -DCMAKE_BUILD_TYPE=Debug ..

Now, you can compile modethec with:
::

    make

.. note:: You can print all the compilation command with the command:
    ::
        
        make VERBOSE=1


.. warning:: If you encounter problems during the compilation process, please ensure that the environment variables ``$I_MPI_ROOT`` and ``$MKLROOT`` are set correctly.



Python modules
**************

This set of Python modules are needed to use all features of the modethec project:

* h5py
* matplotlib
* mayavi
* numpy
* pyface
* PyQt4
* silx
* Sphinx
* sphinx-rtd-theme
* traits
* traitsui
* vtk
* xlrd

.. note:: The use of ``pip`` is advised to install all necessary python modules, as:
    ::
        
        pip install numpy

    If you are not administrator of your workstation and/or under a proxy, you should install these modules with the following command:
    ::
        
        pip install numpy --user --upgrade --proxy=http://proxy.onera


Usage
*****

User interface
--------------

The modethec interface can be launched in any terminal with the command:
::

    modethec_gui

Any parameter file ``.prm`` can be directly opened with the command:
::

    modethec_gui input_file.prm

or can be converted to an up-to-date file with:
::

    modethec_gui input_file.prm -o out_file.prm

Please, refer to the :doc:`/interface` page for more information. All options can be printed with:
::

    modethec_gui -h



Command line
------------

The modethec solver can be launched without any call to the graphical interface with:
::

    modethec input_file.prm

where the ``.prm`` extension file is a parameter file with arguments detailed in :doc:`/general_settings`.


Documentation
-------------

Modethec documentation is managed with Sphinx, a documentation generator under the BSD license. It can generate HTML, PDF with laTeX, Qt Help, ePub,...

First, the output documentation needs to be compiled. In the ``doc/`` directory, type:
::
    
    make html

to generate the documentation website, that could be find at ``doc/build/html/index.html``. The PDF format can be generated with:
::
    
    make latexpdf

, available at ``doc/build/latex/modethec.pdf``. A Qt documentation can also be generated with Sphinx:
::
    
    make qthelp
    qcollectiongenerator build/qthelp/modethec.qhcp

, accessible in the Help menu of ``modethec_gui``.


.. _validation_script:

Validation script
-----------------

A validation script is available in the ``tutos/`` directory, and can be used to check the integrity of the installation.
::
    
    regression.py

It checks that parameter files are read and written correctly by ``modethec_gui`` and that the solver results are identical to reference data. By default, it runs every tutorial case and print statistics. Options of this script are revealed with:
::
    
    regression.py -h

Available arguments are:

* ``-compare`` or ``-c``: print differences for parameter and results files with tkdiff using reference data
* ``-update`` or ``-u``: update parameter and results reference data in the ``ref/`` directories
* ``-only`` or ``-o``: test only some cases in the given list and not all cases
* ``-verbose`` or ``-v``: print all modethec output
* ``-nogui``: do not run validation script on parameter files
* ``-noexe``: do not run the modethec solver
* ``-yes`` or ``-y``: answer yes to all update and compare propositions


Windows install
***************

Spécifications
--------------

* Version de Visual Studio (min 17)
* Compilateur Intel (min 17) avec librairies MKL
* Librairie HDF5 précompilée "Pre-built Binary Distributions" à l'adresse suivante : ``https://www.hdfgroup.org/downloads/hdf5/``

Compilation
-----------

* Commenter dans les sources modethec tous les appels aux fonctions de ``mod_solve_C.f90``
* Modifier les chemins vers les librairies externes (MKL et HDF5) dans le fichier ``make/modethec.vfproj`` en fonction de votre install
* Lancer la compilation de modethec (debug ou release)
* Rajouter les modules manquants suivants les erreurs de compilation.
