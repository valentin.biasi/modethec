﻿Thermo-chemical model
#####################

Introduction
************

In MoDeTheC, any material is represented in this model as a mix of different solid and gas components. For instance, in composite materials, solid components are usually virgin fibers or matrix at the virgin or charred state, or for a more classical representation, the whole solid phase and the charred phase. The gas phase is considered as a mix of several gases. In the MoDeTheC solver, a material is modeled by a set of :math:`I` components including :math:`J` gaseous components. The subscript :math:`i` refers to any component while the subscript :math:`j` refers only to a gaseous component.

The delamination phenomena as well as other mechanical damages are ignored. Decomposition gases are assumed inert and ideal, and no liquid component (oil, tar) are considered. Every :math:`i` component is modeled as a continuum, in order to apply volume averaging operations.

Volume averaging operators
**************************

An averaging volume :math:`V` is defined as large relative to pore diameters (microscopic scale) and small relative to the entire material (macroscopic scale). The volume fraction function of the :math:`i` component is defined in :math:`V` as:

.. math::
    \varphi_{i} = \frac{V_{i}}{V}
    :label: def_phi_alpha

where :math:`V_i` is the volume of :math:`i`. The porosity :math:`\varphi_g` is calculated as the sum of gaseous volume fractions:

.. math::
   \varphi_g = \sum \limits_{j=1}^J \varphi_j
   :label: def_varphi_g


.. _volumeV_prelim:
.. figure:: img/volumeV_regression_surf_en.png
   :width: 50%
   :align: center

   :math:`i` component within the mesoscopic elementary volume :math:`V`


For any local function :math:`\Psi` of the :math:`i` component, the extrinsic phase average :math:`\langle\Psi_{i}\rangle` is defined as:

.. math::
    \langle\Psi_{i}\rangle = \frac{1}{V} \int_{V_{i}} \Psi_{i} dV
    :label: def_psi_alpha


and the intrinsic phase average :math:`\widehat{\Psi_{i}}` is defined as:


.. math::
   \widehat{\Psi_{i}} = \frac{1}{V_{i}} \int_{V_{i}} \Psi_{i} dV = \frac{1}{\varphi_i} \langle\Psi_{i}\rangle
   :label: def_psi_alpha_2

An immediate application of these definitions is the absolute density :math:`\widehat{\rho_{i}}` of the :math:`i` component (intrinsic phase average):

.. math::
    \widehat{ \rho}_i =  \frac{1}{V_{i}} \int_{V_{i}} \rho_{i} dV = \frac{1}{\varphi_i} \langle\rho_{i}\rangle
    :label: def_rhoi

Absolute density of every solid component is assumed constant. The weighted density :math:`\langle\rho\rangle` is given by the relation:

.. math::
    \langle\rho\rangle = \frac{1}{V} \sum \limits_{i=1}^I { \int_{V_i} \rho_i dV } =  \sum \limits_{i=1}^I \varphi_i \widehat{ \rho}_i
    :label: def_rhotot

The mass fraction function of each :math:`i` component is defined in :math:`V` as:

.. math::
    Y_i = \frac{\langle\rho_i\rangle }{\langle\rho\rangle }
    :label: def_frac_mass_vol

The use of derivation operations are necessary to homogenize conservation equations. Using volume averaging method on :math:`V`, the temporal derivate of the function :math:`\Psi_i` is defined as:

.. math::
   \langle \frac{\partial \Psi_{i}}{\partial t}\rangle = \frac{\partial}{\partial t}\langle\Psi_{i}\rangle - \frac{1}{V} \int_{A_{i}}  \Psi_{i} \mathbf{W_{i}} \cdot \mathbf{n_{i}} dA
   :label: def_moy_col_temporelle

where :math:`A_{i}` is the interface area and :math:`\mathbf{n_{i}}` is
the normal mathbfor on :math:`A_i` as shown on :numref:`volumeV_prelim`
. :math:`\mathbf{W_{i}}` is the displacement
velocity of the :math:`i`-interface. A similar relation is used for
gradient operator as:

.. math::
   \langle \boldsymbol{\nabla} \Psi_{i}\rangle = \boldsymbol{\nabla} \langle\Psi_{i}\rangle + \frac{1}{V} \int_{A_{i}}  \Psi_{i}  \mathbf{n_{i}} dA
   :label: def_moy_col_divergent

.. _chemical_reactions:

Chemical reactions
******************

A set of :math:`M` chemical reactions is considered to model pyrolysis or oxidation transformations. It is assumed that only solid components can react. The general form of the :math:`m` reaction is expressed as:

.. math::
   \nu_{\mathcal{R}m} \mathcal{R}_m \hspace{0.5em} + \hspace{0.5em} \nu_{O_2m}  O_2 \hspace{0.5em} \stackrel{+Q_m}{\longrightarrow} \hspace{0.5em} \nu_{\mathcal{P}m} \mathcal{P}_m \hspace{0.5em} + \hspace{0.5em} \sum \limits_{l=1}^L \left( \nu_{lm} \mathcal{G}_{lm} \right)
   :label: react_glob

where :math:`\mathcal{R}_m` is a solid reactant, :math:`O_2` is the oxygen (if present in the environment atmosphere and if oxidation reactions occur), :math:`\mathcal{P}_m` is a solid product and :math:`\mathcal{G}_{lm}` is a gas product (in the set of :math:`L` gas products emitted in the :math:`m` reaction). :math:`\nu` is used to denote stoichiometric mass coefficients and the heat of reaction :math:`Q_m` is introduced to express the generated or consumed heat of the :math:`m` reaction for each consumed quantity of :math:`R_m`. The reaction growth of :math:`m` is given by the function :math:`\alpha_m`:

.. math::
   \alpha_m = 1 - \frac{\langle\rho_{\mathcal{R}_m}\rangle}{\langle\rho_{\mathcal{R}_m}\rangle_\Sigma}
   :label: def_alpha_m

Terms with the subscript ":math:`\Sigma`" correspond to total quantities of :math:`\mathcal{R}_m` hypothetically generated, as detailed in equation :eq:`def_rho_tot_m`. :math:`\alpha_m` function evolves from :math:`0` at the initial state to :math:`1` when the decomposition reaction is completed. The latter is expressed as a remaining mass ratio of :math:`\mathcal{R}_m` and is driven by an Arrhenius’ law:

.. math::
   \frac{\partial \alpha_m}{\partial t} = f_m(O_2)\left( 1 - \alpha_m \right) ^{n_m} A_m \exp\left( \frac{-E_{A_m}}{r_{g}T}\right)
   :label: arrhenius_generale

where :math:`A_m`, :math:`E_{A_m}` and :math:`n_m` are the Arrhenius’ parameters for the :math:`m` reaction. The factor :math:`f(O_2)` in equation :eq:`arrhenius_generale` takes into account the oxygen presence on the reaction growth :math:`\alpha_m` in a simple way, as:

.. math::
   f_m(O_2) = 1 \hspace{0.3em} \textrm{if} \hspace{0.3em} \nu_{O_2m} = 0  , \hspace{1em} f_m(O_2) =  1 \hspace{0.3em} \textrm{otherwise}
   :label: fo2

The mass reaction rate :math:`\dot \omega_{Rm}` (mass loss :math:`\mathcal{R}_m` per volume unit) is linked to :math:`\alpha_m` by the expression:

.. math::
   \dot \omega_{\mathcal{R}m} = - \langle\rho_{\mathcal{R}_m}\rangle_\Sigma \frac{\partial \alpha_m}{\partial t}
   :label: omega_rk_alpha

and also to other :math:`\dot \omega_{im}` reaction rates for each :math:`i` component which takes part in the :math:`m` reaction:

.. math::
   \dot \omega_{im} = \delta_{im} \frac{\nu_{im}}{\nu_{Rm}}  \dot \omega_{Rm}
   :label: dot_omega

:math:`\delta_{im}` sets the sign of the mass reaction rate :math:`\dot \omega_{im}` in equation :eq:`dot_omega`: :math:`\delta_{im}=+1` if :math:`i` is a reactant and :math:`\delta_{im}=-1` if :math:`i` is a product in the :math:`m` reaction. The total mass source term of the :math:`i` component (formation and destruction) is the sum of all reaction growth rates :math:`\dot \omega_{im}` where :math:`i` takes part in the set of :math:`M` reactions:

.. math::
   \dot \omega_{i} = \sum \limits_{m=1}^M \dot \omega_{im}
   :label: prod_gen

The total quantities of :math:`\mathcal{R}_m` generated, denoted :math:`\langle\rho_{\mathcal{R}_m}\rangle_\Sigma`, is expressed as:

.. math::
   \langle\rho_{\mathcal{R}_m}\rangle_\Sigma = \langle\rho_{\mathcal{R}_m}\rangle_0 + \int_0^t \dot \omega_{\mathcal{R}_m} (\tau) d\tau
   :label: def_rho_tot_m

This function is the sum of quantities at the initial state (using the subscript ":math:`0`") plus the sum of quantities generated during
decomposition. The formulation is based on the generalized pyrolysis model developed by Lautenberger et al. Another noticeable value is :math:`\dot Q_{\mathcal{R}}`, the total heat source produced or consumed by the whole :math:`M` reactions:

.. math::
   \dot Q_{\mathcal{R}} = \sum \limits_{m=1}^M  \dot \omega_{Rm} Q_m
   :label: ener_reac

where :math:`Q_m` is the heat of the :math:`m` reaction.

.. _mass_conservation_law:

Mass conservation laws
**********************

For each :math:`i` component, the local mass conservation law is
expressed as:

.. math::
   \frac{\partial \rho_i}{\partial t}  + \boldsymbol{\nabla} \cdot \left( \rho_i   \mathbf{V_i} \right)= 0
   :label: bilan_masse_local

where :math:`\mathbf{V_i}` is the local velocity of the :math:`i`
component. This equation is homogenized in the representative elementary
volume :math:`V` using volume averaging equations :eq:`def_moy_col_temporelle` and
:eq:`def_moy_col_divergent` as:

.. math::
   \frac{\partial }{\partial t} \langle \rho_i \rangle + \boldsymbol{\nabla} \cdot \left(   \langle \rho_i \rangle  \langle\mathbf{V_i} \rangle \right) = \frac{1}{V} \int_{A_{i}}  \rho_i  \mathbf{U_{i}} \cdot \mathbf{n_{i}} dA
   :label: bilan_masse_moy2

where :math:`\mathbf{U_{i}}` is the relative interface velocity such as
:math:`\mathbf{U_{i}} = \mathbf{W_{i}} -  \mathbf{V_{i}}` of the interface
between the :math:`i` component and other components, as shown on :numref:`volumeV_prelim`. Considering that the volume :math:`V` is
non-deformable, apparition of :math:`\mathbf{U_{i}}` velocity is due to
chemical transformations between the :math:`i` component and other
components as defined in thr chemical reactions section.

For each solid component, no transport phenomena are assumed and
absolute densities are constant, meaning
:math:`\langle\mathbf{V_i} \rangle = \mathbf{0}`. Moreover, the right hand
side term in equation :eq:`bilan_masse_moy2`
corresponds to the mass reaction term resulting from chemical reactions.
The mass conservation law of the :math:`i` solid component is
reformulated as:

.. math::
   \frac{\partial }{\partial t} \langle \rho_i \rangle = \dot \omega_i
   :label: bilan_masse_sol_2

Inside the gaseous phase, the average gas velocity is assumed to follow
a Darcy’s law. This Darcy’s velocity is denoted :math:`\mathbf{v_g}` and
is defined as:

.. math::
   \mathbf{v_g} = - \frac{\mathbf{K}_P}{\mu_g} \boldsymbol{\nabla}  \widehat{P}_g
   :label: eq_darcy

where :math:`\mathbf{K}_P` is the permeability tensor, :math:`\mu_g` the
dynamic viscosity. :math:`\widehat{P}_g` is the average gas pressure
inside the elementary volume :math:`V` and is driven by an ideal gas
law:

.. math::
   \widehat{P}_g = \widehat{ \rho}_g \frac{r_{g}}{M_g} \widehat{T}_g
   :label: loi_GP

where :math:`\widehat{T}_g` is the average gaseous phase temperature
inside :math:`V`.

The average gaseous phase velocity :math:`\langle \mathbf{V_g} \rangle`
can be calculated using equation
:eq:`def_psi_alpha_2`:

.. math::
   \langle \mathbf{V_g} \rangle = \frac{\mathbf{v_g}}{\varphi_g}
   :label: vitesse_gaz_moy

and the average velocity of each gaseous component :math:`j`
:math:`\langle \mathbf{V_j} \rangle` is assumed equal to
:math:`\langle \mathbf{V_g} \rangle`. The equation
:eq:`bilan_masse_moy2` applied to a gaseous component
:math:`j` can be reformulated as:

.. math::
   \frac{\partial }{\partial t} \langle \rho_j \rangle + \boldsymbol{\nabla} \cdot \left( \langle \rho_j \rangle \frac{\mathbf{v_g}}{\varphi_g}  \right) = \dot \omega_j
   :label: bilan_masse_gaz

In this relation, the right hand side term deals with the mass source
term resulting from decomposition gases production due to pyrolysis or
oxidation reactions. The second left hand side term results from the
convection process driven by the Darcy’s law.


.. _energy_conservation_law:

Energy conservation laws
************************

Kinetic energy and mechanical work phenomena are ignored in the energy
balance. For a single :math:`i` component, the energy conservation law
is expressed in term of sensible enthalpy :math:`h_i` as:

.. math::
   \frac{\partial \rho_i h_i}{\partial t}  + \boldsymbol{\nabla} \cdot \left( \rho_i h_i  \mathbf{V_i} \right) = - \boldsymbol{\nabla} \cdot \mathbf{q_i}
   :label: bilan_ener_local

where the right hand side term deals with conductive heat flux density.
The sensible enthalpy of each :math:`i` component is calculated as:

.. math::
   h_i = \int_{T_0}^T C_P(\tau) d\tau
   :label: def_enthalp

where :math:`T_0` is a reference temperature, taken at the initial
state. Similarly to mass conservation laws, the volume averaging of the
energy conservation law of a single component can be expressed as:

.. math::
     \frac{\partial \langle \rho_i h_i \rangle }{\partial t} +   \boldsymbol{\nabla} \cdot \left( \langle \rho_i h_i \rangle \langle \mathbf{V_i} \rangle \right)  = - \boldsymbol{\nabla} \cdot \langle \mathbf{q_i} \rangle
    \\
    - \underbrace{ \frac{1}{V} \int_{A_{i}}  \mathbf{q_{i}} \cdot \mathbf{n_{i}} dA }_{a} + \underbrace{ \frac{1}{V} \int_{A_{i}}  \rho_i h_i \mathbf{U_{i}} \cdot \mathbf{n_{i}} dA }_{b}
   :label: bilan_ener_moy

where the term :math:`a` represents conductive heat transfers and the
term :math:`b` represents enthalpy transport between the :math:`i`
component and other components inside the elementary volume :math:`V`.
The local thermal equilibrium is assumed in :math:`V` and allows to
consider:

.. math::
   \widehat{T}_i = \widehat{T} 
   :label: equ_therm_loc

for all components. This assumption can be used because heat transfer
characteristic times are much lower at the microscopic scale than at the
macroscopic scale. Using the local thermal equilibrium assumption, all
energy conservation laws are added up to form only one governing
energetic transfer equation:

.. math::
   \underbrace{  \frac{\partial }{\partial t} \sum_{i=1}^I  \langle \rho_i h_i \rangle  }_a +  \underbrace{ \boldsymbol{\nabla} \cdot  \sum_{i=1}^I \langle \rho_i h_i \rangle  \langle \mathbf{V_i} \rangle }_b= - \underbrace{ \boldsymbol{\nabla} \cdot \sum_{i=1}^I  \langle \mathbf{q_i} \rangle  }_c
   \\
    - \underbrace{ \sum_{i=1}^I \left( \frac{1}{V} \int_{A_{i}}  \mathbf{q_{i}} \cdot \mathbf{n_{i}} dA \right) }_d + \underbrace{ \sum_{i=1}^I \left( \frac{1}{V} \int_{A_{i}}  \rho_i h_i \mathbf{U_{i}} \cdot \mathbf{n_{i}} dA \right) }_e
   :label: bilan_ener_moy3

Total sensible enthalpy in the first term :math:`a` in equation
:eq:`bilan_ener_moy3` can be developed as follows:

.. math::
   \sum_{i=1}^I  \langle \rho_i h_i \rangle =   \sum_{i=1}^I  \langle \rho Y_i h_i \rangle = \langle \rho \rangle \sum_{i=1}^I Y_i  \langle  h_i \rangle =  \langle \rho \rangle   \widehat{h}
   :label: bilan_ener_dev1

where the average sensible enthalpy is introduced as:

.. math::
   \widehat{h} =  \sum_{i=1}^I  Y_i\langle h_i\rangle
   :label: bilan_ener_dev2

Thanks to the local thermal equilibrium assumption, :math:`\widehat{h}`
can be expressed as:

.. math::
   \widehat{h} =  \sum_{i=1}^I  Y_i \int_{\widehat{T}_0}^{\widehat{T}} C_{P_i}(\tau) d\tau =  \int_{\widehat{T}_0}^{\widehat{T}} C_{P}^{*}(\tau) d\tau
   :label: bilan_ener_dev3

where :math:`C_{P}^{*}` is the apparent heat capacity of the material,
calculated as:

.. math::
   C_{P}^{*} = \sum_{i=1}^I  Y_i  C_{P_i}
   :label: bilan_ener_dev4

In the second term :math:`b` of the equation
:eq:`bilan_ener_moy3`, average velocities
:math:`\langle \mathbf{V_i} \rangle` are equal to zero for all solid
components and equal to the average gas velocity for each gas component,
meaning that:

.. math::
   \sum_{i=1}^I \langle \rho_i h_i \rangle \langle \mathbf{V_i} \rangle = \sum_{j=1}^J \langle \rho_j h_j \rangle \frac{1}{\varphi_g} \mathbf{v_g}
   :label: bilan_ener_dev5

The latter expression can be simplified using the same relations as in
equation :eq:`bilan_ener_dev1`:

.. math::
   \sum_{i=1}^I \langle \rho_i h_i \rangle \langle \mathbf{V_i} \rangle =  \frac{\langle \rho_g \rangle \widehat{h_g}}{\varphi_g} \mathbf{v_g}
   :label: bilan_ener_dev6

The third term :math:`c` in the equation
:eq:`bilan_ener_moy3` deals with conductive heat
transfer of all components at the external interface of the elementary
volume :math:`V`. Classical homogenization theories have shown that if
local conductive transfer can be modeled using a Fourier’s law for all
components, that is to say:

.. math::
   \mathbf{q_i}  = - \mathbf{k_i} \boldsymbol{\nabla} T_i
   :label: fourier_class

then it exists an apparent thermal conductivity :math:`\mathbf{k}^{*}`
depending on volume fractions of all components and on microscopic
structure as:

.. math::
   \sum_{i=1}^I  \langle \mathbf{q_i} \rangle = - \mathbf{k}^{*} \boldsymbol{\nabla} \widehat{T}
   :label: fourier_moy

The term :math:`d` of the equation
:eq:`bilan_ener_moy3` expresses the sum of all
conductive transfers inside the elementary volume :math:`V`. This term
is developed using two components :math:`\alpha` and :math:`\beta`. The
expression of interface equilibrium between these two components
integrated on :math:`A_{\alpha \beta}` is:

.. math::
   \int_{A_{\alpha \beta}}  \mathbf{q_{\alpha}} \cdot \mathbf{n_{\alpha \beta}} dA + \int_{A_{\alpha \beta}}  \mathbf{q_{\beta}} \cdot \mathbf{n_{\beta \alpha}} dA = 0
   :label: int_cond_alpha_beta

The sum of all interface equilibrium of every component can be expressed
as the term :math:`d` of the equation
:eq:`bilan_ener_moy3`:

.. math::
   \sum_{i=1}^I \left( \frac{1}{V} \int_{A_{i}}  \mathbf{q_{i}} \cdot \mathbf{n_{i}} dA \right) = 0
   :label: int_cond_somme_i

The last term :math:`e` of the equation
:eq:`bilan_ener_moy3` deals with enthalpy transfers
inside the elementary volume :math:`V` at the relative velocity
:math:`\mathbf{U_i}`. According to Whitaker , the enthalpy variation
between two components :math:`\alpha` and :math:`\beta` due to an
heterogeneous reaction can be expressed as:

.. math::
   \int_{A_{\alpha \beta}} \left( \rho_{\alpha} h_{\alpha} - \rho_{\beta} h_{\beta} \right) \mathbf{U_{\alpha \beta}} \cdot \mathbf{n_{\alpha \beta}} dA = \dot \omega_{\alpha} h_{c{\alpha}} + \dot \omega_{\beta} h_{c{\beta}}
   :label: whithaker_enthalp_reac

where :math:`\dot \omega_{\alpha}` and :math:`\dot \omega_{\beta}` are
the mass source terms of :math:`\alpha` and :math:`\beta`. The mass
conservation at the interface :math:`A_{\alpha \beta}` implies
:math:`\dot \omega_{\alpha} = -  \dot \omega_{\beta}`. :math:`h_{c}`
values are the chemical enthalpies of associated components. In the
equation [eq:whithaker:sub:`e`\ nthalp\ :sub:`r`\ eac], chemical
enthalpies can be replaced by the equivalent heat source term as
:math:`Q_{\alpha \beta} =  h_{c{\alpha}} - h_{c{\beta}}` and as a
consequence:

.. math::
   \int_{A_{\alpha \beta}} \left( \rho_{\alpha} h_{\alpha} - \rho_{\beta} h_{\beta} \right) \mathbf{U_{\alpha \beta}} \cdot \mathbf{n_{\alpha \beta}} dA = \dot \omega_{\alpha} Q_{\alpha \beta}
   :label: whithaker_enthalp_reac2

:math:`Q_{\alpha \beta}` represents the resulting consumed enthalpy to
transform the component :math:`\alpha` into the component :math:`\beta`.
This relation can be extended to all :math:`I` components and all
:math:`M` reactions as:

.. math::
   \sum_{i=1}^I \left( \frac{1}{V} \int_{A_{i}}  \rho_i h_i \mathbf{U_{i}} \cdot \mathbf{n_{i}} dA \right) =  \sum_{m=1}^M \left(  \sum_{i=1}^I \dot \omega_{im} h_{c_{im}} \right)
   :label: somme_enthalp_reac

The heat of reaction :math:`Q_m` of the :math:`m` reaction defined in
equation :eq:`react_glob` can be introduced in equation
:eq:`somme_enthalp_reac` as:

.. math::
   \sum_{i=1}^I \dot \omega_{im} h_{c_{im}} = \dot \omega_{Rm} Q_m
   :label: somme_enthalp_reac2

where :math:`\dot \omega_{Rm}` is the mass reaction rate of the solid
reactant of the :math:`m` reaction as defined in th echemical reactions section. The total heat of reaction
:math:`\dot Q_{\mathcal{R}}` defined in equation :eq:`ener_reac`
appears in equation :eq:`somme_enthalp_reac` as:

.. math::
   \sum_{i=1}^I \left( \frac{1}{V} \int_{A_{i}}  \rho_i h_i \mathbf{U_{i}} \cdot \mathbf{n_{i}} dA \right) =  \dot Q_{\mathcal{R}}
   :label: somme_enthalp_reac3

Finally, the energy conservation law proposed in equation
:eq:`bilan_ener_moy3` can be reduced using relations
:eq:`bilan_ener_dev1`,
:eq:`bilan_ener_dev6`, :eq:`fourier_moy`, :eq:`int_cond_somme_i` and
:eq:`somme_enthalp_reac3` as:

.. math::
   \frac{\partial }{\partial t}  \langle \rho \rangle   \widehat{h} +  \boldsymbol{\nabla} \cdot \left( \frac{\langle \rho_g \rangle   \widehat{h_g}}{\varphi_g}  \mathbf{v_g} \right) =  \boldsymbol{\nabla} \cdot \left( \mathbf{k}^{*} \boldsymbol{\nabla} \widehat{T} \right) +  \dot Q_{\mathcal{R}}
   :label: bilan_ener_glob

This conservation law expresses the variation of total sensible enthalpy
submitted to gaseous phase enthalpy transport, conductive heat transfer
and heat of reaction production.




.. _mori-tanaka_homogenization:

Mori-Tanaka homogenization method
*********************************

To be filled
