# -*- coding: utf-8 -*-
#
# modethec documentation build configuration file, created by
# sphinx-quickstart on Fri Nov 16 16:43:43 2018.
#
# This file is execfile()d with the current directory set to its
# containing dir.
#
# Note that not all possible configuration values are present in this
# autogenerated file.
#
# All configuration values have a default; values that are commented out
# serve to show the default.

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
# #
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- General configuration ------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
#
needs_sphinx = '1.6.5'

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['sphinx.ext.imgmath']
# extensions = ['sphinx.ext.mathjax']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['templates']

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
#
# source_suffix = ['.rst', '.md']
source_suffix = '.rst'

# The master toctree document.
master_doc = 'index'

# General information about the project.
project = u'modethec'
copyright = u'2019, Valentin Biasi'
author = u'Valentin Biasi'

# The version info for the project you're documenting, acts as replacement for
# |version| and |release|, also used in various other places throughout the
# built documents.
#
# The short X.Y version.
version = u'2018.2.0'
# The full version, including alpha/beta/rc tags.
release = u'2018.2.0'

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#
# This is also used if you do content translation via gettext catalogs.
# Usually you set "language" from the command line for these cases.
language = None

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This patterns also effect to html_static_path and html_extra_path
exclude_patterns = ['build', 'Thumbs.db', '.DS_Store']

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'

# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = False


# -- Options for HTML output ----------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'


# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['static']

# Custom sidebar templates, must be a dictionary that maps document names
# to template names.
#
# This is required for the alabaster theme
# refs: http://alabaster.readthedocs.io/en/latest/installation.html#sidebars
html_sidebars = {
    '**': [
        'globaltoc.html',
        'relations.html',  # needs 'show_related': True theme option to display
        'searchbox.html',
    ]
}


# -- Options for HTMLHelp output ------------------------------------------

# Output file base name for HTML help builder.
htmlhelp_basename = 'modethecdoc'


# -- Options for LaTeX output ---------------------------------------------

latex_elements = {
    'papersize': 'a4paper',
    'releasename': " ",
    'fncychap': '\\usepackage{fncychap}',
    'fontpkg': '\\usepackage{helvet}',
    'geometry': '\\usepackage[body={170mm,258mm}, left=26mm, top=26mm, headheight=7mm, headsep=5mm, marginparsep=4mm, marginparwidth=21mm, footskip=10mm]{geometry}',
    'preamble': r'''
        \renewcommand{\familydefault}{phv}
        \setcounter{tocdepth}{2}

        \usepackage{parskip}            % Interlignes - Inter-paragraphes
        \setlength{\parskip}{0.99em plus 0.1em minus 0.3em}
        \setlength{\parindent}{25pt}

        \definecolor{bleuonera}{RGB}{0 86 170}	% bleu ONERA
        \colorlet{bleuonera2}{bleuonera!80!black}		% bleu ONERA 2
        \colorlet{bleuonera3}{bleuonera!30!black}		% bleu ONERA 3
        \colorlet{bleuencadre}{bleuonera2!20!white!93!black}		% bleu ENCADRE

        \usepackage{tikz}

        \usepackage{minitoc}
        \setcounter{minitocdepth}{1}				% jusquaux sous sections 
        \mtcsetfont{minitoc}{subsection}{\small\sffamily\upshape}
        \mtcsetfont{minitoc}{section}{\small\sffamily\upshape\bfseries}
        \mtcsettitlefont{minitoc}{\small\sffamily\upshape\bfseries}
        \mtcsetformat{minitoc}{tocrightmargin}{0.7cm}
        \mtcsetformat{minitoc}{pagenumwidth}{0.5cm}
        \mtcsettitle{minitoc}{}
        \mtcsetrules{minitoc}{off}
        \newcommand{\titlebox}[2]{%
            \tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw]%
            \tikzstyle{title}=[fill=white]%
            %
            %\bigskip
            \noindent\begin{tikzpicture}
            \node[titlebox] (box){%
                    %\begin{minipage}{0.94\textwidth}
                    #2
                    %\end{minipage}
            };
            \draw (box.north west)--(box.north east);
            \node[title,right=-13.7em] at (box.north east) {\large{\textsc{#1}}};
            \end{tikzpicture}\bigskip%
            }
        \newcommand{\sphinxminitoc}{
        \noindent
        \hfill
        \titlebox{Table of contents}{
        \hspace{-0.05\linewidth}
        \begin{minipage}{0.7\linewidth}
        \vspace{-0.0cm}

        \minitoc
        \vspace{-0.5cm}
        \end{minipage}
        \hspace{-0.04\linewidth}
        }}

        \renewcommand{\headrule}{{\color{black!60} \hrule width\headwidth height\headrulewidth \vskip-\headrulewidth}}
        \fancypagestyle{normal}{
            \fancyhf{}
            
            \fancyhead[LE,RO]{\color{black!60}{\thepage}} % ... (E gauche - O droite)
            \fancyhead[RE]{\color{black!60}{\slshape \leftmark}}	% ...
            \fancyhead[LO]{\color{black!60}{\slshape \rightmark}}	% ...
            \fancyfoot[CO, CE]{}

            \renewcommand{\headrulewidth}{0.5pt}
            \renewcommand{\footrulewidth}{0pt}
        }
        \fancypagestyle{plain}{
            \fancyhf{}
            \fancyfoot[LE,RO]{}
            \renewcommand{\headrulewidth}{0pt}
            \renewcommand{\footrulewidth}{0pt}
        }


        \makeatletter
        \renewcommand\thechapter{\@Roman\c@chapter}
        \renewcommand\thesection{\@arabic\c@section}
        \makeatother

        \makeatletter
        \renewcommand{\section}{\@startsection {section}{1}{\z@}%
            {-3.5ex \@plus -1ex \@minus -.2ex}%
            {1.3ex \@plus.2ex}%
            {\reset@font\LARGE\bfseries\color{bleuonera!50!black}}}
        \renewcommand{\subsection}{\@startsection{subsection}{2}{\z@}%
                    {-3.25ex\@plus -1ex \@minus -.2ex}%
                    {0.1ex \@plus .2ex}%
                    {\normalfont\Large\bfseries\color{bleuonera!65!black}}}
        \renewcommand{\subsubsection}{\@startsection{subsubsection}{3}%
                {\z@}%
                {-2.25ex\@plus -1ex \@minus -.2ex}%
                    {0.1ex \@plus .2ex}%
                    {\normalfont\large\bfseries\color{bleuonera!80!black}}}
        \makeatother

        \makeatletter
        \renewcommand{\@makechapterhead}[1]{%
        \vspace*{50 pt}%
        {\setlength{\parindent}{0pt} \raggedright
            \resizebox{!}{1.9cm}{
                \raisebox{-15.0pt}
                {\rotatebox{90}{
                    {\textrm{\textsc {\@chapapp{}}}}
                }}}
            \hspace{0em}
                \vspace{1em}
            \resizebox{2cm}{1.8cm}{\fboxsep=8pt
                \colorbox{bleuonera3}{
                    \color{white}{\normalfont\Huge\bfseries\textrm\thechapter}
            }}%
        \\
        \color{bleuonera3}\normalfont\bfseries\huge\flushright\textrm{#1}
        \par\nobreak\vspace{35 pt}}}
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        \renewcommand{\@makeschapterhead}[1]{%
        \vspace*{114 pt}%
        {\setlength{\parindent}{0pt} \raggedright
        \color{bleuonera3}\bfseries\huge\flushright\textrm{#1}
        \par\nobreak\vspace{35 pt}}}
        \makeatother
        
    ''',


    'maketitle': r'''
        \dominitoc
        \pagenumbering{Roman} %%% to avoid page 1 conflict with actual page 1

        \begin{titlepage}
            \centering

            \vspace*{30mm} %%% * is used to give space from top
                        
            \begin{figure}[!h]
                \centering
                \includegraphics[scale=1.0]{logo_modethec.png}
            \end{figure}
            \vspace{10mm}

            \textbf{\Huge {Modethec documentation}}

            \vspace{0mm}
            %\Large \textbf{{Valentin Biasi}}

            \small Updated on: \today

            \vspace*{0mm}
            %\small  Last updated : \MonthYearFormat\today

            %% \vfill adds at the bottom
            \vfill

            \begin{figure}[!h]
                \centering
                \includegraphics[scale=1.0]{../../img/ONERA.jpg}
            \end{figure}
            \vspace{10mm}


            \small \textit{More documents are freely available at }{\href{http://www.onera.index.html}{onera.fr/modethec}}
        \end{titlepage}

        \cleardoublepage 
    ''',

    'sphinxsetup':
        'OuterLinkColor={rgb}{0,0.45,0}, \
        InnerLinkColor={rgb}{0,0.3,0.6}',
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
    (master_doc, 'modethec.tex', u'Modethec documentation',
     u'Valentin Biasi', 'manual'),
]


# -- Options for manual page output ---------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    (master_doc, 'modethec', u'Modethec documentation',
     [author], 1)
]


# -- Options for Texinfo output -------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
    (master_doc, 'modethec', u'Modethec documentation',
     author, 'modethec', 'One line description of project.',
     'Miscellaneous'),
]

# math_number_all = True
numfig = True
numfig_format = {'figure': 'Fig. %s ', 'table': 'Table %s ',
                 'code-block': 'Listing %s ', 'section': 'Section %s '}


html_logo = "img/logo_modethec_128.png"
html_show_sourcelink = False


def setup(app):
    app.add_stylesheet('custom.css')