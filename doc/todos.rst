Whats' next?
############



.. only:: html


    **Aux actuels et futurs développeurs de modethec :**

    *J'ai commencé à développer ce logiciel en 2012 durant ma première année de thèse à l'ONERA. Au départ, ce devait être un petit prototype Matlab destiné à valider des modèles simples de dégradation des matériaux composites en 1D et qui puisse servir de support pour une intégration dans CEDRE. Pour diverses raisons, cette intégration ne s'est jamais faite mais j'ai poursuivi le développement de ce prototype et l'ai fait tendre vers quelque chose de plus évolutif, structuré et performant.*
    
    *Brique par brique, cet outil a été amélioré pour en faire ce qu'il est aujourd'hui. Quand je vois les possibilités que peut offrir ce logiciel, je ne peux qu'être pleinement satisfait du travail accompli jusqu'à aujourd'hui. Mais je me dis surtout que c'est principalement parce que nous avons cherché à aller plus loin que les besoins initiaux que ce projet a pu se péréniser, car dans ce monde un peu particulier, il faut réussir à avoir des coups d'avance pour s'imposer dans de nouveaux projets et trouver de nouveaux financements.*

    *Enfin, je me veux enthousiaste pour le futur de Modethec, pour lequel j'ai consacré tant de temps et d'efforts. J'espère que vous saurez le faire évoluer vers de nouvelles applications encore plus incroyables et si possible en gardant sa simplicité et son accessibilité. Mais je n'ai aucune inquiétude car je m'apperçoit déjà de la profusion d'idées et de la grande motivation que vous portez pour ce logiciel.*

    *Merci à tous et au plaisir de vous revoir bientôt,*
    
    *Valentin Biasi*
    
    .. image:: img/modethec_gui/giphy.*
        :width: 80%
        :align: center


Priority developments
+++++++++++++++++++++


* Méthode d'interpolation ordre élevé (pour les valeurs aux limites)

* Conditions d'interface sur du multi-domaines

* Orthotropie locale du matériau (matrice de rotation et repères locaux)

* Couplage avec CHARME (type 11 paroi débitante - problème avec Qparoi, type 14) + paroi mobile

* Couplage avec Zset (couplage volumique - échange température - déplacement)

* OpenMP

* Méthode inverse



Miscellaneous
+++++++++++++

* Diffusion massique dans matériau (loi de Fick)

* Termes visqueux de transport en milieux proreux (Klinkenberg)

* Conductivité thermique radiative (mileux semi-transparents) k_rad ~ T**3

* Déséquilibre thermique local (1 équation par phase)

* Echanges radiatifs entre parois (ou équivalent de k_rad)



Bugs
++++

* Plusieurs éléments 3D dans le même domaines

* Problème avec Qparoi dans type 11 pour couplage CHARME

* Amélioration de la robustesse du solveur advection (pistes : amélioration de la jacobienne, pas de temps local)