Git usage
#########

``git``  is a distributed version-control system for tracking changes in source code during software development. Modethec is versioned with ``git`` to coordinate work among programmers.


Compared to other version-control system such as ``svn``, every Git directory on every computer is a full-fledged repository with complete history and full version-tracking abilities, independent of network access or a central server. Therefore, the workflow in ``git`` is more complex to apprehend as there is an intermediate local repository between the working directory and the remote repository.


.. _git_area:
.. figure:: img/git_area.png
    :width: 70%
    :align: center

    Overview of a workflow management in ``git``

The way Modethec is versioned is similar to the standard Git usage. Git documentations are abundant on the internet. Here are some relevant tutorials:

* https://www.atlassian.com/git/tutorials
* http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf
* http://fr.openclassrooms.com/informatique/cours/gerez-vos-codes-source-avec-git


In order to have durable and clear Git history if the Modethec repository, your Git configuration file ``.gitconfig`` (saved in your root directory) should be configured as follow:

.. code-block:: guess
    :linenos:

    [user]
        name = Your Name
        email = youremailaddress@onera.fr
    [color]
        ui = true
    [pull]
        rebase = true
    [gc]
        autoDetach = false


It sets your name and email to your future commits, colored prints, rebase the repository before committing and remove unnecessary files. The rebase option allows to keep a linear history when several programmers are working on the same branch.

A ``.gitignore`` file is saved in the root directory of the repository. It sets all the files that are not automatically added to the version-control system. If an ignored file needs to be versioned, the following command forces Git to add it to its file list:

::

    git add -f filename

.. note:: We recommend to use a Git user interface to efficiently manage the Git repository. Visual Studio Code (with the appropriate extensions: Git History and GitLens) is an excellent candidate. You are free to use Smartgit or any decent Git user interface or none as long as the project is properly maintained.


Program structure
#################

In all high-level subroutines of Modethec, the derived type structure ``DOM`` is the main argument. This data structure contains about everything necessary to the resolution of the system: all parameters extracted from the ``.prm`` file, all fields, fluxes, Jacobian matrices, ... The figure :numref:`struct-donnees` shows the main sub- derived types of this structure.

.. _struct-donnees:
.. figure:: img/struct-donnees.png
    :width: 90%
    :align: center

    ``DOM`` derived type overview

Allocatable derived types are used to define values per species, reactions or boundaries. Derived types ``ETAT`` and ``RSL`` are the only structures that tend to be modified during a standard computation (without external coupling or ablation).

.. note:: By default in Fortran, arguments are passed by reference. It means that the ``DOM`` data structure is not copied when it is passed as argument to another subroutine. It is important because otherwise this large data structure would be copied at each subroutine call.


.. _struct-sources:
.. figure:: img/struct-sources.png
    :width: 60%
    :align: center

    ``SOURCE/`` directory overview

All source files of Modethec are stored in the ``SOURCE/`` directory. The :numref:`struct-sources` shows the content of this directory. Source files are sorted as follow:

* **0_TYPES/**: Source files of derived types definition

* **1_LIBS/**: Source files of low-level subroutines (linear algebra, interpolation functions, ...)

* **2_MESH/**: Source files of the mesh definition. It is the first step of the Modethec solver.

    * **2-1_READ/**: Reading of the mesh file.

    * **2-2_BUILD/**: Building of mesh connectivity.

    * **2-3_COMPUTE/**: Computation of mesh properties (volumes, surfaces, interpolation matrices, ...).

* **3_DATA/**: Source files of the parameters preparation. It is the second step of the Modethec solver.

    * **3-1_READ/**: Reading of the parameter file.

    * **3-2_BUILD/**: Preparation of the data structure (allocations, checks, ...).

    * **3-3_INIT/**: Initialization of the domain.

* **4_CYCLE/**: Source files of the time integration solver. It is the third and main step of the Modethec solver.

    * **4-1_PREPARE/**: Preparation of the solver for each time step (properties, reactions rates, surface recession computations).

    * **4-2_RHS/**: Computation of the right hand side terms of all conservation laws.

    * **4-3_SOLVE/**: System resolution at the current time step for each solver.

    * **4-4_UPDATE/**: Updating the derived values (temperatures, volume fractions, ...) from integral values (mass, internal energy).

    * **4-5_JAC/**: Computation of the Jacobian matrices.

* **5_EXPORT/**: Writing of the output files.


Coding style guide
##################

We try to use coding conventions to make future developments easier. The coding convention of Modethec is largely inspired of the PEP8 convention, but suited for modern Fortran.

.. _code_quality:
.. figure:: img/code_quality.png
    :width: 100%
    :align: center

    Readalibity is for everyone, starting with you

Recommendations
---------------

Indentation
...........

Use 4 spaces per indentation level and try to avoid tabs. Continuation lines should align wrapped elements vertically using implicit line joining inside parentheses or brackets.

Maximum Line Length
...................

Limit all lines to a maximum of 79 characters.

Line Break
..........

The recommended style is to break lines after binary operators or just after a comma.

Source File Encoding
....................

Please use only UTF-8, even if you edit files on Windows.

Blank Lines
...........

Blank lines increases the readability of the source code. Use blank lines to separate functions and subroutines. Use blank lines in functions and subroutines, sparingly, to indicate logical sections. A block of more that 5-6 code lines should be separated by blank lines.

Imports
.......
Imports should usually be on separate lines. For each import, the list of used variables or subroutines should be explicitly listed, as:

.. code-block:: fortran
    :linenos:

    USE mod_cst, ONLY : DP, IP, eps_min

Imports should be done at the beginning of modules and not inside subroutines.

String Quotes
.............
Double-quoted strings are preferred to single-quoted strings. Readability is better when strings contain apostrophes.

Whitespace in Expressions and Statements
........................................
Whitespaces should be placed between operators or after a comma. Whitespaces before or after parentheses are usually not recommended but can be used if it increases the readability. Avoid to use more than one space around an assignment (or other) operator to align it with another. Avoid to use whitespaces immediately before the open parenthesis that starts the argument list of a subroutine.

If operators with different priorities are used, consider adding whitespace around the operators with the lowest priority(ies). Use your own judgment; however, never use more than one space, and always have the same amount of whitespace on both sides of a binary operator.

Yes:

.. code-block:: fortran
    :linenos:

    i = i + 1
    x = x*2 - 1
    hypot2 = x*x + y*y
    c = (a+b) * (a-b)

No:

.. code-block:: fortran
    :linenos:

    i=i+1
    x = x * 2 - 1
    hypot2 = x * x + y * y
    c = (a + b) * (a - b)

Comments
........
Comments that contradict the code are worse than no comments. Always make a priority of keeping the comments up-to-date when the code changes.

Inline comments should be used sparingly. It should not be obvious but give real information for the programmers (variable definition, operation explanation, ...)

Line comments should be used to describe subroutines definitions, loops, block operations. The source code has to be as clear as possible. It is generally said that one third of the code should be comments.


Naming Conventions
..................

Scalar values, strings and integers names should be in lowercase. Vector names should begin with a uppercase. Arrays names should be in uppercase. The use of underscores is encouraged to separate words.

Subroutines should normally be in lowercase and derived types should be in uppercase. Fortran keywords should be in uppercase.


Use of modules
..............

We do not recommend to create subroutines or functions outside of a module. A procedure declared inside a module has an explicit interface created automatically. Several checks are made (arguments consistency, shape, ...) with explicit interfaces that tends to avoid some basic errors that could remain undetected with implicit interfaces.

Module template
---------------

The following code block is an empty template of a subroutine declared inside a module of the Modethec code:

.. code-block:: fortran
    :linenos:

    !> Module descritpion
    MODULE mod_template
    !
    USE mod_cst, ONLY : IP, DP, eps_min
    USE mod_print, ONLY : print_err
    !
    USE mod_dom
    !
    IMPLICIT NONE
    CONTAINS
    !
    !----------------------------------------------------------------------!
    !> Subroutine description
    SUBROUTINE some_routine(DOM)
    !
    !---- Arguments -------------------------------------------------------!
    TYPE (typ_dom) :: DOM
    !
    !---- Delarations -----------------------------------------------------!
    INTEGER(IP) :: ncel
    !
    !---- Definitions -----------------------------------------------------!
    ncel = DOM%MLG%ncel

        ! Call to ...
        CALL something_else( DOM )

    END SUBROUTINE some_routine
    !----------------------------------------------------------------------!
    !
    !---- Module end ------------------------------------------------------!
    END MODULE mod_template


External libraries
###################

Modethec uses different external libraries such as HDF5 or CWIPI. A compiled version these libraries are stored inside the Git repository in the ``hdf5/`` and the ``cwipi/`` directories.

If for some reasons, these libraries have to be updated, compiled with a new compiler, etc., a new compilation becomes necessary. The following procedure details the compilation steps of these libraries.


.. note:: If the installation folders of external libraries needs to be changed, the cmake configuration file ``CMakeLists.txt`` needs to be updated with the appropriate library paths in the ``link_directories`` and ``include_directories`` directives.


CWIPI compilation
-----------------

* Download the CWIPI sources in their latest version on the page https://w3.onera.fr/cwipi/telechargement
* Unzip and move into the desired folder
* Start the configuration command:

::

    ./configure --prefix=<INSTALL_FOLDER> CC=mpicc CXX=mpicxx FC=mpif90 --with-pic

with ``<INSTALL_FOLDER>`` the desired intallation folder (typically ``~/modethec/cwipi``). 

* Then run ``make`` and ``make install``
* Check the compilation of CWIPI: ``make check``

HDF5 compilation
----------------

* Download HDF5 sources in their latest version at https://support.hdfgroup.org/HDF5/release/obtainsrc518.html#conf
* Unzip and move into the desired folder
* Start the configuration command:

::

    ./configure --prefix=<INSTALL_FOLDER>  CC=mpicc CXX=mpicxx FC=mpif90 --with-pic --enable-fortran

with ``<INSTALL_FOLDER>`` the desired intallation folder (typically ``~/modethec/hdf5``). 

* Then ``make`` and ``make install``
