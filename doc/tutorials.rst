Tutorials
#########

All tutorials are placed in the ``tutos/`` directory. They can be launched with the user interface or directly with the ``regression.py`` validation script. More information about the ``regression.py`` can be found in the :ref:`validation_script` section.

Output format is by default tecplot but users can easily modify the format to their needs.

.. include:: ../tutos/1_trapeze/readme.rst

.. include:: ../tutos/2_bouteille/readme.rst

.. include:: ../tutos/3_percage/readme.rst

.. include:: ../tutos/4_henderson/readme.rst

.. include:: ../tutos/5_nozzle/readme.rst

.. include:: ../tutos/6_sphere/readme.rst

.. include:: ../tutos/7_stringer/readme.rst

.. include:: ../tutos/8_cwipi_2D/readme.rst

.. include:: ../tutos/9_cwipi_3D/readme.rst

.. include:: ../tutos/13_multidom_2D/readme.rst

.. include:: ../tutos/14_ortho_multilayer/readme.rst