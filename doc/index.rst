﻿**********************
Modethec documentation
**********************

************
Introduction
************

.. image:: img/logo_modethec.png
   :width: 400px
   :scale: 100 %
   :align: center


MoDeTheC software is a high-performance solver designed to model heat and mass transfer problems in materials. Its general formulation allows to handle various geometries and multi-species material definition. Different physical phenomena can be modeled, such as:

* Heat diffusion 
* Gas transport 
* Chemical reactions 
* Mechanical stress and deformation 
* Ablation 


Contributors
************

MoDeTheC (from the French Modèle de Dégradation Thermique des Composites) has been developed on the initiative of `Valentin Biasi <valentin.biasi@gmail.com>`_ during his PhD thesis on `Thermal modeling of decomposing composite materials submitted to fire <https://depozit.isae.fr/theses/2014/2014_Biasi_Valentin_D.pdf>`_ from 2011 to 2014.

Since then, it was enriched with many great features, thanks to its contributors:

* Gillian Leplat (gillian.leplat@onera.fr)
* Nicolas Perron (nicolas.perron@onera.fr)
* Ysolde Prévereaud (ysolde.prevereaud@onera.fr)
* Bertrand Kirsch
* Jean-Baptiste Clément
* Yann Marchenay


License agreement
*****************

..
    Acknowledgement
    ---------------

    Carefully read the following terms and conditions before using the Modethec software. By using this freeware version you acknowledge that you have read this limited warranty, understand it, and agree to be bound by its terms and conditions. You also agree that unless you have a different license agreement signed by ONERA, your use of this software indicates your acceptance of this license agreement and warranty. If you do not agree to the terms of this agreement, delete the software from all storage media.

    License
    -------

    This Freeware License Agreement (the "Agreement") is a legal agreement between you ("Licensee"), the end-user, and ONERA for the use of the Modethec software ("Software"). Commercial as well as non-commercial use is allowed. By using this Software or storing this program or parts of it on a computer hard drive (or other media), you are agreeing to be bound by the terms of this Agreement. Provided that you verify that you are handling the original freeware version you are hereby licensed to make as many copies of the Freeware version of this Software and documentation as long as you do not charge money or request donations for such copies. You may not alter this Software in any way. You may not modify, rent or resell for profit this Software, or create derivative works based upon this Software.

    Governing Law
    -------------

    This agreement shall be governed by the laws of France. If any portion of this Agreement is deemed unenforceable by a court of competent jurisdiction, it shall not affect the forcibility of the other portions of this Agreement.

    Limited Warranty and Disclaimer of Warranty
    -------------------------------------------

    ONERA expressly disclaims any warranty for the software. This software and the accompanying files are provided "as is" and without warranties as to performance of merchantability or any other warranties whether expressed or implied, or noninfringement. This software is not fault tolerant and should not be used in any environment which requires this. No liability for damages.
    In no event shall ONERA or its suppliers be liable to you for any consequential, incidental or indirect damages whatsoever (including, without limitation, damages for loss of business profits, business interruption, loss of business information, or any other pecuniary loss) arising out of the use of or inability to use this software even if ONERA has been advised of the possibility of such damages. The entire risk arising out of use or performance of the software remains with you.


Copyright
---------

Copyright |copy| by ONERA. All rights reserved.

.. |copy| unicode:: 0xA9 .. copyright sign


**********************
User documentation
**********************

.. raw:: latex

    \sphinxminitoc

.. toctree::
    :maxdepth: 1
    :caption: USER DOCUMENTATION

    install
    interface
    meshing
    general_settings
    solver_settings
    specific_settings
    tutorials


**********************
Technical references
**********************

.. raw:: latex

    \sphinxminitoc

.. toctree::
    :maxdepth: 1
    :caption: TECHNICAL REFERENCES

    thermo-chemical_model
    thermo-mechanical_model
    ablation_model
    numerical_methods
    external_coupling


**********************
Developper guide
**********************

.. raw:: latex

    \sphinxminitoc

.. toctree::
    :maxdepth: 1
    :caption: DEVELOPPER GUIDE
    
    developper_guide
    todos


.. raw:: latex

    \listoffigures
    %\listoftables





.. [HEN1987] Henderson, J.B., and Wiecek, T.E. (1987). *A Mathematical Model to Predict the Thermal Response of Decomposing, Expanding Polymer Composites.* Journal of Composite Materials 21, 373–393.
.. [WIT1986] Whitaker, S. (1986). *Flow in porous media I: A theoretical derivation of Darcy's law.* Transport in Porous Media 1, 3-25.
.. [LUO2012] Luo, C., Lua, J., Desjardin, P., (2012). *Thermo-mechanical damage modeling of polymer matrix sandwich composites in fire.* Composites Part A: Applied Science and Manufacturing 43, 814-821
