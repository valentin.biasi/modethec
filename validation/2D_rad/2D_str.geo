// MAILLAGE D'UN RECTANGLE 2D AXI EN QUAD


dr=0.5; // DIMENSION EN R
r_in = 0.253; // DIMENSION EN HEXA IN
nr1=31; // NOMBRE DE SOMMETS EN R_IN
nr2=21; // NOMBRE DE SOMMETS EN R_OUT

cos45 = 0.707106;
sin45 = 0.707106;

d_spline = 0.02;
d_center = 0.0;


// POINTS
Point(1) = {0, 0, 0, 1};
Point(2) = {dr, 0, 0, 1};
Point(3) = {0, dr, 0, 1};

Point(5) = {r_in+d_spline, 0, 0, 1};
Point(6) = {0, r_in+d_spline, 0, 1};
Point(10) = {r_in, r_in, 0, 1};

Point(12) = {cos45*dr, cos45*dr, 0, 1};
Point(15) = {r_in/2, r_in+d_spline, 0, 1};
Point(18) = {r_in+d_spline, r_in/2, 0, 1};


// LIGNES DROITES
Line(1) = {1, 5};
Line(2) = {5, 2};
Line(3) = {1, 6};
Line(4) = {6, 3};

Line(7) = {10, 12};

Circle(10) = {2, 1, 12};
Circle(11) = {12, 1, 3};

BSpline(21) = {10, 15, 6};
BSpline(20) = {5, 18, 10};

// SURFACES
Line Loop(41) = {3, -21, -20, -1};
Plane Surface(42) = {41};

Line Loop(43) = {4, -11, -7, 21};
Plane Surface(44) = {43};

Line Loop(45) = {7, -10, -2, 20};
Plane Surface(46) = {45};


// RAFFINEMENT DES LIGNES
Transfinite Line {1,3,20,21,10,11} = nr1 Using Progression 1;
Transfinite Line {2,4,7} = nr2 Using Progression 1;


Transfinite Surface {42,44,46};


// ACTIVE NO LIGNE ET POINTS
Geometry.LineNumbers = 1;
Geometry.PointNumbers = 1;
General.Axes = 1;

// ALGO DE MAILLAGE QUAD
Mesh.Algorithm = 8;
Mesh.Algorithm3D = 9;
Recombine Surface "*";


// NOM DES LIGNES ET SURFACES
Physical Line("OUT") = {10,11};
Physical Line("IN") = {1,2,3,4};
Physical Surface("INTERIOR") = {42,44,46};

