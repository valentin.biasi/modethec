// MAILLAGE D'UN RECTANGLE 2D AXI EN QUAD


dr=0.5; // DIMENSION EN R
r_in = 0.253; // DIMENSION EN HEXA IN
nr1=16; // NOMBRE DE SOMMETS EN R_IN
nr2=13; // NOMBRE DE SOMMETS EN R_OUT

cos45 = 0.707106;
sin45 = 0.707106;

d_spline = 0.02;
d_center = 0.02;


// POINTS
Point(1) = {0, 0, 0, 1};
Point(2) = {dr, 0, 0, 1};
Point(3) = {0, dr, 0, 1};
Point(4) = {0, 0, dr, 1};

Point(5) = {r_in+d_spline, 0, 0, 1};
Point(6) = {0, r_in+d_spline, 0, 1};
Point(7) = {0, 0, r_in+d_spline, 1};

Point(8) = {0, r_in, r_in, 1};
Point(9) = {r_in, 0, r_in, 1};
Point(10) = {r_in, r_in, 0, 1};
Point(11) = {r_in-d_center, r_in-d_center, r_in-d_center, 1};


Point(12) = {cos45*dr, cos45*dr, 0, 1};
Point(13) = {cos45*dr, 0, cos45*dr, 1};
Point(14) = {0, cos45*dr, cos45*dr, 1};

Point(15) = {r_in/2, r_in+d_spline, 0, 1};
Point(16) = {r_in/2, 0, r_in+d_spline, 1};
Point(17) = {0, r_in/2, r_in+d_spline, 1};
Point(18) = {r_in+d_spline, r_in/2, 0, 1};
Point(19) = {r_in+d_spline, 0, r_in/2, 1};
Point(20) = {0, r_in+d_spline, r_in/2, 1};

Point(21) = {r_in, r_in, r_in/2, 1};
Point(22) = {r_in, r_in/2, r_in, 1};
Point(23) = {r_in/2, r_in, r_in, 1};

Point(24) = {0, dr, 0, 1};
Rotate {{1, 0, -1}, {0, 0, 0}, Pi*3/10} {
  Point{24};
}



// LIGNES DROITES
Line(1) = {1, 5};
Line(2) = {5, 2};
Line(3) = {1, 6};
Line(4) = {6, 3};
Line(5) = {1, 7};
Line(6) = {7, 4};


Line(7) = {10, 12};
Line(8) = {8, 14};
Line(9) = {9, 13};
Circle(10) = {2, 1, 12};
Circle(11) = {12, 1, 3};
Circle(12) = {3, 1, 14};
Circle(13) = {14, 1, 4};
Circle(14) = {4, 1, 13};
Circle(15) = {13, 1, 2};


BSpline(16) = {6, 20, 8};
BSpline(17) = {8, 17, 7};
BSpline(18) = {7, 16, 9};
BSpline(19) = {9, 19, 5};
BSpline(20) = {5, 18, 10};
BSpline(21) = {10, 15, 6};


BSpline(22) = {8, 23, 11};
BSpline(23) = {10, 21, 11};
BSpline(24) = {9, 22, 11};

Circle(25) = {12, 1, 24};
Circle(26) = {13, 1, 24};
Circle(27) = {24, 1, 14};
Line(28) = {24, 11};


// SURFACES
Line Loop(29) = {16, 17, -5, 3};
Plane Surface(30) = {29};
Line Loop(31) = {8, -12, -4, 16};
Plane Surface(32) = {31};
Line Loop(33) = {13, -6, -17, 8};
Plane Surface(34) = {33};
Line Loop(35) = {14, -9, -18, 6};
Plane Surface(36) = {35};
Line Loop(37) = {15, -2, -19, 9};
Plane Surface(38) = {37};
Line Loop(39) = {18, 19, -1, 5};
Plane Surface(40) = {39};
Line Loop(41) = {3, -21, -20, -1};
Plane Surface(42) = {41};
Line Loop(43) = {4, -11, -7, 21};
Plane Surface(44) = {43};
Line Loop(45) = {7, -10, -2, 20};
Plane Surface(46) = {45};
Line Loop(47) = {20, 23, -24, 19};
Ruled Surface(48) = {47};
Line Loop(49) = {23, -22, -16, -21};
Ruled Surface(50) = {49};
Line Loop(51) = {22, -24, -18, -17};
Ruled Surface(52) = {51};
Line Loop(53) = {24, -28, -26, -9};
Plane Surface(54) = {53};
Line Loop(55) = {7, 25, 28, -23};
Plane Surface(56) = {55};
Line Loop(57) = {8, -27, 28, -22};
Plane Surface(58) = {57};
Line Loop(59) = {27, 13, 14, 26};
Ruled Surface(60) = {59};
Line Loop(61) = {10, 25, -26, 15};
Ruled Surface(62) = {61};
Line Loop(63) = {11, 12, -27, -25};
Ruled Surface(64) = {63};


// VOLUME
Surface Loop(65) = {60, 34, 36, 52, 54, 58};
Volume(66) = {65};
Surface Loop(67) = {50, 64, 44, 32, 58, 56};
Volume(68) = {67};
Surface Loop(69) = {46, 62, 38, 56, 48, 54};
Volume(70) = {69};
Surface Loop(71) = {40, 42, 30, 48, 52, 50};
Volume(72) = {71};


// RAFFINEMENT DES LIGNES
Transfinite Line {1,3,20,21,5,19,23,16,17,18,22,24,12,13,27,25,26,11,14,15,10} = nr1 Using Progression 1;
Transfinite Line {2,4,7,6,8,9,28} = nr2 Using Progression 1;

Transfinite Surface {30,32,34,36,38,40,42,44,46,48,50,52,54,56,58,60,62,64};
Transfinite Volume {66,68,70,72};



// NOM DES LIGNES ET SURFACES
Physical Surface("OUT") = {64,60,62};
Physical Surface("IN") = {40,36,38,42,44,46,30,32,34};
Physical Volume("INTERIOR") = {66,68,70,72};

// ACTIVE NO LIGNE ET POINTS
Geometry.LineNumbers = 1;
Geometry.PointNumbers = 1;
General.Axes = 1;

// ALGO DE MAILLAGE QUAD
Mesh.Algorithm = 8;
Mesh.Algorithm3D = 9;
Recombine Surface "*";

