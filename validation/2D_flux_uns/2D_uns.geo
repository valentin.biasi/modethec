// MAILLAGE D'UN RECTANGLE 2D AXI EN QUAD


dr=0.5; // DIMENSION EN R
nr=42; // NOMBRE DE SOMMETS EN R

// POINTS
Point(1) = {0, 0, 0, 1};
Point(2) = {dr, 0, 0, 1};
Point(3) = {0, dr, 0, 1};

// LIGNES DROITES
Line(1) = {1, 2};
Line(2) = {1, 3};

Circle(5) = {3, 1, 2};

// SURFACES

Line Loop(9) = {1, -5, -2};
Plane Surface(10) = {9};


// RAFFINEMENT DES LIGNES
Transfinite Line {1} = nr Using Progression 1;
Transfinite Line {2} = nr Using Progression 1;
Transfinite Line {5} = nr*3.14/2 Using Progression 1;

// NOM DES LIGNES ET SURFACES
Physical Line("OUT") = {5};
Physical Line("IN") = {1,2};
Physical Surface("INTERIOR") = {10};

// ACTIVE NO LIGNE ET POINTS
Geometry.LineNumbers = 1;
Geometry.PointNumbers = 1;
General.Axes = 1;

// ALGO DE MAILLAGE QUAD
Mesh.Algorithm = 6;
Mesh.Algorithm3D = 9;Mesh.Smoothing = 100;
