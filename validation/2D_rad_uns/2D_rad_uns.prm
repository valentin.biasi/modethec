!----------------------------------------------------------------------!
!-----------FICHIER DE PARAMETRES POUR EXECUTION DE MoDeTheC-----------!
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
! --> PARAM GENERAUX --------------------------------------------------!
version = '0.3'                                                        ! Numero de version MoDeTheC
auteur = 'Valentin_BIASI'                                              ! Auteur(s) du cas
file_mesh = '2D_uns.msh'                                               ! Nom du fichier de maillage
dim_simu = '2D_AXI'                                                    ! Dimension de simulation (3D, 2D_PLAN, 2D_AXI, 1D)
AXE_2D = 0.0 0.0 0.0 0.0 1.0 0.0                                       ! Axe de symétrie [x y z ux uy uz]
DIFFUSION = 'on'                                                       ! Active le solveur de diffusion
ADVECTION = 'off'                                                      ! Active le solveur d'advection
REACTION = 'off'                                                       ! Active le solveur de reactions
STRUCTURE = 'off'                                                      ! Active le solveur d'equilibre mecanique
ABLATION = 'off'                                                       ! Active le solveur d'ablation
COUPLAGE = 'off'                                                       ! Active le couplage avec un solveur externe
!
!----------------------------------------------------------------------!
! --> PARAM PHYSIQUE --------------------------------------------------!
nlim = 2                                                               ! Nombre total de limites
nesp = 1                                                               ! Nombre d'especes solides et gazeuses
ngaz = 0                                                               ! Nombre d'especes gazeuses
nsol = 1                                                               ! Nombre d'especes solides
ID_gaz =                                                               ! Identifiants des especes gazeuses
ID_sol = 1                                                             ! Identifiants des especes solides
nreac = 0                                                              ! Nombre de reactions
atmo = 'air'                                                           ! Type d'atmosphere environnant (air, vide, O2, N2)
satur_T = 0                                                            ! Active la saturation des proprietées au dessus de T_sat
T_sat = 0.0                                                            ! Temperature saturation des propriétés
k_homog = 'melange'                                                    ! Méthode d'évaluation de la conductivité thermique (melange, Mori-Tanaka)
Kp_typ = 'constant'                                                    ! Méthode d'évaluation du tenseur de perméabilité (constant, Henderson, Kozeny, Kozeny-ortho)
Kp_ini = 0.0                                                           ! Valeur de perméabilité isotrope du matériau (constant ou Henderson initial)
Kp_fin = 0.0                                                           ! Valeur de perméabilité isotrope du matériau (Henderson final)
Kp_0 = 0.0                                                             ! Perméabilité isotrope du matériau dans modèle de Kozeny-Karman
Kp_x = 0.0                                                             ! Perméabilité orthotrope en X dans modèle de Kozeny-Karman
Kp_y = 0.0                                                             ! Perméabilité orthotrope en Y dans modèle de Kozeny-Karman
Kp_z = 0.0                                                             ! Perméabilité orthotrope en Z dans modèle de Kozeny-Karman (en 3D)
Kp_max = 0.0                                                           ! Perméabilité maximale du matériau
mu_typ = 'constant'                                                    ! Méthode d'évaluation de la viscosité dynamique du matériau
mu_cst = 0.0                                                           ! Viscosité dynamique du matériau indépendante de la température
mu_nb_poly = 2                                                         ! Nombre de coefficients polynomiaux de viscosité
mu_poly = 0.0 0.0                                                      ! Coefficients polynomiaux de viscosité dynamique fonction de la température du matériau
T_abla = 0.0                                                           ! Température de fusion ``ablative'' du matériau
h_abla = 0.0                                                           ! Enthalpie massique de fusion ``ablative'' du matériau}
meca_typ = 'isotrope'                                                  ! Méthode d'évaluation des propriétés de mécanique structurelle du matériau (isotrope, transverse)
E_iso = 0.0                                                            ! Module d'Young isotrope du matériau
P_iso = 0.0                                                            ! Coefficient de Poisson isotrope du matériau
beta_iso = 0.0                                                         ! Coefficient de dilatation thermique isotrope du matériau
E_x = 0.0                                                              ! Module d'Young orthotrope en X du matériau
E_y = 0.0                                                              ! Module d'Young orthotrope en Y du matériau
P_xy = 0.0                                                             ! Coefficient de Poisson orthotrope en XY du matériau
G_xy = 0.0                                                             ! Module de cisaillement transverse en XY du matériau
beta_x = 0.0                                                           ! Coefficient de dilatation thermique transverse en X du matériau
beta_y = 0.0                                                           ! Coefficient de dilatation thermique transverse en X du matériau
!
!----------------------------------------------------------------------!
! --> PARAM ESPECES ---------------------------------------------------!
!
! --> ESPECE no 1 -----------------------------------------------------!
    id_esp = 1                                                         ! Indice de l'espèce courante
    nom_esp = 'Aluminium'                                              ! Nom d'usage de l'espèce courante
    typ_esp = 'solide'                                                 ! Phase de l'espèce courante (solide, gaz)
    rho_typ = 'constant'                                               ! Méthode d'évaluation de la masse volumique de l'espèce courante
    rho_cst = 2700.0                                                   ! Masse volumique de l'espèce courante indépendante de la température
    rho_nb_poly = 2                                                    ! Nombre de coefficients polynomiaux de rho_poly
    rho_poly = 1500.0 1.05                                             ! Coefficients polynomiaux de masse volumique fonction de la température de l'espèce courante
    Cp_typ = 'constant'                                                ! Méthode d'évaluation de la capacité calorifique de l'espèce courante
    Cp_cst = 910.0                                                     ! Capacité calorifique de l'espèce courante indépendante de la température
    Cp_nb_poly = 2                                                     ! Nombre de coefficients polynomiaux de Cp_poly
    Cp_poly = 910.0 0.0                                                ! Coefficients polynomiaux de capacité calorifique fonction de la température de l'espèce courante
    k_typ = 'constant'                                                 ! Méthode d'évaluation de la conductivité thermique de l'espèce courante
    k_cst = 237.0                                                      ! Conductivité thermique de l'espèce courante indépendante de la température
    k_nb_poly = 2                                                      ! Nombre de coefficients polynomiaux de k_poly
    k_poly = 237.0 1.0                                                 ! Coefficients polynomiaux de conductivité thermique isotrope fonction de la température de l'espèce courante
    k_nb_otho = 2                                                      ! Nombre de coefficients polynomiaux de k_ortho_x et k_ortho_y
    k_ortho_x = 0.0 0.0                                                ! Coefficients polynomiaux de conductivité thermique orthotrope en X fonction de la température
    k_ortho_y = 0.0 0.0                                                ! Coefficients polynomiaux de conductivité thermique orthotrope en Y fonction de la température
    k_ortho_z = 0.0 0.0                                                ! Coefficients polynomiaux de conductivité thermique orthotrope en Z fonction de la température
    k_Eshelby = 0.0 0.0 0.0                                            ! Coefficients diagonaux de la matrice d'Eshelby des inclusions ellispoïdales
    M = 0.0                                                            ! Masse molaire de l'espèce gazeuse courante
    eps = 1.0                                                          ! Emissivité radiative de l'espèce solide courante
    alpha = 1.0                                                        ! Absorptivité radiative de l'espèce solide courante
!
!----------------------------------------------------------------------!
! --> PARAM REACTIONS -------------------------------------------------!
!
!----------------------------------------------------------------------!
! --> PARAM NUMERIQUE -------------------------------------------------!
t0 = 0.0                                                               ! Temps initial de simulation
tf = 300.0                                                             ! Temps final de simulation
dt = 0.5                                                               ! Pas de temps standard
schema_A = 'theta-lin'                                                 ! Schéma d'intégration temporelle des équations d'advection
schema_DR = 'theta-imp'                                                ! Schéma d'intégration temporelle des équations de diffusion-réactions
schema_S = 'implicite'                                                 ! Schéma de résolution des équations de mécanique structurelle
solveur_LIN = 'GMRES'                                                  ! Solveur linéaire (GMRES)
theta_imp = 0.8                                                        ! Paramètre d'un schéma theta-implicite ou theta-implicite linéarisé
delta_max = 0.9                                                        ! Limitation relative des variations des quantités conservées entre chaque it.
CFL_max = 10.0                                                         ! Critère CFL maximal lors du schéma theta-implicite linéarisé schema_A
tol_abs = 1e-06                                                        ! Critère de convergence absolu des équations A-D-R
tol_rel = 1e-06                                                        ! Critère de convergence relatif des équations A-D-R
tol_int = 0.1                                                          ! Critère de convergence des itérations internes du solveur linéaire
niter_max = 20                                                         ! Nombre d'itérations non-linéaires maximal à chaque itération temporelle
niter_max_int = 50                                                     ! Nombre d'itérations internes maximal du solveur linéaire
CFL_AB = 1.0                                                           ! Critère CFL maximal de la déformation du maillage en ablation
pas_ech = 10                                                           ! Nombre de points de sous-échantillonge des limites ablatives
niter_def = 20                                                         ! Nombre d'itérations de déplacement du maillage en ablation (opérateur laplacien)
tol_def = 0.001                                                        ! Critère de convergence des itérations de déformation du maillage en ablation
!
!----------------------------------------------------------------------!
! --> PARAM CONDITIONS INITIALES --------------------------------------!
Tini = 300.0                                                           ! Température initiale à t0 [K]
Pini = 101325.0                                                        ! Pression  initiale à t0 [Pa]
phi_ini = 1.0                                                          ! Fractions volumiques initiales des nesp espèces (dans l'ordre de définition)
!
!----------------------------------------------------------------------!
! --> PARAM CONDITIONS LIMITES ----------------------------------------!
!
! --> LIMITE no 1 -----------------------------------------------------!
    id_lim = 1                                                         ! Indice de la limite courante
    nom_lim = 'IN'                                                     ! Nom de la limite courante
    ener_typ = 'flux'                                                  ! Type de limite énergétique (flux, temp, mixte, speciale)
    Fimp = 0.0                                                         ! Flux thermique imposé (flux ou mixte) [W/m2]
    Timp = 300.0                                                       ! Température imposée (temp) [K]
    T_conv = 293.0                                                     ! Température d'échange convectif (mixte) [K]
    h_conv = 0.0                                                       ! Coefficient d'échange convectif (mixte) [W/m2/K]
    T_rad = 293.0                                                      ! Température d'échange radiatif (mixte) [K]
    file_ener_cls = ''                                                 ! Fichier de condition limite spéciale
    masse_typ = 'debit'                                                ! Type de limite massique (pression, debit)
    Dimp = 0.0                                                         ! Débit imposé (debit) [kg/m2/s]
    Pimp = 101325.0                                                    ! Pression imposée (pression) [Pa]
    struct_typ = 'contrainte'                                          ! Type de limite structure (fixe, contrainte, axe)
    stress_imp = 0.0 0.0                                               ! Contrainte mécanique imposée normale et tangentielle [Pa]
    abla_typ = 'temp'                                                  ! Type de limite ablative (temp, meca)
!
! --> LIMITE no 2 -----------------------------------------------------!
    id_lim = 2                                                         ! Indice de la limite courante
    nom_lim = 'OUT'                                                    ! Nom de la limite courante
    ener_typ = 'mixte'                                                 ! Type de limite énergétique (flux, temp, mixte, speciale)
    Fimp = 0.0                                                         ! Flux thermique imposé (flux ou mixte) [W/m2]
    Timp = 400.0                                                       ! Température imposée (temp) [K]
    T_conv = 0.0                                                       ! Température d'échange convectif (mixte) [K]
    h_conv = 0.0                                                       ! Coefficient d'échange convectif (mixte) [W/m2/K]
    T_rad = 1200.0                                                     ! Température d'échange radiatif (mixte) [K]
    file_ener_cls = ''                                                 ! Fichier de condition limite spéciale
    masse_typ = 'debit'                                                ! Type de limite massique (pression, debit)
    Dimp = 0.0                                                         ! Débit imposé (debit) [kg/m2/s]
    Pimp = 101325.0                                                    ! Pression imposée (pression) [Pa]
    struct_typ = 'contrainte'                                          ! Type de limite structure (fixe, contrainte, axe)
    stress_imp = 0.0 0.0                                               ! Contrainte mécanique imposée normale et tangentielle [Pa]
    abla_typ = 'temp'                                                  ! Type de limite ablative (temp, meca)
!
!----------------------------------------------------------------------!
! --> PARAM SUIVI -----------------------------------------------------!
suivi = 1                                                              ! Suivi atif on(1) - off(0)
file_log = 1                                                           ! Fichier log enregistré on(1) - off(0)
dt_prt = 20.0                                                          ! Pas de temps des print écran (multiple de dt)
file_hdf5 = 0                                                          ! Export des valeurs intégrales au format HDF5
dt_hdf5 = 1.0                                                          ! Pas de temps des exports HDF5
!
!----------------------------------------------------------------------!
! --> PARAM EXPORT ----------------------------------------------------!
export = 1                                                             ! Export atif on(1) - off(0)
nom_exp = 'out_analytique'                                             ! Nom du fichier d'export
dt_exp = 20.0                                                          ! Pas de temps des exports (multiple de dt)
exp_ini = 1                                                            ! Export de l'état initial on(1) - off(0)
deform_mlg = 0                                                         ! Déformation du maillage pour calcul structure on(1) - off(0)
deform_amp = 0.0                                                       ! Amplitude de déformation du maillage
FEmode = 1                                                             ! Mode Finite Elements Tecplot on(1) - off(0)
tecplot_bin = 0                                                        ! Format Tecplot binaire on(1) - off(0)
tecplot_ascii = 0                                                      ! Format Tecplot ASCII on(1) - off(0)
vtk_ascii = 1                                                          ! Format VTK ASCII on(1) - off(0)
sommet = 0                                                             ! Export des variables au sommets
cellule = 1                                                            ! Export des variables au centre des cellules
volume = 1                                                             ! Export des champs volumiques
surface = 0                                                            ! Export des champs surfaciques
nexpt = 1                                                              ! Nombre de variables à exporter
    var_exp = 'T'          1    'T [K]'                                !
!----------------------------------------------------------------------!
! <-- END
