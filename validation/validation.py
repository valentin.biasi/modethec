#! /usr/bin/env python
#­* ­coding: utf­8 ­*­
#
# Script de validations de solutions modethec par rapport a des
# solutions de reference : comsol, solution analytique, ...
#
import sys, os
sys.path.insert(0, '../gui/')
#
import modethec_gui
import mdtc_io
#
from vtk import *
from vtk.util.numpy_support import vtk_to_numpy
import numpy as np
import matplotlib.pyplot as plt
from argparse import ArgumentParser
from scipy import interpolate
import time

# Nom des programmes
modethec = '../../modethec'

# Coloration de la console
col_r = '\033[31m'
col_g = '\033[32m'
col_b = '\033[34m'
col_k = '\033[0m'

#----------------------------------------------------------------------#
# Routine de lecture d'un fichier VTK : Sortie sous forme de nuage de
# points x, y ,z ,var
def read_vtk_data(filename, varname):
    
    # Read the source file.
    reader = vtkUnstructuredGridReader()
    reader.SetFileName(filename)
    reader.Update()
    output = reader.GetOutput()

    # Lecture des coordonnees X, Y et Z aux sommets
    points = output.GetPoints()
    array = points.GetData()
    numpy_nodes = vtk_to_numpy(array)
    x_som = numpy_nodes[:,0]
    y_som = numpy_nodes[:,1]
    z_som = numpy_nodes[:,2]

    # Lecture des coordonnees X, Y et Z aux cellules
    cellCenters = vtkCellCenters()
    cellCenters.SetInputData(output)
    cellCenters.Update()
    numpy_nodes = vtk_to_numpy( cellCenters.GetOutput().GetPoints().GetData() )
    x_cel = numpy_nodes[:,0]
    y_cel = numpy_nodes[:,1]
    z_cel = numpy_nodes[:,2]

    # Lecture de la variable aux sommets si existe
    try:
        var = vtk_to_numpy( output.GetPointData().GetArray( varname ) )
        x = x_som
        y = y_som
        z = z_som

    # Lecture de la variable aux cellules sinon    
    except:
        var = vtk_to_numpy( output.GetCellData().GetArray( varname ) )
        x = x_cel
        y = y_cel
        z = z_cel
        
    # Retourne les coodonnees + la variable demandee
    return x, y, z, var


#----------------------------------------------------------------------#
# Routine de lecture d'un fichier csv exportee a partir de COMSOL
def read_comsol_data(filename, dim):
    
    # Lecture rapide d'un tableau CSV
    A = np.genfromtxt(filename,delimiter=',',skip_header=9)
    
    if dim==1:
        x=A[:,0]
        var=A[:,1]
    elif dim==2:
        x=np.sqrt( A[:,0]**2 + A[:,1]**2)
        var=A[:,2]
    else:
        x=np.sqrt( A[:,0]**2 + A[:,1]**2 + A[:,2]**2)
        var=A[:,3]
    
    # Retourne deux tableaux X,Y
    return x, var


#----------------------------------------------------------------------#
# Definition du plot style
def plot_style():

    plt.style.use('fivethirtyeight')

    plt.rcParams['savefig.transparent'] = True
    plt.rcParams['lines.linewidth'] = 2.0

    plt.rcParams['xtick.major.size'] = 6.0
    plt.rcParams['xtick.major.width'] = 2.0
    plt.rcParams['ytick.major.size'] = 6.0
    plt.rcParams['ytick.major.width'] = 2.0

    plt.rcParams['xtick.direction'] = 'in'
    plt.rcParams['ytick.direction'] = 'in'

    plt.rcParams['axes.edgecolor'] = 'k'
    plt.rcParams['axes.linewidth'] = 2.0

    plt.rcParams['font.size'] = 10.0           
    plt.rcParams['xtick.labelsize'] = 14.0
    plt.rcParams['ytick.labelsize'] = 14.0
    plt.rcParams['axes.labelsize'] = 16.0
    


#---- Main ------------------------------------------------------------#
if __name__ == '__main__':
    
    # Options
    parser = ArgumentParser(usage="usage: analytique.py [options] \nExemple : analytique.py -v --only=1D,2D_temp", conflict_handler="resolve")
    parser.add_argument("-o", "--only", dest="only", help="Calcul uniquement sur les cas listes -o 1D,2D_temp,... (separation uniquement par virgules)")
    parser.add_argument("-v", "--verbose", dest="verbose", action="store_true", default=False, help="Print des sorties modethec en live")
    parser.add_argument("--noplot", dest="noplot", action="store_true", default=False, help="Pas d'affichage des plots a l'ecran")
    args, unknown = parser.parse_known_args()
    verbose = args.verbose
    
    # En-tete de print
    print '!--------------------------------------------------------------!'
    print '!--------- Script de validation analytique de MoDeTheC --------!'
    print '!--------------------------------------------------------------!'
    
    # Comptage du temps CPU
    t0 = time.time()
    
    # Retourne la liste des dossiers dans l'ordre alphabetique
    dirnames = next(os.walk('.'))[1]
    dirnames.sort(key=str.lower)

    # Definition du plot style
    plot_style()
    
    # Split en liste de integer de only (si existe)
    if args.only<>None:
        only = args.only.split(",")
        for i, item in enumerate(only):
            only[i] = item
        
        # Et redefinition de dirnames par le filtre de l'option utilisateur output
        newdirs = []
        for item in only:
            newdirs.extend( [x for x in dirnames if item in x] )
        dirnames = newdirs
        
        
    #--- Pour chaque cas de validation
    for i in range(len(dirnames)):
        
        print '!'
        print '! Validation no %i : %s' % (i+1,dirnames[i])

        # Changement de dossier
        os.chdir(dirnames[i])
        
        # Dimension du cas test
        if '1D' in dirnames[i]:
            dim = 1
        elif '2D' in dirnames[i]:
            dim = 2
        else:
            dim = 3
        
        # Mise en place d'un nouveau plot
        fig = plt.figure()
        # Avec son titre associe
        fig.canvas.set_window_title(dirnames[i])
        
        # Deifintion d'une double echelle ax1 et ax2
        ax1 = fig.add_subplot(111)
        ax2 = fig.add_subplot(111, sharex=ax1, frameon=False)
        ax2.yaxis.tick_right()
        ax2.yaxis.set_label_position("right")
        plt.xlabel('x [m]')
        ax1.set_ylabel('T [K]')
        ax2.spines['right'].set_color('g')
        ax2.yaxis.label.set_color('g')
        
        # Ouverture de l'interface python modethec_gui (sans graphique...)
        MDTC = modethec_gui.mdtc_ui()
        mdtc_io.open_str( MDTC.PRM, dirnames[i]+'.prm' )
        
        # Va chercher des infos dans le fichier parametre pour deinir le nom du fichier d'export final en vtk
        nb_export = int( MDTC.PRM['NUMERIQUE']['tf'].v / MDTC.PRM['EXPORT']['dt_exp'].v )
        result_filename = 'vtk/' + MDTC.PRM['EXPORT']['nom_exp'].v + '.' + str(nb_export) + '.vtk'
        
        # Reenrgistrement du fichier de parametres: permet de garder les fichiers de parametres a jour
        mdtc_io.save_str( MDTC.PRM, dirnames[i]+'.prm' )
        print '! %sMise a jour du fichier %s.prm par modethec_gui : OK%s' % (col_g,dirnames[i],col_k)
        
        #--- Execution de modethec
        print '! %sExecution de modethec en cours ... %s' % (col_b,col_k)
        if args.verbose:
            os.system(modethec+' '+dirnames[i]+'.prm')
        else:
            os.system(modethec+' '+dirnames[i]+'.prm > trash')
            os.system('rm trash')
        
        # Lecture du vtk en mise en forme sous forme de coordonnees cylindriques si dim>1
        print '! %sLecture du fichier %s : OK%s' % (col_g,result_filename,col_k)
        [x,y,z,T] = read_vtk_data(result_filename, "T")
        if (dim>1):
            x = np.sqrt( x**2 +y**2 +z**2 )
        
        # Lecture du fichier de reference au format d'export standard comsol (seul le dernier instant doit etre exporte)
        print '! %sLecture du fichier %s_comsol.csv : OK%s' % (col_g,dirnames[i],col_k)
        [x_ref,T_ref] = read_comsol_data(dirnames[i]+'_comsol.csv',dim)
        

        # Interpolation sur x,T de comsol
        f = interpolate.interp1d(x_ref,T_ref, bounds_error=False,assume_sorted=False)
        epsilon = f(x[3:-3])
        epsilon = epsilon - T[3:-3]
        
        # Plot modethec + comsol
        ax1.plot(x,T,'s',label='Modethec')
        ax1.plot(x_ref,T_ref,'o',label='Comsol')
        
        # Plot de l'erreur
        err, = ax2.plot(x[3:-3],epsilon,'g.')
        ax2.set_ylabel('Error [K]')
        
        # Mise en page
        ax1.legend(loc=0)
        plt.tight_layout()
        
        # Sauvegarde auto
        plt.savefig(dirnames[i]+'.png')
        os.system('mogrify -trim '+dirnames[i]+'.png')
        
        # Affichage
        if (not args.noplot):
            print '! Plot du cas ... (Fermer la fenetre pour continuer)'
            plt.show()
        else:
            plt.close()
        
        # Nettoyage et remonte au dossier racine
        os.system('rm *.log *.out')
        os.chdir('..')
    
    
    print '!'
    print '! Temps total : %.2f s.' % (time.time() - t0)
