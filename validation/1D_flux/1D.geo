// MAILLAGE 1D (2D AVEC 1 MAILLE EN y) REGULIER

dx=1.0; // DIMENSION EN X
nx=501; // NOMBRE DE SOMMETS EN X
ny=2; // NOMBRE DE SOMMETS EN Y = 2
dy=dx/30; // DIMENSION EN Y

// POINTS
Point(1) = {0, 0, 0, 1};
Point(2) = {dx, 0, 0, 1};
Point(3) = {dx, dy, 0, 1};
Point(4) = {0, dy, 0, 1};

// LIGNES DROITES
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

// SURFACES
Line Loop(5) = {1,2,3,4};
Plane Surface(5) = {5};
Transfinite Surface {5};

// RAFFINEMENT DES LIGNES
Transfinite Line {1} = nx Using Progression 1;
Transfinite Line {2} = ny Using Progression 1;
Transfinite Line {3} = nx Using Progression 1;
Transfinite Line {4} = ny Using Progression 1;

// NOM DES LIGNES ET SURFACES
Physical Line("SIDE") = {1,3};
Physical Line("LEFT") = {4};
Physical Line("RIGHT") = {2};
Physical Surface("INTERIEUR") = {5};

// ACTIVE NO LIGNE ET POINTS
Geometry.LineNumbers = 1;
Geometry.PointNumbers = 1;
General.Axes = 1;

// ALGO DE MAILLAGE QUAD
Mesh.Algorithm = 8;
Recombine Surface "*";
