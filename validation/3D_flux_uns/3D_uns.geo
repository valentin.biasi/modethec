// MAILLAGE D'UN RECTANGLE 2D AXI EN QUAD


dr=0.5; // DIMENSION EN R
nr=16; // NOMBRE DE SOMMETS EN R

// POINTS
Point(1) = {0, 0, 0, 1};
Point(2) = {dr, 0, 0, 1};
Point(3) = {0, dr, 0, 1};
Point(4) = {0, 0, dr, 1};

// LIGNES DROITES
Line(1) = {1, 2};
Line(2) = {1, 3};
Line(3) = {1, 4};

Circle(4) = {3, 1, 4};
Circle(5) = {3, 1, 2};
Circle(6) = {2, 1, 4};

// SURFACES
Line Loop(7) = {2, 4, -3};
Plane Surface(8) = {7};
Line Loop(9) = {1, -5, -2};
Plane Surface(10) = {9};
Line Loop(11) = {3, -6, -1};
Plane Surface(12) = {11};
Line Loop(13) = {4, -6, -5};
Ruled Surface(14) = {13};

// VOLUME
Surface Loop(15) = {14, 8, 10, 12};
Volume(16) = {15};

// RAFFINEMENT DES LIGNES
Transfinite Line {1} = nr Using Progression 1;
Transfinite Line {2} = nr Using Progression 1;
Transfinite Line {3} = nr Using Progression 1;
Transfinite Line {4} = nr*3.14/2 Using Progression 1;
Transfinite Line {5} = nr*3.14/2 Using Progression 1;
Transfinite Line {6} = nr*3.14/2 Using Progression 1;

// NOM DES LIGNES ET SURFACES
Physical Surface("OUT") = {14};
Physical Surface("IN") = {8,10,12};
Physical Volume("INTERIOR") = {16};

// ACTIVE NO LIGNE ET POINTS
Geometry.LineNumbers = 1;
Geometry.PointNumbers = 1;
General.Axes = 1;

// ALGO DE MAILLAGE QUAD
Mesh.Algorithm = 6;
Mesh.Algorithm3D = 9;Mesh.Smoothing = 100;
