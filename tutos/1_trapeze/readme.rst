Trapezium
*********


**Directory:** ``tutos/1_trapeze/``


This very simple case exhibits a heat conduction model on a 2D planar trapezium meshed with triangles.

.. _trapeze_tuto:
.. figure:: ../tutos/1_trapeze/trapeze.png
   :width: 80%
   :align: center

   Temperature field at the end of the trapezium simulation


Simulation description:
    + 1 solid species (Aluminium)
    + Thermal conductivity and heat capacity independant of temperature
    + Mesh with equilateral triangles (2nd order in space)
    + Time integration scheme: theta-implicit
    + Boundary condition **"BASE"**: Imposed temperature at :math:`T=293K`
    + Boundary condition **"HAUT"**: Imposed heat flux (Adiabatic wall)
    + Boundary condition **"GAUCHE"**: Convective heating (:math:`T_{conv} = 1200K` and :math:`h_{conv} = 100W/m^2/K`)
    + Boundary condition **"DROITE"**: Radiative heating (:math:`T_{rad} = 1200K` )
