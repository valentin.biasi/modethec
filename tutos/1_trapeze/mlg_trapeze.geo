// MAILLAGE D'UN TRAPEZE 2D EN TRIANGLES

db=0.02; // DIMENSION DE LA BASE / 2
nb=21; // NOMBRE DE SOMMETS PAR FACE

// POINTS
Point(1) = {db, 0, 0, 1};
Point(2) = {db*Cos(Pi/3), db*Sin(Pi/3), 0, 1};
Point(3) = {-db*Cos(Pi/3), db*Sin(Pi/3), 0, 1};
Point(4) = {-db, 0, 0, 1};

// LIGNES DROITES
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

// SURFACES
Line Loop(5) = {1,2,3,4};
Plane Surface(5) = {5};

// RAFFINEMENT DES LIGNES
Transfinite Line {1} = nb Using Progression 1;
Transfinite Line {2} = nb Using Progression 1;
Transfinite Line {3} = nb Using Progression 1;
Transfinite Line {4} = nb*2-1 Using Progression 1;

// NOM DES LIGNES ET SURFACES
Physical Line("DROITE") = {1};
Physical Line("HAUT") = {2};
Physical Line("GAUCHE") = {3};
Physical Line("BASE") = {4};
Physical Surface("INTERIEUR") = {5};

// ACTIVE NO LIGNE ET POINTS
Geometry.LineNumbers = 1;
Geometry.PointNumbers = 1;
General.Axes = 1;

// ALGO DE MAILLAGE TRI
Mesh.Algorithm=6; //Frontal
Mesh.Smoothing=5;
//+
Point(5) = {0, 0, -0, 1.0};
//+
Point(6) = {-0, 0.2, 0, 1.0};
//+
Point(7) = {-0.1, 0.1, 0, 1.0};
//+
Line(5) = {4, 7};
//+
Line(6) = {7, 6};
//+
Line(7) = {6, 2};
//+
Curve Loop(6) = {5, 6, 7, 2, 3};
//+
Plane Surface(6) = {6};
//+
Physical Surface("exterieur") = {6};
