Sphere heat conduction
**********************


**Directory:** ``tutos/6_sphere/``

This case is an simple heat condution simulation, similar to a 1D spherical case, in one eighth of 3D sphere.

.. _sphere_tuto:
.. figure:: ../tutos/6_sphere/sphere.png
   :width: 60%
   :align: center

   Temperature field in the 3D sphere


Simulation description:
    + 1 solid species (Aluminium)
    + Thermal conductivity and heat capacity independant of temperature
    + 3D mesh: tetrahedra
    + Diffusion time integration scheme: theta-implicit
    + Boundary condition **"OUT"**: Fixed temperature :math:`T_{OUT}=400K`
    + Boundary condition **"IN"**: Adiabatic wall (symmetric condition)
    + Initial temperature :math:`T_{0}=300K`

.. note:: This case can be easily solved analytically and cans be used to validate the heat diffusion solver of Modethec.
    