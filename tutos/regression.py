#! /usr/bin/env python
# -*- coding: utf-8 -*-
#----------------------------------------------------------------------#
# Script de non-regression de modethec
# Teste la non-regression de modethec_gui et modethec
#
# Définition des cas tests dans la liste 'DATA' contenant 
# les objets 'tuto'
#
# Utilisation : ./regression.py
#               => Teste tous les cas de tutos
#                  Affiche seulement s'il y a des diff de .prm et .dat
#
# Options :     -v --verbose : Print des sorties modethec en live
#               -c --compare : Print les différences avec tkdiff
#               -u --update  : Mets à jour les .prm et les .dat dans 
#                              REF/ si utilisateur réponds "y"  
#               -o 1,2,... --only=1,2,...
#                            : Régression uniquement sur les tutos listés
#                              (séparation uniquement par virgules)
#
# Débuté le 07/04/2017
# Auteur : Valentin Biasi
#
import os
import sys
import filecmp
from optparse import OptionParser
import time

#---- Classe définissant un tuto -------------------------------------#
# tuto( directory, \
#       parameter_file (string or list), \
#       output_file (string or list), \
#       optional ref_prm_file (string or list), \
#       optional ref_out_file (string or list) )
#---------------------------------------------------------------------#
class tuto(object):

    # A l'initialisation
    def __init__(self, directory, parameter_file, output_file, \
                ref_prm_file=['ref/ref.prm'], \
                ref_out_file=['ref/ref.dat']):

        
        self.dir = directory

        if (type(parameter_file) != str):
            self.prm = parameter_file
        else:
            self.prm = []
            self.prm.append( parameter_file )
        
        self.n_run = len(self.prm)

        if (type(output_file) != str):
            self.out = output_file
        else:
            self.out = []
            self.out.append( output_file )
        
        if (type(ref_prm_file) == str):
            self.ref_prm = []
            self.ref_prm.append( ref_prm_file )
        else:
            if (len( self.prm ) > 1):
                self.ref_prm = []
                for k in range(len(parameter_file)):
                    self.ref_prm.append( 'ref/ref'+str(k+1)+'.prm' )
            else:
                self.ref_prm = ref_prm_file

        if (type(ref_out_file) == str):
            self.ref_out = []
            self.ref_out.append( ref_out_file )
        else:
            if (len( self.out ) > 1):
                self.ref_out = []
                for k in range(len(output_file)):
                    self.ref_out.append( 'ref/ref'+str(k+1)+'.dat' )
            else:
                self.ref_out = ref_out_file

        if (len( self.prm ) > 1):
            self.log_file = []
            for k in range(len(parameter_file)):
                self.log_file.append( 'mdtc_output.log.'+str(k) )
        else:
            self.log_file=['mdtc_output.log']

        if (len( self.prm ) > 1):
            self.ref_log_file = []
            for k in range(len(parameter_file)):
                self.ref_log_file.append( 'ref/ref'+str(k+1)+'.log' )
        else:
            self.ref_log_file=['ref/ref.log']

    def __str__(self):
        return "Tuto name: {}, Parameter: {}, Output: {}".format(self.dir, self.prm, self.out)
#----------------------------------------------------------------------#


#---- Liste des simulations de reference ------------------------------#
#---- A completer au fur et a mesure ----------------------------------#
DATA = []
DATA.append( tuto('1_trapeze/', 'trapeze.prm', 'out_trapeze.dat'))
DATA.append( tuto('2_bouteille/', 'bouteille.prm', 'out_bouteille.dat'))
DATA.append( tuto('3_percage/', 'percage.prm', 'out_percage.dat'))
DATA.append( tuto('4_henderson/', 'henderson.prm', 'out_henderson.dat'))
DATA.append( tuto('5_nozzle/', 'nozzle.prm', 'out_nozzle.dat'))
DATA.append( tuto('6_sphere/', 'sphere.prm', 'out_sphere.dat'))
DATA.append( tuto('7_stringer/', 'stringer.prm', 'out_stringer.dat'))
DATA.append( tuto('8_cwipi_2D/', ['gauche.prm', 'centre.prm', 'droite.prm'], \
                ['out_gauche.dat', 'out_centre.dat', 'out_droite.dat'] ))
DATA.append( tuto('9_cwipi_3D/', ['gauche.prm', 'droite.prm'], \
                ['out_gauche.dat', 'out_droite.dat'] ))
DATA.append( tuto('10_cube3D_AB/', 'cube3D.prm', 'out_cube3D.dat'))  
DATA.append( tuto('11_sphere3D_AB/', 'sphere.prm', 'out_sphere.dat'))
DATA.append( tuto('12_flux_ablation/', 'plaque2D.prm', 'out_plaque2D.dat'))
DATA.append( tuto('13_multidom_2D/', 'multidom_2D.prm', 'out_multidom_2D.dat'))
DATA.append( tuto('14_ortho_multilayer/', 'ortho.prm', 'out_ortho.dat'))
#----------------------------------------------------------------------#


# Nom des programmes
modethec = 'modethec'
modethec_gui = 'modethec_gui'
adethec = 'adethec'

# Coloration de la console
col_r = '\033[31m'
col_g = '\033[32m'
col_b = '\033[34m'
col_k = '\033[0m'


#--- Fonction diff entre 2 fichiers -----------------------------------#
def show_diff( file1, file2, yes_to_all=False ):

    if not(yes_to_all):
        # Avoid EOFError
        sys.stdin = open('/dev/tty')
        # User input
        choice = str(raw_input('See tkdiff? [y/N] [ENTER]\n')).lower().strip()

    else:
       choice = 'y' 

    # Tkdiff of file1 and file2
    if choice == 'y':
        os.system('tkdiff ' + file2 + ' ' + file1)


#--- Fonction update file1 to file2 -----------------------------------#
def update_file( file1, file2, yes_to_all=False ):

    if not(yes_to_all):
        # Avoid EOFError
        sys.stdin = open('/dev/tty')
        # User input
        choice = str(raw_input('Update the reference file? [y/N] [ENTER]\n')).lower().strip()

    else:
       choice = 'y' 
    
    # Replace file2 by the new file1
    if choice == 'y':
        os.system('cp ' + file1 + ' ' + file2)
        print '!--- %s File %s updated %s' % (col_b, file1, col_k)
    
    return choice
        

#---- Main ------------------------------------------------------------#
if __name__ == '__main__':
    
    # Options
    parser = OptionParser(usage="usage: %prog [options] \nExemple: regression.py -v -u -c --only=2,4")
    parser.add_option("-c", "--compare", dest="compare", action="store_true", default=False, help="Print differences for .prm and .dat files with tkdiff")
    parser.add_option("-u", "--update", dest="update", action="store_true", default=False, help="Update .prm and .dat files in the ref/ directory")
    parser.add_option("-o", "--only", dest="only", help="Test only cases in the list -o 1,2,... or --only=1,2,... (case numbers separated with commas)")
    parser.add_option("-v", "--verbose", dest="verbose", action="store_true", default=False, help="Print modethec output")
    parser.add_option("--nogui", dest="nogui", action="store_true", default=False, help="Do not test regression on .prm files")
    parser.add_option("--noexe", dest="noexe", action="store_true", default=False, help="Do not run modethec solver")
    parser.add_option("-y", "--yes", dest="yes_to_all", action="store_true", default=False, help="Yes answer to all update and compare propositions")
    (options, args) = parser.parse_args()
    
    # Split en liste de integer de only (si existe)
    if options.only:
        options.only = options.only.split(",")
        for i, item in enumerate(options.only):
            options.only[i] = int(item)
    
    # Pas d'argument principal (seulement des options)
    if args:
        parser.error("invalid argument")
    
    # En-tete de print
    print '!--------------------------------------------------------------!'
    print '!--------- Validation - regression script of MoDeTheC ---------!'
    print '!--------------------------------------------------------------!'

    # Initialisation
    t_total = 0.0
    ierr_prm = 0
    ierr_log = 0
    ierr_out = 0
    nb_tuto = 0   

    # Pour chaque element de DATA : 1 simulation a valider
    for i in range(len(DATA)):
        
        # Si options.only existe et correspond a l'id courant
        try:
            b = options.only.index(i+1)
            bool_only = True
        except:
            bool_only = False
        
        # Active test no i si --only inactif ou --only actif et id correspond
        if (not options.only) or bool_only:
            
            nb_tuto += 1    # Compteur tuto

            # En-tete par tuto
            print '!'
            print '! %sValidation no %i : %s%s' % (col_b,i+1,DATA[i].dir,col_k)

            # Changement de dossier
            os.chdir(DATA[i].dir)

            # Effacement du fichier de sortie
            try:
                for out_file in DATA[i].out:
                    os.remove(out_file)   
            except:
                pass
            
            # Effacement du/des fichier(s) de log
            try:
                if DATA[i].n_run == 1:
                    os.remove('mdtc_error.log') 
                else:
                    for j in range(DATA[i].n_run):
                        os.remove('mdtc_error.log.'+str(j))
            except:
                pass

            #-------------------------------------------------------------#
            #--- Execution de modethec_gui in -o out ---------------------#
            if not(options.nogui):

                #... pour chaque .prm de la liste DATA[i].prm
                for j in range(DATA[i].n_run):

                    t0 = time.time()
                    os.system(modethec_gui+' '+DATA[i].prm[j]+' -o '+DATA[i].prm[j])
                    t_total += time.time() - t0

                    # Comparaison des fichiers prm
                    bool_cmp = filecmp.cmp(DATA[i].prm[j], DATA[i].ref_prm[j], shallow=False)   
                    if bool_cmp:
                        print '! %sSuccessful comparison file: %s%s' % (col_g, DATA[i].prm[j], col_k)
                    else:
                        print '! %sFailed comparison file: %s%s' % (col_r, DATA[i].prm[j], col_k)
                        ierr_prm += 1
                    
                        # Si compare : Demande affichage tkdiff
                        if options.compare:
                            show_diff( DATA[i].prm[j], DATA[i].ref_prm[j], options.yes_to_all)
                        
                        # Si option update : Demande update
                        if options.update:
                            update_file( DATA[i].prm[j], DATA[i].ref_prm[j], options.yes_to_all)
            #-------------------------------------------------------------#
            
            #-------------------------------------------------------------#
            #--- Execution de modethec *.prm -----------------------------#
            if not(options.noexe):

                # Ligne de commande : Execution de modethec
                # Cas 1 fichier .prm
                if DATA[i].n_run == 1:
                    run_cmd = modethec+' '+DATA[i].prm[0]
                # Cas multiples fichiers .prm
                else:
                    run_cmd = 'mpiexec'
                    for j in range(DATA[i].n_run):
                        run_cmd = run_cmd + ' -np 1 ' + modethec + ' ' + DATA[i].prm[j]
                        if j < len(DATA[i].prm)-1:
                            run_cmd = run_cmd + ' :'

                # Si pas verbose : renvoi vers fichier trash
                if not(options.verbose):
                    run_cmd = run_cmd + ' > trash'

                # Run modethec
                t0 = time.time()
                os.system(run_cmd)
                t_total += time.time() - t0

                # Si execution de modethec non finie
                for j in range(DATA[i].n_run):
                    
                    successful_run = True
                    errors_in_run = True

                    if DATA[i].n_run == 1:
                        error_file = "mdtc_error.log"
                    else:
                        error_file = "mdtc_error.log."+str(j)

                    if not(os.path.isfile(error_file)):
                        successful_run = False

                    # Si execution de modethec finie
                    if successful_run:
    
                        if (os.stat(error_file).st_size == 0):
                            errors_in_run = False
                        
                        # Si erreurs :
                        if errors_in_run:
                            print '! %sErrors in mdtc_error.log %s' % (col_r,col_k)
                            ierr_log += 1
                        else:
                            print '! %sNo error in mdtc_error.log %s' % (col_g,col_k)

                        # Comparaison des fichiers dat
                        bool_cmp = filecmp.cmp(DATA[i].out[j], DATA[i].ref_out[j], shallow=False)   
                        if bool_cmp:
                            print '! %sSuccessful comparison file: %s%s' % (col_g, DATA[i].out[j], col_k)
                        else:
                            print '! %sFailed comparison file: %s%s' % (col_r, DATA[i].out[j], col_k)
                            ierr_out += 1
                            
                            # # Si compare : Demande affichage tkdiff
                            if options.compare:
                                show_diff( DATA[i].out[j], DATA[i].ref_out[j], options.yes_to_all)
                            
                            # Si option update : Demande update
                            if options.update:
                                out_updated = update_file( DATA[i].out[j], DATA[i].ref_out[j], options.yes_to_all)

                                if out_updated == 'y':
                                    update_file( DATA[i].log_file[j], DATA[i].ref_log_file[j], yes_to_all='True')


                    if not(successful_run):
                        print '! %sModethec run has failed %s' % (col_r,col_k)
                        ierr_log += 1
                        ierr_out += 1
            #-------------------------------------------------------------#
     

            # Effacement fichier temporaire
            try:
                os.remove('trash')
            except:
                pass

            # Retour dossier source
            os.chdir('..')

    # Pied de page de print        
    print '!'
    print '!--------------------------------------------------------------!'
    print '!---------- End of MoDeTheC validation - regression -----------!'
    print '!----------                                         -----------!'
    print '!---------- Total time : %7.2f s.                 -----------!' % (t_total)
    print "!---------- Number of tests : %3i                   -----------!" % nb_tuto
    if not(options.nogui):
        print "!---------- Errors in prm files : %3i               -----------!" % ierr_prm
    if not(options.noexe):
        print "!---------- Errors in log files : %3i               -----------!" % ierr_log
        print "!---------- Errors in out files : %3i               -----------!" % ierr_out
    print '!--------------------------------------------------------------!'
