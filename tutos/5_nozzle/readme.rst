Nozzle ablation
***************


**Directory:** ``tutos/5_nozzle/``

This case exhibits the decomposition of an ablative material submitted to high heat flux, with an internal pyrolysis reaction. The simulation dimension is an axisymmetric 2D.

.. _nozzle_tuto:
.. figure:: ../tutos/5_nozzle/nozzle.png
   :width: 80%
   :align: center

   Temperature field on the ablated nozzle


Simulation description:
    + 2 solid species (Virgin and Char)
    + 1 gazeous species (Decomposition gas)
    + Thermal conductivity and heat capacity independant of temperature
    + 2D mesh: triangles
    + Diffusion time integration scheme: theta-implicit
    + Gas transport time integration scheme: theta-implicit
    + One reaction is set to transform the virging species to char and decomposition gas
    + The ablation of the material is modeled using a fusion model, at fixed temperature
    + Boundary condition **"AXIS"**: Adiabatic and hermetic wall, non-ablative
    + Boundary condition **"BASE"**: Adiabatic wall, non-ablative
    + Boundary condition **"NOSE"**: Gaussian heat flux with radiative cooling, ablative boundary

.. note:: The heating of the nose is variable in space. To define such a boundary, a special boundary condition file has to be created. In this simulation, the``ener_top_gauss.cls`` file defines the gaussian impinging heat flux. Please refer to the :ref:`user-specified_conditions` section to get more information.

.. note:: The deformation of the wall due to ablation is done with a spline reconstruction method. Displacements of the cells are done using a Laplacian mesh deformation method. See :doc:`/ablation_model` section for more information.
