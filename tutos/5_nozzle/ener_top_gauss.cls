!----------------------------------------------------------------------!
!-----------FICHIER DE CONDITION LIMITES SPECIALES MoDeTheC------------!
!----------------------------------------------------------------------!
!
ener_typ_cls = 'gaussien'                                               ! TYPE SPECIAL ENER : LASER FLUX GAUSSIEN
Fmax = 1e6                                                              ! INTENSITE MAX DU FLUX (SUR L'AXE) 187.4e3
omega = 30e-3                                                           ! RAYON DU LASER (DEMI-LARGEUR EN 1/e^2)
tf_gauss = 200                                                           ! temps d'apllication du faisceau
!<---END

