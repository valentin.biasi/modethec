// MAILLAGE D'UN CONE AXI TYPE 
ny = 25*2;
nx = ny * 2;
ncercle = Pi/2 * ny;

// POINTS
Point(1) = {0.035, 0, 0, 1.0};
Point(2) = {0.035, 0.025, 0, 1.0};
Point(3) = {0.025, 0.025, 0, 1.0};
Point(4) = {0, 0, 0, 1.0};
Point(5) = {0.025, 0, 0, 1.0};

// LIGNES
Line(1) = {4, 1};
Line(2) = {1, 2};
Line(3) = {2, 3};
Circle(4) = {3, 5, 4};

// SURFACES
Line Loop(5) = {1, 2, 3, 4};
Plane Surface(6) = {5};
//Transfinite Surface {6};

// RAFFINEMENT DES LIGNES
Transfinite Line {1} = 30 Using Progression 1;
Transfinite Line {2} = 20 Using Progression 1;
Transfinite Line {3} = 9 Using Progression 1;
Transfinite Line {4} = 28 Using Progression 1;

// NOM DES LIGNES ET SURFACES
Physical Line("AXE") = {1};
Physical Line("BASE") = {2};
//Physical Line("EXTERIEUR") = {3};
Physical Line("NEZ") = {4,3};
Physical Surface("INTERIEUR") = {6};


// ALGO DE MAILLAGE
Mesh.Algorithm = 6;
Mesh.Smoothing=4;

