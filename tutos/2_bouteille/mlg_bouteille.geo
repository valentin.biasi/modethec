// MAILLAGE D'UN ''BOUTEILLE'' 2D AXI EN QUAD

db=0.04; // RAYON DE LA BOUTEILLE (BASE)
dh=0.15; // HAUTEUR DE LA BOUTEILLE
conv=0.5; // RATIO HAUTEUR CONVERGENT / HAUTEUR TOTALE
bouch=0.25; // RATIO RAYON BOUCHON / RAYON BASE
nb=16; // NOMBRE DE SOMMETS EN X (BASE)
nh=36; // NOMBRE DE SOMMETS EN Y (HAUTEUR)

// POINTS
Point(1) = {0, 0, 0, 1};
Point(2) = {db, 0, 0, 1};
Point(3) = {0, dh, 0, 1};
Point(4) = {bouch*db, dh, 0, 1};
Point(5) = {0, dh*(1-conv), 0, 1};
Point(6) = {db, dh*(1-conv), 0, 1};
Point(7) = {bouch*db, dh*(1-0.3*conv), 0, 1};
Point(8) = {db, dh*(1-0.7*conv), 0, 1};

// LIGNES DROITES
Line(1) = {1, 2};
Line(2) = {2, 6};
BSpline(3) = {4, 7, 8, 6}; // CONVERGENT
Line(4) = {4, 3};
Line(5) = {3, 5};
Line(6) = {5, 1};
Line(7) = {6, 5};

// SURFACES
Line Loop(8) = {1,2,7,6};
Plane Surface(8) = {8};
Transfinite Surface {8};
Line Loop(9) = {-3,4,5,-7};
Plane Surface(9) = {9};
Transfinite Surface {9};

// RAFFINEMENT DES LIGNES
Transfinite Line {1} = nb Using Progression 1;
Transfinite Line {7} = nb Using Progression 1;
Transfinite Line {4} = nb Using Progression 1;
Transfinite Line {2} = nh*(1-conv) Using Progression 1;
Transfinite Line {6} = nh*(1-conv) Using Progression 1;
Transfinite Line {3} = nh*conv*1.2 Using Progression 1;
Transfinite Line {5} = nh*conv*1.2 Using Progression 1;

// NOM DES LIGNES ET SURFACES
Physical Line("FOND") = {1};
Physical Line("FUT") = {2};
Physical Line("EPAULE") = {3};
Physical Line("SORTIE") = {4};
Physical Line("AXE") = {5,6};
Physical Surface("INTERIEUR") = {8,9};

// ACTIVE NO LIGNE ET POINTS
Geometry.LineNumbers = 1;
Geometry.PointNumbers = 1;
General.Axes = 1;

// ALGO DE MAILLAGE QUAD
Mesh.Algorithm = 8;

Recombine Surface {8,9};
Field[1] = Structured;
