Axisymmetric Bottle
*******************


**Directory:** ``tutos/2_bouteille/``

This case exhibits a heat conduction and gas transport model on a 2D axisymmetric mesh (non-cartesian quadrangle).

It simulates a heating on the side of a bottle filled with sand and air. During the heating, the internal pressure increases and a gaz flow rate carries the hot air to the top of the bottle.

.. _bouteille_tuto:
.. figure:: ../tutos/2_bouteille/bouteille.png
   :width: 30%
   :align: center

   Pressure field and velocity vectors of the axisymmetric bottle simulation


Simulation description:
    + 1 solid species (Sand 70%)
    + 1 gaseous species (Air 30%)
    + Thermal conductivity and heat capacity independant of temperature
    + Mesh with non-orthogonal quads
    + Diffusion time integration scheme: theta-implicit
    + Gas transport time integration scheme: linearized theta-implicit
    + Boundary condition **"AXE"**: Adiabatic and hermetic wall
    + Boundary condition **"FOND"**: Adiabatic and hermetic wall
    + Boundary condition **"FUT"**: Heating at :math:`100kW/m^2`
    + Boundary condition **"EPAULE"**: Radiative heating (:math:`T_{rad} = 293K`)
    + Boundary condition **"SORTIE"**: Pressure atmosphere


.. note:: All axis conditions have to be adiabatic and hermetic wall, as no heat or mass flux could cross those kind of boundaries.

.. note:: Please be careful not to choose a B-Spline for the shoulder too deformed, otherwise the quality mesh falls, and the computation diverges.