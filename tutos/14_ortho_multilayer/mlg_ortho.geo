cl__1 = 1;

dx = 0.05;
dy = 0.05;
dz = 0.002;
nx = 10;
ny = 10;
nz = 6;

Point(1) = {0, 0, 0, 1};
Point(2) = {0, dy, 0, 1};
Point(3) = {dx, dy, 0, 1};
Point(4) = {dx, 0, 0, 1};
Point(5) = {0, 0, dz, 1};
Point(6) = {0, dy, dz, 1};
Point(7) = {dx, dy, dz, 1};
Point(8) = {dx, 0, dz, 1};


Line(1) = {1, 5};
Line(2) = {5, 6};
Line(3) = {5, 8};
Line(4) = {1, 4};
Line(5) = {4, 8};
Line(6) = {4, 3};
Line(7) = {3, 7};
Line(8) = {7, 8};
Line(9) = {7, 6};
Line(10) = {6, 2};
Line(11) = {2, 3};
Line(12) = {2, 1};

Line Loop(14) = {2, 10, 12, 1};
Plane Surface(14) = {14};
Line Loop(16) = {-12, -4, -6, 11};
Plane Surface(16) = {16};
Line Loop(18) = {-6, -7, -8, 5};
Plane Surface(18) = {18};
Line Loop(20) = {8, -3, 2, -9};
Plane Surface(20) = {20};
Line Loop(22) = {3, -5, -4, 1};
Plane Surface(22) = {22};
Line Loop(24) = {7, 9, 10, 11};
Plane Surface(24) = {24};
Surface Loop(26) = {24, 18, 16, 14, 20, 22};
Volume(26) = {26};

//+
Translate {0, 0, dz} {
  Duplicata { Volume{26}; }
}
Translate {0, 0, 2*dz} {
  Duplicata { Volume{26}; }
}
Translate {0, 0, 3*dz} {
  Duplicata { Volume{26}; }
}
Translate {0, 0, 4*dz} {
  Duplicata { Volume{26}; }
}

//+
Transfinite Line {117, 90, 63, 36, 8, 6} = ny Using Progression 1;
//+
Transfinite Line {111, 84, 57, 30, 9, 11} = nx Using Progression 1;
//+
Transfinite Line {125, 98, 71, 44, 2, 12} = ny Using Progression 1;
//+
Transfinite Line {131, 104, 77, 50, 3, 4} = nx Using Progression 1;

Transfinite Line {110, 83, 56, 29, 7, 116, 89, 62, 35, 5, 128, 101, 74, 47, 1, 112, 85, 58, 31, 10} = nz Using Progression 1;

Physical Surface("GAUCHE") = {14,43,70,97,124};
Physical Surface("BAS") = {16};
Physical Surface("DROITE") = {18,33,60,87,114};
Physical Surface("HAUT") = {129};
Physical Surface("AVANT") = {22,53,80,107,134};
Physical Surface("ARRIERE") = {24,28,55,82,109};

Physical Volume("Ply_0") = {26,108};
Physical Volume("Ply_90") = {27,81};
Physical Volume("Intra") = {54};

Transfinite Surface "*";
Transfinite Volume "*";
Mesh.Algorithm = 8;
Recombine Surface "*";