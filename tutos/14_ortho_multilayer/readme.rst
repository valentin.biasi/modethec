Orthotropic multilayers material
********************************


**Directory:** ``tutos/14_ortho_multilayer/``

This case defines a multilayers material, composed of 2 external fibers/resin layers in the X direction, 2 intermediate fibers/resin layers in the Y direction and one central layer composed only of resin. This plate is exposed during 10 seconds to a gaussian LASER beam. The Mori-Tanaka homogenization method is used to model the thermal conductivities.

.. _ortho_multilayer_tuto:
.. figure:: ../tutos/14_ortho_multilayer/ortho.png
   :width: 100%
   :align: center

   Temperature field and thermal conductivities of the multilayers domain


Simulation description:
    + 3 solid species (fibers oriented in the X and in the Y direction, plus a resin)
    + 3 domains are set to define 5 layers
    + Meshes are coincident
    + No interfaces are defined between the domains
    + Boundary condition **"HAUT"**: Imposed gaussien LASER beam
    + All other boundary conditions are adiabatic
    + Thermal conductivities are modeled using the Mori-Tanaka homogenization method

.. note:: This case shows that domains nare not necessarly adjacent. For instance, the upper and the lower layer are in the same physical domain.