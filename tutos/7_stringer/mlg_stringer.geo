cl__1 = 1;

d_patte = 0.02;
d_top = 0.02;
d_st = 0.02;
h_top = 0.028;
e_st = 0.005;
eps_st = 0.6;

nx = 21;
ny = 21;
nz = 14;
ref = 1.1;

Point(1) = {0, 0, 0, 1};
Point(2) = {d_patte, 0, 0, 1};
Point(3) = {d_patte+d_st, h_top, 0, 1};
Point(4) = {d_patte+d_st+d_top, h_top, 0, 1};
Point(5) = {d_patte+2*d_st+d_top, 0, 0, 1};
Point(6) = {2*d_patte+2*d_st+d_top, 0, 0, 1};


Line(8) = {6, 5};
Line(9) = {5, 4};
Line(10) = {4, 3};
Line(11) = {3, 2};
Line(12) = {2, 1};


Point(7) = {0, 0+e_st, 0, 1};
Point(8) = {d_patte-e_st*eps_st, 0+e_st, 0, 1};
Point(9) = {d_patte+d_st-e_st*eps_st, h_top+e_st, 0, 1};
Point(10) = {d_patte+d_st+d_top+e_st*eps_st, h_top+e_st, 0, 1};
Point(11) = {d_patte+2*d_st+d_top+e_st*eps_st, 0+e_st, 0, 1};
Point(12) = {2*d_patte+2*d_st+d_top, 0+e_st, 0, 1};


Line(13) = {12, 11};
Line(14) = {11, 10};
Line(15) = {10, 9};
Line(16) = {9, 8};
Line(17) = {8, 7};
Line(18) = {12, 6};
Line(19) = {7, 1};

Line(20) = {8, 2};
Line(21) = {9, 3};
Line(22) = {10, 4};
Line(23) = {11, 5};

Line Loop(24) = {17, 19, -12, -20};
Plane Surface(25) = {24};
Line Loop(26) = {16, 20, -11, -21};
Plane Surface(27) = {26};
Line Loop(28) = {10, -21, -15, 22};
Plane Surface(29) = {28};
Line Loop(30) = {14, 22, -9, -23};
Plane Surface(31) = {30};
Line Loop(32) = {13, 23, -8, -18};
Plane Surface(33) = {32};


Transfinite Line {17, 12, 13, 8} = 16 Using Progression 1;
Transfinite Line {16, 11, 9, 14} = 21 Using Progression 1;
Transfinite Line {10, 15} = 16 Using Progression 1;
Transfinite Line {19, 20, 21, 22, 23, 18} = 11 Using Progression 1;

Transfinite Surface {25};
Transfinite Surface {27};
Transfinite Surface {29};
Transfinite Surface {31};
Transfinite Surface {33};


Mesh.Algorithm = 8;
Recombine Surface "*";

Extrude {0, 0, 0.025} {
  Surface{25,27,29,31,33};
  Layers{12};
  Recombine;
}


Physical Surface("AVANT") = {33, 31, 29, 27, 25};
Physical Surface("DROITE") = {46};
Physical Surface("ARRIERE") = {55, 77, 99, 121, 143};
Physical Surface("GAUCHE") = {142};
Physical Surface("HAUT") = {130, 108, 94, 64, 42};
Physical Surface("BAS") = {50, 72, 86, 116, 138};

Physical Volume("INTERIEUR") = {5, 4, 3, 2, 1};
