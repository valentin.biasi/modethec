!----------------------------------------------------------------------!
!-----------FICHIER DE CONDITION LIMITES SPECIALES MoDeTheC------------!
!----------------------------------------------------------------------!
!
! Laser Banc DMAE 50W avec ouverture de 20mm et pertes de 8%
ener_typ_cls = 'gaussien'                                               ! TYPE SPECIAL ENER : LASER FLUX GAUSSIEN
Fmax = 150e3                                                            ! INTENSITE MAX DU FLUX (SUR L'AXE) 187.4e3
omega = 20.9e-3                                                         ! RAYON DU LASER (DEMI-LARGEUR EN 1/e^2)
tf_gauss = 300.0														! TEMPS DE LASERISATION...
axe = 0.05 0.032 0.00 0.0 -1.0 0.0                                    ! axe de direction (uniquement en 3D)
!<---END 53.7 / 0.93 = 57.74

