Stringer decomposition
**********************


**Directory:** ``tutos/7_stringer/``

This case exhibits the decomposition of mechnaicalk reinforcment (stringer-shaped section) made of composite material submitted to a gaussian beam (LASER). A pyrolysis and a oxidation reaction are modeled and produce decomposition gases that flow through the material.

.. _stringer_tuto:
.. figure:: ../tutos/7_stringer/stringer.png
   :width: 80%
   :align: center

   Temperature field in the stringer section


Simulation description:
    + 3 solid species (Carbon fibers, Resin and Char)
    + 1 gazeous species (Decomposition gas)
    + Thermal conductivity and heat capacity independant of temperature
    + 3D mesh: non-orthogonal quadrangles
    + A first pyrolysis reaction is set to transform the resin to char and decomposition gases
    + A second oxidation reaction is set to transform the char to decomposition gases
    + Boundary condition **"AVANT"**: Adiabatic and hermetic wall
    + Boundary condition **"HAUT"**: 3D gaussian heat flux, defined in the ``ener_top_gauss.cls`` file
    + All other boundary conditions are at constant pressure and use radiative cooling at :math:`T_{rad}=295K`