// MAILLAGE D'UN RECTANGLE 2D PERCE EN SON CENTRE EN TRIANGLES

dx=0.1; // DIMENSION EN X
dy=0.1; // DIMENSION EN Y
D=0.05; // DIAMETRE DU PERCAGE

// POINTS
Point(1) = {0, 0, 0, 1};
Point(2) = {dx/2, 0, 0, 1};
Point(3) = {dx/2, dy, 0, 1};
Point(4) = {0, dy, 0, 1};
Point(5) = {dx/2, dy/2, 0, 1};
Point(6) = {dx/2, dy/2+D/2, 0, 1};
Point(7) = {dx/2, dy/2-D/2, 0, 1};
Point(8) = {dx/2-D/2*Sin(Pi/4), dy/2-D/2*Cos(Pi/4), 0, 1};
Point(9) = {dx/2-D/2*Sin(Pi/4), dy/2+D/2*Cos(Pi/4), 0, 1};


Point(10) = {dx, 0, 0, 1};
Point(11) = {dx, dy, 0, 1};
Point(12) = {dx/2+D/2*Sin(Pi/4), dy/2-D/2*Cos(Pi/4), 0, 1};
Point(13) = {dx/2+D/2*Sin(Pi/4), dy/2+D/2*Cos(Pi/4), 0, 1};

// LIGNES DROITES
Line(1) = {1, 2};
Line(2) = {2, 7};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line(5) = {6, 3};

// PERCAGE CENTRAL
Circle(6) = {7, 5, 8};
Circle(7) = {8, 5, 9};
Circle(8) = {9, 5, 6};
Line(9) = {8, 1};
Line(10) = {9, 4};

Line(11) = {3, 11};
Line(12) = {11, 10};
Line(13) = {10, 2};
Line(14) = {13, 11};
Line(15) = {12, 10};
Circle(16) = {6, 5, 13};
Circle(17) = {13, 5, 12};
Circle(18) = {12, 5, 7};


// SURFACES
Line Loop(11) = {2, 6, 9, 1};
Plane Surface(12) = {11};
Line Loop(13) = {4, -9, 7, 10};
Plane Surface(14) = {13};
Line Loop(15) = {8, 5, 3, -10};
Plane Surface(16) = {15};

Line Loop(16) = {11, -14, -16, 5};
Plane Surface(17) = {16};
Line Loop(17) = {12, -15, -17, 14};
Plane Surface(18) = {17};
Line Loop(18) = {15, 13, 2, -18};
Plane Surface(19) = {18};
Line Loop(19) = {18, 6, 7, 8, 16, 17};
Plane Surface(20) = {19};


Transfinite Line {1, 6, 8, 3, 13, 18, 16, 11} = 11 Using Progression 1;
Transfinite Line {5, 10, 9, 2, 15, 14} = 12 Using Progression 1;
Transfinite Line {7, 4, 17, 12} = 15 Using Progression 1;

Transfinite Surface {16};
Transfinite Surface {14};
Transfinite Surface {12};
Transfinite Surface {19};
Transfinite Surface {18};
Transfinite Surface {17};

Mesh.Algorithm = 8;

Recombine Surface {12,14,16, 19, 18, 17};


// NOM DES LIGNES ET SURFACES
Physical Line("BOTTOM") = {1, 13};
Physical Line("LEFT") = {4};
Physical Line("TOP") = {3, 11};
Physical Line("RIGHT") = {12};
Physical Surface("LEFT_DOM") = {12, 14, 16};
Physical Surface("RIGHT_DOM") = {17, 18, 19};
Physical Surface("CENTER_DOM") = {20};


