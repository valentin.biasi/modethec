Multi-domains internal coupling
*******************************


**Directory:** ``tutos/13_multidom_2D/``

This case is identical to the ``8_cwipi_2D/`` tutorial but with only internal coupling. The 3 domains are defined inside the mesh file using the "physical tag" function of GMSH. One material is defined for each domain.

.. _multidom_2D_tuto:
.. figure:: ../tutos/13_multidom_2D/multidom_2D.png
   :width: 70%
   :align: center

   Temperature field and mesh of the 3 domains


Simulation description:
    + 3 solid species (one for each domain) with different thermal conductivities
    + Meshes are coincident
    + No interfaces are defined between the domains
    + Boundary condition **"LEFT"**: Imposed temperature :math:`T=400K`
    + Boundary condition **"RIGHT"**: Imposed temperature :math:`T=300K`
    + All other boundary conditions are adiabatic

.. note:: Names of the domains defined in the parameter file have to match the names defined in the mesh file.