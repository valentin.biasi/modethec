// MAILLAGE D'UN HEXA 3D RACCORDE DANS LE PLAN YZ

dx=0.1; // DIMENSION EN X
dy=0.1; // DIMENSION EN Y
dz=0.05; // DIMENSION EN Z
D=0.1; // DIAMETRE DU PERCAGE

// POINTS
Point(1) = {dx, 0, 0, 1};
Point(2) = {dx/2, 0, 0, 1};
Point(3) = {dx/2, dy, 0, 1};
Point(4) = {dy, dy, 0, 1};

Point(5) = {dx/2-D, dy/2, 0, 1};

// LIGNES DROITES
Line(1) = {1, 2};
Line(3) = {3, 4};
Line(4) = {4, 1};

Circle(5) = {3, 5, 2};

Line Loop(6) = {5, -1, -4, -3};
Plane Surface(7) = {6};


Mesh.Smoothing = 100;
Mesh.CharacteristicLengthFactor = 0.05;
Mesh.Algorithm = 6;
Mesh.Algorithm3D = 3;
Mesh.Optimize = 1;
Mesh.OptimizeNetgen = 1;

Extrude {0, 0, dz} {
  Surface{7};
  Layers{7};
  Recombine;
}


/*
Transfinite Line {12, 3, 1, 10} = 10 Using Progression 1;
Transfinite Line {19, 23, 14, 15} = 10 Using Progression 1;
Transfinite Line {11, 4, 5, 9} = 10 Using Progression 1;

Transfinite Surface {29};
Transfinite Surface {24};
Transfinite Surface {28};
Transfinite Surface {20};
Transfinite Surface {16};
Transfinite Surface {7};

Transfinite Volume {1};

Recombine Surface {29,24,28,20,16,7};
*/

Physical Surface("RIGHT") = {24};
Physical Surface("BOTTOM") = {20};
Physical Surface("INTER") = {16};
Physical Surface("TOP") = {28};
Physical Surface("FRONT") = {29};
Physical Surface("REAR") = {7};
Physical Volume("INSIDE") = {1};
