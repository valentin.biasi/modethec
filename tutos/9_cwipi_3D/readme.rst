Heat and mass transfer coupling
*******************************


**Directory:** ``tutos/9_cwipi_3D/``

This case defines a 3D heat and mass coupling (heat flux / temperature + mass flow / pressure) between 2 domains of identical properties.

.. _cwipi_3D_tuto:
.. figure:: ../tutos/9_cwipi_3D/cwipi_3D.png
   :width: 70%
   :align: center

   Temperature field in the 2 coupled domains


Simulation description:
    + 1 solid species for each domain with identical properties
    + Meshes are non-coincident and are of different topologies (octahedron or prism)
    + The interface is courved
    + Boundary condition **"LEFT"**: Imposed temperature :math:`T=400K` and external pressure :math:`P=2 \times 10^5 Pa`
    + Boundary condition **"RIGHT"**: Imposed temperature :math:`T=300K` and external pressure :math:`P=1 \times 10^5 Pa`
    + All other boundary conditions are adiabatic and hermetic
    + All coupled boundaries exchanges heat flux and temperature, pressure and mass flow at fixed time steps.

.. note:: This case is similar to a 1D case. It allows to check the interpolation qualiy aon curved interfaces.