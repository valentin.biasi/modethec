Henderson
*********


**Directory:** ``tutos/4_henderson/``

This case exhibits a thermal decomposition of a glass fiber / epoxy resin composite material. The model takes into account non-linear heat conduction, pyrolysis reaction and gas transport.

.. note:: This case is based on the Henderson et al. article of 1987, that is a reference paper on composite materials decomposition model. [HEN1987]_

.. _henderson_tuto:
.. figure:: ../tutos/4_henderson/henderson.png
   :width: 80%
   :align: center

   Pressure field on the 1D sample


Simulation description:
    + 2 solid species (Virgin and Char)
    + 1 gazeous species (Decomposition gas)
    + Thermal conductivity and heat capacity dependant of temperature
    + 1D mesh: litterally a 2D mesh with only one cell in the orthogonal direction to the simulation direction
    + Diffusion time integration scheme: theta-implicit
    + Gas transport time integration scheme: theta-implicit
    + One reaction is set to transform the virging species to char and decomposition gas
    + Boundary condition **"SIDE"**: In 1D, a side condition have to be defined even if it remains unused. Set to adiabatic and hermetic wall
    + Boundary condition **"LEFT"**: Heat flux at :math:`280kW/m^2`, with radiative cooling and atmospheric pressure
    + Boundary condition **"RIGHT"**: Radiative cooling and atmospheric pressure
