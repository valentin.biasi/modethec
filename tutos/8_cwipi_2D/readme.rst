Multi-domains external coupling
*******************************


**Directory:** ``tutos/8_cwipi_2D/``

This case defines a heat coupling (heat flux / temperature) between 3 domains of different thermal conductivities coupled with 4 non-coincident surfaces.

.. _cwipi_2D_tuto:
.. figure:: ../tutos/8_cwipi_2D/cwipi_2D.png
   :width: 70%
   :align: center

   Temperature field and mesh of the 3 coupled domains


Simulation description:
    + 1 solid species for each domain with different thermal conductivities
    + Meshes are non-coincident and are of different topologies (quandrangle or triangle)
    + 4 interfaces are necessary to coupled all the 3 domains, which two are curved
    + Boundary condition **"LEFT"**: Imposed temperature :math:`T=400K`
    + Boundary condition **"RIGHT"**: Imposed temperature :math:`T=300K`
    + All other boundary conditions are adiabatic
    + All coupled boundaries exchanges heat flux and temperature at fixed time steps.

.. note:: The coupled boundaries configuration (imposed temperature or imposed heat flux) have to be defined relatively to the exchanged data. Please refer to :ref:`external_coupling` for more information.