// MAILLAGE D'UN RECTANGLE 2D PERCE EN SON CENTRE EN TRIANGLES

dx=0.1; // DIMENSION EN X
dy=0.1; // DIMENSION EN Y
D=0.05; // DIAMETRE DU PERCAGE

// POINTS
Point(5) = {dx/2, dy/2, 0, 1};
Point(6) = {dx/2, dy/2+D/2, 0, 1};
Point(7) = {dx/2, dy/2-D/2, 0, 1};


// PERCAGE CENTRAL
Circle(1) = {7, 5, 6};
Circle(2) = {6, 5, 7};

Line Loop(3) = {2, 1};

Plane Surface(4) = {3};


Transfinite Line {2, 1} = 25 Using Progression 1;

Mesh.Algorithm = 8;

// NOM DES LIGNES ET SURFACES
Physical Line("LEFT_CIRCLE") = {2};
Physical Line("RIGHT_CIRCLE") = {1};
Physical Surface("INTERIEUR") = {4};


