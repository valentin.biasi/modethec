// MAILLAGE D'UN RECTANGLE 2D PERCE EN SON CENTRE EN TRIANGLES

dx=0.1; // DIMENSION EN X
dy=0.1; // DIMENSION EN Y
D=0.05; // DIAMETRE DU PERCAGE

// POINTS
Point(1) = {0, 0, 0, 1};
Point(2) = {dx/2, 0, 0, 1};
Point(3) = {dx/2, dy, 0, 1};
Point(4) = {0, dy, 0, 1};
Point(5) = {dx/2, dy/2, 0, 1};
Point(6) = {dx/2, dy/2+D/2, 0, 1};
Point(7) = {dx/2, dy/2-D/2, 0, 1};
Point(8) = {dx/2-D/2*Sin(Pi/4), dy/2-D/2*Cos(Pi/4), 0, 1};
Point(9) = {dx/2-D/2*Sin(Pi/4), dy/2+D/2*Cos(Pi/4), 0, 1};

// LIGNES DROITES
Line(1) = {1, 2};
Line(2) = {2, 7};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line(5) = {6, 3};

// PERCAGE CENTRAL
Circle(6) = {7, 5, 8};
Circle(7) = {8, 5, 9};
Circle(8) = {9, 5, 6};
Line(9) = {8, 1};
Line(10) = {9, 4};

// SURFACES
Line Loop(11) = {2, 6, 9, 1};
Plane Surface(12) = {11};
Line Loop(13) = {4, -9, 7, 10};
Plane Surface(14) = {13};
Line Loop(15) = {8, 5, 3, -10};
Plane Surface(16) = {15};

Symmetry {1, 0, 0, 0} {
  Surface{16, 14, 12};
}
Translate {0.1, 0, 0} {
  Surface{16, 14, 12};
}

Transfinite Line {1, 6, 8, 3} = 10 Using Progression 1;
Transfinite Line {5, 10, 9, 2} = 13 Using Progression 1;
Transfinite Line {7, 4} = 15 Using Progression 1;

Transfinite Surface {16};
Transfinite Surface {14};
Transfinite Surface {12};

Mesh.Algorithm = 8;

Recombine Surface {12,14,16};


// NOM DES LIGNES ET SURFACES
Physical Line("BOTTOM") = {1};
Physical Line("RIGHT") = {4};
Physical Line("TOP") = {3};
Physical Line("MID_BOTTOM") = {2};
Physical Line("MID_TOP") = {5};
Physical Line("RIGHT_CIRCLE") = {6,7,8};
Physical Surface("INTERIEUR") = {12,14,16};

