cl__1 = 1;

dx = 0.04;
dy = 0.04;
dz = 0.00228;
nx = 21;
ny = 21;
nz = 14;
ref = 1.1;

Point(1) = {0, 0, 0, 1};
Point(2) = {0, dy, 0, 1};
Point(3) = {dx, dy, 0, 1};
Point(4) = {dx, 0, 0, 1};



Line(4) = {1, 4};
Line(6) = {4, 3};
Line(11) = {2, 3};
Line(12) = {2, 1};

Line Loop(16) = {-12, -4, -6, 11};
Plane Surface(16) = {16};


/*
Transfinite Line {3,4} = nx Using Progression ref;
Transfinite Line {9,11} = nx Using Progression 1;
Transfinite Line {2} = ny Using Progression ref;
Transfinite Line {12} = ny Using Progression 1/ref;
Transfinite Line {6,8} = ny Using Progression 1;
Transfinite Line {1,5,7,10} = nz Using Progression 1;
*/




//Transfinite Surface {14,20,22,24,16,18};

//Mesh.Algorithm = 8;
//Recombine Surface "*";
Field[1] = Attractor;
Field[1].NodesList = {5, 1};
Background Field = 2;
Field[2] = Threshold;
Field[2].LcMax = 0.1;
Field[2].LcMin = 0.01;
Field[2].IField = 1;
Field[2].LcMax = 0.01;
Field[2].LcMin = 0.001;

Field[2].DistMax = 0.03;
Field[2].DistMin = 0.01;
Field[2].LcMax = 0.002;
Field[2].LcMax = 0.004;

Extrude {0, 0, dz} {
  Surface{16};
  Layers{10};
  Recombine;
}

Transfinite Volume {1};

Physical Surface("GAUCHE") = {25};
Physical Surface("BAS") = {16};
Physical Surface("DROITE") = {33};
Physical Surface("HAUT") = {38};
Physical Surface("AVANT") = {37};
Physical Surface("ARRIERE") = {29};
Physical Volume("INTERIEUR") = {1};
