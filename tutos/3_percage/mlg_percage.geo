// MAILLAGE D'UN RECTANGLE 2D PERCE EN SON CENTRE EN TRIANGLES

dx=0.1; // DIMENSION EN X
dy=0.1; // DIMENSION EN Y
D=0.05; // DIAMETRE DU PERCAGE
nx=21; // NOMBRE DE SOMMETS EN X
ny=dy*nx/dx; // NOMBRE DE SOMMETS EN Y
nd=Pi*D*nx/dx; // NOMBRE DE SOMMETS EN PERCAGE CENTRAL

// POINTS
Point(1) = {0, 0, 0, 1};
Point(2) = {dx, 0, 0, 1};
Point(3) = {dx, dy, 0, 1};
Point(4) = {0, dy, 0, 1};
Point(5) = {dx/2, dy/2, 0, 1};
Point(6) = {dx/2+D/2, dy/2, 0, 1};
Point(7) = {dx/2-D/2, dy/2, 0, 1};

// LIGNES DROITES
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

// PERCAGE CENTRAL
Circle(5) =  {6,5,7};
Circle(6) =  {7,5,6};

// SURFACES
Line Loop(7) = {5, 6};
Line Loop(8) = {3, 4, 1, 2};
Plane Surface(9) = {7, 8};

// RAFFINEMENT DES LIGNES
Transfinite Line {1} = nx Using Progression 1;
Transfinite Line {2} = ny Using Progression 1;
Transfinite Line {3} = nx Using Progression 1;
Transfinite Line {4} = ny Using Progression 1;
Transfinite Line {5} = nd/2 Using Progression 1;
Transfinite Line {6} = nd/2 Using Progression 1;

// NOM DES LIGNES ET SURFACES
Physical Line("BOTTOM") = {1};
Physical Line("RIGHT") = {2};
Physical Line("TOP") = {3};
Physical Line("LEFT") = {4};
Physical Line("PERCAGE") = {5,6};
Physical Surface("INTERIEUR") = {9};

// ACTIVE NO LIGNE ET POINTS
Geometry.LineNumbers = 1;
Geometry.PointNumbers = 1;
General.Axes = 1;

// ALGO DE MAILLAGE
Mesh.Algorithm=6; //Frontal
Mesh.Smoothing=5;
