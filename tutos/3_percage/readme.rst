Drilled square
**************


**Directory:** ``tutos/3_percage/``

This case exhibits a heat conduction and themo-mechanical stress and deformation of drilled 2D square of aluminium.

It simulates a heating on the upper side of an aluminium square at 100kW/m2 and the cooling of the bottom side at 20°C. Both lateral sides are constraineded while others boundary conditions are free.

.. _percage_tuto:
.. figure:: ../tutos/3_percage/percage.png
   :width: 60%
   :align: center

   Temperature field of the deformed drilled square


Simulation description:
    + 1 solid species (Aluminium)
    + Thermal conductivity and heat capacity independant of temperature
    + Meshed with triangles
    + Diffusion time integration scheme: theta-implicit
    + Gas transport time integration scheme: linearized theta-implicit
    + Boundary condition **"BOTTOM"**: Imposed temperature at 20°C and mechanically unconstrained
    + Boundary condition **"TOP"**: Heating at :math:`100kW/m^2` and mechanically unconstrained
    + Boundary condition **"RIGHT"**: Adiabatic wall + fixed position
    + Boundary condition **"LEFT"**: Adiabatic wall + fixed position
    + Boundary condition **"PERCAGE"**: Adiabatic wall + mechanically unconstrained


.. note:: The output mesh displacement are amplified with a 500x coefficient. This is only for visualization purposes.