!----------------------------------------------------------------------!
!-----------FICHIER DE PARAMETRES POUR EXECUTION DE MoDeTheC-----------!
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
! --> PARAM GENERAUX --------------------------------------------------!
version = '2018.2.0'                                                   ! MoDeTheC version
auteur = 'Nicolas Perron'                                              ! Author(s)
file_mesh = 'plaque2D.msh'                                             ! Mesh filename
dim_simu = '2D_PLAN'                                                   ! Simulation dimension (3D, 2D_PLANAR, 2D_AXI, 1D)
AXE_2D = 0.0 0.0 0.0 0.0 0.0 0.0                                       ! Symmetry axis: origin [x0 y0 z0], unitary orientation vector [ux uy uz]
DIFFUSION = 'on'                                                       ! Activate heat diffusion solver
ADVECTION = 'off'                                                      ! Activate gas advection solver
REACTION = 'off'                                                       ! Activate reaction kinetics solver
STRUCTURE = 'off'                                                      ! Activate static mechanics solver
ABLATION = 'on'                                                        ! Activate ALE ablation solver
COUPLAGE = 'off'                                                       ! Activate external coupling solver
!
!----------------------------------------------------------------------!
! --> PARAM PHYSIQUE --------------------------------------------------!
ndom = 1                                                               ! Total number of physical domains
nlim = 4                                                               ! Total number of boundary conditions
nesp = 1                                                               ! Number of solid and gas species
ngaz = 0                                                               ! Number of gas species
nsol = 1                                                               ! Number of solid species
ID_gaz =                                                               ! Gas species ID
ID_sol = 1                                                             ! Solid species ID
nreac = 0                                                              ! Number of reactions
atmo = 'air'                                                           ! Surrounding atmosphere type (air, vacuum, O2, N2)
satur_T = 0                                                            ! Activate properties saturation above temperature threshold "T_sat"
T_sat = 0.0                                                            ! Temperature threshold for properties saturation [K]
k_homog = 'melange'                                                    ! Homogenization method for thermal conductivity tensor (law of mixture, Mori-Tanaka)
Kp_typ = 'constant'                                                    ! Homogenization method for permeability tensor (constant, Henderson, Kozeny, Kozeny-ortho)
Kp_ini = 0.0                                                           ! Isotropic permeability value of the medium (constant or Henderson initial) [m2]
Kp_fin = 0.0                                                           ! Isotropic permeability value of the medium (Henderson final) [m2]
Kp_0 = 0.0                                                             ! Permeability value of the medium (Kozeny-Karman isotropic model) [m2]
Kp_x = 0.0                                                             ! Permeability value of the medium along X-direction (Kozeny-Karman orthotropic model) [m2]
Kp_y = 0.0                                                             ! Permeability value of the medium along Y-direction (Kozeny-Karman orthotropic model) [m2]
Kp_z = 0.0                                                             ! Permeability value of the medium along Z-direction (Kozeny-Karman orthotropic 3D model) [m2]
Kp_max = 0.0                                                           ! Permeability value of the medium (maximum threshold value) [m2]
mu_typ = 'constant'                                                    ! Gas phase dynamic viscosity model
mu_cst = 0.0                                                           ! Gas phase dynamic viscosity constant value (no temperature-dependency assumption) [Pa.s]
mu_nb_poly = 2                                                         ! Number of polynomial coefficients for gas phase dynamic viscosity expression as a function of temperature
mu_poly = 0.0 0.0                                                      ! Polynomial coefficients values for gas phase dynamic viscosity expression as a function of temperature
T_abla = 933.52                                                        ! Fusion temperature for ablation model [K]
h_abla = 10670.0                                                       ! Fusion mass enthalpy for ablation model [J/kg]
meca_typ = 'isotrope'                                                  ! Mechanical properties model (isotropic, transverse)
E_iso = 0.0                                                            ! Isotropic Young Modulus [Pa]
P_iso = 0.0                                                            ! Isotropic Poisson coefficient
beta_iso = 0.0                                                         ! Isotropic thermal expansion coefficient [1/K]
E_x = 0.0                                                              ! Young Modulus along X-direction (orthotropic model) [Pa]
E_y = 0.0                                                              ! Young Modulus along Y-direction (orthotropic model) [Pa]
P_xy = 0.0                                                             ! Planar XY Poisson coefficient (orthotropic model)
G_xy = 0.0                                                             ! Transverse Z shear modulus (orthotropic model)
beta_x = 0.0                                                           ! Longitudinal thermal expansion coefficient along X-direction [1/K]
beta_y = 0.0                                                           ! Transverse thermal expansion coefficient along Y-direction [1/K]
!
!----------------------------------------------------------------------!
! --> PARAM ESPECES ---------------------------------------------------!
!
! --> ESPECE no 1 -----------------------------------------------------!
    id_esp = 1                                                         ! Species ID
    nom_esp = 'Aluminium'                                              ! Species name
    typ_esp = 'solide'                                                 ! Species phase (solid or gas)
    rho_typ = 'constant'                                               ! Species density model
    rho_cst = 2.7                                                      ! Species constant density value (no temperature-dependency assumption) [kg/m3]
    rho_nb_poly = 2                                                    ! Number of polynomial coefficients for species density expression as a function of temperature
    rho_poly = 0.0 0.0                                                 ! Polynomial coefficients values for species density expression as a function of temperature
    Cp_typ = 'constant'                                                ! Species specific heat model
    Cp_cst = 940.0                                                     ! Species constant specific heat (no temperature-dependency assumption) [J/kg/K]
    Cp_nb_poly = 2                                                     ! Number of polynomial coefficients for specific heat expression as a function of temperature
    Cp_poly = 0.0 0.0                                                  ! Polynomial coefficients values for specific heat expression as a function of temperature
    k_typ = 'ortho'                                                    ! Species thermal conductivity tensor model
    k_cst = 237.0                                                      ! Species constant thermal conductivity (isotropic + no temperature-dependency assumptions) [W/m/K]
    k_nb_poly = 2                                                      ! Number of polynomial coefficients for species thermal conductivity expression as a function of temperature (isotropic assumption)
    k_poly = 0.0 0.0                                                   ! Polynomial coefficients values for species thermal conductivity expression as a function of temperature (isotropic assumption)
    k_nb_otho = 1                                                      ! Number of polynomial coefficients for species thermal conductivity tensor expression as a function of temperature (orthotropic model)
    k_ortho_x = 237.0                                                  ! Polynomial coefficients values for species thermal conductivity tensor expression as a function of temperature along X-direction (orthotropic model)
    k_ortho_y = 237.0                                                  ! Polynomial coefficients values for species thermal conductivity tensor expression as a function of temperature along Y-direction (orthotropic model)
    k_ortho_z = 237.0                                                  ! Polynomial coefficients values for species thermal conductivity tensor expression as a function of temperature along Z-direction (orthotropic model)
    k_Eshelby = 0.0 0.0 0.0                                            ! Diagonal coefficients of Eshelby tensor for ellipsoid inclusions
    M = 1.0                                                            ! Gas species molar mass [kg/mol]
    eps = 0.9                                                          ! Surface emissivity (solid species)
    alpha = 0.9                                                        ! Surface absorptivity (solid species)
!
!----------------------------------------------------------------------!
! --> PARAM REACTIONS -------------------------------------------------!
!
!----------------------------------------------------------------------!
! --> PARAM NUMERIQUE -------------------------------------------------!
t0 = 0.0                                                               ! Simulation initial time [s]
tf = 0.01                                                              ! Simulation final time [s]
dt = 5e-06                                                             ! Simulation global time step [s]
schema_A = 'theta-lin'                                                 ! Time integration numerical method for advection operator solver
schema_DR = 'theta-imp'                                                ! Time integration numerical method for diffusion/reaction operator solver
schema_S = 'implicite'                                                 ! Integration numerical method for static mechanics operator solver
solveur_LIN = 'PARDISO'                                                ! Space integration linear solver (PARDISO, GMRES)
theta_imp = 1.0                                                        ! Theta value for theta-implicit (linearized) method
delta_max = 1.0                                                        ! Variation limit of conservative variables for each iteration
CFL_max = 1.0                                                          ! Maximum CFL criterion for advection operator solver (linearized theta-implicit scheme)
slope_limiter = 'van_leer'                                             ! Flux limiter type for advection fluxes - TVD scheme (upwind, downwind, central, sou, minmod, superbee, van_leer, van_albada, ospre)
tol_abs = 1e-06                                                        ! Absolute convergence criterion for A+D/R operators solvers
tol_rel = 1e-06                                                        ! Relative convergence criterion for A+D/R operators solvers
tol_int = 0.1                                                          ! Internal iteration convergence criterion for linear solver
niter_max = 50                                                         ! Maximum number of non-linear iterations for each time step
niter_max_int = 10                                                     ! Maximum number of linear iterations for each time step
CFL_AB = 0.5                                                           ! Maximum CFL criterion for ALE ablation solver (mesh deformation)
pas_ech = 10                                                           ! Sub-sampling maximum point number for ablation boundaries
niter_def = 50                                                         ! Mesh displacement iteration number for ablation solver (Laplacian operator)
tol_def = 1e-07                                                        ! Mesh displacement iteration convergence criterion for ablation solver
nvois_def = 3                                                          ! Number of neighbors for ablation smoothing
dt_couplage = 0.0                                                      ! Coupling time step [s]
advanced_num_prm = 0                                                   ! Activate advanced numerical parameters (in the GUI)
restart_backup = 0                                                     ! Restart from last backup computation
!
!----------------------------------------------------------------------!
! --> PARAM CONDITIONS INITIALES --------------------------------------!
!
! --> DOMAINE no 1 ----------------------------------------------------!
    id_dom = 1                                                         ! Physical domain ID
    nom_dom = 'SURF'                                                   ! Physical domain name (please respect mesh domain marks)
    Tini = 900.0                                                       ! Domain initial temperature [K]
    Pini = 101325.0                                                    ! Domain initial pressure [Pa]
    phi_ini = 1.0                                                      ! Initial species mass fractions (please respect ID order)
!
!----------------------------------------------------------------------!
! --> PARAM CONDITIONS LIMITES ----------------------------------------!
!
! --> LIMITE no 1 -----------------------------------------------------!
    id_lim = 1                                                         ! Boundary condition ID
    nom_lim = 'COTE1'                                                  ! Boundary condition name (please respect mesh boundary marks)
    ener_typ = 'mixte'                                                 ! Thermal boundary condition type (heat flux, temperature, combined HF+conv+rad)
    Fimp = 0.0                                                         ! Imposed heat flux (for heat flux or combined HF+conv+rad BC types) [W/m2]
    Timp = 293.0                                                       ! Imposed temperature (for temperature BC type) [K]
    T_conv = 800.0                                                     ! Convective heat transfer temperature (for combined HF+conv+rad BC type) [K]
    h_conv = 0.0                                                       ! Convective heat transfer coefficient (for combined HF+conv+rad BC type) [W/m2/K]
    T_rad = 800.0                                                      ! Radiative heat transfer temperature (for combined HF+conv+rad BC type) [K]
    masse_typ = 'debit'                                                ! Mass flow boundary condition type (external pressure, surface mass flow rate)
    Dimp = 0.0                                                         ! Imposed surface mass flow rate [kg/m2/s]
    Pimp = 101325.0                                                    ! Imposed external pressure [Pa]
    struct_typ = 'contrainte'                                          ! Mechanical boundary type (constrained, stress, symmetry axis)
    stress_imp = 0.0 0.0                                               ! Imposed normal and tangential mechanical stress [Pa]
    abla_typ = 'temp'                                                  ! Ablation boundary type (fusion temperature, ablation velocity)
    Vreg_imp = 0.0                                                     ! Ablation imposed velocity [m/s]
    use_cls = 0                                                        ! Activate user-specified boundary condition [ON(1) - OFF(0)]
    file_cls = ''                                                      ! User-specified boundary condition file
    couplage_lim = 0                                                   ! Boundary external coupling activation (ON[1] - OFF[0])
    application_couplage = ''                                          ! Application name (or domain name) to be coupled via CWIPI (mesh filename for modethec/CWIPI/modethec coupling)
    nvar_isend_coupling = 0                                            ! Number of variables to be sent to external coupling
    nvar_irecv_coupling = 0                                            ! Number of variables to be received from external coupling
!
! --> LIMITE no 2 -----------------------------------------------------!
    id_lim = 2                                                         ! Boundary condition ID
    nom_lim = 'COTE2'                                                  ! Boundary condition name (please respect mesh boundary marks)
    ener_typ = 'mixte'                                                 ! Thermal boundary condition type (heat flux, temperature, combined HF+conv+rad)
    Fimp = 0.0                                                         ! Imposed heat flux (for heat flux or combined HF+conv+rad BC types) [W/m2]
    Timp = 293.0                                                       ! Imposed temperature (for temperature BC type) [K]
    T_conv = 800.0                                                     ! Convective heat transfer temperature (for combined HF+conv+rad BC type) [K]
    h_conv = 0.0                                                       ! Convective heat transfer coefficient (for combined HF+conv+rad BC type) [W/m2/K]
    T_rad = 800.0                                                      ! Radiative heat transfer temperature (for combined HF+conv+rad BC type) [K]
    masse_typ = 'debit'                                                ! Mass flow boundary condition type (external pressure, surface mass flow rate)
    Dimp = 0.0                                                         ! Imposed surface mass flow rate [kg/m2/s]
    Pimp = 101325.0                                                    ! Imposed external pressure [Pa]
    struct_typ = 'contrainte'                                          ! Mechanical boundary type (constrained, stress, symmetry axis)
    stress_imp = 0.0 0.0                                               ! Imposed normal and tangential mechanical stress [Pa]
    abla_typ = 'temp'                                                  ! Ablation boundary type (fusion temperature, ablation velocity)
    Vreg_imp = 0.1                                                     ! Ablation imposed velocity [m/s]
    use_cls = 0                                                        ! Activate user-specified boundary condition [ON(1) - OFF(0)]
    file_cls = 'ener_top_gauss.cls'                                    ! User-specified boundary condition file
    couplage_lim = 0                                                   ! Boundary external coupling activation (ON[1] - OFF[0])
    application_couplage = ''                                          ! Application name (or domain name) to be coupled via CWIPI (mesh filename for modethec/CWIPI/modethec coupling)
    nvar_isend_coupling = 0                                            ! Number of variables to be sent to external coupling
    nvar_irecv_coupling = 0                                            ! Number of variables to be received from external coupling
!
! --> LIMITE no 3 -----------------------------------------------------!
    id_lim = 3                                                         ! Boundary condition ID
    nom_lim = 'BAS'                                                    ! Boundary condition name (please respect mesh boundary marks)
    ener_typ = 'mixte'                                                 ! Thermal boundary condition type (heat flux, temperature, combined HF+conv+rad)
    Fimp = 0.0                                                         ! Imposed heat flux (for heat flux or combined HF+conv+rad BC types) [W/m2]
    Timp = 293.0                                                       ! Imposed temperature (for temperature BC type) [K]
    T_conv = 800.0                                                     ! Convective heat transfer temperature (for combined HF+conv+rad BC type) [K]
    h_conv = 0.0                                                       ! Convective heat transfer coefficient (for combined HF+conv+rad BC type) [W/m2/K]
    T_rad = 800.0                                                      ! Radiative heat transfer temperature (for combined HF+conv+rad BC type) [K]
    masse_typ = 'debit'                                                ! Mass flow boundary condition type (external pressure, surface mass flow rate)
    Dimp = 0.0                                                         ! Imposed surface mass flow rate [kg/m2/s]
    Pimp = 101325.0                                                    ! Imposed external pressure [Pa]
    struct_typ = 'contrainte'                                          ! Mechanical boundary type (constrained, stress, symmetry axis)
    stress_imp = 0.0 0.0                                               ! Imposed normal and tangential mechanical stress [Pa]
    abla_typ = 'temp'                                                  ! Ablation boundary type (fusion temperature, ablation velocity)
    Vreg_imp = 0.15                                                    ! Ablation imposed velocity [m/s]
    use_cls = 0                                                        ! Activate user-specified boundary condition [ON(1) - OFF(0)]
    file_cls = 'ener_top_gauss.cls'                                    ! User-specified boundary condition file
    couplage_lim = 0                                                   ! Boundary external coupling activation (ON[1] - OFF[0])
    application_couplage = ''                                          ! Application name (or domain name) to be coupled via CWIPI (mesh filename for modethec/CWIPI/modethec coupling)
    nvar_isend_coupling = 0                                            ! Number of variables to be sent to external coupling
    nvar_irecv_coupling = 0                                            ! Number of variables to be received from external coupling
!
! --> LIMITE no 4 -----------------------------------------------------!
    id_lim = 4                                                         ! Boundary condition ID
    nom_lim = 'HAUT'                                                   ! Boundary condition name (please respect mesh boundary marks)
    ener_typ = 'mixte'                                                 ! Thermal boundary condition type (heat flux, temperature, combined HF+conv+rad)
    Fimp = 0.0                                                         ! Imposed heat flux (for heat flux or combined HF+conv+rad BC types) [W/m2]
    Timp = 293.0                                                       ! Imposed temperature (for temperature BC type) [K]
    T_conv = 800.0                                                     ! Convective heat transfer temperature (for combined HF+conv+rad BC type) [K]
    h_conv = 0.0                                                       ! Convective heat transfer coefficient (for combined HF+conv+rad BC type) [W/m2/K]
    T_rad = 800.0                                                      ! Radiative heat transfer temperature (for combined HF+conv+rad BC type) [K]
    masse_typ = 'debit'                                                ! Mass flow boundary condition type (external pressure, surface mass flow rate)
    Dimp = 0.0                                                         ! Imposed surface mass flow rate [kg/m2/s]
    Pimp = 101325.0                                                    ! Imposed external pressure [Pa]
    struct_typ = 'contrainte'                                          ! Mechanical boundary type (constrained, stress, symmetry axis)
    stress_imp = 0.0 0.0                                               ! Imposed normal and tangential mechanical stress [Pa]
    abla_typ = 'temp'                                                  ! Ablation boundary type (fusion temperature, ablation velocity)
    Vreg_imp = 0.0                                                     ! Ablation imposed velocity [m/s]
    use_cls = 1                                                        ! Activate user-specified boundary condition [ON(1) - OFF(0)]
    file_cls = 'ener_top_gauss.cls'                                    ! User-specified boundary condition file
    couplage_lim = 0                                                   ! Boundary external coupling activation (ON[1] - OFF[0])
    application_couplage = ''                                          ! Application name (or domain name) to be coupled via CWIPI (mesh filename for modethec/CWIPI/modethec coupling)
    nvar_isend_coupling = 0                                            ! Number of variables to be sent to external coupling
    nvar_irecv_coupling = 0                                            ! Number of variables to be received from external coupling
!
!----------------------------------------------------------------------!
! --> PARAM SUIVI -----------------------------------------------------!
suivi = 1                                                              ! Activate terminal history [ON(1) - OFF(0)]
file_log = 1                                                           ! Record log file [ON(1) - OFF(0)]
dt_prt = 0.001                                                         ! Time step of on-screen prints (multiple of global time step)
file_hdf5 = 1                                                          ! Export integrated variables [HDF5 file format]
dt_hdf5 = 0.001                                                        ! Time step of HDF5 export history
sensors = 0                                                            ! Export variables at specific coordinates [ON(1) - OFF(0)]
nsensors = 4                                                           ! Number of sensors for specific outputs
    coord_sensors = 0.0         0.0       0.4                          !
    coord_sensors = 0.0         0.0       0.3                          !
    coord_sensors = 0.0         0.0       0.15                         !
    coord_sensors = 0.0         0.1       0.3                          !
    coord_sensors = 0.0         0.0       0.0                          !
backup = 0                                                             ! Activate backup output
dt_backup = 0.01                                                       ! Time step between two backup outputs [s]
!
!----------------------------------------------------------------------!
! --> PARAM EXPORT ----------------------------------------------------!
export = 1                                                             ! Save export file [ON(1) - OFF(0)]
nom_exp = 'out_plaque2D'                                               ! Export filename
dt_exp = 0.0001                                                        ! Time step of export file (multiple of global time step)
exp_ini = 1                                                            ! Export initial state t=0 [ON(1) - OFF(0)]
deform_mlg = 0                                                         ! Mesh deformation for mechanical solver [ON(1) - OFF(0)]
deform_amp = 1.0                                                       ! Mesh deformation magnitude factor
FEmode = 1                                                             ! Finite Elements Tecplot mode [ON(1):Cell connectivity - OFF(0):Scatter mode]
tecplot_bin = 1                                                        ! Generate Tecplot binary file [ON(1) - OFF(0)]
tecplot_ascii = 0                                                      ! Generate Tecplot ASCII file [ON(1) - OFF(0)]
vtk_ascii = 0                                                          ! Generate VTK ASCII file [ON(1) - OFF(0)]
sommet = 0                                                             ! Export node-centered variables
cellule = 1                                                            ! Export cell-centered variables
volume = 0                                                             ! Export volume results
surface = 0                                                            ! Export surface results
nexpt = 3                                                              ! Number of variables to be exported
    var_exp = 'T'              1    'Temperature [K]'                  !
    var_exp = 'm'              1    'Masse'                            !
    var_exp = 'E'              1    'Energie'                          !
    var_exp = 'r_courbure'     1    'R_courb'                          !
!----------------------------------------------------------------------!
! <-- END
