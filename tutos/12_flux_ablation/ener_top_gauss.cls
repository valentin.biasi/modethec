!----------------------------------------------------------------------!
!-----------FICHIER DE CONDITION LIMITES SPECIALES MoDeTheC------------!
!----------------------------------------------------------------------!
!
ener_typ_cls = 'directionnel' gaussien'                                               ! TYPE SPECIAL ENER : LASER FLUX GAUSSIEN
Fmax = 5e5                                                              ! INTENSITE MAX DU FLUX (SUR L'AXE) 187.4e3
omega = 0.05                                                           ! RAYON DU LASER (DEMI-LARGEUR EN 1/e^2)
tf_gauss = 200                                                           ! temps d'apllication du faisceau
axe = 0.0 0.20 0.0 0.0 -1.0 0.0 
!<---END

