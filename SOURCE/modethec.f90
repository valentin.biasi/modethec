!> \brief Source programme du solveur MoDeTheC                         !
!        __  __  ___  ___  ___ _____ _  _ ___ ___                      !
!       |  \/  |/ _ \|   \| __|_   _| || | __/ __|                     !
!       | |\/| | (_) | |) | _|  | | | __ | _| (__                      !
!       |_|  |_|\___/|___/|___| |_| |_||_|___\___|                     !
!                                                                      !
!        MODELISATION                                                  !
!               de la DEGRADATION THERMIQUE                            !
!                                   des COMPOSITES                     !
!                                                                      !
! Pour la documentation, se référer au document doc/html/index.html    !
!                                                                      !
! -> ARGUMENT :                                                        !
!           file_PARAM : fichier de paramètres                         !
! -> OUTPUT :                                                          !
!           DOM : Structure de données internes                        !
!> @author Valentin BIASI
PROGRAM modethec
!
USE MPI
!
USE mod_prelim, ONLY : prelim
USE mod_mesh, ONLY : mesh
USE mod_dat, ONLY : dat
USE mod_cyc, ONLY : cyc
!
USE mod_dom
!
IMPLICIT NONE
!#include "petsc/finclude/petscvec.h"
!#include "petsc/finclude/petscvec.h90"
!#include <petsc/finclude/petscsys.h>
!#include <petsc/finclude/petscmat.h>
!#include <petsc/finclude/petscksp.h>
!#include <petsc/finclude/petscpc.h>
!#include <petsc/finclude/petscsnes.h>
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER :: ierr

! Initialisation MPI
CALL MPI_INIT( ierr )

!----------------------------------------------------------------------!
! --> Phase 1 : Préliminaire
CALL prelim( DOM )

!----------------------------------------------------------------------!
! --> Phase 2 : Reconstruction du maillage
CALL mesh( DOM )

!----------------------------------------------------------------------!
! --> Phase 3 : Préparation des données
CALL dat( DOM )

!----------------------------------------------------------------------!
! --> Phase 4 : Résolution du systeme
CALL cyc( DOM )

! Finalisation MPI
CALL MPI_FINALIZE( ierr )


END PROGRAM modethec
!--- Fin du programme -------------------------------------------------!