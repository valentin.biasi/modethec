!> Bibliotheque de fonctions d'export des resultats
MODULE mod_expt_lib
!
USE mod_cst, ONLY : IP, DP, eps_min, P_ref
USE mod_print, ONLY : print_err
USE mod_algebra, ONLY : inttostr
USE mod_interpolate, ONLY : grad_cel, interp_som
!
USE mod_dom
USE mod_mlg
USE mod_cdt
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Interpolations aux sommets du champ U aux cellules : U_som <- U_cel.
!> Utilisable pour l'export seulement (pas pour le calcul)
SUBROUTINE expt_interp_som( U_cel, ncel, U_som, nsom, MLG )
!
!---- Arguments -------------------------------------------------------!
TYPE (typ_mlg), INTENT(IN) :: MLG
REAL(DP), DIMENSION(ncel), INTENT(IN) :: U_cel
INTEGER(IP), INTENT(IN) :: ncel
REAL(DP), DIMENSION(nsom), INTENT(OUT) :: U_som
INTEGER(IP), INTENT(IN) :: nsom
!
! Valeurs interpolées aux sommets
CALL interp_som( U_cel, U_som, ncel, nsom, MLG%CEL_ITP(:), MLG%SOM_ITP(:), MLG%dim_simu )
!
!
END SUBROUTINE expt_interp_som
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Interpolations aux sommets du champ de temperature aux cellules
!> \warning Utilisable pour l'export seulement
SUBROUTINE expt_interp_T( U_cel, ncel, U_som, nsom, MLG, CDT )
!
!---- Arguments -------------------------------------------------------!
TYPE (typ_cdt), INTENT(IN) :: CDT
TYPE (typ_mlg), INTENT(IN) :: MLG
INTEGER(IP), INTENT(IN):: ncel
REAL(DP), DIMENSION(ncel), INTENT(IN) :: U_cel
REAL(DP), DIMENSION(nsom), INTENT(OUT) :: U_som
INTEGER(IP), INTENT(IN) :: nsom
!
!
!---- Déclarations ----------------------------------------------------!
INTEGER(IP) :: ilim, nlim
CHARACTER (len=32) :: ener_typ
INTEGER(IP) :: nfac_limi
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_FACLIMi
INTEGER(IP) :: id_fac, id_som, id_cel
INTEGER(IP) :: isom, nsom_fac
INTEGER(IP) :: ifaclimi
!
!---- Récupérations ---------------------------------------------------!
nlim = CDT%nlim
nsom_fac = 2
!
!
! Valeurs interpolées aux sommets
CALL interp_som( U_cel, U_som, ncel, nsom, MLG%CEL_ITP(:), MLG%SOM_ITP(:), MLG%dim_simu )
!
!
! Boucle par limite
DO ilim = 1,nlim
    
    ! Nombre de faces par limite
    nfac_limi = CDT%LIM(ilim)%nfac_limi
    
    ! Tableaux locaux des ID des faces associées
    ALLOCATE( ID_FACLIMi(nfac_limi) )
    ID_FACLIMi = CDT%LIM(ilim)%ID_FACLIMi
    
    ! Suivant le type de limite ener. ...
    ener_typ = CDT%LIM(ilim)%ener_typ
    SELECT CASE (ener_typ)

        !... si flux nul => gradient nul => T_som=T_cel
        CASE ("flux")
            
            DO ifaclimi = 1,nfac_limi
                
                IF (CDT%LIM(ilim)%Fimp(ifaclimi) <= 2.0_dp*eps_min) THEN

                    ! Face et cellules de ifaclimi
                    id_fac = ID_FACLIMi(ifaclimi)
                    id_cel = MLG%FAC2CEL(id_fac,3)
                    
                    ! Les 2 sommets de chaque face à T_cel
                    DO isom = 1,nsom_fac
                        id_som = MLG%FAC(id_fac,isom+2)
                        
                        ! Cherche la bonne temperature
                        U_som(id_som) = U_cel(id_cel)
                    
                    END DO  

                    END IF

            END DO
    
        ! Sinon rien ...
        CASE DEFAULT

    END SELECT

    DEALLOCATE( ID_FACLIMi ) ! Désallocation pour nouvelle limite
    
END DO

END SUBROUTINE expt_interp_T
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Interpolations aux sommets du champ de deplacement solide aux cellules
!> \warning Utilisable pour l'export seulement
!> \warning Applicable en 2D seulement
SUBROUTINE expt_interp_struct( U_cel, ncel, U_som, nsom, MLG, CDT )
!
!---- Arguments -------------------------------------------------------!
TYPE (typ_cdt), INTENT(IN) :: CDT
TYPE (typ_mlg), INTENT(IN) :: MLG
INTEGER(IP), INTENT(IN):: ncel
REAL(DP), DIMENSION(ncel,3), INTENT(IN) :: U_cel
REAL(DP), DIMENSION(nsom,3), INTENT(OUT) :: U_som
INTEGER(IP), INTENT(IN) :: nsom
!
!
!---- Déclarations ----------------------------------------------------!
INTEGER(IP) :: ilim, nlim
CHARACTER (len=32) :: struct_typ
INTEGER(IP) :: nfac_limi
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_FACLIMi
INTEGER(IP) :: id_fac, id_som
INTEGER(IP) :: isom, nsom_fac
INTEGER(IP) :: ifaclimi
REAL(DP) :: angle_AXE, U_r_som
!
!---- Récupérations ---------------------------------------------------!
nlim = CDT%nlim
nsom_fac = 2
!
!
!--- Suivant X
! Valeurs interpolées aux sommets
CALL interp_som( U_cel(:,1), U_som(:,1), ncel, nsom, MLG%CEL_ITP(:), MLG%SOM_ITP(:), MLG%dim_simu )
!
!
!--- Suivant Y
! Valeurs interpolées aux sommets
CALL interp_som( U_cel(:,2), U_som(:,2), ncel, nsom, MLG%CEL_ITP(:), MLG%SOM_ITP(:), MLG%dim_simu )
!
!
!--- Suivant Z
U_som(:,3) = 0.0_dp
!
! Boucle par limite
DO ilim = 1,nlim
    
    ! Nombre de faces par limite
    nfac_limi = CDT%LIM(ilim)%nfac_limi
    
    ! Tableaux locaux des ID des faces associées
    ALLOCATE( ID_FACLIMi(nfac_limi) )
    ID_FACLIMi = CDT%LIM(ilim)%ID_FACLIMi
    
    ! Suivant le type de limite struct. ...
    struct_typ = CDT%LIM(ilim)%struct_typ
    SELECT CASE (struct_typ)

        !... on fixe la limite
        CASE ("fixe")
            DO ifaclimi = 1,nfac_limi
                id_fac = ID_FACLIMi(ifaclimi)
                
                ! Les 2 sommets de chaque face
                DO isom = 1,nsom_fac
                    id_som = MLG%FAC(id_fac,isom+2)
                    
                    ! Mis a zero
                    U_som(id_som,1) = 0.0_dp
                    U_som(id_som,2) = 0.0_dp
                
                END DO  
            END DO
    
        !... on aligne suivant l'axe
        CASE ("axe")
            ! angle entre X et l'axe
            angle_AXE = MLG%angle_AXE
            
            DO ifaclimi = 1,nfac_limi
                id_fac = ID_FACLIMi(ifaclimi)
                
                ! Les 2 sommets de chaque face
                DO isom = 1,nsom_fac
                    id_som = MLG%FAC(id_fac,isom+2)
                    
                    ! Dans le repere (x,y) => (r,z)
                    U_r_som = (COS(angle_AXE)*U_som(id_som,1)) + (SIN(angle_AXE)*U_som(id_som,2))
                    
                    ! comme U_z_som=0, on reprojete (r,0)=>(x,y)
                    U_som(id_som,1) = COS(angle_AXE) * U_r_som
                    U_som(id_som,2) = SIN(angle_AXE) * U_r_som
                
                END DO  
            END DO
        
        ! Sinon rien ...
        CASE DEFAULT

    END SELECT

    DEALLOCATE( ID_FACLIMi ) ! Désallocation pour nouvelle limite
    
END DO

END SUBROUTINE expt_interp_struct
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Parser qui gere l'interpretation des champs NOM_VAR_ETAT et COL_VAR_EXP
!> pour determiner le vecteur U ecrit en export aux cellules
SUBROUTINE expt_parser( U, ncel, nom_var, col_var, DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: col_var
CHARACTER (len=32) :: nom_var
INTEGER(IP) :: ncel
REAL(DP), DIMENSION(ncel) :: U
REAL(DP), DIMENSION(ncel,3) :: grad_U
!
! Choix du champ ecrit en fonction de nom_var
! Pour plus d'infos sur leur contenu, voir mod_etat et mod_rsl
SELECT CASE (nom_var)

    CASE ("VOLUME")
        U(:) = DOM%MLG%VOLUME(:)
     
    CASE ("T")
        U(:) = DOM%ETAT%T(:)
    CASE ("grad_T")
        CALL grad_cel( DOM%ETAT%T(:), grad_U, DOM%MLG%ncel, DOM%MLG%CEL_ITP, DOM%MLG%dim_simu )
        U(:) = grad_U(:, col_var)
        
    CASE ("P")
        IF (DOM%NUM%ADVECTION == 'off') CALL print_err('ADVECTION doit etre active pour exporter la variable P')
        U(:) = DOM%ETAT%P(:) - P_ref

    CASE ("grad_P")
        IF (DOM%NUM%ADVECTION == 'off') CALL print_err('ADVECTION doit etre active pour exporter la variable grad_P')
        CALL grad_cel( DOM%ETAT%P(:), grad_U, DOM%MLG%ncel, DOM%MLG%CEL_ITP, DOM%MLG%dim_simu )
        U(:) = grad_U(:, col_var)

    CASE ("Y")
        U(:) = DOM%ETAT%Y(:,col_var)
    CASE ("Y_g")
        U(:) = DOM%ETAT%Y_g(:)
    CASE ("Y_s")
        U(:) = DOM%ETAT%Y_s(:)
        
    CASE ("phi")
        U(:) = DOM%ETAT%phi(:,col_var)
    CASE ("phi_g")
        U(:) = DOM%ETAT%phi_g(:)
    CASE ("phi_s")
        U(:) = DOM%ETAT%phi_s(:)

    CASE ("rho")
        U(:) = DOM%ETAT%rho(:)
    CASE ("rho_e")
        U(:) = DOM%ETAT%rho_e(:,col_var)
    CASE ("rho_g")
        U(:) = DOM%ETAT%rho_g(:)
        
    CASE ("v_g")
        IF (DOM%NUM%ADVECTION == 'off') CALL print_err('ADVECTION doit etre active pour exporter la variable v_g')
        CALL grad_cel( DOM%ETAT%P(:), grad_U, DOM%MLG%ncel, DOM%MLG%CEL_ITP, DOM%MLG%dim_simu )
        grad_U = - DOM%ETAT%Kp(:,:) / &
                    SPREAD(DOM%ETAT%mu,2,3) * &
                    grad_U
        U(:) = grad_U(:, col_var)
        
    CASE ("w_e")
        U(:) = DOM%ETAT%w_e(:,col_var)
    CASE ("w_g")
        U(:) = DOM%ETAT%w_g(:)
    CASE ("wQ")
        U(:) = DOM%ETAT%wQ(:)
    CASE ("alpha_reac")
        U(:) = DOM%ETAT%alpha_reac(:,col_var)
        
    CASE ("E")
        U(:) = DOM%RSL%E(:)
    CASE ("dE")
        U(:) = DOM%RSL%dE(:)
    CASE ("m")
        U(:) = DOM%RSL%m(:)
    CASE ("m_e")
        U(:) = DOM%RSL%m_e(:,col_var)
    CASE ("dm_e")
        U(:) = DOM%RSL%dm_e(:,col_var)
        
    CASE ("k")
        U(:) = DOM%ETAT%k(:,col_var)
    CASE ("Cp")
        U(:) = DOM%ETAT%Cp(:)
    CASE ("h")
        U(:) = DOM%ETAT%h(:)
    CASE ("Kp")
        U(:) = DOM%ETAT%Kp(:,col_var)
    
    CASE ("CFL")
        U(:) = DOM%ETAT%CFL(:)
    CASE ("Fo")
        U(:) = DOM%ETAT%Fo(:)    
        
    CASE ("G_XYZ")
        U(:) = DOM%MLG%G_XYZ(:,col_var)
    CASE ("SOMMET")
        U(:) = DOM%MLG%SOMMET(:,col_var)  

    CASE ("U")
     IF (DOM%NUM%STRUCTURE == 'off') CALL print_err('STRUCTURE doit etre active pour exporter la variable U')
        U(:) = DOM%RSL%U(:,col_var)
  
    CASE ("dU")
     IF (DOM%NUM%STRUCTURE == 'off') CALL print_err('STRUCTURE doit etre active pour exporter la variable dU')
        U(:) = DOM%RSL%dU(:,col_var)
        
    CASE ("eps_strain")
     IF (DOM%NUM%STRUCTURE == 'off') CALL print_err('STRUCTURE doit etre active pour exporter la variable eps_strain')
        U(:) = DOM%ETAT%eps_strain(:,col_var)
              
    CASE ("sig_stress")
     IF (DOM%NUM%STRUCTURE == 'off') CALL print_err('STRUCTURE doit etre active pour exporter la variable sig_stress')
        U(:) = DOM%ETAT%sig_stress(:,col_var)
        
    CASE ("residu_A")
        U(:) = DOM%RSL%residu_A(:)

    CASE ("r_courbure")
     IF (DOM%NUM%ABLATION == 'off') CALL print_err('ABLATION doit etre active pour exporter la variable r_courbure')
        U(:) = DOM%MLG%SOM_LIM%r_courb
        
    CASE DEFAULT
        CALL print_err('La variable '//TRIM(nom_var)//' n''est pas disponible a l''export.')
END SELECT

END SUBROUTINE expt_parser
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_expt_lib
