!> Module pour l'export des variables globales au format hdf5
MODULE mod_expt_hdf5
!
USE mod_cst, ONLY : DP, IP, eps_min
USE mod_expt_lib, ONLY : expt_parser
USE mod_interpolate, ONLY : grad_one_cel
!
USE HDF5
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Creation du fichier initial vide (plein de zeros) pour export en HDF5
SUBROUTINE expt_hdf5_ini( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
!
CHARACTER (len=32) :: filename ! Nom du fichier exporte
INTEGER(IP) :: error ! Error flag
INTEGER(IP) :: file_id ! Identifiant fichier
INTEGER(IP) :: group_id ! Identifiant groupe
INTEGER(IP) :: subgroup_id ! Identifiant sous-groupe
INTEGER(IP) :: dataset_id ! Identifiant Dataset
INTEGER(IP) :: dataspace_id ! Identifiant Data space
!
CHARACTER (len=32), DIMENSION(5) :: volume_dataname ! Nom des data VOLUME
CHARACTER (len=32), DIMENSION(12) :: numerical_dataname ! Nom des data NUMERICAL
CHARACTER (len=32), DIMENSION(4) :: surface_dataname ! Nom des data SURFACE
CHARACTER (len=32), DIMENSION(3) :: export_dataname ! Nom des data EXPORT

CHARACTER (len=64) :: varname ! Nom des variables exportees
INTEGER(HSIZE_T), DIMENSION(2) :: dims ! Datasets dimensions
REAL, ALLOCATABLE, DIMENSION(:,:) :: null_data ! Tableau de zeros
INTEGER ::  i, j


! Liste des tableaux / sous-tableaux pour chaque groupe
volume_dataname = (/'Mass','Volume','Energy','Mass_Loss_Rate', &
                    'Energy_Loss_Rate'/)
numerical_dataname = (/'total_CPU_time','dt_CPU_iter','CFL_max',&
                    'Fo_max','niter_A','err_it_A','niter_DR',&
                    'err_it_DR','niter_S','err_it_S','niter_AB',&
                    'err_it_AB'/)
surface_dataname = (/'Total_heat_flux','Heat_flux_density',&
                    'Total_mass_flux','Mass_flux_density'/)
export_dataname = (/'Min.','Max.','Mean'/)


! Dimension des tableaux
dims(1) = 2
dims(2) = 1 + DOM%NUM%niter / DOM%SUI%iiter_hdf5

! Et création de tableau vide (2eme colonne) et temps (1ere colonne)
ALLOCATE( null_data( dims(1), dims(2) ) )
DO i = 1,dims(2)
    null_data(1,i) = (i-1) * DOM%SUI%iiter_hdf5 * DOM%NUM%dt
    null_data(2,i) = 0.0_dp
END DO

! Ouverture interface fortran : 
!!! ATTENTION : cette interface n'est fermee qu'a la fin de modethec
CALL h5open_f(error)

! Creation/Ecrasement du fichier
!!! ATTENTION : on ne ferme ce fichier qu'a la fin de modethec
filename = TRIM(DOM%EXPT%nom_exp) // '.hdf'
CALL h5fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, error)
DOM%SUI%file_id_hdf5 = file_id

! Creation d'un dataspace de dimension dims
CALL h5screate_simple_f(2, dims, dataspace_id, error)


!--- Groupe : 'Volume_integrals'
CALL h5gcreate_f(file_id, 'Volume_integrals', group_id, error)

! Datasets de 'Volume_integrals'
DO i = 1,SIZE(volume_dataname)
    CALL h5dcreate_f(group_id, volume_dataname(i), H5T_NATIVE_REAL, dataspace_id, dataset_id, error)
    CALL h5dwrite_f(dataset_id, H5T_NATIVE_REAL, null_data, dims, error)
END DO


!--- Groupe : 'Numerical_data'
CALL h5gcreate_f(file_id, 'Numerical_data', group_id, error)

! Datasets de 'Numerical_data'
DO i = 1,SIZE(numerical_dataname)
    CALL h5dcreate_f(group_id, numerical_dataname(i), H5T_NATIVE_REAL, dataspace_id, dataset_id, error)
    CALL h5dwrite_f(dataset_id, H5T_NATIVE_REAL, null_data, dims, error)
END DO


!--- Groupe : 'Surface_integrals'
CALL h5gcreate_f(file_id, 'Surface_integrals', group_id, error)

! Pour chaque limite ...
DO i = 1,DOM%CDT%nlim
    ! Sous-groupe Limite i
    CALL h5gcreate_f(group_id, DOM%CDT%LIM(i)%nom_lim, subgroup_id, error)
    
    ! Datasets de 'Surface_integrals' Limite i
    DO j = 1,SIZE(surface_dataname)
        CALL h5dcreate_f(subgroup_id, surface_dataname(j), H5T_NATIVE_REAL, dataspace_id, dataset_id, error)
        CALL h5dwrite_f(dataset_id, H5T_NATIVE_REAL, null_data, dims, error)
    END DO

END DO


!--- Groupe : 'Export_data'
CALL h5gcreate_f(file_id, 'Export_data', group_id, error)


! Pour chaque variable exportee
DO i = 1,DOM%EXPT%nexpt
    
    ! Pre-traitement du nom a ecrire
    varname = DOM%EXPT%NOM_VAR_EXP(i)
    CALL rename_varname( varname )
    
    
    ! Sous-groupe Variable i
    CALL h5gcreate_f(group_id, varname, subgroup_id, error)
    
    ! Datasets de 'Export_data' Variable i
    DO j = 1,SIZE(export_dataname)
        CALL h5dcreate_f(subgroup_id, export_dataname(j), H5T_NATIVE_REAL, dataspace_id, dataset_id, error)
        CALL h5dwrite_f(dataset_id, H5T_NATIVE_REAL, null_data, dims, error)
    END DO

END DO

!--- Groupe : 'Sensors'
CALL h5gcreate_f(file_id, 'Sensors', group_id, error)

! Pour chaque variable exporte
DO i = 1,DOM%EXPT%nexpt
    ! Pre-traitement du nom a ecrire
    varname = DOM%EXPT%NOM_VAR_EXP(i)
    CALL rename_varname( varname ) 

    ! Sous groupe Variable i
    CALL h5gcreate_f(group_id, varname, subgroup_id, error)    

    DO j = 1,DOM%SUI%nsensors

        varname = 'Point_'
        CALL num2str_varname(varname,j)

        CALL h5dcreate_f(subgroup_id, varname, H5T_NATIVE_REAL, dataspace_id, dataset_id, error)
        CALL h5dwrite_f(dataset_id, H5T_NATIVE_REAL, null_data, dims, error)
    END DO  

END DO

! Pas de fermeture du fichier pour gagner temps d'execution !!

END SUBROUTINE expt_hdf5_ini
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Remplissage "au fur et a mesure" du fichier HDF5 pour chaque 
!> pas de temps iiter_hdf5
SUBROUTINE expt_hdf5( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
!
!CHARACTER (len=32) :: filename ! Nom du fichier exporte
CHARACTER(len=64) :: dataset_name          ! Nom du dataset a ecrire
INTEGER(IP) :: file_id ! Identifiant fichier
REAL(4) ::  wdata, surface_limi
CHARACTER (len=64) :: varname ! Nom des variables exportees
!
REAL(DP), ALLOCATABLE, DIMENSION(:) :: U
REAL(DP), DIMENSION(3) :: grad_U, vec_GS
INTEGER ::  i_ligne, i, isensor, id_cel2sens

! No de ligne a remplir
i_ligne = DOM%NUM%iiter / DOM%SUI%iiter_hdf5

! No de fichier HDF deja ouvert dans expt_hdf5_ini
file_id = DOM%SUI%file_id_hdf5

! Allocation du tebleau temporaire U
ALLOCATE( U(DOM%MLG%ncel) )


!----------------------------------------------------------------------!
!--- GROUPE : Numerical_data ------------------------------------------!

dataset_name = 'Numerical_data/Fo_max'
wdata = MAXVAL(DOM%ETAT%Fo)
CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )

dataset_name = 'Numerical_data/CFL_max'
wdata = MAXVAL(DOM%ETAT%CFL)
CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )

dataset_name = 'Numerical_data/err_it_DR'
wdata = DOM%RSL%err_it_DR
CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )

dataset_name = 'Numerical_data/niter_DR'
wdata = DOM%RSL%niter_DR
CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )

dataset_name = 'Numerical_data/err_it_A'
wdata = DOM%RSL%err_it_A
CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )

dataset_name = 'Numerical_data/niter_A'
wdata = DOM%RSL%niter_A
CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )

dataset_name = 'Numerical_data/err_it_S'
wdata = DOM%RSL%err_it_S
CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )

dataset_name = 'Numerical_data/niter_S'
wdata = DOM%RSL%niter_S
CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )

dataset_name = 'Numerical_data/err_it_AB'
wdata = DOM%RSL%err_it_AB
CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )

dataset_name = 'Numerical_data/niter_AB'
wdata = DOM%RSL%niter_AB
CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )

dataset_name = 'Numerical_data/dt_CPU_iter'
wdata = DOM%RSL%dt_CPU
CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )

dataset_name = 'Numerical_data/total_CPU_time'
wdata = DOM%RSL%total_CPU_time
CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )

!----------------------------------------------------------------------!
!--- GROUPE : Volume_integrals ----------------------------------------!

dataset_name = 'Volume_integrals/Mass'
wdata = SUM(DOM%RSL%m)
CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )

dataset_name = 'Volume_integrals/Mass_Loss_Rate'
wdata = SUM(DOM%RSL%dm_e)
CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )

dataset_name = 'Volume_integrals/Energy'
wdata = SUM(DOM%RSL%E)
CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )

dataset_name = 'Volume_integrals/Energy_Loss_Rate'
wdata = SUM(DOM%RSL%dE)
CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )

dataset_name = 'Volume_integrals/Volume'
wdata = SUM(DOM%MLG%VOLUME)
CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )


!----------------------------------------------------------------------!
!--- GROUPE : Surface_integrals ---------------------------------------!

! Pour chaque limite ...
DO i = 1,DOM%CDT%nlim

    ! Surface totale de la limite i
    surface_limi = SUM( DOM%MLG%AIRE( DOM%CDT%LIM(i)%ID_FACLIMi ) )
    
    ! Total heat flux
    dataset_name = 'Surface_integrals/' // TRIM(DOM%CDT%LIM(i)%nom_lim) // '/Total_heat_flux'
    
    ! Somme de tous les types de flux energetiques
    wdata = SUM( DOM%RSL%FL_cond( DOM%CDT%LIM(i)%ID_FACLIMi ) )
    wdata = wdata + SUM( DOM%RSL%FL_E_darcy( DOM%CDT%LIM(i)%ID_FACLIMi ) )
    IF (ALLOCATED( DOM%RSL%FL_ener_def )) THEN
        wdata = wdata + SUM( DOM%RSL%FL_ener_def( DOM%CDT%LIM(i)%ID_FACLIMi ) )
    END IF
    ! Ecriture
    CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )
    
    ! Flux energetique par unite de surface
    dataset_name = 'Surface_integrals/' // TRIM(DOM%CDT%LIM(i)%nom_lim) // '/Heat_flux_density'
    wdata = wdata / (surface_limi + 10*eps_min)
    CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )
    
    ! Total_mass_flux
    dataset_name = 'Surface_integrals/' // TRIM(DOM%CDT%LIM(i)%nom_lim) // '/Total_mass_flux'
    
    ! Somme de tous les types de flux massiques
    wdata = SUM( DOM%RSL%FL_darcy( DOM%CDT%LIM(i)%ID_FACLIMi , : ) )
        IF (ALLOCATED( DOM%RSL%FL_masse_def )) THEN
        wdata = wdata + SUM( DOM%RSL%FL_masse_def( DOM%CDT%LIM(i)%ID_FACLIMi, : ) )
    END IF
    ! Ecriture
    CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )
    
    ! Flux massique par unite de surface
    dataset_name = 'Surface_integrals/' // TRIM(DOM%CDT%LIM(i)%nom_lim) // '/Mass_flux_density'
    wdata = wdata / (surface_limi + 10*eps_min)
    CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )

END DO


!----------------------------------------------------------------------!
!--- GROUPE : Export_data ---------------------------------------------!

! Pour chaque variable exportee...
DO i = 1,DOM%EXPT%nexpt
    
    ! Pre-traitement du nom a ecrire
    varname = DOM%EXPT%NOM_VAR_EXP(i)
    CALL rename_varname( varname )

    ! Appel au parser expt_parser qui retourne une copie de la variable demandee
    CALL expt_parser( U, DOM%MLG%ncel, DOM%EXPT%NOM_VAR_ETAT(i), DOM%EXPT%COL_VAR_EXP(i), DOM )
    
    ! Ecriture du Maximum
    dataset_name = 'Export_data/' // TRIM(varname) // '/Max.'
    wdata = MAXVAL(U)
    CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )
    
    ! Ecriture du Minimum
    dataset_name = 'Export_data/' // TRIM(varname) // '/Min.'
    wdata = MINVAL(U)
    CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )
    
    ! Ecriture de la valeur moyenne
    dataset_name = 'Export_data/' // TRIM(varname) // '/Mean'
    wdata = SUM(U) / DOM%MLG%ncel
    CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )

END DO

!----------------------------------------------------------------------!
!--- GROUPE : Sensors  ------------------------------------------------!

! Pour chaque variable exporte
IF ( DOM%SUI%sensors == 1 ) THEN
    DO i = 1,DOM%EXPT%nexpt

        ! Appel au parser expt_parser qui retourne une copie de la variable demandee
        CALL expt_parser( U, DOM%MLG%ncel, DOM%EXPT%NOM_VAR_ETAT(i), DOM%EXPT%COL_VAR_EXP(i), DOM )

        ! Pour chaque sensor ...
        DO isensor = 1,DOM%SUI%nsensors

            ! Determine le nom de dataset
            varname = 'Point_'
            CALL num2str_varname(varname, isensor)
            dataset_name = 'Sensors/' // TRIM(DOM%EXPT%NOM_VAR_EXP(i)) // '/' // varname
            
            id_cel2sens = DOM%SUI%SENS2CELL(isensor) ! Indice de cellule la plus proche

            ! Gradient de U à la cellule la plus proche
            CALL grad_one_cel( U(:), grad_U, &
                                DOM%MLG%CEL_ITP(id_cel2sens)%ncel_adj, &
                                DOM%MLG%CEL_ITP(id_cel2sens), &
                                DOM%MLG%dim_simu )

            ! Vecteur (centre cellule -> capteur)
            vec_GS = DOM%SUI%COORD_SENSORS(isensor, :) - DOM%MLG%G_XYZ(id_cel2sens, :)

            ! Calcul de la valeur au capteur 
            ! wdata = U(cellule la plus proche) + Grad U * Vecteur (centre cellule -> capteur)
            wdata = DOT_PRODUCT( grad_U, vec_GS )
            wdata = wdata + U(id_cel2sens)

            CALL expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )

        END DO  
    END DO
END IF

END SUBROUTINE expt_hdf5
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Finalisation du fichier HDF5 : femreture du fichier et de l'interface
SUBROUTINE expt_hdf5_end( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
!
INTEGER(IP) :: error ! Error flag
INTEGER(IP) :: file_id ! Identifiant fichier

file_id = DOM%SUI%file_id_hdf5

! Fermer le fichier HDF5
CALL h5fclose_f(file_id, error)

! Femer l'interface FORTRAN
CALL h5close_f(error)

END SUBROUTINE expt_hdf5_end
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Modification du fichier hdf5 du dataset "dataset_name"
!> a la ligne "i_ligne" par le tableau "wdata"
SUBROUTINE expt_modify_hdf5( i_ligne, wdata, file_id, dataset_name )
!
!---- Déclarations ----------------------------------------------------!
!
INTEGER(IP) :: i_ligne                              ! Numero de la ligne
REAL(4) ::  wdata                                   ! Portion du Dataset modifie (precision simple)
INTEGER(IP) :: file_id                              ! Identifiant fichier
CHARACTER(len=64) :: dataset_name                   ! Nom du dataset a ecrire
!
!
INTEGER(IP) :: error                                ! Error flag
INTEGER(IP) :: dataset_id                           ! Identifiant Dataset
INTEGER(IP) :: dataspace_id                         ! Identifiant Data space
INTEGER(IP) :: memspace_id                          ! Identifiant Memory space
!
INTEGER(HSIZE_T), DIMENSION(2) :: count = (/1,1/)  ! Size of hyperslab
INTEGER(HSIZE_T), DIMENSION(2) :: offset           ! Hyperslab offset
INTEGER(HSIZE_T), DIMENSION(2) :: stride = (/1,1/) ! Hyperslab stride 
INTEGER(HSIZE_T), DIMENSION(2) :: block = (/1,1/)  ! Hyperslab block size

offset = (/1,i_ligne/)

! Ouverture du dataset
CALL h5dopen_f(file_id, dataset_name, dataset_id, error)

! Renvoi du dataspace correspondant
CALL h5dget_space_f(dataset_id, dataspace_id, error)


! Selection de la partie du dataspace voulu...
CALL h5sselect_hyperslab_f(dataspace_id, H5S_SELECT_SET_F, offset, count, error, stride, block)

!... et renvoi de l'espace memoire correspondant
CALL h5screate_simple_f(2, block, memspace_id, error)

! Ecriture de "wdata" dans le memspace_id
CALL h5dwrite_f(dataset_id, H5T_NATIVE_REAL, wdata, block, error, memspace_id, dataspace_id)


END SUBROUTINE expt_modify_hdf5
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Creation du fichier initial vide (plein de zeros) pour backup en HDF5
SUBROUTINE expt_hdf5_backup_ini( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
!
CHARACTER (len=32) :: filename ! Nom du fichier exporte
CHARACTER (len=32) :: attr ! Nom de l'attribut
INTEGER(IP) :: error ! Error flag
INTEGER(IP) :: file_id ! Identifiant fichier
INTEGER(IP) :: dataset_id ! Identifiant Dataset
INTEGER(IP) :: dataspace_id ! Identifiant Data space
!
CHARACTER (len=32), DIMENSION(6) :: variable_dataname ! Nom des data 
INTEGER(IP), DIMENSION(6) :: variable_dim_1 ! Dimension de chaque variable
INTEGER(IP), DIMENSION(6) :: variable_dim_2 ! Dimension de chaque variable
!
CHARACTER (len=64) :: varname ! Nom des variables exportees
INTEGER(HSIZE_T), DIMENSION(2) :: dims ! Datasets dimensions
REAL, ALLOCATABLE, DIMENSION(:,:) :: null_data ! Tableau de zeros
INTEGER ::  i, ii

! Liste des tableaux / sous-tableaux pour chaque groupe
variable_dataname = (/'Energy','Mass','alpha_reac','Uxyz','CoordXYZ','Volume'/)
! Dimension des tableaux précédents
variable_dim_1 = (/1,DOM%PHYS%nesp,DOM%PHYS%nreac,3,3,1/) 
variable_dim_2 = (/DOM%MLG%ncel,DOM%MLG%ncel,DOM%MLG%ncel,DOM%MLG%ncel,DOM%MLG%nsom,DOM%MLG%ncel/)

! Ouverture interface fortran si ce n'est pas deja fait dans expt_hdf5_ini : 
!!! ATTENTION : cette interface n'est fermee qu'a la fin de modethec
IF (DOM%SUI%file_hdf5 == 0 .AND. DOM%NUM%restart_backup == 0) CALL h5open_f(error)

! Creation/Ecrasement du fichier
!!! ATTENTION : on ne ferme ce fichier qu'a la fin de modethec
filename = 'backup_' // ADJUSTL(DOM%file_param)
filename = filename(1:LEN(TRIM(filename))-4) // '.hdf'
CALL h5fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, error)
DOM%SUI%file_id_backup_hdf5 = file_id

! Pour chaque variable
DO i = 1,SIZE(variable_dataname)
    IF ( DOM%NUM%REACTION == 'off' .AND. i == 3) CYCLE
    IF ( DOM%NUM%STRUCTURE == 'off' .AND. i == 4) CYCLE
    IF ( DOM%NUM%ABLATION == 'off' .AND. (i == 5 .OR. i == 6)) CYCLE
    ! Dimension des tableaux
    dims(1) = variable_dim_1(i)
    dims(2) = variable_dim_2(i)
    ! Et création de tableau vide
    ALLOCATE( null_data( dims(1), dims(2) ) )
    null_data(:,:) = 0.0_dp

    ! Creation d'un dataspace de dimension dims
    CALL h5screate_simple_f(2, dims, dataspace_id, error)
    
    varname = variable_dataname(i)

    CALL h5dcreate_f(file_id, varname, H5T_NATIVE_REAL, dataspace_id, dataset_id, error)
    CALL h5dwrite_f(dataset_id, H5T_NATIVE_REAL, null_data, dims, error)   
    DEALLOCATE(null_data)
END DO

! Si ablation, on enregistre la qualité de maillage initiale et la raideur des aretes
IF ( DOM%NUM%ABLATION == 'on' ) THEN
    ! Qualité du maillage
    ! Dimension des tableaux
    dims(1) = 2
    dims(2) = DOM%MLG%ncel
    ! Et création de tableau vide
    ALLOCATE( null_data( dims(1), dims(2) ) )
    null_data(1,:) = DOM%MLG%QUALITE_INI(:,1) ! Tableau non nul pour ce cas
    null_data(2,:) = DOM%MLG%QUALITE_INI(:,2) ! Tableau non nul pour ce cas

    ! Creation d'un dataspace de dimension dims
    CALL h5screate_simple_f(2, dims, dataspace_id, error)
    
    varname = 'Qualite_maillage_ini'

    CALL h5dcreate_f(file_id, varname, H5T_NATIVE_REAL, dataspace_id, dataset_id, error)
    CALL h5dwrite_f(dataset_id, H5T_NATIVE_REAL, null_data, dims, error)   
    DEALLOCATE(null_data)

    ! Raideurs lim initiale ( = 1/DIST )
    ! Dimension des tableaux
    dims(1) = 1
    dims(2) = DOM%MLG%nbr_aret_lim
    ! Et création de tableau vide
    ALLOCATE( null_data( dims(1), dims(2) ) )
    null_data(1,:) = DOM%MLG%ARET_LIM(1:DOM%MLG%nbr_aret_lim)%raideur ! Tableau non nul pour ce cas

    ! Creation d'un dataspace de dimension dims
    CALL h5screate_simple_f(2, dims, dataspace_id, error)
    
    varname = 'raideur_lim'

    CALL h5dcreate_f(file_id, varname, H5T_NATIVE_REAL, dataspace_id, dataset_id, error)
    CALL h5dwrite_f(dataset_id, H5T_NATIVE_REAL, null_data, dims, error)   
    DEALLOCATE(null_data)    

    ! Raideurs lim laplacien
    ! Dimension des tableaux
    dims(1) = MAXVAL( DOM%MLG%SOM_LIM%nvoisin )
    dims(2) = DOM%MLG%nsom_lim
    ! Et création de tableau vide
    ALLOCATE( null_data( dims(1), dims(2) ) )
    null_data = 0.0_dp
    DO ii = 1, DOM%MLG%nsom_lim
        null_data( 1:DOM%MLG%SOM_LIM(ii)%nvoisin , ii ) = DOM%MLG%SOM_LIM(ii)%RAIDEUR ! Tableau non nul pour ce cas
    END DO    

    ! Creation d'un dataspace de dimension dims
    CALL h5screate_simple_f(2, dims, dataspace_id, error)
    
    varname = 'raideur_lim_lapl'

    CALL h5dcreate_f(file_id, varname, H5T_NATIVE_REAL, dataspace_id, dataset_id, error)
    CALL h5dwrite_f(dataset_id, H5T_NATIVE_REAL, null_data, dims, error)   
    DEALLOCATE(null_data)        

    ! Raideurs int
    ! Dimension des tableaux
    dims(1) = MAXVAL( DOM%MLG%SOM_INT%nvoisin )
    dims(2) = DOM%MLG%nsom_int
    ! Et création de tableau vide
    ALLOCATE( null_data( dims(1), dims(2) ) )
    null_data = 0.0_dp
    DO ii = 1, DOM%MLG%nsom_int
        null_data( 1:DOM%MLG%SOM_INT(ii)%nvoisin , ii ) = DOM%MLG%SOM_INT(ii)%RAIDEUR ! Tableau non nul pour ce cas
    END DO

    ! Creation d'un dataspace de dimension dims
    CALL h5screate_simple_f(2, dims, dataspace_id, error)
    
    varname = 'raideur_int_lapl'

    CALL h5dcreate_f(file_id, varname, H5T_NATIVE_REAL, dataspace_id, dataset_id, error)
    CALL h5dwrite_f(dataset_id, H5T_NATIVE_REAL, null_data, dims, error)   
    DEALLOCATE(null_data)        
END IF

! ! On enregistre des informations complémentaires sur la simulation

! On enregistre le nom du fichier *.prm
varname = 'filename'
attr = DOM%file_param
CALL expt_write_attr(varname,attr,file_id)

! On crée l'attribut pour le pas de temps
varname = 'dt'
WRITE(attr,*) 0
CALL expt_write_attr(varname,attr,file_id)

! On crée l'attribut pour l'iteration courante
varname = 'iiter'
WRITE(attr,*) 0
CALL expt_write_attr(varname,attr,file_id)

! On crée l'attribut pour le temps final
varname = 'tf'
WRITE(attr,*) 0
CALL expt_write_attr(varname,attr,file_id)

! On enregistre le nombre de cellules
varname = 'ncel'
WRITE(attr,*) DOM%MLG%ncel
CALL expt_write_attr(varname,attr,file_id)

! On enregistre le nombre d'especes
varname = 'nesp'
WRITE(attr,*) DOM%PHYS%nesp
CALL expt_write_attr(varname,attr,file_id)

! On enregistre le nombre de reactions
varname = 'nreac'
WRITE(attr,*) DOM%PHYS%nreac
CALL expt_write_attr(varname,attr,file_id)

! On enregistre les solveurs
varname = 'DIFFUSION'
IF (DOM%NUM%DIFFUSION == 'on') THEN
    attr = 'on'
    CALL expt_write_attr(varname,attr,file_id)
ELSE
    attr = 'off'
    CALL expt_write_attr(varname,attr,file_id)
END IF
varname = 'REACTION'
IF (DOM%NUM%REACTION == 'on') THEN
    attr = 'on'
    CALL expt_write_attr(varname,attr,file_id)
ELSE
    attr = 'off'
    CALL expt_write_attr(varname,attr,file_id)
END IF
varname = 'STRUCTURE'
IF (DOM%NUM%STRUCTURE == 'on') THEN
    attr = 'on'
    CALL expt_write_attr(varname,attr,file_id)
ELSE
    attr = 'off'
    CALL expt_write_attr(varname,attr,file_id)
END IF
varname = 'ABLATION'
IF (DOM%NUM%ABLATION == 'on') THEN
    attr = 'on'
    CALL expt_write_attr(varname,attr,file_id)
ELSE
    attr = 'off'
    CALL expt_write_attr(varname,attr,file_id)
END IF
varname = 'COUPLAGE'
IF (DOM%NUM%COUPLAGE == 'on') THEN
    attr = 'on'
    CALL expt_write_attr(varname,attr,file_id)
ELSE
    attr = 'off'
    CALL expt_write_attr(varname,attr,file_id)
END IF

IF (DOM%NUM%ABLATION == 'on') THEN
    ! On crée l'attribut pour le nombre de voisins a equilibrer
    varname = 'nbr_voisin_eq'
    WRITE(attr,*) 0
    CALL expt_write_attr(varname,attr,file_id)
END IF

! Pas de fermeture du fichier pour gagner temps d'execution !!

END SUBROUTINE expt_hdf5_backup_ini

!----------------------------------------------------------------------!
!> Remplissage "au fur et a mesure" du fichier backup  HDF5 pour chaque 
!> pas de temps iiter_backup. On ecrase les donnes precedentes.
SUBROUTINE expt_backup_hdf5( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
!
CHARACTER (len=32), DIMENSION(6) :: variable_parser ! Variable pour les parser
CHARACTER (len=32), DIMENSION(6) :: variable_dataname ! Nom des data 
INTEGER(IP), DIMENSION(6) :: variable_dim_1 ! Dimension de chaque variable
INTEGER(IP), DIMENSION(6) :: variable_dim_2 ! Dimension de chaque variable
!
INTEGER(IP) :: file_id                              ! Identifiant fichier
INTEGER(HSIZE_T), DIMENSION(2) :: dims              ! Datasets dimensions
!
CHARACTER (len=32) :: attr ! Nom de l'attribut
CHARACTER (len=64) :: varname ! Nom des variables exportees
!
INTEGER(IP) ::  j

! Liste des tableaux / sous-tableaux pour chaque groupe
variable_dataname = (/'Energy','Mass','alpha_reac','Uxyz','CoordXYZ','Volume'/)
! Dimension des tableaux précédents
variable_dim_1 = (/1,DOM%PHYS%nesp,DOM%PHYS%nreac,3,3,1/) 
variable_dim_2 = (/DOM%MLG%ncel,DOM%MLG%ncel,DOM%MLG%ncel,DOM%MLG%ncel,DOM%MLG%nsom,DOM%MLG%ncel/)
! Liste des variables pour le parser
variable_parser = (/'E','m_e','alpha_reac','U','SOMMET','VOLUME'/)

! No de fichier HDF deja ouvert dans expt_hdf5_backup_ini
file_id = DOM%SUI%file_id_backup_hdf5

!----------------------------------------------------------------------!
! On écrit dans le fichier backup hdf les variables variable_dataname
DO j = 1,SIZE(variable_dataname)
    IF ( DOM%NUM%REACTION == 'off' .AND. j == 3) CYCLE
    IF ( DOM%NUM%STRUCTURE == 'off' .AND. j == 4) CYCLE
    IF ( DOM%NUM%ABLATION == 'off' .AND. (j == 5 .OR. j == 6)) CYCLE
    dims = (/variable_dim_1(j),variable_dim_2(j)/)
    CALL expt_write_backup_data_hdf5(variable_dataname(j), dims, file_id, variable_parser(j), DOM)
END DO

!----------------------------------------------------------------------!
!--- Informations sur la simulation  ----------------------------------!
! On modifie l'attribut pour le pas de temps
varname = 'dt'
WRITE(attr,*) DOM%NUM%dt
CALL expt_modif_attr(varname,attr,file_id)

! On modifie l'attribut pour l'iteration courante
varname = 'iiter'
WRITE(attr,*) DOM%NUM%iiter
CALL expt_modif_attr(varname,attr,file_id)

! On modifie l'attribut pour le temps final
varname = 'tf'
WRITE(attr,*) DOM%NUM%ti
CALL expt_modif_attr(varname,attr,file_id)

IF ( DOM%NUM%ABLATION == 'on' ) THEN
    ! On modifie l'attribut pour l'iteration courante
    varname = 'nbr_voisin_eq'
    WRITE(attr,*) DOM%NUM%nvois_def
    CALL expt_modif_attr(varname,attr,file_id)
END IF

END SUBROUTINE expt_backup_hdf5
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Ecriture de la variable wdata dans le fichier backup
SUBROUTINE expt_write_backup_data_hdf5( dataset_name, dims, file_id, dataname, DOM )
!
!---- Déclarations ----------------------------------------------------!
!
TYPE(typ_dom) :: DOM
!
CHARACTER(len=32) :: dataset_name                   ! Nom du dataset a ecrire
CHARACTER(len=32) :: dataname                       ! Nom du dataset pour parser
!
REAL, ALLOCATABLE, DIMENSION(:,:) :: U_backup       ! Tableau de valeur
REAL(DP), ALLOCATABLE, DIMENSION(:) :: U_parser     ! Tableau de valeur du parser
INTEGER(IP) :: dim_parser                           ! Dimension
INTEGER(HSIZE_T), DIMENSION(2) :: dims              ! Datasets dimensions
!
INTEGER(IP) :: file_id                              ! Identifiant fichier
!
INTEGER(IP) :: error                                ! Error flag
INTEGER(IP) :: dataset_id                           ! Identifiant Dataset
INTEGER(IP) :: j
!
ALLOCATE( U_backup(dims(1),dims(2)) )
ALLOCATE( U_parser(dims(2)))
!
! Open an existing dataset.
CALL h5dopen_f(file_id, dataset_name, dataset_id, error)
!
! Write the dataset.
dim_parser = dims(2)
DO j = 1,dims(1)
    CALL expt_parser( U_parser, dim_parser, dataname, j, DOM )
    U_backup(j,:) = U_parser(:)
END DO    
!
CALL h5dwrite_f(dataset_id, H5T_NATIVE_REAL, U_backup, dims, error)
!
DEALLOCATE(U_backup)
DEALLOCATE(U_parser)
!
! Close the dataset.
CALL h5dclose_f(dataset_id, error)
!
END SUBROUTINE expt_write_backup_data_hdf5
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Finalisation du fichier backup HDF5 : femreture du fichier et de l'interface
SUBROUTINE expt_hdf5_backup_end( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
!
INTEGER(IP) :: error ! Error flag
INTEGER(IP) :: file_id ! Identifiant fichier

file_id = DOM%SUI%file_id_backup_hdf5

! Fermer le fichier HDF5
CALL h5fclose_f(file_id, error)

! Femer l'interface FORTRAN
IF (DOM%SUI%file_hdf5 == 0) CALL h5close_f(error)

END SUBROUTINE expt_hdf5_backup_end
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Reecriture de "varame" au format export hdf5, c'est-a-dire sans
!> caractere speciaux, /, <greek>, <math>, <sub>, <sup>
SUBROUTINE rename_varname( varname )
!
!---- Déclarations ----------------------------------------------------!
CHARACTER (len=64) :: varname ! Nom des variables exportees
INTEGER :: pos
!
DO WHILE (index(varname,'/') > 0)
    pos=index(varname,'/')
    varname=varname(1:pos-1)//varname(pos+1:)
END DO
DO WHILE (index(varname,'<greek>') > 0)
    pos=index(varname,'<greek>')
    varname=varname(1:pos-1)//varname(pos+7:)
END DO
DO WHILE (index(varname,'<sub>') > 0)
    pos=index(varname,'<sub>')
    varname=varname(1:pos-1)//varname(pos+5:)
END DO
DO WHILE (index(varname,'<sup>') > 0)
    pos=index(varname,'<sup>')
    varname=varname(1:pos-1)//varname(pos+5:)
END DO
DO WHILE (index(varname,'<math>') > 0)
    pos=index(varname,'<math>')
    varname=varname(1:pos-1)//varname(pos+6:)
END DO
!
!
END SUBROUTINE 
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Concaténation de varname et de j où j est un entier
!> Cela sert lors que l'on veut créer des str avec incrémentation d'un
!> entier.
SUBROUTINE num2str_varname(varname,num)
!
!---- Déclarations ----------------------------------------------------!
CHARACTER (len=64) :: varname ! Nom des variables exportees
CHARACTER (len=64) :: num_str ! Entier num convertit en str
INTEGER :: num
!
WRITE(num_str,*) num
varname = varname(1:LEN(TRIM(varname)))//ADJUSTL(num_str)
!
!
END SUBROUTINE
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Ecriture d'un attribut dans le fichier backup
SUBROUTINE expt_write_attr(varname,attr,file_id)
!
!---- Déclarations ----------------------------------------------------!
CHARACTER (len=32) :: varname ! Nom de l'atttribut
CHARACTER (len=32) :: attr ! Nom de l'atttribut
INTEGER(IP) :: error          ! Error flag
INTEGER(IP) :: file_id                              ! Identifiant fichier
INTEGER(HSIZE_T), DIMENSION(2) :: dims ! Datasets dimensions
INTEGER(HSIZE_T), DIMENSION(1) :: adims = (/1/) ! Attribute dimension
INTEGER(HID_T) :: attr_id ! Attribute identifier
INTEGER(HID_T) :: aspace_id     ! Attribute Dataspace identifier
INTEGER(HID_T) :: atype_id      ! Attribute Dataspace identifier
!
dims(1) = 1
dims(2) = 1
! Create datatype for the attribute.
CALL h5tcopy_f(H5T_NATIVE_CHARACTER, atype_id, error)
CALL h5tset_size_f(atype_id, 32, error)
! Create scalar data space for the attribute.
CALL h5screate_simple_f(1 , adims, aspace_id, error)
! Create dataset attribute.
CALL h5acreate_f(file_id, varname, atype_id, aspace_id, attr_id, error)
! Write the attribute data.
CALL h5awrite_f(attr_id, atype_id, attr, dims, error)
! Close the attribute.
CALL h5aclose_f(attr_id, error)
!
!
END SUBROUTINE
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Modification d'un attribut dans le fichier backup
SUBROUTINE expt_modif_attr(varname,attr,file_id)
!
!---- Déclarations ----------------------------------------------------!
CHARACTER (len=32) :: varname ! Nom de l'atttribut
CHARACTER (len=32) :: attr ! Nom de l'atttribut
INTEGER(IP) :: error          ! Error flag
INTEGER(IP) :: file_id                              ! Identifiant fichier
INTEGER(HSIZE_T), DIMENSION(2) :: dims ! Datasets dimensions
INTEGER(HSIZE_T), DIMENSION(1) :: adims = (/1/) ! Attribute dimension
INTEGER(HID_T) :: attr_id ! Attribute identifier
INTEGER(HID_T) :: aspace_id     ! Attribute Dataspace identifier
INTEGER(HID_T) :: atype_id      ! Attribute Dataspace identifier
!
dims(1) = 1
dims(2) = 1
! Create datatype for the attribute.
CALL h5tcopy_f(H5T_NATIVE_CHARACTER, atype_id, error)
CALL h5tset_size_f(atype_id, 32, error)
! Create scalar data space for the attribute.
CALL h5screate_simple_f(1 , adims, aspace_id, error)
! Open dataset attribute.
CALL h5aopen_name_f(file_id, varname, attr_id, error)
! Write the attribute data.
CALL h5awrite_f(attr_id, atype_id, attr, dims, error)
! Close the attribute.
CALL h5aclose_f(attr_id, error)
!
!
END SUBROUTINE
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_expt_hdf5
