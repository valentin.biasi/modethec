!> Module pour la finalisation des exports
MODULE mod_expt_end
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Finalisation des exports tecplot : preplot
!> \todo : ecriture directe en binaire
SUBROUTINE expt_end( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
CHARACTER (len=64) :: cmd_line

! Finalisation des export au format TECPLOT en binaire
IF (DOM%EXPT%tecplot_bin == 1) THEN
    
    WRITE(cmd_line,'(A,A,A)'),'preplot ',TRIM(DOM%EXPT%nom_exp),'.dat >/dev/null 2>&1'
    CALL SYSTEM(cmd_line)

    !WRITE(cmd_line,'(A,A,A)'),'rm ',TRIM(DOM%EXPT%nom_exp),'.dat'
    !CALL SYSTEM(cmd_line)
    
END IF

END SUBROUTINE expt_end
!----------------------------------------------------------------------!

!---- Fin du module ---------------------------------------------------!
END MODULE mod_expt_end
