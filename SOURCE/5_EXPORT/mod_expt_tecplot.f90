!> Export des resultats vers le format tecplot
MODULE mod_expt_tecplot
!
USE mod_cst, ONLY : IP, DP, eps_min
USE mod_print, ONLY : print_err
USE mod_algebra, ONLY : inttostr
USE mod_expt_lib, ONLY : expt_interp_struct, expt_parser, expt_interp_som
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Routine d'export du fichier tecplot .dat ASCII
SUBROUTINE expt_tecplot( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
CHARACTER (len=32+4) :: nom_exp
INTEGER(IP) :: iexpt, nexpt
REAL(DP) :: ti
INTEGER(IP) :: no_expt
CHARACTER (len=40) :: tmp_str
CHARACTER (len=32), PARAMETER :: form_exp = '(E14.6,E14.6,E14.6,E14.6,E14.6)'
INTEGER(IP) :: ncel, nsom, nsom_lim, isom
INTEGER(IP) :: nsom_cel, icel
REAL(DP), ALLOCATABLE, DIMENSION(:) :: U_cel, U_som, U_som_lim
REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: U_struct_som
INTEGER(IP) :: col_var
CHARACTER (len=32) :: nom_var
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: cel2som
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
nsom = DOM%MLG%nsom
nsom_lim = DOM%MLG%nsom_lim
!
!---- Allocations -----------------------------------------------------!
ALLOCATE( U_cel(ncel) )
IF (DOM%EXPT%sommet == 1) ALLOCATE( U_som(nsom) )
IF (DOM%NUM%STRUCTURE == 'on') ALLOCATE( U_struct_som(nsom,3) )
IF (DOM%NUM%ABLATION == 'on') ALLOCATE( U_som_lim(nsom_lim) )
!
! Réouverture du fichier d'export
nom_exp = TRIM(DOM%EXPT%nom_exp) // '.dat'
OPEN(3,FILE=nom_exp ,&
    STATUS='old', ACTION='write', POSITION='append' )
!

!------------------------------------------------------------------!
! En-tete pour chaque zone

ti = DOM%NUM%ti ! Temps courant
no_expt = DOM%NUM%iiter / DOM%EXPT%iiter_exp ! Numero de l'export total

! Titre de la zone
WRITE(3,'(A,I5,A,A,A,F11.4,A)'),' ZONE T= "Zone ',no_expt,' - Domain ',TRIM(ADJUSTL(DOM%nom_dom)),' - t =',ti,' sec."'
! position temporelle
WRITE(3,'(A,F11.4)'),' STRANDID=1, SOLUTIONTIME=',ti
! Nombre de points
WRITE(3,'(A,I6,A)'),' I=',DOM%MLG%nsom,', J=1, K=1, ZONETYPE=Ordered, DATAPACKING=Block'

! Mode FINITE ELEMENT
IF (DOM%EXPT%FEmode == 1) THEN
    IF (DOM%MLG%dim_simu == '3D') THEN
        tmp_str = ' ZONETYPE = FEBrick, Nodes='
    ELSE
        tmp_str = ' ZONETYPE = FEQuadrilateral, Nodes='
    END IF
    
    WRITE(3,'(A,I6,A,I6)'), tmp_str, &
                DOM%MLG%nsom,', Elements=', &
                DOM%MLG%ncel
END IF

! Cas centre sur cellule
IF (DOM%EXPT%cellule == 1) THEN
    WRITE(tmp_str,'(I6)'),DOM%EXPT%nexpt+3
    
    IF (DOM%EXPT%nexpt > 1) THEN
        WRITE(3,'(A,A,A)'),' VARLOCATION=([4-',TRIM(ADJUSTL(tmp_str)),']=CELLCENTERED)'
    ELSE IF (DOM%EXPT%nexpt == 1) THEN
        WRITE(3,'(A,A,A)'),' VARLOCATION=([',TRIM(ADJUSTL(tmp_str)),']=CELLCENTERED)'
    END IF
END IF

!  Listes de connecivite partagees avec 1ere zone
IF (DOM%EXPT%file_ini == 0) THEN
    WRITE(3,*), 'CONNECTIVITYSHAREZONE = 1'
END IF

! Ecriture des positions des sommets
!--- CAS MAILLAGE FIXE
IF (((DOM%NUM%STRUCTURE == 'off') .OR. (DOM%EXPT%deform_mlg == 0)) .AND. (DOM%NUM%ABLATION == 'off')) THEN

    ! Si Premiere zone : Ecriture des coordonnees des points sommets X Y et Z
    IF (DOM%EXPT%file_ini == 1) THEN
        WRITE(3,form_exp), DOM%MLG%SOMMET(:,1)
        WRITE(3,form_exp), DOM%MLG%SOMMET(:,2)
        WRITE(3,form_exp), DOM%MLG%SOMMET(:,3)
    
    ! Pour les zones 2 et suivantes, connexion aux autres donnees
    ELSE IF (DOM%EXPT%file_ini == 0) THEN
        WRITE(3,*), 'VARSHARELIST = ([1-3]=1)'
        
    END IF
    
END IF

! Ecriture des positions des sommets
!--- CAS MAILLAGE AVEC DEPLACEMENT (STRUCTURE)
IF ((DOM%NUM%STRUCTURE == 'on') .AND. (DOM%EXPT%deform_mlg == 1)) THEN
    
    ! Interpolation des deplacements aux sommets
    U_struct_som(:,:) = 0.0_dp
    CALL expt_interp_struct( DOM%RSL%U, ncel, &
                                U_struct_som, nsom, &
                                DOM%MLG, DOM%CDT )
                                
    WRITE(3,form_exp), DOM%MLG%SOMMET(:,1) + (DOM%EXPT%deform_amp * U_struct_som(:,1))
    WRITE(3,form_exp), DOM%MLG%SOMMET(:,2) + (DOM%EXPT%deform_amp * U_struct_som(:,2))
    WRITE(3,form_exp), DOM%MLG%SOMMET(:,3) + (DOM%EXPT%deform_amp * U_struct_som(:,3))
    
END IF


! Ecriture des positions des sommets
!--- CAS MAILLAGE AVEC DEPLACEMENT (ABLATION)
IF (DOM%NUM%ABLATION == 'on') THEN
    
    ! Deplacements aux sommets                 
    WRITE(3,form_exp), DOM%MLG%SOMMET(:,1)
    WRITE(3,form_exp), DOM%MLG%SOMMET(:,2)
    WRITE(3,form_exp), DOM%MLG%SOMMET(:,3)
    
END IF    
    
! Boucle sur chaque variable exortée demandée
nexpt = DOM%EXPT%nexpt
DO iexpt = 1,nexpt
    ! Nom et colonne de la variable
    nom_var = DOM%EXPT%NOM_VAR_ETAT(iexpt)
    col_var = DOM%EXPT%COL_VAR_EXP(iexpt)

    IF (nom_var == 'r_courbure') THEN ! Traitement particulier quand les infos sont que sur les sommets limites
        ! Cas centre sur sommet
        IF (DOM%EXPT%sommet == 1) THEN            
            ! Récupération de la valeur aux sommets
            CALL expt_parser( U_som_lim, nsom_lim, nom_var, col_var, DOM )
            U_som = 0.0_dp
            DO isom = 1, nsom_lim
                U_som( DOM%MLG%SOM_ADJ_LIM(isom,1) ) = U_som_lim(isom)
            END DO
            WRITE(3,form_exp), U_som(:)

        ! Cas centre sur cellule
        ELSE IF (DOM%EXPT%cellule == 1) THEN
            CALL print_err('L''export des rayons de courbures ne peut se faire qu''en "node centered"')
            U_cel = 0.0_dp
            WRITE(3,form_exp), U_cel
        END IF

    ELSE
        ! Récupération de la valeur aux cellules
        CALL expt_parser( U_cel, ncel, nom_var, col_var, DOM )

        ! Cas centre sur cellule
        IF (DOM%EXPT%cellule == 1) THEN
            WRITE(3,form_exp), U_cel
        
        ! Cas centre sur sommet
        ELSE IF (DOM%EXPT%sommet == 1) THEN
            
            ! Interpolation des deplacements aux sommets
            IF (nom_var == "U") THEN
                
                CALL expt_interp_struct( DOM%RSL%U, ncel, &
                                    U_struct_som, nsom, &
                                    DOM%MLG, DOM%CDT )
                WRITE(3,form_exp), U_struct_som(:,col_var)
            
            ! Interpolation des temperatures aux sommets
            ! Pas utilise car manque extrapolation suivant axe
            !ELSE IF (nom_var == "T") THEN
            !    
            !    CALL expt_interp_T( DOM%ETAT%T, ncel, &
            !                     U_som, nsom, &
            !                     DOM%MLG, DOM%CDT )
            !    WRITE(3,form_exp), U_som(:)
            
            ! Cas standart : interpolation de toutes les autres variables
            ELSE
            
                CALL expt_interp_som( U_cel, ncel, U_som, nsom, DOM%MLG )
                WRITE(3,form_exp), U_som(:)
                
            END IF
            
        END IF
    END IF
END DO

! Dimensionnement du tableau cel2som en fonction de la dimension de calcul
IF (DOM%MLG%dim_simu == '3D') THEN
    ALLOCATE( cel2som( 8 ))
ELSE
    ALLOCATE( cel2som( 4 ))
END IF

! Liste de connectivites des cellules (FEmode == 1) pour la 1ere zone
IF (DOM%EXPT%file_ini == 1 .AND. DOM%EXPT%FEmode == 1) THEN
    DO icel = 1,ncel
        ! Nombre de sommets par cellule
        nsom_cel = DOM%MLG%CEL2SOM(icel,2)
        
        ! Cas 3D :
        cel2som = 0
        IF (DOM%MLG%dim_simu == '3D') THEN
            ! Connectivites par cellule (avec zeros)
            cel2som(1:8) = DOM%MLG%CEL2SOM(icel,3:10)
            
            ! Gestion des topolgies FEBrik tecplot
            SELECT CASE (nsom_cel)
                
                ! Cas tetraedre
                CASE(4)
                    cel2som(8) = cel2som(4)
                    cel2som(7) = cel2som(4)
                    cel2som(6) = cel2som(4)
                    cel2som(5) = cel2som(4)
                    cel2som(4) = cel2som(3)
            
                ! Cas pyramide
                CASE(5)
                    cel2som(8) = cel2som(5)
                    cel2som(7) = cel2som(5)
                    cel2som(6) = cel2som(5)
                    
                ! Cas prisme
                CASE(6)
                    cel2som(8) = cel2som(6)
                    cel2som(7) = cel2som(6)
                    cel2som(6) = cel2som(5)
                    cel2som(5) = cel2som(4)
                    cel2som(4) = cel2som(3)
            
            END SELECT
            
            ! Ecriture connectivite 3D
            WRITE(3,*), cel2som
        
        ! Cas 2D :
        ELSE
        
            ! Connectivites pas cellule (avec zeros)
            cel2som(1:4) = DOM%MLG%CEL2SOM(icel,3:6)
            
            ! Gestion des topolgies FEQuadrilateral pour triangles
            IF (nsom_cel == 3) THEN
                cel2som(4) = cel2som(1)
            END IF
            
            ! Ecriture connectivite 2D
            WRITE(3,*), cel2som
            
        END IF
        
        
        
    END DO
END IF

! Si passage 1ere zone, eh ben c'est fini 
IF (DOM%EXPT%file_ini == 1) DOM%EXPT%file_ini = 0

IF (DOM%NUM%ABLATION == 'on') DEALLOCATE( U_som_lim )

CLOSE(3) ! et fermeture du fichier .dat

END SUBROUTINE expt_tecplot
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_expt_tecplot
