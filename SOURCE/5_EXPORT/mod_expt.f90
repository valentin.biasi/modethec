!> Export des resultats vers le format souhaite
!> \warning Fonctionne pour 1 seul domaine actuellement !
MODULE mod_expt
!
USE mod_expt_tecplot, ONLY : expt_tecplot
USE mod_expt_vtk, ONLY : expt_vtk
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Export des resultats vers le format souhaite
SUBROUTINE expt( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM

! Export au format TECPLOT ...
IF (DOM%EXPT%tecplot_bin == 1 .OR. DOM%EXPT%tecplot_ascii == 1) THEN
    CALL expt_tecplot( DOM )

END IF

! Export au format VTK ...
IF (DOM%EXPT%vtk_ascii == 1) THEN
    CALL expt_vtk( DOM )

END IF

END SUBROUTINE expt
!----------------------------------------------------------------------!

!---- Fin du module ---------------------------------------------------!
END MODULE mod_expt
