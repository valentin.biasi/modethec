!> Initialisation des export des resultats (en-tete des fichiers)
!> \warning Fonctionne pour 1 seul domaine actuellement
MODULE mod_expt_ini
!
USE mod_cst, ONLY : IP
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Choix du type de fichier pour initialisation des export des resultats 
SUBROUTINE expt_ini( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM

! Export au format TECPLOT ...
IF (DOM%EXPT%tecplot_bin == 1 .OR. DOM%EXPT%tecplot_ascii == 1) THEN
    CALL expt_ini_tecplot( DOM )
END IF

! Export au format VTK ...
IF (DOM%EXPT%vtk_ascii == 1) THEN
    CALL expt_ini_vtk( DOM )
END IF

END SUBROUTINE expt_ini
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de creation d'en-tete de fichier tecplot .dat
SUBROUTINE expt_ini_tecplot( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
CHARACTER (len=32+4) :: nom_exp
CHARACTER (len=512) :: nom_var ! ca peut etre long...
INTEGER(IP) :: iexpt, nexpt
!
! Fichier d'export
nom_exp = TRIM(DOM%EXPT%nom_exp) // '.dat'

! Création du fichier
OPEN(3,FILE=nom_exp ,&
    STATUS='replace', ACTION='write' )
! ... et écriture du titre
WRITE(3,*),'TITLE      = "',TRIM(DOM%EXPT%nom_exp),'"'

! ... puis nom des variables écrite
nom_var = 'VARIABLES  = "X [m]" "Y [m]" "Z [m]"'
WRITE(3,*), TRIM(nom_var)
nexpt = DOM%EXPT%nexpt
DO iexpt = 1,nexpt
    !nom_var = TRIM(nom_var)//' '//TRIM(DOM%EXPT%NOM_VAR_EXP(iexpt))
    WRITE(3,*), '"', TRIM(DOM%EXPT%NOM_VAR_EXP(iexpt)), '"'
END DO

CLOSE(3) ! Fermeture du fichier

END SUBROUTINE expt_ini_tecplot
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de creation d'en-tete de fichier paraview .vtk
SUBROUTINE expt_ini_vtk( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
CHARACTER (len=32+4) :: cmd_line

! Cree un dossier vtk
CALL SYSTEM('mkdir -p vtk')

! Efface les anciens fichiers VTK
WRITE(cmd_line,'(A,A,A)'),'rm vtk/',TRIM(DOM%EXPT%nom_exp),'.*.vtk'
CALL SYSTEM(cmd_line)

END SUBROUTINE expt_ini_vtk
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_expt_ini
