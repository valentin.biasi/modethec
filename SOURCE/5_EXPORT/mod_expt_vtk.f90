!> Export des resultats vers le format vtk
MODULE mod_expt_vtk
!
USE mod_cst, ONLY : IP, DP, eps_min
USE mod_print, ONLY : print_err
USE mod_algebra, ONLY : inttostr
USE mod_expt_lib, ONLY : expt_interp_struct, expt_parser, expt_interp_som
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Routine d'export du fichier paraview .vtk ASCII
SUBROUTINE expt_vtk( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
CHARACTER (len=32+4) :: nom_exp
CHARACTER (len=32), PARAMETER :: form_exp = '(E14.6,E14.6,E14.6,E14.6,E14.6)'
!
CHARACTER (len=512) :: nom_var, nom_var_exp ! ca peut etre long...
INTEGER(IP) :: iexpt,nexpt
INTEGER(IP) :: isom, nsom
INTEGER(IP) :: icel, ncel
INTEGER(IP) :: nval
INTEGER(IP) :: nsom_cel
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: cel2som
INTEGER(IP) :: nfile
REAL(DP), ALLOCATABLE, DIMENSION(:) :: U_cel, U_som
INTEGER(IP) :: col_var
REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: U_struct_som
INTEGER(IP) :: idx_split
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
nsom = DOM%MLG%nsom

!---- Allocations -----------------------------------------------------!
ALLOCATE( U_cel(ncel) )
IF (DOM%EXPT%sommet == 1) ALLOCATE( U_som(nsom) )
IF (DOM%NUM%STRUCTURE == 'on') ALLOCATE( U_struct_som(nsom,3) )

!----------------------------------------------------------------------!
! Fichier d'export
nfile = DOM%NUM%iiter / DOM%EXPT%iiter_exp
nom_exp = 'vtk/' // TRIM(DOM%EXPT%nom_exp) // '.'// TRIM(ADJUSTL(inttostr(nfile)))  // '.vtk'
!
! Création du fichier
OPEN(3,FILE=nom_exp ,&
    STATUS='replace', ACTION='write' )
!
!
!----------------------------------------------------------------------!
! ... et écriture de l'en-tete
WRITE(3,'(A)'),'# vtk DataFile Version 3.0'
WRITE(3,'(A,A,F12.5,A)'),TRIM(nom_exp),' - ti = ',DOM%NUM%ti,'s'
WRITE(3,'(A)'),'ASCII'
WRITE(3,'(A)'),'DATASET UNSTRUCTURED_GRID'
WRITE(3,'(A)'),''
!
!
!----------------------------------------------------------------------!
! CONNECTIVITES
!
! Ecriture des positions des sommets
!--- CAS MAILLAGE AVEC DEPLACEMENT (STRUCTURE)
WRITE(3,'(A,I8,A)'),'POINTS ',nsom,' float'
!
IF ((DOM%NUM%STRUCTURE == 'on') .AND. (DOM%EXPT%deform_mlg == 1)) THEN
    
    ! Interpolation des deplacements aux sommets
    U_struct_som(:,:) = 0.0_dp
    CALL expt_interp_struct( DOM%RSL%U, ncel, &
                             U_struct_som, nsom, &
                             DOM%MLG, DOM%CDT )
    U_struct_som = U_struct_som * DOM%EXPT%deform_amp
    U_struct_som = U_struct_som + DOM%MLG%SOMMET
    ! Ecriture des positions sommets fixes
    DO isom = 1,nsom
        WRITE(3,'(E14.6,E14.6,E14.6)'), U_struct_som(isom,1), &
        U_struct_som(isom,2), U_struct_som(isom,3)
    END DO
    
ELSE
    
    ! Ecriture des positions sommets fixes
    DO isom = 1,nsom
     WRITE(3,'(E14.6,E14.6,E14.6)'),DOM%MLG%SOMMET(isom,1), &
     DOM%MLG%SOMMET(isom,2),DOM%MLG%SOMMET(isom,3)
    END DO
    
END IF
WRITE(3,*),''
!
!
! Nombres de cellules
nval = SUM( DOM%MLG%CEL2SOM(:,2) ) + ncel
WRITE(3,'(A,I8,I8)'),'CELLS ',ncel,nval
!
! Connectivite des cellules
DO icel = 1,ncel
    ! Nb de sommets par cellule
    nsom_cel = DOM%MLG%CEL2SOM(icel,2)

    ! Points de chaque cellule
    ALLOCATE( cel2som( nsom_cel ))
    cel2som = DOM%MLG%CEL2SOM(icel,3:nsom_cel+2)
    cel2som(:) = cel2som(:) - 1

    ! Ecriture
    WRITE(3,*), nsom_cel, cel2som
    DEALLOCATE( cel2som )
END DO
WRITE(3,*),''
!
!
! Cellules de type : 
WRITE(3,'(A,I6)'),'CELL_TYPES ',ncel
DO icel = 1,ncel
    
    ! Cas 3D :
    IF (DOM%MLG%dim_simu == '3D') THEN
    
        ! Nb de sommets par cellule
        nsom_cel = DOM%MLG%CEL2SOM(icel,2)
        
        SELECT CASE (nsom_cel)
            CASE(4) ! Type tetraedre
                WRITE(3,*),'10'
            CASE(5) ! Type pyramide
                WRITE(3,*),'14'
            CASE(6) ! Type prisme
                WRITE(3,*),'13'
            CASE(8) ! Type hexaedre
                WRITE(3,*),'12'
            CASE DEFAULT
                CALL print_err('Type de cellule non prise en charge par modethec')
        END SELECT
    
    ! Cas 2D :
    ELSE
        ! Type polygone general
        WRITE(3,*),'7'
    
    END IF
END DO
!
!----------------------------------------------------------------------!
! DONNEES
!
! Cas centre sur cellule
IF (DOM%EXPT%cellule == 1) THEN
 WRITE(3,'(A)'),''
 WRITE(3,'(A,I6)'), 'CELL_DATA ',ncel

! Cas centre sur sommet
ELSE IF (DOM%EXPT%sommet == 1) THEN
 WRITE(3,'(A)'),''
 WRITE(3,'(A,I6)'), 'POINT_DATA ',nsom
 
END IF

  
! Boucle sur chaque variable exportée demandée
nexpt = DOM%EXPT%nexpt
DO iexpt = 1,nexpt
 ! Nom et colonne de la variable
 nom_var = DOM%EXPT%NOM_VAR_ETAT(iexpt)
 col_var = DOM%EXPT%COL_VAR_EXP(iexpt)
 nom_var_exp = ADJUSTL(DOM%EXPT%NOM_VAR_EXP(iexpt))
 idx_split = INDEX( nom_var_exp , ' ' )
 nom_var_exp = nom_var_exp(1:idx_split)
 
 WRITE(3,'(A,A,A)'), 'SCALARS ', TRIM(nom_var_exp), ' float'
 WRITE(3,'(A)'), 'LOOKUP_TABLE default'

 ! Récupération de la valeur aux cellules
 CALL expt_parser( U_cel, ncel, nom_var, col_var, DOM )

 ! Cas centre sur cellule
 IF (DOM%EXPT%cellule == 1) THEN
  WRITE(3,form_exp), U_cel
 
 ! Cas centre sur sommet
 ELSE IF (DOM%EXPT%sommet == 1) THEN
  CALL expt_interp_som( U_cel, ncel, U_som, nsom, DOM%MLG )
  WRITE(3,form_exp), U_som
  
 END IF
END DO


CLOSE(3) ! et fermeture du fichier .dat

END SUBROUTINE expt_vtk
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_expt_vtk
