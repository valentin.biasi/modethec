!> Calcul des flux advectifs pour l'ensemble des cellules du domaine i
MODULE mod_advection
!
USE mod_cst, ONLY : DP, IP, eps_min
USE mod_print, ONLY : print_err
USE mod_interpolate, ONLY : grad_cel, interp_one_fac
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!----------------------------------------------------------------------!
!> Calcul des flux advectifs pour l'ensemble des cellules du domaine i
SUBROUTINE cyc_advection( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM

! Flux advectifs aux faces internes dans FL_darcy
CALL advection_int( DOM )

! Flux advectifs aux faces limites dans FL_darcy
CALL advection_lim( DOM )

! Ajout des flux advectifs dans dm_e
CALL advection_maj( DOM )

!
END SUBROUTINE cyc_advection

!----------------------------------------------------------------------!
!> Calcul des flux advectifs des faces internes
SUBROUTINE advection_int( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: ncel, id_fac, ifac_int, nfac_int
REAL(DP), DIMENSION(3) :: vfac, GiGj
REAL(DP) :: deb_G
INTEGER(IP) :: iesp, nesp
INTEGER(IP) :: cel_UP, cel_DOWN
CHARACTER (len=32) :: slope_limiter
REAL(DP) :: phi_UP, phi_DOWN
REAL(DP), DIMENSION(3) :: grad_phi_UP
REAL(DP), DIMENSION(DOM%MLG%ncel, 3, DOM%PHYS%nesp) :: grad_phi_mass
REAL(DP), DIMENSION(DOM%MLG%ncel, DOM%PHYS%nesp) :: phi_mass
REAL(DP), DIMENSION(DOM%MLG%ncel, 3) :: grad_phi_ener
REAL(DP), DIMENSION(DOM%MLG%ncel) :: phi_ener

!---- Récupérations ---------------------------------------------------!
slope_limiter = DOM%NUM%slope_limiter
nesp = DOM%PHYS%nesp
ncel = DOM%MLG%ncel

!> \todo A modifier pour interpolation de cellule à la volée
! Pour chaque espèce gazeuse ...
DO iesp = 1,nesp
IF (DOM%PHYS%ESP(iesp)%typ_esp == 'gaz') THEN

	! ... Calcul de phi_mass = rho_g * Y_i / Y_g
	phi_mass(:,iesp) = DOM%ETAT%rho_g(:) * DOM%ETAT%Y(:, iesp) / DOM%ETAT%Y_g(:)
	! ... Calcul de grad( phi_mass )
	CALL grad_cel( phi_mass(:,iesp), grad_phi_ener, ncel, DOM%MLG%CEL_ITP, DOM%MLG%dim_simu )
	grad_phi_mass(:,:,iesp) = grad_phi_ener
	
END IF
END DO

! Calcul de phi_ener = rho_g * h_g
phi_ener(:) = DOM%ETAT%rho_g(:) * DOM%ETAT%h_g(:)
CALL grad_cel( phi_ener(:), grad_phi_ener, ncel, DOM%MLG%CEL_ITP, DOM%MLG%dim_simu )


! Boucle pour chaque face interne
nfac_int = DOM%MLG%nfac_int
DO ifac_int = 1,nfac_int

    ! Indice absolu de la face
    id_fac = DOM%MLG%ID_FACINT(ifac_int)
    
    ! Veteur normal à id_fac
    vfac = DOM%MLG%VFAC(id_fac,:)
    
    ! v_g choisi aux faces
    !==> Flux = Aire * (vg_face.n)
    deb_G = DOT_PRODUCT(DOM%ETAT%v_g_itp(id_fac,:),vfac) * DOM%MLG%AIRE(id_fac)

	! Avec le debit de gaz, on cherche la cellule amont
	IF (deb_G >= 0.0_dp) THEN
		cel_UP = DOM%MLG%FAC2CEL(id_fac,3)
		cel_DOWN = DOM%MLG%FAC2CEL(id_fac,4)
	ELSE 
		cel_UP = DOM%MLG%FAC2CEL(id_fac,4)
		cel_DOWN = DOM%MLG%FAC2CEL(id_fac,3)
	END IF

	GiGj = DOM%MLG%G_XYZ(cel_DOWN,:) - DOM%MLG%G_XYZ(cel_UP,:)  ! Vecteur Gi -> Gj

	!--- Flux massiques de Darcy
	! Pour chaque espèce gazeuse ...
	DO iesp = 1,nesp
	IF (DOM%PHYS%ESP(iesp)%typ_esp == 'gaz') THEN

		! ... Récupération de phi_UP, phi_DOWN et grad_phi_UP
		phi_UP = phi_mass(cel_UP, iesp)
		phi_DOWN = phi_mass(cel_DOWN, iesp)
		grad_phi_UP = grad_phi_mass(cel_UP, :, iesp)

		! ... Calcul de phi interpolé = phi_UP + Psi(r_grad) * (phi_DOWN-Phi_UP)
		DOM%RSL%FL_darcy(id_fac,iesp) = phi_TVD(phi_UP, phi_DOWN, grad_phi_UP, GiGj, slope_limiter)
		! ... Calcul du flux = - deb_G * phi interpolé 
		DOM%RSL%FL_darcy(id_fac,iesp) = - deb_G * DOM%RSL%FL_darcy(id_fac,iesp)
		
	END IF
	END DO ! Fin iesp

	!--- Flux énergétiques de Darcy
	! ... Récupération de phi_UP, phi_DOWN et grad_phi_UP
	phi_UP = phi_ener(cel_UP)
	phi_DOWN = phi_ener(cel_DOWN)
	grad_phi_UP = grad_phi_ener(cel_UP, :)

	! ... Calcul de phi interpolé = phi_UP + Psi(r_grad) * (phi_DOWN-Phi_UP)
	DOM%RSL%FL_E_darcy(id_fac) = phi_TVD(phi_UP, phi_DOWN, grad_phi_UP, GiGj, slope_limiter)
	! ... Calcul du flux = - deb_G * phi interpolé 
	DOM%RSL%FL_E_darcy(id_fac) = - deb_G * DOM%RSL%FL_E_darcy(id_fac)

END DO ! Fin face interne


END SUBROUTINE advection_int
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul des flux advectifs des faces limites
!> \todo Debit impose avec les Yi des gaz
SUBROUTINE advection_lim( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: ilim, nlim
CHARACTER (len=32) :: masse_typ
CHARACTER (len=32) :: slope_limiter
REAL(DP) :: mg
INTEGER(IP) :: iesp, nesp
INTEGER(IP) :: ifac_limi, nfac_limi
INTEGER(IP) :: id_fac
CHARACTER (len=32) :: schema
REAL(DP) :: h_g_itp, rho_g_itp, Y_itp, Y_g_itp
!
!---- Récupérations ---------------------------------------------------!
slope_limiter = DOM%NUM%slope_limiter
nesp = DOM%PHYS%nesp
nlim = DOM%CDT%nlim
schema="TVD"

! Boucle pour chaque limite du système
DO ilim = 1,nlim
    
    nfac_limi = DOM%CDT%LIM(ilim)%nfac_limi
    
    ! En fonction du type de limite adoptée
    masse_typ = DOM%CDT%LIM(ilim)%masse_typ
    SELECT CASE (masse_typ)
    
        ! Cas Flux entrant imposé
        CASE ("debit")
            ! Pour chaque face limite
            DO ifac_limi = 1,nfac_limi
                id_fac = DOM%CDT%LIM(ilim)%ID_FACLIMi(ifac_limi)
                
                ! FLUX = Aire * DEBIT_imposé
                mg = DOM%MLG%AIRE(id_fac) * DOM%CDT%LIM(ilim)%Dimp(ifac_limi)
                
				! Interpolation aux faces limites
				CALL interp_one_fac( DOM%ETAT%h_g, h_g_itp, DOM%MLG%FAC_ITP(id_fac), DOM%MLG )
				CALL interp_one_fac( DOM%ETAT%Y_g, Y_g_itp, DOM%MLG%FAC_ITP(id_fac), DOM%MLG )
				

                ! Avec les propriétés aux faces
                ! Flux massiques de Darcy
                DO iesp = 1,nesp
                    IF (DOM%PHYS%ESP(iesp)%typ_esp == 'gaz') THEN

						! Interpolation aux faces de Y(iesp)
						CALL interp_one_fac( DOM%ETAT%Y(:,iesp), Y_itp, DOM%MLG%FAC_ITP(id_fac), DOM%MLG ) 

                        DOM%RSL%FL_darcy(id_fac,iesp) = mg * Y_itp / (Y_g_itp + 2*eps_min)
                    END IF
                END DO

                ! Flux ener de Darcy
				DOM%RSL%FL_E_darcy(id_fac) = mg * h_g_itp


            END DO
            
        ! Cas pression imposée
        CASE ("pression")
        
            ! Pour chaque face limite
            DO ifac_limi = 1,nfac_limi
                id_fac = DOM%CDT%LIM(ilim)%ID_FACLIMi(ifac_limi)
            
                ! Valeur de grad_P_itp a Pimp aux faces déjà calcule dans interp_P
                ! Donc valeur de v_g_itp aux limites deja calculé à Pimp
                ! mg = (v_g_itp.vfac) * Aire
                mg = DOT_PRODUCT(DOM%ETAT%v_g_itp(id_fac,:),DOM%MLG%VFAC(id_fac,:)) * DOM%MLG%AIRE(id_fac)
		
				! Interpolation aux faces limites
				CALL interp_one_fac( DOM%ETAT%h_g, h_g_itp, DOM%MLG%FAC_ITP(id_fac), DOM%MLG )
				CALL interp_one_fac( DOM%ETAT%rho_g, rho_g_itp, DOM%MLG%FAC_ITP(id_fac), DOM%MLG )
				CALL interp_one_fac( DOM%ETAT%Y_g, Y_g_itp, DOM%MLG%FAC_ITP(id_fac), DOM%MLG )

				! cel_UP = cel_faclim
				! Pression dominante aux faces limites => Prop aux faces limites
				! Flux massiques de Darcy
				DO iesp = 1,nesp
				IF (DOM%PHYS%ESP(iesp)%typ_esp == 'gaz') THEN

					! Interpolation aux faces de Y(iesp)
					CALL interp_one_fac( DOM%ETAT%Y(:,iesp), Y_itp, DOM%MLG%FAC_ITP(id_fac), DOM%MLG ) 

					DOM%RSL%FL_darcy(id_fac,iesp) = - mg * rho_g_itp * Y_itp / (Y_g_itp + 2*eps_min)
				END IF
				END DO

				! Flux ener de Darcy
				DOM%RSL%FL_E_darcy(id_fac) = - mg * rho_g_itp * h_g_itp

            END DO

        ! Cas autre = erreur
        CASE DEFAULT
            CALL print_err('Un parametre typ_masse n''est pas correctement renseigne.', 1)
            
    END SELECT

END DO ! Fin ilim

END SUBROUTINE advection_lim
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> MAJ de dm_e et dE avec les flux advectifs totaux
SUBROUTINE advection_maj( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: iesp, nesp
INTEGER(IP) :: ifac, nfac
INTEGER(IP) :: id_cel1, id_cel2
INTEGER(IP) :: nb_cel
!
!---- Récupérations ---------------------------------------------------!
nesp = DOM%PHYS%nesp

! Pour chaque face du domaine
nfac = DOM%MLG%nfac
DO ifac = 1,nfac
    
    ! Indices des cellules 1 (et 2 si interne) adjacentes à ifac
    nb_cel = DOM%MLG%FAC2CEL(ifac,2)
    id_cel1 = DOM%MLG%FAC2CEL(ifac,3)
    id_cel2 = DOM%MLG%FAC2CEL(ifac,4)
    
    ! Ajout a dm_e
    ! Pour chaque espece gazeuse
    DO iesp = 1,nesp
    IF (DOM%PHYS%ESP(iesp)%typ_esp == 'gaz') THEN
        
        ! Ajout à dm_e pour cel1
        DOM%RSL%dm_e(id_cel1,iesp) = DOM%RSL%dm_e(id_cel1,iesp) + DOM%RSL%FL_darcy(ifac,iesp)
        ! Ajout à dm_e pour cel2
        IF (nb_cel == 2) THEN
            DOM%RSL%dm_e(id_cel2,iesp) = DOM%RSL%dm_e(id_cel2,iesp) - DOM%RSL%FL_darcy(ifac,iesp)
        END IF
        
    END IF
    END DO
    
    ! Ajout a dE pour le bilan ener total
    ! Ajout à dE pour cel1
    DOM%RSL%dE(id_cel1) = DOM%RSL%dE(id_cel1) + DOM%RSL%FL_E_darcy(ifac)
    ! Ajout à dE pour cel2
    IF (nb_cel == 2) THEN
        DOM%RSL%dE(id_cel2) = DOM%RSL%dE(id_cel2) - DOM%RSL%FL_E_darcy(ifac)
    END IF
    
END DO

!
END SUBROUTINE advection_maj
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Fonction de calcul du limiteur Psi en fonction r_grad
FUNCTION get_psi_limiter(r_grad, slope_limiter) result(Psi)
!
!---- Déclarations ----------------------------------------------------!
REAL(DP) :: r_grad
CHARACTER(len=32) :: slope_limiter
REAL(DP) :: Psi

! Sélection du type de limiteur de pente : schéma TVD non-structuré (Darwish and Moullaked 2003)
SELECT CASE (slope_limiter)

	! Cas Upwind
	CASE ("upwind")
	
		Psi = 0.0_dp

	! Cas Downwind
	CASE ("downwind")
	
		Psi = 2.0_dp

	! Cas Central
	CASE ("central")
	
		Psi = 1.0_dp

	! Cas SOU : Second Order Upwind
	CASE ("sou")
	
		Psi = r_grad
		Psi = MIN(Psi, 2.0_dp)
		Psi = MAX(0.0_dp, Psi)

	! Limiteur Min-Mod (Roe 1986)
	CASE ("minmod")
	
		Psi = MIN(r_grad, 1.0_dp)
		Psi = MAX(0.0_dp, Psi)

	! Limiteur Superbee (Roe 1986)
	CASE ("superbee")
	
		Psi = MAX(MIN(2.0_dp*r_grad, 1.0_dp), MIN(r_grad, 2.0_dp))
		Psi = MAX(0.0_dp, Psi)

	! Limiteur Van Leer (Van Leer 1974)
	CASE ("van_leer")
		Psi = r_grad + ABS(r_grad)
		Psi = Psi / (1.0_dp + ABS(r_grad))
		Psi = MAX(0.0_dp, Psi)

	! Limiteur Van Albada (Van Albada 1982)
	CASE ("van_albada")
		Psi = r_grad**2 + r_grad
		Psi = Psi / (r_grad**2 + 1.0_dp)
		Psi = MAX(0.0_dp, Psi)

	! Limiteur Ospre (Waterson and Deconinck 1995)
	CASE ("ospre")
		Psi = r_grad**2 + r_grad
		Psi = 1.5_dp * Psi
		Psi = Psi / (1.0_dp + r_grad + r_grad**2)
		Psi = MAX(0.0_dp, Psi)

	! Sinon : nom limiteur non reconnu
	CASE DEFAULT
		CALL print_err('Flux limiter undefined: '//TRIM(slope_limiter),1)
		
END SELECT

RETURN

END FUNCTION get_psi_limiter
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Fonction de calcul du limiteur Psi en fonction r_grad
FUNCTION phi_TVD(phi_UP, phi_DOWN, grad_phi_UP, GiGj, slope_limiter) result(phi_itp)
!
!---- Déclarations ----------------------------------------------------!
REAL(DP) :: phi_UP, phi_DOWN
REAL(DP), DIMENSION(3) :: grad_phi_UP, GiGj
CHARACTER(len=32) :: slope_limiter
REAL(DP) :: phi_itp
REAL(DP) :: r_grad

! Calcul de r_grad en non_structuré : r_grad = 2*grad_phi_UP.GiGj/(phi_DOWN-Phi_UP) - 1
IF (ABS(phi_DOWN - phi_UP) > 10*eps_min) THEN
	r_grad = (2.0_dp*DOT_PRODUCT(grad_phi_UP(:),GiGj))
	r_grad = r_grad / ( phi_DOWN - phi_UP )
	r_grad = r_grad - 1.0_dp
ELSE
	r_grad = 1.0_dp
END IF

! Calcul de phi interpolé = phi_UP + Psi(r_grad) * (phi_DOWN-Phi_UP)
phi_itp = phi_DOWN - phi_UP
phi_itp = phi_itp * 0.5_dp * get_psi_limiter(r_grad, slope_limiter)
phi_itp = phi_itp + phi_UP

RETURN

END FUNCTION phi_TVD
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_advection
