!> Calcul des termes sources pour l'ensemble des cellules du domaine i
MODULE mod_source
!
USE mod_cst, ONLY : DP, IP, eps_min
USE mod_print, ONLY : print_err
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Calcul des termes sources pour l'ensemble des cellules du domaine i
SUBROUTINE cyc_source( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: nesp 

nesp = DOM%PHYS%nesp

DOM%RSL%dm_e(:,:) = DOM%RSL%dm_e(:,:) + DOM%ETAT%w_e(:,:) * SPREAD(DOM%MLG%VOLUME,2,nesp)

DOM%RSL%dE(:) = DOM%RSL%dE(:) + DOM%ETAT%wQ(:) * DOM%MLG%VOLUME

!
END SUBROUTINE cyc_source
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_source
