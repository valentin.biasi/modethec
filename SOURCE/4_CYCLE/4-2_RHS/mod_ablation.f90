!> Mise à jour des quantités conservées (énergie et masses) en cas de déformation de maillage
MODULE mod_ablation
!
USE mod_cst, ONLY : DP, IP
USE mod_update, ONLY : cyc_update_DR
USE mod_print, ONLY : print_err
USE mod_algebra, ONLY : norm
!
USE mod_mesh_volu, ONLY : mesh_volu
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!----------------------------------------------------------------------!
!> Calcul des flux ablatifs pour l'ensemble des cellules du domaine i
SUBROUTINE cyc_ablation( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: ncel

! Recuperation
ncel = DOM%MLG%ncel

! RAZ des flux
DOM%RSL%FL_ener_def (:) = 0.0_dp
DOM%RSL%FL_masse_def (:,:) = 0.0_dp
DOM%RSL%FL_Vol(:) = 0.0_dp

! Flux de masses et d'energies aux faces internes
CALL ablation_lim( DOM )

! Flux de masses et d'energies aux faces limites
CALL ablation_int( DOM )

! MAJ des masses, energies et volumes des cellules
CALL ablation_maj( DOM )

! MAJ des grandeurs intensives 
CALL cyc_update_DR( DOM )
!
END SUBROUTINE cyc_ablation
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul des flux ablatifs des faces internes
SUBROUTINE ablation_int( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: id_fac, ifac_int, nfac_int
INTEGER(IP) :: id_cel1, id_cel2, isom
REAL(DP), DIMENSION(3) :: vfac, Df
REAL(DP) :: deb_G, CFL, CFL_AB
INTEGER(IP) :: cel_UP
INTEGER(IP) :: iesp, nesp

! Recuperation
nesp = DOM%PHYS%nesp
nfac_int = DOM%MLG%nfac_int
CFL_AB = DOM%NUM%CFL_AB
CFL = 0.0_dp


! Boucle pour chaque face interne
DO ifac_int = 1,nfac_int

    ! Indice absolu de la face
    id_fac = DOM%MLG%ID_FACINT(ifac_int)
    
    ! Indice de la cel1 et la cel2
    id_cel1 = DOM%MLG%FAC2CEL(id_fac,3)
    id_cel2 = DOM%MLG%FAC2CEL(id_fac,4)
    
    ! Veteur normal à id_fac
    vfac = DOM%MLG%VFAC(id_fac,:)
    
    ! Calcul du deplacement au centre de la face 
    Df = 0.0_dp
    DO isom = 1,DOM%MLG%FAC( id_fac,2 )
        Df = Df + DOM%ETAT%D_SOM( DOM%MLG%FAC( id_fac,2 + isom ),: )
    END DO
    Df = Df/DOM%MLG%FAC( id_fac,2 )

    !==> Flux = Aire * (Vf_face.n)
    deb_G = DOT_PRODUCT(Df,vfac) * DOM%MLG%AIRE(id_fac)

    ! Application du schéma UPWIND :
    ! Avec le debit de volume, on cherche la cellule aval du mouvement (schema inverse d'un flux de gaz)
    IF (deb_G >= 0.0_dp) THEN
        cel_UP = id_cel2
    ELSE 
        cel_UP = id_cel1
    END IF
   
    ! Flux massiques du a la deformation de maillage
    DO iesp = 1,nesp
        ! Determination du flux de masse aux faces interieures
        DOM%RSL%FL_masse_def(id_fac,iesp) = deb_G * DOM%ETAT%rho_e(cel_UP,iesp) * DOM%ETAT%phi(cel_UP,iesp) 
    END DO

    ! Determination du flux d'energie aux faces interieures
    DOM%RSL%FL_ener_def(id_fac) = deb_G * DOM%ETAT%rho(cel_UP) * DOM%ETAT%h(cel_UP)
        
    ! Determination du flux de volume
    DOM%RSL%FL_Vol (id_fac) = deb_G    

    ! Caclul du CFL max
    CFL = max (CFL, norm(Df)/ DOM%MLG%dL_min(cel_UP))  
END DO

! Avertissement si le critere de stabilite n'est pas respecte
IF (CFL > CFL_AB) THEN
    CALL print_err("Le CFL de deformation est superieur au CFL_AB max indique dans le fichier prm" )
    write(*,*), CFL
END IF
!
END SUBROUTINE ablation_int
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul des flux ablatifs des faces limites
SUBROUTINE ablation_lim( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: id_fac, ifac, nfac_limi
INTEGER(IP) :: id_cel
REAL(DP), DIMENSION(3) :: vfac, Df
REAL(DP) :: deb_G, CFL, CFL_AB
INTEGER(IP) :: cel_UP, ilim, nlim
INTEGER(IP) :: iesp, nesp, isom

nlim = DOM%CDT%nlim
nesp = DOM%PHYS%nesp
CFL_AB = DOM%NUM%CFL_AB
CFL = 0.0_dp

! Boucle sur chaque limite
DO ilim = 1, nlim

    ! Boucle pour chaque face limite
    nfac_limi = DOM%CDT%LIM( ilim )%nfac_limi
    DO ifac = 1,nfac_limi

        ! Indice absolu de la face
        id_fac =  DOM%CDT%LIM( ilim )%ID_FACLIMi( ifac )
        
        ! Indice de la cel
        id_cel = DOM%MLG%FAC2CEL(id_fac,3)
        
        ! Veteur normal à id_fac
        vfac = DOM%MLG%VFAC(id_fac,:)
        ! Calcul du deplacement au centre de la face (les faces limites ont 3 ou 4 sommets)
        Df = 0.0_dp
        DO isom = 1,DOM%MLG%FAC( id_fac,2 )
            Df = Df + DOM%ETAT%D_SOM( DOM%MLG%FAC( id_fac,2 + isom ),: )
        END DO
        Df = Df/DOM%MLG%FAC( id_fac,2 )        
          
        !==> Flux = Aire * (Df_face.n)
        deb_G = DOT_PRODUCT(Df,vfac) * DOM%MLG%AIRE(id_fac)
        
        ! Application du schéma UPWIND :
        cel_UP = id_cel
       
        ! Flux massiques du a la deformation de maillage
        DO iesp = 1,nesp
            ! Determination du flux de masse aux faces limites
            DOM%RSL%FL_masse_def( id_fac,iesp ) = deb_G * DOM%ETAT%rho_e( cel_UP,iesp )* DOM%ETAT%phi(cel_UP,iesp) 
        END DO

        ! Determination du flux d'energie a la face limite (dépend du type de modèle d'ablation et du schéma DR utilisé)
        SELECT CASE (DOM%CDT%LIM( ilim )%abla_typ)
        
            CASE ("temp")    
                SELECT CASE (DOM%NUM%schema_DR)
                    ! Cas explicite : on enlève du système l'énergie de changement d'état et l'énergie thermique stockée
                    CASE ("euler-exp")
                        DOM%RSL%FL_ener_def(id_fac) =  deb_G * DOM%ETAT%rho(cel_UP) *( DOM%PHYS%MAT%h_abla + DOM%ETAT%h(cel_UP) + DOM%ETAT%Cp(cel_UP)*( DOM%PHYS%MAT%T_abla - DOM%ETAT%T(cel_UP) ))
                    ! Cas implicite : l'énergie de changement d'état n'entre pas dans le système donc on enlève uniquement l'énergie thermique stockée    
                    CASE ("theta-imp")  
                        DOM%RSL%FL_ener_def(id_fac) =  deb_G * DOM%ETAT%rho(cel_UP) * DOM%ETAT%h(cel_UP)
                        
                    CASE DEFAULT 
                        CALL print_err("le schéma DR n'est pas reconnue par le solveur ablation; mod_ablation/ablation_lim",1)                
                END SELECT
                
            CASE("meca") ! on soustrait l'énergie d'ablation + l'énergie thermique stockée
                DOM%RSL%FL_ener_def(id_fac) =  deb_G * DOM%ETAT%rho(cel_UP) *DOM%ETAT%Y_s(cel_UP)*( DOM%PHYS%MAT%h_abla + DOM%ETAT%h(cel_UP) )
                        
            CASE DEFAULT !cas d'un déplacement de paroi à vitesse imposée : il faut soustraire l'énergie thermique stockée 
                DOM%RSL%FL_ener_def(id_fac)= deb_G * DOM%ETAT%rho(cel_UP) * DOM%ETAT%h(cel_UP)
        END SELECT
        ! Détermination du flux de volume (cohérent avec le flux de masse pour garder une masse volumique constante en absence de réaction ) 
        DOM%RSL%FL_Vol (id_fac) = deb_G      

        ! Calcul du CFL max 
        CFL = max (CFL, norm(Df)/ DOM%MLG%dL_min(cel_UP)) 
    END DO
END DO
! Avertissement si le critere de stabilite n'est pas respecte
IF (CFL > CFL_AB) THEN   
    CALL print_err("Le CFL de deformation est superieur au CFL_AB max indique dans le fichier prm" )
    write(*,*), CFL
END IF
!
END SUBROUTINE ablation_lim
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> MAJ de dm_e et dE avec les flux ablatifs
SUBROUTINE ablation_maj( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: iesp, nesp, nfac 
INTEGER(IP) :: icel, ncel, ifac_adj, nfac_adj, id_fac
REAL(DP) :: ori
REAL(DP), ALLOCATABLE, DIMENSION(:) :: VOL_TEMP


nesp = DOM%PHYS%nesp
ncel = DOM%MLG%ncel
nfac = DOM%MLG%nfac
ori = 0.0_dp

ALLOCATE (VOL_TEMP(ncel))

! initialisation de VOL_TEMP
VOL_TEMP = DOM%MLG%VOLUME

! Boucle sur les cellules
DO icel = 1, ncel
    nfac_adj = DOM%MLG%CEL2FAC(icel,2)
    ! Boucle sur les faces adjacentes de la cellule courante
    DO ifac_adj = 1, nfac_adj
        id_fac = ABS(DOM%MLG%CEL2FAC(icel,2+ifac_adj))
        
    ! Orientation de la face courante par rapport a la cellule courante
        ! Premier cas, la cellule courante est la cellule amont de la face  
        IF(DOM%MLG%FAC2CEL(id_fac,3) == icel) THEN
        
        ! ori est positif lorsque le vecteur directeur de la face est sortant par rapport a la cellule (donc oriente selon G1H)
        ori = sign(1.0_dp,DOT_PRODUCT( DOM%MLG%G1H( id_fac,: ),DOM%MLG%VFAC( id_fac,: ) ) )        
        
        ! Deuxieme cas : la cellule courante est la cellule aval de la face 
        ELSE IF ( DOM%MLG%FAC2CEL( id_fac,4 ) == icel ) THEN
        
        ! ori est positif lorsque le vecteur directeur de la face est sortant par rapport a la cellule (donc oriente selon - HG2)
        ori = sign(1.0_dp,DOT_PRODUCT( - DOM%MLG%G1H( id_fac,: ),DOM%MLG%VFAC( id_fac,: ) ) )             
        
        ELSE ! erreur, probleme de correspondance dans le tableau FAC2CEL
            CALL print_err ( "Une erreur est survenue dans la correspondance face/cellule FAC2CEL au cours de la mise a jour masse/energie suite a l'ablation (mod_ablation)" )
        END IF 
      
        ! Boucle sur les especes
        DO iesp = 1, nesp
            ! MAJ de la masse de l'espece courante dans la cellule courante
            DOM%RSL%m_e( icel,iesp ) = DOM%RSL%m_e( icel,iesp ) + ori * DOM%RSL%FL_masse_def( id_fac,iesp )
        END DO
        ! MAJ de l'energie de la cellule courante
        DOM%RSL%E( icel ) = DOM%RSL%E( icel ) + ori * DOM%RSL%FL_ener_def( id_fac )
               
        ! MAJ du volume de la cellule courante
        VOL_TEMP( icel ) = VOL_TEMP( icel ) + ori * DOM%RSL%FL_Vol( id_fac )          
    END DO    
END DO

! Appel routine mesh_volu pour calcul du nouveau CG
CALL mesh_volu( DOM )

! Recopie de la valeur des volumes calculés par les flux ( on ecrase la valeur calculee par la position des sommets suite a l'appel de mesh_volu )
DOM%MLG%VOLUME = VOL_TEMP 

IF (MINVAL(DOM%RSL%m/DOM%RSL%m_ini) < 0.5) DOM%RSL%nvoisin_plus_1 = .TRUE.
IF (MINVAL(DOM%MLG%VOLUME/DOM%RSL%volume_ini) < 0.5) DOM%RSL%nvoisin_plus_1 = .TRUE.

DEALLOCATE(VOL_TEMP)

END SUBROUTINE ablation_maj
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_ablation
