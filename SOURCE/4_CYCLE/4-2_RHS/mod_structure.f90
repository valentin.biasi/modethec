!> Calcul des flux qte de mouvement (structure) pour l'ensemble des cellules du domaine i
!> \warning Calcul structure valble en 2D uniquement
!> \warning Matrice de rigidite isotrope simple uniquement
!> \todo Faire une vraie condition d'axe pour le calcul structure
!> \todo Enlvever les structure_ext et co. pour rester sur bilan forces tot = 0
MODULE mod_structure
!
USE mod_cst, ONLY : DP, IP, eps_min
USE mod_print, ONLY : print_err
USE mod_interpolate, ONLY : grad_cel, grad_fac
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Calcul des flux qte de mouvement (structure) pour l'ensemble des cellules du domaine i
SUBROUTINE cyc_structure( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM

! flux qte de mouvement aux faces internes
CALL structure_int( DOM )

! flux qte de mouvement aux faces limites
CALL structure_lim( DOM )

! test test
CALL structure_vol( DOM )

! Ajout flux qte de mouvement dans dU
CALL structure_maj( DOM )

!
END SUBROUTINE cyc_structure
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul des flux qte de mouvement (structure) fixes pour les cellules limites (RHS du systeme lineaire)
SUBROUTINE cyc_structure_ext( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM


! flux qte de mouvement aux faces limites avec contraintes imposees (pas de cdt limite U fixe,...)
CALL structure_lim_sig( DOM )

! Calcul des flux qte de mouvement (structure) volumiques fixes pour toutes les cellules
CALL structure_vol( DOM )

! Ajout flux qte de mouvement dans dU
CALL structure_maj( DOM )

!
END SUBROUTINE cyc_structure_ext
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul des flux qte de mouvement (structure) volumiques fixes pour toutes les cellules
!> , c-à-d les forces de pressions internes
SUBROUTINE structure_vol( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: ncel
REAL(DP), DIMENSION(DOM%MLG%ncel) :: phig_x_P
REAL(DP), DIMENSION(DOM%MLG%ncel,3) :: grad_phig_x_P
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel

! Si advection actif
IF (DOM%NUM%ADVECTION == 'on') THEN

    ! Effort volumique sur les cellules = grad( phi_g * P_g )
    ! Fonctionne en axi comme en plan
    phig_x_P = DOM%ETAT%phi_g * DOM%ETAT%P
    CALL grad_cel( phig_x_P, grad_phig_x_P, ncel, DOM%MLG%CEL_ITP(:), DOM%MLG%dim_simu )


    ! Ajout aux qtés de mouvements par cellules
    DOM%RSL%dU(:,1) = DOM%RSL%dU(:,1) + (grad_phig_x_P(:,1) * DOM%MLG%VOLUME(:))
    DOM%RSL%dU(:,2) = DOM%RSL%dU(:,2) + (grad_phig_x_P(:,2) * DOM%MLG%VOLUME(:))

END IF

!
END SUBROUTINE structure_vol
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul des flux qte de mouvement (structure) des faces internes
SUBROUTINE structure_int( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_FACINT
!INTEGER(IP) :: ncel, nfac
INTEGER(IP) :: id_fac, ifac_int, nfac_int
REAL(DP), DIMENSION(3) :: vfac
!
!---- Récupérations ---------------------------------------------------!

! Indices des faces internes
ALLOCATE( ID_FACINT(DOM%MLG%nfac_int) )
ID_FACINT = DOM%MLG%ID_FACINT


!----------------------------------------------------------------------!                    
!---- Construction de eps_strain --------------------------------------!
CALL prop_eps_strain( DOM )


!----------------------------------------------------------------------!                    
!---- Construction de sig_stress --------------------------------------!

! Aux centres des cellules
DOM%ETAT%sig_stress(:,1) = DOM%ETAT%Crig(:,1) * DOM%ETAT%eps_strain(:,1) + DOM%ETAT%Crig(:,3) * DOM%ETAT%eps_strain(:,2)
DOM%ETAT%sig_stress(:,2) = DOM%ETAT%Crig(:,3) * DOM%ETAT%eps_strain(:,1) + DOM%ETAT%Crig(:,2) * DOM%ETAT%eps_strain(:,2)
DOM%ETAT%sig_stress(:,3) = 0.0_dp
DOM%ETAT%sig_stress(:,4) = 0.0_dp
DOM%ETAT%sig_stress(:,5) = 0.0_dp
DOM%ETAT%sig_stress(:,6) = DOM%ETAT%Crig(:,4) * DOM%ETAT%eps_strain(:,6)

! Aux centres des faces
DOM%ETAT%sig_stress_itp(:,1) = DOM%ETAT%Crig_itp(:,1) * DOM%ETAT%eps_strain_itp(:,1) + &
                                DOM%ETAT%Crig_itp(:,3) * DOM%ETAT%eps_strain_itp(:,2)
DOM%ETAT%sig_stress_itp(:,2) = DOM%ETAT%Crig_itp(:,3) * DOM%ETAT%eps_strain_itp(:,1) + &
                                DOM%ETAT%Crig_itp(:,2) * DOM%ETAT%eps_strain_itp(:,2)
DOM%ETAT%sig_stress_itp(:,3) = 0.0_dp
DOM%ETAT%sig_stress_itp(:,4) = 0.0_dp
DOM%ETAT%sig_stress_itp(:,5) = 0.0_dp
DOM%ETAT%sig_stress_itp(:,6) = DOM%ETAT%Crig_itp(:,4) * DOM%ETAT%eps_strain_itp(:,6) 



! Boucle pour chaque face interne
nfac_int = DOM%MLG%nfac_int
DO ifac_int = 1,nfac_int

    ! Indice absolu de la face
    id_fac = ID_FACINT(ifac_int)
    
    ! Veteur normal à id_fac
    vfac = DOM%MLG%VFAC(id_fac,:)
    
    !==> Flux = Aire * (sigma.n)    
    DOM%RSL%FL_struct( id_fac, 1 ) = (DOM%ETAT%sig_stress_itp(id_fac,1)*vfac(1) + &
                                      DOM%ETAT%sig_stress_itp(id_fac,6)*vfac(2)) * DOM%MLG%AIREs(id_fac) 
    DOM%RSL%FL_struct( id_fac, 2 ) = (DOM%ETAT%sig_stress_itp(id_fac,6)*vfac(1) + &
                                      DOM%ETAT%sig_stress_itp(id_fac,2)*vfac(2)) * DOM%MLG%AIREs(id_fac) 
    DOM%RSL%FL_struct( id_fac, 3 ) = 0.0_dp
    
END DO
!

END SUBROUTINE structure_int
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul des flux qte de mouvement (structure) des faces limites
SUBROUTINE structure_lim( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: ilim, nlim
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_FACLIMi
CHARACTER (len=32) :: struct_typ
INTEGER(IP) :: ifac_limi, nfac_limi
INTEGER(IP) :: id_fac
REAL(DP), DIMENSION(3) :: vfac


! Boucle pour chaque limite du système
nlim = DOM%CDT%nlim
DO ilim = 1,nlim
    
    nfac_limi = DOM%CDT%LIM(ilim)%nfac_limi
    
    ! Indice des faces de la limite i
    ALLOCATE( ID_FACLIMi(nfac_limi) )
    ID_FACLIMi = DOM%CDT%LIM(ilim)%ID_FACLIMi
    
    ! En fonction du type de limite adoptée
    struct_typ = DOM%CDT%LIM(ilim)%struct_typ
    SELECT CASE (struct_typ)
    
        ! Cas position face limite fixée
        CASE ("fixe")
            ! Pour chaque face limite
            DO ifac_limi = 1,nfac_limi
                id_fac = ID_FACLIMi(ifac_limi)

    ! Vecteur normal à id_fac
    vfac = DOM%MLG%VFAC(id_fac,:)
    
    !==> Flux = Aire * (sigma.n)    
    DOM%RSL%FL_struct( id_fac, 1 ) = (DOM%ETAT%sig_stress_itp(id_fac,1)*vfac(1) + &
               DOM%ETAT%sig_stress_itp(id_fac,6)*vfac(2)) * DOM%MLG%AIREs(id_fac) 
    DOM%RSL%FL_struct( id_fac, 2 ) = (DOM%ETAT%sig_stress_itp(id_fac,6)*vfac(1) + &
               DOM%ETAT%sig_stress_itp(id_fac,2)*vfac(2)) * DOM%MLG%AIREs(id_fac) 
    DOM%RSL%FL_struct( id_fac, 3 ) = 0.0_dp
    
            END DO
            
            
        ! Cas position face limite de type axe
        CASE ("axe")
            ! Pour chaque face limite
            DO ifac_limi = 1,nfac_limi
                id_fac = ID_FACLIMi(ifac_limi)

    ! Vecteur normal à id_fac
    vfac = DOM%MLG%VFAC(id_fac,:)
    
    !==> Flux = Aire * (sigma.n)    
    DOM%RSL%FL_struct( id_fac, 1 ) = (DOM%ETAT%sig_stress_itp(id_fac,1)*vfac(1) + &
               DOM%ETAT%sig_stress_itp(id_fac,6)*vfac(2)) * DOM%MLG%AIREs(id_fac)
    DOM%RSL%FL_struct( id_fac, 2 ) = (DOM%ETAT%sig_stress_itp(id_fac,6)*vfac(1) + &
               DOM%ETAT%sig_stress_itp(id_fac,2)*vfac(2)) * DOM%MLG%AIREs(id_fac)
    DOM%RSL%FL_struct( id_fac, 3 ) = 0.0_dp
       
            END DO
            
        ! Cas pression imposée (force normale a la surface)
        CASE ("pression")
        
            IF (DOM%NUM%ADVECTION == 'on') THEN
                CALL print_err('Impossible d''avoir une limite structure de type ')
                CALL print_err('pression avec calcul advectif active',1)
            END IF
            
            ! Pour chaque face limite
            DO ifac_limi = 1,nfac_limi
                id_fac = ID_FACLIMi(ifac_limi)
                
                ! Vecteur normal à id_fac
    vfac = DOM%MLG%VFAC(id_fac,:)
            
    DOM%RSL%FL_struct( id_fac, 1 ) = - DOM%ETAT%P_itp(id_fac) * vfac(1) * DOM%MLG%AIREs(id_fac) 
    DOM%RSL%FL_struct( id_fac, 2 ) = - DOM%ETAT%P_itp(id_fac) * vfac(2) * DOM%MLG%AIREs(id_fac)
    DOM%RSL%FL_struct( id_fac, 3 ) = 0.0_dp
                
            END DO
            
            
        ! Cas contrainte imposée (dans le repere cartesien XYZ)
        CASE ("contrainte")
        
            ! Pour chaque face limite
            DO ifac_limi = 1,nfac_limi
                id_fac = ID_FACLIMi(ifac_limi)
                
                ! Vecteur normal à id_fac
    vfac = DOM%MLG%VFAC(id_fac,:)
            
                DOM%RSL%FL_struct( id_fac, 1 ) = - (DOM%CDT%LIM(ilim)%stress_imp(1) * vfac(1) + &
                                                     DOM%CDT%LIM(ilim)%stress_imp(2) * vfac(2)) * DOM%MLG%AIREs(id_fac) 
    DOM%RSL%FL_struct( id_fac, 2 ) = - (DOM%CDT%LIM(ilim)%stress_imp(1) * vfac(2) + &
                                                     DOM%CDT%LIM(ilim)%stress_imp(2) * vfac(1)) * DOM%MLG%AIREs(id_fac)
    DOM%RSL%FL_struct( id_fac, 3 ) = 0.0_dp
                
            END DO

        ! Cas autre = erreur
        CASE DEFAULT
            CALL print_err('un parametre struct_typ n est pas bien renseigne')
            
    END SELECT
    
    ! Désallocations
    DEALLOCATE( ID_FACLIMi )

END DO
!
END SUBROUTINE structure_lim
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul des qte de mouvement aux faces limites avec contraintes imposees
!> (pas de cdt limite U fixe,...)
SUBROUTINE structure_lim_sig( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: ilim, nlim
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_FACLIMi
CHARACTER (len=32) :: struct_typ
INTEGER(IP) :: ifac_limi, nfac_limi
INTEGER(IP) :: id_fac
REAL(DP), DIMENSION(3) :: vfac


! Boucle pour chaque limite du système
nlim = DOM%CDT%nlim
DO ilim = 1,nlim
    
    nfac_limi = DOM%CDT%LIM(ilim)%nfac_limi
    
    ! Indice des faces de la limite i
    ALLOCATE( ID_FACLIMi(nfac_limi) )
    ID_FACLIMi = DOM%CDT%LIM(ilim)%ID_FACLIMi
    
    ! En fonction du type de limite adoptée
    struct_typ = DOM%CDT%LIM(ilim)%struct_typ
    SELECT CASE (struct_typ)
    
        ! Cas position face limite fixée
        CASE ("fixe")
   ! Attention RIEN - c'est narmol
            
        ! Cas position face limite sur l'axe de symetrie
        CASE ("axe")
   ! Attention RIEN - c'est narmol
            
        ! Cas pression imposée (force normale a la surface)
        CASE ("pression")
            
            ! Pas avec l'advection
            IF (DOM%NUM%ADVECTION == 'on') THEN
                CALL print_err('Impossible d''avoir une limite structure de type ')
                CALL print_err('pression avec calcul advectif active',1)
            END IF
        
            ! Pour chaque face limite
            DO ifac_limi = 1,nfac_limi
                id_fac = ID_FACLIMi(ifac_limi)
                
                ! Vecteur normal à id_fac
    vfac = DOM%MLG%VFAC(id_fac,:)
            
    DOM%RSL%FL_struct( id_fac, 1 ) = - DOM%ETAT%P_itp(id_fac) * vfac(1) * DOM%MLG%AIREs(id_fac) 
    DOM%RSL%FL_struct( id_fac, 2 ) = - DOM%ETAT%P_itp(id_fac) * vfac(2) * DOM%MLG%AIREs(id_fac)
    DOM%RSL%FL_struct( id_fac, 3 ) = 0.0_dp
                
            END DO
            
            
        ! Cas contrainte imposée (dans le repere cartesien XYZ)
        CASE ("contrainte")
        
            ! Pour chaque face limite
            DO ifac_limi = 1,nfac_limi
                id_fac = ID_FACLIMi(ifac_limi)
                
                ! Vecteur normal à id_fac
    vfac = DOM%MLG%VFAC(id_fac,:)
            
    DOM%RSL%FL_struct( id_fac, 1 ) = - (DOM%CDT%LIM(ilim)%stress_imp(1) * vfac(1) + &
                                                     DOM%CDT%LIM(ilim)%stress_imp(2) * vfac(2)) * DOM%MLG%AIREs(id_fac) 
    DOM%RSL%FL_struct( id_fac, 2 ) = - (DOM%CDT%LIM(ilim)%stress_imp(1) * vfac(2) + &
                                                     DOM%CDT%LIM(ilim)%stress_imp(2) * vfac(1)) * DOM%MLG%AIREs(id_fac)
    DOM%RSL%FL_struct( id_fac, 3 ) = 0.0_dp
                
            END DO

        ! Cas autre = erreur
        CASE DEFAULT
            CALL print_err('un parametre struct_typ n est pas bien renseigne')
            
    END SELECT
    
    
    ! Désallocations
    DEALLOCATE( ID_FACLIMi )

END DO
!
END SUBROUTINE structure_lim_sig
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> MAJ de dU avec les flux qte de mouvement
SUBROUTINE structure_maj( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: ifac, nfac
INTEGER(IP) :: id_cel1, id_cel2
INTEGER(IP) :: nb_cel

! Pour chaque face du domaine
nfac = DOM%MLG%nfac
DO ifac = 1,nfac
    
    ! Indices des cellules 1 (et 2 si interne) adjacentes à ifac
    nb_cel = DOM%MLG%FAC2CEL(ifac,2)
    id_cel1 = DOM%MLG%FAC2CEL(ifac,3)
    id_cel2 = DOM%MLG%FAC2CEL(ifac,4)
    
    ! Ajout à dU pour cel1
    !DOM%RSL%dU(id_cel1,:) = DOM%RSL%dU(id_cel1,:) + DOM%RSL%FL_struct(ifac,:)
    DOM%RSL%dU(id_cel1,1) = DOM%RSL%dU(id_cel1,1) + DOM%RSL%FL_struct(ifac,1)
    DOM%RSL%dU(id_cel1,2) = DOM%RSL%dU(id_cel1,2) + DOM%RSL%FL_struct(ifac,2)
    ! Ajout à dE pour cel2
    IF (nb_cel == 2) THEN
        !DOM%RSL%dU(id_cel2,:) = DOM%RSL%dU(id_cel2,:) - DOM%RSL%FL_struct(ifac,:)
        DOM%RSL%dU(id_cel2,1) = DOM%RSL%dU(id_cel2,1) - DOM%RSL%FL_struct(ifac,1)
        DOM%RSL%dU(id_cel2,2) = DOM%RSL%dU(id_cel2,2) - DOM%RSL%FL_struct(ifac,2)
    END IF

END DO

!
END SUBROUTINE structure_maj
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul des tenseur de deformation eps_strain
SUBROUTINE prop_eps_strain( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: ncel, nfac
INTEGER(IP) :: ilim, nlim
CHARACTER (len=32) :: struct_typ
INTEGER(IP) :: nfac_limi, ifac_limi, id_cellimi, id_faclimi
REAL(DP), DIMENSION( DOM%MLG%ncel,3 ) :: grad_Ux, grad_Uy
REAL(DP), DIMENSION( DOM%MLG%nfac,3 ) :: grad_Ux_itp, grad_Uy_itp
REAL(DP) :: U_G_axe


!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
nfac = DOM%MLG%nfac
nlim = DOM%CDT%nlim


!----------------------------------------------------------------------!                    
!---- Calcul des gradients de U ---------------------------------------!

! Calcul des gradients pour Ux
CALL grad_cel( DOM%RSL%U(:,1), &    ! Données aux cel.
    grad_Ux, &                      ! Gradient cellules
    ncel, &                         ! Nombre cellules
    DOM%MLG%CEL_ITP(:), &           ! Type interp. cellules
    DOM%MLG%dim_simu )              ! Dimension

CALL grad_fac( DOM%RSL%U(:,1), &    ! Données aux cel.
    DOM%RSL%U_itp(:,1), &           ! Interpolées aux faces
    grad_Ux_itp, &                  ! Interpolées aux faces
    nfac, &                         ! Nombre de faces
    ncel, &                         ! Nombre de cellules
    DOM%MLG%nsom, &                 ! Nombre de sommets
    DOM%MLG%FAC_ITP(:), &           ! Type interp. faces
    DOM%MLG%CEL_ITP(:), &           ! Type interp. cellules
    DOM%MLG%SOM_ITP(:), &           ! Type interp. sommets
    DOM%MLG%dim_simu )              ! Dimension

! Calcul des gradients pour Uy
CALL grad_cel( DOM%RSL%U(:,2), &    ! Données aux cel.
    grad_Uy, &                      ! Gradient cellules
    ncel, &                         ! Nombre cellules
    DOM%MLG%CEL_ITP(:), &           ! Type interp. cellules
    DOM%MLG%dim_simu )              ! Dimension

CALL grad_fac( DOM%RSL%U(:,2), &    ! Données aux cel.
    DOM%RSL%U_itp(:,2), &           ! Interpolées aux faces
    grad_Uy_itp, &                  ! Interpolées aux faces
    nfac, &                         ! Nombre de faces
    ncel, &                         ! Nombre de cellules
    DOM%MLG%nsom, &                 ! Nombre de sommets
    DOM%MLG%FAC_ITP(:), &           ! Type interp. faces
    DOM%MLG%CEL_ITP(:), &           ! Type interp. cellules
    DOM%MLG%SOM_ITP(:), &           ! Type interp. sommets
    DOM%MLG%dim_simu )              ! Dimension

!----------------------------------------------------------------------!
!---- Gradient pour limite Dirichlet (fixe - axe ) --------------------!

! Boucle par limite
DO ilim = 1,nlim

    ! Nombre de faces par limite
    nfac_limi = DOM%CDT%LIM(ilim)%nfac_limi
    
    ! En fonction du type de limite adoptée
    struct_typ = DOM%CDT%LIM(ilim)%struct_typ
    SELECT CASE (struct_typ)
    
        ! Si la limite est 'fixe'
        CASE ('fixe')  
            
            ! Boucle pour chaque face de ilim
            DO ifac_limi = 1,nfac_limi
                
                ! ID des faces et cel associées 
                id_faclimi = DOM%CDT%LIM(ilim)%ID_FACLIMi(ifac_limi)
                id_cellimi = DOM%MLG%FAC2CEL(id_faclimi,3)
            
                ! Gradient de Ux suivant x aux parois avec U face fixe
                grad_Ux_itp( id_faclimi,1 ) = 0.0_dp - DOM%RSL%U( id_cellimi,1 )
                grad_Ux_itp( id_faclimi,1 ) = grad_Ux_itp( id_faclimi,1 ) * DOM%MLG%G1H(id_faclimi,1)
                grad_Ux_itp( id_faclimi,1 ) = grad_Ux_itp( id_faclimi,1 ) / DOM%MLG%n2_G1H(id_faclimi)
                
                ! Gradient de Ux suivant y aux parois avec U face fixe
                grad_Ux_itp( id_faclimi,2 ) = 0.0_dp - DOM%RSL%U( id_cellimi,1 )
                grad_Ux_itp( id_faclimi,2 ) = grad_Ux_itp( id_faclimi,2 ) * DOM%MLG%G1H(id_faclimi,2)
                grad_Ux_itp( id_faclimi,2 ) = grad_Ux_itp( id_faclimi,2 ) / DOM%MLG%n2_G1H(id_faclimi)
                
                ! Gradient de Uy suivant x aux parois avec U face fixe
                grad_Uy_itp( id_faclimi,1 ) = 0.0_dp - DOM%RSL%U( id_cellimi,2 )
                grad_Uy_itp( id_faclimi,1 ) = grad_Uy_itp( id_faclimi,1 ) * DOM%MLG%G1H(id_faclimi,1)
                grad_Uy_itp( id_faclimi,1 ) = grad_Uy_itp( id_faclimi,1 ) / DOM%MLG%n2_G1H(id_faclimi)
                
                ! Gradient de Uy suivant y aux parois avec U face fixe
                grad_Uy_itp( id_faclimi,2 ) = 0.0_dp - DOM%RSL%U( id_cellimi,2 )
                grad_Uy_itp( id_faclimi,2 ) = grad_Uy_itp( id_faclimi,2 ) * DOM%MLG%G1H(id_faclimi,2)
                grad_Uy_itp( id_faclimi,2 ) = grad_Uy_itp( id_faclimi,2 ) / DOM%MLG%n2_G1H(id_faclimi)
            
            END DO
            
            
        ! Si la limite est 'axe'
        CASE ('axe')  
            
            ! Boucle pour chaque face de ilim
            DO ifac_limi = 1,nfac_limi
                
                ! ID des faces et cel associées 
                id_faclimi = DOM%CDT%LIM(ilim)%ID_FACLIMi(ifac_limi)
                id_cellimi = DOM%MLG%FAC2CEL(id_faclimi,3)
                
                ! Deplacement dans lme repere (AXE , NORMAL)
                !U_G_axe = DOM%RSL%U( id_cellimi,1 ) * COS(DOM%MLG%angle_AXE) - &
                !          DOM%RSL%U( id_cellimi,2 ) * SIN(DOM%MLG%angle_AXE)
                U_G_axe = - DOM%RSL%U( id_cellimi,1 ) * SIN(DOM%MLG%angle_AXE) + &
                          DOM%RSL%U( id_cellimi,2 ) * COS(DOM%MLG%angle_AXE)
                 
                U_G_axe = (0.0_dp - U_G_axe) / DOM%MLG%dist_AXE_G(id_cellimi)
                
                
                ! Gradient de Ux suivant x aux parois avec U face axe
                grad_Ux_itp( id_faclimi,1 ) = SIN(DOM%MLG%angle_AXE) * SIN(DOM%MLG%angle_AXE) * U_G_axe
                
                ! Gradient de Ux suivant y aux parois avec U face axe
                grad_Ux_itp( id_faclimi,2 ) = COS(DOM%MLG%angle_AXE) * SIN(DOM%MLG%angle_AXE) * U_G_axe
                
                ! Gradient de Uy suivant x aux parois avec U face axe
                grad_Uy_itp( id_faclimi,1 ) = COS(DOM%MLG%angle_AXE) * SIN(DOM%MLG%angle_AXE) * U_G_axe
                
                ! Gradient de Uy suivant y aux parois avec U face axe
                grad_Uy_itp( id_faclimi,2 ) = COS(DOM%MLG%angle_AXE) * COS(DOM%MLG%angle_AXE) * U_G_axe
                
                ! Gradient de Ux et Uy suivant z
                grad_Ux_itp(id_faclimi,3) = 0.0_dp
                grad_Uy_itp(id_faclimi,3) = 0.0_dp

            END DO
            
        ! Sinon
        CASE DEFAULT
        
            ! Boucle pour chaque face de ilim
            DO ifac_limi = 1,nfac_limi
            
                ! ID des faces et cel associées 
                id_faclimi = DOM%CDT%LIM(ilim)%ID_FACLIMi(ifac_limi)
                
                ! Gradient nul sinon
                grad_Ux_itp( id_faclimi,: ) = 0.0_dp 
                grad_Uy_itp( id_faclimi,: ) = 0.0_dp 
                
            END DO
            
    END SELECT
            
            
END DO


!----------------------------------------------------------------------!                    
!---- Construction de eps_strain --------------------------------------!

! Aux centres des cellules
DOM%ETAT%eps_strain(:,1) = grad_Ux(:,1)
DOM%ETAT%eps_strain(:,2) = grad_Uy(:,2)
DOM%ETAT%eps_strain(:,3) = 0.0_dp
DOM%ETAT%eps_strain(:,4) = 0.0_dp
DOM%ETAT%eps_strain(:,5) = 0.0_dp
DOM%ETAT%eps_strain(:,6) = 0.5_dp * ( grad_Ux(:,2) + grad_Uy(:,1) )

! Aux centres des faces
DOM%ETAT%eps_strain_itp(:,1) = grad_Ux_itp(:,1)
DOM%ETAT%eps_strain_itp(:,2) = grad_Uy_itp(:,2)
DOM%ETAT%eps_strain_itp(:,3) = 0.0_dp
DOM%ETAT%eps_strain_itp(:,4) = 0.0_dp
DOM%ETAT%eps_strain_itp(:,5) = 0.0_dp
DOM%ETAT%eps_strain_itp(:,6) = 0.5_dp * ( grad_Ux_itp(:,2) + grad_Uy_itp(:,1) )


!----------------------------------------------------------------------!                    
!---- Ajout des termes de thermo-elasticite ---------------------------!
IF (DOM%PHYS%MAT%meca_typ == 'isotrope') THEN
    
    ! Aux centres des cellules
    DOM%ETAT%eps_strain(:,1) = DOM%ETAT%eps_strain(:,1) - (DOM%PHYS%MAT%beta_iso * (DOM%ETAT%T - DOM%CDT%INI(1)%Tini))
    DOM%ETAT%eps_strain(:,2) = DOM%ETAT%eps_strain(:,2) - (DOM%PHYS%MAT%beta_iso * (DOM%ETAT%T - DOM%CDT%INI(1)%Tini))

    ! Aux centres des faces
    DOM%ETAT%eps_strain_itp(:,1) = DOM%ETAT%eps_strain_itp(:,1) - (DOM%PHYS%MAT%beta_iso * (DOM%ETAT%T_itp - DOM%CDT%INI(1)%Tini))
    DOM%ETAT%eps_strain_itp(:,2) = DOM%ETAT%eps_strain_itp(:,2) - (DOM%PHYS%MAT%beta_iso * (DOM%ETAT%T_itp - DOM%CDT%INI(1)%Tini))

ELSE IF (DOM%PHYS%MAT%meca_typ == 'transverse') THEN

    ! Aux centres des cellules
    DOM%ETAT%eps_strain(:,1) = DOM%ETAT%eps_strain(:,1) - (DOM%PHYS%MAT%beta_x * (DOM%ETAT%T - DOM%CDT%INI(1)%Tini))
    DOM%ETAT%eps_strain(:,2) = DOM%ETAT%eps_strain(:,2) - (DOM%PHYS%MAT%beta_y * (DOM%ETAT%T - DOM%CDT%INI(1)%Tini))

    ! Aux centres des faces
    DOM%ETAT%eps_strain_itp(:,1) = DOM%ETAT%eps_strain_itp(:,1) - (DOM%PHYS%MAT%beta_x * (DOM%ETAT%T_itp - DOM%CDT%INI(1)%Tini))
    DOM%ETAT%eps_strain_itp(:,2) = DOM%ETAT%eps_strain_itp(:,2) - (DOM%PHYS%MAT%beta_y * (DOM%ETAT%T_itp - DOM%CDT%INI(1)%Tini))
    
END IF
!
!
END SUBROUTINE prop_eps_strain
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul des flux qte de mouvement (structure) pour les cellules internes (LHS du systeme lineaire)
SUBROUTINE cyc_structure_cel( DOM, icel )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: icel
INTEGER(IP) :: ifac_adj, nfac_adj, id_fac
REAL(DP), DIMENSION(3) :: vfac
INTEGER(IP) :: id_cel1, id_cel2
INTEGER(IP) :: nb_cel
REAL(DP), DIMENSION( 3 ) :: grad_Ux_itp, grad_Uy_itp
INTEGER(IP) :: ilim, nlim
REAL(DP) :: U_G_axe
!
!---- Récupérations ---------------------------------------------------!
nfac_adj = DOM%MLG%CEL2FAC(icel,2)
nlim = DOM%CDT%nlim
!
!---- Initialisations -------------------------------------------------!
DOM%RSL%dU(:,:) = 0.0_dp
DOM%RSL%FL_struct(:,:) = 0.0_dp
DOM%ETAT%sig_stress_itp(:,:) = 0.0_dp
DOM%ETAT%eps_strain_itp(:,:) = 0.0_dp


! Si problème de convergence du process predicteur-correcteur
! On retire tous les termes extra-diagonaux
!C_12 = 0.0_dp
! NB : Converge moins rapidement mais plus surement



!----------------------------------------------------------------------!
!--- Construction des gradients de U pour les faces adjacentes a icel -!
DO ifac_adj = 1,nfac_adj
    
    grad_Ux_itp(:) = 0.0_dp
    grad_Uy_itp(:) = 0.0_dp

    id_fac = ABS( DOM%MLG%CEL2FAC(icel,ifac_adj+2) )
    
    ! Veteur normal à id_fac
    vfac = DOM%MLG%VFAC(id_fac,:)
    
    ! Indices des cellules 1 (et 2 si interne) adjacentes à ifac
    nb_cel = DOM%MLG%FAC2CEL(id_fac,2)
    id_cel1 = DOM%MLG%FAC2CEL(id_fac,3)
    id_cel2 = DOM%MLG%FAC2CEL(id_fac,4)
    
    
    !------------------------------------------------------------------!                    
    !---- Construction des gradients de U -----------------------------!
    ! Pour les cellules internes
    IF (nb_cel == 2) THEN
    
        ! Gradient de Ux suivant x
        grad_Ux_itp(1) = DOM%RSL%U( id_cel2,1 ) - DOM%RSL%U( id_cel1,1 )
        grad_Ux_itp(1) = grad_Ux_itp(1) * DOM%MLG%G1G2(id_fac,1)
        grad_Ux_itp(1) = grad_Ux_itp(1) / DOM%MLG%n2_G1G2(id_fac)
        
        ! Gradient de Ux suivant y
        grad_Ux_itp(2) = DOM%RSL%U( id_cel2,1 ) - DOM%RSL%U( id_cel1,1 )
        grad_Ux_itp(2) = grad_Ux_itp(2) * DOM%MLG%G1G2(id_fac,2)
        grad_Ux_itp(2) = grad_Ux_itp(2) / DOM%MLG%n2_G1G2(id_fac)
        
        ! Gradient de Uy suivant x
        grad_Uy_itp(1) = DOM%RSL%U( id_cel2,2 ) - DOM%RSL%U( id_cel1,2 )
        grad_Uy_itp(1) = grad_Uy_itp(1) * DOM%MLG%G1G2(id_fac,1)
        grad_Uy_itp(1) = grad_Uy_itp(1) / DOM%MLG%n2_G1G2(id_fac)
        
        ! Gradient de Uy suivant y
        grad_Uy_itp(2) = DOM%RSL%U( id_cel2,2 ) - DOM%RSL%U( id_cel1,2 )
        grad_Uy_itp(2) = grad_Uy_itp(2) * DOM%MLG%G1G2(id_fac,2)
        grad_Uy_itp(2) = grad_Uy_itp(2) / DOM%MLG%n2_G1G2(id_fac)
        
        ! Gradient de Ux et Uy suivant z
        grad_Ux_itp(3) = 0.0_dp
        grad_Uy_itp(3) = 0.0_dp
       
    
    !------------------------------------------------------------------!
    !--- Et pour les cellules limites de type U fixe ou axe -----------!
    ELSE
    DO ilim =1,nlim
        
        ! Type fixe
        IF (DOM%CDT%LIM(ilim)%struct_typ == 'fixe') THEN   
        IF (ANY( DOM%CDT%LIM(ilim)%ID_FACLIMi(:) == id_fac )) THEN
          
            ! Gradient de Ux suivant x
            grad_Ux_itp(1) = 0.0_dp - DOM%RSL%U( id_cel1,1 )
            grad_Ux_itp(1) = grad_Ux_itp(1) * DOM%MLG%G1H(id_fac,1)
            grad_Ux_itp(1) = grad_Ux_itp(1) / DOM%MLG%n2_G1H(id_fac)
            
            ! Gradient de Ux suivant y
            grad_Ux_itp(2) = 0.0_dp - DOM%RSL%U( id_cel1,1 )
            grad_Ux_itp(2) = grad_Ux_itp(2) * DOM%MLG%G1H(id_fac,2)
            grad_Ux_itp(2) = grad_Ux_itp(2) / DOM%MLG%n2_G1H(id_fac)
            
            ! Gradient de Uy suivant x
            grad_Uy_itp(1) = 0.0_dp - DOM%RSL%U( id_cel1,2 )
            grad_Uy_itp(1) = grad_Uy_itp(1) * DOM%MLG%G1H(id_fac,1)
            grad_Uy_itp(1) = grad_Uy_itp(1) / DOM%MLG%n2_G1H(id_fac)
            
            ! Gradient de Uy suivant y
            grad_Uy_itp(2) = 0.0_dp - DOM%RSL%U( id_cel1,2 )
            grad_Uy_itp(2) = grad_Uy_itp(2) * DOM%MLG%G1H(id_fac,2)
            grad_Uy_itp(2) = grad_Uy_itp(2) / DOM%MLG%n2_G1H(id_fac)
            
            ! Gradient de Ux et Uy suivant z
            grad_Ux_itp(3) = 0.0_dp
            grad_Uy_itp(3) = 0.0_dp
                    
        END IF
        END IF
        
        ! Type axe
        IF (DOM%CDT%LIM(ilim)%struct_typ == 'axe') THEN   
        IF (ANY( DOM%CDT%LIM(ilim)%ID_FACLIMi(:) == id_fac )) THEN
            
            ! Deplacement dans lme repere (AXE , NORMAL)
            !U_G_axe = DOM%RSL%U( id_cel1,1 ) * COS(DOM%MLG%angle_AXE) - &
            !          DOM%RSL%U( id_cel1,2 ) * SIN(DOM%MLG%angle_AXE)
            U_G_axe = - DOM%RSL%U( id_cel1,1 ) * SIN(DOM%MLG%angle_AXE) + &
                          DOM%RSL%U( id_cel1,2 ) * COS(DOM%MLG%angle_AXE)
             
            U_G_axe = (0.0_dp - U_G_axe) / DOM%MLG%dist_AXE_G(id_cel1)
            
            
            ! Gradient de Ux suivant x aux parois avec U face axe
            grad_Ux_itp(1) = SIN(DOM%MLG%angle_AXE) * SIN(DOM%MLG%angle_AXE) * U_G_axe
            
            ! Gradient de Ux suivant y aux parois avec U face axe
            grad_Ux_itp(2) = COS(DOM%MLG%angle_AXE) * SIN(DOM%MLG%angle_AXE) * U_G_axe
            
            ! Gradient de Uy suivant x aux parois avec U face axe
            grad_Uy_itp(1) = COS(DOM%MLG%angle_AXE) * SIN(DOM%MLG%angle_AXE) * U_G_axe
            
            ! Gradient de Uy suivant y aux parois avec U face axe
            grad_Uy_itp(2) = COS(DOM%MLG%angle_AXE) * COS(DOM%MLG%angle_AXE) * U_G_axe
            
            ! Gradient de Ux et Uy suivant z
            grad_Ux_itp(3) = 0.0_dp
            grad_Uy_itp(3) = 0.0_dp
                
        END IF
        END IF
    
    END DO

    END IF

    !------------------------------------------------------------------!                    
    !---- Construction de eps_strain ----------------------------------!
    ! Aux centres des faces
    DOM%ETAT%eps_strain_itp(id_fac,1) = grad_Ux_itp(1)
    DOM%ETAT%eps_strain_itp(id_fac,2) = grad_Uy_itp(2)
    DOM%ETAT%eps_strain_itp(id_fac,3) = 0.0_dp
    DOM%ETAT%eps_strain_itp(id_fac,4) = 0.0_dp
    DOM%ETAT%eps_strain_itp(id_fac,5) = 0.0_dp
    DOM%ETAT%eps_strain_itp(id_fac,6) = 0.5_dp * ( grad_Ux_itp(2) + grad_Uy_itp(1) )


    !------------------------------------------------------------------!                    
    !---- Construction de sig_stress ----------------------------------!
    ! Aux centres des faces
    DOM%ETAT%sig_stress_itp(id_fac,1) = DOM%ETAT%Crig_itp(id_fac,1) * DOM%ETAT%eps_strain_itp(id_fac,1) + &
                                         DOM%ETAT%Crig_itp(id_fac,3) * DOM%ETAT%eps_strain_itp(id_fac,2)
    DOM%ETAT%sig_stress_itp(id_fac,2) = DOM%ETAT%Crig_itp(id_fac,3) * DOM%ETAT%eps_strain_itp(id_fac,1) + &
                                         DOM%ETAT%Crig_itp(id_fac,2) * DOM%ETAT%eps_strain_itp(id_fac,2)
    DOM%ETAT%sig_stress_itp(id_fac,3) = 0.0_dp
    DOM%ETAT%sig_stress_itp(id_fac,4) = 0.0_dp
    DOM%ETAT%sig_stress_itp(id_fac,5) = 0.0_dp
    DOM%ETAT%sig_stress_itp(id_fac,6) = DOM%ETAT%Crig_itp(id_fac,4) * DOM%ETAT%eps_strain_itp(id_fac,6)

    
    !------------------------------------------------------------------!
    !==> Flux = Aire * (sigma.n)    
    DOM%RSL%FL_struct( id_fac, 1 ) = (DOM%ETAT%sig_stress_itp(id_fac,1)*vfac(1) + &
            DOM%ETAT%sig_stress_itp(id_fac,6)*vfac(2)) * DOM%MLG%AIREs(id_fac) 
    DOM%RSL%FL_struct( id_fac, 2 ) = (DOM%ETAT%sig_stress_itp(id_fac,6)*vfac(1) + &
            DOM%ETAT%sig_stress_itp(id_fac,2)*vfac(2)) * DOM%MLG%AIREs(id_fac) 
    DOM%RSL%FL_struct( id_fac, 3 ) = 0.0_dp


    
    !------------------------------------------------------------------!
    ! Ajout à dU pour cel1
    DOM%RSL%dU(id_cel1,1) = DOM%RSL%dU(id_cel1,1) + DOM%RSL%FL_struct(id_fac,1)
    DOM%RSL%dU(id_cel1,2) = DOM%RSL%dU(id_cel1,2) + DOM%RSL%FL_struct(id_fac,2)
    ! Ajout à dE pour cel2
    IF (nb_cel == 2) THEN
        DOM%RSL%dU(id_cel2,1) = DOM%RSL%dU(id_cel2,1) - DOM%RSL%FL_struct(id_fac,1)
        DOM%RSL%dU(id_cel2,2) = DOM%RSL%dU(id_cel2,2) - DOM%RSL%FL_struct(id_fac,2)
    END IF


END DO

!
END SUBROUTINE cyc_structure_cel
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_structure
