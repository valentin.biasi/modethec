!> Calcul des flux diffusifs pour l'ensemble des cellules du domaine
MODULE mod_diffusion
!
USE mod_cst, ONLY : DP, IP, eps_min, sigmaSB, hugem
USE mod_print, ONLY : print_err
USE mod_regression, ONLY : cyc_regression
USE mod_algebra, ONLY : norm, normL2, cross
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Calcul des flux diffusifs pour l'ensemble des cellules du domaine
SUBROUTINE cyc_diffusion( DOM,iiter )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: iiter

! Flux diffusifs aux faces internes dans FL_cond
CALL diffusion_int( DOM )

! Flux diffusifs aux faces limites dans FL_cond
CALL diffusion_lim( DOM,iiter )

! Ajout des flux diffusifs dans dE
CALL diffusion_maj( DOM )
!
END SUBROUTINE cyc_diffusion
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul des flux diffusifs des faces internes
SUBROUTINE diffusion_int( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_FACINT

! Indices des faces internes
ALLOCATE( ID_FACINT(DOM%MLG%nfac_int) )
ID_FACINT = DOM%MLG%ID_FACINT

! FLUX = - (-k*grad(T) . Vfac) * Aire
DOM%RSL%FL_cond(ID_FACINT) = + DOM%MLG%AIRE(ID_FACINT) * &
                SUM( DOM%ETAT%k_itp(ID_FACINT,:) * &
                DOM%ETAT%grad_T_itp(ID_FACINT,:) * &
                DOM%MLG%VFAC(ID_FACINT,:), 2)
!
END SUBROUTINE diffusion_int
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul des flux diffusifs des faces limites
SUBROUTINE diffusion_lim( DOM,iiter )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: ilim, nfac_limi, iiter
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_FACLIMi
CHARACTER (len=32) :: ener_typ

! Boucle pour chaque limite du système
DO ilim = 1,DOM%CDT%nlim

    nfac_limi = DOM%CDT%LIM(ilim)%nfac_limi

    ! Indice des faces de la limite i
    ALLOCATE( ID_FACLIMi(nfac_limi) )
    ID_FACLIMi = DOM%CDT%LIM(ilim)%ID_FACLIMi
    
    ! En fonction du type de limite adoptée
    ener_typ = DOM%CDT%LIM(ilim)%ener_typ
    SELECT CASE (ener_typ)
    
        ! Cas Flux entrant imposé
        CASE ("flux")  
                     
            ! FLUX = Aire * PHI_imposé
            DOM%RSL%FL_cond(ID_FACLIMi) = DOM%MLG%AIRE(ID_FACLIMi) * DOM%CDT%LIM(ilim)%Fimp(:)

            
        ! Cas température imposée
        CASE ("temp")
        
            ! Valeur de grad_T_itp a Timp aux faces déjà calcule dans interp_T
            ! FLUX = - (-k*grad(T) . Vfac) * Aire
            DOM%RSL%FL_cond(ID_FACLIMi) = + DOM%MLG%AIRE(ID_FACLIMi) * &
                SUM( DOM%ETAT%k_itp(ID_FACLIMi,:) * &
                DOM%ETAT%grad_T_itp(ID_FACLIMi,:) * &
                DOM%MLG%VFAC(ID_FACLIMi,:), 2)
        
        
        ! Flux = (Flux impose + Flux conv + Flux rad) * Aire
        CASE ("mixte")
                                
            ! Flux impose a alpha_rad pres
            DOM%RSL%FL_cond(ID_FACLIMi) = DOM%ETAT%alpha_rad(ID_FACLIMi) * DOM%CDT%LIM(ilim)%Fimp(:)

            ! + Flux convectif
            DOM%RSL%FL_cond(ID_FACLIMi) = DOM%RSL%FL_cond(ID_FACLIMi) + &
            DOM%CDT%LIM(ilim)%h_conv(:) * (DOM%CDT%LIM(ilim)%T_conv(:) - DOM%ETAT%T_itp(ID_FACLIMi))
            ! + Flux radiatif
            DOM%RSL%FL_cond(ID_FACLIMi) = DOM%RSL%FL_cond(ID_FACLIMi) + &
            sigmaSB * DOM%ETAT%alpha_rad(ID_FACLIMi) * (DOM%CDT%LIM(ilim)%T_rad(:) ** 4) - &
            sigmaSB * DOM%ETAT%eps_rad(ID_FACLIMi) * (DOM%ETAT%T_itp(ID_FACLIMi) ** 4)
                    
            DOM%RSL%FL_cond(ID_FACLIMi) = DOM%RSL%FL_cond(ID_FACLIMi) *  DOM%MLG%AIRE(ID_FACLIMi)
        

        ! Cas par défaut: erreur.
        CASE DEFAULT
            CALL print_err('Un parametre ener_typ n''est pas correctement renseigne',1)
    
    END SELECT
    
    ! Correction du flux traversant la frontière du dommaine en fonction des conditions d'ablation    
    IF ( ( DOM%NUM%ABLATION == 'on' ) ) THEN 
        CALL cyc_regression (DOM, ilim,iiter)         
    END IF
    
    DEALLOCATE( ID_FACLIMi )

END DO
!
END SUBROUTINE diffusion_lim
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> MAJ de dE avec les flux diffusifs totaux
SUBROUTINE diffusion_maj( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: ifac, nfac
INTEGER(IP) :: id_cel1, id_cel2
INTEGER(IP) :: nb_cel

! Pour chaque face du domaine
nfac = DOM%MLG%nfac
DO ifac = 1,nfac
    
    ! Indices des cellules 1 (et 2 si interne) adjacentes à ifac
    nb_cel = DOM%MLG%FAC2CEL(ifac,2)
    id_cel1 = DOM%MLG%FAC2CEL(ifac,3)
    id_cel2 = DOM%MLG%FAC2CEL(ifac,4)
    
    ! Ajout à dE pour cel1
    DOM%RSL%dE(id_cel1) = DOM%RSL%dE(id_cel1) + DOM%RSL%FL_cond(ifac)
    ! Ajout à dE pour cel2
    IF (nb_cel == 2) THEN
        DOM%RSL%dE(id_cel2) = DOM%RSL%dE(id_cel2) - DOM%RSL%FL_cond(ifac)
    END IF

END DO
!
END SUBROUTINE diffusion_maj
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_diffusion
