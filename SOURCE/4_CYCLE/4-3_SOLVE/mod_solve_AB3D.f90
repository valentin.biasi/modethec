!> Module pour la resolution du cycle des termes d'ablation.
MODULE mod_solve_AB3D
!
USE mod_algebra, ONLY : cross, norm, dot, INV_MAT2, INV_MAT3
USE mod_cst, ONLY : IP, DP, eps_min, coef_bin, Pi
USE mod_print, ONLY : print_err
USE mod_ablation, ONLY : cyc_ablation
USE mod_deform, ONLY : deform_laplacien, deform_spline, deform_maj_spline
USE mod_deform3d, ONLY : ablation_matrices_passage, ablation_lambdai, ablation_coord_loc, update_bezier_face_tri, &
    & rational_bezier, abl_alphai, abl_di, deform3D_ntag2, deform3D_ntag, deform3D_int, update_bezier_face_quad, hermite, &
    & find_face_lim_som, rational_bezier_pol
USE mod_mesh_recpt, ONLY : mesh_recpt
USE mod_dat_bld_cls, ONLY : dat_bld_cls
USE mod_mesh_volu, ONLY : mesh_volu
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Routine principale du solveur d'ablation
SUBROUTINE solve_AB3D ( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: isom, nsom_lim
REAL(DP) :: dt
!
!---- Récupérations ---------------------------------------------------!
nsom_lim = DOM%MLG%nsom_lim
dt = DOM%NUM%dt

! Mise a 0 des deplacements de sommets et des donnees de résolution
DOM%ETAT%D_SOM(:,:) = 0.0_dp
DOM%RSL%niter_eq_AB = 0
DOM%RSL%err_eq_AB = 0.0_dp
DOM%RSL%niter_proj_AB = 0
DOM%RSL%err_proj_AB = 0.0_dp
DOM%MLG%SOM_LIM%projection_surf = .FALSE.
DOM%MLG%SOM_LIM%projection_courb = .FALSE.
DOM%RSL%equilibrage = .FALSE.

! Calcul de D_SOM pour les sommets limites
CALL solve_AB_DSOM ( DOM )  

! On projette les sommets des faces aux surfaces
DO isom = 1, nsom_lim
    IF ( DOM%MLG%SOM_LIM(isom)%projection_courb ) CALL solve_AB_proj_curve ( DOM, isom )
    IF ( DOM%MLG%SOM_LIM(isom)%projection_surf ) CALL solve_AB_proj_face ( DOM, isom )
END DO
DOM%MLG%SOM_LIM%projection_courb = .FALSE.
DOM%MLG%SOM_LIM%projection_surf = .FALSE.
DOM%RSL%equilibrage = .TRUE.

! On remet à jour le maillage : l'equilibrage se fait sur le maillage ablaté
! TO DO : ne remettre a jour que ce qui a bougé !
DOM%MLG%SOMMET = DOM%MLG%SOMMET + DOM%ETAT%D_SOM
DOM%ETAT%D_SOM_ABL = DOM%ETAT%D_SOM
DOM%ETAT%D_SOM = 0.0_dp
CALL mesh_recpt( DOM )

! Pré-traitement pour savoir quels sommets on devra équilibrer
IF ( DOM%RSL%update_somvois ) THEN
    CALL sommet_to_move ( DOM )
END IF

! Equilibrage des sommets limites avec Ntag = 2
CALL deform3D_ntag2 ( DOM )

! Equilibrage des sommets limites avec Ntag = 1
IF ( DOM%MLG%dim_simu == '3D' ) CALL deform3D_ntag ( DOM ) 

! On projette les sommets limites sur les courbes ou les surfaces
DO isom = 1, nsom_lim
    IF ( DOM%MLG%SOM_LIM(isom)%projection_courb ) CALL solve_AB_proj_curve ( DOM, isom )
    IF ( DOM%MLG%SOM_LIM(isom)%projection_surf ) CALL solve_AB_proj_face ( DOM, isom )
END DO

! Equilibrage des sommets internes
CALL deform3D_int ( DOM )

! MAJ des deplacements de sommet :
DOM%MLG%SOMMET = DOM%MLG%SOMMET + DOM%ETAT%D_SOM
DOM%ETAT%D_SOM = DOM%ETAT%D_SOM + DOM%ETAT%D_SOM_ABL

! MAJ des quantités conservatives du systémes (masse, energie) et du volume
CALL cyc_ablation ( DOM )
! CALL mesh_volu( DOM ) ! quand on ne fait pas cyc_ablation

! MAJ des données de maillages (aires, CG,...) et des infos pour l'ablation
CALL mesh_recpt ( DOM )

! MAJ des champs de conditions limites speciales (dépendant de VFAC par exemple...)
CALL dat_bld_cls ( DOM )

END SUBROUTINE solve_AB3D
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Routine de calcul des déplacements limites D_SOM
SUBROUTINE solve_AB_DSOM ( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: ifac, isom, nsom, nb_tag, id_som, itag, id_face, ivois, compt_tags
INTEGER(IP) :: tag, tags, proj_face, id_som1, id_som2, pos, irete, proj_arete
REAL(DP) :: dt, aire, lambda_dep, PS, PS_arete, dist, dist_arete, max_dep_som
REAL(DP), DIMENSION(3) :: reg_som, dir, dir_intersec, orig_droite
REAL(DP), DIMENSION(3) :: sommet1, sommet2, cross_vect, cross_vect2
REAL(DP), DIMENSION(2,2) :: mat_inverse
REAL(DP), DIMENSION(3,3) :: mat_verif
REAL(DP), DIMENSION(2) :: second_terme
REAL(DP), DIMENSION(3) :: second_terme3
LOGICAL :: first
! Pour les tableaux suivant, on fixe la taille pour éviter ALLOC/DEALLOC (NTAGS < 10 & NTAG < 6)
INTEGER(IP), DIMENSION(6) :: tag_move
REAL(DP), DIMENSION(6) :: lambda_dep_vect
REAL(DP), DIMENSION(6,3) :: tag_move_vect, tag_vect_dir
INTEGER(IP), DIMENSION(10) :: tag_move_surf
REAL(DP), DIMENSION(10) :: tag_norm_dep_surf
REAL(DP), DIMENSION(10,3) :: tag_vect_dir_surf
!
!---- Récupérations ---------------------------------------------------!
dt = DOM%NUM%dt
nsom = DOM%MLG%nsom
!
DOM%ETAT%D_SOM_BRUT = 0.0_dp
!
! DOM%MLG%SOM_LIM%moved_abl = .FALSE.
DOM%MLG%SOM_LIM%projection_surf = .FALSE.
max_dep_som = 0.0_dp
!
! Boucle sur les sommets limites
DO isom = 1,DOM%MLG%nsom_lim
    nb_tag = MAXVAL( ABS( DOM%MLG%FACLIM_GRP(isom,:) ) )
    tag_move = 0
    tag_move_surf = 0
    tag_norm_dep_surf = 0.0_dp
    tag_vect_dir = 0.0_dp
    tag_vect_dir_surf = 0.0_dp
    tag_move_vect = 0.0_dp
    lambda_dep_vect = 0.0_dp
    lambda_dep = 0.0_dp
    id_som = DOM%MLG%SOMLIM2FACLIM(isom,1)

    ! Premiere boucle pour determiner la norme des déplacements de chaque tag ainsi que le nombre de
    ! face du tag qui se deplacent
    DO ifac = 1, DOM%MLG%SOMLIM2FACLIM( isom,2 )
        IF ( DOM%ETAT%taux_reg( DOM%MLG%SOMLIM2FACLIM(isom, 2+ifac) ) > 0.0_dp ) THEN
            tag = ABS( DOM%MLG%FACLIM_GRP(isom, ifac) )
            tags = ABS( DOM%MLG%FACLIM_GRP_SURF(isom, ifac) )
            id_face = DOM%MLG%SOMLIM2FACLIM(isom, 2+ifac)
            aire = DOM%MLG%AIRE( id_face )
            tag_norm_dep_surf( tags ) = tag_norm_dep_surf( tags ) + ABS(DOM%ETAT%taux_reg( id_face )/aire)
            tag_move(tag) = tag_move(tag) + 1
            tag_move_surf(tags) = tag_move_surf(tags) + 1
        END IF
    END DO

    ! Si aucun mouvement autour du sommet on passe le sommet
    IF ( MAXVAL(tag_norm_dep_surf) < 10*eps_min) CYCLE

    ! On retient que le sommet à bougé à cause de l'ablation et on regarde s'il avait bougé à l'itération N-1
    DOM%MLG%SOM_LIM(isom)%moved_abl = .TRUE.
    IF ( DOM%MLG%SOM_LIM(isom)%moved_abl .AND. .NOT. DOM%MLG%SOM_LIM(isom)%moved_abl_N1 ) DOM%RSL%update_somvois = .TRUE.

    ! Determination du vecteur directeur du deplacement
    DO ifac = 1, DOM%MLG%SOMLIM2FACLIM( isom,2 )
        IF ( DOM%ETAT%taux_reg( DOM%MLG%SOMLIM2FACLIM(isom,2+ifac) ) > eps_min) THEN
            tags = ABS( DOM%MLG%FACLIM_GRP_SURF( isom,ifac ) )
            id_face = DOM%MLG%SOMLIM2FACLIM( isom,2+ifac )
            dir = DOM%MLG%VFAC( id_face,: )
            aire = DOM%MLG%AIRE( id_face )
            ! Signe négatif car le vecteur dir est dirigé vers l'exterieur du domaine
            tag_vect_dir_surf( tags,: ) = tag_vect_dir_surf( tags,: ) - DOM%ETAT%taux_reg( id_face )/aire*dir/tag_norm_dep_surf(tags)
        END IF
    END DO

    ! On passe des directions de déplacements séparées par FACLIM_GRP_SURF à celle séparées par FACLIM_GRP
    compt_tags = 1
    DO ifac = 1, DOM%MLG%SOMLIM2FACLIM( isom,2 )
        tag = ABS( DOM%MLG%FACLIM_GRP(isom,ifac) )
        tags = ABS( DOM%MLG%FACLIM_GRP_SURF(isom,ifac) )
        IF (tags == compt_tags) THEN
            IF ( tag_move_surf(tags) /= 0 ) tag_vect_dir(tag,:) = tag_vect_dir(tag,:) + tag_norm_dep_surf(tags)*tag_vect_dir_surf(tags,:)/tag_move_surf(tags)
            compt_tags = compt_tags + 1
        END IF
    END DO

    ! On determine, pour chaque tag, de combien on se deplace le long du vecteur deplacement
    DO ifac = 1, DOM%MLG%SOMLIM2FACLIM( isom,2 )
        IF ( DOM%ETAT%taux_reg( DOM%MLG%SOMLIM2FACLIM(isom,2+ifac) ) > eps_min ) THEN
            tag = ABS( DOM%MLG%FACLIM_GRP(isom,ifac) )
            id_face = DOM%MLG%SOMLIM2FACLIM( isom, 2+ifac )
            dir = DOM%MLG%VFAC( id_face,:)
            aire = DOM%MLG%AIRE( id_face )
            lambda_dep = -(DOM%ETAT%taux_reg( id_face )/aire)/(tag_vect_dir(tag,1)*dir(1)+tag_vect_dir(tag,2)*dir(2)+tag_vect_dir(tag,3)*dir(3))
            IF (lambda_dep > lambda_dep_vect(tag)) lambda_dep_vect(tag) = lambda_dep
        END IF
    END DO
    DO itag = 1, nb_tag
        tag_move_vect( itag,: ) = lambda_dep_vect( itag )*tag_vect_dir( itag,: )
        DOM%ETAT%D_SOM_BRUT( id_som,: ) = DOM%ETAT%D_SOM_BRUT( id_som,: ) + tag_move_vect( itag,: )
    END DO

    lambda_dep = 0.0_dp
    IF ( nb_tag /= 1 ) THEN
        ! correction du vecteur deplacement 
        ! Différent de find_face_lim_som car on projette selon le plan normal au vecteur déplacement et non selon
        ! la normale des faces
        DO itag = 1, nb_tag
            IF ( lambda_dep_vect(itag) > eps_min ) THEN
                proj_face = 0
                dist = 0.0_dp
                first = .TRUE.
                PS_arete = 0.0_dp
                dist_arete = 0.0_dp
                proj_arete = 0

                ! 1: On detecte l'arete la "plus proche" du vecteur deplacement (que pour le cas "pic")
                IF ( DOM%MLG%FACLIM_GRP(isom,1) > eps_min ) THEN
                    DO irete = 1, DOM%MLG%SOM_LIM(isom)%nvoisin
                        sommet1 = DOM%MLG%SOMMET( DOM%MLG%SOM_ADJ_LIM( DOM%MLG%SOM_LIM(isom)%SOMMETS_LIM(irete),1 ), : ) - DOM%MLG%SOMMET( id_som,: )
                        sommet1 = sommet1/NORM(sommet1)
                        PS_arete = DOT_PRODUCT( sommet1, tag_move_vect(itag,:) )
                        IF ( PS_arete > dist_arete ) THEN
                            dist_arete = PS_arete
                            proj_arete = DOM%MLG%SOM_ADJ_LIM( DOM%MLG%SOM_LIM(isom)%SOMMETS_LIM(irete),1 )
                        END IF
                    END DO
                END IF

                ! 2: On choisit la face qui contient l'arete qui a la plus petite distance au point deplace
                DO ifac = 1, DOM%MLG%SOMLIM2FACLIM( isom,2 )
                    IF ( DOM%MLG%FACLIM_GRP(isom,ifac) /= itag ) THEN
                        ! calcul du produit scalaire DEP.NORMAL_FACE
                        id_face = DOM%MLG%SOMLIM2FACLIM( isom, 2+ifac )
                        IF ( DOM%MLG%FACLIM_GRP(isom,1) > eps_min ) THEN
                            ! Cas "pic"
                            IF ( ANY( DOM%MLG%FAC(id_face,3:) == proj_arete ) ) THEN
                                dir = DOM%MLG%VFAC( id_face, : )
                                PS = DOT_PRODUCT( tag_move_vect(itag,:) , dir(:) )
                                IF ( PS > eps_min ) THEN
                                    lambda_dep = ABS( ( tag_vect_dir(itag,1)*dir(1) + tag_vect_dir(itag,2)*dir(2) + tag_vect_dir(itag,3)*dir(3) ) / ( dir(1)**2 + dir(2)**2 + dir(3)**2) )
                                    IF ( lambda_dep < dist .OR. first ) THEN
                                    ! si projection plus petite que les autres on garde le numero de face
                                        first = .FALSE.
                                        dist = lambda_dep
                                        proj_face = id_face
                                    END IF
                                END IF
                            END IF
                        ELSE
                            ! Cas "cavite"
                            dir = DOM%MLG%VFAC( id_face,:)
                            PS = DOT_PRODUCT( tag_move_vect(itag,:) , dir(:) )
                            IF ( PS > eps_min ) THEN
                                lambda_dep = ABS( ( tag_move_vect(itag,1)*dir(1) + tag_move_vect(itag,2)*dir(2) + tag_move_vect(itag,3)*dir(3) ) / ( dir(1)**2 + dir(2)**2 + dir(3)**2) )
                                IF ( lambda_dep < dist .OR. first ) THEN
                                ! si projection plus petite que les autres on garde le numero de face
                                    first = .FALSE.
                                    dist = lambda_dep
                                    proj_face = id_face
                                END IF                                
                            END IF  
                        END IF
                    END IF
                END DO
                ! CHOIX 2 pour le cas "cavite"
                IF ( proj_face /= 0 .AND. DOM%MLG%FACLIM_GRP(isom,1) < 0 ) THEN
                    dir = DOM%MLG%VFAC( proj_face,:)
                    tag_move_vect( itag,: ) = tag_move_vect( itag,: ) - dist*dir(:)
                END IF
                !
                ! si on doit faire une projection, on cherche en premier la droite d'intersection des plans
                ! 1) de la face sur laquelle on projette 2) et du plan normal au vecteur deplacement passant
                ! par le point projeté avec correction. 
                ! On cherche ensuite la projection du point sur cette droite.
                ! 
                ! Choix 1 pour le cas "cavite"
                ! IF (proj_face /= 0) THEN
                ! Choix 2 pour le cas "cavite"
                IF ( proj_face /= 0 .AND. DOM%MLG%FACLIM_GRP(isom,1) > 0 ) THEN
                    orig_droite = 0.0_dp
                    dir = DOM%MLG%VFAC( proj_face, : )
                    dir = dir / norm( dir )
                    reg_som = tag_move_vect( itag, : )
                    dir_intersec = cross( dir, reg_som )
                    dir_intersec = dir_intersec / norm( dir_intersec )

                    second_terme(1) = dir(1)*DOM%MLG%SOMMET( id_som, 1 ) + &
                        & dir(2)*DOM%MLG%SOMMET( id_som, 2 ) + dir(3)*DOM%MLG%SOMMET( id_som, 3 )
                    second_terme(2) = reg_som(1)*( DOM%MLG%SOMMET(id_som, 1) + reg_som(1) ) + &
                        & reg_som(2)*( DOM%MLG%SOMMET(id_som, 2) + reg_som(2) ) + &
                        &reg_som(3)*( DOM%MLG%SOMMET(id_som, 3) + reg_som(3) )

                    ! On vérifie que la matrice est inversible avec le determinant non nul 
                    ! On cherche une composante du vecteur normée superieure à 0.5 pour eviter des erreurs numériques à l'inversion
                    IF ( ABS( dir_intersec(3) ) > 0.5 ) THEN
                        mat_inverse(1,1) = dir(1)
                        mat_inverse(2,1) = dir(2)
                        mat_inverse(1,2) = reg_som(1)
                        mat_inverse(2,2) = reg_som(2) 
                        mat_inverse = INV_MAT2( mat_inverse )
                        orig_droite(1:2) = MATMUL( second_terme, mat_inverse )
                    ELSEIF ( ABS( dir_intersec(2) ) > 0.5 ) THEN   
                        mat_inverse(1,1) = dir(1)
                        mat_inverse(2,1) = dir(3)
                        mat_inverse(1,2) = reg_som(1)
                        mat_inverse(2,2) = reg_som(3) 
                        mat_inverse = INV_MAT2( mat_inverse )
                        second_terme = MATMUL( second_terme, mat_inverse )
                        orig_droite(1) = second_terme(1) 
                        orig_droite(3) = second_terme(2)
                    ELSEIF ( ABS( dir_intersec(1) ) > 0.5 ) THEN
                        mat_inverse(1,1) = dir(2)
                        mat_inverse(2,1) = dir(3)
                        mat_inverse(1,2) = reg_som(2)
                        mat_inverse(2,2) = reg_som(3) 
                        mat_inverse = INV_MAT2( mat_inverse )
                        orig_droite(2:3) = MATMUL( second_terme, mat_inverse )
                    END IF
                    PS = ( ( ( DOM%MLG%SOMMET(id_som,1) + reg_som(1) - orig_droite(1) ) * dir_intersec(1) ) + &
                         & ( ( DOM%MLG%SOMMET(id_som,2) + reg_som(2) - orig_droite(2) ) * dir_intersec(2) ) + &
                         & ( ( DOM%MLG%SOMMET(id_som,3) + reg_som(3) - orig_droite(3) ) * dir_intersec(3) ) )

                    tag_move_vect( itag, : ) = orig_droite + PS*dir_intersec - DOM%MLG%SOMMET( id_som, : )

                    ! Le sommet peut toujours etre en dehors du domaine si la projection sur la face n'appartient
                    ! pas a la cellule
                    ! On exprime le vecteur deplacement dans le repere de la cellule (avec les 2 aretes voisines)
                    ! OM = lambda*OS1 + mu*OS2 + gamma*(OS1^OS2)
                    ! On regarde si lambda ou mu est negatif => changement necessaire
                    ! On peut verifier que gamma est tres petit 
                    ! On enleve ici le cas 2D
                    IF ( DOM%MLG%FAC( proj_face, 2 ) > 2 .AND. DOM%MLG%FACLIM_GRP( isom, 1 ) > 0 ) THEN 
                        pos = 0
                        DO ivois = 3, DOM%MLG%FAC(proj_face,2) + 2
                            IF ( DOM%MLG%FAC( proj_face, ivois ) == id_som ) THEN
                                pos = ivois
                                EXIT
                            END IF
                        END DO  
                        ! IDEE : avoir les sommets voisins de la face
                        IF ( pos == 3 ) THEN
                            id_som1 = DOM%MLG%FAC( proj_face, 4 )
                            id_som2 = DOM%MLG%FAC( proj_face, DOM%MLG%FAC(proj_face,2) + 2 )
                        ELSEIF (pos == DOM%MLG%FAC( proj_face,2) + 2) THEN
                            id_som1 = DOM%MLG%FAC( proj_face, 3 )
                            id_som2 = DOM%MLG%FAC( proj_face, DOM%MLG%FAC(proj_face,2) + 1 )
                        ELSE 
                            id_som1 = DOM%MLG%FAC( proj_face, pos-1 )
                            id_som2 = DOM%MLG%FAC( proj_face, pos+1 )
                        ENDIF
                        sommet1 = DOM%MLG%SOMMET( id_som1, : ) - DOM%MLG%SOMMET( id_som, : )
                        sommet2 = DOM%MLG%SOMMET( id_som2, : ) - DOM%MLG%SOMMET( id_som, : )
                        cross_vect = cross( sommet1, sommet2 )
                        mat_verif(:,1) = sommet1
                        mat_verif(:,2) = sommet2
                        mat_verif(:,3) = cross_vect

                        second_terme3 = tag_move_vect(itag,:)
                        mat_verif = INV_MAT3( mat_verif )
                        second_terme3 = MATMUL( mat_verif, second_terme3 )
                        IF ( second_terme3(1) < eps_min .AND. second_terme3(2) < eps_min ) THEN
                            CALL print_err("Erreur lors du deplacement du sommet suite a l'ablation",1)
                        ELSEIF ( second_terme3(1) < eps_min ) THEN
                            ! Application de la loi des sinus pour trouver de combien on se deplace selon dir_intersec
                            cross_vect2 = CROSS( sommet2, dir_intersec )
                            lambda_dep = ABS( second_terme3(1) )*NORM( cross_vect )/NORM( cross_vect2 )
                            IF ( DOT_PRODUCT(sommet1, dir_intersec) < eps_min ) lambda_dep = -lambda_dep
                            tag_move_vect(itag,:) = tag_move_vect(itag,:) + lambda_dep*dir_intersec(:)
                        ELSEIF ( second_terme3(2) < eps_min ) THEN
                            cross_vect2 = CROSS( sommet1, dir_intersec )
                            lambda_dep = ABS( second_terme3(2) )*NORM( cross_vect )/NORM( cross_vect2 )
                            IF ( DOT_PRODUCT(sommet2,dir_intersec) < eps_min ) lambda_dep = -lambda_dep
                            tag_move_vect(itag,:) = tag_move_vect(itag,:) + lambda_dep*dir_intersec(:)
                        ENDIF
                    ENDIF
                END IF
            END IF
        END DO
    END IF

    ! On somme la contribution de chaque tag pour avoir le déplacement total
    DO itag = 1, nb_tag
        IF ( tag_move(itag) > 0 ) THEN
            DOM%ETAT%D_SOM( id_som, : ) = DOM%ETAT%D_SOM( id_som, : ) + tag_move_vect( itag, : )
        END IF
    END DO

    ! On vérifie que le vecteur est dans le domaine
    ! On regarde s'il est sur une des faces, dans ce cas on doit projeter le deplacement sur la surface
    reg_som = DOM%ETAT%D_SOM( id_som, : )
    max_dep_som = MAX( max_dep_som , NORM(reg_som) )
    IF ( NORM(reg_som) > 10*eps_min ) THEN
        reg_som = reg_som/NORM( reg_som )
        DO ifac = 1, DOM%MLG%SOMLIM2FACLIM( isom, 2 )
            id_face = DOM%MLG%SOMLIM2FACLIM( isom, 2+ifac )
            dir = DOM%MLG%VFAC( id_face, : )
            PS = DOT_PRODUCT( dir, reg_som )
            ! Cette partie est en commentaire car normalement, aucun sommet ne doit se retrouver en dehors du domaine apres cette routine
            ! De toute facon, si le sommet est dehors, il sera projeté sur les patchs => no problem
            ! IF (PS > 1E-5) THEN ! Cela correspond à un angle entre entre les 2 vecteurs inférieurs à 89.99 deg
            !     ! write(6,*) "PS",PS
            !     ! write(6,*) "sommet"
            !     ! write(6,*) DOM%MLG%SOMMET(id_som,1),DOM%MLG%SOMMET(id_som,2),DOM%MLG%SOMMET(id_som,3)
            !     ! write(6,*) "dir face pb"
            !     ! write(6,*) dir(1),dir(2),dir(3)
            !     ! write(6,*) "dir reg_som"
            !     ! write(6,*) reg_som(1),reg_som(2),reg_som(3)
            !     ! write(6,*) "info somm", isom, id_som
            !     ! CALL print_err('Déplacement à corriger : pas encore implémenté',1)
            ! ELSE IF (ABS(PS) < 1E-5) THEN
            !     ! Déplacement à projeter
            !     EXIT
            ! END IF
            IF ( PS > -1E-5 ) THEN ! 1E-5 <==> 89.99 deg
                ! Déplacement à projeter
                IF ( DOM%MLG%dim_simu == '3D' ) THEN
                    DOM%MLG%SOM_LIM(isom)%projection_surf = .TRUE.
                ELSE
                    DOM%MLG%SOM_LIM(isom)%projection_courb = .TRUE.
                END IF
                EXIT
            END IF            
        END DO
    END IF
END DO
!
! DOM%RSL%max_dep_som = max_dep_som
DOM%RSL%max_dep_som = MAX( max_dep_som , DOM%RSL%max_dep_som )
!
END SUBROUTINE solve_AB_DSOM
!----------------------------------------------------------------------!        
!
!----------------------------------------------------------------------!
!> Routine de projection d'un sommet sur la surface
SUBROUTINE solve_AB_proj_face ( DOM , isom )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: isom, id_som, proj_face, iarete, proj_arete, proj_arete2, ii
INTEGER(IP) :: idsom1, idsom2, idsom3, idsom4, iiter, niter
REAL(DP), DIMENSION(3) :: SOM1, SOM_D, reg_som, rbezT, rbezdT, rbezd2T, VFAC
REAL(DP), DIMENSION(3) :: coord_tri_P, lambda_v, Ru, cros, RC1, RC2, RC3, et_v, bt_v, move, move_m1
REAL(DP), DIMENSION(3) :: SOMa, SOMb, SOMc, SOMd, V1, V2, V3, second_terme3, cross_vect, R13, R24, R0
REAL(DP), DIMENSION(3,3) :: mat_verif
REAL(DP) :: alphait, dit, zero, eps1, eps2, eps3, den_lambda, tol_def, ra1, ra2, ra3, ra4
REAL(DP), DIMENSION(7,3) :: PTS_CTR, PTS_CTR0
REAL(DP), DIMENSION(8,3) :: rbez_u123, rbezd_u123, rbezd2_u123, Ti_ui ! r1(u3),r1(1-u2),r2(u1),r2(1-u3),r3(u2),r3(1-u1)
REAL(DP), DIMENSION(4) :: hermite_u, hermite_v
REAL(DP), DIMENSION(2,4) :: hermite_temp
LOGICAL :: PS1, PS2, PS3
!
niter = DOM%NUM%niter_def
tol_def = DOM%NUM%tol_def
!
! 1- On cherche sur quelle face projeter le sommmet déplacé
! Sauf si le rayon de courbure du sommet nous dit que l'on est sur un plan. Dans ce cas
! on ne projette pas sur la surface vu que la surface et la face sont confondus
id_som = DOM%MLG%SOMLIM2FACLIM( isom, 1 )
reg_som = DOM%ETAT%D_SOM( id_som, : )
SOM1 = DOM%MLG%SOMMET( id_som, : )
SOM_D = SOM1 + reg_som

IF ( DOM%MLG%SOM_LIM(isom)%r_courb > 1E20 ) THEN

    move_m1 = 0.0_dp
    iiter = 0
    Ru = SOM_D

ELSE

    CALL find_face_lim_som ( DOM , isom , reg_som, proj_face , coord_tri_P , proj_arete, proj_arete2 )

    ! 2- On projette !
    IF ( DOM%MLG%FAC_LIM(proj_face)%nb_som == 3 ) THEN ! Cas face limite = triangle
        ! On met à jour le patch
        CALL update_bezier_face_tri( DOM , proj_face)

        ! Vecteur des lambda_i
        CALL ablation_lambdai( coord_tri_P , lambda_v )

        idsom1 = DOM%MLG%SOMLIM2FACLIM( DOM%MLG%FAC_LIM(proj_face)%SOMMETS(1) , 1 )
        idsom2 = DOM%MLG%SOMLIM2FACLIM( DOM%MLG%FAC_LIM(proj_face)%SOMMETS(2) , 1 )
        idsom3 = DOM%MLG%SOMLIM2FACLIM( DOM%MLG%FAC_LIM(proj_face)%SOMMETS(3) , 1 )
        SOMa = DOM%MLG%SOMMET( idsom3, : ) - DOM%MLG%SOMMET( idsom2, : )
        SOMb = DOM%MLG%SOMMET( idsom1, : ) - DOM%MLG%SOMMET( idsom3, : )
        SOMc = DOM%MLG%SOMMET( idsom2, : ) - DOM%MLG%SOMMET( idsom1, : )
        V1 = -CROSS( SOMa, SOMc )
        V1 = CROSS( V1, SOMa )
        V1 = V1 / NORM( V1 )
        V2 = -CROSS( SOMb, SOMa )
        V2 = CROSS( V2, SOMb )
        V2 = V2 / NORM( V2 )
        V3 = -CROSS( SOMc, SOMb )
        V3 = CROSS( V3, SOMc )
        V3 = V3 / NORM( V3 )
        eps1 = 0.01
        eps2 = 0.01
        eps3 = 0.01
        move_m1 = 0.0_dp
        PS1 = .TRUE.
        PS2 = .TRUE.
        PS3 = .TRUE.
        VFAC = DOM%MLG%VFAC( DOM%MLG%FACLIM(proj_face,2) , : )

        ! Le vecteur lambda_v ne donne pas le point de la surface que l'on souhaite (le plus proche du sommet déplacé)
        ! On fait donc une "dichotomie" sur lambda_v pour se rapprocher du "bon" point de la surface
        ! Les PS1,PS2,PS3 servent a ne diminuer le epsilon de modification de lambda_v que si on change de "direction"
        DO iiter = 1, niter
            ! Récupération des valeurs des courbes de Bezier
            DO iarete = 1, 3
                PTS_CTR = DOM%MLG%ARET_LIM( DOM%MLG%FAC_LIM(proj_face)%ARETES(iarete) )%PTS_CTR
                IF ( DOM%MLG%FAC_LIM(proj_face)%SENS_ARET(iarete) == 0 ) THEN
                    ! Si les sommets ne sont pas dans le meme ordre entre la face et l'arete, on inverse l'ordre des points de controles
                    DO ii = 1, 7
                        PTS_CTR0(ii,:) = PTS_CTR(8-ii,:)
                    END DO
                    PTS_CTR = PTS_CTR0
                END IF
                ! r_i(u_i-1)
                CALL rational_bezier ( PTS_CTR , lambda_v( MODULO(iarete+1, 3) + 1 ) , rbezT , rbezdT , rbezd2T )
                rbez_u123( 2*iarete-1, : ) = rbezT
                rbezd_u123( 2*iarete-1, : ) = rbezdT
                rbezd2_u123( 2*iarete-1, : ) = rbezd2T
                ! r_i(1-u_i+1)
                CALL rational_bezier ( PTS_CTR , 1-lambda_v( MODULO(iarete, 3) + 1) , rbezT , rbezdT , rbezd2T )
                rbez_u123( 2*iarete, : ) = rbezT    
                rbezd_u123( 2*iarete, : ) = rbezdT
                rbezd2_u123( 2*iarete, : ) = rbezd2T
            END DO

            ! Calcul des Ti
            zero = 0.0
            DO iarete = 1, 3
                rbezT = DOM%MLG%PATCH_LIM%RP( 2*(MODULO(iarete+1, 3) + 1) , : )
                rbezdT = DOM%MLG%PATCH_LIM%RP( 2*MODULO(iarete,3)+1 , : )
                ! Ti(u_i-1)
                CALL abl_alphai ( Pi - DOM%MLG%PATCH_LIM%ANGLES( MODULO(iarete,3)+1 ) , zero , zero , DOM%MLG%PATCH_LIM%ANGLES( MODULO(iarete+1,3)+1 ) , lambda_v( MODULO(iarete+1,3)+1 ) , alphait )
                CALL abl_di ( NORM(rbezT) , zero , zero , NORM(rbezdT) , lambda_v( MODULO(iarete+1,3)+1 ), dit )
                et_v = rbezd_u123( 2*iarete-1, : )
                bt_v = rbezd2_u123( 2*iarete-1, : )
                bt_v = CROSS( et_v, bt_v )
                et_v = et_v / NORM( et_v )
                IF ( NORM(bt_v) > eps_min ) THEN
                    bt_v = bt_v / NORM( bt_v )
                ELSE 
                    bt_v = (/ 0.0, 0.0, 0.0 /)
                END IF
                cros = CROSS( et_v, bt_v )
                IF ( DOT_PRODUCT(cros, VFAC) < eps_min ) bt_v = -bt_v
                Ti_ui( 2*iarete-1, : ) = dit*( COS(alphait)*et_v + SIN(alphait)*bt_v )
                ! Ti(1-u_i+1)
                CALL abl_alphai ( Pi - DOM%MLG%PATCH_LIM%ANGLES( MODULO(iarete,3)+1 ) , zero , zero , DOM%MLG%PATCH_LIM%ANGLES( MODULO(iarete+1,3)+1 ) , 1 - lambda_v( MODULO(iarete,3)+1 ) , alphait )
                CALL abl_di ( NORM(rbezT) , zero , zero , NORM(rbezdT) , 1 - lambda_v( MODULO(iarete,3)+1 ) , dit )
                et_v = rbezd_u123( 2*iarete, : )
                bt_v = rbezd2_u123( 2*iarete, : )
                bt_v = CROSS( et_v, bt_v )
                et_v = et_v / NORM( et_v )
                IF ( NORM(bt_v) > eps_min ) THEN
                    bt_v = bt_v / NORM( bt_v )
                ELSE 
                    bt_v = (/ 0.0, 0.0, 0.0 /)
                END IF
                cros = CROSS( et_v, bt_v )
                IF ( DOT_PRODUCT( cros, VFAC ) < eps_min ) bt_v = -bt_v
                Ti_ui( 2*iarete, : ) = dit*( COS(alphait)*et_v + SIN(alphait)*bt_v )
            END DO

            ! Calcul de R(u) = SUM Ri(u)
            ! Signe + car T_i-1(0) = -r'_i-1(1)
            RC1 = DOM%MLG%PATCH_LIM%TWIST( 1, : )
            RC2 = DOM%MLG%PATCH_LIM%TWIST( 2, : )
            RC3 = DOM%MLG%PATCH_LIM%TWIST( 3, : )
            IF ( MIN( NORM(RC1), NORM(RC2), NORM(RC3) ) > eps_min ) THEN ! Twist ou rayon de courbure pour la condition C0/C1
                Ru = lambda_v(1)*( rbez_u123(5,:) + coord_tri_P(3)*( Ti_ui(5,:) + DOM%MLG%PATCH_LIM%RP(4,:) ) + rbez_u123(4,:) + &
                    & coord_tri_P(2)*( Ti_ui(4,:) - DOM%MLG%PATCH_LIM%RP(5,:) ) - DOM%MLG%SOMMET(idsom1,:) - coord_tri_P(2)*coord_tri_P(3)*DOM%MLG%PATCH_LIM%TWIST(1,:) ) + &
                        & lambda_v(2)*( rbez_u123(1,:) + coord_tri_P(1)*( Ti_ui(1,:) + DOM%MLG%PATCH_LIM%RP(6,:) ) + rbez_u123(6,:) + &
                    & coord_tri_P(3)*( Ti_ui(6,:) - DOM%MLG%PATCH_LIM%RP(1,:) ) - DOM%MLG%SOMMET(idsom2,:) - coord_tri_P(1)*coord_tri_P(3)*DOM%MLG%PATCH_LIM%TWIST(2,:) )  + &
                        & lambda_v(3)*( rbez_u123(3,:) + coord_tri_P(2)*( Ti_ui(3,:) + DOM%MLG%PATCH_LIM%RP(2,:) ) + rbez_u123(2,:) + &
                    & coord_tri_P(1)*( Ti_ui(2,:) - DOM%MLG%PATCH_LIM%RP(3,:) ) - DOM%MLG%SOMMET(idsom3,:) - coord_tri_P(1)*coord_tri_P(2)*DOM%MLG%PATCH_LIM%TWIST(3,:) )  
            ELSE
                Ru = lambda_v(1)*( rbez_u123(5,:) + rbez_u123(4,:) - DOM%MLG%SOMMET(idsom1,:) ) + &
                & lambda_v(2)*( rbez_u123(1,:) + rbez_u123(6,:) - DOM%MLG%SOMMET(idsom2,:) ) + &
                & lambda_v(3)*( rbez_u123(3,:) + rbez_u123(2,:) - DOM%MLG%SOMMET(idsom3,:) ) 
            END IF

            move = Ru - SOM_D
            move_m1 = move_m1 - move

            IF ( NORM(move_m1) < tol_def ) EXIT

            IF ( DOT_PRODUCT( move,V1 ) > 0.0_dp ) THEN
                IF ( .NOT. PS1 ) eps1 = eps1/2
                lambda_v(1) = lambda_v(1) - eps1
                IF ( lambda_v(1) < 100*eps_min ) lambda_v(1) = 0.0_dp
                PS1 = .TRUE.
            ELSE
                IF ( PS1 ) eps1 = eps1/2
                lambda_v(1) = lambda_v(1) + eps1
                IF ( lambda_v(1) < 100*eps_min ) lambda_v(1) = 0.0_dp
                PS1 = .FALSE.
            END IF
            IF ( DOT_PRODUCT( move,V2 ) > 0.0_dp ) THEN
                IF ( .NOT. PS2 ) eps2 = eps2/2
                lambda_v(2) = lambda_v(2) - eps2
                IF ( lambda_v(2) < 100*eps_min ) lambda_v(2) = 0.0_dp
                PS2 = .TRUE.
            ELSE
                IF ( PS2 ) eps2 = eps2/2
                lambda_v(2) = lambda_v(2) + eps2
                IF ( lambda_v(2) < 100*eps_min ) lambda_v(2) = 0.0_dp
                PS2 = .FALSE.
            END IF
            IF ( DOT_PRODUCT( move,V3 ) > 0.0_dp ) THEN
                IF ( .NOT. PS3 ) eps3 = eps3/2
                lambda_v(3) = lambda_v(3) - eps3
                IF ( lambda_v(3) < 100*eps_min ) lambda_v(3) = 0.0_dp
                PS3 = .TRUE.
            ELSE
                IF ( PS3 ) eps3 = eps3/2
                lambda_v(3) = lambda_v(3) + eps3
                IF ( lambda_v(3) < 100*eps_min ) lambda_v(3) = 0.0_dp
                PS3 = .FALSE.
            END IF

            den_lambda = lambda_v(1) + lambda_v(2) + lambda_v(3)
            lambda_v = lambda_v / den_lambda
            move_m1 = move
        END DO

    ELSE IF ( DOM%MLG%FAC_LIM(proj_face)%nb_som == 4 ) THEN ! Cas face limite = rectangle
        ! On met à jour le patch
        CALL update_bezier_face_quad( DOM , proj_face )

        idsom1 = DOM%MLG%SOMLIM2FACLIM( DOM%MLG%FAC_LIM(proj_face)%SOMMETS(1) , 1 )
        idsom2 = DOM%MLG%SOMLIM2FACLIM( DOM%MLG%FAC_LIM(proj_face)%SOMMETS(2) , 1 )
        idsom3 = DOM%MLG%SOMLIM2FACLIM( DOM%MLG%FAC_LIM(proj_face)%SOMMETS(3) , 1 )
        idsom4 = DOM%MLG%SOMLIM2FACLIM( DOM%MLG%FAC_LIM(proj_face)%SOMMETS(4) , 1 )
        SOMa = DOM%MLG%SOMMET( idsom2,: ) - DOM%MLG%SOMMET( idsom1,: )
        SOMb = DOM%MLG%SOMMET( idsom4,: ) - DOM%MLG%SOMMET( idsom1,: )
        SOMc = DOM%MLG%SOMMET( idsom3,: ) - DOM%MLG%SOMMET( idsom4,: )
        SOMd = DOM%MLG%SOMMET( idsom3,: ) - DOM%MLG%SOMMET( idsom2,: )
        V1 = ( SOMa + SOMc ) / 2.0
        V1 = V1 / NORM(V1)
        V2 = ( SOMb + SOMd ) / 2.0
        V2 = V2 / NORM(V2)
        eps1 = 0.01
        eps2 = 0.01
        move_m1 = 0.0_dp
        PS1 = .TRUE.
        PS2 = .TRUE.
        VFAC = DOM%MLG%VFAC( DOM%MLG%FACLIM(proj_face,2) , : )

        ! Calcul de lambda_v = coord dans le cas du rectangle
        cross_vect = cross( SOMa , SOMb )
        mat_verif(:,1) = SOMa
        mat_verif(:,2) = SOMb
        mat_verif(:,3) = cross_vect / NORM( cross_vect )
        second_terme3 = ( SOM1 - DOM%MLG%SOMMET(idsom1,:) ) + reg_som 
        mat_verif = INV_MAT3( mat_verif )
        second_terme3 = MATMUL( mat_verif, second_terme3 )
        IF ( second_terme3(1) < eps_min ) THEN
            lambda_v(1) = 0.0
        ELSE IF ( second_terme3(1) > 1.0 ) THEN
            lambda_v(1) = 1.0
        ELSE
            lambda_v(1) = second_terme3(1)
        END IF
        IF ( second_terme3(2) < eps_min ) THEN
            lambda_v(2) = 0.0
        ELSE IF ( second_terme3(2) > 1.0 ) THEN
            lambda_v(2) = 1.0
        ELSE
            lambda_v(2) = second_terme3(2)
        END IF    
        lambda_v(3) = 0.0
        
        ! Le vecteur lambda_v ne donne pas le point de la surface que l'on souhaite (le plus proche du sommet déplacé)
        ! On fait donc une "dichotomie" sur lambda_v pour se rapprocher du "bon" point de la surface
        ! Les PS1,PS2 servent a ne diminuer le epsilon de modification de lambda_v que si on change de "direction"
        DO iiter = 1, niter
            ! Récupération des valeurs des courbes de Bezier
            DO iarete = 1, 4
                PTS_CTR = DOM%MLG%ARET_LIM( DOM%MLG%FAC_LIM(proj_face)%ARETES(iarete) )%PTS_CTR
                IF ( DOM%MLG%FAC_LIM(proj_face)%SENS_ARET(iarete) == 0 ) THEN
                    ! Si les sommets ne sont pas dans le meme ordre entre la face et l'arete, on inverse l'ordre des points de controles
                    DO ii = 1, 7
                        PTS_CTR0(ii,:) = PTS_CTR(8-ii,:)
                    END DO
                    PTS_CTR = PTS_CTR0
                END IF
                ! r_i(u)
                CALL rational_bezier ( PTS_CTR, lambda_v(1), rbezT, rbezdT, rbezd2T )
                rbez_u123(2*iarete-1,:) = rbezT
                rbezd_u123(2*iarete-1,:) = rbezdT
                rbezd2_u123(2*iarete-1,:) = rbezd2T
                ! r_i(v)
                CALL rational_bezier ( PTS_CTR, lambda_v(2), rbezT, rbezdT, rbezd2T )
                rbez_u123(2*iarete,:) = rbezT    
                rbezd_u123(2*iarete,:) = rbezdT
                rbezd2_u123(2*iarete,:) = rbezd2T
            END DO

            ! Calcul des di*Ti
            zero = 0.0
            DO iarete = 1, 2
                ! di*Ti(u) (i = 1 et 3)
                rbezT = DOM%MLG%PATCH_LIM%RP( 4*(iarete-1)+1 , : )
                rbezdT = DOM%MLG%PATCH_LIM%RP( 4*(iarete-1)+2 , : )         
                CALL abl_alphai ( DOM%MLG%PATCH_LIM%ANGLES( 3*(iarete-1)+1 ) , zero , zero , DOM%MLG%PATCH_LIM%ANGLES( iarete+1 ) , lambda_v(1) , alphait )
                CALL abl_di ( NORM(rbezT) , zero , zero , NORM(rbezdT) , lambda_v(1) , dit )
                et_v = rbezd_u123( 4*(iarete-1)+1 , : )
                bt_v = rbezd2_u123( 4*(iarete-1)+1 , : )
                bt_v = CROSS( et_v , bt_v )
                et_v = et_v / NORM( et_v )
                IF ( NORM(bt_v) > eps_min ) THEN
                    bt_v = bt_v / NORM( bt_v )
                ELSE 
                    bt_v = (/ 0.0, 0.0, 0.0 /)
                END IF
                cros = CROSS( et_v , bt_v )
                IF ( DOT_PRODUCT( cros,VFAC ) < eps_min ) bt_v = -bt_v
                Ti_ui(2*iarete-1,:) = dit*( COS(alphait)*et_v + SIN(alphait)*bt_v )

                ! di*Ti(v) (i = 2 et 4)
                rbezT = DOM%MLG%PATCH_LIM%RP( 4*(iarete-1)+3 , : )
                rbezdT = DOM%MLG%PATCH_LIM%RP( 4*(iarete-1)+4 , : ) 
                CALL abl_alphai ( -DOM%MLG%PATCH_LIM%ANGLES(iarete) , zero , zero , -DOM%MLG%PATCH_LIM%ANGLES(5-iarete) , lambda_v(2) , alphait )
                CALL abl_di ( NORM(rbezT) , zero , zero , NORM(rbezdT) , lambda_v(2) , dit )
                et_v = rbezd_u123( 4*(iarete-1)+4 , : )
                bt_v = rbezd2_u123( 4*(iarete-1)+4 , : )
                bt_v = CROSS( et_v , bt_v )
                et_v = et_v / NORM( et_v )
                IF ( NORM(bt_v) > eps_min ) THEN
                    bt_v = bt_v / NORM( bt_v )
                ELSE 
                    bt_v = (/ 0.0, 0.0, 0.0 /)
                END IF
                cros = CROSS( et_v , bt_v )
                IF ( DOT_PRODUCT( cros,VFAC ) < eps_min ) bt_v = -bt_v
                Ti_ui(2*iarete,:) = dit*( COS(alphait)*et_v + SIN(alphait)*bt_v )
            END DO  

            ! Calcul des polynomes d'Hermite
            hermite_temp = hermite( lambda_v(1) )
            hermite_u = hermite_temp(1,:)
            hermite_temp = hermite( lambda_v(2) )
            hermite_v = hermite_temp(1,:)

            ! Calcul de R(u) = R13(u,v) + R24(u,v) - R0(u,v)
            ra1 = 1.0 / DOM%MLG%SOM_LIM( DOM%MLG%FAC_LIM(proj_face)%SOMMETS(1) )%r_courb
            ra2 = 1.0 / DOM%MLG%SOM_LIM( DOM%MLG%FAC_LIM(proj_face)%SOMMETS(2) )%r_courb
            ra3 = 1.0 / DOM%MLG%SOM_LIM( DOM%MLG%FAC_LIM(proj_face)%SOMMETS(3) )%r_courb
            ra4 = 1.0 / DOM%MLG%SOM_LIM( DOM%MLG%FAC_LIM(proj_face)%SOMMETS(4) )%r_courb

            ! IF ( MIN( ra1,ra2,ra3,ra4 ) > 10*eps_min ) THEN ! Twist ou rayon de courbure pour la condition C0/C1    
            IF ( MIN( ra1,ra2,ra3,ra4 ) < 1E-8 ) THEN ! Twist ou rayon de courbure pour la condition C0/C1 
                R13 = hermite_v(1)*rbez_u123(1,:) + hermite_v(2)*Ti_ui(1,:) + hermite_v(3)*Ti_ui(3,:) + hermite_v(4)*rbez_u123(5,:)
                R24 = hermite_u(1)*rbez_u123(4,:) + hermite_u(2)*Ti_ui(2,:) + hermite_u(3)*Ti_ui(4,:) + hermite_u(4)*rbez_u123(8,:)
                R0 = DOM%MLG%SOMMET(idsom1,:)*hermite_u(1)*hermite_v(1) + &
                    & DOM%MLG%PATCH_LIM%RP(3,:)*hermite_u(1)*hermite_v(2) + &
                    & DOM%MLG%PATCH_LIM%RP(4,:)*hermite_u(1)*hermite_v(3) + &
                    & DOM%MLG%SOMMET(idsom4,:)*hermite_u(1)*hermite_v(4) + &
                    !
                    & DOM%MLG%PATCH_LIM%RP(1,:)*hermite_u(2)*hermite_v(1) + &
                    & DOM%MLG%PATCH_LIM%TWIST(1,:)*hermite_u(2)*hermite_v(2) + &
                    & DOM%MLG%PATCH_LIM%TWIST(4,:)*hermite_u(2)*hermite_v(3) + &
                    & DOM%MLG%PATCH_LIM%RP(5,:)*hermite_u(2)*hermite_v(4) + &
                    !
                    & DOM%MLG%PATCH_LIM%RP(2,:)*hermite_u(3)*hermite_v(1) + &
                    & DOM%MLG%PATCH_LIM%TWIST(2,:)*hermite_u(3)*hermite_v(2) + &
                    & DOM%MLG%PATCH_LIM%TWIST(3,:)*hermite_u(3)*hermite_v(3) + &
                    & DOM%MLG%PATCH_LIM%RP(6,:)*hermite_u(3)*hermite_v(4) + &
                    !
                    & DOM%MLG%SOMMET(idsom2,:)*hermite_u(4)*hermite_v(1) + &
                    & DOM%MLG%PATCH_LIM%RP(7,:)*hermite_u(4)*hermite_v(2) + &
                    & DOM%MLG%PATCH_LIM%RP(8,:)*hermite_u(4)*hermite_v(3) + &
                    & DOM%MLG%SOMMET(idsom3,:)*hermite_u(4)*hermite_v(4)
            ELSE 
                R13 = hermite_v(1)*rbez_u123(1,:) + hermite_v(4)*rbez_u123(5,:)
                R24 = hermite_u(1)*rbez_u123(4,:) + hermite_u(4)*rbez_u123(8,:)
                R0 = DOM%MLG%SOMMET(idsom1,:)*hermite_u(1)*hermite_v(1) + &
                    & DOM%MLG%SOMMET(idsom4,:)*hermite_u(1)*hermite_v(4) + &
                    & DOM%MLG%SOMMET(idsom2,:)*hermite_u(4)*hermite_v(1) + &
                    & DOM%MLG%SOMMET(idsom3,:)*hermite_u(4)*hermite_v(4)
            END IF

            Ru = R13 + R24 - R0

            move = Ru - SOM_D
            move_m1 = move_m1 - move        

            IF ( NORM( move_m1 ) < tol_def ) EXIT

            IF ( DOT_PRODUCT( move,V1 ) > eps_min ) THEN
                IF ( .NOT. PS1 ) eps1 = eps1/2
                lambda_v(1) = lambda_v(1) - eps1
                IF ( lambda_v(1) < 100*eps_min ) lambda_v(1) = 0.0_dp
                PS1 = .TRUE.
            ELSE
                IF ( PS1 ) eps1 = eps1/2
                lambda_v(1) = lambda_v(1) + eps1
                IF ( lambda_v(1) > 1.0 ) lambda_v(1) = 1.0
                PS1 = .FALSE.
            END IF
            IF ( DOT_PRODUCT( move,V2 ) > eps_min ) THEN
                IF ( .NOT. PS2 ) eps2 = eps2/2
                lambda_v(2) = lambda_v(2) - eps2
                IF ( lambda_v(2) < 100*eps_min ) lambda_v(2) = 0.0_dp
                PS2 = .TRUE.
            ELSE
                IF ( PS2 ) eps2 = eps2/2
                lambda_v(2) = lambda_v(2) + eps2
                IF ( lambda_v(2) > 1.0 ) lambda_v(2) = 1.0
                PS2 = .FALSE.
            END IF
           
            move_m1 = move  

        END DO
    END IF

END IF
!
DOM%RSL%niter_proj_AB(2) = MAX( DOM%RSL%niter_proj_AB(2) , iiter )
DOM%RSL%err_proj_AB(2) = MAX( DOM%RSL%err_proj_AB(2) , NORM(move_m1) )
DOM%ETAT%D_SOM(id_som,:) = Ru - SOM1
!
END SUBROUTINE solve_AB_proj_face
!----------------------------------------------------------------------!      
!
!----------------------------------------------------------------------!
!> Routine de projection d'un sommet sur la courbe
SUBROUTINE solve_AB_proj_curve ( DOM , isom )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: isom, ivois, id_som, iarete, idarete, iiter, niter, tag_lim_dimension
REAL(DP), DIMENSION(3) :: SOM0, SOM1, SOM2, SOM12, SOM_D, reg_som, rbezT
REAL(DP), DIMENSION(3) :: move, move_m1
REAL(DP) :: dist, eps, lambda, tol_def, PS
REAL(DP), DIMENSION(7,3) :: PTS_CTR
LOGICAL :: first, UP_DOWN
!
niter = DOM%NUM%niter_def
tol_def = DOM%NUM%tol_def
!
dist = 0.0_dp
first = .TRUE.
idarete = 0
IF ( DOM%MLG%dim_simu == '3D' ) THEN
    tag_lim_dimension = 2
ELSE
    tag_lim_dimension = 1
END IF
!
! 1- On cherche sur quelle courbe projeter le sommmet déplacé
id_som = DOM%MLG%SOMLIM2FACLIM( isom, 1 )
reg_som = DOM%ETAT%D_SOM( id_som, : )
SOM1 = DOM%MLG%SOMMET( id_som, : )
SOM0 = SOM1
SOM_D = SOM1 + reg_som
DO ivois = 1, DOM%MLG%SOM_LIM(isom)%nvoisin
    iarete = DOM%MLG%SOM_LIM(isom)%ARETES(ivois)
    IF ( DOM%MLG%ARET_LIM(iarete)%SOMMETS(1) == isom ) THEN
        IF ( DOM%MLG%SOM_LIM( DOM%MLG%ARET_LIM(iarete)%SOMMETS(2) )%nb_tags == tag_lim_dimension-1 ) CYCLE
        SOM2 = DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iarete)%SOMMETS(2), 1 ) , : )
    ELSE
        IF ( DOM%MLG%SOM_LIM( DOM%MLG%ARET_LIM(iarete)%SOMMETS(1) )%nb_tags == tag_lim_dimension-1 ) CYCLE
        SOM2 = DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iarete)%SOMMETS(1), 1 ) , : )
    END IF
    SOM12 = SOM2 - SOM1
    SOM12 = SOM12 / NORM( SOM12 )
    PS = DOT_PRODUCT( reg_som , SOM12 )
    IF ( PS > dist .OR. first ) THEN
        first = .FALSE.
        idarete = iarete
        dist = PS
    END IF
END DO
!
PTS_CTR = DOM%MLG%ARET_LIM(idarete)%PTS_CTR
!
SOM1 = DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(idarete)%SOMMETS(1), 1 ), : )
SOM2 = DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(idarete)%SOMMETS(2), 1 ), : )
SOM12 = SOM2 - SOM1
IF ( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(idarete)%SOMMETS(1), 1 ) == id_som ) THEN
    lambda = NORM( reg_som ) / NORM( SOM12 )
ELSE
    lambda = 1 - NORM( reg_som ) / NORM( SOM12 )
END IF
eps = 0.01
move_m1 = 0.0_dp
UP_DOWN = .TRUE.
!
DO iiter = 1, niter
    CALL rational_bezier_pol ( PTS_CTR , lambda , rbezT )
    
    move = rbezT - SOM_D
    move_m1 = move_m1 - move

    IF ( NORM( move_m1 ) < tol_def ) EXIT

    IF ( DOT_PRODUCT( move,SOM12 ) > eps_min ) THEN
        IF ( .NOT. UP_DOWN ) eps = eps/2
        lambda = lambda - eps
        IF ( lambda < eps_min ) lambda = 0.0_dp
        UP_DOWN = .TRUE.
    ELSE
        IF ( UP_DOWN ) eps = eps/2
        lambda = lambda + eps
        IF ( lambda > 1.0 ) lambda = 1.0
        UP_DOWN = .FALSE.
    END IF

    move_m1 = move

END DO
!
DOM%RSL%niter_proj_AB(1) = MAX( DOM%RSL%niter_proj_AB(1) , iiter )
DOM%RSL%err_proj_AB(1) = MAX( DOM%RSL%err_proj_AB(1) , NORM(move_m1) )
DOM%ETAT%D_SOM(id_som,:) = rbezT - SOM0
!
END SUBROUTINE solve_AB_proj_curve
!----------------------------------------------------------------------!      
!
!----------------------------------------------------------------------!
!> On enregistre les sommets que l'on doit équilibrer
!> On sépare les sommets limites des sommets intérieurs
SUBROUTINE sommet_to_move ( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: isom, nsom_lim, nsom_int, nmoved, NVOIS, ndonem1, ndonen, ivois
INTEGER(IP) :: ii, iisom, ndoneINT, ndoneLIM, idsom
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: SDONE_LIM, SDONE_INT
!
nsom_lim = DOM%MLG%nsom_lim
nsom_int = DOM%MLG%nsom_int
!
ALLOCATE( SDONE_LIM( nsom_lim ) )
ALLOCATE( SDONE_INT( nsom_int ) )
nmoved = 0
ndoneLIM = 0
ndoneINT = 0
SDONE_LIM = 0
SDONE_INT = 0
NVOIS = DOM%NUM%nvois_def
IF ( NVOIS < 1 ) CALL print_err('Equilibrage impossible sur 0 hauteur de maille !',1)
! Mettre une condition pour si pas de grosse modif ou pas de nécessité on ne refait pas cette routine
!
! On récupère les indices des sommets ablatés
DO isom = 1, nsom_lim
    IF ( DOM%MLG%SOM_LIM(isom)%moved_abl ) THEN
        nmoved = nmoved + 1
        SDONE_LIM(nmoved) = DOM%MLG%SOM_ADJ_LIM( isom, 1 )
    END IF
END DO
ndoneLIM = nmoved
!
! On note les NVOIS voisins des sommets ablatés
! On boucle NVOIS fois pour récupérer les 1er voisins, puis les 2nd, ...
! On fait les sommets limites ...
ndonem1 = 1
DO ii = 1, NVOIS
    ndonen = ndoneLIM
    DO isom = ndonem1, ndonen
        idsom = SDONE_LIM( isom )
        iisom = DOM%MLG%IDSOM2ISOM( idsom )
        DO ivois = 1, DOM%MLG%SOM_LIM(iisom)%nvoisin
            IF ( .NOT. ANY( SDONE_LIM(1:ndoneLIM) == DOM%MLG%SOM_ADJ_LIM( DOM%MLG%SOM_LIM(iisom)%SOMMETS_LIM(ivois) , 1 ) ) ) THEN
                ndoneLIM = ndoneLIM + 1
                SDONE_LIM(ndoneLIM) = DOM%MLG%SOM_ADJ_LIM( DOM%MLG%SOM_LIM(iisom)%SOMMETS_LIM(ivois) , 1 )
            END IF
        END DO
    END DO
    ndonem1 = ndonen + 1
END DO
! 
! ... puis les sommets internes (on ne ré-enregistre pas les sommets limites)
!
DO isom = 1, nmoved
    idsom = SDONE_LIM( isom )
    DO ivois = 1, DOM%MLG%SOM_ADJ( idsom, 2 )
        IF ( (.NOT. ANY( SDONE_INT(1:ndoneINT) == DOM%MLG%SOM_ADJ(idsom, 2+ivois) ) ) .AND. (.NOT. ANY( DOM%MLG%SOM_ADJ_LIM(:,1) == DOM%MLG%SOM_ADJ(idsom, 2+ivois) ) ) ) THEN
            ndoneINT = ndoneINT + 1
            SDONE_INT(ndoneINT) = DOM%MLG%SOM_ADJ( idsom, 2+ivois )
        END IF
    END DO
END DO
!
ndonem1 = 1
DO ii = 1, NVOIS-1
    ndonen = ndoneINT
    DO isom = ndonem1, ndonen
        idsom = SDONE_INT( isom )
        DO ivois = 1, DOM%MLG%SOM_ADJ( idsom, 2 )
            IF ( (.NOT. ANY( SDONE_INT(1:ndoneINT) == DOM%MLG%SOM_ADJ(idsom, 2+ivois) ) ) .AND. (.NOT. ANY( DOM%MLG%SOM_ADJ_LIM(:,1) == DOM%MLG%SOM_ADJ(idsom,2+ivois) ) ) ) THEN
                ndoneINT = ndoneINT + 1
                SDONE_INT(ndoneINT) = DOM%MLG%SOM_ADJ( idsom, 2+ivois )
            END IF
        END DO
    END DO
    ndonem1 = ndonen + 1
END DO
!
! Enregistrement des sommets limites
DO isom = 1,ndoneLIM
    DOM%MLG%SOM_LIM( DOM%MLG%IDSOM2ISOM( SDONE_LIM(isom) ) )%equilibrage = .TRUE.
END DO
!
! et des sommets internes
DO isom = 1,ndoneINT
    DOM%MLG%SOM_INT( DOM%MLG%IDSOM2ISOM( SDONE_INT(isom) ) )%equilibrage = .TRUE.
END DO
!
! On arrete de chercher plus de voisins si on a deja tous les sommets
IF ( ndoneLIM == nsom_lim .AND. ndoneINT == nsom_int ) THEN
    DOM%RSL%nvois_max = .TRUE.
END IF
!
DOM%MLG%SOM_LIM%moved_abl_N1 = DOM%MLG%SOM_LIM%moved_abl
DOM%RSL%update_somvois = .FALSE.
DEALLOCATE( SDONE_LIM )
DEALLOCATE( SDONE_INT )
!
END SUBROUTINE sommet_to_move
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_solve_AB3D
