!> Module pour resolution d'un cycle de couplage avec solveur externe
MODULE mod_solve_C
!
USE mod_cst, ONLY : DP, IP, eps_min
USE mod_print, ONLY : print_err, print_solve_C_ini, print_solve_C_end
USE mod_algebra, ONLY : normL2, realtostr
USE mod_dat_read_cls, ONLY : dat_read_cls
USE mod_interpolate, ONLY : interp_one_fac
!
USE MPI
USE CWIPI
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Initialisation des echanges CWIPI + création du maillage d'interface
SUBROUTINE solve_C_init( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
!
REAL(DP), ALLOCATABLE, DIMENSION(:) :: coordinates              ! Tableau de coordonnées des sommets (x1 y1 z1 x2 y2 ...)
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: connectivity          ! Tableau de connectivité (s1 s2 s3 | s2 s3 s4 | ...)
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: connectivity_index    ! Tableau index de début d'élément (0 id_con_fac1 id_con_fac2 ...) (n+1 éléments) - Type compressed row
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: SOM_LIMi              ! Tableau de construction id_som relatif > id_som absolu
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: SOM_ABS_LIMi          ! Tableau de construction id_som absolu > id_som relatif
!
INTEGER(IP) :: ilim, nlim, ncon
INTEGER(IP) :: isom, nsom, id_som, isom_limi, nsom_limi
INTEGER(IP) :: ndim, id_fac, ifac_lim, nfac_limi
!INTEGER :: irank, ierr, commWorldSize
INTEGER(IP) :: localcom, nNotLocatedPoints
CHARACTER (len=128):: codeName, CoupledcodeName, couplingID
LOGICAL :: find_som

!CALL MPI_COMM_RANK(MPI_COMM_WORLD, irank, ierr)
!CALL MPI_COMM_SIZE(MPI_COMM_WORLD, commWorldSize, ierr)


! Nom de l'application de couplage = nom du fichier de maillage utilisé
codeName = DOM%nom_dom

! Initialisation de l'environnement CWIPI
CALL CWIPI_INIT_F (MPI_COMM_WORLD, &
                    codeName, &
                    localcom)


! Boucle pour chaque limite du système
nlim = DOM%CDT%nlim
DO ilim = 1,nlim

    ! Si pour la limite courante, il existe une application de couplage active, alors :
    IF (DOM%CDT%LIM(ilim)%couplage_lim == 1) THEN
                
        ! Identifiant de couplage = nom de la limite du domaine modethec, identique pour le code à coupler
        couplingID = DOM%CDT%LIM(ilim)%nom_lim

        ! Nom de l'application distante à coupler = nom du fichier de maillage pour couplage modethec-modethec, sinon à modifier dans fichier .prm
        CoupledcodeName = DOM%CDT%LIM(ilim)%application_couplage
        
        ! Vérification application_couplage n'est pas vide
        IF (CoupledcodeName == '') THEN
            CALL print_err('Boundary '//TRIM(DOM%CDT%LIM(ilim)%nom_lim)//': application_couplage parameter is incomplete', 1)
        END IF

        ! Dimension du couplage de l'interface (sufracique) : 1D pour domaine 2D ou 2D pour domaine 3D
        IF (DOM%MLG%dim_simu == '3D') THEN
            ndim = 2
        ELSE
            ndim = 1
        ENDIF

        ! Initilisation dans boucle de limite
        nfac_limi = DOM%CDT%LIM(ilim)%nfac_limi
        nsom_limi = 0
        ncon = 0
        ALLOCATE( SOM_LIMi( nfac_limi*4 ) )
        SOM_LIMi(:) = 0
        ALLOCATE( SOM_ABS_LIMi( DOM%MLG%nsom ) )
        SOM_ABS_LIMi(:) = 0
        ALLOCATE( connectivity_index( nfac_limi+1 ) )
        connectivity_index(:) = 0
        
        !-----------------------------------------------------------------!
        !--- Partie creation du maillage d'interface CWIPI ---------------!

        ! Pour chaque face limite de limi...
        DO ifac_lim = 1,nfac_limi

            ! ... On recupère l'identifiant face et le nombre de sommets associés
            id_fac = DOM%CDT%LIM(ilim)%ID_FACLIMi( ifac_lim )
            nsom = DOM%MLG%FAC( id_fac, 2 )

            ! ... Pour chaque sommet de id_fac ...
            DO isom = 1,nsom
                
                ! On cherche si id_som est dans la liste des sommets déjà identifiés
                id_som = DOM%MLG%FAC( id_fac, 2+isom )
                find_som = ANY(SOM_LIMi(:) == id_som)

                ! Si rien trouvé, c'est un nouveau sommet...
                IF (.NOT.find_som) THEN

                    ! Et on l'ajoute à la liste SOM_LIMi
                    nsom_limi = nsom_limi + 1
                    SOM_LIMi(nsom_limi) = id_som
                    ! Et on construit le tableau inverse d'indice relatif dans SOM_ABS_LIMi[nsom]
                    SOM_ABS_LIMi(id_som) = nsom_limi

                END IF

            END DO ! Fin sommet de id_fac

                connectivity_index(ifac_lim) = ncon
                ncon = ncon + nsom

        END DO ! Fin face limite

        connectivity_index( nfac_limi+1 ) = ncon
       
       ! Allocation des tableaux de maillage CWIPI
        ALLOCATE( connectivity( ncon ) )
        connectivity(:) = 0
        ALLOCATE( coordinates( nsom_limi*3 ) )
        coordinates(:) = 0.0_dp

        ! Pour chaque face limite de limi...
        ncon = 0
        DO ifac_lim = 1,nfac_limi

            ! ... On recupère l'identifiant face et le nombre de sommets associés
            id_fac = DOM%CDT%LIM(ilim)%ID_FACLIMi( ifac_lim )
            nsom = DOM%MLG%FAC( id_fac, 2 )
            
            ! ... Pour chaque sommet de id_fac ...
            DO isom = 1,nsom

                ! Ajout d'une connexion
                ncon = ncon + 1
                ! Ecriture dans connectivity
                id_som = DOM%MLG%FAC( id_fac, 2+isom )
                connectivity( ncon ) = SOM_ABS_LIMi(id_som)

            END DO ! Fin sommet de id_fac

        END DO ! Fin face limite

        ! Pour chaque sommet limite de limi...
        DO isom_limi = 1,nsom_limi

            ! Ecriture dans coordinates des coordonnées en (x1 y1 z1 x2 y2 ...)
            id_som = SOM_LIMi(isom_limi)
            coordinates(1+(isom_limi-1)*3) = DOM%MLG%SOMMET(id_som,1)
            coordinates(2+(isom_limi-1)*3) = DOM%MLG%SOMMET(id_som,2)
            coordinates(3+(isom_limi-1)*3) = DOM%MLG%SOMMET(id_som,3)

        END DO ! Fin sommet limite de limi

        !--- Fin création du maillage d'interface CWIPI ----------------!
        !---------------------------------------------------------------!

        !--- Création du couplage entre codeName et CoupledcodeName ----!
        CALL CWIPI_CREATE_COUPLING_F( couplingID, &                     ! Identifiant de couplage
                                    CWIPI_CPL_PARALLEL_WITH_PART, &     ! Type de couplage
                                    CoupledcodeName, &                  ! Nom de l'application distante
                                    ndim,     &                         ! Dimension (1, 2 ou 3)
                                    0.1d0, &                            ! Tolerance geometrique
                                    CWIPI_STATIC_MESH, &                ! Type de maillage (evite de reconstruire fonctions d'interpolation)
                                    CWIPI_SOLVER_CELL_CENTER, &         ! Type de solveur 
                                    -1, &                                ! Fréquence de sortie des champs
                                    "Ensight Gold",&                    ! Format des fichiers de visu
                                    "text")                             ! Output option


        !--- Création du maillage de couplage propre à codeName --------!
        CALL CWIPI_DEFINE_MESH_F(couplingID, &                          ! Identifiant de couplage
                                nsom_limi, &                            ! Nombre de sommets
                                nfac_limi, &                            ! Nombre d'éléments = nombre de faces limites
                                coordinates, &                          ! Coordonnées des sommets
                                connectivity_index, &                   ! Tableau des index pour lecture de connectivity
                                connectivity)                           ! Tableau de connectivité face > sommet

        !--- Localisation des points de valeurs échangées --------------!
        CALL CWIPI_LOCATE_F(couplingID)

        !--- Vérification que les points échangés sont bien localisés --!
        CALL CWIPI_GET_N_NOT_LOCATED_PTS_F(couplingID, nNotLocatedPoints)
        IF (nNotLocatedPoints /= 0) THEN
            CALL print_err('Il y a des points non-localisés lors du couplage CWIPI '//couplingID)
        END IF

        ! Désallocation des tableaux
        DEALLOCATE( coordinates, connectivity, connectivity_index )
        DEALLOCATE( SOM_LIMi, SOM_ABS_LIMi )

        ! Allocation des tableaux d'échanges de valeurs 
        IF (DOM%CDT%LIM(ilim)%nvar_isend_coupling > 0) THEN
            ALLOCATE( DOM%CDT%LIM(ilim)%ISEND_values( nfac_limi * DOM%CDT%LIM(ilim)%nvar_isend_coupling ))
            DOM%CDT%LIM(ilim)%ISEND_values(:) = 0.0_dp
        END IF
        IF (DOM%CDT%LIM(ilim)%nvar_irecv_coupling > 0) THEN
            ALLOCATE( DOM%CDT%LIM(ilim)%IRECV_values( nfac_limi * DOM%CDT%LIM(ilim)%nvar_irecv_coupling ))
            DOM%CDT%LIM(ilim)%IRECV_values(:) = 0.0_dp
        END IF
        
    END IF

END DO ! Fin boucle limite
!
END SUBROUTINE solve_C_init
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine d'échanges CWIPI pour le couplage externe
SUBROUTINE solve_C( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
!
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_FACLIMi
REAL(DP), ALLOCATABLE, DIMENSION(:) :: values
INTEGER(IP), DIMENSION(DOM%CDT%nlim) :: Rrequest, Srequest
!
CHARACTER (len=128) :: couplingID
CHARACTER (len=256) :: isend_coupling_name, irecv_coupling_name
character(len=256)  :: exch_name      !nom de la quantité à échanger                                 
INTEGER(IP) :: ilim, nlim, ifac_limi, nfac_limi, id_fac
INTEGER(IP) :: tag
INTEGER(IP) :: time_step
INTEGER(IP) :: ivar, nvar_isend_coupling, nvar_irecv_coupling
REAL(DP) :: time_value
REAL(DP) :: rho_g_itp
!
!
! Boucle pour chaque limite du système
nlim = DOM%CDT%nlim
DO ilim = 1,nlim

    ! Si pour la limite courante, il existe une application de couplage active, alors :
    IF (DOM%CDT%LIM(ilim)%couplage_lim == 1) THEN
        
        ! Identifiant de couplage = nom de la limite du domaine modethec, identique pour le code à coupler
        couplingID = DOM%CDT%LIM(ilim)%nom_lim
        
        ! Initilisation dans boucle de limite
        nfac_limi = DOM%CDT%LIM(ilim)%nfac_limi
        ALLOCATE( ID_FACLIMi(nfac_limi) )
        ID_FACLIMi = DOM%CDT%LIM(ilim)%ID_FACLIMi

        ! Valeurs du no d'itération + valeur de temps
        time_step = DOM%NUM%iiter / DOM%NUM%iiter_couplage
        time_value = DOM%NUM%ti


        !-----------------------------------------------------------------!
        !--- Partie préparation de la réception des messages CWIPI -------!
        nvar_irecv_coupling = DOM%CDT%LIM(ilim)%nvar_irecv_coupling

        ! Si réception demandée...
        IF (nvar_irecv_coupling > 0) THEN

            ! Initialisation variables envoi
            irecv_coupling_name = ""
            DOM%CDT%LIM(ilim)%IRECV_values(:) = 0.0_dp

            ! Mise en forme des irecv_coupling_name
            DO ivar = 1,nvar_irecv_coupling
                irecv_coupling_name = TRIM(ADJUSTL(irecv_coupling_name))//' '// &
                                      TRIM(ADJUSTL(DOM%CDT%LIM(ilim)%IRECV_COUPLING_NAME(ivar)))
            END DO

            tag = 100
            exch_name = 'ech_'//trim(couplingID)                                 
            !--- Préparation de IRECV_values sur le couplingID --------------!
            CALL CWIPI_IRECEIVE_F(couplingID, &                              ! Identifiant de couplage
                    exch_name, &                                             ! Exchange name
                    tag, &                                                   ! tag : identifiant MPI
                    nvar_irecv_coupling, &                                   ! stride : nombre de variables à envoyer (champs entrelacés)
                    time_step, &                                             ! Time step (only for visualization)
                    time_value, &                                            ! Time value (only for visualization)
                    irecv_coupling_name, &                                   ! Nom des champs entrelacés
                    DOM%CDT%LIM(ilim)%IRECV_values, &                       ! Valeurs des champs entrelacés
                    Rrequest(ilim) )                                         ! Identifiant MPI du message

        END IF
        !--- Fin préparation de la réception des messages CWIPI ----------!
        !-----------------------------------------------------------------!

        !-----------------------------------------------------------------!
        !--- Partie préparation de l'envoi des messages CWIPI ------------!
        nvar_isend_coupling = DOM%CDT%LIM(ilim)%nvar_isend_coupling

        ! Si envoi demandé...
        IF (nvar_isend_coupling > 0) THEN

            ! Initialisation variables envoi
            ALLOCATE( values(nfac_limi) )
            isend_coupling_name = ""
            DOM%CDT%LIM(ilim)%ISEND_values(:) = 0.0_dp

            ! Mise en forme des values et des isend_coupling_name
            DO ivar = 1,nvar_isend_coupling

                values(:) = 0.0_dp

                ! Choix du type de variable à envoyer...
                SELECT CASE(DOM%CDT%LIM(ilim)%ISEND_INT_NAME(ivar))

                    ! Si Flux à la paroi = -k * gradt_T * vfac
                    CASE("Fimp")
                        values = - SUM( DOM%ETAT%k_itp(ID_FACLIMi,:) * &
                                        DOM%ETAT%grad_T_itp(ID_FACLIMi,:) * &
                                        DOM%MLG%VFAC(ID_FACLIMi,:), 2)

                    ! Si Température de paroi
                    CASE("Timp")
                        values = DOM%ETAT%T_itp(ID_FACLIMi)

                    ! Si Température de convection
                    CASE("T_conv")
                        values = DOM%ETAT%T_itp(ID_FACLIMi)

                    ! Si coefficient d'échange convectif
                    CASE("h_conv")
                        values = - SUM( DOM%ETAT%k_itp(ID_FACLIMi,:) * &
                                        DOM%ETAT%grad_T_itp(ID_FACLIMi,:) * &
                                        DOM%MLG%VFAC(ID_FACLIMi,:), 2)
                        values = values / ( DOM%ETAT%T_itp(ID_FACLIMi) - DOM%CDT%LIM(ilim)%Timp(:) )
                        
                    ! Si température d'environnement radiatif
                    CASE("T_rad")
                        values = DOM%ETAT%T_itp(ID_FACLIMi)
                    
                    ! Si Débit à la paroi
                    CASE("Dimp")
                        values = SUM(DOM%ETAT%v_g_itp(ID_FACLIMi,:) * &
                                        DOM%MLG%VFAC(ID_FACLIMi,:), 2)

                        ! Pour chaque face limite
                        DO ifac_limi = 1,nfac_limi
                            id_fac = ID_FACLIMi(ifac_limi)

                            ! Interpolation de rho_g sur face limite
                            CALL interp_one_fac( DOM%ETAT%rho_g, rho_g_itp, DOM%MLG%FAC_ITP(id_fac), DOM%MLG )

                            values(ifac_limi) = values(ifac_limi) * rho_g_itp
                        END DO

                    ! Si Pression à la paroi
                    CASE("Pimp")
                        values = DOM%ETAT%P_itp(ID_FACLIMi)

                    ! Sinon : erreur
                    CASE DEFAULT
                        CALL print_err("Sent variable "//TRIM(DOM%CDT%LIM(ilim)%ISEND_INT_NAME(ivar))//" is not available", 1)

                END SELECT

                ! Ecriture de values
                DO ifac_limi= 1,nfac_limi
                    DOM%CDT%LIM(ilim)%ISEND_values( ivar + nvar_isend_coupling * (ifac_limi-1) ) = values( ifac_limi )
                END DO

                ! Ecriture de isend_coupling_name
                isend_coupling_name = TRIM(ADJUSTL(isend_coupling_name))//' '// &
                                      TRIM(ADJUSTL(DOM%CDT%LIM(ilim)%ISEND_COUPLING_NAME(ivar)))

            END DO
            
            tag = 100
            exch_name = 'ech_'//trim(couplingID)                                 
            !--- Envoi de ISEND_values sur le couplingID --------------------!
            CALL CWIPI_ISSEND_F(couplingID, &                                ! Identifiant de couplage
                    exch_name, &                                             ! Exchange name
                    tag, &                                                   ! tag : identifiant MPI
                    nvar_isend_coupling, &                                   ! stride : nombre de variables à envoyer (champs entrelacés)
                    time_step, &                                             ! Time step (only for visualization)
                    time_value, &                                            ! Time value (only for visualization)
                    isend_coupling_name, &                                   ! Nom des champs entrelacés
                    DOM%CDT%LIM(ilim)%ISEND_values, &                       ! Valeurs des champs entrelacés
                    Srequest(ilim) )                                         ! Identifiant MPI du message


            DEALLOCATE( values ) ! Désallocation

        END IF
        !--- Fin préparation de l'envoi des messages CWIPI ---------------!
        !-----------------------------------------------------------------!

        DEALLOCATE( ID_FACLIMi ) ! Désallocation
        
        ! Print de couplage lorsque tout est préparé
        IF (DOM%NUM%iiter == 0) THEN
            CALL print_solve_C_ini( couplingID, exch_name, &
                                    tag, nvar_isend_coupling, nvar_irecv_coupling, &
                                    isend_coupling_name, irecv_coupling_name, &
                                    DOM%SUI%suivi, DOM%SUI%file_log )
        END IF

    END IF

END DO ! Fin boucle limite


! Boucle pour chaque limite du système
DO ilim = 1,nlim

    ! Si pour la limite courante, il existe une application de couplage active, alors :
    IF (DOM%CDT%LIM(ilim)%couplage_lim == 1) THEN
        
        ! Identifiant de couplage = nom de la limite du domaine modethec, identique pour le code à coupler
        couplingID = DOM%CDT%LIM(ilim)%nom_lim

        nfac_limi = DOM%CDT%LIM(ilim)%nfac_limi

        ! Attente validation envoi de Srequest
        nvar_isend_coupling = DOM%CDT%LIM(ilim)%nvar_isend_coupling
        IF (nvar_isend_coupling > 0) THEN
            CALL CWIPI_WAIT_ISSEND_F( couplingID, Srequest(ilim) )
        END IF

        ! Attente validation réception de Rrequest
        nvar_irecv_coupling = DOM%CDT%LIM(ilim)%nvar_irecv_coupling
        IF (nvar_irecv_coupling > 0) THEN
            CALL CWIPI_WAIT_IRECV_F( couplingID, Rrequest(ilim) )
        END IF
        

        ! Si réception demandée...
        IF (nvar_irecv_coupling > 0) THEN

            ! Initialisation variables réception
            ALLOCATE( values(nfac_limi) )

            ! Mise en forme des values et des irecv_coupling_name
            DO ivar = 1,nvar_irecv_coupling

                values(:) = 0.0_dp

                ! Ecriture de values à partir des champs entrelacés reçus
                DO ifac_limi= 1,nfac_limi
                    values(ifac_limi) = DOM%CDT%LIM(ilim)%IRECV_values( ivar + nvar_irecv_coupling * (ifac_limi-1) )
                END DO

                ! Choix du type de variable à recevoir...
                SELECT CASE(DOM%CDT%LIM(ilim)%IRECV_INT_NAME(ivar))

                    ! Si Flux à la paroi
                    CASE("Fimp")
                        DOM%CDT%LIM(ilim)%Fimp(:) = values(:)

                    ! Si Température de paroi
                    CASE("Timp")
                        DOM%CDT%LIM(ilim)%Timp(:) = values(:)

                    ! Si Température de convection
                    CASE("T_conv")
                        DOM%CDT%LIM(ilim)%T_conv(:) = values(:)

                    ! Si coefficient d'échange convectif
                    CASE("h_conv")
                        DOM%CDT%LIM(ilim)%h_conv(:) = values(:)

                    ! Si température d'environnement radiatif
                    CASE("T_rad")
                        DOM%CDT%LIM(ilim)%T_rad(:) = values(:)
                    
                    ! Si Débit à la paroi
                    CASE("Dimp")
                        DOM%CDT%LIM(ilim)%Dimp(:) = values(:)
                    
                    ! Si Pression à la paroi
                    CASE("Pimp")
                        DOM%CDT%LIM(ilim)%Pimp(:) = values(:)

                    ! Sinon : erreur
                    CASE DEFAULT
                        CALL print_err("Received variable "//TRIM(DOM%CDT%LIM(ilim)%IRECV_INT_NAME(ivar))//" is not available", 1)

                END SELECT
                
            END DO

            DEALLOCATE( values ) ! Désallocation

        END IF

        ! Print de couplage lorsque tout est préparé
        IF (MODULO(DOM%NUM%iiter, DOM%SUI%iiter_prt) == 0) THEN
            CALL print_solve_C_end( couplingID, DOM%SUI%suivi, DOM%SUI%file_log )
        END IF

    END IF

END DO ! Fin boucle limite
!
!
END SUBROUTINE solve_C
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Finalisation des echanges CWIPI
SUBROUTINE solve_C_end( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
CHARACTER (len=128) :: couplingID
INTEGER(IP) :: ilim, nlim

! Boucle pour chaque limite du système
nlim = DOM%CDT%nlim
DO ilim = 1,nlim

    ! Si pour la limite courante, il existe une application de couplage active, alors :
    IF (DOM%CDT%LIM(ilim)%couplage_lim == 1) THEN
                
        ! Identifiant de couplage = nom de la limite du domaine modethec, identique pour le code à coupler
        couplingID = DOM%CDT%LIM(ilim)%nom_lim

        ! Effacement du couplage couplingID
        CALL CWIPI_DELETE_COUPLING_F( couplingID )

    END IF

END DO ! Fin boucle limite

! Initialisation de l'environnement CWIPI
CALL CWIPI_FINALIZE_F()

END SUBROUTINE solve_C_end
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_solve_C

