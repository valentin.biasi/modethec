!> Module pour resolution du cycle des termes STRUCTURES
MODULE mod_solve_S
!
USE mod_cst, ONLY : DP, IP, eps_min
USE mod_print, ONLY : print_err
USE mod_algebra, ONLY : normL2, realtostr
USE mod_structure, ONLY : cyc_structure, cyc_structure_ext
USE mod_jac_S, ONLY : jac_S
USE mod_gmres, ONLY : mgmres_st, ax_st
USE mod_prop, ONLY : prop_meca
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Choix de la methode de resolution du systeme STRUCTURE
SUBROUTINE solve_S( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
CHARACTER (len=32) :: schema_S
REAL(DP) :: dt_ref, dt_tot
INTEGER(IP) :: issiter
! REAL(DP), DIMENSION(DOM%MLG%ncel) :: DT_loc

! Methode choisie par l'utilisateur
schema_S = DOM%NUM%schema_S

! Re-initialisation du booleen de convergence
DOM%RSL%converg_S = .FALSE.

! Choix de la methode STRUCTURE
SELECT CASE (schema_S)

    !------------------------------------------------------------------!
    ! Cas Euler explicite (cherche etat stationnaire)
    CASE ('euler-exp')
        dt_ref = DOM%NUM%dt
        dt_tot = 0.0_dp
        issiter = 0
        
        CALL print_err('Euler explicit mechanical solver is no more maintained', 1)
        
        ! ! Arret si pas de temps dt_ref est ecoulee ou convergence explicite activee
        ! DO WHILE ( (ABS(dt_ref-dt_tot) > 2.0_dp*eps_min) .AND. .NOT.(DOM%RSL%converg_S)) 
        
        !     ! DT_loc aux faces
        !     DT_loc = 0.99_dp * SQRT( DOM%MLG%n2_G1G2 )
        !     DT_loc = DT_loc * SQRT( DOM%ETAT%rho_itp )
        !     DT_loc = DT_loc / SQRT( DOM%PHYS%MAT%E_iso / (1.0_dp-DOM%PHYS%MAT%P_iso**2) )
            
        !     ! Calcul du dt minimal en explicite
        !     DOM%NUM%dt = MINVAL( ABS(DT_loc) )
        !     DOM%NUM%dt = MIN( DOM%NUM%dt, ABS(dt_ref-dt_tot) )
   
        !     ! Appel a la routine de resolution explicite avec le pas de temps dt
        !     CALL solve_S_eulerexp( DOM )
            
        !     ! Avance en pas de temps
        !     dt_tot = dt_tot + DOM%NUM%dt
        !     ! Et en nb d'it de resol explicite
        !     issiter = issiter + 1
            
        !     ! Si err_it_S < tol => equations STRUCTURE convergees
        !     IF (DOM%RSL%err_it_S < DOM%NUM%tol_abs) THEN
        !         DOM%RSL%converg_S = .TRUE.
        !     END IF
    
        ! ! print*,normL2( DOM%RSL%dU(:,1)/DOM%MLG%VOLUME(:) ) + normL2( DOM%RSL%dU(:,2)/DOM%MLG%VOLUME(:) )
        ! END DO

        DOM%NUM%dt = dt_ref
        DOM%RSL%niter_S = issiter
        
    !------------------------------------------------------------------!
    ! Cas schema stationnaire implicte
    CASE ('implicite')
    
        CALL solve_S_implicite( DOM )
        
    !------------------------------------------------------------------!
    ! Sinon erreur
    CASE DEFAULT
        CALL print_err('La methode d''integration schema_S n''est pas reconnue',1)
        
END SELECT

END SUBROUTINE solve_S
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de resolution S euler explicite
SUBROUTINE solve_S_eulerexp( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
REAL(DP) ::  tmp

!----------------------------------------------------------------------!
! 1/ on determine U(n+1) vecterus déplacements pour la resolution des equations de structure
!     rho * (U(n+1) - U(n)) / dt^2 = STRUCTURE

! Remise a zero des termes de bilan qte de mouvement
DOM%RSL%dU(:,:) = 0.0_dp
DOM%RSL%FL_struct(:,:) = 0.0_dp

! Evaluation des nouvelles proprietes meca materiau
CALL prop_meca( DOM )

! Evaluation des seconds membres des equations de conservation de qte de mouvement solide : dU
CALL cyc_structure( DOM )


! Integration Euler explicite : rho * d^2U/dt^2 = F(U)
! => U(n+1) = U(n) + 1/m*dt^2 * F(U(n))
DOM%RSL%U = DOM%RSL%U_cou + DOM%NUM%dt**2 / SPREAD(DOM%RSL%m(:),2,3) * DOM%RSL%dU


! Calcul des nouveaux residus
tmp = DOM%RSL%err_it_S
DOM%RSL%err_it_S = normL2( DOM%ETAT%rho(:)*(DOM%RSL%U(:,1) - DOM%RSL%U_cou(:,1)) / DOM%NUM%dt**2 )
DOM%RSL%err_it_S = DOM%RSL%err_it_S + &
     normL2( DOM%ETAT%rho(:)*(DOM%RSL%U(:,2) - DOM%RSL%U_cou(:,2)) / DOM%NUM%dt**2 )


! Mise a jour des variables naturelles
DOM%RSL%U_cou = DOM%RSL%U   ! Etat n+1 -> Etat n


! Verification de stabilite du calcul explicite
! Residu(n+1) < Residu(n) + 1 sinon erreur
! Le +1 evite de tenir compte des oscillations mineures
IF ((DOM%RSL%err_it_S > tmp+1.0_dp) .AND. tmp > 2.0_dp*eps_min) THEN
    CALL print_err('La resolution STRUCTURE explicite semble diverger')
    CALL print_err('Veuillez abaisser le critere CFL_max_S')
END IF


END SUBROUTINE solve_S_eulerexp
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de resolution S implicite
SUBROUTINE solve_S_implicite( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
REAL(DP), DIMENSION(DOM%RSL%ndl_S) :: F_ext, R
!REAL(DP), DIMENSION(DOM%RSL%ndl_S) :: diag_J, RHS
REAL(DP) :: residu, eps_conv
INTEGER(IP) :: iiter
INTEGER(IP) :: niter_lin
INTEGER(IP) :: ncel
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel

! Iteration initiale
iiter = 1

! Critere d'arret
eps_conv = 1.0e-1_dp !DOM%NUM%tol_abs

!----------------------------------------------------------------------!
! 1/ on determine U solution des equations de mecanique solide
!     div . sigma(U) = F_ext

!--- PREPARATION ------------------------------------------------------!

! Deplacements a l'it. precedente sauvé dans Q
CALL solve_S_setQ( DOM )

! Evaluation des nouvelles proprietes meca materiau
CALL prop_meca( DOM )

! Evaluation des residus equations de qte de mouvement solide : dU
DOM%RSL%dU(:,:) = 0.0_dp
DOM%RSL%FL_struct(:,:) = 0.0_dp
CALL cyc_structure( DOM )
! 
residu = normL2( DOM%RSL%dU(:,1) ) + normL2( DOM%RSL%dU(:,2) )

! Quitte si le residu n'est pas suffisant (en statique au moins)
IF (residu <= eps_conv) THEN
    DOM%RSL%niter_S = 0
    DOM%RSL%err_it_S = residu
    
    RETURN
END IF


!--- MATRICE JACOBIENNE -----------------------------------------------!
DOM%RSL%JAC_S(:,:,:,:) = 0.0_dp
DOM%RSL%JAC_S_stval(:) = 0.0_dp

! Evaluation de la matrice jacobienne de la fonction dU( Q(n) )
CALL jac_S( DOM )


! Mise ne forme de la matrice jacobnienne dRdQ* en forme ST (sparse triplet)
CALL solve_S_setST( DOM%RSL%JAC_S, DOM%RSL%id_JAC_S, DOM%RSL%JAC_S_stval )


!--- SECONDS MEMBRES DES EQUATIONS DE CONSERVATION --------------------!
! Seconds membres des equations de qte de mouvement solide : dU
DOM%RSL%dU(:,:) = 0.0_dp
DOM%RSL%FL_struct(:,:) = 0.0_dp
F_ext(:) = 0.0_dp
!CALL cyc_structure_ext( DOM )
! NB : Fonctionne aussi avec cyc_structure -> a améliorer
CALL cyc_structure( DOM )

! RHS des EDP a l'etat initial
CALL solve_S_setF( DOM )
F_ext = DOM%RSL%F_S

! Evaluation du RHS initial
R = - F_ext - DOM%RSL%F_S_tan


!----------------------------------------------------------------------!
!--- METHODE ITERATIVE DU SYSTEME LINEAIRE STRUCTURE STATIQUE ---------!
DO WHILE ( ((residu>=eps_conv) .AND. (iiter<=DOM%NUM%niter_max)) )
!DO WHILE ( iiter <= 2 )
    
    ! Nombre d'it externe du solveur lineaire
    niter_lin = DOM%NUM%niter_max_int
    
    !--- APPEL AU SOLVEUR GMRES ---------------------------------------!
    CALL mgmres_st ( DOM%RSL%ndl_S, &         ! Nb de degres de libertes du systeme
                     DOM%RSL%nnz_S, &         ! Nombre de non-zeros dans la matrice sparse JAC_S_stval
                     DOM%RSL%JAC_S_strow, &   ! Ligne de JAC_S_stval(nnz) dans les mat. pleine A(ndl,ndl)
                     DOM%RSL%JAC_S_stcol, &   ! Col. de JAC_S_stval(nnz) dans les mat. pleine A(ndl,ndl)
                     DOM%RSL%JAC_S_stval, &   ! Valeurs de JAC_S_stval(nnz) dans les mat. pleine A(ndl,ndl)
                     DOM%RSL%Q_S, &           ! Resultats
                     R, &                  ! Termes de droite
                     niter_lin, &               ! Nb d'it. externes
                     DOM%NUM%niter_max_int, &  ! Nb d'it. internes
                     DOM%NUM%tol_abs, &        ! Tol absolue
                     0.01_dp )! DOM%NUM%tol_rel )         ! Tol relative

    ! Sauvegarde du nombre d'it. externes
    DOM%RSL%niter_S_lin = niter_lin
    
    !--- MAJ DES ETAT -------------------------------------------------!

    ! Recuperation des deplacements relatifs Ux et Uy
    CALL solve_S_getQ( DOM )
        
    ! Nouveaux bilans de forces totaux
    DOM%RSL%dU(:,:) = 0.0_dp
    DOM%RSL%FL_struct(:,:) = 0.0_dp
    CALL cyc_structure( DOM )
    
    ! Bilan de forces totaux stockées dans DOM%RSL%F_S
    CALL solve_S_setF( DOM )
    
    ! Calcul des forces tangentielles (non orthogonales)
    ! F_tot = F_S_tan + F_normal + F_ext
    ! or F_normal = JAC_S . U = R
    DOM%RSL%F_S_tan = DOM%RSL%F_S - R - F_ext
 
 ! Nouveaux seconds membres des equations de qte de mouvement solide : dU
    R = - F_ext - DOM%RSL%F_S_tan

    ! Recalcul des résidus
    residu = normL2( DOM%RSL%dU(:,1) ) + normL2( DOM%RSL%dU(:,2) )

    iiter = iiter + 1       ! Incrementation
    
 print*,'***',iiter,residu,niter_lin !; read(*,*)    
 
END DO


! Sauvergarde du nb d'it. et du residu final
DOM%RSL%niter_S = iiter - 1
DOM%RSL%err_it_S = residu

END SUBROUTINE solve_S_implicite
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Mise en forme des valeurs de la matrice A du systeme S au format Sparse Triplet 
SUBROUTINE solve_S_setST( JAC_S, id_JAC_S, JAC_S_stval )
!
!---- Déclarations ----------------------------------------------------!
REAL(DP), DIMENSION(:,:,:,:) ::  JAC_S
INTEGER(IP), DIMENSION(:,:,:,:) ::  id_JAC_S
REAL(DP), DIMENSION(:) :: JAC_S_stval
INTEGER(IP) :: nd1, nd2, nd3, nd4
INTEGER(IP) :: id1, id2, id3, id4
INTEGER(IP) :: inz

! Shape de la matrice A du systeme Ax=b dans le format sparse MoDeTheC 
nd1 = SIZE(JAC_S,1)
nd2 = SIZE(JAC_S,2)
nd3 = SIZE(JAC_S,3)
nd4 = SIZE(JAC_S,4)

! Pour chaque élément de la matrice sparse MoDeTheC
DO id1=1,nd1 ; DO id2=1,nd2 ; DO id3=1,nd3 ; DO id4 = 1,nd4

    ! On récupère la position (inz) correspondante pour le format Sparse Triplet
    inz = id_JAC_S(id1,id2,id3,id4)
    ! Si la valeur fait partie du système (pas cellule limite)
    IF (inz /= 0) THEN
    
        ! On ecrit dans la matrice sparse format SP
        JAC_S_stval(inz) = JAC_S(id1,id2,id3,id4)
    
    END IF
    
! Fin triple boucle
END DO ; END DO ; END DO ; END DO

END SUBROUTINE solve_S_setST
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Mise en forme des valeurs du vecteur F(RHS des EDP) 
!> du systeme S au format Sparse Triplet 
SUBROUTINE solve_S_setF( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: icel, ncel
INTEGER(IP) :: neq_S
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
neq_S = DOM%RSL%neq_S

! NB : vecteur F_S( ndl_S )
! F_S =  | dUx (cel1)    -\
!        | dUy (cel1)     |-  cel1
!        | dUx (cel2)    -\
!        | dUy (cel2)     |-  cel2
!        | dUx (cel3)    -\
!        | dUy (cel3)     |-  cel3
!        | ....


! Pour chaque cellule
DO icel = 1,ncel

    ! Termes de flux qte de mvt suivant x
    DOM%RSL%F_S( neq_S*(icel-1) + 1 ) = DOM%RSL%dU(icel,1)! / DOM%RSL%m(icel)
    
    ! Termes de flux qte de mvt suivant y
    DOM%RSL%F_S( neq_S*(icel-1) + 2 ) = DOM%RSL%dU(icel,2)! / DOM%RSL%m(icel)
    
END DO

END SUBROUTINE solve_S_setF
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Mise en forme des valeurs du vecteur Q (quantites conservees des EDP) 
!> du systeme S au format Sparse Triplet 
SUBROUTINE solve_S_setQ( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: icel, ncel
INTEGER(IP) :: neq_S
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
neq_S = DOM%RSL%neq_S

! NB : vecteur Q_S( ndl_S )
! Q_S =  | Ux (cel1)    -\
!        | Uy (cel1)     |-  cel1
!        | Ux (cel2)    -\
!        | Uy (cel2)     |-  cel2
!        | Ux (cel3)    -\
!        | Uy (cel3)     |-  cel3
!        | ....

! Pour chaque cellule
DO icel = 1,ncel
    
    ! Deplacement relatif Ux
    DOM%RSL%Q_S( neq_S*(icel-1) + 1 ) = DOM%RSL%U(icel,1)
    
    ! Deplacement relatif Uy
    DOM%RSL%Q_S( neq_S*(icel-1) + 2 ) = DOM%RSL%U(icel,2)
    
END DO

END SUBROUTINE solve_S_setQ
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Recuperation des valeurs du vecteur Q (quantites conservees des EDP) 
!> du systeme S au format Sparse Triplet 
SUBROUTINE solve_S_getQ( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: icel, ncel
INTEGER(IP) :: neq_S
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
neq_S = DOM%RSL%neq_S

! NB : vecteur Q_S( ndl_S )
! Q_S =  | Ux (cel1)    -\
!        | Uy (cel1)     |-  cel1
!        | Ux (cel2)    -\
!        | Uy (cel2)     |-  cel2
!        | Ux (cel3)    -\
!        | Uy (cel3)     |-  cel3
!        | ....

! Pour chaque cellule
DO icel = 1,ncel
    
    ! Deplacement relatif Ux
    DOM%RSL%U(icel,1) = DOM%RSL%Q_S( neq_S*(icel-1) + 1 )
    
    ! Deplacement relatif Uy
    DOM%RSL%U(icel,2) = DOM%RSL%Q_S( neq_S*(icel-1) + 2 )
    
END DO

END SUBROUTINE solve_S_getQ
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_solve_S
