!> Module pour resolution du cycle a un pas de temps donne
MODULE mod_solve
!
USE mod_print, ONLY : print_err
USE mod_cst, ONLY : DP, IP, eps_min
USE mod_solve_DR, ONLY : solve_DR
USE mod_solve_A, ONLY : solve_A
USE mod_solve_S, ONLY : solve_S
! USE mod_solve_AB, ONLY : solve_AB
USE mod_solve_AB3D, ONLY : solve_AB3D
USE mod_solve_C, ONLY : solve_C
USE mod_prop, ONLY : prop_stab
USE mod_dat_bld_cls, ONLY : update_cls
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Routine cyc_solve pour la resolution entre t et t+dt
SUBROUTINE cyc_solve( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM

!----------------------------------------------------------------------!
! Methode d'operator-splitting du 1er ordre utilisee pour resoudre les equations 
! de la forme : dQ/dt = DIFFUSION + ADVECTION + REACTION
! On separe en 2 sous-resolutions ces equations :

! 1/ on determine Q* solution avec les termes reactifs et diffusifs seuls
!     (Q* - Q(n)) / dt = DIFFUSION + REACTION
IF ( DOM%NUM%DIFFUSION == 'on' ) THEN
    
    DOM%MLG%VOLUME_TEMP = DOM%MLG%VOLUME
    CALL solve_DR( DOM )
    DOM%MLG%VOLUME = DOM%MLG%VOLUME_TEMP

END IF


! Resolution du probleme d'ablation
IF ( DOM%NUM%ABLATION == 'on' ) THEN

    ! CALL solve_AB ( DOM )   

    IF ( ABS( MAXVAL(DOM%ETAT%taux_reg) ) > 10*eps_min ) CALL solve_AB3D ( DOM )
    
END IF


! 2/ on resout alors a partir de Q* les termes advectifs
!    (Q(n+1) - Q*) / dt = ADVECTION
IF ( DOM%NUM%ADVECTION == 'on' ) THEN

    CALL solve_A( DOM )
    
END IF


! Resolution des equations de structures
IF ( DOM%NUM%STRUCTURE == 'on' ) THEN
    IF (MODULO(DOM%NUM%iiter,DOM%EXPT%iiter_exp) == 0) THEN

        CALL solve_S( DOM )
        
END IF
END IF


! Couplage avec solveur externe
IF ( DOM%NUM%COUPLAGE == 'on' ) THEN
    IF ((MODULO(DOM%NUM%iiter,DOM%NUM%iiter_couplage) == 0) .AND. (DOM%NUM%iiter /= DOM%NUM%niter)) THEN
    
        CALL solve_C ( DOM )
    
    END IF
END IF


! Update des champs liés aux conditions limites spéciales
CALL update_cls( DOM )

! Evaluation des coeef de stabilite : CFL + Fo
CALL prop_stab( DOM )


END SUBROUTINE cyc_solve
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_solve
