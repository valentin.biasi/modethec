!> Module pour resolution du cycle de l'equation en PRESSION (DARCY)
!> ATTENTION : CE MODULE EST OBSOLETE ET N'EST PLUS UTILISE !!
MODULE mod_solve_P
!
USE mod_cst, ONLY : DP, IP, eps_min, R_GP
USE mod_print, ONLY : print_err
USE mod_algebra, ONLY : normL2, realtostr
USE mod_reset, ONLY : cyc_reset
USE mod_advection, ONLY : cyc_advection 
USE mod_prop, ONLY : prop_vg, cyc_prop_A
USE mod_jac_P, ONLY : jac_P_anal
USE mod_gmres, ONLY : mgmres_st
USE mod_interpolate, ONLY : interp_P
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Routine de resolution P theta implicite
SUBROUTINE solve_P_thetaimp( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
REAL(DP), DIMENSION(DOM%MLG%ncel) :: P_0, F_0, F_1, R, delta_P, dm_g
REAL(DP) :: theta_imp, dt, residu, eps_conv
INTEGER(IP) :: iiter
INTEGER(IP) :: niter_lin
INTEGER(IP) :: igaz, ngaz, iesp
!
!---- Récupérations ---------------------------------------------------!
theta_imp = DOM%NUM%theta_imp
dt = DOM%NUM%dt
ngaz = DOM%PHYS%ngaz

!----------------------------------------------------------------------!
! 1/ on determine Q(n+1) solution avec les termes advectifs seuls
!     (Q(n+1) - Q*) / dt = ADVECTIFS

!----------------------------------------------------------------------!
!--- 1ERE EVAL DES RESIDUS

!--- PREPARATION ------------------------------------------------------!

! Remise a zero des termes de flux
delta_P(:) = 0.0_dp
dm_g(:) = 0.0_dp
CALL cyc_reset( DOM )

! Pression a l'etat precedent sauvé dans P_0
P_0(:) = DOM%ETAT%P(:)

!--- SECONDS MEMBRES DES EQUATIONS DE CONSERVATION --------------------!
! Termes advectifs : dE et dm_e (gaz)
CALL cyc_advection( DOM )
DO igaz = 1,ngaz
    iesp = DOM%PHYS%ID_gaz(igaz)
    dm_g = dm_g + DOM%RSL%dm_e(:,iesp)
END DO    

! RHS des EDP a l'etat precedent sauve dans F_0
F_0(:) = dm_g(:) * R_GP * DOM%ETAT%T(:) / DOM%ETAT%M(:) / DOM%ETAT%phi_g(:) / DOM%MLG%VOLUME(:)

! Evaluation du residu initial
R(:) = dt*F_0(:)
residu = normL2( R(:) )

! Critere d'arret
eps_conv = (residu * DOM%NUM%tol_rel) + DOM%NUM%tol_abs

!print*,'***',eps_conv,residu

!----------------------------------------------------------------------!
!--- METHODE ITERATIVE DE NEWTON-RAPHSON DU SYSTEME ADVECTION ---------!
iiter = 1
DO WHILE ( (residu>=eps_conv) .AND. (iiter<= 1 ))!DOM%NUM%niter_max) )
    
    ! Remise a zero des termes de flux et de jacobiennes
    delta_P(:) = 0.0_dp
    dm_g(:) = 0.0_dp
    CALL cyc_reset( DOM )
    
    ! Reevaluation des proprietes
    !CALL cyc_prop_A( DOM )
    
    !--- EVALUATION DES FLUX ------------------------------------------!
    CALL cyc_advection( DOM )
    DO igaz = 1,ngaz
        iesp = DOM%PHYS%ID_gaz(igaz)
        dm_g = dm_g + DOM%RSL%dm_e(:,iesp)
    END DO 
    
    ! RHS des EDP a l'etat precedent sauve dans F_0
    F_1(:) = dm_g(:) * R_GP * DOM%ETAT%T(:) / DOM%ETAT%M(:) / DOM%ETAT%phi_g(:) / DOM%MLG%VOLUME(:)

    ! Terme de droite du systeme lineaire (Id - dt*theta*dFdQ) dQ = -R
    R = DOM%ETAT%P - P_0 - theta_imp*dt*F_1(:) - (1.0_dp-theta_imp)*dt*F_0(:)
    R = -R
    
    !--- MATRICE JACOBIENNE -------------------------------------------!
    ! Evaluation de la matrice jacobienne de la fonction F( Q*, Q(n) )
    CALL jac_P_anal( DOM )

    ! Evaluation de la matrice jacobienne de la fonction R = (Q* - Q(n)) - dt*F( Q*, Q(n) )
    ! dR/dQ* = Id - theta*dt*dF/dQ
    DOM%RSL%JAC_P(:,:) = - (theta_imp*dt) * DOM%RSL%JAC_P(:,:)
    DOM%RSL%JAC_P(:,1) = 1.0_dp + DOM%RSL%JAC_P(:,1)
    
    ! Mise ne forme de la matrice jacobnienne dRdQ* en forme ST (sparse triplet)
    CALL solve_P_setST( DOM%RSL%JAC_P, DOM%RSL%id_JAC_P, DOM%RSL%JAC_P_stval )
    
    ! Nombre d'it externe du solveur lineaire
    niter_lin = DOM%NUM%niter_max_int
    
    !--- APPEL AU SOLVEUR GMRES ---------------------------------------!
    CALL mgmres_st ( DOM%MLG%ncel, &         ! Nb de degres de libertes du systeme
                     DOM%RSL%nnz_P, &         ! Nombre de non-zeros dans la matrice sparse JAC_A_stval
                     DOM%RSL%JAC_P_strow, &   ! Ligne de JAC_A_stval(nnz) dans les mat. pleine A(ndl,ndl)
                     DOM%RSL%JAC_P_stcol, &   ! Col. de JAC_A_stval(nnz) dans les mat. pleine A(ndl,ndl)
                     DOM%RSL%JAC_P_stval, &   ! Valeurs de JAC_A_stval(nnz) dans les mat. pleine A(ndl,ndl)
                     delta_P, &                      ! Resultats
                     R, &                       ! Termes de droite
                     niter_lin, &               ! Nb d'it. externes
                     DOM%NUM%niter_max_int, &  ! Nb d'it. internes
                     DOM%NUM%tol_abs, &        ! Tol absolue
                     DOM%NUM%tol_rel )         ! Tol relative
    
    
    !--- MAJ DES ETAT -------------------------------------------------!
    ! Nouvelles quantites conservees
    DOM%ETAT%P = DOM%ETAT%P + delta_P
    
    ! Interpolation et calcul des gradients pour pression
    CALL interp_P(  DOM%ETAT%P, &              ! Pressions aux cel.
            DOM%MLG%ncel, &            ! Nombre de cel.
            DOM%ETAT%P_itp, &          ! Interpolées aux faces
            DOM%ETAT%grad_P_itp, &     ! Gradients aux faces
            DOM%MLG%nfac, &            ! Nombre de faces
            DOM%MLG, &                 ! Type données maillage
            DOM%CDT )                  ! Type données conditions limites
            
    ! Calcul des vitesses de Darcy
    CALL prop_vg( DOM ) 
    
    ! TEST
    !DOM%ETAT%rho_g(:) = DOM%ETAT%P(:) * DOM%ETAT%M(:) / R_GP / DOM%ETAT%T(:)

    ! TEST !!!! ATTENTION !!!! (:,2)
    !DOM%RSL%m_e(:,2) = DOM%ETAT%rho_g(:) * DOM%ETAT%phi_g(:) * DOM%MLG%VOLUME(:)

    ! TEST
    ! CALL cyc_update_A( DOM )

    ! Recalcul des résidus
    R = DOM%ETAT%P - P_0 - theta_imp*dt*F_1(:) - (1.0_dp-theta_imp)*dt*F_0(:)
    residu = normL2( R(:) )
    
    
    iiter = iiter + 1       ! Incrementation
    
END DO          

!print*,'Resol P : ',iiter,residu
! Sauvergarde du nb d'it. et du residu final
DOM%RSL%niter_A = iiter - 1
DOM%RSL%err_it_A = residu

END SUBROUTINE solve_P_thetaimp
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Mise en forme des valeurs de la matrice A du systeme A au format Sparse Triplet 
SUBROUTINE solve_P_setST( JAC_P, id_JAC_P, JAC_P_stval )
!
!---- Déclarations ----------------------------------------------------!
REAL(DP), DIMENSION(:,:) ::  JAC_P
INTEGER(IP), DIMENSION(:,:) ::  id_JAC_P
REAL(DP), DIMENSION(:) :: JAC_P_stval
INTEGER(IP) :: nd1, nd2
INTEGER(IP) :: id1, id2
INTEGER(IP) :: inz

! Shape de la matrice A du systeme Ax=b dans le format sparse MoDeTheC 
nd1 = SIZE(JAC_P,1)
nd2 = SIZE(JAC_P,2)

! Pour chaque élément de la matrice sparse MoDeTheC
DO id1=1,nd1 ; DO id2=1,nd2

    ! On récupère la position (inz) correspondante pour le format Sparse Triplet
    inz = id_JAC_P(id1,id2)
    ! Si la valeur fait partie du système (pas cellule limite)
    IF (inz /= 0) THEN
    
        ! On ecrit dans la matrice sparse format SP
        JAC_P_stval(inz) = JAC_P(id1,id2)
    
    END IF
    
! Fin triple boucle
END DO ; END DO

END SUBROUTINE solve_P_setST
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_solve_P
