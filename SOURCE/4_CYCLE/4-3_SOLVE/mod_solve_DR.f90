!> Module pour resolution du cycle des termes DIFFUSIF + REACTIF
MODULE mod_solve_DR
!
USE mod_cst, ONLY : DP, IP
USE mod_print, ONLY : print_err
USE mod_algebra, ONLY : normL2, realtostr
USE mod_prop, ONLY : cyc_prop_D, prop_P
USE mod_reset, ONLY : cyc_reset
USE mod_diffusion, ONLY : cyc_diffusion
USE mod_reac, ONLY : cyc_reac
USE mod_source, ONLY : cyc_source
USE mod_update, ONLY : cyc_update_DR, update_rho_rel
USE mod_jac_DR, ONLY : jac_DR
USE mod_regression, ONLY : cyc_regression
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!
!----------------------------------------------------------------------!
!> Choix de la methode de resolution du systeme DR
SUBROUTINE solve_DR( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
CHARACTER (len=32) :: schema_DR

! Methode choisie par l'utilisateur
schema_DR = DOM%NUM%schema_DR

! Choix de la methode DR
SELECT CASE (schema_DR)

    ! Cas Euler explicite
    CASE ("euler-exp")
        CALL solve_DR_eulerexp( DOM )
        
    ! Cas schema theta implicte (Euler generalise)
    CASE ("theta-imp")
        CALL solve_DR_thetaimp( DOM )
        
    ! Cas schema theta linearise (Euler avec une seule it de Newton)
    CASE ("theta-lin")
        ! CALL solve_DR_thetalin( DOM )
        CALL print_err('Ce solveur n''est plus maintenu',1)
        
    ! Sinon erreur
    CASE DEFAULT
        CALL print_err('La methode d''integration schema_DR n''est pas reconnue',1)
        
END SELECT

END SUBROUTINE solve_DR
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de resolution DR euler explicite
SUBROUTINE solve_DR_eulerexp( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM

!----------------------------------------------------------------------!
! 1/ on determine Q* solution avec les termes advectifs et diffusifs seuls
!     (Q* - Q(n)) / dt = DIFFUSION + REACTION

!--- PREPARATION ------------------------------------------------------!
! Remise a zero des termes de flux
CALL cyc_reset( DOM )

! Evaluation des nouvelles proprietes avec variables modifiees
CALL cyc_prop_D( DOM )


!--- SECONDS MEMBRES DES EQUATIONS DE CONSERVATION --------------------!

! Termes diffusifs : dE
CALL cyc_diffusion( DOM,0 )

! Termes réactifs
IF (DOM%NUM%REACTION == 'on') THEN

    ! Termes de réactions : dalpha
    CALL cyc_reac( DOM )
    ! Puis dm_e et dE correspondants
    CALL cyc_source( DOM )
    
END IF

! Verification de la stabilite du calcul
CALL solve_DR_stab( DOM )

!--- INTEGRATION EULER EXPLICITE --------------------------------------!
! Integration Euler explicite : dQ/dt = F(Q)
! => Q(n+1) = Q(n) + dt * F(Q(n))

DOM%RSL%E = DOM%RSL%E + DOM%NUM%dt * DOM%RSL%dE ! pour le bilan ener

! Termes de masse et réactifs
IF (DOM%NUM%REACTION == 'on') THEN
    
    DOM%RSL%m_e = DOM%RSL%m_e + DOM%NUM%dt * DOM%RSL%dm_e ! et les bilans massiques
    DOM%ETAT%alpha_reac = DOM%ETAT%alpha_reac + DOM%NUM%dt * DOM%RSL%dalpha ! et les avancements de reactions

END IF

! Mise a jour des variables naturelles avec m_e et E modifiés
CALL cyc_update_DR( DOM )
! + pression
IF (DOM%NUM%ADVECTION == 'on') THEN
    CALL prop_P ( DOM )
END IF

! Mise a jour des rho_rel pour les avancements des reactions
IF (DOM%NUM%REACTION == 'on') THEN
    CALL update_rho_rel( DOM )
END IF


END SUBROUTINE solve_DR_eulerexp
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de resolution DR theta implicite
SUBROUTINE solve_DR_thetaimp( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
REAL(DP), DIMENSION(DOM%RSL%ndl_DR) :: Q_0, F_0, R, dQ
REAL(DP), DIMENSION(DOM%MLG%ncel,DOM%PHYS%nesp) :: m_0
REAL(DP) :: theta_imp, dt, residu, eps_conv
INTEGER(IP) :: iiter
!
! Parametres PARDISO
INTEGER(IP) :: maxfct = 1
INTEGER(IP) :: mnum = 1
INTEGER(IP) :: phase
INTEGER(IP) :: nrhs = 1
INTEGER(IP) :: msglvl = 0
INTEGER(IP) :: error
!
!---- Récupérations ---------------------------------------------------!
theta_imp = DOM%NUM%theta_imp
dt = DOM%NUM%dt
error = 0

!----------------------------------------------------------------------!
! 1/ on determine Q* solution avec les termes réactifs et diffusifs
!     (Q* - Q(n)) / dt = DIFFUSION + REACTION

!----------------------------------------------------------------------!
!--- 1ERE EVAL DES RESIDUS

!--- PREPARATION ------------------------------------------------------!

! Remise a zero des termes de flux
dQ(:) = 0.0_dp
CALL cyc_reset( DOM )

! Quantites conservees a l'etat precedent sauvé dans Q_0
CALL solve_DR_setQ( DOM )
Q_0(:) = DOM%RSL%Q_DR(:)
! Masses a l'etat precedent
m_0 = DOM%RSL%m_e

! Evaluation des nouvelles proprietes avec variables modifiees
CALL cyc_prop_D( DOM )


!--- SECONDS MEMBRES DES EQUATIONS DE CONSERVATION --------------------!
! Termes diffusifs : dE
CALL cyc_diffusion( DOM,1 )

! Termes réactifs
IF (DOM%NUM%REACTION == 'on') THEN

    ! Termes de réactions : dalpha
    CALL cyc_reac( DOM )
    ! Puis dm_e et dE correspondants
    CALL cyc_source( DOM )

END IF

! RHS des EDP a l'etat precedent sauve dans F_0
CALL solve_DR_setF( DOM )
F_0(:) = DOM%RSL%F_DR(:)

! Evaluation du residu initial
R(:) = DOM%RSL%Q_DR(:) - Q_0(:) - (theta_imp*dt)*DOM%RSL%F_DR(:) - ((1.0_dp-theta_imp)*dt)*F_0(:)
residu = normL2( R(:) )

! Critere d'arret
eps_conv = (residu * DOM%NUM%tol_rel) + DOM%NUM%tol_abs


!----------------------------------------------------------------------!
!--- METHODE ITERATIVE DE NEWTON-RAPHSON DU SYSTEME DIFF-REAC ---------!
iiter = 1
DO WHILE ( ((residu>=eps_conv) .AND. (iiter<=DOM%NUM%niter_max)) .OR. (iiter==1) )

    ! Remise a zero des termes de flux et de jacobiennes
    dQ(:) = 0.0_dp
    CALL cyc_reset( DOM )
    
    !--- EVALUATION DES FLUX ------------------------------------------!
    !--- ET RHS DU SYSTEME --------------------------------------------!
    
    ! Evaluation des nouvelles proprietes avec variables modifiees
    CALL cyc_prop_D( DOM )
    
    ! Termes diffusifs : dE
    CALL cyc_diffusion( DOM, 0 )
    
    ! Termes réactifs
    IF (DOM%NUM%REACTION == 'on') THEN
        
        ! Termes de réactions : dalpha
        CALL cyc_reac( DOM )
        ! Puis dm_e et dE correspondants
        CALL cyc_source( DOM )
    
    END IF
    
    ! RHS des EDP au format ST
    CALL solve_DR_setF( DOM )
    
    ! Terme de droite du systeme lineaire (Id - dt*theta*dFdQ) dQ = -R
    R = DOM%RSL%Q_DR - Q_0 - theta_imp*dt*DOM%RSL%F_DR - (1.0_dp-theta_imp)*dt*F_0
    R = -R

    !--- MATRICE JACOBIENNE -------------------------------------------!
    ! Evaluation et inversion seulement si niter_max_int est depasse     DOM%NUM%niter_max_int DOM%NUM%
    IF (MODULO(iiter, DOM%NUM%niter_max_int) == 1) THEN

        ! Evaluation de la matrice jacobienne de la fonction F( Q*, Q(n) )
        CALL jac_DR( DOM )
        
        ! Evaluation de la matrice jacobienne de la fonction R = (Q* - Q(n)) - dt*F( Q*, Q(n) )
        ! dR/dQ* = Id - theta*dt*dF/dQ
        DOM%RSL%JAC_DR(:,:,:) = - (theta_imp*dt) * DOM%RSL%JAC_DR(:,:,:)
        DOM%RSL%JAC_DR(:,1,:) = 1.0_dp + DOM%RSL%JAC_DR(:,1,:)
        
        ! Mise en forme de la matrice jacobnienne dRdQ* en forme ST (sparse triplet)
        CALL solve_DR_setST( DOM%RSL%JAC_DR, DOM%RSL%id_JAC_DR, DOM%RSL%JAC_DR_stval )
        
        
    !~     DO i = 1,DOM%RSL%nnz_DR
    !~         print *, DOM%RSL%JAC_DR_csrow(i), DOM%RSL%JAC_DR_stcol(i), DOM%RSL%JAC_DR_stval(i)
    !~     END DO
        
        !--- Phase analyse+factorisation de la matrice DR avec solveur PARDISO
        phase = 12
        CALL PARDISO (DOM%RSL%PT, &            ! Adresses pointeurs internes PARDISO
                    maxfct, &                   ! Nombre de systemes
                    mnum, &                     ! Indice du systeme <= maxfct
                    DOM%RSL%MTYPE, &           ! Type de matrice = 11 (reel non symetrique)
                    phase, &                    ! Phase de resolution
                    DOM%RSL%ndl_DR, &          ! Nombre d'equations
                    DOM%RSL%JAC_DR_stval, &    ! Vecteur reel de la matrice format CSR3
                    DOM%RSL%JAC_DR_csrow, &    ! Vecteur indice ligne format CSR3
                    DOM%RSL%JAC_DR_stcol, &    ! Vecteur indice colonne format CSR3
                    DOM%RSL%PERM, &            ! Indice des permutations (non-utilise)
                    nrhs, &                     ! Nombre de RHS
                    DOM%RSL%IPARM, &           ! Tableau des parametres (defini dans don_csti_rsl.f90)
                    msglvl, &                   ! Mode verbose
                    R, &                        ! Terme de droite
                    dQ, &                       ! Solution
                    error)                      ! Code erreur
        

    END IF

    !--- Phase resolution systeme avec PARDISO
    phase = 33
    CALL PARDISO (DOM%RSL%PT, &            ! Adresses pointeurs internes PARDISO
                maxfct, &                   ! Nombre de systemes
                mnum, &                     ! Indice du systeme <= maxfct
                DOM%RSL%MTYPE, &           ! Type de matrice = 11 (reel non symetrique)
                phase, &                    ! Phase de resolution
                DOM%RSL%ndl_DR, &          ! Nombre d'equations
                DOM%RSL%JAC_DR_stval, &    ! Vecteur reel de la matrice format CSR3
                DOM%RSL%JAC_DR_csrow, &    ! Vecteur indice ligne format CSR3
                DOM%RSL%JAC_DR_stcol, &    ! Vecteur indice colonne format CSR3
                DOM%RSL%PERM, &            ! Indice des permutations (non-utilise)
                nrhs, &                     ! Nombre de RHS
                DOM%RSL%IPARM, &           ! Tableau des parametres (defini dans don_csti_rsl.f90)
                msglvl, &                   ! Mode verbose
                R, &                        ! Terme de droite
                dQ, &                       ! Solution
                error)                      ! Code erreur


    ! --- APPEL AU SOLVEUR GMRES ---------------------------------------!
    ! CALL mgmres_st ( DOM%RSL%ndl_DR, &         ! Nb de degres de libertes du systeme
    !                  DOM%RSL%nnz_DR, &         ! Nombre de non-zeros dans la matrice sparse JAC_DR_stval
    !                  DOM%RSL%JAC_DR_strow, &   ! Ligne de JAC_DR_stval(nnz) dans les mat. pleine A(ndl,ndl)
    !                  DOM%RSL%JAC_DR_stcol, &   ! Col. de JAC_DR_stval(nnz) dans les mat. pleine A(ndl,ndl)
    !                  DOM%RSL%JAC_DR_stval, &   ! Valeurs de JAC_DR_stval(nnz) dans les mat. pleine A(ndl,ndl)
    !                  dQ, &                      ! Resultats
    !                  R, &                       ! Termes de droite
    !                  niter_lin, &               ! Nb d'it. externes
    !                  DOM%NUM%niter_max_int, &  ! Nb d'it. internes
    !                  DOM%NUM%tol_abs, &        ! Tol absolue
    !                  DOM%NUM%tol_rel )         ! Tol relative
    
    
    !--- MAJ DES ETAT -------------------------------------------------!
    ! Nouvelles quantites conservees
    DOM%RSL%Q_DR = DOM%RSL%Q_DR + dQ

    ! Recuperation des quantites conservees : ALPHA ET E
    CALL solve_DR_getQ( DOM )
    
    ! Recalcul des résidus
    R = DOM%RSL%Q_DR - Q_0 - theta_imp*dt*DOM%RSL%F_DR - (1.0_dp-theta_imp)*dt*F_0
    residu = normL2( R(:) )
    
    ! Variations de masse correspondantes au alpha trouvé
    !CALL cyc_reset( DOM )
    !CALL cyc_reac( DOM )
    !CALL cyc_source( DOM )
    DOM%RSL%m_e = m_0 + DOM%NUM%dt * DOM%RSL%dm_e
    
    
    ! Mise a jour des variables naturelles avec m_e et E modifiés
    CALL cyc_update_DR( DOM )
    ! + pression
    IF (DOM%NUM%ADVECTION == 'on') CALL prop_P ( DOM )

    iiter = iiter + 1       ! Incrementation
    
END DO ! End while residu


! Mise a jour des rho_rel pour les avancements des reactions
IF (DOM%NUM%REACTION == 'on') THEN
    CALL update_rho_rel( DOM )
END IF

! Sauvergarde du nb d'it. et du residu final
DOM%RSL%niter_DR = iiter - 1
DOM%RSL%err_it_DR = residu

END SUBROUTINE solve_DR_thetaimp
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de verification de stabilite du calcul explicite (critere Fo)
SUBROUTINE solve_DR_stab( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
REAL(DP) :: crit_stab

! Stabilite explicite pour resolution explicite : 
! crit_stab = 2*Fo < 1
crit_stab = MAXVAL(2.0_dp*ABS(DOM%ETAT%Fo))
IF (crit_stab >= 1.0_dp) THEN
    CALL print_err('Le critere de stabilite explicite n''est plus respecte')
    CALL print_err('2x Fo = '//realtostr(crit_stab)//' > 1')
END IF

END SUBROUTINE solve_DR_stab
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Mise en forme des valeurs de la matrice A du systeme DR au format Sparse Triplet 
SUBROUTINE solve_DR_setST( JAC_DR, id_JAC_DR, JAC_DR_stval )
!
!---- Déclarations ----------------------------------------------------!
REAL(DP), DIMENSION(:,:,:) ::  JAC_DR
INTEGER(IP), DIMENSION(:,:,:) ::  id_JAC_DR
REAL(DP), DIMENSION(:) :: JAC_DR_stval
INTEGER(IP) :: nd1, nd2, nd3
INTEGER(IP) :: id1, id2, id3
INTEGER(IP) :: inz

! Shape de la matrice A du systeme Ax=b dans le format sparse MoDeTheC 
nd1 = SIZE(JAC_DR,1)
nd2 = SIZE(JAC_DR,2)
nd3 = SIZE(JAC_DR,3)

! Pour chaque élément de la matrice sparse MoDeTheC
DO id1=1,nd1 ; DO id2=1,nd2 ; DO id3=1,nd3

    ! On récupère la position (inz) correspondante pour le format Sparse Triplet
    inz = id_JAC_DR(id1,id2,id3)
    
    ! Si la valeur fait partie du système (pas cellule limite)
    IF (inz /= 0) THEN
    
        ! On ecrit dans la matrice sparse format SP
        JAC_DR_stval(inz) = JAC_DR(id1,id2,id3)
    
    END IF
    
! Fin triple boucle
END DO ; END DO ; END DO

END SUBROUTINE solve_DR_setST
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Mise en forme des valeurs du vecteur F(RHS des EDP) 
!> du systeme DR au format Sparse Triplet 
SUBROUTINE solve_DR_setF( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: ireac, nreac
INTEGER(IP) :: icel, ncel
INTEGER(IP) :: neq_DR
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
nreac = DOM%PHYS%nreac
neq_DR = DOM%RSL%neq_DR

! NB : vecteur F_DR( ndl_DR )
! F_AD = | dE (cel1)                -\
!        | dalpha (cel1,reac1)       |
!        | dalpha (cel1,reac2)       |-  cel1
!        | dalpha (cel1,...)         |
!        | dalpha (cel1,nreac)      -/
!        | dE (cel2)                -\
!        | dalpha (cel2,reac1)       |
!        | ....                      |-  cel2

! Pour chaque cellule
DO icel = 1,ncel

    ! Termes de flux ener AD
    DOM%RSL%F_DR( neq_DR*(icel-1) + 1 ) = DOM%RSL%dE(icel)
    
    ! Et chaque reaction
    DO ireac = 1,nreac

        ! Terme de variation de alpha
        DOM%RSL%F_DR( neq_DR*(icel-1) + 1 + ireac ) = DOM%RSL%dalpha(icel,ireac)

    END DO
END DO

END SUBROUTINE solve_DR_setF
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Mise en forme des valeurs du vecteur Q (quantites conservees des EDP) 
!> du systeme DR au format Sparse Triplet 
SUBROUTINE solve_DR_setQ( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: ireac, nreac
INTEGER(IP) :: icel, ncel
INTEGER(IP) :: neq_DR
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
nreac = DOM%PHYS%nreac
neq_DR = DOM%RSL%neq_DR

! NB : vecteur Q_DR( ndl_DR )
! Q_DR = | E (cel1)                -\
!        | alpha (cel1,reac1)       |
!        | alpha (cel1,reac2)       |-  cel1
!        | alpha (cel1,...)         |
!        | alpha (cel1,nreac)      -/
!        | E (cel2)                -\
!        | alpha (cel2,reac1)       |
!        | ....                     |-  cel2

! Pour chaque cellule
DO icel = 1,ncel
    
    ! Quantité ener DR
    DOM%RSL%Q_DR( neq_DR*(icel-1) + 1 ) = DOM%RSL%E(icel)
    
    ! Et chaque reaction
    DO ireac = 1,nreac

        ! Avancement reaction
        DOM%RSL%Q_DR( neq_DR*(icel-1) + 1 + ireac ) = DOM%ETAT%alpha_reac(icel,ireac)

    END DO
END DO

END SUBROUTINE solve_DR_setQ
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Recuperation des valeurs du vecteur Q (quantites conservees des EDP) 
!> du systeme DR au format Sparse Triplet 
!> NB : Fonction reciproque de solve_DR_setQ
SUBROUTINE solve_DR_getQ( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: ireac, nreac
INTEGER(IP) :: icel, ncel
INTEGER(IP) :: neq_DR
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
nreac = DOM%PHYS%nreac
neq_DR = DOM%RSL%neq_DR

! NB : vecteur Q_DR( ndl_DR )
! Q_DR = | E (cel1)                -\
!        | alpha (cel1,reac1)       |
!        | alpha (cel1,reac2)       |-  cel1
!        | alpha (cel1,...)         |
!        | alpha (cel1,nreac)      -/
!        | E (cel2)                -\
!        | alpha (cel2,reac1)       |
!        | ....                     |-  cel2

! Pour chaque cellule
DO icel = 1,ncel
    
    ! Quantité ener DR
    DOM%RSL%E(icel) = DOM%RSL%Q_DR( neq_DR*(icel-1) + 1 )
    
    ! Et chaque reaction
    DO ireac = 1,nreac

        ! Avancement reaction
        DOM%ETAT%alpha_reac(icel,ireac) = DOM%RSL%Q_DR( neq_DR*(icel-1) + 1 + ireac )

    END DO
END DO

END SUBROUTINE solve_DR_getQ
!----------------------------------------------------------------------!

!---- Fin du module ---------------------------------------------------!
END MODULE mod_solve_DR
