! !> Module pour la resolution du cycle des termes d'ablation.
! MODULE mod_solve_AB
! !
! USE mod_algebra, ONLY : cross, norm, dot
! USE mod_cst, ONLY : IP, DP, eps_min
! USE mod_print, ONLY : print_err
! USE mod_ablation, ONLY : cyc_ablation
! USE mod_deform, ONLY : deform_laplacien, deform_spline, deform_maj_spline
! USE mod_mesh_recpt, ONLY : mesh_recpt
! USE mod_dat_bld_cls, ONLY : dat_bld_cls
! !
! USE mod_dom
! !
! IMPLICIT NONE
! CONTAINS
! !
! !----------------------------------------------------------------------!
! !> Routine principale du solveur d'ablation
! SUBROUTINE solve_AB ( DOM )
! !
! !---- Déclarations ----------------------------------------------------!
! TYPE (typ_dom) :: DOM
! INTEGER(IP) :: ilim, nlim, nsom
! REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: SOMMET_TEMP ! Tableau servant a stocker la position des sommets a l'it precedente pour permettre la MAJ des vitesses de sommets en fin d'it
! REAL(DP) :: dt
! !
! !---- Récupérations ---------------------------------------------------!
! nlim = DOM%CDT%nlim
! nsom = DOM%MLG%nsom
! dt = DOM%NUM%dt
! ALLOCATE(SOMMET_TEMP(nsom,3))

! ! Mise a 0 des deplacements de sommets et des donnees de résolution
! DOM%ETAT%D_SOM(:,:) = 0.0_dp
! DOM%RSL%niter_AB = 0
! DOM%RSL%err_it_AB = 0.0_dp

! ! stockage des positions des sommets avant deformation
! SOMMET_TEMP(:,:) = DOM%MLG%SOMMET(:,:)

! ! Boucle sur les limites du domaine
! DO ilim = 1, nlim
!     ! Deplacement de la limite courante  
!         CALL solve_AB_regression_lim (DOM, ilim)   
! END DO

! DO ilim = 1, nlim
!  ! Rerepartition des sommets du maillage sur les limites non ablatees
!     IF ( DOM%CDT%LIM( ilim )%abla_typ == "fixe" ) THEN  
!         CALL deform_spline( DOM, ilim )
!     END IF
! END DO
    
! ! Deformation du maillage interieur
! CALL deform_laplacien ( DOM,DOM%MLG%SOM_ADJ_INT )

! ! MAJ des deplacements de sommet :
! DOM%ETAT%D_SOM = DOM%MLG%SOMMET - SOMMET_TEMP

! ! MAJ des quantités conservatives du systémes (masse, energie) et du volume
! CALL cyc_ablation ( DOM )

! ! MAJ des données de maillages (aires, CG,...)
! CALL mesh_recpt ( DOM )

! ! MAJ des champs de conditions limites speciales (dépendant de VFAC par exemple...)
! CALL dat_bld_cls ( DOM )

! DEALLOCATE (SOMMET_TEMP)

! END SUBROUTINE solve_AB
! !----------------------------------------------------------------------!

! !----------------------------------------------------------------------!
! !> Routine de calcul des déplacements limites D_SOM "brutes"
! SUBROUTINE solve_AB_regression_lim ( DOM,ilim )
! !
! !---- Déclarations ----------------------------------------------------!
! TYPE (typ_dom) :: DOM
! INTEGER(IP) :: ilim, ifac, ifac_lim, nfac_limi, isom, nsom, som1, som2
! INTEGER(IP) :: coin1, coin2, cor1, cor2
! REAL(DP) :: dt, aire
! REAL(DP), DIMENSION(3) :: P1P2, dir
! !
! !---- Récupérations ---------------------------------------------------!
! dt = DOM%NUM%dt
! nsom = DOM%MLG%nsom
! nfac_limi = DOM%CDT%LIM( ilim )%nfac_limi    

! !--- Choix du type de condition d'ablation ----------------------------!
! SELECT CASE ( DOM%CDT%LIM( ilim )%abla_typ )

!     !--- La limite courante est ablatée a vitesse constante suivant le vecteur Vreg_imp défini dans le .prm
!     CASE ( "vitesse" )         
!         ! Boucle sur les faces de la limite i
!         DO ifac = 1, nfac_limi
!             ifac_lim = DOM%CDT%LIM( ilim )%ID_FACLIMi( ifac )
!             !Affectation de la vitesse à chaque sommet de la frontiere.
!             DOM%ETAT%D_SOM( DOM%MLG%FAC( ifac_lim,3 ),1:3 ) = DOM%CDT%LIM( ilim )%Vreg_imp( 1:3 )*dt
!             DOM%ETAT%D_SOM( DOM%MLG%FAC( ifac_lim,4 ),1:3 ) = DOM%CDT%LIM( ilim )%Vreg_imp( 1:3 )*dt    
!         END DO  


!     !--- Cas de déformation sévère pour tester la robustesse de l'algo de déformation de maillage :
!     !--- seule la face centrale de la limite se déplace suivant Vreg_imp
!     CASE ( "monoface" )
!         ifac_lim = DOM%CDT%LIM( ilim )%ID_FACLIMi( nfac_limi / 2 + 1 )
!         !Affectation de la vitesse a la face centrale de la limite
!         DOM%ETAT%D_SOM( DOM%MLG%FAC( ifac_lim,3 ),1:3 ) = DOM%CDT%LIM( ilim )%Vreg_imp( 1:3 )*dt
!         DOM%ETAT%D_SOM( DOM%MLG%FAC( ifac_lim,4 ),1:3 ) = DOM%CDT%LIM( ilim )%Vreg_imp( 1:3 )*dt    


!     !--- Déplacement a vitesse constante avec un profil triangulaire
!     CASE ( "triangle" )         
!         ! Boucle sur les faces de la limite i
!         DO ifac = 1, nfac_limi -1
!             ifac_lim = DOM%CDT%LIM( ilim )%ID_FACLIMi( ifac )
!             !Affectation de la vitesse à chaque sommet de la frontiere.
!             som1 = DOM%MLG%FAC( ifac_lim,3 )
!             som2 = DOM%MLG%FAC( ifac_lim,4 )
            
!             !Affectation de la vitesse a la face centrale de la limite
!             DOM%ETAT%D_SOM( DOM%MLG%FAC( ifac_lim,4 ),: ) = DOM%CDT%LIM( ilim )%Vreg_imp( 1:3 )* dt * min( norm ( DOM%MLG%SOMMET( DOM%CDT%LIM( ilim )%SOM_COINi( 1 ),: ) - DOM%MLG%SOMMET( som2,: ) ) , norm ( DOM%MLG%SOMMET( DOM%CDT%LIM( ilim )%SOM_COINi( 2 ),: ) - DOM%MLG%SOMMET( som2,: ) ) ) / norm ( DOM%MLG%SOMMET( DOM%CDT%LIM( ilim )%SOM_COINi( 1 ),: ) - DOM%MLG%SOMMET( DOM%CDT%LIM( ilim )%SOM_COINi( 2 ),: ) )
!         END DO   


!     !--- Déplacement à vitesse constante avec un profil quadratique
!     CASE ( "quadratique" )         
!         ! Boucle sur les faces de la limite i
!         DO ifac = 2, nfac_limi
!             ifac_lim = DOM%CDT%LIM( ilim )%ID_FACLIMi( ifac )
!             !Affectation de la vitesse à chaque sommet de la frontiere.
!             som1 = DOM%MLG%FAC( ifac_lim,3 )
!             som2 = DOM%MLG%FAC( ifac_lim,4 )
            
!             !Affectation de la vitesse a la face centrale de la limite
!             DOM%ETAT%D_SOM( DOM%MLG%FAC( ifac_lim,3 ),: ) = DOM%CDT%LIM( ilim )%Vreg_imp( 1:3 )* dt * sqrt( min( norm ( DOM%MLG%SOMMET( DOM%CDT%LIM( ilim )%SOM_COINi( 1 ),: ) - DOM%MLG%SOMMET( som1,: ) ) , norm ( DOM%MLG%SOMMET( DOM%CDT%LIM( ilim )%SOM_COINi( 2 ),: ) - DOM%MLG%SOMMET( som2,: ) ) ) / norm ( DOM%MLG%SOMMET( DOM%CDT%LIM( ilim )%SOM_COINi( 1 ),: ) - DOM%MLG%SOMMET( DOM%CDT%LIM( ilim )%SOM_COINi( 2 ),: ) ) )
!         END DO  

     
!     !--- Cas des déplacements physiques : par ablation à température de paroi fixée
!     !--- ou avec vitesse d'ablation dépendant de la température.   
!     CASE ( "temp","meca" )
!         ! RAZ des variables utilisées pour la correction de vitesse d'ablation des sommets coins.
!         coin1 = 0
!         coin2 = 0
!         cor1 = 0
!         cor2 = 0
        
!         ! Boucle sur les faces de la limite i
!         DO ifac = 1, nfac_limi
!             ifac_lim = DOM%CDT%LIM( ilim )%ID_FACLIMi( ifac )
!             !Affectation des sommets de la face limite courante
!             som1 = DOM%MLG%FAC( ifac_lim,3 )
!             som2 = DOM%MLG%FAC( ifac_lim,4 )
!             ! Affectations des vecteurs liés à la face courante
!             ! vecteur face
!             P1P2 = DOM%MLG%SOMMET(som2,:) - DOM%MLG%SOMMET(som1,:)
!             ! vecteur unitaire de direction G1H
!             !  dir = DOM%MLG%G1H( ifac_lim,: ) / sqrt( DOM%MLG%n2_G1H( ifac_lim ) )
!             dir = DOM%MLG%VFAC( ifac_lim,:) 
!             ! Aire équivalente d'abaltion
!             aire = DOM%MLG%AIRE( ifac_lim )
!             ! aire = norm( cross( P1P2,dir ) )  
            
!             !Affectation du déplacement au sommet 1 :
!             ! ... sommet coin : le déplacement vaut le taux de régression de la seule face adjacente de la limite
!             IF ( count ( DOM%CDT%LIM( ilim )%SOM_COINi( : ) == som1 ) > 0.5_dp ) THEN
!                 ! Affectation des indices de sommets coin pour la correction de vitesse d'ablation du sommet coin
!                 IF (coin1 == 0) THEN
!                     coin1 = som1
!                     cor1 = som2
!                 ELSE
!                     coin2 = som1
!                     cor2 = som2
!                 END IF
!                 DOM%ETAT%D_SOM( som1,: ) = DOM%ETAT%D_SOM( som1,: ) - DOM%ETAT%taux_reg( ifac_lim ) / aire * dir
            
!             ! ... sommet interne, on prend la moyenne des taux de régression des deux faces limites adjacentes.
!             ELSE
!                 DOM%ETAT%D_SOM( som1,: ) = DOM%ETAT%D_SOM( som1,: ) - 0.5_dp * DOM%ETAT%taux_reg( ifac_lim ) / aire * dir                
!             END IF
            
!              !Affectation du déplacement au sommet 2 :
!              ! ... sommet coin : le déplacement vaut le taux de régression de la seule face adjacente de la limite
!             IF ( count ( DOM%CDT%LIM( ilim )%SOM_COINi( : ) == som2 ) > 0.5_dp ) THEN
!                 DOM%ETAT%D_SOM( som2,: ) = DOM%ETAT%D_SOM( som2,: ) - DOM%ETAT%taux_reg( ifac_lim ) / aire * dir                
!                 ! Affectation des indices de sommets coin pour la correction finale 
!                 IF (coin1 == 0) THEN
!                     coin1 = som2
!                     cor1 = som1
!                 ELSE
!                     coin2 = som2
!                     cor2 = som1
!                 END IF
            
!             ! ... sommet interne, on prend la moyenne des taux de régression des deux faces limites adjacentes.
!             ELSE
!                 DOM%ETAT%D_SOM( som2,: ) = DOM%ETAT%D_SOM( som2,: ) - 0.5_dp * DOM%ETAT%taux_reg( ifac_lim ) / aire * dir
!             END IF
!         END DO        
        
!         ! Correction de la vitesse d'ablation du sommet coin : on ajoute un correctif pour conserver la variation de vitesse d'ablation au sommet coin.
!         ! Correction utile dans le cas du trapèze mais parfois instable.  
! !~         IF (DOM%CDT%LIM( ilim )%SOM_COINi(1) /=0 ) THEN
! !~             D1 = SUM( DOM%ETAT%D_SOM( coin1,: )**2,1 )
! !~             D2 = SUM( DOM%ETAT%D_SOM( coin2,: )**2,1 )
! !~         END IF
! !~         ! Correction linéaire du déplacement du coin 1
! !~         IF ( ( coin1 /=0 ) .AND. ( D1 > eps_min ) ) THEN
! !~             ! Projection du delta de correction            
! !~             delta = dot (DOM%ETAT%D_SOM( cor1,: ) - DOM%ETAT%D_SOM( coin1,: ) , DOM%ETAT%D_SOM( coin1,: ) ) /  D1
! !~             DOM%ETAT%D_SOM( coin1,: ) = max(0.0_dp, 1.0_dp - delta ) * DOM%ETAT%D_SOM( coin1,: )
! !~         END IF 
! !~         ! Correction linéaire du déplacement du coin 2
! !~         IF ( ( coin2 /=0 ) .AND. ( D2 > eps_min ) ) THEN
! !~             ! Projection du delta de correction
! !~             delta = dot (DOM%ETAT%D_SOM( cor2,: ) - DOM%ETAT%D_SOM( coin2,: ) , DOM%ETAT%D_SOM( coin2,: ) ) /  D2
! !~             DOM%ETAT%D_SOM( coin2,: ) = max(0.0_dp, 1.0_dp - delta ) * DOM%ETAT%D_SOM( coin2  ,: )
! !~         END IF


!     CASE DEFAULT ! Enregistrement des limites non ablatee
!         DOM%CDT%LIM( ilim )%abla_typ = "fixe"        
    
! END SELECT


! IF (DOM%CDT%LIM( ilim )%abla_typ /= 'fixe') THEN

!     ! Correction de vitesses des sommets communs avec une autre limite :
!     ! la trajectoire du sommet doit rester confondue avec la limite comune.
!     ! (pas de déformation de la limite non ablatée).
!     CALL solve_AB_correction_vitesse ( DOM,ilim )

!     ! Deplacement des sommets frontieres
!     DO isom = 1, nsom
!         DOM%MLG%SOMMET( isom,1:3 ) = DOM%MLG%SOMMET( isom,1:3 ) + DOM%ETAT%D_SOM( isom,1:3 )
!     END DO

!     ! RAZ du tableau des déplacements
!     DOM%ETAT%D_SOM = 0.0_dp 

!     ! MAJ des interpolations par splines suite a la deformation de la limite
!     CALL deform_MAJ_spline( DOM, ilim )
!     ! Rerepartition des sommets sur la limite
!     CALL deform_spline( DOM, ilim )

! END IF
! !
! !
! END SUBROUTINE solve_AB_regression_lim
! !----------------------------------------------------------------------!

! !----------------------------------------------------------------------!
! !> Routine de correction de D_SOM et de connectivité PP2PSi pour les forcer à 
! !> être contenues dans les limites adjacentes
! SUBROUTINE solve_AB_correction_vitesse ( DOM, ilim_abla )
! !
! !---- Déclarations ----------------------------------------------------!
! TYPE (typ_dom) :: DOM
! INTEGER(IP) :: ilim, nlim, ifac_lim, nfac_limi, id_fac_lim, ncol, nps
! INTEGER(IP) :: i, isom_adj, id_som, id_pp, idps, idps2
! INTEGER(IP) :: ilim_abla, id_som1, id_som2, id_som3
! REAL(DP) :: alpha
! REAL(DP), DIMENSION(3) :: d_som1, d_som2, w, ptcor, PS1, PS2, proj, z, h
! LOGICAL :: decale 
! !
! !---- Récupérations ---------------------------------------------------!
! nlim = DOM%CDT%nlim
! ncol = size( DOM%MLG%SOM_ADJ( 1,: ) )

! ! Pour chaque limite ablatée, on corrige la position et la connectivité par rapport
! ! aux sommets secondaires des deux sommets coins quand il existent (limites ouvertes)
! ! pour forcer leur déplacement respectifs sur les limites adjacentes.
! ! Boucle sur les limites
! DO ilim = 1, nlim
!     nps = size ( DOM%CDT%LIM( ilim )%PSi( :,1 ) )
!     nfac_limi = DOM%CDT%LIM( ilim )%nfac_limi     
!     IF ( ilim /= ilim_abla ) THEN
        
!         ! Boucle sur les faces de la limite courante
!         DO ifac_lim =1, nfac_limi

!             id_fac_lim = DOM%CDT%LIM( ilim )%ID_FACLIMi( ifac_lim )
!             id_som1 = DOM%MLG%FAC( id_fac_lim,3 )
!             id_som2 = DOM%MLG%FAC( id_fac_lim,4 )
!             d_som1 = DOM%ETAT%D_SOM( id_som1,: )
!             d_som2 = DOM%ETAT%D_SOM( id_som2,: )          

!             ! Recherche d'un sommet commun a 2 limites ( sommet "coin" ) ayant une vitesse d'ablation non nulle
!             IF( ( count ( DOM%CDT%LIM( ilim )%SOM_COINi( : ) == id_som1 ) > 0.5_dp ) .AND. ( norm( d_som1 ) > 10.0_dp*eps_min ) ) THEN                 
                
!                 ! Recherche du sommet adjacent de la face ablatee
!                 id_som3 = 0
!                 DO isom_adj = 1, ncol-2  
!                     id_som = DOM%MLG%SOM_ADJ( id_som1,isom_adj+2 )                   
!                     IF( id_som /= 0 ) THEN  
!                         IF( maxval( ABS( DOM%ETAT%D_SOM( id_som,: ) ) ) > 100.0_dp*eps_min ) THEN
!                             id_som3 = id_som
!                         END IF                    
!                     END IF
!                 END DO     

!                 ! Controle de l'existence d'un sommet adjacent ablate
!                 IF( id_som3 /= 0 ) THEN                    
                    
!                     ! vecteur representant la face ablatee adjacente
!                     w = DOM%MLG%SOMMET( id_som3,: ) - DOM%MLG%SOMMET( id_som1,: ) 
!                     ! position du point non corrige
!                     ptcor = DOM%MLG%SOMMET( id_som1,: ) + d_som1(:)                   
                    
!                     ! Recherche de l'indice de id_som1 dans le tableau PP2PSi qui donne la
!                     ! connectivité entre ppoints principaux et points secondaires.
!                     id_pp = 0
!                     DO i = 1, nfac_limi+1
!                         IF ( DOM%CDT%LIM( ilim )%PP2PSi(i,1) == id_som1 ) THEN
!                             id_pp = i
!                             EXIT
!                         END IF 
!                     END DO                    
                    
!                     ! Contrôle de la determination de l'indice de id_som1 dans PP2PSi
!                     IF (i > nfac_limi + 2 ) THEN
!                         CALL print_err("ablation err1 (mod_solve_AB) : erreur survenue dans la correction de vitesse d'un des coin du domaine ablate",1)
!                     END IF
                   
!                     ! Recuperations des deux points secondaires adjacents au point principal avant son deplacement
!                     idps = DOM%CDT%LIM( ilim )%PP2PSi( id_pp,2 )
!                     idps2 = DOM%CDT%LIM( ilim )%PP2PSi( id_pp,3 )
!                     ! initialisation du booleen qui est vrai lorsqu'il faut déplacer le sommet coin, faux sinon                       
!                     decale = .TRUE.  
!                     z = cross( w,d_som1 )                    
!                     IF ( norm( z ) < 10.0_dp*eps_min ) THEN
!                         decale = .FALSE. 
!                     END IF

!                     ! le vecteur h dirige l'axe sur lequel on mesure la distance des points par rapport au front d'ablation 
!                     h = cross( z,w ) / norm ( z )
!                     DO WHILE (decale)        

!                         PS1 = DOM%CDT%LIM( ilim )%PSi( idps,: )
!                         PS2 = DOM%CDT%LIM( ilim )%PSi( idps2,: )
!                         ! le point corrige est placé sur le segment entre PS1 et PS2, point corrige = PS1 + alpha * PS2
!                         ! le calcul d'alpha s'effectue en projetant sur l'axe dirigé par h les vecteurs ptnoncor - PS1 et PS2 - PS1
!                         alpha =  dot( ptcor - PS1 , h ) / dot( PS2 - PS1 , h )

!                         ! Si le coefficient est négatif, la projection sur le segment PS1PS2
!                         ! du point non corrige est situé avant PS1 : on décale la connectivé
!                         IF (alpha < eps_min) THEN
!                             IF (idps > 1) THEN
!                                 idps2 = idps
!                                 idps = idps - 1
!                                 ! MAJ de PP2PSi
!                                 DOM%CDT%LIM( ilim )%PP2PSi( id_pp,2 ) = min( idps, idps2 )
!                                 DOM%CDT%LIM( ilim )%PP2PSi( id_pp,3 ) = max( idps, idps2 )
!                             ELSE
!                                 write(*,*), id_som1
!                                 CALL print_err( "ablation err2 (mod_solve_AB): l'un des sommets coin a ete deplace vers l'extérieur du domaine : deplacement non physique",1 )
!                             END IF

!                         ! Si le coefficient est supérieur à 1, la projection sur le segment PS1PS2
!                         ! du point non corrige est situé après PS2 : on décale la connectivé
!                         ELSE IF (alpha > 1.0_dp+100.0_dp*eps_min) THEN
!                             IF (idps2 < nps) THEN
!                                 idps = idps2
!                                 idps2 = idps2+1
!                                 ! MAJ de PP2PSi
!                                 DOM%CDT%LIM( ilim )%PP2PSi( id_pp,2 ) = min( idps, idps2 )
!                                 DOM%CDT%LIM( ilim )%PP2PSi( id_pp,3 ) = max( idps, idps2 )
!                             ELSE
!                                 write(*,*), id_som1
!                                 CALL print_err( "ablation err3 (mod_solve_AB): l'un des sommets coin a ete deplace vers l'extérieur du domaine : deplacement non physique",1 )
!                             END IF

!                         ! Sinon le coeffcient est entre 0 et 1 : la projection sur le segment PS1PS2
!                         ! du point non corrige : la projection est corecte on l'enregistre
!                         ELSE
!                             ! Projection du point non corrige sur le segment reliant les deux points secondaires adequats
!                             proj = PS1 +alpha * (PS2 - PS1)  
!                             ! MAJ de la valeur du deplacement du sommet coin
!                             DOM%ETAT%D_SOM( id_som1,: ) = proj - DOM%MLG%SOMMET( id_som1,: )
!                             ! l'agorithme est termine on met le booleen "decale" a false
!                             decale = .FALSE.

!                         END IF ! End alpha < eps_min

!                     END DO ! End decale == .TRUE.

!                 END IF 
                
!             ! meme algorithme traduit pour le sommet 2
!             ELSE IF( ( count( DOM%CDT%LIM( ilim )%SOM_COINi( : ) == id_som2 ) > 0.5_dp ) .AND.( norm( d_som2 ) > 10.0_dp*eps_min )) THEN
                
!                 id_som3 = 0
!                 DO isom_adj = 1, ncol-2  
!                     id_som = DOM%MLG%SOM_ADJ( id_som2,isom_adj+2 )                   
!                     IF( id_som /= 0 ) THEN  
!                         IF( maxval( ABS( DOM%ETAT%D_SOM( id_som,: ) ) ) > 100.0_dp*eps_min ) THEN
!                             id_som3 = id_som
!                         END IF                    
!                     END IF
!                 END DO                                           
                
!                 IF( id_som3 /= 0 ) THEN                    
!                     w = DOM%MLG%SOMMET( id_som3,: ) - DOM%MLG%SOMMET( id_som2,: )
!                     ptcor = DOM%MLG%SOMMET( id_som2,: ) + d_som2(:)                   
!                     id_pp = 0
                    
!                     DO i = 1, nfac_limi+1
!                         IF ( DOM%CDT%LIM( ilim )%PP2PSi(i,1) == id_som2 ) THEN
!                             id_pp = i
!                             EXIT
!                         END IF 
!                     END DO                    
                    
!                     IF (i > nfac_limi + 2 ) THEN
!                         CALL print_err("ablation err4 (mod_solve_AB) : erreur survenue dans la correction de vitesse d'un des coin du domaine ablate",1)
!                     END IF
                    
!                     idps = DOM%CDT%LIM( ilim )%PP2PSi( id_pp,2 )
!                     idps2 = DOM%CDT%LIM( ilim )%PP2PSi( id_pp,3 )                             
!                     decale = .TRUE.  
!                     z = cross( w,d_som2 )                    
                    
!                     IF ( norm( z ) < 10.0_dp*eps_min ) THEN
!                         decale = .FALSE. 
!                     END IF
                    
!                     h = cross( z,w ) / norm ( z )
                    
!                     DO WHILE (decale)     
                        
!                         PS1 = DOM%CDT%LIM( ilim )%PSi( idps,: )
!                         PS2 = DOM%CDT%LIM( ilim )%PSi( idps2,: )
!                         alpha =  dot( ptcor - PS1 , h ) / dot( PS2 - PS1 , h )                        
                        
!                         IF (alpha < eps_min) THEN
!                             IF (idps > 1) THEN
!                                 idps2 = idps
!                                 idps = idps - 1
!                                 DOM%CDT%LIM( ilim )%PP2PSi( id_pp,2 ) = min( idps, idps2 )
!                                 DOM%CDT%LIM( ilim )%PP2PSi( id_pp,3 ) = max( idps, idps2 )
!                             ELSE
!                                 write(*,*), id_som2
!                                 CALL print_err( "ablation err5 (mod_solve_AB): l'un des sommets coin a ete deplace vers l'extérieur du domaine ; deplacement non physique",1 )
!                             END IF
                        
!                         ELSE IF (alpha > 1.0_dp) THEN
!                             IF (idps2 < nps) THEN
!                                 idps = idps2
!                                 idps2 = idps2+1
!                                 DOM%CDT%LIM( ilim )%PP2PSi( id_pp,2 ) = min( idps, idps2 )
!                                 DOM%CDT%LIM( ilim )%PP2PSi( id_pp,3 ) = max( idps, idps2 )
!                             ELSE
!                                 write(*,*), id_som2
!                                CALL print_err( "ablation err6 (mod_solve_AB): l'un des sommets coin a ete deplace vers l'extérieur du domaine ; deplacement non physique",1 )           
!                             END IF
                        
!                         ELSE
!                             proj = PS1 +alpha * (PS2 - PS1)  
!                             DOM%ETAT%D_SOM( id_som2,: ) = proj - DOM%MLG%SOMMET( id_som2,: )
!                             decale = .FALSE.
                        
!                         END IF
                    
!                     END DO ! End decale
                
!                 END IF ! End id_som3 /= 0
            
!             END IF
        
!         END DO ! Fin ifac_limi
    
!     END IF
! END DO ! Fin ilim
! !
! !
! END SUBROUTINE solve_AB_correction_vitesse
! !----------------------------------------------------------------------!
! !
! !---- Fin du module ---------------------------------------------------!
! END MODULE mod_solve_AB
