!> Module pour resolution du cycle des termes ADVECTIF
MODULE mod_solve_A
!
USE mod_cst, ONLY : DP, IP, eps_min
USE mod_print, ONLY : print_err
USE mod_algebra, ONLY : normL2, realtostr
USE mod_prop, ONLY : cyc_prop_A, prop_P, prop_vg
USE mod_reset, ONLY : cyc_reset
USE mod_advection, ONLY : cyc_advection
USE mod_update, ONLY : cyc_update_A
USE mod_jac_A, ONLY : jac_A
USE mod_gmres, ONLY : mgmres_st
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Choix de la methode de resolution du systeme A
SUBROUTINE solve_A( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
CHARACTER (len=32) :: schema_A
REAL(DP) :: dt_ref, dt_tot
INTEGER(IP) :: issiter
REAL(DP), DIMENSION(DOM%MLG%nfac) :: DT_loc
! Evaluation des nouvelles proprietes avec variables modifiees
CALL cyc_prop_A( DOM )

! Methode choisie par l'utilisateur
schema_A = DOM%NUM%schema_A

! Choix de la methode d'intégration ADVECTION
SELECT CASE (schema_A)

    ! Cas Euler explicite
    CASE ("euler-exp")

        dt_ref = DOM%NUM%dt
        dt_tot = 0.0_dp
        issiter = 0
        
        DO WHILE ( ABS(dt_ref-dt_tot) > 2.0_dp*eps_min ) 
        
            ! DT_loc aux faces
            DT_loc = SQRT( DOM%MLG%n2_G1G2 ) * DOM%NUM%CFL_max * DOM%ETAT%phi_g_itp
            DT_loc = DT_loc / ( 2.0_dp*eps_min + SQRT( DOM%ETAT%v_g_itp(:,1)**2 + DOM%ETAT%v_g_itp(:,2)**2 + DOM%ETAT%v_g_itp(:,3)**2 ) )
                     
            DOM%NUM%dt = MINVAL( ABS(DT_loc) )
            DOM%NUM%dt = MIN( DOM%NUM%dt, ABS(dt_ref-dt_tot) )
        
            !print*, issiter,dt_tot, MAXVAL(ABS(DOM%ETAT%CFL))
            CALL solve_A_eulerexp( DOM )
            
            dt_tot = dt_tot + DOM%NUM%dt
            issiter = issiter + 1

        END DO

        DOM%NUM%dt = dt_ref
        DOM%RSL%niter_A = issiter
        
        
    ! Cas schema theta implicte (Euler generalise)
    CASE ("theta-imp")
        CALL solve_A_thetaimp( DOM )
        
        
    ! Cas schema theta linearise (Euler avec une seule it de Newton)
    CASE ("theta-lin")
        dt_ref = DOM%NUM%dt
        dt_tot = 0.0_dp
        issiter = 0

        DO WHILE ( ABS(dt_ref-dt_tot) > 2.0_dp*eps_min ) 

            ! DT_loc aux faces
            DT_loc = SQRT( DOM%MLG%n2_G1G2 ) * DOM%NUM%CFL_max * DOM%ETAT%phi_g_itp
            DT_loc = DT_loc / ( 2.0_dp*eps_min + SQRT( DOM%ETAT%v_g_itp(:,1)**2 + DOM%ETAT%v_g_itp(:,2)**2 + DOM%ETAT%v_g_itp(:,3)**2 ) )

            DOM%NUM%dt = MINVAL( ABS(DT_loc) )
            DOM%NUM%dt = MIN( DOM%NUM%dt, ABS(dt_ref-dt_tot) )
            
            !print*, issiter,dt_tot, MAXVAL(ABS(DOM%ETAT%CFL))
            ! CALL solve_A_thetalin( DOM )
            CALL solve_A_thetaimp( DOM )
            
            dt_tot = dt_tot + DOM%NUM%dt
            issiter = issiter + 1
             
        END DO

        DOM%NUM%dt = dt_ref
        DOM%RSL%niter_A = issiter

        
    ! Sinon erreur
    CASE DEFAULT
        CALL print_err('La methode d''integration schema_A n''est pas reconnue',1)
        
END SELECT

END SUBROUTINE solve_A
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de resolution A euler explicite
SUBROUTINE solve_A_eulerexp( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM

!----------------------------------------------------------------------!
! 1/ on determine Q* solution avec les termes advectifs et diffusifs seuls
!     (Q* - Q(n)) / dt = ADVECTION

! Remise a zero des termes de flux
CALL cyc_reset( DOM )

! Evaluation des nouvelles proprietes avec variables modifiees
CALL cyc_prop_A( DOM )

! Evaluation des seconds membres des equations de conservation : dE et dm_e
! Puis termes advectifs
CALL cyc_advection( DOM )

! Verification de la stabilite du calcul
CALL solve_A_stab( DOM )

! Integration Euler explicite : dQ/dt = F(Q)
! => Q(n+1) = Q(n) + dt * F(Q(n))
DOM%RSL%E = DOM%RSL%E + DOM%NUM%dt * DOM%RSL%dE ! pour le bilan ener
DOM%RSL%m_e = DOM%RSL%m_e + DOM%NUM%dt * DOM%RSL%dm_e ! et les bilans massiques

! Mise a jour des variables naturelles avec m_e et E modifiés
CALL cyc_update_A( DOM )
! + pression
CALL prop_P( DOM )
! + Vitesse gaz
CALL prop_vg( DOM )

END SUBROUTINE solve_A_eulerexp


!----------------------------------------------------------------------!
!> Routine de resolution A theta implicite
SUBROUTINE solve_A_thetaimp( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
REAL(DP), DIMENSION(DOM%RSL%ndl_A) :: Q_0, F_0, R, dQ
REAL(DP), DIMENSION(DOM%MLG%ncel) :: E_0
REAL(DP) :: theta_imp, dt, residu, eps_conv, tol_absolue
INTEGER(IP) :: iiter
!
! Parametres PARDISO
INTEGER(IP) :: maxfct = 1
INTEGER(IP) :: mnum = 1
INTEGER(IP) :: phase
INTEGER(IP) :: nrhs = 1
INTEGER(IP) :: msglvl = 0
INTEGER(IP) :: error
!
!---- Récupérations ---------------------------------------------------!
theta_imp = DOM%NUM%theta_imp
dt = DOM%NUM%dt

!----------------------------------------------------------------------!
! 1/ on determine Q(n+1) solution avec les termes advectifs seuls
!     (Q(n+1) - Q*) / dt = ADVECTIFS

!----------------------------------------------------------------------!
!--- 1ERE EVAL DES RESIDUS

!--- PREPARATION ------------------------------------------------------!

! Remise a zero des termes de flux
dQ(:) = 0.0_dp
CALL cyc_reset( DOM )

! Quantites conservees a l'etat precedent sauvé dans Q_0
CALL solve_A_setQ( DOM )
Q_0(:) = DOM%RSL%Q_A(:)
! Et sauvegarde des energies a l'etat precedent
E_0 = DOM%RSL%E

! Evaluation des nouvelles proprietes avec variables modifiees
CALL cyc_prop_A( DOM )


!--- SECONDS MEMBRES DES EQUATIONS DE CONSERVATION --------------------!
! Termes advectifs : dE et dm_e (gaz)
CALL cyc_advection( DOM )

! RHS des EDP a l'etat precedent sauve dans F_0
CALL solve_A_setF( DOM )
F_0(:) = DOM%RSL%F_A(:)

! Evaluation du residu initial
R(:) = DOM%RSL%Q_A(:) - Q_0(:) - (theta_imp*dt)*DOM%RSL%F_A(:) - ((1.0_dp-theta_imp)*dt)*F_0(:)
residu = normL2( R(:) )

! Critere d'arret
tol_absolue = 1.0e-09_dp
eps_conv = (residu * DOM%NUM%tol_rel) + tol_absolue
    

!----------------------------------------------------------------------!
!--- METHODE ITERATIVE DE NEWTON-RAPHSON DU SYSTEME ADVECTION ---------!
iiter = 1
DO WHILE ( ((residu>=eps_conv) .AND. (iiter<=DOM%NUM%niter_max)) .OR. (iiter==1)  )
    
    !--- METHODE PISO -------------------------------------------------!
    ! Resolution du champ de pression et des vitesse de gaz correspondantes
    ! A l'equation de conservation de masse gazeuse applique au flux de darcy
    !CALL solve_P_thetaimp( DOM )
    
    ! Remise a zero des termes de flux et de jacobiennes
    dQ(:) = 0.0_dp
    CALL cyc_reset( DOM )
    
    !--- EVALUATION DES FLUX ------------------------------------------!
    !--- ET RHS DU SYSTEME --------------------------------------------!
    
    ! Evaluation des nouvelles proprietes avec variables modifiees
    CALL cyc_prop_A( DOM )
    
    
    !------------------------------------------------------------------!
    ! Termes advectifs : dE et dm_e (gaz)
    CALL cyc_advection( DOM )

    ! RHS des EDP au format ST
    CALL solve_A_setF( DOM )
    
    ! Terme de droite du systeme lineaire (Id - dt*theta*dFdQ) dQ = -R
    R = DOM%RSL%Q_A - Q_0 - theta_imp*dt*DOM%RSL%F_A - (1.0_dp-theta_imp)*dt*F_0
    R = -R
    
    !--- MATRICE JACOBIENNE -------------------------------------------!
    ! Evaluation et inversion seulement si niter_max_int est depasse DOM%NUM%niter_max_int DOM%NUM%
    IF (MODULO(iiter, DOM%NUM%niter_max_int) == 1) THEN

        ! Evaluation de la matrice jacobienne de la fonction F( Q*, Q(n) )
        CALL jac_A( DOM )
        
        ! Evaluation de la matrice jacobienne de la fonction R = (Q* - Q(n)) - dt*F( Q*, Q(n) )
        ! dR/dQ* = Id - theta*dt*dF/dQ
        DOM%RSL%JAC_A(:,:,:) = - (theta_imp*dt) * DOM%RSL%JAC_A(:,:,:)
        DOM%RSL%JAC_A(:,1,:) = 1.0_dp + DOM%RSL%JAC_A(:,1,:)
        
        ! Mise ne forme de la matrice jacobnienne dRdQ* en forme ST (sparse triplet)
        CALL solve_A_setST( DOM%RSL%JAC_A, DOM%RSL%id_JAC_A, DOM%RSL%JAC_A_stval )
        
        !  DO i = 1,DOM%RSL%nnz_A
        !      print *, DOM%RSL%JAC_A_csrow(i), DOM%RSL%JAC_A_stcol(i), DOM%RSL%JAC_A_stval(i)
        !  END DO

        !--- Phase analyse+factorisation de la matrice DR avec solveur PARDISO
        phase = 12
        CALL PARDISO (DOM%RSL%PT, &            ! Adresses pointeurs internes PARDISO
                    maxfct, &                   ! Nombre de systemes
                    mnum, &                     ! Indice du systeme <= maxfct
                    DOM%RSL%MTYPE, &           ! Type de matrice = 11 (reel non symetrique)
                    phase, &                    ! Phase de resolution
                    DOM%RSL%ndl_A, &          ! Nombre d'equations
                    DOM%RSL%JAC_A_stval, &    ! Vecteur reel de la matrice format CSR3
                    DOM%RSL%JAC_A_csrow, &    ! Vecteur indice ligne format CSR3
                    DOM%RSL%JAC_A_stcol, &    ! Vecteur indice colonne format CSR3
                    DOM%RSL%PERM, &            ! Indice des permutations (non-utilise)
                    nrhs, &                     ! Nombre de RHS
                    DOM%RSL%IPARM, &           ! Tableau des parametres (defini dans don_csti_rsl.f90)
                    msglvl, &                   ! Mode verbose
                    R, &                        ! Terme de droite
                    dQ, &                       ! Solution
                    error)                      ! Code erreur

    END IF

    !--- Phase resolution systeme avec PARDISO
    phase = 33
    CALL PARDISO (DOM%RSL%PT, &            ! Adresses pointeurs internes PARDISO
                maxfct, &                   ! Nombre de systemes
                mnum, &                     ! Indice du systeme <= maxfct
                DOM%RSL%MTYPE, &           ! Type de matrice = 11 (reel non symetrique)
                phase, &                    ! Phase de resolution
                DOM%RSL%ndl_A, &          ! Nombre d'equations
                DOM%RSL%JAC_A_stval, &    ! Vecteur reel de la matrice format CSR3
                DOM%RSL%JAC_A_csrow, &    ! Vecteur indice ligne format CSR3
                DOM%RSL%JAC_A_stcol, &    ! Vecteur indice colonne format CSR3
                DOM%RSL%PERM, &            ! Indice des permutations (non-utilise)
                nrhs, &                     ! Nombre de RHS
                DOM%RSL%IPARM, &           ! Tableau des parametres (defini dans don_csti_rsl.f90)
                msglvl, &                   ! Mode verbose
                R, &                        ! Terme de droite
                dQ, &                       ! Solution
                error)                      ! Code erreur

    
    !--- APPEL AU SOLVEUR GMRES ---------------------------------------!
    ! CALL mgmres_st ( DOM%RSL%ndl_A, &         ! Nb de degres de libertes du systeme
    !                  DOM%RSL%nnz_A, &         ! Nombre de non-zeros dans la matrice sparse JAC_A_stval
    !                  DOM%RSL%JAC_A_strow, &   ! Ligne de JAC_A_stval(nnz) dans les mat. pleine A(ndl,ndl)
    !                  DOM%RSL%JAC_A_stcol, &   ! Col. de JAC_A_stval(nnz) dans les mat. pleine A(ndl,ndl)
    !                  DOM%RSL%JAC_A_stval, &   ! Valeurs de JAC_A_stval(nnz) dans les mat. pleine A(ndl,ndl)
    !                  dQ, &                      ! Resultats
    !                  R, &                       ! Termes de droite
    !                  niter_lin, &               ! Nb d'it. externes
    !                  DOM%NUM%niter_max_int, &  ! Nb d'it. internes
    !                  DOM%NUM%tol_abs, &        ! Tol absolue
    !                  DOM%NUM%tol_rel )         ! Tol relative
    
    !--- MAJ DES ETAT -------------------------------------------------!
    ! Nouvelles quantites conservees
    DOM%RSL%Q_A = DOM%RSL%Q_A + dQ

    ! Recalcul des résidus
    R = DOM%RSL%Q_A - Q_0 - theta_imp*dt*DOM%RSL%F_A - (1.0_dp-theta_imp)*dt*F_0
    DOM%RSL%residu_A = R !!!TEST!!!
    residu = normL2( R(:) )

    ! Recuperation des quantites conservees : m_e (gaz)
    CALL solve_A_getQ( DOM )
    
    ! Variations d'energie correspondantes au dm_e (gaz) trouvé
    DOM%RSL%E = E_0 + dt * DOM%RSL%dE
    
    ! Mise a jour des variables naturelles avec m_e et E modifiés
    CALL cyc_update_A( DOM )
    ! + pression
    CALL prop_P( DOM )
    ! + Vitesse gaz
    CALL prop_vg( DOM )


    iiter = iiter + 1       ! Incrementation
    
END DO

! Sauvergarde du nb d'it. et du residu final
DOM%RSL%niter_A = iiter - 1
DOM%RSL%err_it_A = residu

END SUBROUTINE solve_A_thetaimp
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de resolution A theta implicite linearise
SUBROUTINE solve_A_thetalin( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
REAL(DP), DIMENSION(DOM%RSL%ndl_A) :: Q_0, F_0, R, dQ
REAL(DP), DIMENSION(DOM%MLG%ncel) :: E_0
REAL(DP) :: theta_imp, dt, residu
INTEGER(IP) :: niter_lin
!
! Parametres PARDISO
INTEGER(IP) :: maxfct = 1
INTEGER(IP) :: mnum = 1
INTEGER(IP) :: phase
INTEGER(IP) :: nrhs = 1
INTEGER(IP) :: msglvl = 0
INTEGER(IP) :: error
!
!---- Récupérations ---------------------------------------------------!
theta_imp = DOM%NUM%theta_imp
dt = DOM%NUM%dt

!----------------------------------------------------------------------!
! 1/ on determine Q(n+1) solution avec les termes advectifs seuls
!     (Q(n+1) - Q*) / dt = ADVECTIFS

!----------------------------------------------------------------------!
!--- 1ERE EVAL DES RESIDUS

!--- PREPARATION ------------------------------------------------------!

! Remise a zero des termes de flux
dQ(:) = 0.0_dp
CALL cyc_reset( DOM )

! Quantites conservees a l'etat precedent sauvé dans Q_0
CALL solve_A_setQ( DOM )
Q_0(:) = DOM%RSL%Q_A(:)
! Et sauvegarde des energies a l'etat precedent
E_0 = DOM%RSL%E

! Evaluation des nouvelles proprietes avec variables modifiees
CALL cyc_prop_A( DOM )


!--- SECONDS MEMBRES DES EQUATIONS DE CONSERVATION --------------------!
! Termes advectifs : dE et dm_e (gaz)
CALL cyc_advection( DOM )

! RHS des EDP a l'etat precedent sauve dans F_0
CALL solve_A_setF( DOM )
F_0(:) = DOM%RSL%F_A(:)

! Evaluation du residu initial
R(:) = + dt*F_0(:)
   

!----------------------------------------------------------------------!
!--- METHODE ITERATIVE DE NEWTON-RAPHSON DU SYSTEME ADVECTION ---------!


!--- MATRICE JACOBIENNE -------------------------------------------!
! Evaluation de la matrice jacobienne de la fonction F( Q*, Q(n) )
CALL jac_A( DOM )

! Evaluation de la matrice jacobienne de la fonction R = (Q* - Q(n)) - dt*F( Q*, Q(n) )
! dR/dQ* = Id - theta*dt*dF/dQ
DOM%RSL%JAC_A(:,:,:) = - (theta_imp*dt) * DOM%RSL%JAC_A(:,:,:)
DOM%RSL%JAC_A(:,1,:) = 1.0_dp + DOM%RSL%JAC_A(:,1,:)

! Mise ne forme de la matrice jacobnienne dRdQ* en forme ST (sparse triplet)
CALL solve_A_setST( DOM%RSL%JAC_A, DOM%RSL%id_JAC_A, DOM%RSL%JAC_A_stval )

! Nombre d'it externe du solveur lineaire
niter_lin = DOM%NUM%niter_max_int

!--- Phase analyse + factorisation + resolution systeme avec PARDISO
phase = 13
CALL PARDISO (DOM%RSL%PT, &            ! Adresses pointeurs internes PARDISO
            maxfct, &                   ! Nombre de systemes
            mnum, &                     ! Indice du systeme <= maxfct
            DOM%RSL%MTYPE, &           ! Type de matrice = 11 (reel non symetrique)
            phase, &                    ! Phase de resolution
            DOM%RSL%ndl_A, &          ! Nombre d'equations
            DOM%RSL%JAC_A_stval, &    ! Vecteur reel de la matrice format CSR3
            DOM%RSL%JAC_A_csrow, &    ! Vecteur indice ligne format CSR3
            DOM%RSL%JAC_A_stcol, &    ! Vecteur indice colonne format CSR3
            DOM%RSL%PERM, &            ! Indice des permutations (non-utilise)
            nrhs, &                     ! Nombre de RHS
            DOM%RSL%IPARM, &           ! Tableau des parametres (defini dans don_csti_rsl.f90)
            msglvl, &                   ! Mode verbose
            R, &                        ! Terme de droite
            dQ, &                       ! Solution
            error)                      ! Code erreur


!--- APPEL AU SOLVEUR GMRES ---------------------------------------!
! CALL mgmres_st ( DOM%RSL%ndl_A, &         ! Nb de degres de libertes du systeme
!      DOM%RSL%nnz_A, &         ! Nombre de non-zeros dans la matrice sparse JAC_A_stval
!      DOM%RSL%JAC_A_strow, &   ! Ligne de JAC_A_stval(nnz) dans les mat. pleine A(ndl,ndl)
!      DOM%RSL%JAC_A_stcol, &   ! Col. de JAC_A_stval(nnz) dans les mat. pleine A(ndl,ndl)
!      DOM%RSL%JAC_A_stval, &   ! Valeurs de JAC_A_stval(nnz) dans les mat. pleine A(ndl,ndl)
!      dQ, &                      ! Resultats
!      R, &                       ! Termes de droite
!      niter_lin, &               ! Nb d'it. externes
!      DOM%NUM%niter_max_int, &  ! Nb d'it. internes
!      DOM%NUM%tol_abs, &        ! Tol absolue
!      DOM%NUM%tol_rel )         ! Tol relative

! Sauvegarde du nombre d'it. externes
DOM%RSL%niter_A_lin = niter_lin

!--- MAJ DES ETAT -------------------------------------------------!
! Nouvelles quantites conservees
DOM%RSL%Q_A = DOM%RSL%Q_A + dQ

! Recuperation des quantites conservees : m_e (gaz)
CALL solve_A_getQ( DOM )

! Nouveaux Termes advectifs : dE et dm_e (gaz)
CALL cyc_reset( DOM )
CALL cyc_advection( DOM )
! Eval du nouveau F_A 
CALL solve_A_setF( DOM )

! Recalcul des résidus
R = DOM%RSL%Q_A - Q_0 - theta_imp*dt*DOM%RSL%F_A - (1.0_dp-theta_imp)*dt*F_0
DOM%RSL%residu_A = R !!!TEST!!!
residu = normL2( R(:) )

! Variations d'energie correspondantes au dm_e (gaz) trouvé
DOM%RSL%E = E_0 + dt * DOM%RSL%dE

! Mise a jour des variables naturelles avec m_e et E modifiés
CALL cyc_update_A( DOM )
! + pression
CALL prop_P( DOM )
! + Vitesse gaz
CALL prop_vg( DOM )


! Sauvergarde du residu final
DOM%RSL%err_it_A = residu

END SUBROUTINE solve_A_thetalin
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de verification de stabilite du calcul explicite (critere CFL)
SUBROUTINE solve_A_stab( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
REAL(DP) :: crit_stab
REAL(DP), DIMENSION(DOM%MLG%nfac) :: CFL_fac

! CFL_fac aux faces
CFL_fac = MAX( 2.0_dp*eps_min, SQRT( DOM%ETAT%v_g_itp(:,1)**2 + DOM%ETAT%v_g_itp(:,2)**2 + DOM%ETAT%v_g_itp(:,3)**2 ))
CFL_fac = CFL_fac / SQRT( DOM%MLG%n2_G1G2 )
CFL_fac = CFL_fac * DOM%NUM%dt
            
! Stabilite explicite pour resolution explicite : 
! crit_stab = max(CFL) < CFL_max
crit_stab = MAXVAL( ABS(CFL_fac) )
            
IF (crit_stab >= DOM%NUM%CFL_max) THEN
    CALL print_err('Le critere de stabilite explicite n''est plus respecte')
    CALL print_err('CFL = '//realtostr(crit_stab)//' > '//realtostr(DOM%NUM%CFL_max))
END IF

END SUBROUTINE solve_A_stab
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Mise en forme des valeurs de la matrice A du systeme A au format Sparse Triplet 
SUBROUTINE solve_A_setST( JAC_A, id_JAC_A, JAC_A_stval )
!
!---- Déclarations ----------------------------------------------------!
REAL(DP), DIMENSION(:,:,:) ::  JAC_A
INTEGER(IP), DIMENSION(:,:,:) ::  id_JAC_A
REAL(DP), DIMENSION(:) :: JAC_A_stval
INTEGER(IP) :: nd1, nd2, nd3
INTEGER(IP) :: id1, id2, id3
INTEGER(IP) :: inz

! Shape de la matrice A du systeme Ax=b dans le format sparse MoDeTheC 
nd1 = SIZE(JAC_A,1)
nd2 = SIZE(JAC_A,2)
nd3 = SIZE(JAC_A,3)

! Pour chaque élément de la matrice sparse MoDeTheC
DO id1=1,nd1 ; DO id2=1,nd2 ; DO id3=1,nd3

    ! On récupère la position (inz) correspondante pour le format Sparse Triplet
    inz = id_JAC_A(id1,id2,id3)
    ! Si la valeur fait partie du système (pas cellule limite)
    IF (inz /= 0) THEN
    
        ! On ecrit dans la matrice sparse format SP
        JAC_A_stval(inz) = JAC_A(id1,id2,id3)
    
    END IF
    
! Fin triple boucle
END DO ; END DO ; END DO

END SUBROUTINE solve_A_setST
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Mise en forme des valeurs du vecteur F(RHS des EDP) 
!> du systeme A au format Sparse Triplet 
SUBROUTINE solve_A_setF( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: igaz, ngaz, id_esp
INTEGER(IP) :: icel, ncel
INTEGER(IP) :: neq_A
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
ngaz = DOM%PHYS%ngaz
neq_A = DOM%RSL%neq_A

! NB : vecteur F_A( ndl_A )
! F_A = | dm_e (cel1,gaz1)        -\
!       | dm_e (cel1,gaz2)         |
!       | dm_e (cel1,...)          |-  cel1
!       | dm_e (cel1,ngaz)        -/
!       | dm_e (cel2,gaz1)        -\
!       | dm_e (cel2,gaz2)         |
!       | ....                     |-  cel2

! Pour chaque cellule
DO icel = 1,ncel

    ! Et chaque gaz
    DO igaz = 1,ngaz

        id_esp = DOM%PHYS%ID_gaz( igaz )
        ! Terme de variation de dme
        DOM%RSL%F_A( neq_A*(icel-1) + igaz ) = DOM%RSL%dm_e(icel,id_esp)

    END DO
END DO

END SUBROUTINE solve_A_setF
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Mise en forme des valeurs du vecteur Q (quantites conservees des EDP) 
!> du systeme A au format Sparse Triplet 
SUBROUTINE solve_A_setQ( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: igaz, ngaz, id_esp
INTEGER(IP) :: icel, ncel
INTEGER(IP) :: neq_A
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
ngaz = DOM%PHYS%ngaz
neq_A = DOM%RSL%neq_A

! NB : vecteur Q_A( ndl_A )
! Q_A = | m_e (cel1,gaz1)        -\
!       | m_e (cel1,gaz2)         |
!       | m_e (cel1,...)          |-  cel1
!       | m_e (cel1,ngaz)        -/
!       | m_e (cel2,gaz1)        -\
!       | m_e (cel2,gaz2)         |
!       | ...                     |-  cel2

! Pour chaque cellule
DO icel = 1,ncel
    
    ! Et chaque gaz
    DO igaz = 1,ngaz

        id_esp = DOM%PHYS%ID_gaz( igaz )
        ! Masses de gaz
        DOM%RSL%Q_A( neq_A*(icel-1) + igaz ) = DOM%RSL%m_e(icel,id_esp)

    END DO
END DO

END SUBROUTINE solve_A_setQ
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Recuperation des valeurs du vecteur Q (quantites conservees des EDP) 
!> du systeme A au format Sparse Triplet 
!> NB : Fonction reciproque de solve_A_setQ
SUBROUTINE solve_A_getQ( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: igaz, ngaz, id_esp
INTEGER(IP) :: icel, ncel
INTEGER(IP) :: neq_A
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
ngaz = DOM%PHYS%ngaz
neq_A = DOM%RSL%neq_A

! NB : vecteur Q_A( ndl_A )
! Q_A = | m_e (cel1,gaz1)        -\
!       | m_e (cel1,gaz2)         |
!       | m_e (cel1,...)          |-  cel1
!       | m_e (cel1,ngaz)        -/
!       | m_e (cel2,gaz1)        -\
!       | m_e (cel2,gaz2)         |
!       | ...                     |-  cel2

! Pour chaque cellule
DO icel = 1,ncel
    
    ! Et chaque gaz
    DO igaz = 1,ngaz

        id_esp = DOM%PHYS%ID_gaz( igaz )
        ! Masses de gaz
        DOM%RSL%m_e(icel,id_esp) = DOM%RSL%Q_A( neq_A*(icel-1) + igaz )

    END DO
END DO

END SUBROUTINE solve_A_getQ
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_solve_A
