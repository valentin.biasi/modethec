!> Module de calcul des flux thermiques liés à la 
!> régression de surface en cas d'ablation
!> Utilisé dans mod_diffusion.f90
MODULE mod_regression
!
USE mod_cst, ONLY : IP, DP, eps_min
USE mod_print, ONLY : print_err
USE mod_algebra, ONLY : norm,dot
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Calcul de l'éventuelle régression de surface en cas d'ablation
SUBROUTINE cyc_regression (DOM, ilim,iiter)
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: ilim, ifac_lim, nfac_limi, id_fac, id_cel, iiter
REAL(DP) :: dt, FL_entrant, X
REAL(DP), DIMENSION(3) :: VEC1, VEC2
!
!---- Récupérations ---------------------------------------------------!
dt = DOM%NUM%dt


SELECT CASE ( DOM%CDT%LIM( ilim )%abla_typ )

! Cas de l'ablation à vitesse de régraession imposée
CASE ("vitesse")
    ! Boucle sur les faces de la limite courante
    nfac_limi = DOM%CDT%LIM(ilim)%nfac_limi
    DO ifac_lim = 1, nfac_limi
        ! Indice absolu de la face
        id_fac = DOM%CDT%LIM(ilim)%ID_FACLIMi( ifac_lim )
        DOM%ETAT%taux_reg( id_fac ) = DOM%MLG%AIRE(id_fac)*DOM%CDT%LIM( ilim )%Vreg_imp*dt  
    END DO

! Cas de l'ablation à température de paroi constante    
CASE ("temp") 
    SELECT CASE (DOM%NUM%schema_DR)
    
    ! Cas explicite : le flux rentre intégralement dans le système et on en déduit la régression de surface 
    ! à appliquer pour éliminer l'énergie excédentaire pour rétablir T_abla à la surface.
    CASE ("euler-exp")
    
        nfac_limi = DOM%CDT%LIM(ilim)%nfac_limi
        ! Boucle sur les faces de la limite courante
        DO ifac_lim = 1, nfac_limi
            ! Indice absolu de la face
            id_fac = DOM%CDT%LIM(ilim)%ID_FACLIMi( ifac_lim )                
            ! Indice de la cel
            id_cel = DOM%MLG%FAC2CEL(id_fac,3)
            ! Le flux ablatif correspond à l'éxcédent d'énergie stockée entre T face et T abla 
            ! on applique un coef 1.5 pour passer du delta T paroi au delta T cellule
            ! (extrapolation linéaire de la température paroi suivant celle de la température adjacente)
            DOM%RSL%FL_abla( id_fac )  = max(0.0_dp,DOM%ETAT%rho(id_cel)*DOM%MLG%VOLUME(id_cel)*DOM%ETAT%Cp(id_cel)*1.5_dp*(DOM%ETAT%T_itp(id_fac) - DOM%PHYS%MAT%T_abla )/dt)
            IF ( ABS( DOM%RSL%FL_abla( id_fac ) ) > eps_min) THEN
                DOM%ETAT%taux_reg( id_fac ) = DOM%RSL%FL_abla( id_fac ) * dt / ( DOM%ETAT%rho(id_cel) * (DOM%PHYS%MAT%h_abla + DOM%ETAT%h(id_cel)))              
            ELSE
               DOM%ETAT%taux_reg( id_fac ) = 0.0_dp 
            END IF 
        END DO
    
    ! Cas implicite, on enlève au flux entrant un flux ablatif afin d'éviter
    ! l'entrée dans le système d'une énergie excédentaire
    CASE ("theta-imp")    

        nfac_limi = DOM%CDT%LIM(ilim)%nfac_limi
        ! Boucle sur les faces de la limite courante
        DO ifac_lim = 1, nfac_limi
            ! Indice absolu de la face
            id_fac = DOM%CDT%LIM(ilim)%ID_FACLIMi( ifac_lim )                
            ! Indice de la cel
            id_cel = DOM%MLG%FAC2CEL(id_fac,3)
            
            ! Switch à la première itération entre le mode normal sans ablation
            ! et le mode "saturé" avec ablation
            IF (iiter == 1) THEN            
                IF(( DOM%PHYS%MAT%T_abla - DOM%ETAT%T_itp(id_fac)) < eps_min) THEN ! passage en mode saturé 
                    DOM%CDT%LIM( ilim )%ACTIVATION_ABLA (ifac_lim) = 1
                ELSE
                    DOM%CDT%LIM( ilim )%ACTIVATION_ABLA (ifac_lim) = 0 ! passage en mode normal
                END IF
            END IF
            
            ! Cas saturé
            IF(DOM%CDT%LIM( ilim )%ACTIVATION_ABLA (ifac_lim) == 1) THEN 
                ! température de la face limite = Tabla
                DOM%ETAT%T_itp(id_fac) = DOM%PHYS%MAT%T_abla
                ! Détermination du flux ablatif et saturation du flux entrant
                FL_entrant = DOM%RSL%FL_cond( id_fac )
                VEC1 = DOM%ETAT%k_itp(id_fac,:) * ( DOM%PHYS%MAT%T_abla - DOM%ETAT%T(id_cel) )*DOM%MLG%G1H(id_fac,:) / DOM%MLG%n2_G1H(id_fac)
                VEC2 = DOM%MLG%VFAC(id_fac,:)
                DOM%RSL%FL_cond( id_fac ) = max (0.0_dp,DOM%MLG%AIRE(id_fac) * dot( VEC1 , VEC2 ) )
                DOM%RSL%FL_abla( id_fac ) = FL_entrant - DOM%RSL%FL_cond( id_fac )
            ELSE
                DOM%RSL%FL_abla( id_fac ) = 0.0_dp
            END IF
            
            ! Détermination du volume à ablater
            IF ( DOM%RSL%FL_abla( id_fac ) > eps_min) THEN
                DOM%ETAT%taux_reg( id_fac ) = DOM%RSL%FL_abla( id_fac ) * dt / ( DOM%ETAT%rho(id_cel) * (DOM%PHYS%MAT%h_abla  + DOM%ETAT%Cp(id_cel)*(DOM%PHYS%MAT%T_abla - DOM%ETAT%T(id_cel))))
            ELSE
                DOM%ETAT%taux_reg( id_fac ) = 0.0_dp 
            END IF
        
        END DO
    END SELECT

! Cas de l'ablation mécanique avec une vitesse d'ablation dépendant de la température
CASE("meca")

    ! Détermination de la vitesse de régression en fonction de la température de la face
    nfac_limi = DOM%CDT%LIM(ilim)%nfac_limi
    ! Boucle sur les faces de la limite courante
    DO ifac_lim = 1, nfac_limi
        ! Indice absolu de la face
        id_fac = DOM%CDT%LIM(ilim)%ID_FACLIMi( ifac_lim )                
        ! Indice de la cel
        id_cel = DOM%MLG%FAC2CEL(id_fac,3)
        ! Calcul de la vitesse d'ablation de la vitesse de la face courante (modèle "liège" )
        IF ( DOM%ETAT%T_itp(id_fac) < 1200.0_dp ) THEN
            DOM%CDT%LIM( ilim )%Vreg_meca( ifac_lim ) = 0.0_dp
        ELSE IF (DOM%ETAT%T_itp(id_fac) > 2000.0_dp) THEN
            DOM%CDT%LIM( ilim )%Vreg_meca( ifac_lim ) = 0.162D-3
        ELSE
            ! Détermination du paramètre X qui est la variable d'entrée de la fonction vitesse d'albation
            X =  DOM%ETAT%T_itp(id_fac) - 1200.0_dp
            ! Détermination de la vitesse d'ablation (régression polynomiale de données fabricant)
            DOM%CDT%LIM( ilim )%Vreg_meca( ifac_lim ) = 1.945167412D-10 * X**2 + 4.94780857D-8 * X
        END IF 
        
                        
        ! Le flux ablatif correspond à l'énergie stocké dans le volume aire *(Vreg_meca*dt) 
        DOM%RSL%FL_abla( id_fac )  = max(0.0_dp,DOM%ETAT%rho(id_cel)*(DOM%ETAT%h(id_cel)+DOM%PHYS%MAT%h_abla)*DOM%MLG%AIRE(id_fac)*DOM%CDT%LIM( ilim )%Vreg_meca( ifac_lim ))
        IF ( ABS( DOM%RSL%FL_abla( id_fac ) ) > eps_min) THEN
            DOM%ETAT%taux_reg( id_fac ) = DOM%MLG%AIRE(id_fac)*DOM%CDT%LIM( ilim )%Vreg_meca( ifac_lim )*dt            
        ELSE
           DOM%ETAT%taux_reg( id_fac ) = 0.0_dp 
        END IF         
    END DO


! Cas de limite non ablative
CASE("fixe","axe")
    ! Rien ici

CASE DEFAULT

    CALL print_err("Le type de limite ablative n'est pas reconnu")


END SELECT

END SUBROUTINE cyc_regression
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE
