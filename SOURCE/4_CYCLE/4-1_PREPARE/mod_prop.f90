!> Calcul des propriétés du système (conductivité, masse molaire,...)
!> en fonction des variables naturelles du système.  
!> \details Pour toules les routines, seule la structure DOM%ETAT%... est modifiée.
!> \todo Le calcul de diffusivite de masse (Fick) n'est pas implemente
!> \todo Calcul avec Cp global
MODULE mod_prop
!
USE mod_cst, ONLY : DP, IP, R_GP, eps_min
USE mod_algebra, ONLY : polyval, normL2, realtostr
USE mod_print, ONLY : print_err
USE mod_interpolate, ONLY : grad_cel, interp_fac, interp_P
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Calcul de l'ensemble des proprietes thermiques (DIFFUSION CHALEUR)
SUBROUTINE cyc_prop_D( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM

! Evaluation de la conductivite thermique homogeneise
CALL prop_kth( DOM )

! Evaluation de la capacite calorifique moyenne
CALL prop_Cp( DOM )

! Evaluation de l'enthalpie moyenne
CALL prop_h( DOM )

! Evaluation des proprietes radiatives de surface
CALL prop_rad( DOM )

!
END SUBROUTINE cyc_prop_D
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul de l'ensemble des proprietes massiques (ADVECTION GAZ)
SUBROUTINE cyc_prop_A( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM

! Evaluation de l'enthalpie de la phase gaz
CALL prop_hg( DOM )

! Evaluation de la viscosite dynamique
CALL prop_mu( DOM )

! Evaluation de la permeabilite
CALL prop_Kp( DOM )

! Evaluation des vitesses de darcy
CALL prop_vg( DOM )

!
END SUBROUTINE cyc_prop_A
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul des temperatures a Cp variable par la methode de Newton-Raphson
!> \warning Le cas rho variable n'est pas implemente mais est tres simple a ajouter
SUBROUTINE E_to_T( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
REAL(DP), DIMENSION(DOM%MLG%ncel) :: F, dFdT
INTEGER(IP) :: ncel
REAL(DP) :: eps
INTEGER(IP) :: iiter
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
!
! Evaluation de F = E - rho*V*h , fonction a resoudre tq F(T*)=0
! Actualisation de h avec le nouveau T(i)
CALL prop_h( DOM )
! Actualisation de Cp avec le nouveau T(i)
CALL prop_Cp( DOM )
! Et calcul du nouveau F(T(i))
F = DOM%RSL%E - (DOM%MLG%VOLUME * DOM%ETAT%rho * DOM%ETAT%h)
!
! Calcul de la tolerance cible = eps
eps = (DOM%NUM%tol_rel * normL2(F)) !+ DOM%NUM%tol_abs

! Evite les conflits avec solveurs a tolerance identique 
eps = eps * 1.0e-3_dp

iiter = 1
! Boucle pour methode de Newton-Raphson
DO WHILE ( (normL2(F)>eps) .AND. (iiter<=DOM%NUM%niter_max) )
    
    iiter = iiter + 1       ! Incrementation
    
    !--- Calcul de F = E-rho*V*h
    ! Actualisation de h avec le nouveau T(i)
    CALL prop_h( DOM )
    ! Et calcul du nouveau F(T(i))
    F = DOM%RSL%E - (DOM%MLG%VOLUME * DOM%ETAT%rho * DOM%ETAT%h)


    !--- Calcul de dFdT = -rho*V*Cp
    dFdT = - (DOM%MLG%VOLUME * DOM%ETAT%rho * DOM%ETAT%Cp)
    
    ! T(i+1) = T(i) - F(T(i)) / dFdT(T(i))
    DOM%ETAT%T = DOM%ETAT%T - (F / dFdT)
    
    ! Probleme si nombre d'it. max depasse
    ! IF (iiter == DOM%NUM%niter_max) THEN
    !     CALL print_err('Le nombre d''iterations max est depasse dans E_to_T')
    !     CALL print_err('eps =  '//realtostr(eps)//' < norm(E) = '//realtostr(normL2(F)))
    !     !PRINT*, DOM%ETAT%T
    ! END IF

END DO

END SUBROUTINE E_to_T
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul des masses volumiques absolues des constituants solides
SUBROUTINE prop_rho_e( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: nesp
CHARACTER (len=32) :: typ_esp
CHARACTER (len=32) :: rho_typ
INTEGER(IP) :: iesp
!
!---- Récupérations ---------------------------------------------------!
nesp = DOM%PHYS%nesp
!
! Mise a zero
DOM%ETAT%rho_e(:,:) = 0.0_dp
!
! Boucle sur les espèces
DO iesp = 1,nesp
    
    ! Si l'espèce est un solide
    typ_esp = DOM%PHYS%ESP(iesp)%typ_esp
    IF (typ_esp == 'solide') THEN
        
        ! Choix de la méthode de calcul : valeur constante, polynome de T,...
        rho_typ = DOM%PHYS%ESP(iesp)%rho_typ
        SELECT CASE (rho_typ)
        
            ! Valeur constante
            CASE ("constant")
                DOM%ETAT%rho_e(:,iesp) = DOM%PHYS%ESP(iesp)%rho_cst
            
            ! Polynome de T
            CASE ("polyn")
                CALL polyval(   DOM%PHYS%ESP(iesp)%rho_poly, &
                                DOM%ETAT%T(:), &
                                DOM%ETAT%rho_e(:,iesp) )
            ! Autres cas ...
        END SELECT
    END IF
END DO
!
END SUBROUTINE prop_rho_e
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul de la masse molaire équivalente du mélange gazeux tel que :
!> M = 1/phi_g * somme( M_j * phi_j ) avec j constituant gaz
SUBROUTINE prop_M( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: nesp
CHARACTER (len=32) :: typ_esp
INTEGER(IP) :: iesp
!
!---- Récupérations ---------------------------------------------------!
nesp = DOM%PHYS%nesp
!
! Mise a zero
DOM%ETAT%M(:) = 0.0_dp
!
! Boucle sur les espèces
DO iesp = 1,nesp
    
    ! Si l'espèce est un gaz
    typ_esp = DOM%PHYS%ESP(iesp)%typ_esp
    IF (typ_esp == 'gaz') THEN
        
        ! M <= somme( M_j * phi_j )
        !DOM%ETAT%M(:) = DOM%ETAT%M(:) + &
        !                 ( DOM%ETAT%phi(:,iesp) * DOM%PHYS%ESP(iesp)%M )
        
        ! M <= somme( 1/ M_j * Y_j / Y_g )
        DOM%ETAT%M(:) = DOM%ETAT%M(:) + &
                         ( DOM%ETAT%Y(:,iesp) / (DOM%ETAT%Y_g(:)) / DOM%PHYS%ESP(iesp)%M )

    END IF
END DO
!
! M <= 1/M
DOM%ETAT%M(:) = 1.0_dp / DOM%ETAT%M(:)
!
IF (ANY( DOM%ETAT%phi_g(:) < -2.0_dp*eps_min )) THEN
    CALL print_err('phi_g semble contenir des valeurs negatives',1)
END IF
!
! M <= M / phi_g
!WHERE (DOM%ETAT%phi_g(:) > 2*eps_min)
!    DOM%ETAT%M(:) = DOM%ETAT%M(:) / DOM%ETAT%phi_g(:)
!END WHERE
!
END SUBROUTINE prop_M
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul de la capacite calorifique équivalente moyenne telle que :
!> Cp = somme( Cp_i * Y_i ) avec i constituants
SUBROUTINE prop_Cp( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: iesp, nesp
CHARACTER (len=32) :: Cp_typ, Cp_homog
INTEGER(IP) :: ncel
REAL(DP), DIMENSION(DOM%MLG%ncel) :: Cp_i
REAL(DP), DIMENSION(DOM%MLG%ncel) :: T
!
!---- Récupérations ---------------------------------------------------!
nesp = DOM%PHYS%nesp
ncel = DOM%MLG%ncel
!
Cp_homog = DOM%PHYS%MAT%Cp_homog
!
!
! Mise a zero
DOM%ETAT%Cp(:) = 0.0_dp
T = DOM%ETAT%T
!
! Saturation en température des propriétés
IF (DOM%PHYS%MAT%satur_T == 1) THEN
    WHERE (T(:) > DOM%PHYS%MAT%T_sat)
        T = DOM%PHYS%MAT%T_sat
    END WHERE
END IF
!
! Boucle sur les espèces
DO iesp = 1,nesp
        
    ! Choix de la méthode de calcul : valeur constante, polynome de T,...
    Cp_typ = DOM%PHYS%ESP(iesp)%Cp_typ
    SELECT CASE (Cp_typ)
    
        ! Valeur constante
        CASE ("constant")
            Cp_i = DOM%PHYS%ESP(iesp)%Cp_cst * DOM%ETAT%Y(:,iesp)
            DOM%ETAT%Cp(:) = DOM%ETAT%Cp(:) + Cp_i
                    
        ! Polynome de T
        CASE ("polyn")
            CALL polyval(   DOM%PHYS%ESP(iesp)%Cp_poly, T(:), Cp_i )
            DOM%ETAT%Cp(:) = DOM%ETAT%Cp(:) + (Cp_i * DOM%ETAT%Y(:,iesp))
            
        ! Autres cas ...
        CASE DEFAULT
            CALL print_err('Les methodes de calcul de Cp ne sont pas reconnues',1)
            
    END SELECT

END DO

END SUBROUTINE prop_Cp
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul de la conductivite thermique équivalente moyenne telle que :
!> k = somme( Cp_i * phi_i ) avec i constituants - loi des melanges
SUBROUTINE prop_kth( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: iesp, nesp
CHARACTER (len=32) :: k_typ, k_homog
INTEGER(IP) :: icel, ncel, ifac, nfac
REAL(DP), DIMENSION(DOM%MLG%ncel) :: k_i, T
REAL(DP), DIMENSION(DOM%MLG%nfac) :: k_i_itp, T_itp
INTEGER(IP) :: i_dim
INTEGER(IP) :: id_continu
REAL(DP), DIMENSION(3) :: E_MT
REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: A_MT, P, Q, A_MT_itp, P_itp, Q_itp
REAL(DP), ALLOCATABLE, DIMENSION(:) :: k_continu, k_continu_itp
!
!---- Récupérations ---------------------------------------------------!
nesp = DOM%PHYS%nesp
ncel = DOM%MLG%ncel
nfac = DOM%MLG%nfac
!
k_homog = DOM%PHYS%MAT%k_homog
!
!
! Mise a zero
DOM%ETAT%k(:,:) = 0.0_dp
DOM%ETAT%k_itp(:,:) = 0.0_dp
!
T = DOM%ETAT%T
T_itp = DOM%ETAT%T_itp

! Saturation en température des propriétés
IF (DOM%PHYS%MAT%satur_T == 1) THEN
    WHERE (T(:) > DOM%PHYS%MAT%T_sat)
        T = DOM%PHYS%MAT%T_sat
    END WHERE
    WHERE (T_itp(:) > DOM%PHYS%MAT%T_sat)
        T_itp = DOM%PHYS%MAT%T_sat
    END WHERE
END IF
!
!
! Type d'homogeneisation de conductivite : 'global' - 'melange' - ...
SELECT CASE (k_homog)

!---- Cas loi globale : polynome de T
CASE ("global")
    
    ! Loi polynomiale simple
    CALL polyval( DOM%PHYS%MAT%k0_poly, T(:), k_i )
    CALL polyval( DOM%PHYS%MAT%k0_poly, T_itp(:), k_i_itp )
        
    DO i_dim =1,3
        DOM%ETAT%k(:,i_dim) = k_i
        DOM%ETAT%k_itp(:,i_dim) = k_i_itp
    END DO


!---- Cas loi de mélange simple : k = sum( k_i * phi_i )
CASE ("melange")

    ! Boucle sur les espèces
    DO iesp = 1,nesp
            
        ! Choix de la méthode de calcul : valeur constante, polynome de T,...
        k_typ = DOM%PHYS%ESP(iesp)%k_typ
        SELECT CASE (k_typ)
        
            ! Valeur constante
            CASE ("constant")
                k_i = DOM%PHYS%ESP(iesp)%k_cst * DOM%ETAT%phi(:,iesp)
                k_i_itp = DOM%PHYS%ESP(iesp)%k_cst * DOM%ETAT%phi_itp(:,iesp)
                DO i_dim =1,3
                    DOM%ETAT%k(:,i_dim) = DOM%ETAT%k(:,i_dim) + k_i
                    DOM%ETAT%k_itp(:,i_dim) = DOM%ETAT%k_itp(:,i_dim) + k_i_itp
                END DO
                        
            ! Polynome de T
            CASE ("polyn")
                CALL polyval(   DOM%PHYS%ESP(iesp)%k_poly, T(:), k_i )
                CALL polyval(   DOM%PHYS%ESP(iesp)%k_poly, T_itp(:), k_i_itp )
                
                DO i_dim =1,3
                    DOM%ETAT%k(:,i_dim) = DOM%ETAT%k(:,i_dim) + (k_i * DOM%ETAT%phi(:,iesp))
                    DOM%ETAT%k_itp(:,i_dim) = DOM%ETAT%k_itp(:,i_dim) + &
                                            (k_i_itp * DOM%ETAT%phi_itp(:,iesp))
                END DO
            
            ! Orthotrope + Fonction de T
            CASE ("ortho")
                ! En X - Aux cellules
                CALL polyval(   DOM%PHYS%ESP(iesp)%k_ortho_x, T(:), k_i )
                DOM%ETAT%k(:,1) = DOM%ETAT%k(:,1) + (k_i * DOM%ETAT%phi(:,iesp))
                ! En X - Aux faces
                CALL polyval(   DOM%PHYS%ESP(iesp)%k_ortho_x, T_itp(:), k_i_itp )
                DOM%ETAT%k_itp(:,1) = DOM%ETAT%k_itp(:,1) + (k_i_itp * DOM%ETAT%phi_itp(:,iesp))
                ! En Y - Aux cellules
                CALL polyval(   DOM%PHYS%ESP(iesp)%k_ortho_y, T(:), k_i )
                DOM%ETAT%k(:,2) = DOM%ETAT%k(:,2) + (k_i * DOM%ETAT%phi(:,iesp))
                ! En Y - Aux faces
                CALL polyval(   DOM%PHYS%ESP(iesp)%k_ortho_y, T_itp(:), k_i_itp )
                DOM%ETAT%k_itp(:,2) = DOM%ETAT%k_itp(:,2) + (k_i_itp * DOM%ETAT%phi_itp(:,iesp))
                ! En Z - Aux cellules
                CALL polyval(   DOM%PHYS%ESP(iesp)%k_ortho_z, T(:), k_i )
                DOM%ETAT%k(:,3) = DOM%ETAT%k(:,3) + (k_i * DOM%ETAT%phi(:,iesp))
                ! En Z - Aux faces
                CALL polyval(   DOM%PHYS%ESP(iesp)%k_ortho_z, T_itp(:), k_i_itp )
                DOM%ETAT%k_itp(:,3) = DOM%ETAT%k_itp(:,3) + (k_i_itp * DOM%ETAT%phi_itp(:,iesp))
            
            ! Autres cas ...
            CASE DEFAULT
                CALL print_err('Les methodes de calcul de k ne sont pas reconnues',1)
                
        END SELECT

    END DO

                
!---- Mori-Tanaka Fibres-Résine-Char-Gaz
!---- A FAIRE : CHOIX DE LA DIRECTION DU PLAN COMPOSITE IMPOSE EN X
!---- Conductivite constantes ou polynomiales seulement
CASE ("Mori-Tanaka")
   
    ! Allocations dans cas Mori-Tanaka
    ALLOCATE( k_continu( ncel ) )
    ALLOCATE( k_continu_itp( nfac ) )
    ALLOCATE( A_MT( ncel,3 ) )
    ALLOCATE( Q( ncel,3 ) )
    ALLOCATE( P( ncel,3 ) )
    ALLOCATE( A_MT_itp( nfac,3 ) )
    ALLOCATE( Q_itp( nfac,3 ) )
    ALLOCATE( P_itp( nfac,3 ) )
    
    ! RAZ des variables P et Q
    P(:,:) = 0.0_dp
    Q(:,:) = 0.0_dp
    P_itp(:,:) = 0.0_dp
    Q_itp(:,:) = 0.0_dp
    
    ! Trouve le milieu continu
    id_continu = 0
    DO iesp = 1,nesp        
        IF (SUM(DOM%PHYS%ESP(iesp)%k_Eshelby) < 2.0_dp*eps_min) THEN
            id_continu = iesp
        END IF
    END DO
    ! S'il n'y en a pas -> Erreur
    IF (id_continu == 0) THEN
        CALL print_err('Aucun milieu continu n''est defini pour Mori-Tananka',1)
    END IF
    
    ! Conductivite du milieu englobant
    ! Choix de la méthode de calcul : valeur constante, polynome de T,...
    k_typ = DOM%PHYS%ESP(id_continu)%k_typ
    SELECT CASE (k_typ)
    
        ! Valeur constante
        CASE ("constant")
            k_i = DOM%PHYS%ESP(id_continu)%k_cst
            k_i_itp = DOM%PHYS%ESP(id_continu)%k_cst
                    
        ! Polynome de T
        CASE ("polyn")
            CALL polyval(   DOM%PHYS%ESP(id_continu)%k_poly, T(:), k_i )
            CALL polyval(   DOM%PHYS%ESP(id_continu)%k_poly, T_itp(:), k_i_itp )
            
        ! Autres cas ...
        CASE DEFAULT
            CALL print_err('Les methodes de calcul de k ne sont pas reconnues',1)
            
    END SELECT
    ! Copie dans k_continu
    k_continu = k_i
    k_continu_itp = k_i_itp
        
        
    ! Boucle sur les espèces
    DO iesp = 1,nesp
        
        ! Calcul de k_i par espece
        ! Choix de la méthode de calcul : valeur constante, polynome de T,...
        k_typ = DOM%PHYS%ESP(iesp)%k_typ
        SELECT CASE (k_typ)
        
            ! Valeur constante
            CASE ("constant")
                k_i = DOM%PHYS%ESP(iesp)%k_cst
                k_i_itp = DOM%PHYS%ESP(iesp)%k_cst
                        
            ! Polynome de T
            CASE ("polyn")
                CALL polyval(   DOM%PHYS%ESP(iesp)%k_poly, T(:), k_i )
                CALL polyval(   DOM%PHYS%ESP(iesp)%k_poly, T_itp(:), k_i_itp )
                
            ! Autres cas ...
            CASE DEFAULT
                CALL print_err('Les methodes de calcul de k ne sont pas reconnues',1)
                
        END SELECT
        
        
        ! Matrice de concentration A_MT = ( Id - E_MT * (k_o-k_i)/k_o ) ^-1
        E_MT = DOM%PHYS%ESP(iesp)%k_Eshelby
        DO icel=1,ncel
            A_MT(icel,:) = E_MT * ( k_continu(icel) - k_i(icel) ) / k_continu(icel)
            A_MT(icel,:) = 1.0_dp - A_MT(icel,:)
        END DO
        DO ifac = 1,nfac
            A_MT_itp(ifac,:) = E_MT * ( k_continu_itp(ifac) - k_i_itp(ifac) ) / k_continu_itp(ifac)
            A_MT_itp(ifac,:) = 1.0_dp - A_MT_itp(ifac,:)
        END DO
        A_MT(:,:) = 1.0_dp / A_MT(:,:)
        A_MT_itp(:,:) = 1.0_dp / A_MT_itp(:,:)
        
        ! Q = Somme ( phi_i * k_i * A_MT_i )
        Q(:,:) = Q(:,:) + SPREAD( DOM%ETAT%phi(:,iesp) * k_i, 2, 3) * A_MT(:,:)
        Q_itp(:,:) = Q_itp(:,:) + SPREAD( DOM%ETAT%phi_itp(:,iesp) * k_i_itp, 2, 3) * A_MT_itp(:,:)
        
        ! P = Somme ( phi_i * A_MT_i )
        P(:,:) = P(:,:) + SPREAD( DOM%ETAT%phi(:,iesp), 2, 3) * A_MT(:,:)
        P_itp(:,:) = P_itp(:,:) + SPREAD( DOM%ETAT%phi_itp(:,iesp), 2, 3) * A_MT_itp(:,:)
        
    END DO
    
    ! Conductivite finale = Q * P ^-1
    DOM%ETAT%k(:,:) = Q(:,:) / P(:,:)
    DOM%ETAT%k_itp(:,:) = Q_itp(:,:) / P_itp(:,:)


    !!! TEST ISOTROPE !!!
    !DOM%ETAT%k(:,1) = (DOM%ETAT%k(:,1) + DOM%ETAT%k(:,2)) / 2
    !DOM%ETAT%k(:,2) = DOM%ETAT%k(:,1)
    !DOM%ETAT%k_itp(:,1) = (DOM%ETAT%k_itp(:,1) + DOM%ETAT%k_itp(:,2)) / 2
    !DOM%ETAT%k_itp(:,2) = DOM%ETAT%k_itp(:,1)
    
    ! Modif a l'ARRACHE!!
    ! En cas 2D axi => Composite quasi-isotrope sinon c'est débile
    IF (DOM%MLG%dim_simu == '2D_AXI') THEN
    
        ! Si axe = X => Modif de k_y = 1/2(k_y+k_z)
        IF ( ( normL2(DOM%MLG%AXE_2D(4:6)) - ABS(DOM%MLG%AXE_2D(4)) ) < 3.0_dp*eps_min ) THEN
            DOM%ETAT%k(:,2) = ( DOM%ETAT%k(:,2) + DOM%ETAT%k(:,3) ) / 2.0_dp
            DOM%ETAT%k_itp(:,2) = ( DOM%ETAT%k_itp(:,2) + DOM%ETAT%k_itp(:,3) ) / 2.0_dp
        
        ! Si axe = Y => Modif de k_x = 1/2(k_x+k_z)
        ELSE IF ( ( normL2(DOM%MLG%AXE_2D(4:6)) - ABS(DOM%MLG%AXE_2D(5)) ) < 3.0_dp*eps_min ) THEN
            DOM%ETAT%k(:,1) = ( DOM%ETAT%k(:,1) + DOM%ETAT%k(:,3) ) / 2.0_dp
            DOM%ETAT%k_itp(:,1) = ( DOM%ETAT%k_itp(:,1) + DOM%ETAT%k_itp(:,3) ) / 2.0_dp
            
        ! Sinon : cas non implemente
        ELSE
            CALL print_err('Probleme dans la definition de l''axe pour homog Mori-Tanaka',1)
        END IF

        ! Et k_z = 0 en 2D AXI
        DOM%ETAT%k(:,3) = 0.0_dp
        DOM%ETAT%k_itp(:,3) = 0.0_dp
        
    END IF

    DEALLOCATE(k_continu, k_continu_itp, A_MT, Q, P, A_MT_itp, Q_itp, P_itp)
    
!---- Sinon probleme
CASE DEFAULT
    
    CALL print_err('La methode d''homogeneisation de k n''est pas reconnue',1)
    
END SELECT


END SUBROUTINE prop_kth
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul de l'enthalpie équivalente moyenne telle que :
!> h = somme( h_i * Y_i ) = somme( int(Cpi) * Y_i ) avec i constituants
SUBROUTINE prop_h( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: iesp, nesp
CHARACTER (len=32) :: Cp_typ
INTEGER(IP) :: ncel, nfac
REAL(DP), DIMENSION(DOM%MLG%ncel) :: T, h_i, h_0
REAL(DP) :: T_lim
!
!---- Récupérations ---------------------------------------------------!
nesp = DOM%PHYS%nesp
ncel = DOM%MLG%ncel
nfac = DOM%MLG%nfac
!
! Mise a zero
DOM%ETAT%h(:) = 0.0_dp
!
T = DOM%ETAT%T
!
! Saturation en température des propriétés
IF (DOM%PHYS%MAT%satur_T == 1) THEN
    T_lim = DOM%PHYS%MAT%T_sat
    WHERE (T(:) > T_lim)
        T = T_lim
    END WHERE
END IF
!
! Boucle sur les espèces
DO iesp = 1,nesp
        
    ! Choix de la méthode de calcul : valeur constante, polynome de T,...
    Cp_typ = DOM%PHYS%ESP(iesp)%Cp_typ
    SELECT CASE (Cp_typ)
    
        ! Valeur constante
        CASE ("constant")
            ! Aux cellules
            h_i = DOM%PHYS%ESP(iesp)%Cp_cst * (DOM%ETAT%T - DOM%ETAT%T_ref)
            h_i = h_i * DOM%ETAT%Y(:,iesp)
            DOM%ETAT%h(:) = DOM%ETAT%h(:) + h_i
                    
        ! Polynome de T
        CASE ("polyn")
            ! Aux cellules
            CALL polyval( DOM%PHYS%ESP(iesp)%int_Cp_poly, T, h_i )
            CALL polyval( DOM%PHYS%ESP(iesp)%int_Cp_poly, DOM%ETAT%T_ref, h_0 )
            h_i = h_i - h_0
            !
            h_i = h_i * DOM%ETAT%Y(:,iesp)
            DOM%ETAT%h(:) = DOM%ETAT%h(:) + h_i
            
        ! Autres cas ...
        CASE DEFAULT
            CALL print_err('Les methodes de calcul de h (c-a-d Cp) ne sont pas reconnues',1)
            
    END SELECT

END DO

! Saturation en température des propriétés
IF (DOM%PHYS%MAT%satur_T == 1) THEN
    WHERE (DOM%ETAT%T(:) >= T_lim)
        DOM%ETAT%h = DOM%ETAT%Cp * ( DOM%ETAT%T - DOM%ETAT%T_ref )
        !h_i = h_i +  Cp_lim*(DOM%ETAT%T - T_lim)
    END WHERE
END IF
!
END SUBROUTINE prop_h
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul de l'enthalpie équivalente moyenne pour la phase gazeuse telle que :
!> h_g = somme( h_j * Y_j ) = somme( int(Cpj) * Y_j ) avec j gaz
SUBROUTINE prop_hg( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: iesp, nesp
CHARACTER (len=32) :: Cp_typ
INTEGER(IP) :: ncel, nfac
REAL(DP), ALLOCATABLE, DIMENSION(:) :: h_i, h_0
REAL(DP), ALLOCATABLE, DIMENSION(:) :: T
REAL(DP) :: T_lim
!
!---- Récupérations ---------------------------------------------------!
nesp = DOM%PHYS%nesp
ncel = DOM%MLG%ncel
nfac = DOM%MLG%nfac
!
! Tableau intermediaire : Cp par espece
ALLOCATE( h_i(ncel) )
ALLOCATE( h_0(ncel) )
ALLOCATE( T(ncel) )
!
! Mise a zero
DOM%ETAT%h_g(:) = 0.0_dp
!
T = DOM%ETAT%T
!
! Saturation en température des propriétés
IF (DOM%PHYS%MAT%satur_T == 1) THEN
    T_lim = DOM%PHYS%MAT%T_sat
    WHERE (T(:) > T_lim)
        T = T_lim
    END WHERE
END IF
!
! Boucle sur les espèces
DO iesp = 1,nesp
    ! Si iesp est un gaz
    IF (DOM%PHYS%ESP(iesp)%typ_esp .eq. 'gaz') THEN
    
        ! Choix de la méthode de calcul : valeur constante, polynome de T,...
        Cp_typ = DOM%PHYS%ESP(iesp)%Cp_typ
        SELECT CASE (Cp_typ)
        
            ! Valeur constante
            CASE ("constant")
                ! Aux cellules
                h_i = DOM%PHYS%ESP(iesp)%Cp_cst * (DOM%ETAT%T - DOM%ETAT%T_ref)
                h_i = h_i * DOM%ETAT%Y(:,iesp) / (DOM%ETAT%Y_g(:) + 2*eps_min)
                DOM%ETAT%h_g(:) = DOM%ETAT%h_g(:) + h_i
                        
            ! Polynome de T
            CASE ("polyn")
                ! Aux cellules
                CALL polyval( DOM%PHYS%ESP(iesp)%int_Cp_poly, T, h_i )
                CALL polyval( DOM%PHYS%ESP(iesp)%int_Cp_poly, DOM%ETAT%T_ref, h_0 )
                h_i = h_i - h_0
                
                h_i = h_i * DOM%ETAT%Y(:,iesp) / (DOM%ETAT%Y_g(:) + 2*eps_min)
                DOM%ETAT%h_g(:) = DOM%ETAT%h_g(:) + h_i
                
                
            ! Autres cas ...
            CASE DEFAULT
                CALL print_err('Les methodes de calcul de h (c-a-d Cp) ne sont pas reconnues',1)
                
        END SELECT

    END IF
END DO

IF (DOM%PHYS%MAT%satur_T == 1) THEN
    WHERE (DOM%ETAT%T(:) >= T_lim)
        DOM%ETAT%h_g = DOM%ETAT%Cp * (DOM%ETAT%T - DOM%ETAT%T_ref)
    END WHERE
END IF
!
DEALLOCATE( h_i, h_0, T )
!
END SUBROUTINE prop_hg
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul de la viscosite dynamique équivalente du mélange gazeux : 
!> Valeur independante de l'etat de degradation
SUBROUTINE prop_mu( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
CHARACTER (len=32) :: mu_typ
REAL(DP), ALLOCATABLE, DIMENSION(:) :: T, T_itp
REAL(DP) :: T_lim
!
ALLOCATE( T(DOM%MLG%ncel) )
ALLOCATE( T_itp(DOM%MLG%nfac) )
!
! Mise a zero
DOM%ETAT%mu(:) = 0.0_dp
DOM%ETAT%mu_itp(:) = 0.0_dp
!
T = DOM%ETAT%T
T_itp = DOM%ETAT%T_itp
!
! Saturation en température des propriétés
IF (DOM%PHYS%MAT%satur_T == 1) THEN
    T_lim = DOM%PHYS%MAT%T_sat
    WHERE (T(:) > T_lim)
        T = T_lim
    END WHERE
    WHERE (T_itp(:) > T_lim)
        T_itp = T_lim
    END WHERE
END IF
!
! Choix de la méthode de calcul : valeur constante, polynome de T,...
mu_typ = DOM%PHYS%MAT%mu_typ
SELECT CASE (mu_typ)

    ! Valeur constante
    CASE ("constant")
        ! Aux cellules
        DOM%ETAT%mu(:) = DOM%PHYS%MAT%mu_cst
        ! Aux faces
        DOM%ETAT%mu_itp(:) = DOM%PHYS%MAT%mu_cst
                
    ! Polynome de T
    CASE ("polyn")
        ! Aux cellules
        CALL polyval( DOM%PHYS%MAT%mu_poly, T, DOM%ETAT%mu(:) )
        ! Aux faces
        CALL polyval( DOM%PHYS%MAT%mu_poly, T_itp, DOM%ETAT%mu_itp(:) )
        
    ! Autres cas ...
    CASE DEFAULT
        CALL print_err('Les methodes de calcul de mu ne sont pas reconnues',1)
        
END SELECT
!
DEALLOCATE( T, T_itp )
!
END SUBROUTINE prop_mu
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul de la permeabilite du milieu
SUBROUTINE prop_Kp( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
CHARACTER (len=32) :: Kp_typ
!
!
! Mise a zero
DOM%ETAT%Kp(:,:) = 0.0_dp
DOM%ETAT%Kp_itp(:,:) = 0.0_dp
!
! Choix de la méthode de calcul : valeur constante, polynome de T,...
Kp_typ = DOM%PHYS%MAT%Kp_typ
SELECT CASE (Kp_typ)
       
    ! Type constant
    CASE ("constant")
        ! Aux cellules
        DOM%ETAT%Kp(:,:) = DOM%PHYS%MAT%Kp_ini
        ! Aux faces
        DOM%ETAT%Kp_itp(:,:) = DOM%PHYS%MAT%Kp_ini
        
        
    ! Type special
    CASE ("special")
        ! Aux cellules
        DOM%ETAT%Kp(:,1) = DOM%PHYS%MAT%Kp_ini*(3.0-2.0*cos(DOM%MLG%G_XYZ(:,1)*(10.0/0.2)))
        ! Aux faces
        DOM%ETAT%Kp_itp(:,1) = DOM%PHYS%MAT%Kp_ini*(3.0-2.0*cos(DOM%MLG%G_XYZ(:,1)*(10.0/0.2)))
        ! Idem en Y, Z
        DOM%ETAT%Kp(:,2) = DOM%ETAT%Kp(:,1)
        DOM%ETAT%Kp(:,3) = DOM%ETAT%Kp(:,1)
        DOM%ETAT%Kp_itp(:,2) = DOM%ETAT%Kp_itp(:,1)
        DOM%ETAT%Kp_itp(:,3) = DOM%ETAT%Kp_itp(:,1)
        
    ! Type palier spécial en fonction du temps
    CASE ("palier_temp")
        ! Aux cellules
        DOM%ETAT%Kp(:,1) = DOM%PHYS%MAT%Kp_ini + DOM%PHYS%MAT%Kp_ini*(DOM%NUM%ti/(DOM%NUM%tf-DOM%NUM%t0))*100.0
        ! Aux faces
        DOM%ETAT%Kp_itp(:,1) = DOM%PHYS%MAT%Kp_ini + DOM%PHYS%MAT%Kp_ini*(DOM%NUM%ti/(DOM%NUM%tf-DOM%NUM%t0))*100.0
        
        ! + Limitation faces + cellules
        WHERE ( DOM%ETAT%Kp(:,1) > DOM%PHYS%MAT%Kp_max )
			DOM%ETAT%Kp(:,1)=DOM%PHYS%MAT%Kp_max
            DOM%ETAT%Kp_itp(:,1) = DOM%PHYS%MAT%Kp_max
        END WHERE
        
        ! Idem en Y, Z
        DOM%ETAT%Kp(:,2) = DOM%ETAT%Kp(:,1)
        DOM%ETAT%Kp(:,3) = DOM%ETAT%Kp(:,1)
        DOM%ETAT%Kp_itp(:,2) = DOM%ETAT%Kp_itp(:,1)
        DOM%ETAT%Kp_itp(:,3) = DOM%ETAT%Kp_itp(:,1)
        
        
    ! Type Henderson
    CASE ("Henderson")
        ! Aux cellules
        DOM%ETAT%Kp(:,1) = DOM%PHYS%MAT%Kp_ini * (1.0_dp - DOM%ETAT%alpha_reac(:,1))
        DOM%ETAT%Kp(:,1) = DOM%ETAT%Kp(:,1) + DOM%PHYS%MAT%Kp_fin * DOM%ETAT%alpha_reac(:,1)
        ! Idem en Y, Z
        DOM%ETAT%Kp(:,2) = DOM%ETAT%Kp(:,1)
        DOM%ETAT%Kp(:,3) = DOM%ETAT%Kp(:,1)
        
        ! Aux faces
        ! => Méthode par interpolation plus stable
        CALL interp_fac( DOM%ETAT%Kp(:,1), &        ! Données aux cel.
                    DOM%ETAT%Kp_itp(:,1), &         ! Interpolées aux faces
                    DOM%MLG%nfac, &                 ! Nombre de faces
                    DOM%MLG%ncel, &                 ! Nombre de cellules
                    DOM%MLG%nsom, &                 ! Nombre de sommets
                    DOM%MLG%FAC_ITP(:), &           ! Type interp. faces
                    DOM%MLG%CEL_ITP(:), &           ! Type interp. cellules
                    DOM%MLG%SOM_ITP(:), &           ! Type interp. sommets
                    DOM%MLG%dim_simu )              ! Dimension
                    
        ! + Limitation faces
        WHERE ( DOM%ETAT%Kp_itp(:,1) > DOM%PHYS%MAT%Kp_max )
            DOM%ETAT%Kp_itp(:,1) = DOM%PHYS%MAT%Kp_max
        END WHERE
        
        ! Idem en Y, Z
        DOM%ETAT%Kp_itp(:,2) = DOM%ETAT%Kp_itp(:,1)
        DOM%ETAT%Kp_itp(:,3) = DOM%ETAT%Kp_itp(:,1)
        
        
    ! Type Kozeny-Carman
    CASE ("Kozeny")
    
        ! Aux cellules
        DOM%ETAT%Kp(:,1) = DOM%PHYS%MAT%Kp_0 * (DOM%ETAT%phi_g(:) ** 3)
        DOM%ETAT%Kp(:,1) = DOM%ETAT%Kp(:,1) / ( (1.0_dp - DOM%ETAT%phi_g(:)) ** 2)
        
        ! + Limitation cellules
        WHERE ( DOM%ETAT%Kp(:,1) > DOM%PHYS%MAT%Kp_max )
            DOM%ETAT%Kp(:,1) = DOM%PHYS%MAT%Kp_max
        END WHERE
        
        ! Idem en Y, Z
        DOM%ETAT%Kp(:,2) = DOM%ETAT%Kp(:,1)
        DOM%ETAT%Kp(:,3) = DOM%ETAT%Kp(:,1)
        
        
        ! Aux faces
        ! => Méthode par interpolation plus stable
        CALL interp_fac( DOM%ETAT%Kp(:,1), &        ! Données aux cel.
            DOM%ETAT%Kp_itp(:,1), &         ! Interpolées aux faces
            DOM%MLG%nfac, &                 ! Nombre de faces
            DOM%MLG%ncel, &                 ! Nombre de cellules
            DOM%MLG%nsom, &                 ! Nombre de sommets
            DOM%MLG%FAC_ITP(:), &           ! Type interp. faces
            DOM%MLG%CEL_ITP(:), &           ! Type interp. cellules
            DOM%MLG%SOM_ITP(:), &           ! Type interp. sommets
            DOM%MLG%dim_simu )              ! Dimension
                    
        ! + Limitation faces
        WHERE ( DOM%ETAT%Kp_itp(:,:) > DOM%PHYS%MAT%Kp_max )
            DOM%ETAT%Kp_itp(:,:) = DOM%PHYS%MAT%Kp_max
        END WHERE
        
        
    ! Type Kozeny-Carman orthotrope
    CASE ("Kozeny-ortho")
    
        ! Aux cellules
        DOM%ETAT%Kp(:,1) = DOM%PHYS%MAT%Kp_x * (DOM%ETAT%phi_g(:) ** 3)
        DOM%ETAT%Kp(:,1) = DOM%ETAT%Kp(:,1) / ( (1.0_dp - DOM%ETAT%phi_g(:)) ** 2)
        
        ! Proportion en Y
        DOM%ETAT%Kp(:,2) = DOM%ETAT%Kp(:,1) * DOM%PHYS%MAT%Kp_y / DOM%PHYS%MAT%Kp_x
        ! RAZ en Z
        DOM%ETAT%Kp(:,3) = DOM%ETAT%Kp(:,1) * DOM%PHYS%MAT%Kp_z / DOM%PHYS%MAT%Kp_x
        
        
        ! + Limitation cellules
        WHERE ( DOM%ETAT%Kp(:,:) > DOM%PHYS%MAT%Kp_max )
            DOM%ETAT%Kp(:,:) = DOM%PHYS%MAT%Kp_max
        END WHERE
        
        ! Aux faces
        ! => Méthode par interpolation plus stable
        CALL interp_fac( DOM%ETAT%Kp(:,1), &        ! Données aux cel.
            DOM%ETAT%Kp_itp(:,1), &         ! Interpolées aux faces
            DOM%MLG%nfac, &                 ! Nombre de faces
            DOM%MLG%ncel, &                 ! Nombre de cellules
            DOM%MLG%nsom, &                 ! Nombre de sommets
            DOM%MLG%FAC_ITP(:), &           ! Type interp. faces
            DOM%MLG%CEL_ITP(:), &           ! Type interp. cellules
            DOM%MLG%SOM_ITP(:), &           ! Type interp. sommets
            DOM%MLG%dim_simu )              ! Dimension
        CALL interp_fac( DOM%ETAT%Kp(:,2), &        ! Données aux cel.
            DOM%ETAT%Kp_itp(:,2), &         ! Interpolées aux faces
            DOM%MLG%nfac, &                 ! Nombre de faces
            DOM%MLG%ncel, &                 ! Nombre de cellules
            DOM%MLG%nsom, &                 ! Nombre de sommets
            DOM%MLG%FAC_ITP(:), &           ! Type interp. faces
            DOM%MLG%CEL_ITP(:), &           ! Type interp. cellules
            DOM%MLG%SOM_ITP(:), &           ! Type interp. sommets
            DOM%MLG%dim_simu )              ! Dimension
        CALL interp_fac( DOM%ETAT%Kp(:,3), &        ! Données aux cel.
            DOM%ETAT%Kp_itp(:,3), &         ! Interpolées aux faces
            DOM%MLG%nfac, &                 ! Nombre de faces
            DOM%MLG%ncel, &                 ! Nombre de cellules
            DOM%MLG%nsom, &                 ! Nombre de sommets
            DOM%MLG%FAC_ITP(:), &           ! Type interp. faces
            DOM%MLG%CEL_ITP(:), &           ! Type interp. cellules
            DOM%MLG%SOM_ITP(:), &           ! Type interp. sommets
            DOM%MLG%dim_simu )              ! Dimension        
        
        ! + Limitation faces
        WHERE ( DOM%ETAT%Kp_itp(:,:) > DOM%PHYS%MAT%Kp_max )
            DOM%ETAT%Kp_itp(:,:) = DOM%PHYS%MAT%Kp_max
        END WHERE
        
    ! Autres cas ...
    CASE DEFAULT
        CALL print_err('Les methodes de calcul de mu ne sont pas reconnues',1)
        
END SELECT
!
END SUBROUTINE prop_Kp
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul des proprietes radiatives (alpha , eps)
SUBROUTINE prop_rad( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: nesp
CHARACTER (len=32) :: typ_esp
INTEGER(IP) :: iesp
!
! Recuperations
nesp = DOM%PHYS%nesp
!
! Mise a zero
DOM%ETAT%eps_rad(:) = 0.0_dp
DOM%ETAT%alpha_rad(:) = 0.0_dp
!
! Boucle sur les espèces
DO iesp = 1,nesp
    
    ! Si l'espèce est un solide
    typ_esp = DOM%PHYS%ESP(iesp)%typ_esp
    IF (typ_esp == 'solide') THEN
        
        ! eps <= somme( eps_i * phi_i )
        DOM%ETAT%eps_rad(:) = DOM%ETAT%eps_rad(:) + &
                         ( DOM%ETAT%phi_itp(:,iesp) * DOM%PHYS%ESP(iesp)%eps )
        ! alpha <= somme( alpha_i * phi_i )
        DOM%ETAT%alpha_rad(:) = DOM%ETAT%alpha_rad(:) + &
                         ( DOM%ETAT%phi_itp(:,iesp) * DOM%PHYS%ESP(iesp)%alpha )
    END IF
END DO
!
IF (ANY( DOM%ETAT%phi_s_itp(:) < 0.0_dp )) THEN
    CALL print_err('phi_s_itp semble contenir des valeurs negatives')
END IF
!
! alpha <= alpha / phi_s_itp
WHERE (DOM%ETAT%phi_s_itp(:) > eps_min)
    DOM%ETAT%alpha_rad = DOM%ETAT%alpha_rad / DOM%ETAT%phi_s_itp
    DOM%ETAT%eps_rad = DOM%ETAT%eps_rad / DOM%ETAT%phi_s_itp
!
! Si phi_s_itp trop faible : eps=alpha=0
ELSE WHERE
    DOM%ETAT%alpha_rad = 0.0_dp
    DOM%ETAT%eps_rad = 0.0_dp
END WHERE
!
END SUBROUTINE prop_rad
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul des vitesses de Darcy
SUBROUTINE prop_vg( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
!
! Mise a zero
DOM%ETAT%v_g_itp(:,:) = 0.0_dp
!
!
!Aux faces
DOM%ETAT%v_g_itp(:,:) = - DOM%ETAT%Kp_itp(:,:) / &
                        SPREAD(DOM%ETAT%mu_itp,2,3) * &
                        DOM%ETAT%grad_P_itp(:,:)
!
END SUBROUTINE prop_vg
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul des Pressions
SUBROUTINE prop_P( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
!
! Mise a zero
DOM%ETAT%P(:) = 0.0_dp
DOM%ETAT%P_itp(:) = 0.0_dp
!

! PRESSIONS, GAZ PARFAITS
! MAJ des pressions a rho_g et T donné
DOM%ETAT%P(:) = DOM%ETAT%rho_g(:) *  R_GP / DOM%ETAT%M(:) * DOM%ETAT%T(:)

! Interpolation et calcul des gradients pour pression
CALL interp_P(  DOM%ETAT%P, &              ! Pressions aux cel.
                DOM%MLG%ncel, &            ! Nombre de cel.
                DOM%ETAT%P_itp, &          ! Interpolées aux faces
                DOM%ETAT%grad_P_itp, &     ! Gradients aux faces
                DOM%MLG%nfac, &            ! Nombre de faces
                DOM%MLG, &                 ! Type données maillage
                DOM%CDT )                  ! Type données conditions limites
                
!
END SUBROUTINE prop_P
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul des criteres CFL Et Fourrier
!> \warning CFL valable en 2D plan et axi seulement 
SUBROUTINE prop_stab( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
REAL(DP), DIMENSION(DOM%MLG%ncel,3) :: v_g
!REAL(DP), ALLOCATABLE, DIMENSION(:) :: celGH, celHH
!
! Mise a zero
DOM%ETAT%Fo(:) = 0.0_dp
DOM%ETAT%CFL(:) = 0.0_dp
!
!Nombre de Fourier aux cellules
! Fo = (k/rho/Cp) * dt / dL^2
IF (DOM%MLG%dim_simu == '3D') THEN
    ! Formule 1/dx^2 + 1/dy^2 + 1/dz^2 = (A/2V)^2 - P/(2V)
    ! Valable uniquement en 3D
    DOM%ETAT%Fo(:) = MAXVAL(DOM%ETAT%k,2) / DOM%ETAT%rho / DOM%ETAT%Cp * DOM%NUM%dt &
                        * (1.0_dp / (DOM%MLG%dL_min ** 2) - DOM%MLG%PERIM/(2.0_dp*DOM%MLG%VOLUME))

ELSE
    ! Formule 1/dx^2 + 1/dy^2 = (A/2V)^2 - 2/V
    ! Valable uniquement en 2D
    DOM%ETAT%Fo(:) = MAXVAL(DOM%ETAT%k,2) / DOM%ETAT%rho / DOM%ETAT%Cp * DOM%NUM%dt &
                        * (1.0_dp / (DOM%MLG%dL_min ** 2) - 2.0_dp/DOM%MLG%VOLUME)
ENDIF

! CFL aux cellules
IF ( DOM%NUM%ADVECTION == 'on' ) THEN

    CALL grad_cel( DOM%ETAT%P(:), v_g, DOM%MLG%ncel, DOM%MLG%CEL_ITP, DOM%MLG%dim_simu )

    v_g(:,:) = - DOM%ETAT%Kp(:,:) / &
                SPREAD(DOM%ETAT%mu,2,3) * &
                v_g(:,:)

    DOM%ETAT%CFL(:) = SQRT( v_g(:,1)**2 + v_g(:,2)**2 + v_g(:,3)**2 ) / &
                    DOM%MLG%dL_min 
    DOM%ETAT%CFL(:) = DOM%ETAT%CFL(:) * DOM%NUM%dt / DOM%ETAT%phi_g
END IF

! NB :                     
!CFL = u * dt / dx
!avec u = vitesse reelle des gaz = v_g / phi_g
! u = vitesse de transport des quantites phi_i * rho_i


END SUBROUTINE prop_stab
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul des matrices de rigidite sur l'ensemble du domaine
!> Matrice de rigidite = ( C_11, C_22, C_12, C_66 )
SUBROUTINE prop_meca( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
CHARACTER (len=32) :: meca_typ
!
! Mise a zero
DOM%ETAT%Crig(:,:) = 0.0_dp
DOM%ETAT%Crig_itp(:,:) = 0.0_dp
!
!
! Choix de la méthode de calcul : isotrope, transeverse
meca_typ = DOM%PHYS%MAT%meca_typ
SELECT CASE (meca_typ)

    ! Type isotrope
    CASE ("isotrope")        
        
        ! Aux cellules
        ! C_11 = E / (1-P**2)
        DOM%ETAT%Crig(:,1) = DOM%PHYS%MAT%E_iso / ( 1.0_dp - DOM%PHYS%MAT%P_iso**2 )
        ! C_22 = E / (1-P**2)
        DOM%ETAT%Crig(:,2) = DOM%ETAT%Crig(:,1)
        ! C_12 = E * P/ (1-P**2)
        DOM%ETAT%Crig(:,3) = DOM%ETAT%Crig(:,1) * DOM%PHYS%MAT%P_iso
        ! C_33 = E / (1+P)
        DOM%ETAT%Crig(:,4) = DOM%PHYS%MAT%E_iso / ( 1.0_dp + DOM%PHYS%MAT%P_iso )
        
        
        ! Aux faces
        ! C_11 = E / (1-P**2)
        DOM%ETAT%Crig_itp(:,1) = DOM%PHYS%MAT%E_iso / ( 1.0_dp - DOM%PHYS%MAT%P_iso**2 )
        ! C_22 = E / (1-P**2)
        DOM%ETAT%Crig_itp(:,2) = DOM%ETAT%Crig_itp(:,1)
        ! C_12 = E * P/ (1-P**2)
        DOM%ETAT%Crig_itp(:,3) = DOM%ETAT%Crig_itp(:,1) * DOM%PHYS%MAT%P_iso
        ! C_33 = E / (1+P)
        DOM%ETAT%Crig_itp(:,4) = DOM%PHYS%MAT%E_iso / ( 1.0_dp + DOM%PHYS%MAT%P_iso )
        
        
    ! Cas orthotrope-transeverse
    CASE ("transverse")
    
        ! Aux cellules
        ! C_11 = E_x ** 2 / (E_x - P_xy**2 * E_y)
        DOM%ETAT%Crig(:,1) = DOM%PHYS%MAT%E_x * DOM%PHYS%MAT%E_x
        DOM%ETAT%Crig(:,1) = DOM%ETAT%Crig(:,1) / ( DOM%PHYS%MAT%E_x - (DOM%PHYS%MAT%P_xy**2 * DOM%PHYS%MAT%E_y) )
        ! C_22 = E_x * E_y / (E_x - P_xy**2 * E_y)
        DOM%ETAT%Crig(:,2) = DOM%PHYS%MAT%E_x * DOM%PHYS%MAT%E_y
        DOM%ETAT%Crig(:,2) = DOM%ETAT%Crig(:,2) / ( DOM%PHYS%MAT%E_x - (DOM%PHYS%MAT%P_xy**2 * DOM%PHYS%MAT%E_y) )
        ! C_12 = P_xy * E_x * E_y / (E_x - P_xy**2 * E_y)
        DOM%ETAT%Crig(:,3) = DOM%ETAT%Crig(:,2) * DOM%PHYS%MAT%P_xy
        !C_33 = 2 * G_xy
        DOM%ETAT%Crig(:,4) = 2.0_dp * DOM%PHYS%MAT%G_xy
        
        
        ! Aux faces
        ! C_11 = E_x ** 2 / (E_x - P_xy**2 * E_y)
        DOM%ETAT%Crig_itp(:,1) = DOM%PHYS%MAT%E_x * DOM%PHYS%MAT%E_x
        DOM%ETAT%Crig_itp(:,1) = DOM%ETAT%Crig_itp(:,1) / ( DOM%PHYS%MAT%E_x - (DOM%PHYS%MAT%P_xy**2 * DOM%PHYS%MAT%E_y) )
        ! C_22 = E_x * E_y / (E_x - P_xy**2 * E_y)
        DOM%ETAT%Crig_itp(:,2) = DOM%PHYS%MAT%E_x * DOM%PHYS%MAT%E_y
        DOM%ETAT%Crig_itp(:,2) = DOM%ETAT%Crig_itp(:,2) / ( DOM%PHYS%MAT%E_x - (DOM%PHYS%MAT%P_xy**2 * DOM%PHYS%MAT%E_y) )
        ! C_12 = P_xy * E_x * E_y / (E_x - P_xy**2 * E_y)
        DOM%ETAT%Crig_itp(:,3) = DOM%ETAT%Crig_itp(:,2) * DOM%PHYS%MAT%P_xy
        !C_33 = 2 * G_xy
        DOM%ETAT%Crig_itp(:,4) = 2.0_dp * DOM%PHYS%MAT%G_xy
    
    
    ! Cas par default = erreur
    CASE DEFAULT
        CALL print_err('La methode meca_typ n''est pas reconnue',1)

END SELECT

END SUBROUTINE prop_meca
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_prop
