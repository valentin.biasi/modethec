!> Module de Remise A Zero des variables de flux et de seconds membres
!> pour preparer un nouveau cycle de resolution
MODULE mod_reset
!
USE mod_cst, ONLY : DP
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Remise A Zero des variables pour preparer un nouveau cycle de resolution
SUBROUTINE cyc_reset( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM

! Seconds membres ener et masses
DOM%RSL%dE(:) = 0.0_dp
DOM%RSL%dm_e(:,:) = 0.0_dp

! Flux aux faces
DOM%RSL%FL_cond(:) = 0.0_dp
DOM%RSL%FL_darcy(:,:) = 0.0_dp
DOM%RSL%FL_E_darcy(:) = 0.0_dp

! Variation d'avancement des reactions
DOM%RSL%dalpha(:,:) = 0.0_dp

IF ( DOM%NUM%REACTION == 'on' ) THEN
    DOM%ETAT%alpha_reac0(:,:) = DOM%ETAT%alpha_reac(:,:)
END IF

! RESET des Mat jacobiennes
IF (ALLOCATED( DOM%RSL%JAC_A )) THEN
    DOM%RSL%JAC_A(:,:,:) = 0.0_dp
END IF
IF (ALLOCATED( DOM%RSL%JAC_P )) THEN
    DOM%RSL%JAC_P(:,:) = 0.0_dp
END IF
IF (ALLOCATED( DOM%RSL%JAC_DR )) THEN
    DOM%RSL%JAC_DR(:,:,:) = 0.0_dp
END IF
IF (ALLOCATED( DOM%RSL%JAC_A_stval )) THEN
    DOM%RSL%JAC_A_stval(:) = 0.0_dp
END IF
IF (ALLOCATED( DOM%RSL%JAC_P_stval )) THEN
    DOM%RSL%JAC_P_stval(:) = 0.0_dp
END IF
IF (ALLOCATED( DOM%RSL%JAC_DR_stval )) THEN
    DOM%RSL%JAC_DR_stval(:) = 0.0_dp
END IF

! RESET des Vecteur RHS pour les resol implictes
IF (ALLOCATED( DOM%RSL%F_A )) THEN
    DOM%RSL%F_A(:) = 0.0_dp
END IF
IF (ALLOCATED( DOM%RSL%F_DR )) THEN
    DOM%RSL%F_DR(:) = 0.0_dp
END IF

! RESET des quantitées liées à l'ablation
IF ( DOM%NUM%ABLATION == 'on' ) THEN
    DOM%RSL%FL_abla(:) = 0.0_dp
!~     DOM%ETAT%taux_reg_prec = 0.0_dp
!~     DOM%ETAT%taux_reg = 0.0_dp
END IF
    
!
END SUBROUTINE cyc_reset
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_reset
