!> Calcul des sources reactionnelles
!> en fonction des variables naturelles du système.  
!> \details Pour toules les routines, seules les variables 
!> DOM%ETAT%we, DOM%ETAT%wg et DOM%ETAT%wQ sont modifiees.
MODULE mod_reac
!
USE mod_cst, ONLY : DP, IP, R_GP, eps_min
USE mod_print, ONLY : print_err
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Calcul de l'ensemble des sources reactionnelles
SUBROUTINE cyc_reac( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: ncel
INTEGER(IP) :: ireac, nreac
INTEGER(IP) :: id_R, id_P, id_O2
INTEGER(IP) :: igazP, ngazP
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_gazP
REAL(DP) :: nu_R, nu_P, nu_O2
REAL(DP), ALLOCATABLE, DIMENSION(:) :: nu_gazP
REAL(DP), DIMENSION(DOM%MLG%ncel) :: wR, wP, wG
CHARACTER (len=32) :: atmo
LOGICAL :: activ_reac
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
nreac = DOM%PHYS%nreac
atmo = DOM%PHYS%atmo
!
!
! Remise a zero
DOM%ETAT%w_e(:,:) = 0.0_dp
DOM%ETAT%w_g(:) = 0.0_dp
DOM%ETAT%wQ (:)= 0.0_dp
DOM%RSL%dalpha(:,:) = 0.0_dp

! Boucle sur l'ensemble des reactions
IF (nreac > 0) THEN
DO ireac = 1,nreac

    ! Recuperations pour chaque reaction
    id_R = DOM%PHYS%REAC(ireac)%id_R
    id_P = DOM%PHYS%REAC(ireac)%id_P
    id_O2 = DOM%PHYS%REAC(ireac)%id_O2
    ngazP = DOM%PHYS%REAC(ireac)%ngazP
    
    ! Reaction active si atmo oxidante ...
    activ_reac = .FALSE.
    IF (atmo == 'air') activ_reac = .TRUE.
    IF (atmo == 'O2') activ_reac = .TRUE.
    ! ... ou si atmo inerte + reac sans oxygene
    IF ((atmo == 'vide') .OR. (atmo == 'N2')) THEN
        IF (id_O2==0) activ_reac = .TRUE.
    END IF
    
    ! SI reac peut se faire
    IF (activ_reac) THEN
        
        ! Recuperation des coefficients
        ALLOCATE( ID_gazP(ngazP) )
        ID_gazP = DOM%PHYS%REAC(ireac)%ID_gazP
        nu_R = DOM%PHYS%REAC(ireac)%nu_R
        nu_P = DOM%PHYS%REAC(ireac)%nu_P
        nu_O2 = DOM%PHYS%REAC(ireac)%nu_O2
        ALLOCATE( nu_gazP(ngazP) )
        nu_gazP = DOM%PHYS%REAC(ireac)%nu_gazP
        
        ! Appel à reac_dalpha pour determiner dalpha(:,ireac)
        CALL reac_dalpha( DOM, ireac )
        
        
        ! Terme de consommation du reactif solide
        wR(:) = - DOM%ETAT%rho_rel_0(:,id_R) * DOM%RSL%dalpha(:,ireac)
        ! Et ajout a la prod du constituant
        DOM%ETAT%w_e(:,id_R) = DOM%ETAT%w_e(:,id_R) + wR(:)
        
        ! Terme de production du produit solide
        IF (id_P /= 0) THEN
            wP(:) = - nu_P / nu_R * wR(:)
            ! Et ajout a la prod du constituant
            DOM%ETAT%w_e(:,id_P) = DOM%ETAT%w_e(:,id_P) + wP(:)
        END IF
        
        ! Si gaz produits
        IF (ngazP > 0) THEN
        DO igazP = 1,ngazP
            ! Terme de production du produit gazeux
            wG(:) = - nu_gazP(igazP) / nu_R * wR(:)
            ! Et ajout a la prod du constituant
            DOM%ETAT%w_e(:,ID_gazP(igazP)) = DOM%ETAT%w_e(:,ID_gazP(igazP)) + wG(:)
            !--------------------------------------------------------------!
            ! Ajout a l'ensemble des termes de productions de gaz
            DOM%ETAT%w_g(:) = DOM%ETAT%w_g(:) + wG(:)
        END DO
        END IF
        
        ! Termes de consommation de O2 si présent
        !IF (id_O2 /= 0) THEN
        !    wG(:) = - nu_O2 / nu_R * wR(:)
        !    ! Et ajout a la prod du constituant
        !    DOM%ETAT%w_e(:,id_O2) = DOM%ETAT%w_e(:,id_O2) + wG(:)
        !END IF
        
        ! Ajout des quantites energetiques consommees (Q>0 endoth, Q<0 exoth)
        DOM%ETAT%wQ(:) = DOM%ETAT%wQ(:) + wR(:) * DOM%PHYS%REAC(ireac)%Q
        
        ! Desallocations
        DEALLOCATE( ID_gazP )
        DEALLOCATE( nu_gazP )
        
    END IF
    
END DO
END IF
!
END SUBROUTINE cyc_reac
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul de l'ensemble des dalpha (= dalpha/dt) de reaction
SUBROUTINE reac_dalpha( DOM, ireac )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: ireac
REAL(DP), DIMENSION(DOM%MLG%ncel) :: k_Arrh
!INTEGER(IP) :: id_O2
LOGICAL, DIMENSION(DOM%MLG%ncel) :: var_alpha_max
INTEGER(IP) :: icel, ncel
REAL(DP) :: alpha_trans_hend
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
!

! Coefficient d'Arrhenius  = A * exp(-EA/(RT))
k_Arrh = DOM%PHYS%REAC(ireac)%A * EXP( -DOM%PHYS%REAC(ireac)%E_A / R_GP / DOM%ETAT%T )

DOM%RSL%dalpha(:,ireac) = k_Arrh * ((1.0_dp-DOM%ETAT%alpha_reac(:,ireac))**DOM%PHYS%REAC(ireac)%n)

! Cas particulier : donnees en 'dur' dans le code
! Cas test de Henderson 1987
IF (DOM%PHYS%REAC(ireac)%nom_reac == 'Henderson_test') THEN
    
    CALL print_err('Modele reaction Henderson actif')
    ! dalpha *= (1-nu_char)^(n-1)
    DOM%RSL%dalpha(:,ireac) = DOM%RSL%dalpha(:,ireac) * &
        (1.0_dp-DOM%PHYS%REAC(ireac)%nu_P/DOM%PHYS%REAC(ireac)%nu_R) ** &
        (DOM%PHYS%REAC(ireac)%n-1.0_dp)

    ! Puis réaction secondaire de Henderson ---------------------------!
    
    ! alpha de transition tel que m/m0 = 0.91 -> Voir publi Henderson
    alpha_trans_hend = (1.0_dp - 0.91_dp)/(1.0_dp - DOM%PHYS%REAC(ireac)%nu_P/DOM%PHYS%REAC(ireac)%nu_R)
    
    WHERE (DOM%ETAT%alpha_reac(:,ireac) >= alpha_trans_hend)
        k_Arrh = 8.17e18_dp * EXP( -DOM%PHYS%REAC(ireac)%E_A / R_GP / DOM%ETAT%T ) ! Henderson 1987
        !k_Arrh = 8.17e18_dp * EXP( -3.54e5 / R_GP / DOM%ETAT%T ) ! Luo 2007
        
        ! Attention Luo 2007 n=6.50 mais henderson n=6.30
        DOM%RSL%dalpha(:,ireac) = k_Arrh * ((1.0_dp-DOM%ETAT%alpha_reac(:,ireac))**6.30_dp)
        
        DOM%RSL%dalpha(:,ireac) = DOM%RSL%dalpha(:,ireac) * &
        (1.0_dp-DOM%PHYS%REAC(ireac)%nu_P/DOM%PHYS%REAC(ireac)%nu_R) ** &
        (6.30_dp-1.0_dp)
    END WHERE

END IF

DO icel = 1,ncel
    
    IF (DOM%NUM%ti >= 115.0_dp) THEN
        !PRINT*, DOM%RSL%dalpha(icel,ireac)
    END IF
    
    IF (DOM%RSL%dalpha(icel,ireac) /= DOM%RSL%dalpha(icel,ireac)) THEN
        !PRINT*, 'DTC DTC DTC'
        DOM%RSL%dalpha(icel,ireac) = 0.0_dp
    END IF
    
    !IF (DOM%RSL%dalpha(icel,ireac) >= 0.0) THEN
    !    IF (DOM%RSL%dalpha(icel,ireac) <= 0.0) THEN
    !    PRINT*, 'DTC DTC DTC'
    !    END IF
    !END IF

END DO

! Termes avec O2 si présent
!id_O2 = DOM%PHYS%REAC(ireac)%id_O2
!IF (id_O2 /= 0) THEN
    ! dalpha += ((phi*rho)_O2/(phi0*rho0)_O2) ^ n_O2
!    DOM%RSL%dalpha(:,ireac) = DOM%RSL%dalpha(:,ireac) * &
!        ( (DOM%ETAT%phi(:,id_O2)*DOM%ETAT%rho_e(:,id_O2)) / &
!          (DOM%CDT%INI%phi_ini(id_O2)*DOM%CDT%INI%rho_ini(id_O2)) ) ** &
!          DOM%PHYS%REAC(ireac)%n_O2
!END IF


! Enfin verification de la variation de alpha par par pas de temps
var_alpha_max =  (ABS(DOM%RSL%dalpha(:,ireac))) > (1.0_dp - DOM%ETAT%alpha_reac0(:,ireac)) * &
                                                    DOM%NUM%delta_max / DOM%NUM%dt
IF (ANY(var_alpha_max)) THEN
    CALL print_err('La variation de quantite alpha reactionnel est depasse')
    CALL print_err('Veuillez modifier la valeur de dt ou la valeur de delta_max')
    ! Et limitation
    DO icel = 1,ncel
        IF (var_alpha_max(icel)) THEN
        ! Variation max atteinte        
        DOM%RSL%dalpha(icel,ireac) = SIGN(1.0_dp,DOM%RSL%dalpha(icel,ireac)) * &
                        MIN( ABS(DOM%RSL%dalpha(icel,ireac)), &
                        (1.0_dp - DOM%ETAT%alpha_reac0(icel,ireac)) * DOM%NUM%delta_max / DOM%NUM%dt )
        END IF
    END DO
END IF

END SUBROUTINE reac_dalpha
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_reac
