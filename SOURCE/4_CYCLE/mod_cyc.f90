!> Module pour routine cyc qui pilote la resolution en temps
MODULE mod_cyc
!
USE mod_print, ONLY : print_cyc_ini, print_cyc_end, print_expt
USE mod_solve, ONLY : cyc_solve
USE mod_expt, ONLY : expt
USE mod_expt_end, ONLY : expt_end
USE mod_expt_hdf5, ONLY : expt_hdf5, expt_backup_hdf5, expt_hdf5_end, expt_hdf5_backup_end
USE mod_solve_C, ONLY : solve_C_end
USE mod_print, ONLY : print_err
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Routine cyc qui pilote la resolution en temps
SUBROUTINE cyc( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) ::iiter,niter

!
!---- Récupérations ---------------------------------------------------!
niter = DOM%NUM%niter

!---- Boucle sur l'ensemble des iterations temporelles ----------------!
DO iiter = 1,niter
    
    ! Ecritue de l'it. courante
    DOM%NUM%iiter = iiter
        
        ! Print ecran debut de cycle
        IF (MODULO(iiter,DOM%SUI%iiter_prt) == 0) THEN
            CALL print_cyc_ini( DOM )
        END IF

        
        !--------------------------------------------------------------!
        !---- RESOLUTION AU PAS DE TEMPS DONNE ------------------------!
        CALL cyc_solve( DOM )
        !--------------------------------------------------------------!

        
        ! Avance du temps courant
        DOM%NUM%ti = DOM%NUM%ti + DOM%NUM%dt
        
        ! Export des variables dans le format demande
        IF (DOM%EXPT%export == 1) THEN
            IF (MODULO(iiter,DOM%EXPT%iiter_exp) == 0) THEN
                CALL expt( DOM )
            END IF
        END IF
        
        ! Export des donnees globales dans le fichier hdf5
        IF (DOM%SUI%file_hdf5 == 1) THEN
            IF (MODULO(iiter,DOM%SUI%iiter_hdf5) == 0) THEN
                CALL expt_hdf5( DOM )
            END IF
        END IF

        ! Export des donnees backup dans le fichier backup hdf5
        IF (DOM%SUI%backup == 1) THEN
            IF (MODULO(iiter,DOM%SUI%iiter_backup) == 0) THEN
                CALL expt_backup_hdf5( DOM )
            END IF
        END IF

        ! Print ecran en fin de cycle
        IF (MODULO(iiter,DOM%SUI%iiter_prt) == 0) THEN
            CALL print_cyc_end( DOM )
        END IF

END DO
!----------------------------------------------------------------------!
!
! Finalisation des échanges cwipi
IF ( DOM%NUM%COUPLAGE == 'on' ) THEN
    CALL solve_C_end( DOM )
END IF

! Finalisation des exports volumiques
IF (DOM%EXPT%export == 1) THEN
    CALL print_expt( DOM )
    CALL expt_end( DOM )
END IF

! Finalisation des exports backup HDF5
IF (DOM%SUI%backup == 1) THEN
    CALL expt_hdf5_backup_end( DOM )
END IF

! Finalisation des exports HDF5
IF (DOM%SUI%file_hdf5 == 1) THEN
    CALL expt_hdf5_end( DOM )
END IF
IF (DOM%RSL%remail) CALL print_err('Remaillage nécessaire...') ! TEMPORAIRE ! pour ne pas avoir le message a chaque itération ...
!
END SUBROUTINE cyc
!----------------------------------------------------------------------!

!---- Fin du module ---------------------------------------------------!
END MODULE mod_cyc
