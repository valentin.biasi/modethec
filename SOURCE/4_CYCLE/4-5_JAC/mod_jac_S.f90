!> Module pour calcul de la matrice jacobienne des termes STRUCTURES
MODULE mod_jac_S
!
USE mod_cst, ONLY : DP, IP
USE mod_structure, ONLY : cyc_structure_cel
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Matrices jacobiennes des termes STRUCTURES
SUBROUTINE jac_S( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM

! Calcul de la jacobienne pour les termes STRUCTURES par perturbation
CALL jac_S_diff( DOM )

END SUBROUTINE jac_S
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Matrices jacobiennes des termes STRUCTURES : CALCUL PAR PERTURBATION
SUBROUTINE jac_S_diff( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: icel, ncel
INTEGER(IP) :: ifac_adj, nfac_adj
INTEGER(IP) :: jcel
REAL(DP), DIMENSION( DOM%MLG%ncel,3 ) :: U0, dU0
REAL(DP) :: eps_jac
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
nfac_adj = DOM%MLG%nfac_adj_max ! Nombre de faces adjacentes


U0 = DOM%RSL%U

! espilon de variation de quantite pour valeur de perturbation
eps_jac = 1.0e-8_dp

!----------------------------------------------------------------------!
! MATRICE JACOBIENNE DES TERMES STRUCTURES PAR PERTURBATION
!
! JAC_S stocké en forme sparse : JAC_S( ncel , ncel_adj+1, ndim , ndim )
! pour chaque icel :
!
! Sur le 2eme ordre du tableau
! JAC_S(icel,1,:,:) = termes derive par rapport à icel
! JAC_S(icel,2:end,:,:) = termes derive par rapport aux jcel adjacentes
!
! Sur le 3eme ordre du tableau
! JAC_S(icel,:,1:end,1) = termes derivees par rapport a Ux
! JAC_S(icel,:,1:end,2) = termes derivees par rapport a Uy
!


! Boucle sur les cellules
DO icel = 1,ncel

    ! --- Etat de reference
    DOM%RSL%U = U0
    ! Flux de reference pour icel et ses copains
    DOM%RSL%dU(:,:) = 0.0_dp
    DOM%RSL%FL_struct(:,:) = 0.0_dp
    CALL cyc_structure_cel( DOM, icel )
    dU0(:,:) = DOM%RSL%dU(:,:)
    

    !------------------------------------------------------------------!
    ! --- Suivant X ---------------------------------------------------!
    
    ! --- Perturbation sur icel
    DOM%RSL%U(icel,1) = DOM%RSL%U(icel,1) + eps_jac
    ! Flux perturbes pour icel et ses copains
    DOM%RSL%dU(:,:) = 0.0_dp
    DOM%RSL%FL_struct(:,:) = 0.0_dp
    CALL cyc_structure_cel( DOM, icel )
    
    
    ! --- Enregistrement de la deviation pour les cel adjacentes ----- !
    ! Boucle sur les cellules adjacentes
    DO ifac_adj = 1,nfac_adj
        
    ! Indice de la j face adjacente
    jcel = DOM%MLG%CEL_ADJ(icel,ifac_adj+2)
        
    ! On passe les cellules limites
    IF (jcel /= 0) THEN
        
        ! pour le flux x
        DOM%RSL%JAC_S(icel,ifac_adj+1,1,1) = ( DOM%RSL%dU(jcel,1) - dU0(jcel,1) ) / eps_jac
        
        ! et le flux y
        DOM%RSL%JAC_S(icel,ifac_adj+1,1,2) = ( DOM%RSL%dU(jcel,2) - dU0(jcel,2) ) / eps_jac
        
    END IF
    ! Fin boucle cel adjacente
    END DO

    
    ! --- Enregistrement de la deviation a icel ---------------------- !
    ! pour le flux x
    DOM%RSL%JAC_S(icel,1,1,1) = ( DOM%RSL%dU(icel,1) - dU0(icel,1) ) / eps_jac
    
    ! et le flux y
    DOM%RSL%JAC_S(icel,1,1,2) = ( DOM%RSL%dU(icel,2) - dU0(icel,2) ) / eps_jac
    
    
    
    ! --- Retour etat de reference
    DOM%RSL%U = U0
    
    !------------------------------------------------------------------!
    ! --- Suivant Y ---------------------------------------------------!
    
    ! --- Perturbation sur icel
    DOM%RSL%U(icel,2) = DOM%RSL%U(icel,2) + eps_jac
    ! Flux perturbes pour icel et ses copains
    DOM%RSL%dU(:,:) = 0.0_dp
    DOM%RSL%FL_struct(:,:) = 0.0_dp
    CALL cyc_structure_cel( DOM, icel )
    
    
    ! --- Enregistrement de la deviation pour les cel adjacentes ----- !
    ! Boucle sur les cellules adjacentes
    DO ifac_adj = 1,nfac_adj
        
    ! Indice de la j face adjacente
    jcel = DOM%MLG%CEL_ADJ(icel,ifac_adj+2)
        
    ! On passe les cellules limites
    IF (jcel /= 0) THEN
        
        ! pour le flux x
        DOM%RSL%JAC_S(icel,ifac_adj+1,2,1) = ( DOM%RSL%dU(jcel,1) - dU0(jcel,1) ) / eps_jac
        
        ! et le flux y
        DOM%RSL%JAC_S(icel,ifac_adj+1,2,2) = ( DOM%RSL%dU(jcel,2) - dU0(jcel,2) ) / eps_jac

    END IF
    ! Fin boucle cel adjacente
    END DO
    
    
    ! --- Enregistrement de la deviation a icel ---------------------- !
    ! pour le flux x
    DOM%RSL%JAC_S(icel,1,2,1) = ( DOM%RSL%dU(icel,1) - dU0(icel,1) ) / eps_jac
    
    ! et le flux y
    DOM%RSL%JAC_S(icel,1,2,2) = ( DOM%RSL%dU(icel,2) - dU0(icel,2) ) / eps_jac
    

END DO

! Remise a l'etat inital de U = U0
DOM%RSL%U(:,:) = U0(:,:)


END SUBROUTINE jac_S_diff
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_jac_S
