!> Module pour calcul de la matrice jacobienne des termes DIFFUSIFS et REACTIFS
!>
!> JAC_DR stocké en forme sparse : JAC_DR( ncel , ncel_adj+1, nreac+1 )
!> pour chaque icel :
!>
!> Sur le 2eme ordre du tableau
!> JAC_DR(icel,1,:) = termes derive par rapport à icel
!> JAC_DR(icel,2:end,:) = termes derive par rapport aux jcel adjacentes
!>
!> Sur le 3eme ordre du tableau
!> JAC_DR(icel,:,1) = termes de l'equation d'énergie
!> JAC_DR(icel,:,2:end) = termes des bilans de masses
!>
!> Attention, on a négligé les termes extradiagonaux des blocs (à justifier...)
MODULE mod_jac_DR
!
USE mod_cst, ONLY : DP, IP, eps_min, sigmaSB
USE mod_print, ONLY : print_err
USE mod_reac, ONLY : cyc_reac, reac_dalpha
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Matrices jacobiennes des termes DIFFUSIFS-REACTIFS
SUBROUTINE jac_DR( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM

! Calcul de la jacobienne pour les termes diffusifs
CALL jac_D_anal( DOM )

! Calcul de la jacobienne pour les termes reactifs
IF (DOM%NUM%REACTION == 'on') THEN
    CALL jac_R_diff( DOM )
END IF

END SUBROUTINE jac_DR
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Matrices jacobiennes des termes DIFFUSIFS : CALCUL ANALYTIQUE
SUBROUTINE jac_D_anal( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: icel, ncel
INTEGER(IP) :: ifac_adj, nfac_adj
INTEGER(IP) :: jcel
INTEGER(IP) :: fac_ij
INTEGER(IP) :: dir_ij
REAL(DP) :: AIRE_ij, VOLUME_i, VOLUME_j
REAL(DP), DIMENSION(3) :: VFAC_ij, GiGj
REAL(DP) :: n2_GiGj
REAL(DP) :: dFdQ
INTEGER(IP) :: ilim, nlim
CHARACTER (len=32) :: ener_typ
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
nlim = DOM%CDT%nlim


! Boucle sur les cellules
DO icel = 1,ncel
    
    nfac_adj = DOM%MLG%CEL2FAC(icel,2) ! Nombre de faces adjacentes
    
    ! Boucle sur les cellules adjacentes
    DO ifac_adj = 1,nfac_adj
        
        ! Indice de la j face adjacente
        jcel = DOM%MLG%CEL_ADJ(icel,ifac_adj+2)
        ! Indice de la face ij
        fac_ij = ABS( DOM%MLG%CEL2FAC(icel,ifac_adj+2) )
            
        !--------------------------------------------------------------!
        !--- RECUPERATIONS PAR CEL ADJACENTE
        AIRE_ij = DOM%MLG%AIRE(fac_ij)             ! Aire de la face ij
        VFAC_ij = DOM%MLG%VFAC(fac_ij,:)           ! Vecteur normal de la face ij
        dir_ij = DOM%MLG%dir_VFAC(icel,ifac_adj)   ! Direction de vfac par rapport à GiGj
        n2_GiGj = DOM%MLG%n2_G1G2(fac_ij)          ! Norme au carre de GiGj
        VOLUME_i = DOM%MLG%VOLUME(icel)            ! Volume de la cel i
        IF (jcel /= 0) THEN
            GiGj = DOM%MLG%G_XYZ(jcel,:) - DOM%MLG%G_XYZ(icel,:)  ! Veceur Gi -> Gj
            VOLUME_j = DOM%MLG%VOLUME(jcel)            ! Volume de la cel j
        ELSE
            GiGj = DOM%MLG%G1G2(fac_ij,:) ! Vecteur G1H pour condition limite
        END IF


        ! Pour les cellules internes
        IF (jcel /= 0) THEN
        

            !--------------------------------------------------------------!
            !--- Terme diffusif - Bilan énergie - derivée par quantité d'énergie de icel
            ! JAC_DR(icel,1,1) += sum( -1 / (rho_i*Cp_i*VOLUME_i) * k_ij * A_ij /
            !                           \\GiGj\\^2 * (GiGj . VFAC) )
            
            dFdQ = 0.0_dp   ! RAZ de dFdQ
            
            ! Calul de la variable intermediaire dFdQ
            dFdQ = - DOT_PRODUCT( (DOM%ETAT%k_itp(fac_ij,:) * GiGj), REAL(dir_ij,DP)*VFAC_ij )
            dFdQ = dFdQ / n2_GiGj
            dFdQ = dFdQ * AIRE_ij
            dFdQ = dFdQ / (DOM%ETAT%rho(icel) * DOM%ETAT%Cp(icel))
            dFdQ = dFdQ / VOLUME_i
            
            ! Ajout dans JAC_DR(icel,1,1)
            DOM%RSL%JAC_DR(icel,1,1) = DOM%RSL%JAC_DR(icel,1,1) + dFdQ
            
            
            !--------------------------------------------------------------!
            !--- Terme diffusif - Bilan énergie - derivée par quantité d'énergie de jcel
            ! JAC_DR(icel,2:end,1) +=  +1 / (rho_j*Cp_j*VOLUME_j) * k_ij * A_ij /
            !                           \\GiGj\\^2 * (GiGj . VFAC)
            
            dFdQ = 0.0_dp   ! RAZ de dFdQ
            
            ! Calul de la variable intermediaire dFdQ
            dFdQ = DOT_PRODUCT( (DOM%ETAT%k_itp(fac_ij,:) * GiGj), REAL(dir_ij,DP)*VFAC_ij )
            dFdQ = dFdQ / n2_GiGj
            dFdQ = dFdQ * AIRE_ij
            dFdQ = dFdQ / (DOM%ETAT%rho(jcel) * DOM%ETAT%Cp(jcel))
            dFdQ = dFdQ / VOLUME_j
            
            ! Ajout dans JAC_DR(icel,2:end,1)
            DOM%RSL%JAC_DR(icel,ifac_adj+1,1) = DOM%RSL%JAC_DR(icel,ifac_adj+1,1) + dFdQ
            
            
            !--------------------------------------------------------------!
            !--- Terme ........ la suite au prochain épisode
    
    
        ! Pour les cellules limites
        ELSE IF (jcel == 0) THEN
        
            ! Trouve le type de limite correspondante à fac_if
            DO ilim =1,nlim
            IF (ANY( DOM%CDT%LIM(ilim)%ID_FACLIMi(:) == fac_ij )) THEN
            
                IF ( (DOM%CDT%LIM( ilim )%abla_typ == "temp") .AND. ALLOCATED( DOM%ETAT%taux_reg ) ) THEN
                    IF ( DOM%ETAT%taux_reg( fac_ij ) > eps_min ) THEN
                        ! Correction de la jacobienne de type température fixée
                        dFdQ = 0.0_dp   ! RAZ de dFdQ
                        dFdQ = - DOT_PRODUCT( (DOM%ETAT%k_itp(fac_ij,:) * GiGj), VFAC_ij )
                        dFdQ = dFdQ / n2_GiGj
                        dFdQ = dFdQ * AIRE_ij
                        dFdQ = dFdQ / (DOM%ETAT%rho(icel) * DOM%ETAT%Cp(icel))
                        dFdQ = dFdQ / VOLUME_i
                            
                        ! Ajout dans JAC_DR(icel,1,1)
                        DOM%RSL%JAC_DR(icel,1,1) = DOM%RSL%JAC_DR(icel,1,1) + dFdQ
                    END IF
                    
                    
                ELSE
                
                    ! En fonction du type de limite : dérivée différente
                    ener_typ = DOM%CDT%LIM(ilim)%ener_typ
                    SELECT CASE (ener_typ)

                        ! Cas flux imposé
                        CASE ("flux")
                        ! Rien psk
                        ! d(Flux)/d(phV) = 0                    

                        
                        ! Cas température imposée
                        CASE ("temp")
                        ! JAC_DR(icel,1,1) += sum( -1 / (rho_i*Cp_i*VOLUME_i) * k_ij * A_ij /
                        !                     \\GiH\\^2 * (GiH . VFAC) )
                            
                            dFdQ = 0.0_dp   ! RAZ de dFdQ
                            dFdQ = - DOT_PRODUCT( (DOM%ETAT%k_itp(fac_ij,:) * GiGj), VFAC_ij )
                            dFdQ = dFdQ / n2_GiGj
                            dFdQ = dFdQ * AIRE_ij
                            dFdQ = dFdQ / (DOM%ETAT%rho(icel) * DOM%ETAT%Cp(icel))
                            dFdQ = dFdQ / VOLUME_i
                            
                            
                            ! Ajout dans JAC_DR(icel,1,1)
                            DOM%RSL%JAC_DR(icel,1,1) = DOM%RSL%JAC_DR(icel,1,1) + dFdQ
                        
                        
                        ! Cas mixte ou autre avec convection + radiatif
                        ! ATTENTION : h_conv(1) - Calcul incorrect si coeff convectif non-uniforme (calcul couplé)
                        CASE ("mixte","gaussien","nuage","directionnel")                
                        ! JAC_DR(icel,1,1) += sum( -1 / (rho_i*Cp_i*VOLUME_i) * A_ij *
                        !                     ( h + 4*T^3 * sigmaSB * eps_rad)
                        
                            
                        
                            dFdQ = 0.0_dp   ! RAZ de dFdQ
                            dFdQ = - DOM%CDT%LIM(ilim)%h_conv(1)
                            dFdQ = dFdQ - ( 4.0_dp * DOM%ETAT%eps_rad(icel) * sigmaSB * DOM%ETAT%T(icel)**3 )
                            dFdQ = dFdQ * AIRE_ij
                            dFdQ = dFdQ / (DOM%ETAT%rho(icel) * DOM%ETAT%Cp(icel) * VOLUME_i)
                            
                            ! Ajout dans JAC_DR(icel,1,1)
                            DOM%RSL%JAC_DR(icel,1,1) = DOM%RSL%JAC_DR(icel,1,1) + dFdQ
                        

                        ! Cas par défaut: erreur.
                        CASE DEFAULT
                            CALL print_err('Un parametre ener_typ n''est pas correctement renseigne',1)
                            
                    END SELECT
                
                END IF

            ! Fin de conditions limites
            END IF
            END DO


        END IF
    ! Fin boucle cel adjacente
    END DO
    

END DO ! Fin

END SUBROUTINE jac_D_anal
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Matrices jacobiennes des termes REACTIF : CALCUL PAR PERTURBATION
!> \todo utiliser reac_dalpha au lieu de cyc_reac pour les dalpha
SUBROUTINE jac_R_diff( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
REAL(DP):: eps_jac
REAL(DP) :: eps_Q
INTEGER(IP) :: ncel
INTEGER(IP) :: ireac, nreac
REAL(DP), DIMENSION(DOM%MLG%ncel) :: E_0, wQ_0, T_0
REAL(DP), DIMENSION(DOM%MLG%ncel) :: alpha_0, dalpha_0
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
nreac = DOM%PHYS%nreac

! espilon de variation de quantite pour valeur de perturbation
eps_jac = 1.0d-7 !DOM%NUM%dt
!----------------------------------------------------------------------!
! 1ere equation :
! Q = rho*h*VOLUME
! F = wQ*VOLUME
! JAC_DR(:,1,1) += d(wQ) / d(rho*h)

! d(rho*h) pour toutes les cellules
eps_Q = MAXVAL(ABS( DOM%RSL%E )) * eps_jac
eps_Q = eps_Q + 10.0_dp*eps_min ! empeche probleme cas E nul

! Mise en mémoire des variables à modifier
E_0 = DOM%RSL%E
wQ_0 = DOM%ETAT%wQ
T_0 = DOM%ETAT%T

! Perturbation de E
DOM%RSL%E = DOM%RSL%E + eps_Q
! Calcul des nouvelles températures correspondantes a E modifié
! Une evaluation du 1er orde est faite (a Cp constant)
DOM%ETAT%T = T_0 + eps_Q / ( DOM%ETAT%rho * DOM%ETAT%Cp * DOM%MLG%VOLUME )
! NB : Evaluation de T reel n'est pas necessaire


! Evaluation de wQ perturbe avec T modifié
CALL cyc_reac( DOM )

! EVALUATION DE JAC_DR
DOM%RSL%JAC_DR(:,1,1) = DOM%RSL%JAC_DR(:,1,1) + (DOM%ETAT%wQ - wQ_0) * DOM%MLG%VOLUME / eps_Q

! Remise a l'etat initial des variables conservees
DOM%RSL%E = E_0
DOM%ETAT%wQ = wQ_0
DOM%ETAT%T = T_0

!----------------------------------------------------------------------!
! 2eme a nreac+1 colonne :
! Q = alpha_ireac
! F = dalpha_i
! JAC_DR(:,1,2:end) += d(dalpha_i) / d(alpha)

DO ireac = 1,nreac

    ! d(alpha) pour toutes les cellules
    eps_Q = eps_jac + 2.0_dp*eps_min
    
    ! Mise en mémoire des variables à modifier
    alpha_0 = DOM%ETAT%alpha_reac(:,ireac)
    dalpha_0 = DOM%RSL%dalpha(:,ireac)
    
    ! Perturbation de alpha
    DOM%ETAT%alpha_reac(:,ireac) = DOM%ETAT%alpha_reac(:,ireac) + eps_Q
    
    ! Evaluation de dalpha perturbe avec alpha modifié
    CALL reac_dalpha( DOM, ireac )
    !CALL cyc_reac( DOM )
    
    ! EVALUATION DE JAC_R
    DOM%RSL%JAC_DR(:,1,ireac+1) = DOM%RSL%JAC_DR(:,1,ireac+1) + &
                                (DOM%RSL%dalpha(:,ireac) - dalpha_0) / eps_Q
    
    ! Ré-enregestriment des variables a l'etat initial
    DOM%ETAT%alpha_reac(:,ireac) = alpha_0
    DOM%RSL%dalpha(:,ireac) = dalpha_0

END DO

END SUBROUTINE jac_R_diff
!----------------------------------------------------------------------!

!---- Fin du module ---------------------------------------------------!
END MODULE mod_jac_DR
