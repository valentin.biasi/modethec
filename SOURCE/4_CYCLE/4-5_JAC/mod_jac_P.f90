!> Module pour calcul de la matrice jacobienne des termes PRESSION DARCY
!> ATTENTION : CE MODULE EST OBSOLETE ET N'EST PLUS UTILISE !!
MODULE mod_jac_P
!
USE mod_cst, ONLY : DP, IP, eps_min
USE mod_print, ONLY : print_err
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Matrices jacobiennes des termes PRESSION DARCY : CALCUL ANALYTIQUE
SUBROUTINE jac_P_anal( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: icel, ncel
INTEGER(IP) :: ifac_adj, nfac_adj
INTEGER(IP) :: jcel
INTEGER(IP) :: fac_ij
INTEGER(IP) :: dir_ij
REAL(DP) :: AIRE_ij, VOLUME_i, VOLUME_j
REAL(DP), DIMENSION(3) :: VFAC_ij, GiGj
REAL(DP) :: n2_GiGj
REAL(DP) :: dFdQ
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel

!----------------------------------------------------------------------!
! MATRICE JACOBIENNE ANALYTIQUE DES TERMES PRESSION DARCY
!
! JAC_P stocké en forme sparse : JAC_P( ncel , ncel_adj+1 )
! pour chaque icel :
!
! Sur le 2eme ordre du tableau
! JAC_A(icel,1) = termes derive par rapport à icel
! JAC_A(icel,2:end) = termes derive par rapport aux jcel adjacentes


! Boucle sur les cellules
DO icel = 1,ncel
    
    nfac_adj = DOM%MLG%CEL2FAC(icel,2) ! Nombre de faces adjacentes
    
    ! Boucle sur les cellules adjacentes
    DO ifac_adj = 1,nfac_adj
        
    ! Indice de la j face adjacente
    jcel = DOM%MLG%CEL_ADJ(icel,ifac_adj+2)
    ! Indice de la face ij
    fac_ij = ABS( DOM%MLG%CEL2FAC(icel,ifac_adj+2) )
        
    ! On passe les cellules limites
    IF (jcel /= 0) THEN
        
        !--------------------------------------------------------------!
        !--- RECUPERATIONS PAR CEL ADJACENTE
        AIRE_ij = DOM%MLG%AIRE(fac_ij)             ! Aire de la face ij
        VOLUME_i = DOM%MLG%VOLUME(icel)            ! Volume de la cel i
        VOLUME_j = DOM%MLG%VOLUME(jcel)            ! Volume de la cel j
        VFAC_ij = DOM%MLG%VFAC(fac_ij,:)           ! Vecteur normal de la face ij
        dir_ij = DOM%MLG%dir_VFAC(icel,ifac_adj)   ! Direction de vfac par rapport à GiGj
        GiGj = DOM%MLG%G_XYZ(jcel,:) - DOM%MLG%G_XYZ(icel,:)  ! Veceur Gi -> Gj
        n2_GiGj = DOM%MLG%n2_G1G2(fac_ij)          ! Norme au carre de GiGj
        
        
        !--------------------------------------------------------------!
        !--- Terme distribution pression - derivée par quantité de pression de icel
        ! JAC_P(icel,1) += - P_i/(V_i*phi_g_i) * sum(K_ij / mu_ij /||MiMj||^2 * MiMj . VFAC * AIRE_ij)
        dFdQ = 0.0_dp   ! RAZ de dFdQ
        
        ! Calul de la variable intermediaire dFdQ
        dFdQ = - DOT_PRODUCT( (DOM%ETAT%Kp_itp(fac_ij,:) * GiGj), REAL(dir_ij,DP)*VFAC_ij )
        dFdQ = dFdQ / n2_GiGj
        dFdQ = dFdQ * AIRE_ij
        dFdQ = dFdQ / DOM%ETAT%mu_itp(fac_ij) 
        dFdQ = dFdQ / DOM%ETAT%phi_g(icel)
        dFdQ = dFdQ * DOM%ETAT%P(icel)
        dFdQ = dFdQ / VOLUME_i
        
        ! Ajout dans JAC_P(icel,1)
        DOM%RSL%JAC_P(icel,1) = DOM%RSL%JAC_P(icel,1) + dFdQ
        
        
        !--------------------------------------------------------------!
        !--- Terme distribution pression - derivée par quantité de pression de jcel
        ! JAC_P(icel,2:end) += + P_i/(V_i*phi_g_i) * K_ij / mu_ij /||MiMj||^2 * MiMj . VFAC * AIRE_ij
        DOM%RSL%JAC_P(icel,1+ifac_adj) = DOM%RSL%JAC_P(icel,1+ifac_adj) - dFdQ
        
        
        !--------------------------------------------------------------!
        !--- Terme ........ la suite au prochain épisode
    
    
    END IF
    ! Fin boucle cel adjacente
    END DO
    

END DO ! Fin


END SUBROUTINE jac_P_anal
!----------------------------------------------------------------------!

!---- Fin du module ---------------------------------------------------!
END MODULE mod_jac_P
