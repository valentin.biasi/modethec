!> Module pour calcul de la matrice jacobienne des termes ADVECTIFS
MODULE mod_jac_A
!
USE mod_cst, ONLY : DP, IP, eps_min, R_GP
USE mod_print, ONLY : print_err
USE mod_advection, ONLY : cyc_advection
USE mod_update, ONLY : cyc_update_A
USE mod_prop, ONLY : prop_P, prop_vg, cyc_prop_A
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Matrices jacobiennes des termes ADVECTION
SUBROUTINE jac_A( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM

! Au choix ...

! Calcul de la jacobienne pour les termes ADVECTION par perturbation
!CALL jac_A_diff( DOM )

! Calcul de la jacobienne pour les termes ADVECTION analytique
CALL jac_A_new( DOM )

END SUBROUTINE jac_A
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Matrices jacobiennes des termes ADVECTIFS : CALCUL ANALYTIQUE 
!>todo Utilisation d'un schéma upwind ou autre
SUBROUTINE jac_A_new( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: icel, ncel
INTEGER(IP) :: igaz, ngaz
INTEGER(IP) :: ifac_adj, nfac_adj
INTEGER(IP) :: jcel
INTEGER(IP) :: fac_ij
INTEGER(IP) :: dir_ij
REAL(DP) :: AIRE_ij, VOLUME_i, VOLUME_j
REAL(DP), DIMENSION(3) :: VFAC_ij, GiGj
REAL(DP) :: n2_GiGj
REAL(DP) :: dFdQ
REAL(DP) :: A, B, fx
INTEGER(IP) :: ilim, nlim
CHARACTER (len=32) :: masse_typ
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
ngaz = DOM%PHYS%ngaz
nlim = DOM%CDT%nlim

!----------------------------------------------------------------------!
! MATRICE JACOBIENNE ANALYTIQUE DES TERMES ADVECTIFS
!
! JAC_A stocké en forme sparse : JAC_A( ncel , ncel_adj+1, ngaz )
! pour chaque icel :
!
! Sur le 2eme ordre du tableau
! JAC_A(icel,1,:) = termes derive par rapport à icel
! JAC_A(icel,2:end,:) = termes derive par rapport aux jcel adjacentes
!
! Sur le 3eme ordre du tableau
! JAC_AD(icel,:,1:end) = termes des bilans de masses
!
! Attention, on a négligé les termes extradiagonaux par bloc (pas de diffusion gaz/gaz)


! Boucle sur les cellules
DO icel = 1,ncel
    
    nfac_adj = DOM%MLG%CEL2FAC(icel,2) ! Nombre de faces adjacentes
    
    ! Boucle sur les cellules adjacentes
    DO ifac_adj = 1,nfac_adj
        
    ! Indice de la j cel adjacente
    jcel = DOM%MLG%CEL_ADJ(icel,ifac_adj+2)
    ! Indice de la face ij
    fac_ij = ABS( DOM%MLG%CEL2FAC(icel,ifac_adj+2) )
    
    
    !------------------------------------------------------------------!
    !--- RECUPERATIONS PAR CEL ADJACENTE
    AIRE_ij = DOM%MLG%AIRE(fac_ij)             ! Aire de la face ij
    VOLUME_i = DOM%MLG%VOLUME(icel)            ! Volume de la cel i
    VFAC_ij = DOM%MLG%VFAC(fac_ij,:)           ! Vecteur normal de la face ij
    dir_ij = DOM%MLG%dir_VFAC(icel,ifac_adj)   ! Direction de vfac par rapport à GiGj
    n2_GiGj = DOM%MLG%n2_G1G2(fac_ij)          ! Norme au carre de GiGj
    IF (jcel /= 0) THEN
        GiGj = DOM%MLG%G_XYZ(jcel,:) - DOM%MLG%G_XYZ(icel,:)  ! Veceur Gi -> Gj
        VOLUME_j = DOM%MLG%VOLUME(jcel)            ! Volume de la cel j
    ELSE
        GiGj = DOM%MLG%G1G2(fac_ij,:) ! Vecteur G1H pour condition limite
    END IF


    ! Pour les cellules internes
    IF (jcel /= 0) THEN
        
		!----------------------------------------------------------!
		!--- Terme convectif - Bilan masse - derivée par quantité de masse de icel
		! JAC_A(icel,1,igaz) += ...
		
        fx=0.5_dp !! à coder si jamais l'aspect des cellules change fortement
        
        ! Calul de la variable intermediaire dFdQ
        ! Derivée par rapport à icel
        dFdQ = 0.0_dp   ! RAZ de dFdQ   
        A=0.0_dp            
        A = - fx*AIRE_ij / ( VOLUME_i )
        A = A/(DOM%ETAT%phi_g_itp(fac_ij))
        A = A* ( DOT_PRODUCT(DOM%ETAT%v_g_itp(fac_ij,:),REAL(dir_ij,DP)*VFAC_ij))
        
        B=0.0_dp
        B = - (AIRE_ij/VOLUME_i)
        B = B*(DOM%ETAT%T_itp(fac_ij)*R_GP/DOM%ETAT%M(icel))
        B = B*(DOM%ETAT%rho_g(icel)/(DOM%ETAT%phi_g(icel)*DOM%ETAT%mu_itp(fac_ij)))
        B = B*(DOT_PRODUCT( DOM%ETAT%Kp_itp(fac_ij,:) * GiGj,REAL(dir_ij,DP)*VFAC_ij )/n2_GiGj)
        
        DO igaz = 1,ngaz

			! Ajout dans JAC_AD(icel,1,1:end)
            dFdQ=A+B
			DOM%RSL%JAC_A(icel,1,igaz) = DOM%RSL%JAC_A(icel,1,igaz) + dFdQ

		END DO	

        ! Calul de la variable intermediaire dFdQ
        ! Derivée par rapport à jcel
        dFdQ = 0.0_dp   ! RAZ de dFdQ
        A=0.0_dp                 
        A= - (1.0_dp-fx)*AIRE_ij / ( VOLUME_j )
        A = A/(DOM%ETAT%phi_g_itp(fac_ij))
        A = A * ( DOT_PRODUCT(DOM%ETAT%v_g_itp(fac_ij,:),REAL(dir_ij,DP)*VFAC_ij))
        
        B=0.0_dp
        B = (AIRE_ij/VOLUME_j)
        B = B*(DOM%ETAT%T_itp(fac_ij)*R_GP/DOM%ETAT%M(jcel))
        B = B*(DOM%ETAT%rho_g(jcel)/(DOM%ETAT%phi_g(jcel)*DOM%ETAT%mu_itp(fac_ij)))
        B = B*(DOT_PRODUCT( DOM%ETAT%Kp_itp(fac_ij,:) * GiGj,REAL(dir_ij,DP)*VFAC_ij )/n2_GiGj)
 		
        DO igaz = 1,ngaz
		
        	! Ajout dans JAC_AD(icel,2:end,2:end)
            dFdQ=A+B
			DOM%RSL%JAC_A(icel,ifac_adj+1,igaz) = DOM%RSL%JAC_A(icel,ifac_adj+1,igaz) + dFdQ
			
		END DO
			
        !--------------------------------------------------------------!
        !--- Terme ........ la suite au prochain épisode
    
    
    ! Pour les cellules limites
    ELSE IF (jcel == 0) THEN
    
        ! Touve le type de limite correspondante à fac_if
        DO ilim =1,nlim
        IF (ANY( DOM%CDT%LIM(ilim)%ID_FACLIMi(:) == fac_ij )) THEN
                
            ! En fonction du type de limite : dérivée différente
            masse_typ = DOM%CDT%LIM(ilim)%masse_typ
            SELECT CASE (masse_typ)

                    ! Cas debit imposé
                    CASE ("debit")
                    ! Rien psk
                    ! d(Debit)/d(phV) = 0

                    
                    ! Cas pression imposée
                    ! ATTENTION : Pimp(1) - Calcul incorrect si pression non-uniforme (calcul couplé)
                    CASE ("pression")
                    ! JAC_DR(icel,1,1) += ...
                        
                        A=0.0_dp
                        A = DOT_PRODUCT( DOM%ETAT%Kp_itp(fac_ij,:) * GiGj,VFAC_ij )
                        A = A / n2_GiGj
                        A = A * AIRE_ij
                        A = A*fx/(DOM%ETAT%phi_g_itp(fac_ij)*VOLUME_i )
                        A = A / DOM%ETAT%mu_itp(fac_ij)
                        A = A * ( DOM%CDT%LIM(ilim)%Pimp(1) - DOM%ETAT%P(icel) )
						
                        B=0.0_dp
                        B = -(AIRE_ij/VOLUME_i)
						B = B*(DOM%ETAT%T_itp(fac_ij)*R_GP/DOM%ETAT%M(icel))
						B = B*(DOM%ETAT%rho_g(icel)/(DOM%ETAT%phi_g(icel)*DOM%ETAT%mu_itp(fac_ij)))
						B = B*(DOT_PRODUCT( DOM%ETAT%Kp_itp(fac_ij,:) * GiGj,VFAC_ij )/n2_GiGj)
                        
                        ! Ajout dans JAC_AD(icel,1,1:end)
                        dFdQ=A+B
                        DO igaz = 1,ngaz
                            DOM%RSL%JAC_A(icel,1,igaz) = DOM%RSL%JAC_A(icel,1,igaz) + dFdQ
                        END DO

                    ! Cas par défaut: erreur.
                    CASE DEFAULT
                        CALL print_err('Un parametre masse_typ n''est pas correctement renseigne',1)
                        
            END SELECT

		! Fin de conditions limites
		END IF
		END DO

    END IF
    ! Fin boucle cel adjacente
    END DO

END DO

! Fin
END SUBROUTINE jac_A_new
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Matrices jacobiennes des termes ADVECTION : CALCUL PAR PERTURBATION
SUBROUTINE jac_A_diff( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: icel, ncel
INTEGER(IP) :: ifac_adj, nfac_adj
INTEGER(IP) :: jcel
REAL(DP), DIMENSION( DOM%MLG%ncel,DOM%PHYS%nesp ) :: m0, dm0
REAL(DP) :: eps_jac
INTEGER(IP) :: igaz, ngaz, id_gaz
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
ngaz = DOM%PHYS%ngaz
nfac_adj = DOM%MLG%nfac_adj_max ! Nombre de faces adjacentes

DOM%RSL%dm_e(:,:) = 0.0_dp
DOM%RSL%FL_darcy(:,:) = 0.0_dp
DOM%RSL%FL_E_darcy(:) = 0.0_dp
DOM%RSL%dE(:) = 0.0_dp
CALL cyc_update_A( DOM )
CALL cyc_prop_A( DOM )
! + pression
CALL prop_P( DOM )
! + Vitesse gaz
CALL prop_vg( DOM )
CALL cyc_advection( DOM )

dm0 = DOM%RSL%dm_e
m0 = DOM%RSL%m_e

! espilon de variation de quantite pour valeur de perturbation
eps_jac = 1.0e-8_dp

!----------------------------------------------------------------------!
! MATRICE JACOBIENNE DES TERMES ADVECTION PAR PERTURBATION
!
! JAC_A stocké en forme sparse : JAC_A( ncel , ncel_adj+1, ngaz )
! pour chaque icel :
!
! Sur le 2eme ordre du tableau
! JAC_A(icel,1,:) = termes derive par rapport à icel
! JAC_A(icel,2:end,:) = termes derive par rapport aux jcel adjacentes
!
! Sur le 3eme ordre du tableau
! JAC_AD(icel,:,1:end) = termes des bilans de masses
!
! Attention, on a négligé les termes extradiagonaux par bloc (pas de diffusion gaz/gaz)
!

! Boucle sur les cellules
DO icel = 1,ncel

    ! --- Etat de reference
    ! Flux de reference pour icel et ses copains
    DOM%RSL%dm_e(:,:) = 0.0_dp
    DOM%RSL%dE(:) = 0.0_dp
    DOM%RSL%FL_darcy(:,:) = 0.0_dp
    DOM%RSL%FL_E_darcy(:) = 0.0_dp
	DOM%RSL%m_e = m0
    
    ! --- Perturbation sur icel
    DO igaz = 1,ngaz
        id_gaz = DOM%PHYS%ID_gaz(igaz)
        DOM%RSL%m_e(icel,id_gaz) = DOM%RSL%m_e(icel,id_gaz) + eps_jac
    END DO
    
  ! MAJ
    CALL cyc_update_A( DOM )
    CALL cyc_prop_A( DOM )
    ! + pression
    CALL prop_P( DOM )
    ! + Vitesse gaz
    CALL prop_vg( DOM )

    ! Flux perturbes pour icel et ses copains
    CALL cyc_advection( DOM )
    
	! Pour chaque gaz
	DO igaz = 1,ngaz
		
		! ID du gaz
		id_gaz = DOM%PHYS%ID_gaz(igaz)
		
		!--- Enregistrement de la deviation  par rapport a icel ---!
		DOM%RSL%JAC_A(icel,1,igaz) =  (DOM%RSL%dm_e(icel,id_gaz) - dm0(icel,id_gaz)) / eps_jac
		
    END DO
           
	!--- Enregistrement de la deviation pour les cel adj ------!
	! Boucle sur les cellules adjacentes
	DO ifac_adj = 1,nfac_adj
            
		! Indice de la j face adjacente
		jcel = DOM%MLG%CEL_ADJ(icel,ifac_adj+2)
            
		! On passe les cellules limites
		IF (jcel /= 0) THEN
		
			DOM%RSL%m_e = m0
			DOM%RSL%dm_e(:,:) = 0.0_dp
            DOM%RSL%dE(:) = 0.0_dp
			DOM%RSL%FL_darcy(:,:) = 0.0_dp
			DOM%RSL%FL_E_darcy(:) = 0.0_dp
			
			! --- Perturbation sur icel
			DO igaz = 1,ngaz
				id_gaz = DOM%PHYS%ID_gaz(igaz)
				DOM%RSL%m_e(jcel,id_gaz) = DOM%RSL%m_e(jcel,id_gaz) + eps_jac
			END DO
			
			! MAJ
			CALL cyc_update_A( DOM )
			CALL cyc_prop_A( DOM )
            ! + pression
            CALL prop_P( DOM )
            ! + Vitesse gaz
            CALL prop_vg( DOM )
			! Flux perturbes pour icel et ses copains
			CALL cyc_advection( DOM )
		
		
			! Pour chaque gaz
			DO igaz = 1,ngaz
				
				! ID du gaz
				id_gaz = DOM%PHYS%ID_gaz(igaz)
				
				!--- Enregistrement de la deviation  par rapport a icel ---!
				DOM%RSL%JAC_A(icel,1+ifac_adj,igaz) =  (DOM%RSL%dm_e(icel,id_gaz) - dm0(icel,id_gaz)) / eps_jac
				
			END DO
                 
			
		END IF

    END DO !Fin faces adjacentes

END DO ! Fin icel

! Remise a l'etat inital de m_e = m0
DOM%RSL%m_e(:,:) = m0(:,:)
! Remise a l'etat inital de dm_e = dm0
DOM%RSL%dm_e(:,:) = dm0(:,:)
!
! Fin
END SUBROUTINE jac_A_diff
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_jac_A
