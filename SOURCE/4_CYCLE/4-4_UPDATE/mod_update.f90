!> Calcul des variables naturelles ajustees a t +dt pour le domaine i
MODULE mod_update
!
USE mod_cst, ONLY : DP, IP, tinym, R_GP, eps_min
USE mod_print, ONLY : print_err
USE mod_prop, ONLY : prop_M, prop_rho_e, E_to_T
USE mod_interpolate, ONLY : interp_fac, interp_T
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> MAJ PAR DOMAINE de l'état du système pour la resol DIFFUSION-REACTION
SUBROUTINE cyc_update_DR( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
!
! MAJ des autres variables naturelles au centre des cellules
CALL update_cel_DR( DOM )
!
!! MAJ des autres variables naturelles au centre des faces
CALL update_fac_DR( DOM )
!
!
END SUBROUTINE cyc_update_DR
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> MAJ PAR DOMAINE de l'état du système pour la resol ADVECTION
SUBROUTINE cyc_update_A( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
!
! MAJ des autres variables naturelles au centre des cellules pour la resol ADVECTION
CALL update_cel_A( DOM )
!
!! MAJ des autres variables naturelles au centre des faces pour la resol ADVECTION
CALL update_fac_A( DOM )
!
!
END SUBROUTINE cyc_update_A
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> MAJ PAR DOMAINE de l'état du système DIFFUSION-REACTION
!> Pour les variables naturelles aux cellules
SUBROUTINE update_cel_DR( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: iesp, nesp
INTEGER(IP) :: igaz, ngaz, nsol
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_gaz, ID_sol
REAL(DP) :: M_i
!
!---- Récupérations ---------------------------------------------------!
nesp = DOM%PHYS%nesp
ngaz = DOM%PHYS%ngaz
nsol = DOM%PHYS%nsol
!
!---- Allocations -----------------------------------------------------!
ALLOCATE( ID_gaz( ngaz ) )
ID_gaz = DOM%PHYS%ID_gaz
ALLOCATE( ID_sol( nsol ) )
ID_sol = DOM%PHYS%ID_sol


! Verifications : Masses positives
IF (ANY(DOM%RSL%m_e < 0.0_dp)) THEN
    CALL print_err('Masses negatives dans l''etat du systeme dans solveur DR')
    CALL print_err('Veuillez abaisser le pas de temps',1)
    
    WHERE (DOM%RSL%m_e <= 2.0_dp*tinym)
        DOM%RSL%m_e = 10.0_dp*tinym
    END WHERE
END IF
!
! TEMPERATURES
! MAJ des températures à E donné
CALL E_to_T( DOM )

! Masses totales des cellules
! m =somme( m_i )
DOM%RSL%m = SUM( DOM%RSL%m_e , 2 )

! Masse vol. apparente par cellule
! rho = m / Volume
DOM%ETAT%rho = DOM%RSL%m / DOM%MLG%VOLUME

! Frac. mass. des cellules
! Y_i = m_i / m
DOM%ETAT%Y = DOM%RSL%m_e / SPREAD(DOM%RSL%m,2,nesp)

! Frac. mass. des cellules (gaz et solide)
DOM%ETAT%Y_s = SUM( DOM%ETAT%Y(:,ID_sol) , 2 )
DOM%ETAT%Y_g = SUM( DOM%ETAT%Y(:,ID_gaz) , 2 )

! Evaluation de la masse vol des especes solides
CALL prop_rho_e( DOM )

!~ ! Frac. volumiques des solides
DO iesp = 1,nesp
    IF (DOM%PHYS%ESP(iesp)%typ_esp == 'solide') THEN
        DOM%ETAT%phi(:,iesp) = DOM%RSL%m_e(:,iesp) / &
                                DOM%ETAT%rho_e(:,iesp) / &
                                DOM%MLG%VOLUME(:)        
    END IF
END DO

! Frac. volumiques de solide et gaz
DOM%ETAT%phi_s = SUM( DOM%ETAT%phi(:,ID_sol) , 2 )
! Limitation de la derive de la valeur de phi_s en cas de changement de volume (ablation)
IF ( DOM%NUM%ABLATION == 'on' ) THEN
    IF (maxval(DOM%ETAT%phi_s(:))> 1.0_dp+10.0e-4_dp) THEN        
        CALL print_err("Le solveur ablation induit des valeurs de phi_g negatives : abaissez le pas de temps")        
    END IF
        
    ! Correction de l'erreur numerique
    WHERE (DOM%ETAT%phi_s(:) > 1.0_dp)
        DOM%ETAT%phi_s(:) = 1.0_dp
    END WHERE
END IF

DOM%ETAT%phi_g = 1.0_dp - DOM%ETAT%phi_s

! Masse vol. absolue de la phase gaz
! rho_g = rho * Y_g / phi_g
WHERE (DOM%ETAT%phi_g(:) > 2.0_dp*eps_min)
    DOM%ETAT%rho_g(:) = DOM%ETAT%rho * DOM%ETAT%Y_g / DOM%ETAT%phi_g
END WHERE


! Evaluation de la masse mol du mélange gazeux
IF (ngaz > 0) THEN
    CALL prop_M( DOM )
END IF

! Masse vol des especes gazeuses (voir hypothèses des gaz parfaits, vol. mol cst...)
! rho_i = M_i / M * rho_g
DO igaz = 1,ngaz
    ! Masse mol de l'espece gaz igaz
    iesp = ID_gaz(igaz)
    M_i = DOM%PHYS%ESP(iesp)%M
    
    ! calcul de rho_igaz = rho_g * M_i/M
    WHERE (DOM%ETAT%M(:) > 2.0_dp*eps_min)
        DOM%ETAT%rho_e(:,iesp) = DOM%ETAT%rho_g * M_i / DOM%ETAT%M(:)
    END WHERE
    
END DO


! Frac. volumiques des gaz
! phi_i = m_i / rho_i / V
! Nouvelle version : phi_i = phi_g * m_i / m * M / m_i
DO iesp = 1,nesp
    IF (DOM%PHYS%ESP(iesp)%typ_esp == 'gaz') THEN
        M_i = DOM%PHYS%ESP(iesp)%M
        
        WHERE (DOM%ETAT%rho_e(:,iesp) > 2.0_dp*eps_min)
            !DOM%ETAT%phi(:,iesp) = DOM%RSL%m_e(:,iesp) / &
            !                        DOM%ETAT%rho_e(:,iesp) / &
            !                        DOM%MLG%VOLUME(:)
            
            DOM%ETAT%phi(:,iesp) = DOM%ETAT%phi_g(:) * &
                                    DOM%ETAT%Y(:,iesp) / &
                                    DOM%ETAT%Y_g(:) * &
                                    DOM%ETAT%M(:) / M_i
        END WHERE
    END IF
END DO


END SUBROUTINE update_cel_DR
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> MAJ PAR DOMAINE de l'état du système DIFFUSION-REACTION
!> Pour les variables naturelles aux faces
SUBROUTINE update_fac_DR( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: iesp, nesp
INTEGER(IP) :: ncel
INTEGER(IP) :: nfac
REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: tmp_U, tmp_V
INTEGER(IP) :: ngaz, nsol
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_gaz, ID_sol
!
!---- Récupérations ---------------------------------------------------!
nesp = DOM%PHYS%nesp
ncel = DOM%MLG%ncel
nfac = DOM%MLG%nfac
ngaz = DOM%PHYS%ngaz
nsol = DOM%PHYS%nsol
!
!---- Allocations -----------------------------------------------------!
ALLOCATE( tmp_U( ncel,3 ) )
ALLOCATE( tmp_V( nfac,3 ) )
!
ALLOCATE( ID_gaz( ngaz ) )
ID_gaz = DOM%PHYS%ID_gaz
ALLOCATE( ID_sol( nsol ) )
ID_sol = DOM%PHYS%ID_sol
!
!
! Interpolation et calcul des gradients pour températures
CALL interp_T(  DOM%ETAT%T, &              ! Températures aux cel.
                DOM%MLG%ncel, &            ! Nombre de cel.
                DOM%ETAT%T_itp, &          ! Interpolées aux faces
                DOM%ETAT%grad_T_itp, &     ! Gradients aux faces
                DOM%MLG%nfac, &            ! Nombre de faces
                DOM%MLG, &                 ! Type données maillage
                DOM%CDT )                  ! Type données conditions limites

!~ ! Correction de température en cas d'ablation
!~ IF ( DOM%NUM%ABLATION == 'on' ) THEN
!~     nlim = DOM%CDT%nlim
!~     DO ilim = 1, nlim  
!~            
!~         IF ( DOM%CDT%LIM( ilim )%abla_typ == "temp" ) THEN
!~         
!~             nfac_limi = DOM%CDT%LIM(ilim)%nfac_limi             
!~             DO ifac_lim=1, nfac_limi
!~                 ! Indice absolu de la face
!~                 id_fac = DOM%CDT%LIM(ilim)%ID_FACLIMi( ifac_lim ) 
!~                 ! Indice de la cel
!~                 id_cel = DOM%MLG%FAC2CEL(id_fac,3)   
!~                 DOM%ETAT%T_itp( id_fac ) = min ( DOM%ETAT%T_itp( id_fac ), DOM%PHYS%MAT%T_abla )                      
!~                 DOM%ETAT%grad_T_itp( id_fac,:) = min(DOM%ETAT%grad_T_itp( id_fac,:), ( DOM%PHYS%MAT%T_abla - DOM%ETAT%T(id_cel) )*DOM%MLG%G1H(id_fac,:) / DOM%MLG%n2_G1H(id_fac)) 
!~             END DO
!~         END IF
!~     END DO
!~ END IF
!
! Interpolations pour chaque espèce
DO iesp = 1,nesp

    ! Interpolation et calcul des gradients pour fractions volumiques
    CALL interp_fac(  DOM%ETAT%phi(:,iesp), &       ! Températures aux cel.
                    DOM%ETAT%phi_itp(:,iesp), &     ! Interpolées aux faces
                    nfac, &                         ! Nombre de faces
                    ncel, &                         ! Nombre de cellules
                    DOM%MLG%nsom, &                 ! Nombre de sommets
                    DOM%MLG%FAC_ITP(:), &           ! Type interp. faces
                    DOM%MLG%CEL_ITP(:), &           ! Type interp. cellules
                    DOM%MLG%SOM_ITP(:), &           ! Type interp. sommets
                    DOM%MLG%dim_simu )              ! Dimension

    ! Limitation de phi_itp entre 0 et 1
    WHERE (DOM%ETAT%phi_itp(:,iesp) < 0.0_dp)
        DOM%ETAT%phi_itp(:,iesp) = 0.0_dp
    END WHERE
    WHERE (DOM%ETAT%phi_itp(:,iesp) > 1.0_dp)
        DOM%ETAT%phi_itp(:,iesp) = 1.0_dp
    END WHERE
    
END DO


! Frac. volumiques de solide et gaz aux faces
DOM%ETAT%phi_s_itp = SUM( DOM%ETAT%phi_itp(:,ID_sol) , 2 )
DOM%ETAT%phi_g_itp = SUM( DOM%ETAT%phi_itp(:,ID_gaz) , 2 )
!
!
END SUBROUTINE update_fac_DR
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> MAJ PAR DOMAINE de l'état du système.
!> Pour les variables naturelles aux cellules pour la resol ADVECTION
SUBROUTINE update_cel_A( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: iesp, nesp
INTEGER(IP) :: igaz, ngaz, nsol
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_gaz, ID_sol
REAL(DP) :: M_i
!
!---- Récupérations ---------------------------------------------------!
nesp = DOM%PHYS%nesp
ngaz = DOM%PHYS%ngaz
nsol = DOM%PHYS%nsol
!
!---- Allocations -----------------------------------------------------!
ALLOCATE( ID_gaz( ngaz ) )
ID_gaz = DOM%PHYS%ID_gaz
ALLOCATE( ID_sol( nsol ) )
ID_sol = DOM%PHYS%ID_sol


! Verifications : Masses positives
IF (ANY(DOM%RSL%m_e < 0.0_dp)) THEN
    CALL print_err('Masses negatives dans l''etat du systeme A')
    CALL print_err('Veuillez abaisser le pas de temps')
    ! WHERE (DOM%RSL%m_e <= 2.0_dp*tinym)
    !     DOM%RSL%m_e = 10.0_dp*tinym
    ! END WHERE
END IF

! TEMPERATURES
! MAJ des températures à E donné
CALL E_to_T( DOM )

! Masses totales des cellules
! m =somme( m_i )
DOM%RSL%m = SUM( DOM%RSL%m_e , 2 )

! Masse vol. apparente par cellule
! rho = m / Volume
DOM%ETAT%rho = DOM%RSL%m / DOM%MLG%VOLUME

! Frac. mass. des cellules
! Y_i = m_i / m
DOM%ETAT%Y = DOM%RSL%m_e / SPREAD(DOM%RSL%m,2,nesp)

! Frac. mass. des cellules (gaz et solide)
DOM%ETAT%Y_s = SUM( DOM%ETAT%Y(:,ID_sol) , 2 )
DOM%ETAT%Y_g = SUM( DOM%ETAT%Y(:,ID_gaz) , 2 )


! Masse vol. absolue de la phase gaz
! rho_g = rho * Y_g / phi_g
DOM%ETAT%rho_g(:) = DOM%ETAT%rho * DOM%ETAT%Y_g / DOM%ETAT%phi_g

! Evaluation de la masse mol du mélange gazeux
IF (ngaz > 0) THEN
    CALL prop_M( DOM )
END IF

! Masse vol des especes gazeuses (voir hypothèses des gaz parfaits, vol. mol cst...)
! rho_i = M_i / M * rho_g
DO igaz = 1,ngaz
    ! Masse mol de l'espece gaz igaz
    iesp = ID_gaz(igaz)
    M_i = DOM%PHYS%ESP(iesp)%M
    
    ! calcul de rho_igaz
    DOM%ETAT%rho_e(:,iesp) = DOM%ETAT%rho_g * M_i / DOM%ETAT%M(:)
END DO

! Frac. volumiques des gaz
! phi_i = m_i / rho_i / V
! Nouvelle version : phi_i = phi_g * m_i / m * M / m_i
DO iesp = 1,nesp
    IF (DOM%PHYS%ESP(iesp)%typ_esp == 'gaz') THEN
        M_i = DOM%PHYS%ESP(iesp)%M
        
        WHERE (ABS(DOM%ETAT%Y_g(:)) > 2.0_dp*eps_min)
            !DOM%ETAT%phi(:,iesp) = DOM%RSL%m_e(:,iesp) / &
            !                        DOM%ETAT%rho_e(:,iesp) / &
            !                        DOM%MLG%VOLUME(:)
            
            DOM%ETAT%phi(:,iesp) = DOM%ETAT%phi_g(:) * &
                                    DOM%ETAT%Y(:,iesp) / &
                                    DOM%ETAT%Y_g(:) * &
                                    DOM%ETAT%M(:) / M_i
        END WHERE
    END IF
END DO

END SUBROUTINE update_cel_A
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> MAJ PAR DOMAINE de l'état du système.
!> Pour les variables naturelles aux faces pour la resol ADVECTION
SUBROUTINE update_fac_A( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: iesp, nesp
INTEGER(IP) :: ncel
INTEGER(IP) :: nfac
REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: tmp_U, tmp_V
INTEGER(IP) :: ngaz, nsol
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_gaz, ID_sol
!
!---- Récupérations ---------------------------------------------------!
nesp = DOM%PHYS%nesp
ncel = DOM%MLG%ncel
nfac = DOM%MLG%nfac
ngaz = DOM%PHYS%ngaz
nsol = DOM%PHYS%nsol
!
!---- Allocations -----------------------------------------------------!
ALLOCATE( tmp_U( ncel,3 ) )
ALLOCATE( tmp_V( nfac,3 ) )
!
ALLOCATE( ID_gaz( ngaz ) )
ID_gaz = DOM%PHYS%ID_gaz
ALLOCATE( ID_sol( nsol ) )
ID_sol = DOM%PHYS%ID_sol
!
!
! Interpolation et calcul des gradients pour températures
CALL interp_T(  DOM%ETAT%T, &              ! Températures aux cel.
                DOM%MLG%ncel, &            ! Nombre de cel.
                DOM%ETAT%T_itp, &          ! Interpolées aux faces
                DOM%ETAT%grad_T_itp, &     ! Gradients aux faces
                DOM%MLG%nfac, &            ! Nombre de faces
                DOM%MLG, &                 ! Type données maillage
                DOM%CDT )                  ! Type données conditions limites
!
!
! Interpolations pour chaque espèce
DO iesp = 1,nesp
IF (DOM%PHYS%ESP(iesp)%typ_esp == 'gaz') THEN

    ! Interpolation et calcul des gradients pour fractions volumiques
    CALL interp_fac( DOM%ETAT%phi(:,iesp), & ! Températures aux cel.
            DOM%ETAT%phi_itp(:,iesp), &     ! Interpolées aux faces
            nfac, &                         ! Nombre de faces
            ncel, &                         ! Nombre de cellules
            DOM%MLG%nsom, &                 ! Nombre de sommets
            DOM%MLG%FAC_ITP(:), &           ! Type interp. faces
            DOM%MLG%CEL_ITP(:), &           ! Type interp. cellules
            DOM%MLG%SOM_ITP(:), &           ! Type interp. sommets
            DOM%MLG%dim_simu )              ! Dimension
                    
    ! Limitation de phi_itp entre 0 et 1
    WHERE (DOM%ETAT%phi_itp(:,iesp) < 0.0_dp)
        DOM%ETAT%phi_itp(:,iesp) = 0.0_dp
    END WHERE
    WHERE (DOM%ETAT%phi_itp(:,iesp) > 1.0_dp)
        DOM%ETAT%phi_itp(:,iesp) = 1.0_dp
    END WHERE
                    
END IF
END DO
!
!
END SUBROUTINE update_fac_A
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> MAJ des rho_rel_0 de facon explite, cad en fin d'it. non lineaire
SUBROUTINE update_rho_rel( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: ireac, nreac
INTEGER(IP) :: id_R
!
!---- Récupérations ---------------------------------------------------!
nreac = DOM%PHYS%nreac

! Evaluation des nouveau rho_relatifs(iesp) += dt * w_e(iesp)
WHERE( DOM%ETAT%w_e(:,:) > 0.0_dp )
    DOM%ETAT%rho_rel_0 = DOM%ETAT%rho_rel_0 + DOM%NUM%dt * DOM%ETAT%w_e(:,:)
END WHERE


! Evaluation des nouveaux alpha_reac(ireac) = 1 - rho(iesp)*phi(id_R) / rho_rel_0(id_R)
DO ireac = 1,nreac

    ! Id du reactif solide
    id_R = DOM%PHYS%REAC(ireac)%id_R
    
    ! Evite les divisions par zero
    WHERE( DOM%ETAT%rho_rel_0(:,id_R) > 1.0e-8_dp )
    
        ! Calcul de rho(iesp)*phi(id_R) / rho_rel_0(id_R) seuillé
        DOM%ETAT%alpha_reac(:,ireac) = - DOM%ETAT%phi(:,id_R) * DOM%ETAT%rho_e(:,id_R)
        DOM%ETAT%alpha_reac(:,ireac) = DOM%ETAT%alpha_reac(:,ireac) / DOM%ETAT%rho_rel_0(:,id_R)
        ! + 1
        DOM%ETAT%alpha_reac(:,ireac) = DOM%ETAT%alpha_reac(:,ireac) + 1.0_dp
        
    END WHERE
END DO
!
!
END SUBROUTINE update_rho_rel
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_update
