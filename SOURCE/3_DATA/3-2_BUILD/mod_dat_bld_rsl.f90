!> Module de construction des données des champs RSL
MODULE mod_dat_bld_rsl
!
USE mod_cst, ONLY : IP, DP
USE mod_print, ONLY : print_err
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Construction des données des champs RSL
!> \warning Matrice jacobienne de l'equation de structure est valable en 2D seulement
SUBROUTINE dat_bld_rsl( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: nesp
INTEGER(IP) :: ncel
INTEGER(IP) :: nfac
!INTEGER(IP) :: niter
INTEGER(IP) :: nreac
INTEGER(IP) :: ngaz
INTEGER(IP) :: ndim

!---- Récupérations ---------------------------------------------------!
nesp = DOM%PHYS%nesp
ncel = DOM%MLG%ncel
nfac = DOM%MLG%nfac
!niter = DOM%NUM%niter
nreac = DOM%PHYS%nreac
ngaz = DOM%PHYS%ngaz
ndim = 2    ! A modifier pour tenir compte des cas 2D et 3D


CALL PARDISOINIT( DOM%RSL%PT, &
                  DOM%RSL%MTYPE, &
                  DOM%RSL%IPARM, &
                  DOM%RSL%error)
! DOM%RSL%IPARM(2) = 2
! DOM%RSL%IPARM(3) = 8
! DOM%RSL%IPARM(4) = 31
! DOM%RSL%IPARM(32) = 1


!--- NOMBRE DE VARIABLES ----------------------------------------------!
! Pour le système Advectif
DOM%RSL%neq_A = ngaz ! Nombres d'équations = ngaz bilans masse
DOM%RSL%ndl_A = DOM%RSL%neq_A * ncel ! Nombre de degrés de liberté
DOM%RSL%nnz_A = DOM%RSL%neq_A * (SUM(DOM%MLG%CEl_ADJ(:,2)) + ncel) ! Nombre de coef non-zeros pour la jacobienne A
!
! Pour le système PISO pression
DOM%RSL%nnz_P = SUM(DOM%MLG%CEl_ADJ(:,2)) + ncel
!
! Pour le système Diffusif-Reactif
DOM%RSL%neq_DR = nreac + 1 ! Nombres d'équations = 1 eq ener + nreac reactions
DOM%RSL%ndl_DR = DOM%RSL%neq_DR * ncel ! Nombre de degrés de liberté
DOM%RSL%nnz_DR = DOM%RSL%neq_DR * (SUM(DOM%MLG%CEl_ADJ(:,2)) + ncel) ! Nombre de coef non-zeros pour la jacobienne DR
!
! Pour le système Structure
DOM%RSL%neq_S = ndim ! Nombres d'équations = nombre de dimensions spatiales
DOM%RSL%ndl_S = DOM%RSL%neq_S * ncel ! Nombre de degrés de liberté
DOM%RSL%nnz_S = DOM%RSL%neq_S * DOM%RSL%neq_S * (SUM(DOM%MLG%CEl_ADJ(:,2)) + ncel) ! Nombre de coef non-zeros pour la jacobienne S

!--- INIT. DES NB IT. ET RESIDUS --------------------------------------!
! Pour le système Advectif
DOM%RSL%err_it_A = 0.0_dp  ! Erreur maximale survenue dans les it. temporelles
DOM%RSL%niter_A = 0 ! Nombre d'it solveur par it temporelle
DOM%RSL%niter_A_lin = 0 ! Nombre d'it solveur par it lineaire

! Pour le système Diffusif-Reactif
DOM%RSL%err_it_DR = 0.0_dp  ! Erreur maximale survenue dans les it. temporelles
DOM%RSL%niter_DR = 0 ! Nombre d'it solveur par it temporelle
DOM%RSL%niter_DR_lin = 0 ! Nombre d'it solveur par it lineaire

! Pour le système Structure
DOM%RSL%err_it_S = 0.0_dp  ! Erreur maximale survenue dans les it. temporelles
DOM%RSL%niter_S = 0 ! Nombre d'it solveur par it temporelle
DOM%RSL%niter_S_lin = 0 ! Nombre d'it solveur par it lineaire


!---- ALLOCATIONS FLUX ET QTES CONSERVEES -----------------------------!
ALLOCATE( DOM%RSL%E( ncel ) )
ALLOCATE( DOM%RSL%dE( ncel ) )
ALLOCATE( DOM%RSL%m( ncel ) )

ALLOCATE( DOM%RSL%m_e( ncel,nesp ) )
ALLOCATE( DOM%RSL%dm_e( ncel,nesp ) )
ALLOCATE( DOM%RSL%dalpha( ncel,nreac ) )

ALLOCATE( DOM%RSL%FL_cond( nfac ) )
ALLOCATE( DOM%RSL%FL_darcy( nfac,nesp ) )
ALLOCATE( DOM%RSL%FL_E_darcy( nfac ) )



ALLOCATE( DOM%RSL%Q_A( DOM%RSL%ndl_A ) )
ALLOCATE( DOM%RSL%F_A( DOM%RSL%ndl_A ) )

ALLOCATE( DOM%RSL%Q_DR( DOM%RSL%ndl_DR ) )
ALLOCATE( DOM%RSL%F_DR( DOM%RSL%ndl_DR ) )

ALLOCATE( DOM%RSL%PERM( DOM%RSL%ndl_DR ) )
DOM%RSL%PERM(:) = 0


ALLOCATE( DOM%RSL%residu_A( ncel ) ) ! Allocation tableau residu advection !!!TEST!!!


! Allocations pour calcul structure
IF (DOM%NUM%STRUCTURE == 'on') THEN
 ALLOCATE( DOM%RSL%U( ncel,3 ) )
 ALLOCATE( DOM%RSL%U_cou( ncel,3 ) )
 ALLOCATE( DOM%RSL%U_itp( nfac,3 ) )
 ALLOCATE( DOM%RSL%dU( ncel,3 ) )
 ALLOCATE( DOM%RSL%FL_struct( nfac,3 ) )
 DOM%RSL%U( :,: ) = 0.0_dp
 DOM%RSL%U_cou( :,: ) = 0.0_dp
 DOM%RSL%U_itp( :,: ) = 0.0_dp
 DOM%RSL%dU( :,: ) = 0.0_dp
 DOM%RSL%FL_struct( :,: ) = 0.0_dp
    
    ALLOCATE( DOM%RSL%Q_S( DOM%RSL%ndl_S ) )
    ALLOCATE( DOM%RSL%F_S( DOM%RSL%ndl_S ) )
    ALLOCATE( DOM%RSL%F_S_tan( DOM%RSL%ndl_S ) )

END IF

! Allocations pour le calcul d'ablation
IF (DOM%NUM%ABLATION == 'on') THEN
    ALLOCATE( DOM%RSL%FL_masse_def( nfac,nesp ) )
    ALLOCATE( DOM%RSL%FL_ener_def( nfac ) )
    ALLOCATE( DOM%RSL%FL_Vol( nfac ) )
    ALLOCATE( DOM%RSL%FL_abla( nfac ) )
    ALLOCATE( DOM%RSL%m_ini( ncel ))
    ALLOCATE( DOM%RSL%volume_ini( ncel ))
END IF

!---- ALLOCATIONS MAT JACOBIENNES -------------------------------------!
! Partie advection
IF (DOM%NUM%schema_A == 'theta-imp' .OR. DOM%NUM%schema_A == 'theta-lin') THEN
    ALLOCATE( DOM%RSL%JAC_A( ncel, DOM%MLG%nfac_adj_max+1, DOM%RSL%neq_A ) )
    ALLOCATE( DOM%RSL%id_JAC_A( ncel, DOM%MLG%nfac_adj_max+1, DOM%RSL%neq_A ) )
    ALLOCATE( DOM%RSL%JAC_A_stval( DOM%RSL%nnz_A ) )
    ALLOCATE( DOM%RSL%JAC_A_stcol( DOM%RSL%nnz_A ) )
    ALLOCATE( DOM%RSL%JAC_A_csrow( DOM%RSL%neq_A * ncel + 1 ) )
    
    CALL dat_bld_jac_A( DOM )
END IF

! Partie pression (abandonnée!)
IF (DOM%NUM%schema_A == 'PISO') THEN
    ALLOCATE( DOM%RSL%JAC_P( ncel, DOM%MLG%nfac_adj_max+1 ) )
    ALLOCATE( DOM%RSL%id_JAC_P( ncel, DOM%MLG%nfac_adj_max+1 ) )
    ALLOCATE( DOM%RSL%JAC_P_stval( DOM%RSL%nnz_A ) )
    ALLOCATE( DOM%RSL%JAC_P_strow( DOM%RSL%nnz_A ) )
    ALLOCATE( DOM%RSL%JAC_P_stcol( DOM%RSL%nnz_A ) )
    
    CALL dat_bld_jac_P( DOM )
END IF

! Partie diffusion-reaction
IF ( DOM%NUM%schema_DR == 'theta-imp' .OR. DOM%NUM%schema_DR == 'theta-lin') THEN
    ALLOCATE( DOM%RSL%JAC_DR( ncel, DOM%MLG%nfac_adj_max+1, DOM%RSL%neq_DR ) )
    ALLOCATE( DOM%RSL%id_JAC_DR( ncel, DOM%MLG%nfac_adj_max+1, DOM%RSL%neq_DR ) )
    ALLOCATE( DOM%RSL%JAC_DR_stval( DOM%RSL%nnz_DR ) )
    ALLOCATE( DOM%RSL%JAC_DR_stcol( DOM%RSL%nnz_DR ) )
    ALLOCATE( DOM%RSL%JAC_DR_csrow( DOM%RSL%neq_DR * ncel + 1 ) )
    
    CALL dat_bld_jac_DR( DOM )
END IF

! Partie structure
IF (DOM%NUM%STRUCTURE == 'on') THEN
    IF ( DOM%NUM%schema_S == 'implicite') THEN
        ALLOCATE( DOM%RSL%JAC_S( ncel, DOM%MLG%nfac_adj_max+1, DOM%RSL%neq_S, DOM%RSL%neq_S ) )
        ALLOCATE( DOM%RSL%id_JAC_S( ncel, DOM%MLG%nfac_adj_max+1, DOM%RSL%neq_S, DOM%RSL%neq_S ) )
        ALLOCATE( DOM%RSL%JAC_S_stval( DOM%RSL%nnz_S ) )
        ALLOCATE( DOM%RSL%JAC_S_strow( DOM%RSL%nnz_S ) )
        ALLOCATE( DOM%RSL%JAC_S_stcol( DOM%RSL%nnz_S ) )
        
        CALL dat_bld_jac_S( DOM )
    END IF
END IF

END SUBROUTINE dat_bld_rsl
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Construction des tableaux d'indices pour les matrices sparse (jacobiennes)
!> Partie advection
SUBROUTINE dat_bld_jac_A( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: icel, ncel, jcel
INTEGER(IP) :: ifac_adj, nfac_adj
INTEGER(IP) :: ieq_A, neq_A
INTEGER(IP) :: inz_A, nnz_A
INTEGER(IP) :: idl_A, jdl_A
INTEGER(IP) :: i_switch, jdl_switch, inz_debut, id_csr

!---- Récupérations ---------------------------------------------------!
nfac_adj = DOM%MLG%nfac_adj_max
ncel = DOM%MLG%ncel
neq_A = DOM%RSL%neq_A
nnz_A = DOM%RSL%nnz_A

! Init de inz
inz_A = 0
id_csr = 0

! Init de id_JAC_A
DOM%RSL%id_JAC_A(:,:,:) = 0
DOM%RSL%JAC_A_stcol(:) = 0
DOM%RSL%JAC_A_csrow(:) = 0

! NB : On boucle sur tous les éléments de la jacobienne sparse RSL%JAC_A et
! et determine à quel position absolu dans la matrice chaque élément correspond
! 1/ On incrémente l'indice non zero de la mat. sparse inz
! 2/ On détermine la ligne et la colonne correspondante dans la mat. jac.

! Pour chaque cellule
DO icel = 1,ncel

    ! Et chaque équation
    DO ieq_A = 1,neq_A
        
        ! Les indices JAC_A(icel,1,ieq_A) correspondent aux derives 
        ! par quantite de cette meme cellule dF(Qi)/dQi
        
        ! Incrémentation et écriture de inz (indice non nulle de la jac)
        inz_A = inz_A + 1
        DOM%RSL%id_JAC_A(icel,1,ieq_A) = inz_A
        ! et inz_csr (premier indice non nul pour icel)
        id_csr = id_csr + 1
        inz_debut = inz_A ! Premiere valeur inz de la cellule pour CSR

        ! Ligne et cellule correspondante
        idl_A = ((icel-1) * neq_A) + ieq_A
        jdl_A = idl_A ! termes diagonaux
        ! Ecriture
        DOM%RSL%JAC_A_stcol(inz_A) = jdl_A
        DOM%RSL%JAC_A_csrow(id_csr) = inz_A
        
        ! Maintenant pour chaque cellule adjacente
        DO ifac_adj = 1,nfac_adj
        
            ! Si ce n'est pas une face limite
            IF (DOM%MLG%CEL_ADJ(icel,ifac_adj+2) /= 0) THEN
            
                ! Incrémentation et écriture de inz (indice non nul de la jac)
                inz_A = inz_A + 1
                DOM%RSL%id_JAC_A(icel,ifac_adj+1,ieq_A) = inz_A

                ! Ligne correspondante
                idl_A = ((icel-1) * neq_A) + ieq_A
                ! Cellule correspondante
                jcel = DOM%MLG%CEL_ADJ(icel,ifac_adj+2)
                jdl_A = ((jcel-1) * neq_A) + ieq_A
                ! Ecriture
                DOM%RSL%JAC_A_stcol(inz_A) = jdl_A
                
                !--- Restructuration CSR de stcol en ordre croissant 
                ! Pour chaque noucelle cel_adj ajoutee...
                DO i_switch = inz_A,inz_debut+1,-1
                    
                    ! On switche avec la precedente si inferieure
                    IF (DOM%RSL%JAC_A_stcol(i_switch) < DOM%RSL%JAC_A_stcol(i_switch-1)) THEN
                    
                        jdl_switch = DOM%RSL%JAC_A_stcol(i_switch-1)
                        DOM%RSL%JAC_A_stcol(i_switch-1) = DOM%RSL%JAC_A_stcol(i_switch)
                        DOM%RSL%JAC_A_stcol(i_switch) = jdl_switch

                    END IF
                END DO
                !--- Fin restructuration CSR ordre stcol

            END IF
        
        END DO


        !--- Restructuration de id_JAC_A pour avoir stcol en ordre croissant
        ! Pour chaque cel et cel_adj et icel...
        DO ifac_adj = 0,nfac_adj
            IF (DOM%RSL%id_JAC_A(icel,ifac_adj+1,ieq_A) /= 0) THEN
                
                ! ...On reconstruit le jdl_DR de base...
                IF (ifac_adj == 0) THEN
                    jdl_A = ((icel-1) * neq_A) + ieq_A
                ELSE
                    jcel = DOM%MLG%CEL_ADJ(icel,ifac_adj+2)
                    jdl_A = ((jcel-1) * neq_A) + ieq_A
                END IF
                
                ! ...et on teste dans stcol a quelle position ca correspond
                DO i_switch = 0,inz_A - inz_debut
                    IF (DOM%RSL%JAC_A_stcol(inz_debut + i_switch) == jdl_A) THEN
                    
                        ! On ecrit id_JAC_DR et on sort directement
                        DOM%RSL%id_JAC_A(icel,ifac_adj+1,ieq_A) = inz_debut + i_switch
                        EXIT
                        
                    END IF
                END DO
        
            END IF
        END DO
        !--- Fin Restructuration de id_JAC_DR

    END DO
END DO

DOM%RSL%JAC_A_csrow(id_csr+1) = inz_A + 1

! Si on n'a pas bouclé sur tous les termes non nuls
IF (inz_A /= nnz_A) THEN
    CALL print_err('La matrice jacobienne n''est pas conforme')
    CALL print_err('Le tableau id_JAC_A ne peut etre alloue',1)
END IF


END SUBROUTINE dat_bld_jac_A
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Construction des tableaux d'indices pour les matrices sparse (jacobiennes)
!> Partie pression
SUBROUTINE dat_bld_jac_P( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: icel, ncel, jcel
INTEGER(IP) :: ifac_adj, nfac_adj
INTEGER(IP) :: inz_P, nnz_P
INTEGER(IP) :: idl_P, jdl_P

!---- Récupérations ---------------------------------------------------!
nfac_adj = DOM%MLG%nfac_adj_max
ncel = DOM%MLG%ncel
nnz_P = DOM%RSL%nnz_P

! Init de inz
inz_P = 0

! Init de id_JAC_P
DOM%RSL%id_JAC_P(:,:) = 0
DOM%RSL%JAC_P_strow(:) = 0
DOM%RSL%JAC_P_stcol(:) = 0


! NB : On boucle sur tous les éléments de la jacobienne sparse RSL%JAC_P et
! et determine à quel position absolu dans la matrice chaque élément correspond
! 1/ On incrémente l'indice non zero de la mat. sparse inz
! 2/ On détermine la ligne et la colonne correspondante dans la mat. jac.

! Pour chaque cellule
DO icel = 1,ncel
        
        ! Les indices JAC_P(icel,1) correspondent aux derives 
        ! par quantite de cette meme cellule dF(Qi)/dQi
        
        ! Incrémentation et écriture de inz (indice non nulle de la jac)
        inz_P = inz_P + 1
        DOM%RSL%id_JAC_P(icel,1) = inz_P
        
        ! Ligne et cellule correspondante
        idl_P = icel
        jdl_P = idl_P ! termes diagonaux
        ! Ecriture
        DOM%RSL%JAC_P_strow(inz_P) = idl_P
        DOM%RSL%JAC_P_stcol(inz_P) = jdl_P
        
        ! Maintenant pour chaque cellule adjacente
        DO ifac_adj = 1,nfac_adj
        
            ! Si ce n'est pas une face limite
            IF (DOM%MLG%CEL_ADJ(icel,ifac_adj+2) /= 0) THEN
            
                ! Incrémentation et écriture de inz (indice non nul de la jac)
                inz_P = inz_P + 1
                DOM%RSL%id_JAC_P(icel,ifac_adj+1) = inz_P
                
                ! Ligne correspondante
                idl_P = icel
                ! Cellule correspondante
                jcel = DOM%MLG%CEL_ADJ(icel,ifac_adj+2)
                jdl_P = jcel
                ! Ecriture
                DOM%RSL%JAC_P_strow(inz_P) = idl_P
                DOM%RSL%JAC_P_stcol(inz_P) = jdl_P
                
            END IF
        
        END DO
END DO

! Si on n'a pas bouclé sur tous les termes non nuls
IF (inz_P /= nnz_P) THEN
    CALL print_err('La matrice jacobienne n''est pas conforme')
    CALL print_err('Le tableau id_JAC_P ne peut etre alloue',1)
END IF


END SUBROUTINE dat_bld_jac_P
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Construction des tableaux d'indices pour les matrices sparse (jacobiennes)
!> Partie diffusion-reaction
SUBROUTINE dat_bld_jac_DR( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: icel, ncel, jcel
INTEGER(IP) :: ifac_adj, nfac_adj
INTEGER(IP) :: ieq_DR, neq_DR
INTEGER(IP) :: inz_DR, nnz_DR
INTEGER(IP) :: idl_DR, jdl_DR
INTEGER(IP) :: i_switch, jdl_switch, inz_debut, id_csr

!---- Récupérations ---------------------------------------------------!
nfac_adj = DOM%MLG%nfac_adj_max
ncel = DOM%MLG%ncel
neq_DR = DOM%RSL%neq_DR
nnz_DR = DOM%RSL%nnz_DR

! Init de inz
inz_DR = 0
id_csr = 0

! Init de id_JAC_DR
DOM%RSL%id_JAC_DR(:,:,:) = 0
DOM%RSL%JAC_DR_stcol(:) = 0
DOM%RSL%JAC_DR_csrow(:) = 0

! NB : On boucle sur tous les éléments de la jacobienne sparse RSL%JAC_DR et
! et determine à quel position absolu dans la matrice chaque élément correspond
! 1/ On incrémente l'indice non zero de la mat. sparse inz
! 2/ On détermine la ligne et la colonne correspondante dans la mat. jac.

! Pour chaque cellule
DO icel = 1,ncel

    ! Et chaque équation
    DO ieq_DR = 1,neq_DR
        
        ! Les indices JAC_DR(icel,1,ieq_DR) correspondent aux derives 
        ! par quantite de cette meme cellule dF(Qi)/dQi
        
        ! Incrémentation et écriture de inz (indice non nulle de la jac)
        inz_DR = inz_DR + 1
        DOM%RSL%id_JAC_DR(icel,1,ieq_DR) = inz_DR
        ! et inz_csr (premier indice non nul pour icel)
        id_csr = id_csr + 1
        inz_debut = inz_DR ! Premiere valeur inz de la cellule pour CSR

        ! Ligne et cellule correspondante
        idl_DR = ((icel-1) * neq_DR) + ieq_DR
        jdl_DR = idl_DR ! termes diagonaux
        ! Ecriture
        !DOM%RSL%JAC_DR_strow(inz_DR) = idl_DR
        DOM%RSL%JAC_DR_stcol(inz_DR) = jdl_DR
        DOM%RSL%JAC_DR_csrow(id_csr) = inz_DR
                    
        ! Maintenant pour chaque cellule adjacente
        DO ifac_adj = 1,nfac_adj
        
            ! Si ce n'est pas une face limite
            IF (DOM%MLG%CEL_ADJ(icel,ifac_adj+2) /= 0) THEN
            
                ! Incrémentation et écriture de inz (indice non nul de la jac)
                inz_DR = inz_DR + 1
                DOM%RSL%id_JAC_DR(icel,ifac_adj+1,ieq_DR) = inz_DR
                
                ! Ligne correspondante
                idl_DR = ((icel-1) * neq_DR) + ieq_DR
                ! Cellule correspondante
                jcel = DOM%MLG%CEL_ADJ(icel,ifac_adj+2)
                jdl_DR = ((jcel-1) * neq_DR) + ieq_DR
                ! Ecriture
                DOM%RSL%JAC_DR_stcol(inz_DR) = jdl_DR
                
                !--- Restructuration CSR de stcol en ordre croissant 
                ! Pour chaque noucelle cel_adj ajoutee...
                DO i_switch = inz_DR,inz_debut+1,-1
                    
                    ! On switche avec la precedente si inferieure
                    IF (DOM%RSL%JAC_DR_stcol(i_switch) < DOM%RSL%JAC_DR_stcol(i_switch-1)) THEN
                    
                        jdl_switch = DOM%RSL%JAC_DR_stcol(i_switch-1)
                        DOM%RSL%JAC_DR_stcol(i_switch-1) = DOM%RSL%JAC_DR_stcol(i_switch)
                        DOM%RSL%JAC_DR_stcol(i_switch) = jdl_switch

                    END IF
                END DO
                !--- Fin restructuration CSR ordre stcol
                
            END IF
        
        END DO
        
        
        !--- Restructuration de id_JAC_DR pour avoir stcol en ordre croissant
        ! Pour chaque cel et cel_adj et icel...
        DO ifac_adj = 0,nfac_adj
            IF (DOM%RSL%id_JAC_DR(icel,ifac_adj+1,ieq_DR) /= 0) THEN
                
                ! ...On reconstruit le jdl_DR de base...
                IF (ifac_adj == 0) THEN
                    jdl_DR = ((icel-1) * neq_DR) + ieq_DR
                ELSE
                    jcel = DOM%MLG%CEL_ADJ(icel,ifac_adj+2)
                    jdl_DR = ((jcel-1) * neq_DR) + ieq_DR
                END IF
                
                ! ...et on teste dans stcol a quelle position ca correspond
                DO i_switch = 0,inz_DR - inz_debut
                    IF (DOM%RSL%JAC_DR_stcol(inz_debut + i_switch) == jdl_DR) THEN
                    
                        ! On ecrit id_JAC_DR et on sort directement
                        DOM%RSL%id_JAC_DR(icel,ifac_adj+1,ieq_DR) = inz_debut + i_switch
                        EXIT
                        
                    END IF
                END DO
        
            END IF
        END DO
        !--- Fin Restructuration de id_JAC_DR
        
    END DO
END DO

DOM%RSL%JAC_DR_csrow(id_csr+1) = inz_DR + 1

! Si on n'a pas bouclé sur tous les termes non nuls
IF (inz_DR /= nnz_DR) THEN
    CALL print_err('La matrice jacobienne n''est pas conforme')
    CALL print_err('Le tableau id_JAC_DR ne peut etre alloue',1)
END IF


END SUBROUTINE dat_bld_jac_DR
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Construction des tableaux d'indices pour les matrices sparse (jacobiennes)
!> Partie structure
SUBROUTINE dat_bld_jac_S( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: icel, ncel, jcel
INTEGER(IP) :: ifac_adj, nfac_adj
INTEGER(IP) :: ieq_S, neq_S
INTEGER(IP) :: inz_S, nnz_S
INTEGER(IP) :: idl_S, jdl_S

!---- Récupérations ---------------------------------------------------!
nfac_adj = DOM%MLG%nfac_adj_max
ncel = DOM%MLG%ncel
neq_S = DOM%RSL%neq_S
nnz_S = DOM%RSL%nnz_S

! Init de inz
inz_S = 0

! Init de id_JAC_S
DOM%RSL%id_JAC_S(:,:,:,:) = 0
DOM%RSL%JAC_S_strow(:) = 0
DOM%RSL%JAC_S_stcol(:) = 0

! NB : On boucle sur tous les éléments de la jacobienne sparse RSL%JAC_S et
! et determine à quel position absolu dans la matrice chaque élément correspond
! 1/ On incrémente l'indice non zero de la mat. sparse inz
! 2/ On détermine la ligne et la colonne correspondante dans la mat. jac.


! BOUCLE -> COLONNE CELLULE -> LIGNE CELLULE -> LIGNE BLOC -> COLONNE BLOC ---
!        |                                                                   |
!        ---------------------------------------------------------------------


! Pour chaque cellule
DO icel = 1,ncel
        
    !------------------------------------------------------------------!
    ! Les indices JAC_S(icel,1,ieq_S,jeq_S) correspondent aux matrices blocs
    ! par quantite de cette meme cellule dF(Qi)/dQi
    
    !--- En X (1ERE COLONNE BLOC) -------------------------------------!
    DO ieq_S = 1,neq_S  ! pour chaque équation
    
        ! Incrémentation et écriture de inz (indice non nulle de la jac)
        inz_S = inz_S + 1
        DOM%RSL%id_JAC_S(icel,1,1,ieq_S) = inz_S
        
        ! Ligne et colonne correspondante
        idl_S = ((icel-1) * neq_S) + ieq_S
        jdl_S = ((icel-1) * neq_S) + 1
        ! Ecriture
        DOM%RSL%JAC_S_strow(inz_S) = idl_S
        DOM%RSL%JAC_S_stcol(inz_S) = jdl_S
    
    END DO
    
    !--- En Y (2EME COLONNE BLOC) -------------------------------------!
    DO ieq_S = 1,neq_S  ! pour chaque équation
    
        ! Incrémentation et écriture de inz (indice non nulle de la jac)
        inz_S = inz_S + 1
        DOM%RSL%id_JAC_S(icel,1,2,ieq_S) = inz_S
        
        ! Ligne et colonne correspondante
        idl_S = ((icel-1) * neq_S) + ieq_S
        jdl_S = ((icel-1) * neq_S) + 2
        ! Ecriture
        DOM%RSL%JAC_S_strow(inz_S) = idl_S
        DOM%RSL%JAC_S_stcol(inz_S) = jdl_S
    
    END DO
        
    !--------------------------------------------------------------!
    ! Les indices JAC_S(icel,ifac_adj + 1,ieq_S,jeq_S) correspondent aux matrices blocs
    ! par quantite de cellule adjacente dF(Qi)/dQj
    
    ! Pour chaque cellule adjacente
    DO ifac_adj = 1,nfac_adj
        
        ! Cellule correspondante
        jcel = DOM%MLG%CEL_ADJ(icel,ifac_adj+2)
        
        ! Si ce n'est pas une face limite
        IF (jcel /= 0) THEN
            
            !--- En X (1ERE COLONNE BLOC) -----------------------------!
            DO ieq_S = 1,neq_S  ! pour chaque équation
            
                ! Incrémentation et écriture de inz (indice non nulle de la jac)
                inz_S = inz_S + 1
                DOM%RSL%id_JAC_S(icel,ifac_adj+1,1,ieq_S) = inz_S
                
                ! Ligne et colonne correspondante
                idl_S = ((jcel-1) * neq_S) + ieq_S
                jdl_S = ((icel-1) * neq_S) + 1
                ! Ecriture
                DOM%RSL%JAC_S_strow(inz_S) = idl_S
                DOM%RSL%JAC_S_stcol(inz_S) = jdl_S
            
            END DO
            
            !--- En Y (2EME COLONNE BLOC) -----------------------------!
            DO ieq_S = 1,neq_S  ! pour chaque équation
            
                ! Incrémentation et écriture de inz (indice non nulle de la jac)
                inz_S = inz_S + 1
                DOM%RSL%id_JAC_S(icel,ifac_adj+1,2,ieq_S) = inz_S
                
                ! Ligne et colonne correspondante
                idl_S = ((jcel-1) * neq_S) + ieq_S
                jdl_S = ((icel-1) * neq_S) + 2
                ! Ecriture
                DOM%RSL%JAC_S_strow(inz_S) = idl_S
                DOM%RSL%JAC_S_stcol(inz_S) = jdl_S
            
            END DO
                
        END IF
        
    END DO

END DO

! Si on n'a pas bouclé sur tous les termes non nuls
IF (inz_S /= nnz_S) THEN
    CALL print_err('La matrice jacobienne n''est pas conforme')
    CALL print_err('Le tableau id_JAC_S ne peut etre alloue',1)
END IF


END SUBROUTINE dat_bld_jac_S
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_dat_bld_rsl
