!> Module de construction des données des champs PHYS
MODULE mod_dat_bld_phys
!
USE mod_cst, ONLY : IP, DP, eps_min
USE mod_print, ONLY : print_err
USE mod_algebra, ONLY : inttostr, polyint
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Construction des données des champs PHYS
SUBROUTINE dat_bld_phys( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: nreac, ireac
INTEGER(IP) :: iesp, nesp
INTEGER(IP) :: nsol, isol, id_sol
INTEGER(IP) :: ngaz, igaz, id_gaz
INTEGER(IP) :: id_R, id_P
REAL(DP) :: sum_nu
CHARACTER(len=32)::tmp_str

!---- Récupérations ---------------------------------------------------!
nreac = DOM%PHYS%nreac
nesp = DOM%PHYS%nesp
nsol = DOM%PHYS%nsol
ngaz = DOM%PHYS%ngaz

!---- Vérifications ---------------------------------------------------!

IF ((DOM%PHYS%nreac > 0) .AND. (DOM%NUM%REACTION == 'off')) THEN
    CALL print_err('Le solveur reactif est desactive mais nreac>0')
END IF

IF ((nsol+ngaz) .ne. nesp) THEN
    CALL print_err('Le nombre de gaz et solides declares ne correspondent pas a nesp')
END IF

DO isol = 1,nsol
    id_sol = DOM%PHYS%ID_sol(isol)
    IF (DOM%PHYS%ESP(id_sol)%typ_esp .ne. 'solide') THEN
        tmp_str = ADJUSTL(inttostr(id_sol))
        CALL print_err('L''espece '//TRIM(tmp_str)//' a ete declaree comme solide mais son type ne correspond pas')
    END IF
END DO

DO igaz = 1,ngaz
    id_gaz = DOM%PHYS%ID_gaz(igaz)
    IF (DOM%PHYS%ESP(id_gaz)%typ_esp .ne. 'gaz') THEN
        tmp_str = ADJUSTL(inttostr(id_gaz))
        CALL print_err('L''espece '//TRIM(tmp_str)//' a ete declaree comme gaz mais son type ne correspond pas')
    END IF
END DO

! Integration des coef. polynomiaux de Cp
DO iesp =1,nesp
    IF (DOM%PHYS%ESP(iesp)%Cp_typ .eq. 'polyn') THEN
        ALLOCATE( DOM%PHYS%ESP(iesp)%int_Cp_poly( DOM%PHYS%ESP(iesp)%Cp_nb_poly + 1 ) )
        CALL polyint( DOM%PHYS%ESP(iesp)%Cp_poly, DOM%PHYS%ESP(iesp)%int_Cp_poly )
    END IF
END DO

! Vérification pour les réactions
DO ireac = 1,nreac
    
    ! id_R => 'solide'
    id_R = DOM%PHYS%REAC(ireac)%id_R
    IF (DOM%PHYS%ESP(id_R)%typ_esp .ne. 'solide') THEN
        tmp_str = ADJUSTL(inttostr(ireac))
        CALL print_err('Le reactif R de la reaction '//TRIM(tmp_str)//' n''est pas de type solide')
    END IF
    
    ! id_P => 'solide'
    id_P = DOM%PHYS%REAC(ireac)%id_P
    IF (id_P /= 0) THEN
    IF (DOM%PHYS%ESP(id_P)%typ_esp .ne. 'solide') THEN
        tmp_str = ADJUSTL(inttostr(ireac))
        CALL print_err('Le produit P de la reaction '//TRIM(tmp_str)//' n''est pas de type solide')
    END IF
    END IF
    
    ! Bilan de masse réactionnel : nu_R + nu_O2 == nu_P + nu_gazP
    sum_nu = DOM%PHYS%REAC(ireac)%nu_R
    sum_nu = sum_nu + DOM%PHYS%REAC(ireac)%nu_O2
    sum_nu = sum_nu - DOM%PHYS%REAC(ireac)%nu_P
    sum_nu = sum_nu - SUM(DOM%PHYS%REAC(ireac)%nu_gazP)
    IF (ABS(sum_nu) > 2.0_dp*eps_min) THEN
        tmp_str = ADJUSTL(inttostr(ireac))
        CALL print_err('Le bilan de masse reactionnel de la reaction '//TRIM(tmp_str)//' n''est pas nul')
        CALL print_err('Veuillez verifier les coefficients nu',1)
    END IF
    
END DO

! Cas test Henderson a bien ete parametré ...
IF ((DOM%PHYS%MAT%Kp_typ == 'Henderson') .AND. (nreac/=1)) THEN
    CALL print_err('Le cas modele Henderson ne permet de traiter que UNE seule reaction')
END IF

END SUBROUTINE dat_bld_phys
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_dat_bld_phys
