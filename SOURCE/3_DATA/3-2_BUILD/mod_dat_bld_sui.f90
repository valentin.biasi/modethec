!> Module de construction des données des champs SUI
MODULE mod_dat_bld_sui
!
USE mod_cst, ONLY : IP, DP, eps_min
USE mod_print, ONLY : print_err
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Construction des données des champs SUI
SUBROUTINE dat_bld_sui( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
REAL(DP) :: iiter_real

!---- Récupérations ---------------------------------------------------!

! Nb d'ité. entre chaque pas de calcul
IF (DOM%SUI%suivi == 1 .OR. DOM%SUI%file_log == 1) THEN
    
    ! Nb d'it entre print : iiter_prt 
    iiter_real = ABS( DOM%SUI%dt_prt - FLOOR( DOM%SUI%dt_prt/DOM%NUM%dt ) * DOM%NUM%dt )
    IF (iiter_real > 10.0_dp*eps_min) THEN
        CALL print_err('La valeur de dt_prt n''est pas multiple de dt.' )
        DOM%SUI%iiter_prt = 1
    ELSE
        DOM%SUI%iiter_prt =  INT( DOM%SUI%dt_prt / DOM%NUM%dt )
    END IF

END IF

! Nb d'it entre export hdf5 : iiter_hdf5
IF (DOM%SUI%file_hdf5 == 1) THEN

    iiter_real = ABS( DOM%SUI%dt_hdf5 - FLOOR( DOM%SUI%dt_hdf5/DOM%NUM%dt ) * DOM%NUM%dt )
    IF (iiter_real > 10.0_dp*eps_min) THEN
        CALL print_err('La valeur de dt_hdf5 n''est pas multiple de dt.' )
        DOM%SUI%iiter_hdf5 = 1
    ELSE
        DOM%SUI%iiter_hdf5 =  INT( DOM%SUI%dt_hdf5 / DOM%NUM%dt )
    END IF

    ! Nb d'it entre backup : iiter_backup
    IF (DOM%SUI%backup == 1) THEN
    
        iiter_real = ABS( DOM%SUI%dt_backup - FLOOR( DOM%SUI%dt_backup/DOM%NUM%dt ) * DOM%NUM%dt )
        IF (iiter_real > 10.0_dp*eps_min) THEN
            CALL print_err('La valeur de dt_backup n''est pas multiple de dt.' )
            DOM%SUI%iiter_backup = 1
        ELSE
            DOM%SUI%iiter_backup =  INT( DOM%SUI%dt_backup / DOM%NUM%dt )
        END IF
    END IF

END IF


END SUBROUTINE dat_bld_sui
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_dat_bld_sui
