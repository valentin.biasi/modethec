!> Module de construction des données des champs NUM
MODULE mod_dat_bld_num
!
USE mod_cst, ONLY : IP, DP, eps_min
USE mod_print, ONLY : print_err
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Construction des données des champs NUM
SUBROUTINE dat_bld_num( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
REAL(DP) :: iiter_real

!---- Initialisation
DOM%NUM%iiter = 0


! Nombre d'itérations
DOM%NUM%niter = CEILING( DOM%NUM%tf / DOM%NUM%dt )

!DOM%NUM%pas_reduit = 0  !NON-UTILISE   ! Definit si le pas est reduit par rapport a l'etat standart
!DOM%NUM%pre_eval = 0   !NON-UTILISE   ! Definit si l'on est a la 1ere evaluation du cycle ou non

! Si couplage actif
IF (DOM%NUM%COUPLAGE == 'on') THEN
    
    ! Nb d'ité. entre chaque couplage
    iiter_real = ABS( DOM%NUM%dt_couplage - FLOOR( DOM%NUM%dt_couplage/DOM%NUM%dt ) * DOM%NUM%dt )
    IF (iiter_real > 3.0_dp*eps_min) THEN
        CALL print_err('La valeur de dt_couplage n''est pas multiple de dt.' )
        DOM%NUM%iiter_couplage = 1
    ELSE
        DOM%NUM%iiter_couplage =  INT( DOM%NUM%dt_couplage / DOM%NUM%dt )
    END IF
    
END IF


END SUBROUTINE dat_bld_num
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_dat_bld_num
