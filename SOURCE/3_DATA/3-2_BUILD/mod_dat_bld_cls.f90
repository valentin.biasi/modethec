!> Module de construction des champs de conditions limites speciales (CLS)
MODULE mod_dat_bld_cls
!
USE mod_cst, ONLY : IP, DP, sigmaSB, hugem
USE mod_print, ONLY : print_err
USE mod_algebra, ONLY : inttostr, normL2 
USE mod_mesh_axe, ONLY : mesh_axe_cls
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Construction des champs de conditions limites speciales (CLS)
SUBROUTINE dat_bld_cls( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: nfac, ncel
INTEGER(IP) :: ilim, nlim, ifac_lim, nfac_limi, id_fac
CHARACTER (len=32) :: ener_typ_cls
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_FACLIMi
REAL(DP) :: X, Fimp_nuage, Fimp_gauss, Fimp_dir
INTEGER(IP) :: ipt_nuage, id_pt1, id_pt2
REAL(DP) :: norm_ipt, norm_pt1, norm_pt2
REAL(DP), DIMENSION(3) :: P_XYZ
!
!---- Récupérations ---------------------------------------------------!
nfac = DOM%MLG%nfac
ncel = DOM%MLG%ncel

! Pour chaque limite du domaine
nlim = DOM%CDT%nlim
DO ilim = 1,nlim
IF (DOM%CDT%LIM(ilim)%use_cls == 1) THEN

    nfac_limi = DOM%CDT%LIM(ilim)%nfac_limi

    ! Indice des faces de la limite i
    ALLOCATE( ID_FACLIMi(nfac_limi) )
    ID_FACLIMi = DOM%CDT%LIM(ilim)%ID_FACLIMi

    ! Pour les conditions limites speciales de type energetiques
    ener_typ_cls = DOM%CDT%LIM(ilim)%ener_typ_cls
    SELECT CASE (ener_typ_cls)

        !--------------------------------------------------------------!
        ! Cas limites heterogenes : flux gaussien
        CASE ("gaussien")
            
            ! Flux applique ssi ti <= tf_gauss
            !IF (DOM%NUM%ti >= DOM%CDT%LIM(ilim)%ti_gauss .AND. DOM%NUM%ti <= DOM%CDT%LIM(ilim)%tf_gauss ) THEN
            
            ! Calcul des distances à l'axe 
            IF (.NOT. ALLOCATED( DOM%CDT%LIM(ilim)%dist_axe )) ALLOCATE( DOM%CDT%LIM(ilim)%dist_axe(DOM%CDT%LIM(ilim)%nfac_limi) )
            CALL mesh_axe_cls( DOM , ilim )

            DO ifac_lim = 1,nfac_limi

                ! Indice absolu de la face
                id_fac = ID_FACLIMi(ifac_lim)

                ! Flux gaussien = Fmax * exp(-2 * r**2 / omega**2 )
                Fimp_gauss = EXP( - 2.0_dp * DOM%CDT%LIM(ilim)%dist_axe(ifac_lim)**2 / DOM%CDT%LIM(ilim)%omega**2 )
                Fimp_gauss = Fimp_gauss * DOM%CDT%LIM(ilim)%Fmax

                ! Projection du flux sur la paroi
                IF (DOT_PRODUCT( DOM%MLG%VFAC(id_fac,:), DOM%CDT%LIM(ilim)%axe(4:6) ) > 0.0_dp) CYCLE ! Face "cachée"
                Fimp_gauss = Fimp_gauss * ABS( DOT_PRODUCT( DOM%MLG%VFAC(id_fac,:), DOM%CDT%LIM(ilim)%axe(4:6) ) )

                DOM%CDT%LIM(ilim)%Fimp(ifac_lim) = Fimp_gauss

            END DO

        !--------------------------------------------------------------!
        ! Cas limites heterogenes : flux directionnel
        CASE ("directionnel")
            
            ! Calcul des distances à l'axe 
            IF (.NOT. ALLOCATED( DOM%CDT%LIM(ilim)%dist_axe )) ALLOCATE( DOM%CDT%LIM(ilim)%dist_axe(DOM%CDT%LIM(ilim)%nfac_limi) )
            CALL mesh_axe_cls( DOM , ilim )   

            DO ifac_lim = 1,nfac_limi

                ! On n'applique un flux que sur les faces dont la distance à l'axe est inférieure au rayon
                IF (DOM%CDT%LIM(ilim)%dist_axe(ifac_lim) > DOM%CDT%LIM(ilim)%omega) CYCLE

                ! Indice absolu de la face
                id_fac = ID_FACLIMi(ifac_lim)

                ! Flux directionnel = Fmax 
                Fimp_dir = DOM%CDT%LIM(ilim)%Fmax

                ! Projection du flux sur la paroi
                IF (DOT_PRODUCT( DOM%MLG%VFAC(id_fac,:), DOM%CDT%LIM(ilim)%axe(4:6) ) > 0.0_dp) CYCLE ! Face "cachée"
                Fimp_dir = Fimp_dir * ABS( DOT_PRODUCT( DOM%MLG%VFAC(id_fac,:), DOM%CDT%LIM(ilim)%axe(4:6) ) )

                DOM%CDT%LIM(ilim)%Fimp(ifac_lim) = Fimp_dir

            END DO

        !--------------------------------------------------------------!
        ! Cas limites heterogenes : flux en nuage de points (VALABLE EN 2D SEULEMENT)
        CASE ("nuage")
            DO ifac_lim = 1,nfac_limi
                
                ! Indice absolu de la face
                id_fac = ID_FACLIMi(ifac_lim)
                id_pt1 = 0
                id_pt2 = 0
                norm_pt1 = hugem
                norm_pt2 = hugem
                
                ! Recherche des deux points du nuage les plus proches de id_fac
                DO ipt_nuage = 1,DOM%CDT%LIM(ilim)%npt_nuage
                        
                        P_XYZ(1:2) = DOM%CDT%LIM(ilim)%val_nuage(ipt_nuage,1:2)
                        P_XYZ(3) = 0.0_dp
                        norm_ipt = normL2( DOM%MLG%H_XYZ(id_fac,:) - P_XYZ(:) )
                        
                        IF ((norm_ipt < norm_pt1) .AND. (norm_ipt < norm_pt2)) THEN
                            norm_pt2 = norm_pt1
                            id_pt2 = id_pt1
                            
                            norm_pt1 = norm_ipt
                            id_pt1 = ipt_nuage
                            
                        ELSE IF ((norm_ipt > norm_pt1) .AND. (norm_ipt < norm_pt2)) THEN
                            norm_pt2 = norm_ipt
                            id_pt2 = ipt_nuage
                            
                        END IF

                END DO
                
                ! Inerpolation linéaire en 1D entre id_pt1 et id_pt2 
                Fimp_nuage = 0.0_dp
                Fimp_nuage = DOT_PRODUCT( DOM%CDT%LIM(ilim)%val_nuage(id_pt2,1:2) - DOM%CDT%LIM(ilim)%val_nuage(id_pt1,1:2), &
                                     DOM%MLG%H_XYZ(id_fac,1:2) - DOM%CDT%LIM(ilim)%val_nuage(id_pt1,1:2) )
                Fimp_nuage = Fimp_nuage / normL2( DOM%CDT%LIM(ilim)%val_nuage(id_pt2,1:2) - DOM%CDT%LIM(ilim)%val_nuage(id_pt1,1:2) ) ** 2                     
                Fimp_nuage = Fimp_nuage * ( DOM%CDT%LIM(ilim)%val_nuage(id_pt2,3) - DOM%CDT%LIM(ilim)%val_nuage(id_pt1,3) )
                Fimp_nuage = Fimp_nuage + DOM%CDT%LIM(ilim)%val_nuage(id_pt1,3)
                
                DOM%CDT%LIM(ilim)%Fimp(ifac_lim) = Fimp_nuage
                
            END DO


        !--------------------------------------------------------------!
        ! Cas special pour la simulation du reservoir satellite en TA6V (le flux est défini en dur par une fonction dépendante de la coordonnée X)    
        CASE ("reservoir")
            DO ifac_lim = 1, nfac_limi  

                ! Indice absolu de la face
                id_fac = DOM%CDT%LIM(ilim)%ID_FACLIMi( ifac_lim )
                ! Coordonnée en X du centre de la face courante
                X = DOM%MLG%H_XYZ(id_fac,1) 

                ! Flux entrant à la face courante en fonction de X       
                IF(X < 0.0_dp) THEN
                    DOM%CDT%LIM(ilim)%Fimp(ifac_lim) = (5610544.0_dp * (-X)**(1.5476_dp)) &
                                                    - sigmaSB * DOM%ETAT%eps_rad(id_fac) * (DOM%ETAT%T_itp(id_fac) ** 4)   
                ELSE
                    DOM%CDT%LIM(ilim)%Fimp(ifac_lim) = 0.0_dp
                END IF

            END DO


    END SELECT ! Fin ener_typ_cls

    DEALLOCATE( ID_FACLIMi ) ! Désallocation

END IF
END DO ! Fin ilim

END SUBROUTINE dat_bld_cls

!----------------------------------------------------------------------!
!> MAJ des champs de conditions limites speciales (CLS)
SUBROUTINE update_cls( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: ilim, nlim
CHARACTER (len=32) :: ener_typ_cls

! Pour chaque limite du domaine
nlim = DOM%CDT%nlim
DO ilim = 1,nlim
IF (DOM%CDT%LIM(ilim)%use_cls == 1) THEN

    ! Pour les conditions limites speciales de type energetiques
    ener_typ_cls = DOM%CDT%LIM(ilim)%ener_typ_cls
    SELECT CASE (ener_typ_cls)

        !--------------------------------------------------------------!
        ! Cas limites heterogenes : flux gaussien
        CASE ("gaussien")

            IF (DOM%NUM%ti > DOM%CDT%LIM(ilim)%tf_gauss) THEN
                DOM%CDT%LIM(ilim)%Fimp = 0.0_dp
            END IF

    END SELECT ! Fin ener_typ_cls

END IF
END DO ! Fin ilim

END SUBROUTINE update_cls
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_dat_bld_cls