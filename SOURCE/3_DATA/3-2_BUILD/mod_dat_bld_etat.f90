!> Module de construction des données des champs ETAT
MODULE mod_dat_bld_etat
!
USE mod_cst, ONLY : IP
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Construction des données des champs ETAT : Variables naturelles
SUBROUTINE dat_bld_etat( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: nesp
INTEGER(IP) :: ncel
INTEGER(IP) :: nfac
INTEGER(IP) :: nreac
INTEGER(IP) :: nsom
INTEGER(IP) :: nfac_lim

!---- Récupérations ---------------------------------------------------!
nesp = DOM%PHYS%nesp
nreac = DOM%PHYS%nreac
ncel = DOM%MLG%ncel
nfac = DOM%MLG%nfac
nsom = DOM%MLG%nsom
nfac_lim = DOM%MLG%nfac_lim

!---- Allocations -----------------------------------------------------!

! Températures
ALLOCATE( DOM%ETAT%T( ncel ))
ALLOCATE( DOM%ETAT%T_itp( nfac ))
ALLOCATE( DOM%ETAT%grad_T_itp( nfac,3 ))
ALLOCATE( DOM%ETAT%T_ref( ncel ))

! Fractions massiques
ALLOCATE( DOM%ETAT%Y( ncel,nesp ))
ALLOCATE( DOM%ETAT%Y_g( ncel ))
ALLOCATE( DOM%ETAT%Y_s( ncel ))

! Fractions volumiques
ALLOCATE( DOM%ETAT%phi( ncel,nesp ))
ALLOCATE( DOM%ETAT%phi_itp( nfac,nesp ))
ALLOCATE( DOM%ETAT%phi_g( ncel ))
ALLOCATE( DOM%ETAT%phi_g_itp( nfac ))
ALLOCATE( DOM%ETAT%phi_s( ncel ))
ALLOCATE( DOM%ETAT%phi_s_itp( nfac ))

! Masses volumiques
ALLOCATE( DOM%ETAT%rho( ncel ))
ALLOCATE( DOM%ETAT%rho_e( ncel,nesp ))
ALLOCATE( DOM%ETAT%rho_g( ncel ))

! Pressions
IF (DOM%NUM%ADVECTION == 'on') THEN
    ALLOCATE( DOM%ETAT%P( ncel ))
    ALLOCATE( DOM%ETAT%P_itp( nfac ))
    ALLOCATE( DOM%ETAT%grad_P_itp( nfac,3 ))
END IF

! Vitesses de gaz
IF (DOM%NUM%ADVECTION == 'on') THEN
    ALLOCATE( DOM%ETAT%v_g_itp( nfac,3 ))
END IF

! Avancements réactions
IF (DOM%NUM%REACTION == 'on') THEN
    ALLOCATE( DOM%ETAT%w_e( ncel,nesp ))
    ALLOCATE( DOM%ETAT%w_g( ncel ))
    ALLOCATE( DOM%ETAT%wQ( ncel ))
    ALLOCATE( DOM%ETAT%alpha_reac( ncel,nreac ))
    ALLOCATE( DOM%ETAT%alpha_reac0( ncel,nreac ))
    ALLOCATE( DOM%ETAT%rho_rel_0( ncel,nesp ))
END IF

! Variables naturelles pour calcul structure
IF (DOM%NUM%STRUCTURE == 'on') THEN
    ALLOCATE( DOM%ETAT%eps_strain( ncel,6 ) )
    ALLOCATE( DOM%ETAT%eps_strain_itp( nfac,6 ) )
    ALLOCATE( DOM%ETAT%sig_stress( ncel,6 ) )
    ALLOCATE( DOM%ETAT%sig_stress_itp( nfac,6 ) )
END IF
! Matrice de rigidite du systeme structure
IF (DOM%NUM%STRUCTURE == 'on') THEN
    ALLOCATE( DOM%ETAT%Crig( ncel,4 ))
    ALLOCATE( DOM%ETAT%Crig_itp( nfac,4 ))
END IF


! Propriétés du système
ALLOCATE( DOM%ETAT%k( ncel,3 ))
ALLOCATE( DOM%ETAT%k_itp( nfac,3 ))
ALLOCATE( DOM%ETAT%Cp( ncel ))
ALLOCATE( DOM%ETAT%h( ncel ))
ALLOCATE( DOM%ETAT%M( ncel ))
ALLOCATE( DOM%ETAT%eps_rad( nfac ))
ALLOCATE( DOM%ETAT%alpha_rad( nfac ))

! Propriétés du système : ADVECTION
IF (DOM%NUM%ADVECTION == 'on') THEN
    ALLOCATE( DOM%ETAT%h_g( ncel ))
    ALLOCATE( DOM%ETAT%mu( ncel ))
    ALLOCATE( DOM%ETAT%mu_itp( nfac ))
    ALLOCATE( DOM%ETAT%D_itp( nfac ))
    ALLOCATE( DOM%ETAT%Kp( ncel,3 ))
    ALLOCATE( DOM%ETAT%Kp_itp( nfac,3 ))
END IF

! Propriétés du système : ABLATION
IF (DOM%NUM%ABLATION == 'on') THEN
    ALLOCATE( DOM%ETAT%D_SOM( nsom,3 ))    
    ALLOCATE( DOM%ETAT%D_SOM_ABL( nsom,3 ))   
    ALLOCATE( DOM%ETAT%D_SOM_BRUT( nsom,3 ))  
    ALLOCATE( DOM%ETAT%taux_reg( nfac ) )
    ALLOCATE( DOM%ETAT%taux_reg_prec( nfac ) )
END IF

! Criteres de stabilite
ALLOCATE( DOM%ETAT%Fo( ncel ))
ALLOCATE( DOM%ETAT%CFL( ncel ))

END SUBROUTINE dat_bld_etat
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_dat_bld_etat