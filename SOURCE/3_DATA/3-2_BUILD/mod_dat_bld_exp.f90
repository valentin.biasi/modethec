!> Module de construction des données des champs EXPT
MODULE mod_dat_bld_exp
!
USE mod_cst, ONLY : IP, DP, eps_min
USE mod_print, ONLY : print_err
USE mod_algebra, ONLY : realtostr
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Construction des données des champs EXPT
SUBROUTINE dat_bld_exp( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
REAL(DP) :: iiter_real
INTEGER(IP) :: i
INTEGER(IP), DIMENSION(1) :: icel
REAL(DP), ALLOCATABLE, DIMENSION(:) :: dist_capt_cell

!---- Récupérations ---------------------------------------------------!

! Si export actif
IF (DOM%EXPT%export == 1) THEN
    
    ! Nb d'ité. entre chaque pas de calcul
    iiter_real = ABS( DOM%EXPT%dt_exp - FLOOR( DOM%EXPT%dt_exp/DOM%NUM%dt ) * DOM%NUM%dt )
    IF (iiter_real > 10.0_dp*eps_min) THEN
        CALL print_err('La valeur de dt_exp n''est pas multiple de dt.' )
        DOM%EXPT%iiter_exp = 1
    ELSE
        DOM%EXPT%iiter_exp =  INT( DOM%EXPT%dt_exp / DOM%NUM%dt )
    END IF
    
    ! Export aux sommets et aux cellules incompatible
    IF (DOM%EXPT%sommet == 1 .AND. DOM%EXPT%cellule == 1) THEN
        CALL print_err('Export aux cellules et aux sommets incompatible.')
    ELSE IF (DOM%EXPT%sommet == 0 .AND. DOM%EXPT%cellule == 0) THEN
        CALL print_err('Export aux cellules ET export aux sommets inactifs.')
    END IF
    
    ! Si FEmode est actif, l'export ne peut etre que sur les sommets
    IF (DOM%EXPT%FEmode == 0 .AND. DOM%EXPT%cellule == 1) THEN
        CALL print_err('Le mode scatter (FEmode=0) est incompatible')
        CALL print_err('avec l''export aux centres des cellules.')
    END IF

    ! Si les capteurs sont actifs, on enregistre le numéro de la cellule la plus
    ! proche de chaque capteur
    IF (DOM%SUI%sensors == 1) THEN
        DOM%SUI%SENS2CELL(:) = 0
        ALLOCATE (dist_capt_cell(DOM%MLG%ncel))
        DO i = 1,DOM%SUI%nsensors
            dist_capt_cell(:) = 0
            dist_capt_cell(:) = (DOM%MLG%G_XYZ(:,1)-DOM%SUI%COORD_SENSORS(i,1))**2 + &
                                (DOM%MLG%G_XYZ(:,2)-DOM%SUI%COORD_SENSORS(i,2))**2 + &
                                (DOM%MLG%G_XYZ(:,3)-DOM%SUI%COORD_SENSORS(i,3))**2
            icel = MINLOC(dist_capt_cell(:))
            ! On doit prendre icel(1) car la fonction MINLOC renvoit un tableau 
            ! et non un entier
            DOM%SUI%SENS2CELL(i) = icel(1)
        END DO
        DEALLOCATE (dist_capt_cell)
    END IF
    
END IF

END SUBROUTINE dat_bld_exp
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_dat_bld_exp
