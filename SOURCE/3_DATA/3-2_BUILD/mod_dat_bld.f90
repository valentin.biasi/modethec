!> Module de construction des variables principales PAR DOMAINE
MODULE mod_dat_bld
!
USE mod_cst, ONLY : IP
USE mod_dat_bld_etat, ONLY : dat_bld_etat
USE mod_dat_bld_num, ONLY : dat_bld_num
USE mod_dat_bld_rsl, ONLY : dat_bld_rsl
USE mod_dat_bld_phys, ONLY : dat_bld_phys
USE mod_dat_bld_cdt, ONLY : dat_bld_cdt
USE mod_dat_bld_sui, ONLY : dat_bld_sui
USE mod_dat_bld_exp, ONLY : dat_bld_exp
USE mod_dat_bld_cls, ONLY : dat_bld_cls
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Construction des variables principales PAR DOMAINE
SUBROUTINE dat_bld( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM


! Construction ETAT
CALL dat_bld_etat( DOM )

! Construction NUMERIQUE
CALL dat_bld_num( DOM )

! Construction RESOLUTION
CALL dat_bld_rsl( DOM )

! Construction PHYSIQUE
CALL dat_bld_phys( DOM )

! Construction CONDITION
CALL dat_bld_cdt( DOM )

! Construction SUIVI
CALL dat_bld_sui( DOM )

! Construction EXPORT
CALL dat_bld_exp( DOM )

! Construction CLS (Conditions Limites Spéciales)
CALL dat_bld_cls( DOM )

END SUBROUTINE dat_bld
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_dat_bld