!> Module de construction des données des champs CDT
MODULE mod_dat_bld_cdt
!
USE mod_cst, ONLY : IP, DP, eps_min
USE mod_print, ONLY : print_err
USE mod_algebra, ONLY : inttostr
USE mod_deform, ONLY : deform_MAJ_spline
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!> Construction des données des champs CDT
SUBROUTINE dat_bld_cdt( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: nesp
INTEGER(IP) :: ilim, nlim
INTEGER(IP) :: i, j !, compt1, compt2
INTEGER(IP) :: ifac_lim, nfac_lim
INTEGER(IP) :: nfac_limi, isom, nsom
INTEGER(IP) :: nfac_max
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_FACLIM, ID_SOMMET, COMPT_SOMMET
INTEGER(IP) :: nfac_lim_tot
INTEGER(IP) :: id_fac, id_som1, id_som2
REAL(DP), DIMENSION(3) :: som1, som2
REAL(DP), DIMENSION(3) :: axe1, axe2
REAL(DP) :: dist_axe
!
!---- Récupérations ---------------------------------------------------!
nesp = DOM%PHYS%nesp
nlim = DOM%CDT%nlim
nfac_lim = DOM%MLG%nfac_lim
nfac_max = nfac_lim
nsom = DOM%MLG%nsom
!
!---- Allocations -----------------------------------------------------!
ALLOCATE( ID_FACLIM( nfac_max ) )
!
! Nb de limites définies correspond bien au fichier maillage
IF (nlim .ne. DOM%MLG%nlim) THEN
    CALL print_err('Le nombre de limites declarees ne correspond pas au fichier maillage')
END IF
!
! Remplissage du tableau ID_FACLIM pour chaque limite : ID des faces appartenant à ilim
! Boucle sur les limites
DO ilim = 1,nlim
    ! Réinitialisation des valeurs
    ID_FACLIM(:) = 0
    nfac_limi = 0
    
    ! Boucle sur les faces limites
    DO ifac_lim = 1,nfac_lim
        
        ! Si le marquage de la face limite correspond au nom de la ilim limite
        IF (ADJUSTL(DOM%MLG%FACLIM_MARQ(ifac_lim)) == ADJUSTL(DOM%CDT%LIM(ilim)%nom_lim)) THEN
            
            ! On remplit le tableau de face limite de la ilim limite
            nfac_limi = nfac_limi + 1
            ID_FACLIM(nfac_limi) = DOM%MLG%FACLIM(ifac_lim,2)
        END IF
    END DO
    
    ! Pour chaque limite
    ! On remplit le nombre de faces dans ilim
    DOM%CDT%LIM(ilim)%nfac_limi = nfac_limi
    ! Et on attribut le tableau des ID de facs de ilim
    ALLOCATE( DOM%CDT%LIM(ilim)%ID_FACLIMi( nfac_limi ) )
    DOM%CDT%LIM(ilim)%ID_FACLIMi(:) = ID_FACLIM(1:nfac_limi)
    
    ! Vérification que ilim contienne au moins une face
    IF (nfac_limi == 0) THEN
        CALL print_err('La limite no '//TRIM(ADJUSTL(inttostr(ilim)))//' ne semble contenir aucune face.')
        CALL print_err('Veuillez verifier le fichier de parametres.',1)
    END IF

END DO

! Creation pour chaque limite d'un tableau recensant pour chaque sommet interne
! a la limite les sommets adjacent de cette meme limite.
! ...


! Vérification que toutes les faces aient été balayées dans les tableaux ID_FACLIM
nfac_lim_tot = 0
DO ilim = 1,nlim
    nfac_lim_tot = nfac_lim_tot + DOM%CDT%LIM(ilim)%nfac_limi
END DO
IF (nfac_lim_tot /= nfac_lim) THEN
    CALL print_err('Les limites definies dans le fichier de parametres ne permettent')
    CALL print_err('pas de balayer l''ensemble des faces limites.')
END IF


! Réallocation des tableaux de conditions limites à la taille nfac_limi pour pouvoir etre modifié
! par face limite (pour couplage ou CLS)
DO ilim = 1,nlim

    nfac_limi = DOM%CDT%LIM(ilim)%nfac_limi

    CALL reallocate_boundary_arrays( DOM%CDT%LIM(ilim)%Fimp, nfac_limi )
    CALL reallocate_boundary_arrays( DOM%CDT%LIM(ilim)%Timp, nfac_limi )
    CALL reallocate_boundary_arrays( DOM%CDT%LIM(ilim)%T_conv, nfac_limi )
    CALL reallocate_boundary_arrays( DOM%CDT%LIM(ilim)%h_conv, nfac_limi )
    CALL reallocate_boundary_arrays( DOM%CDT%LIM(ilim)%T_rad, nfac_limi )

    CALL reallocate_boundary_arrays( DOM%CDT%LIM(ilim)%Pimp, nfac_limi )
    CALL reallocate_boundary_arrays( DOM%CDT%LIM(ilim)%Dimp, nfac_limi )

END DO


! Vérification que les faces conditions axe pour le calcul structure
! 2D_AXI sont bien sur l'axe de revolution
IF (DOM%NUM%STRUCTURE == 'on') THEN
    ! Pour l'ensemble des limites de type 'axe'
    DO ilim = 1,nlim
        IF (DOM%CDT%LIM(ilim)%struct_typ == 'axe') THEN
    
            ! Si on n'est pas en 2D_AXI => probleme
            IF (DOM%MLG%dim_simu /= '2D_AXI') THEN
                CALL print_err('Il existe des conditions structure de type axe')
                CALL print_err('alors que la dimension n''est pas 2D_AXI',1)
            END IF
            
            ! Axe de symmetrie
            axe1 = DOM%MLG%AXE_2D(1:3)
            axe2 = DOM%MLG%AXE_2D(4:6)
            
            ! Boucle sur les faces limites de type 'axe'
            DO ifac_lim = 1,DOM%CDT%LIM(ilim)%nfac_limi
                
                ! Indice de la face
                id_fac = DOM%CDT%LIM(ilim)%ID_FACLIMi(ifac_lim)
                
                ! Indice des sommets associes
                id_som1 = DOM%MLG%FAC(id_fac,3)
                id_som2 = DOM%MLG%FAC(id_fac,4)
                
                ! Position des sommets 1 et 2
                som1 = DOM%MLG%SOMMET(id_som1,:)
                som2 = DOM%MLG%SOMMET(id_som2,:)
                
                ! Distance entre le vecteur (axe2 - axe1) 
                ! et le vecteur (som1 - axe1)
                dist_axe = (axe2(1) - axe1(1)) * (som1(2) - axe1(2))
                dist_axe = dist_axe  - ((axe2(2) - axe1(2)) * (som1(1) - axe1(1)))
                ! Si distance non-nulle => ERREUR
                IF ( ABS(dist_axe) >= 2.0_dp*eps_min ) THEN
                    CALL print_err('Certains sommets limites de type ''axe''')
                    CALL print_err('ne sont pas sur l''axe de symetrie',1)
                END IF
                
                ! Idem sommet 2 :
                ! Distance entre le vecteur (axe2 - axe1) 
                ! et le vecteur (som2 - axe1)
                dist_axe = (axe2(1) - axe1(1)) * (som2(2) - axe1(2))
                dist_axe = dist_axe  - ((axe2(2) - axe1(2)) * (som2(1) - axe1(1)))
                ! Si distance non-nulle => ERREUR
                IF ( ABS(dist_axe) >= 2.0_dp*eps_min ) THEN
                    CALL print_err('Certains sommets limites de type ''axe''')
                    CALL print_err('ne sont pas sur l''axe de symetrie',1)
                END IF
                
            END DO    
    END IF
    END DO
END IF

! Données pour l'ablation 3D
IF(DOM%NUM%ABLATION == 'on') THEN 
    ! On garde la structure en différenciant les CL car cela sera utile pour le cas 2D axi
    DO ilim = 1, nlim
        nfac_limi = DOM%CDT%LIM(ilim)%nfac_limi  
        ALLOCATE ( DOM%CDT%LIM( ilim )%ACTIVATION_ABLA (nfac_limi) )
        DOM%CDT%LIM( ilim )%ACTIVATION_ABLA = 0        
        IF (DOM%CDT%LIM( ilim )%abla_typ == "meca") THEN
            ALLOCATE ( DOM%CDT%LIM( ilim )%Vreg_meca (nfac_limi) )
        END IF
    END DO
END IF

! Creation des tableaux SOM_ADJ_LIMi pour chaque limite si il y a ablation
! ! 1: n° som ||| 2: nb som adj ||| 3: n° somadj1 ||| 4: n° somadj2 |||...
! et du vecteur SOM_COINi contenant l'id des 2 sommets d'extremite de la limite courante

!!!!!!!!!!!!!!!!!!! MODIFICATION POUR L ABLATION 3D!!!!!!!!!!!!!!!!!!!!!!!

! IF(DOM%NUM%ABLATION == 'on') THEN 

!     DO ilim = 1, nlim    

!         nfac_limi = DOM%CDT%LIM(ilim)%nfac_limi         
!         ALLOCATE(ID_SOMMET(nsom))
!         ID_SOMMET = 0
!         i=1
!         j=1
        
!         ! Creation de SOM_COINi 
!         ALLOCATE (COMPT_SOMMET(nsom))
!         COMPT_SOMMET = 0
!         DO ifac_lim = 1, nfac_limi
!             id_som1 = DOM%MLG%FAC(DOM%CDT%LIM(ilim)%ID_FACLIMi(ifac_lim),3)
!             id_som2 = DOM%MLG%FAC(DOM%CDT%LIM(ilim)%ID_FACLIMi(ifac_lim),4)
!             COMPT_SOMMET(id_som1) = COMPT_SOMMET(id_som1) +1
!             COMPT_SOMMET(id_som2) = COMPT_SOMMET(id_som2) +1
!         END DO
                
!         IF (count(COMPT_SOMMET(:) == 1) == 2) THEN ! cas de la limite ouverte
!             DO isom = 1 , nsom
!                 IF (COMPT_SOMMET(isom) == 1) THEN
!                     DOM%CDT%LIM(ilim)%SOM_COINi(i) = isom
!                     i = i+1
!                 END IF                
!             END DO
!             ! allocation du tableau SOM_ADJ_LIMi
!             ALLOCATE (DOM%CDT%LIM(ilim)%SOM_ADJ_LIMi(nfac_limi-1,4))
!             DOM%CDT%LIM(ilim)%SOM_ADJ_LIMi(:,:) = 0
!         ELSE  ! cas de la limite fermee sur elle-meme
!             DOM%CDT%LIM(ilim)%SOM_COINi( : ) = 0
!             ! allocation du tableau SOM_ADJ_LIMi
!             ALLOCATE (DOM%CDT%LIM(ilim)%SOM_ADJ_LIMi(nfac_limi,4))
!             DOM%CDT%LIM(ilim)%SOM_ADJ_LIMi( :,: ) = 0
!         END IF
        
!         DEALLOCATE (COMPT_SOMMET)
        
!         i = 1
!         ! Creation de SOM_ADJ_LIMi
!         DO ifac_lim = 1, nfac_limi
!             id_som1 = DOM%MLG%FAC( DOM%CDT%LIM( ilim )%ID_FACLIMi( ifac_lim ),3 )
!             id_som2 = DOM%MLG%FAC( DOM%CDT%LIM( ilim )%ID_FACLIMi( ifac_lim ),4 )
!             IF( count( DOM%CDT%LIM( ilim )%SOM_COINi( : )== id_som1 ) < 0.5 ) THEN            
!                 ! Premier cas : Le sommet 1 n'est pas encore dans le tableau
!                 IF( count( DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( :,1 ) == id_som1 ) < 1 ) THEN
!                     DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( i,1 ) = id_som1                
!                     ! inscription de l'id du sommet adjacent
!                     DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( i,DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( i,2 )+3 ) = id_som2
!                     ! MAJ du nombre de sommets adjacents
!                     DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( i,2 ) = DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( i,2 )+1
!                     ! inscription de la relation id_sommet_index dans le tableau provisoire ID_SOMMET
!                     ID_SOMMET( id_som1 ) = i
!                     i = i+1
!                 ! Deuxieme cas : Le sommet 1 est deja dans le tableau
!                 ELSE IF( count( DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( :,1 ) == id_som1 ) > 0.5 ) THEN
!                     DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( ID_SOMMET( id_som1 ),DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( ID_SOMMET( id_som1 ),2 )+3 ) = id_som2
!                     DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( ID_SOMMET( id_som1 ),2 ) = DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( ID_SOMMET( id_som1 ),2 )+1
!                 ELSE ! Erreur presence d'un doublon de ligne dans le tableau
!                     CALL print_err ("Le solveur ablation a rencontre une erreur dans le recensement des sommets adjacents de chaques sommets")
!                 END IF
!             END IF
                
!             ! idem pour le sommet 2
!             IF( count( DOM%CDT%LIM( ilim )%SOM_COINi( : ) == id_som2 ) < 0.5 ) THEN  
!                 IF( count( DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( :,1 ) == id_som2 ) < 0.5 ) THEN
!                     DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( i,1 ) = id_som2                
!                     ! inscription de l'id du sommet adjacent
!                     DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( i,DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( i,2 )+3) = id_som1
!                     ! MAJ du nombre de sommets adjacents
!                     DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( i,2 ) = DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( i,2 )+1
!                     ! inscription de la relation id_sommet_index dans le tableau provisoire ID_SOMMET
!                     ID_SOMMET( id_som2 ) = i
!                     i = i+1
!                 ! Le sommet 2 est deja dans le tableau
!                 ELSE IF( count( DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( :,1 ) == id_som2 ) > 0.5 ) THEN
!                     DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( ID_SOMMET( id_som2 ),DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( ID_SOMMET( id_som2 ),2 )+3 ) = id_som1
!                     DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( ID_SOMMET( id_som2 ),2 ) = DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( ID_SOMMET( id_som2 ),2 )+1
!                 ELSE ! Erreur presence d'un doublon de ligne dans le tableau
!                     CALL print_err ("Le solveur ablation a rencontre une erreur dans le recensement des sommets adjacents de chaques sommets")
!                 END IF
!             END IF
!         END DO
!         DEALLOCATE ( ID_SOMMET )
    
!         ! ALLOCATION DU TABLEAU SPLINEi et du PSi (points secondaire de la limite i)
!         ALLOCATE ( DOM%CDT%LIM( ilim )%SPLINEi( nfac_limi,10 ) )
!         ALLOCATE ( DOM%CDT%LIM( ilim )%IDSPLINEi( nfac_limi,3 ) )
!         ALLOCATE ( DOM%CDT%LIM( ilim )%PSi( DOM%NUM%pas_ech*nfac_limi + 1,3 ) )
!         ALLOCATE ( DOM%CDT%LIM( ilim )%PS2FACLIMi( DOM%NUM%pas_ech*nfac_limi + 1,3 ) )
!         ALLOCATE ( DOM%CDT%LIM( ilim )%PP2PSi( nfac_limi+1,3 ) )
!         ALLOCATE ( DOM%CDT%LIM( ilim )%ACTIVATION_ABLA (nfac_limi) )
!         DOM%CDT%LIM( ilim )%ACTIVATION_ABLA = 0
!         IF (DOM%CDT%LIM( ilim )%abla_typ == "meca") THEN
!             ALLOCATE ( DOM%CDT%LIM( ilim )%Vreg_meca (nfac_limi) )
!         END IF
        
!         ! Initialisation de l'interpolation par spline
!         CALL deform_MAJ_spline( DOM, ilim )
    
!     END DO
! END IF

END SUBROUTINE dat_bld_cdt
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de reallocation des tableaux ARRAY(1) définit
! à la récupération par un nouveau tableau de taille n
SUBROUTINE reallocate_boundary_arrays( ARRAY, n )
!
REAL(DP), ALLOCATABLE, DIMENSION(:) :: ARRAY
INTEGER(IP), INTENT(IN) :: n
REAL(DP) :: tmp_value

! Test de lecture correcte, sinon allocation et valeur par défaut à 0
IF (.NOT.(ALLOCATED(ARRAY))) THEN
    CALL print_err('A boundary condition seems missing')
    ALLOCATE( ARRAY(1) )
    ARRAY(1) = 0.0_dp
END IF

! Reallocation via var. intermédiaire
tmp_value = ARRAY(1)
DEALLOCATE(ARRAY)
ALLOCATE( ARRAY( n ) )
ARRAY(:) = tmp_value

END SUBROUTINE reallocate_boundary_arrays
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_dat_bld_cdt