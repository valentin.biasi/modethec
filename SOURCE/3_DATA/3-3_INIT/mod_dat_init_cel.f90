!> Module d'initialisation PAR DOMAINE de l'état aux cellules
MODULE mod_dat_init_cel
!
USE mod_cst, ONLY : IP, DP, tinym, R_GP, eps_min
USE mod_prop, ONLY : prop_M, prop_rho_e
USE mod_print, ONLY : print_err
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Initialisation PAR DOMAINE de l'état du système.
!> Pour les variables naturelles aux cellules
!> \warning Traite seulement les etats uniformes actuellement
SUBROUTINE dat_init_cel( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: iesp, nesp
INTEGER(IP) :: igaz, ngaz, nsol, idom
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_gaz, ID_sol
REAL(DP) :: M_i
!
!---- Récupérations ---------------------------------------------------!
nesp = DOM%PHYS%nesp
ngaz = DOM%PHYS%ngaz
nsol = DOM%PHYS%nsol
!
!---- Allocations -----------------------------------------------------!
ALLOCATE( ID_gaz( ngaz ) )
ID_gaz = DOM%PHYS%ID_gaz
ALLOCATE( ID_sol( nsol ) )
ID_sol = DOM%PHYS%ID_sol
!
!
! Initialisation des frac. volumiques 
DO iesp = 1,nesp
    DO idom = 1,DOM%CDT%ndom
        WHERE (DOM%MLG%CEL2DOM_MARQ == DOM%CDT%INI(idom)%nom_dom)
            DOM%ETAT%phi(:,iesp) = DOM%CDT%INI(idom)%phi_ini(iesp)
        END WHERE
    END DO
END DO

!
! Frac. volumiques de solide et gaz
DOM%ETAT%phi_s = SUM( DOM%ETAT%phi(:,ID_sol) , 2 )
DOM%ETAT%phi_g = SUM( DOM%ETAT%phi(:,ID_gaz) , 2 )
!
! Vérification : somme des frac. vol == 1
IF (ANY( (DOM%ETAT%phi_s+DOM%ETAT%phi_g-1.0_dp) > 2.0_dp*eps_min )) THEN
    CALL print_err('La somme des fractions volumiques n''est pas egale a 1',1)
END IF
!
! Evaluation de la masse vol des especes solides
CALL prop_rho_e( DOM )
!
! Evaluation de la masse mol du mélange gazeux
!CALL prop_M( DOM )
DOM%ETAT%M(:) = 0.0_dp
DO igaz = 1,ngaz
    ! Masse mol de l'espece gaz igaz
    iesp = ID_gaz(igaz)
    M_i = DOM%PHYS%ESP(iesp)%M
    
    DOM%ETAT%M(:) = DOM%ETAT%M(:) + DOM%ETAT%phi(:,iesp) * M_i / DOM%ETAT%phi_g

END DO
!
! Calcul de la masse vol. du mélange gazeux avec la loi des gaz parfaits
! rho_g = P*M / R*T
IF (DOM%NUM%ADVECTION == 'on') THEN
    DOM%ETAT%rho_g(:) = DOM%ETAT%P(:) * &
                       DOM%ETAT%M(:) / &
                       DOM%ETAT%T(:) / R_GP
ELSE
    DOM%ETAT%rho_g(:) = DOM%CDT%INI(1)%Pini * &
                       DOM%ETAT%M(:) / &
                       DOM%ETAT%T(:) / R_GP
END IF
!
! Masse vol des especes gazeuses (voir hypothèses des gaz parfaits, vol. mol cst...)
! rho_i = M_i / M * rho_g
DO igaz = 1,ngaz
    ! Masse mol de l'espece gaz igaz
    iesp = ID_gaz(igaz)
    M_i = DOM%PHYS%ESP(iesp)%M
    
    ! calcul de rho_igaz
    WHERE (DOM%ETAT%M(:) > 2.0_dp*eps_min)
        DOM%ETAT%rho_e(:,iesp) = DOM%ETAT%rho_g * M_i / DOM%ETAT%M(:)
    END WHERE

END DO
!
! Masse des cellules par espèce
! m_i = rho_i * phi_i * Volume
DOM%RSL%m_e = DOM%ETAT%rho_e * DOM%ETAT%phi * SPREAD(DOM%MLG%VOLUME,2,nesp)
!
! Masse totale des cellules
! m =somme( m_i )
DOM%RSL%m = SUM( DOM%RSL%m_e , 2 )
!
! Masse vol. apparente par cellule
! rho = m / Volume
DOM%ETAT%rho = DOM%RSL%m / DOM%MLG%VOLUME
!
! Frac. mass. des cellules
! Y_i = m_i / m
DOM%ETAT%Y = DOM%RSL%m_e / SPREAD(DOM%RSL%m,2,nesp)
!
! Frac. mass. des cellules (gaz et solide)
DOM%ETAT%Y_s = SUM( DOM%ETAT%Y(:,ID_sol) , 2 )
DOM%ETAT%Y_g = SUM( DOM%ETAT%Y(:,ID_gaz) , 2 )
!
!
! Init des masses volumiques relatives a l'etat initial pour calcul des avancements réactions
IF (DOM%NUM%REACTION == 'on') THEN
    DO iesp = 1,nesp
       DOM%ETAT%rho_rel_0(:,iesp) = DOM%ETAT%rho_e(:,iesp) * DOM%ETAT%phi(:,iesp)
    END DO
END IF
!
! On sauvegarde les masses et volumes initiaux dans le cas de l'ablation
IF (DOM%NUM%ABLATION == 'on') THEN
    DOM%RSL%m_ini = DOM%RSL%m
    DOM%RSL%volume_ini = DOM%MLG%VOLUME
END IF
!
END SUBROUTINE dat_init_cel
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_dat_init_cel
