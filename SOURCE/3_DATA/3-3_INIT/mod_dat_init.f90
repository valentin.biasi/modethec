!> Module d'initialisation PAR DOMAINE de l'état du système
MODULE mod_dat_init
!
USE mod_cst, ONLY : IP, DP
USE mod_print, ONLY : print_err
USE mod_prop, ONLY : cyc_prop_D, cyc_prop_A
USE mod_solve_C, ONLY : solve_C_init, solve_C
USE mod_dat_init_cel, ONLY : dat_init_cel
USE mod_dat_init_fac, ONLY : dat_init_fac
USE mod_interpolate, ONLY : interp_T, interp_P
USE mod_expt_ini, ONLY : expt_ini
USE mod_expt, ONLY : expt
USE mod_expt_hdf5, ONLY : expt_hdf5_ini, expt_hdf5, expt_hdf5_backup_ini
USE mod_dat_init_restart, ONLY : dat_init_restart
USE mod_deform3d, ONLY : deform3D_ntag_init_raideur, deform3D_int_init_raideur
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Initialisation PAR DOMAINE de l'état du système
!> \todo Faire les conditions limites spécifiques
SUBROUTINE dat_init( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: idom

! Initialisation des températures (champ uniforme)
DOM%ETAT%T_ref = -1.0_dp ! Pour verifier que tout est alloué
DO idom = 1,DOM%CDT%ndom
    WHERE (DOM%MLG%CEL2DOM_MARQ == DOM%CDT%INI(idom)%nom_dom)
        DOM%ETAT%T_ref = DOM%CDT%INI(idom)%Tini
    END WHERE
END DO
IF (ANY(DOM%ETAT%T_ref < 0.0_dp)) THEN
    CALL print_err("Some cells are not associated to a domain")
    CALL print_err("Please check the domains names in the initial conditions section", 1)
END IF
DOM%ETAT%T = DOM%ETAT%T_ref

! Interpolation et calcul des gradients pour températures
CALL interp_T(  DOM%ETAT%T, &              ! Températures aux cel.
                DOM%MLG%ncel, &            ! Nombre de cel.
                DOM%ETAT%T_itp, &          ! Interpolées aux faces
                DOM%ETAT%grad_T_itp, &     ! Gradients aux faces
                DOM%MLG%nfac, &            ! Nombre de faces
                DOM%MLG, &                 ! Type données maillage
                DOM%CDT )                  ! Type données conditions limites

! Initialisation des pressions (champ uniforme)
IF (DOM%NUM%ADVECTION == 'on') THEN
    ! Aux cellules
    DO idom = 1,DOM%CDT%ndom
        WHERE (DOM%MLG%CEL2DOM_MARQ == DOM%CDT%INI(idom)%nom_dom)
            DOM%ETAT%P = DOM%CDT%INI(idom)%Pini
        END WHERE
    END DO
    
    ! Aux faces
    CALL interp_P(  DOM%ETAT%P, &              ! Pressions aux cel.
                    DOM%MLG%ncel, &            ! Nombre de cel.
                    DOM%ETAT%P_itp, &          ! Interpolées aux faces
                    DOM%ETAT%grad_P_itp, &     ! Gradients aux faces
                    DOM%MLG%nfac, &            ! Nombre de faces
                    DOM%MLG, &                 ! Type données maillage
                    DOM%CDT )                  ! Type données conditions limites
END IF


! Init. des autres variables naturelles au centre des cellules
CALL dat_init_cel( DOM )

!! Init. des autres variables naturelles au centre des faces
CALL dat_init_fac( DOM )

! Initialisation des proprietes (utile uniquement pour l'export)
CALL cyc_prop_D( DOM )
IF (DOM%NUM%ADVECTION == 'on') THEN
    CALL cyc_prop_A( DOM )
END IF

! Init des reactions a l'etat d'avancement zero
IF (DOM%NUM%REACTION == 'on') THEN
    DOM%ETAT%alpha_reac(:,:) = 0.0_dp
END IF

! Mise a zero des energies conservees par cellule
DOM%RSL%E(:) = 0.0_dp


! Initialisation des couplages CWIPI
IF (DOM%NUM%COUPLAGE == 'on') THEN
    CALL solve_C_init( DOM )

    ! Echange à l'état initial (utile ?)
    CALL solve_C ( DOM )
END IF

! Initialisation des raideurs pour l'ablation (deformation de maillage)
IF ( DOM%NUM%ABLATION == 'on' ) THEN
    IF ( DOM%MLG%dim_simu == '3D' ) CALL deform3D_ntag_init_raideur( DOM )
    CALL deform3D_int_init_raideur( DOM )
END IF

! Si export actif
IF (DOM%EXPT%export == 1) THEN

    ! Demande d'initialisation des exports ...
    CALL expt_ini( DOM )
    ! ... et export de l'etat initial si demande
    IF ( DOM%EXPT%exp_ini == 1 .AND. DOM%NUM%restart_backup == 0) THEN
        CALL expt( DOM )
    END IF
    
END IF


! Si export des variables globales en HDF5 actif
IF (DOM%SUI%file_hdf5 == 1) THEN
    ! Creation du fichier et de sa structure mais plein de zeros
    CALL expt_hdf5_ini( DOM )
    
    !... et export HDF5 de l'etat initial
    CALL expt_hdf5( DOM )
END IF

! Si calcul de reprise, initialisation des champs avec le
! fichier backup (énergie, masse, alpha_reac, U_deplacement, ...) 
IF (DOM%NUM%restart_backup == 1) THEN
    ! Lecture du fichier backup et initialisation des champs
    CALL dat_init_restart( DOM )
    CALL expt( DOM )

END IF

! Si backup en HDF5 actif
IF (DOM%SUI%backup == 1) THEN
    ! Creation du fichier et de sa structure mais plein de zeros
    CALL expt_hdf5_backup_ini( DOM )
    ! On n'appelle pas ici expt_hdf5_backup car c'est inutile de 
    ! sauvegarder l'etat initial
END IF


END SUBROUTINE dat_init
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_dat_init