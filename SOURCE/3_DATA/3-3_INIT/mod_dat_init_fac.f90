!> Module d'initialisation PAR DOMAINE de l'état aux faces
MODULE mod_dat_init_fac
!
USE mod_cst, ONLY : IP, DP, eps_min
USE mod_print, ONLY : print_err
USE mod_interpolate, ONLY : interp_fac
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Initialisation PAR DOMAINE de l'état du système.
!> Pour les variables naturelles (nécessaires) aux faces
SUBROUTINE dat_init_fac( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
INTEGER(IP) :: iesp, nesp
INTEGER(IP) :: ncel
INTEGER(IP) :: nfac
INTEGER(IP) :: ngaz, nsol
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_gaz, ID_sol
!
!---- Récupérations ---------------------------------------------------!
nesp = DOM%PHYS%nesp
ncel = DOM%MLG%ncel
nfac = DOM%MLG%nfac
ngaz = DOM%PHYS%ngaz
nsol = DOM%PHYS%nsol
!
!---- Allocations -----------------------------------------------------!
ALLOCATE( ID_gaz( ngaz ) )
ID_gaz = DOM%PHYS%ID_gaz
ALLOCATE( ID_sol( nsol ) )
ID_sol = DOM%PHYS%ID_sol
!
!
! Interpolations pour chaque espèce
DO iesp = 1,nesp

    ! Interpolation et calcul des gradients pour fractions volumiques
    CALL interp_fac(  DOM%ETAT%phi(:,iesp), &  ! Doonées aux cel.
            DOM%ETAT%phi_itp(:,iesp), &     ! Interpolées aux faces
            nfac, &                         ! Nombre de faces
            ncel, &                         ! Nombre de cellules
            DOM%MLG%nsom, &                 ! Nombre de sommets
            DOM%MLG%FAC_ITP(:), &           ! Type interp. faces
            DOM%MLG%CEL_ITP(:), &           ! Type interp. cellules
            DOM%MLG%SOM_ITP(:), &           ! Type interp. sommets
            DOM%MLG%dim_simu )              ! Dimension

END DO


! Frac. volumiques de solide et gaz aux faces
DOM%ETAT%phi_s_itp = SUM( DOM%ETAT%phi_itp(:,ID_sol) , 2 )
DOM%ETAT%phi_g_itp = SUM( DOM%ETAT%phi_itp(:,ID_gaz) , 2 )
!
END SUBROUTINE dat_init_fac
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_dat_init_fac
