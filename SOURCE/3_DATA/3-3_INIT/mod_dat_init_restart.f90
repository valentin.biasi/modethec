!> Module d'initialisation l'état du système pour les calculs de reprise
MODULE mod_dat_init_restart
!
USE mod_cst, ONLY : DP, IP
!
USE mod_dom
USE mod_update, ONLY : cyc_update_DR
USE mod_mesh_recpt, ONLY : mesh_recpt
USE mod_mesh_cpt, ONLY : mesh_cpt
USE mod_ablation, ONLY : cyc_ablation
USE mod_print, ONLY : print_err
USE mod_deform, ONLY : deform_maj_spline
USE mod_mesh_volu, ONLY : mesh_volu
!
USE HDF5
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Initialisation de l'état du système pour un calcul de reprise
SUBROUTINE dat_init_restart( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE(typ_dom) :: DOM
!
CHARACTER (len=32) :: filename ! Nom du fichier de backup
CHARACTER(len=64) :: dataset_name   ! Nom du dataset a recuperer
!
INTEGER(IP) :: file_id                              ! Identifiant fichier
INTEGER(IP) :: error                                ! Error flag
INTEGER(IP) :: dataset_id                           ! Identifiant Dataset
INTEGER(HSIZE_T), DIMENSION(2) :: dims              ! Datasets dimensions
!
REAL, ALLOCATABLE, DIMENSION(:,:) :: U_restart ! Tableau de valeur
INTEGER(IP), DIMENSION(5,1) :: solver_choices ! Tableau des choix de solver
INTEGER(IP) :: ncel, j
!
CHARACTER (len=32) :: varname ! Nom de l'atttribut
CHARACTER (len=32) :: attr ! Nom de l'atttribut
INTEGER(HID_T) :: attr_id ! Attribute identifier
INTEGER(HID_T) :: atype_id      ! Attribute Dataspace identifier
!
! Ouverture interface fortran si ce n'est pas deja fait dans expt_hdf5_ini
IF (DOM%SUI%file_hdf5 == 0) THEN
    CALL h5open_f(error)
END IF    

filename = 'backup_' // ADJUSTL(DOM%file_param)
filename = filename(1:LEN(TRIM(filename))-4) // '.hdf'

! Nombre de cellules
ncel = DOM%MLG%ncel

! Open an existing file.
CALL h5fopen_f (filename, H5F_ACC_RDWR_F, file_id, error)

!----------------------------------------------------------------------!
!--- Informations sur la simulation  ----------------------------------!
! On vérifie que le fichier de restart est "compatible" avec la simulation
! demandées. On regarde les options "solver", le nombre de cellule, de 
! réactions, d'espèces.

ALLOCATE (U_restart(5,1))
U_restart(:,:) = 0
solver_choices(:,:) = 0
IF (DOM%NUM%DIFFUSION == 'on') solver_choices(1,1) = 1
IF (DOM%NUM%REACTION == 'on') solver_choices(2,1) = 1
IF (DOM%NUM%STRUCTURE == 'on') solver_choices(3,1) = 1
IF (DOM%NUM%ABLATION == 'on') solver_choices(4,1) = 1
IF (DOM%NUM%COUPLAGE == 'on') solver_choices(5,1) = 1

dims(1) = 1
dims(2) = 1 

! Create datatype for the attribute.
CALL h5tcopy_f(H5T_NATIVE_CHARACTER, atype_id, error)
CALL h5tset_size_f(atype_id, 32, error)

! Pour les solveurs
varname = 'DIFFUSION'
! Open dataset attribute.
CALL h5aopen_name_f(file_id, varname, attr_id, error)
! Write the attribute data.
CALL h5aread_f(attr_id, atype_id, attr, dims, error)
! Close the attribute.
CALL h5aclose_f(attr_id, error)
IF (attr == 'on') THEN
    U_restart(1,1) = 1
END IF
varname = 'REACTION'
! Open dataset attribute.
CALL h5aopen_name_f(file_id, varname, attr_id, error)
! Write the attribute data.
CALL h5aread_f(attr_id, atype_id, attr, dims, error)
! Close the attribute.
CALL h5aclose_f(attr_id, error)
IF (attr == 'on') THEN
    U_restart(2,1) = 1
END IF
varname = 'STRUCTURE'
! Open dataset attribute.
CALL h5aopen_name_f(file_id, varname, attr_id, error)
! Write the attribute data.
CALL h5aread_f(attr_id, atype_id, attr, dims, error)
! Close the attribute.
CALL h5aclose_f(attr_id, error)
IF (attr == 'on') THEN
    U_restart(3,1) = 1
END IF
varname = 'ABLATION'
! Open dataset attribute.
CALL h5aopen_name_f(file_id, varname, attr_id, error)
! Write the attribute data.
CALL h5aread_f(attr_id, atype_id, attr, dims, error)
! Close the attribute.
CALL h5aclose_f(attr_id, error)
IF (attr == 'on') THEN
    U_restart(4,1) = 1
END IF
varname = 'COUPLAGE'
! Open dataset attribute.
CALL h5aopen_name_f(file_id, varname, attr_id, error)
! Write the attribute data.
CALL h5aread_f(attr_id, atype_id, attr, dims, error)
! Close the attribute.
CALL h5aclose_f(attr_id, error)
IF (attr == 'on') THEN
    U_restart(5,1) = 1
END IF

DO j = 1,5 
        IF (U_restart(j,1) /= solver_choices(j,1)) CALL print_err('Les solveurs du fichier de reprise sont diférents &
            de ceux du fichier prm.', 1)
END DO  

DEALLOCATE(U_restart) 

dims(1) = 1
dims(2) = 1
ALLOCATE (U_restart(1,1))

! Nombre de cellules
varname = 'ncel'
! Open dataset attribute.
CALL h5aopen_name_f(file_id, varname, attr_id, error)
! Write the attribute data.
CALL h5aread_f(attr_id, atype_id, attr, dims, error)
! Close the attribute.
CALL h5aclose_f(attr_id, error)
READ(attr,*) U_restart
IF (U_restart(1,1) /= ncel) CALL print_err('Le nombre de cellules du fichier de reprise &
    n''est pas compatible avec le fichier prm.', 1)

! Nombre de reaction
IF ( DOM%NUM%REACTION == 'on' ) THEN
    varname = 'nreac'
    ! Open dataset attribute.
    CALL h5aopen_name_f(file_id, varname, attr_id, error)
    ! Write the attribute data.
    CALL h5aread_f(attr_id, atype_id, attr, dims, error)
    ! Close the attribute.
    CALL h5aclose_f(attr_id, error)
    READ(attr,*) U_restart
    IF (U_restart(1,1) /= DOM%PHYS%nreac) CALL print_err('Le nombre de cellules du fichier de reprise &
        n''est pas compatible avec le fichier prm.', 1)
END IF

! Nombre d'especes
varname = 'nesp'
! Open dataset attribute.
CALL h5aopen_name_f(file_id, varname, attr_id, error)
! Write the attribute data.
CALL h5aread_f(attr_id, atype_id, attr, dims, error)
! Close the attribute.
CALL h5aclose_f(attr_id, error)
READ(attr,*) U_restart
IF (U_restart(1,1) /= DOM%PHYS%nesp) CALL print_err('Le nombre d''especes du fichier de reprise &
    n''est pas compatible avec le fichier prm.', 1)

! Temps initial
varname = 'tf'
! Open dataset attribute.
CALL h5aopen_name_f(file_id, varname, attr_id, error)
! Write the attribute data.
CALL h5aread_f(attr_id, atype_id, attr, dims, error)
! Close the attribute.
CALL h5aclose_f(attr_id, error)
READ(attr,*) U_restart
DOM%NUM%ti = U_restart(1,1)

DEALLOCATE (U_restart)    

! Dimension des tableaux
dims(1) = 1
dims(2) = ncel

!----------------------------------------------------------------------!
!--- ENERGY  ----------------------------------------------------------!
dataset_name = 'Energy'

! Open an existing dataset.
CALL h5dopen_f(file_id, dataset_name, dataset_id, error)

! Read the dataset.
dims(1) = 1
ALLOCATE (U_restart(dims(1),ncel))
CALL h5dread_f(dataset_id, H5T_NATIVE_REAL, U_restart, dims, error)
DOM%RSL%E(:) = U_restart(1,:)
DEALLOCATE (U_restart)

! Close the dataset.
CALL h5dclose_f(dataset_id, error)

!----------------------------------------------------------------------!
!--- MASS  ----------------------------------------------------------!
dataset_name = 'Mass'

! Open an existing dataset.
CALL h5dopen_f(file_id, dataset_name, dataset_id, error)

! Read the dataset.
dims(1) = DOM%PHYS%nesp
ALLOCATE (U_restart(dims(1),ncel))
CALL h5dread_f(dataset_id, H5T_NATIVE_REAL, U_restart, dims, error)
DO j = 1,DOM%PHYS%nesp
    DOM%RSL%m_e(:,j) = U_restart(j,:)
END DO
DEALLOCATE (U_restart)

! Close the dataset.
CALL h5dclose_f(dataset_id, error)

!----------------------------------------------------------------------!
!--- alpha_reac  ------------------------------------------------------!
IF ( DOM%NUM%REACTION == 'on' ) THEN
    dataset_name = 'alpha_reac'

    ! Open an existing dataset.
    CALL h5dopen_f(file_id, dataset_name, dataset_id, error)

    ! Read the dataset.
    dims(1) = DOM%PHYS%nreac
    ALLOCATE (U_restart(dims(1),ncel))
    CALL h5dread_f(dataset_id, H5T_NATIVE_REAL, U_restart, dims, error)
    DO j = 1,DOM%PHYS%nreac
        DOM%ETAT%alpha_reac(:,j) = U_restart(j,:)
    END DO
    DEALLOCATE (U_restart)

    ! Close the dataset.
    CALL h5dclose_f(dataset_id, error)
END IF

!----------------------------------------------------------------------!
!--- Deplacement de la structure solide  ------------------------------!
IF ( DOM%NUM%STRUCTURE == 'on' ) THEN
    dataset_name = 'Uxyz'

    ! Open an existing dataset.
    CALL h5dopen_f(file_id, dataset_name, dataset_id, error)

    ! Read the dataset.
    dims(1) = 3
    ALLOCATE (U_restart(dims(1),ncel))
    CALL h5dread_f(dataset_id, H5T_NATIVE_REAL, U_restart, dims, error)
    DO j = 1,3
        DOM%RSL%U(:,j) = U_restart(j,:)
    END DO
    DEALLOCATE (U_restart)

    ! Close the dataset.
    CALL h5dclose_f(dataset_id, error)
END IF

!----------------------------------------------------------------------!
!--- Ablation de la structure solide  ---------------------------------!
IF ( DOM%NUM%ABLATION == 'on' ) THEN
    dataset_name = 'CoordXYZ'

    ! Open an existing dataset.
    CALL h5dopen_f(file_id, dataset_name, dataset_id, error)

    ! Read the dataset.
    dims(1) = 3
    dims(2) = DOM%MLG%nsom
    ALLOCATE (U_restart(dims(1),DOM%MLG%nsom))
    CALL h5dread_f(dataset_id, H5T_NATIVE_REAL, U_restart, dims, error)
    DO j = 1,3
        DOM%MLG%SOMMET(:,j) = U_restart(j,:)
    END DO
    DEALLOCATE (U_restart)

    ! Close the dataset.
    CALL h5dclose_f(dataset_id, error)

    ! On calcule les centres des cellules
    CALL mesh_volu ( DOM )

    dataset_name = 'Volume'

    ! Open an existing dataset.
    CALL h5dopen_f(file_id, dataset_name, dataset_id, error)

    ! Read the dataset.
    dims(1) = 1
    dims(2) = DOM%MLG%ncel
    ALLOCATE (U_restart(dims(1),DOM%MLG%ncel))
    CALL h5dread_f(dataset_id, H5T_NATIVE_REAL, U_restart, dims, error)
    DOM%MLG%VOLUME(:) = U_restart(1,:)
    DEALLOCATE (U_restart)

    ! Close the dataset.
    CALL h5dclose_f(dataset_id, error)   

    dataset_name = 'Qualite_maillage_ini'

    ! Open an existing dataset.
    CALL h5dopen_f(file_id, dataset_name, dataset_id, error)

    ! Read the dataset.
    dims(1) = 2
    dims(2) = DOM%MLG%ncel
    ALLOCATE (U_restart(dims(1),DOM%MLG%ncel))
    CALL h5dread_f(dataset_id, H5T_NATIVE_REAL, U_restart, dims, error)
    DOM%MLG%QUALITE_INI(:,1) = U_restart(1,:)
    DOM%MLG%QUALITE_INI(:,2) = U_restart(2,:)
    DEALLOCATE (U_restart)

    ! Close the dataset.
    CALL h5dclose_f(dataset_id, error)    

    dataset_name = 'raideur_lim'

    ! Open an existing dataset.
    CALL h5dopen_f(file_id, dataset_name, dataset_id, error)

    ! Read the dataset.
    dims(1) = 1
    dims(2) = DOM%MLG%nbr_aret_lim
    ALLOCATE (U_restart(dims(1),DOM%MLG%nbr_aret_lim))
    CALL h5dread_f(dataset_id, H5T_NATIVE_REAL, U_restart, dims, error)
    DOM%MLG%ARET_LIM(1:DOM%MLG%nbr_aret_lim)%raideur = U_restart(1,:)
    DEALLOCATE (U_restart)

    ! Close the dataset.
    CALL h5dclose_f(dataset_id, error) 

    dataset_name = 'raideur_int_lapl'

    ! Open an existing dataset.
    CALL h5dopen_f(file_id, dataset_name, dataset_id, error)

    ! Read the dataset.
    dims(1) = MAXVAL( DOM%MLG%SOM_INT%nvoisin )
    dims(2) = DOM%MLG%nsom_int
    ALLOCATE( U_restart( dims(1), dims(2) ) )
    CALL h5dread_f(dataset_id, H5T_NATIVE_REAL, U_restart, dims, error)
    DO j = 1, DOM%MLG%nsom_int
        DOM%MLG%SOM_INT(j)%RAIDEUR = U_restart( 1:DOM%MLG%SOM_INT(j)%nvoisin , j )
    END DO
    DEALLOCATE (U_restart)

    ! Close the dataset.
    CALL h5dclose_f(dataset_id, error)         

    dataset_name = 'raideur_lim_lapl'

    ! Open an existing dataset.
    CALL h5dopen_f(file_id, dataset_name, dataset_id, error)

    ! Read the dataset.
    dims(1) = MAXVAL( DOM%MLG%SOM_LIM%nvoisin )
    dims(2) = DOM%MLG%nsom_lim
    ALLOCATE( U_restart( dims(1), dims(2) ) )
    CALL h5dread_f(dataset_id, H5T_NATIVE_REAL, U_restart, dims, error)
    DO j = 1, DOM%MLG%nsom_lim
        DOM%MLG%SOM_LIM(j)%RAIDEUR = U_restart( 1:DOM%MLG%SOM_LIM(j)%nvoisin , j )
    END DO
    DEALLOCATE (U_restart)

    ! Close the dataset.
    CALL h5dclose_f(dataset_id, error)       

    varname = 'nbr_voisin_eq'
    ALLOCATE (U_restart(1,1))
    ! Open dataset attribute.
    CALL h5aopen_name_f(file_id, varname, attr_id, error)
    ! Write the attribute data.
    CALL h5aread_f(attr_id, atype_id, attr, dims, error)
    ! Close the attribute.
    CALL h5aclose_f(attr_id, error)
    READ(attr,*) U_restart
    DOM%NUM%nvois_def = U_restart(1,1)
    DEALLOCATE(U_restart) 
END IF

! Close the file.
CALL h5fclose_f(file_id, error)

! Fermeture interface fortran si ce ne sera pas fait dans expt_hdf5_end
! et si hdf5 n'est pas utilisé pour le backup
IF (DOM%SUI%file_hdf5 == 0 .AND. DOM%SUI%backup == 0) CALL h5close_f(error)

!----------------------------------------------------------------------!
!--- Mise a jour des variables  ---------------------------------------!
IF ( DOM%NUM%ABLATION == 'on' ) THEN
    CALL mesh_recpt( DOM )
    CALL cyc_ablation ( DOM )
ELSE
    CALL cyc_update_DR( DOM )
END IF

END SUBROUTINE dat_init_restart
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_dat_init_restart