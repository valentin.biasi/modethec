!> Module général de construction des structures de données
MODULE mod_dat
!
USE mod_cst, ONLY : IP, DP
USE mod_print, ONLY : print_dat_read, print_dat_bld, print_dat_init
USE mod_dat_read, ONLY : dat_read
USE mod_dat_bld, ONLY : dat_bld
USE mod_dat_init, ONLY : dat_init
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Fonction de construction des structures de données
SUBROUTINE dat( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM

!----------------------------------------------------------------------!
! --> Phase 1 : Récupérations des informations du fichier de paramètres
CALL dat_read( DOM )
CALL print_dat_read( DOM )

!----------------------------------------------------------------------!
! --> Phase 2 : Construction des variables principales (allocations,...)
CALL dat_bld( DOM )
CALL print_dat_bld( DOM )

!----------------------------------------------------------------------!
! --> Phase 3 : Initialisation
CALL dat_init( DOM )
CALL print_dat_init( DOM )

        
END SUBROUTINE dat
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_dat
