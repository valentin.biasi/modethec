!> Module de récupération du fichier de paramètres NUMERIQUES
MODULE mod_dat_read_num
!
USE mod_cst, ONLY : IP
!
USE mod_num
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Récupération des informations du fichier de paramètres NUMERIQUES
SUBROUTINE dat_read_num( NUM, nom_var )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_num) :: NUM
CHARACTER (len=32) :: nom_var
CHARACTER (len=1) :: egal

!---- Récupérations ---------------------------------------------------!

! Cas suivant le mot clé associé
SELECT CASE (nom_var)
    
    CASE ("dt") ! PAS DE TEMPS STANDARD
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%dt

    CASE ("t0") ! TEMPS INITIAL
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%t0
        
    CASE ("tf") ! TEMPS FINAL
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%tf
        
    CASE ("l") ! PROFONDEUR CEL 2D SUIVANT DIRECTION INVARIANTE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%l
    
    CASE ("schema_A") ! SCHEMA D'INTEGRATION DES TERMES ADVECTIFS
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%schema_A
        
    CASE ("schema_DR") ! SCHEMA D'INTEGRATION DES TERMES DIFFUSIFS-REACTIFS
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%schema_DR
        
    CASE ("schema_S") ! SCHEMA D'INTEGRATION DES TERMES STRUCTURES
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%schema_S
        
    CASE ("solveur_NLIN") ! SOLVEUR NON LINEAIRE 'newton'
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%solveur_NLIN
        
    CASE ("solveur_LIN") ! SOLVEUR LINEAIRE 'GMRES' - 'BICGSTAB' - 'TFQMR'
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%solveur_LIN
    
    CASE ("slope_limiter") ! SCHEMA D'INTEGRATION
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%slope_limiter
    
    CASE ("theta_imp") ! THETA DU SCHEMA THETA-IMPLICITE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%theta_imp
        
    CASE ("pt_bdf") ! NOMBRE DE POINTS EN SCHEMA BDF ( 2<= <=6 )
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%pt_bdf
        
    CASE ("jacobien") ! EVALUATION DE LA MAT. JACOBIENNE (0 non 1 si demande 2 toujours)
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%jacobien
        
    CASE ("CFL_max") ! CFL MAXIMUM EN EXPLICITE 0 < CFL_max < 1
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%CFL_max
        
    CASE ("CFL_AB") ! CFL MAXIMUM RELATIVEMENT A LA VITESSE DE DEFORMATION DE MAILLAGE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%CFL_AB
        
    CASE ("delta_max") ! VARIATION MAXIMUM DE QUANTITE PAR ITERATION
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%delta_max
        
    CASE ("tol_abs") ! TOLERANCE ABSOLUE >0 
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%tol_abs
        
    CASE ("tol_rel") ! TOLERANCE RELATIVE >0 
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%tol_rel
        
    CASE ("tol_int") ! TOLERANCE ITERATIONS INTERNES >0
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%tol_int
    
    CASE ("niter_max") ! NOMBRE MAXIMUM D'ITERATIONS NON-LINEAIRES
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%niter_max
        
    CASE ("niter_max_int") ! NOMBRE MAXIMUM D'ITERATIONS INTERNES
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%niter_max_int
        
    CASE ("dt_couplage") ! PAS DE TEMPS DE COUPLAGE AVEC AUTRE SOLVEUR
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%dt_couplage
        
    CASE ("cmd_couplage") ! COMMANDE SYSTEME DE COUPLAGE AVEC AUTRE SOLVEUR
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%cmd_couplage

    CASE ("niter_def") ! NOMBRE MAXIMUM D'ITERATIONS DE DEFORMATION DE MAILLAGE - ABLATION
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%niter_def
        
    CASE ("tol_def") ! TOLERANCE RELATIVE DE L'OPERATEUR DE DEFORMATION DE MAILLAGE - ABLATION
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%tol_def

    CASE ("nvois_def") ! NOMBRE DE VOISINS A UTILISER POUR L EQUILIBRAGE DES SOMMETS - ABLATION
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%nvois_def      
        
    CASE ("pas_ech") ! NOMBRE DE POINTS SECONDAIRES A CREER PAR FACE LIMITE POUR L'INTERPOLATION PAR SPLINE - ABLATION
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%pas_ech

    ! Diffusion active
    CASE ("DIFFUSION")
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%DIFFUSION
     
    ! Advection active
    CASE ("ADVECTION")
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%ADVECTION
      
    ! Reaction active
    CASE ("REACTION")
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%REACTION
      
    ! Structure active
    CASE ("STRUCTURE")
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%STRUCTURE
      
    ! Ablation active
    CASE ("ABLATION")
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%ABLATION
        
    ! Couplage actif
    CASE ("COUPLAGE")
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%COUPLAGE

    ! Calcul de reprise
    CASE ("restart_backup")
        BACKSPACE(1)
        READ(1,*) nom_var, egal, NUM%restart_backup
  
!....
END SELECT


END SUBROUTINE dat_read_num
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_dat_read_num