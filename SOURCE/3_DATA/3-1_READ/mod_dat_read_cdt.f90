!> Module de récupération du fichier de paramètres CONDITIONS
MODULE mod_dat_read_cdt
!
USE mod_cst, ONLY : IP, P_ref
USE mod_print, ONLY : print_err
!
USE mod_cdt
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Récupération des informations du fichier de paramètres CONDITIONS
SUBROUTINE dat_read_cdt( CDT, nom_var, nesp, id_dom, id_lim, id_isend, id_irecv )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_cdt) :: CDT
CHARACTER (len=32) :: nom_var
CHARACTER (len=1) :: egal
INTEGER(IP) :: nesp
INTEGER(IP) :: id_dom, id_lim
INTEGER(IP) :: id_isend, id_irecv

! Cas suivant le mot clé associé
SELECT CASE (nom_var)

    CASE ("ndom") ! NOMBRE DE DOMAINES PHYSIQUES
        BACKSPACE(1)
        READ(1,*) nom_var, egal, CDT%ndom
        ALLOCATE( CDT%INI( CDT%ndom ) )

    CASE ("nlim") ! NOMBRE TOTAL DE LIMITES
        BACKSPACE(1)
        READ(1,*) nom_var, egal, CDT%nlim
        ALLOCATE( CDT%LIM( CDT%nlim ) )

    !------------------------------------------------------------------!
    ! DONNEES DU ID_DOM -----------------------------------------------!
    CASE ("id_dom")
        BACKSPACE(1)
        READ(1,*) nom_var, egal, id_dom
        CDT%INI(id_dom)%id_dom = id_dom    ! ID DU DOMAINE PHYSIQUE

    CASE ("nom_dom") ! NOM COURANT DU DOMAINE PHYSIQUE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, CDT%INI(id_dom)%nom_dom

    CASE ("Tini") ! TEMPERATURE A t0
        BACKSPACE(1)
        READ(1,*) nom_var, egal, CDT%INI(id_dom)%Tini
        
    CASE ("Pini") ! PRESSION A t0
        BACKSPACE(1)
        READ(1,*) nom_var, egal, CDT%INI(id_dom)%Pini
        CDT%INI(id_dom)%Pini = CDT%INI(id_dom)%Pini + P_ref
        
    CASE ("phi_ini") ! FRACTIONS VOLUMIQUES INIT DES nesp
        ALLOCATE( CDT%INI(id_dom)%phi_ini( nesp ) )
        BACKSPACE(1)
        READ(1,*) nom_var, egal, CDT%INI(id_dom)%phi_ini
        
    !------------------------------------------------------------------!
    ! DONNEES DE LA ID_LIM --------------------------------------------!
    CASE ("id_lim")
        BACKSPACE(1)
        READ(1,*) nom_var, egal, id_lim
        CDT%LIM(id_lim)%id_lim = id_lim    ! ID DE LA LIMITE

        ! RAZ DES COMPTEURS VARIABLES DE COUPLAGE A CHAQUE CHANGEMENT DE LIMITE
        id_isend = 0
        id_irecv = 0
     
    CASE ("nom_lim") ! NOM COURANT DE LA LIMITE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, CDT%LIM(id_lim)%nom_lim
        
    ! TYPE ENER.-------------------------------------------------------!
    CASE ("ener_typ") ! TYPE DE LIMITE ENER (flux - temp - mixte ou special...
        BACKSPACE(1)
        READ(1,*) nom_var, egal, CDT%LIM(id_lim)%ener_typ

    CASE ("Fimp") ! FLUX IMPOSE POUR flux OU mixte [W/m2]
        BACKSPACE(1)
        ALLOCATE( CDT%LIM(id_lim)%Fimp(1) )
        READ(1,*) nom_var, egal, CDT%LIM(id_lim)%Fimp(1)
        
    CASE ("Timp") ! TEMPERATURE IMPOSE POUR temp [K]
        BACKSPACE(1)
        ALLOCATE( CDT%LIM(id_lim)%Timp(1) )
        READ(1,*) nom_var, egal, CDT%LIM(id_lim)%Timp(1)
        
    CASE ("T_conv") ! TEMPERATURE ECHANGE CONVECTIF (mixte) [K]
        BACKSPACE(1)
        ALLOCATE( CDT%LIM(id_lim)%T_conv(1) )
        READ(1,*) nom_var, egal, CDT%LIM(id_lim)%T_conv(1)
        
    CASE ("h_conv") ! COEF ECHANGE CONVECTIF (mixte) [W/m2/K]
        BACKSPACE(1)
        ALLOCATE( CDT%LIM(id_lim)%h_conv(1) )
        READ(1,*) nom_var, egal, CDT%LIM(id_lim)%h_conv(1)
        
    CASE ("T_rad") ! TEMPERATURE ECHANGE RADIATIF (mixte) [K]
        BACKSPACE(1)
        ALLOCATE( CDT%LIM(id_lim)%T_rad(1) )
        READ(1,*) nom_var, egal, CDT%LIM(id_lim)%T_rad(1)
        

    ! TYPE MASSE-------------------------------------------------------!
    CASE ("masse_typ") ! TYPE DES LIMITE MASSE (pression - debit)
        BACKSPACE(1)
        READ(1,*) nom_var, egal, CDT%LIM(id_lim)%masse_typ
        
    CASE ("Pimp") ! PRESSION IMPOSEE >0 (pression)
        BACKSPACE(1)
        ALLOCATE( CDT%LIM(id_lim)%Pimp(1) )
        READ(1,*) nom_var, egal, CDT%LIM(id_lim)%Pimp(1)
        CDT%LIM(id_lim)%Pimp(1) = CDT%LIM(id_lim)%Pimp(1) + P_ref
        
    CASE ("Dimp") ! DEBIT IMPOSE >0 (debit)
        BACKSPACE(1)
        ALLOCATE( CDT%LIM(id_lim)%Dimp(1) )
        READ(1,*) nom_var, egal, CDT%LIM(id_lim)%Dimp(1)
    
    ! LECTURE CLS------------------------------------------------------!
    CASE ("use_cls") ! ACTIVATION DE L'UTILISATION D'UN FICHIER EXTERNE DE CONDITION LIMITE SPECIALE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, CDT%LIM(id_lim)%use_cls

    CASE ("file_cls") ! FICHIER DE CONDITIONS LIMITES SPECIALE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, CDT%LIM(id_lim)%file_cls

       
    ! TYPE STRUCTURE---------------------------------------------------!
    CASE ("struct_typ") ! TYPE DES LIMITE STRUCTURE (fixe - axe - pression - contrainte)
        BACKSPACE(1)
        READ(1,*) nom_var, egal, CDT%LIM(id_lim)%struct_typ
        
    CASE ("stress_imp") ! 
        BACKSPACE(1)
        READ(1,*) nom_var, egal, CDT%LIM(id_lim)%stress_imp(1:2)
    
    
    ! TYPE ABLATION----------------------------------------------------!
    CASE ("abla_typ")   ! TYPE DES LIMITES ABLATION
        BACKSPACE(1)
        READ(1,*) nom_var, egal, CDT%LIM(id_lim)%abla_typ
        
    CASE ("Vreg_imp")   ! VITESSE DE REGRESSION IMPOSEE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, CDT%LIM(id_lim)%Vreg_imp
        
        
    ! TYPE COUPLAGE----------------------------------------------------!
    CASE ("couplage_lim")   ! ACTIVATION DU COUPLAGE DE LA LIMITE on(1) / off(0)
        BACKSPACE(1)
        READ(1,*) nom_var, egal, CDT%LIM(id_lim)%couplage_lim
        
    CASE ("nom_couplage")   ! NOM DU FICHIER DE COUPLAGE EXPORTE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, CDT%LIM(id_lim)%nom_couplage
    
    CASE ("application_couplage")   !  NOM DE L'APPLICATION CWIPI AVEC LAQUELLE LA LIMITE EST COUPLEE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, CDT%LIM(id_lim)%application_couplage

    CASE ("nvar_isend_coupling")   !  NUMBER OF SENT VARIABLES FOR EXTERNAL COUPLING
        BACKSPACE(1)
        READ(1,*) nom_var, egal, CDT%LIM(id_lim)%nvar_isend_coupling
            
        ! ALLOCATATION DES TABLEAUX DE VARIABLES DE COUPLAGE EN ENVOI
        ALLOCATE( CDT%LIM(id_lim)%ISEND_INT_NAME( CDT%LIM(id_lim)%nvar_isend_coupling ))
        ALLOCATE( CDT%LIM(id_lim)%ISEND_DIM( CDT%LIM(id_lim)%nvar_isend_coupling ))
        ALLOCATE( CDT%LIM(id_lim)%ISEND_COUPLING_NAME( CDT%LIM(id_lim)%nvar_isend_coupling ))

    ! LIGNE DESCRIPTION VARIABLES D'ENVOI POUR COUPLAGE EXTERNE
    CASE ("isend_coupling")
        id_isend = id_isend + 1
        IF (id_isend <= CDT%LIM(id_lim)%nvar_isend_coupling) THEN
            BACKSPACE(1)
            READ(1,*) nom_var, egal, &
                CDT%LIM(id_lim)%ISEND_INT_NAME( id_isend ), &
                CDT%LIM(id_lim)%ISEND_DIM( id_isend ), &
                CDT%LIM(id_lim)%ISEND_COUPLING_NAME( id_isend )
        END IF

        ! VERIFICATION COHERENCE DE isend_coupling PAR RAPPORT A nvar_isend_coupling
        READ(1,*) nom_var
        IF ((nom_var /= "isend_coupling") .AND. (id_isend < CDT%LIM(id_lim)%nvar_isend_coupling)) THEN
            CALL print_err('Boundary '//TRIM(CDT%LIM(id_lim)%nom_lim)//': Configured variables number is incoherent with nvar_isend_coupling' ,1)
        END IF
        BACKSPACE(1)

    CASE ("nvar_irecv_coupling")   !  NUMBER OF RECEIVED VARIABLES FOR EXTERNAL COUPLING
        BACKSPACE(1)
        READ(1,*) nom_var, egal, CDT%LIM(id_lim)%nvar_irecv_coupling
            
        ! ALLOCATATION DES TABLEAUX DE VARIABLES DE COUPLAGE EN ENVOI
        ALLOCATE( CDT%LIM(id_lim)%IRECV_INT_NAME( CDT%LIM(id_lim)%nvar_irecv_coupling ))
        ALLOCATE( CDT%LIM(id_lim)%IRECV_DIM( CDT%LIM(id_lim)%nvar_irecv_coupling ))
        ALLOCATE( CDT%LIM(id_lim)%IRECV_COUPLING_NAME( CDT%LIM(id_lim)%nvar_irecv_coupling ))

    ! LIGNE DESCRIPTION VARIABLES D'ENVOI POUR COUPLAGE EXTERNE
    CASE ("irecv_coupling")
        id_irecv = id_irecv + 1
        IF (id_irecv <= CDT%LIM(id_lim)%nvar_irecv_coupling) THEN
            BACKSPACE(1)
            READ(1,*) nom_var, egal, &
                CDT%LIM(id_lim)%IRECV_INT_NAME( id_irecv ), &
                CDT%LIM(id_lim)%IRECV_DIM( id_irecv ), &
                CDT%LIM(id_lim)%IRECV_COUPLING_NAME( id_irecv )
        END IF

        ! VERIFICATION COHERENCE DE irecv_coupling PAR RAPPORT A nvar_irecv_coupling
        READ(1,*) nom_var
        IF ((nom_var /= "irecv_coupling") .AND. (id_irecv < CDT%LIM(id_lim)%nvar_irecv_coupling)) THEN
            CALL print_err('Boundary '//TRIM(CDT%LIM(id_lim)%nom_lim)//': Configured variables number is incoherent with nvar_irecv_coupling' ,1)
        END IF
        BACKSPACE(1)


!....
END SELECT


END SUBROUTINE dat_read_cdt
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_dat_read_cdt
