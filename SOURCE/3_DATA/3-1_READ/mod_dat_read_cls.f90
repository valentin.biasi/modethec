!> Module de récupération de conditions limites speciales (CLS)
MODULE mod_dat_read_cls
!
USE mod_cst, ONLY : IP
USE mod_print, ONLY : print_err
USE mod_algebra, ONLY : inttostr
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Récupération des informations pour les conditions limites speciales (CLS)
SUBROUTINE dat_read_cls( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
CHARACTER (len=32) :: nom_var
INTEGER(IP) :: ilim, nlim
CHARACTER (len=32) :: file_cls
LOGICAL :: file_exist
INTEGER(IP) :: EOF
CHARACTER (len=1) :: egal
INTEGER(IP) :: ipt
!
!---- Initialisation --------------------------------------------------!


! Pour chaque limite du domaine
nlim = DOM%CDT%nlim
DO ilim = 1,nlim
    EOF = 0
    
    ! Si pour cette limite, une CLS est demandée
    IF (DOM%CDT%LIM(ilim)%use_cls == 1) THEN
        
        ! Nom du fichier
        file_cls = DOM%CDT%LIM(ilim)%file_cls 

        ! Vérifie l'existance du fichier
        INQUIRE( FILE=file_cls, EXIST=file_exist)
        IF (.NOT.(file_exist)) THEN
            CALL print_err('Le fichier CLS ener "'//TRIM(file_cls)//'" de la limite '//inttostr(ilim)//' n''existe pas', 1)
        END IF

        ! Ouverture du fichier
        OPEN(1, FILE=file_cls, &
            STATUS='old', ACTION='read')
            
        ! Pour chaque ligne du fichier
        DO WHILE ( EOF == 0 )
            READ( 1,*, IOSTAT=EOF ) nom_var  ! Nom de la variable à récupérer

            ! Cas suivant le mot clé associé
            SELECT CASE (nom_var)
                
                !--- FLUX TYPE GAUSSIEN ---------------------------!
                ! Type de limite ener special
                CASE ("ener_typ_cls")
                    BACKSPACE(1)
                    READ(1,*) nom_var, egal, DOM%CDT%LIM(ilim)%ener_typ_cls
                    
                ! Intensite max du flux (sur l'axe pour "gaussien")
                CASE ("Fmax")
                    BACKSPACE(1)
                    READ(1,*) nom_var, egal, DOM%CDT%LIM(ilim)%Fmax
                    
                ! Rayon du laser (demi-largeur en 1/e^2)
                CASE ("omega")
                    BACKSPACE(1)
                    READ(1,*) nom_var, egal, DOM%CDT%LIM(ilim)%omega
                    
                ! Temps d'application du faisceau gaussien
                CASE ("tf_gauss")
                    BACKSPACE(1)
                    READ(1,*) nom_var, egal, DOM%CDT%LIM(ilim)%tf_gauss
                    
                ! Axe de direction du faisceau gaussien
                CASE ("axe")
                    BACKSPACE(1)
                    READ(1,*) nom_var, egal, DOM%CDT%LIM(ilim)%axe(1:6)

                !--- FLUX TYPE NUAGE DE POINTS --------------------!
                ! Nombre de points du nuage de points
                CASE ("npt_nuage")
                    BACKSPACE(1)
                    READ(1,*) nom_var, egal, DOM%CDT%LIM(ilim)%npt_nuage
                    
                ! Valeurs de flux + Positions du nuage de points
                CASE ("val_nuage")
                    IF ( ALLOCATED( DOM%CDT%LIM(ilim)%val_nuage ) ) THEN
                        DEALLOCATE( DOM%CDT%LIM(ilim)%val_nuage )
                    END IF
                    ALLOCATE( DOM%CDT%LIM(ilim)%val_nuage( DOM%CDT%LIM(ilim)%npt_nuage, 3 ) )
                    DOM%CDT%LIM(ilim)%val_nuage = 0.0_dp
                    ! Lecture du tableau val_nuage( X, Y, VAL )
                    DO ipt = 1,DOM%CDT%LIM(ilim)%npt_nuage
                        READ(1,*) DOM%CDT%LIM(ilim)%val_nuage(ipt,1:3)
                    END DO
                    BACKSPACE(1)
                
                    
            END SELECT
            
        END DO ! Fin lecture fichier
        
        CLOSE(1) ! Fermeture du fichier

    END IF ! Fin use_cls

END DO ! Fin ilim

END SUBROUTINE dat_read_cls
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_dat_read_cls