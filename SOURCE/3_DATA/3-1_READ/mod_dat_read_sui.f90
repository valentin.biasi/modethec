!> Module de récupération du fichier de paramètres SUIVI
MODULE mod_dat_read_sui
!
USE mod_cst, ONLY : IP
!
USE mod_sui
USE mod_print, ONLY : print_err
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Récupération des informations du fichier de paramètres SUIVI
SUBROUTINE dat_read_sui( SUI, nom_var, id_sensors )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_sui) :: SUI
CHARACTER (len=32) :: nom_var
CHARACTER (len=1) :: egal
INTEGER(IP) :: id_sensors

!---- Récupérations ---------------------------------------------------!

! Cas suivant le mot clé associé
SELECT CASE (nom_var)
    
    CASE ("suivi") ! SUIVI ACTIF on(1) / off(0)
        BACKSPACE(1)
        READ(1,*) nom_var, egal, SUI%suivi
        
    CASE ("file_log") ! FICHIER .log ENREGISTRE on(1) / off(0)
        BACKSPACE(1)
        READ(1,*) nom_var, egal, SUI%file_log
        
    CASE ("dt_prt") ! PRINT ECRAN (MULTIPLE DE dt !!!) (0 SI AUCUN)
        BACKSPACE(1)
        READ(1,*) nom_var, egal, SUI%dt_prt

    CASE ("file_hdf5") ! FICHIER .hdf ENREGISTRE on(1) / off(0)
        BACKSPACE(1)
        READ(1,*) nom_var, egal, SUI%file_hdf5
        
    CASE ("dt_hdf5") ! PAS DE TEMPS DES EXPORTS HDF5  (MULTIPLE DE dt !!!) (0 SI AUCUN)
        BACKSPACE(1)
        READ(1,*) nom_var, egal, SUI%dt_hdf5
    
    CASE ("sensors")
        BACKSPACE(1)
        READ(1,*) nom_var, egal, SUI%sensors
        
    CASE("nsensors")
        BACKSPACE(1)
        READ(1,*) nom_var, egal, SUI%nsensors
        IF (SUI%sensors == 1 .AND. SUI%nsensors <=0 ) THEN
            CALL print_err('Capteurs demande mais nsensors est nul ou negatif', 1)
        ELSE
            ! ALLOCATION DE LA MATRICE DES COORDONNEES ET DU TABLEAU DE LA CELLULE LA PLUS PROCHE
            ALLOCATE (SUI%COORD_SENSORS(SUI%nsensors,3))
            ALLOCATE (SUI%SENS2CELL(SUI%nsensors))
        END IF
        
    CASE("coord_sensors")
        id_sensors = id_sensors + 1
        IF (id_sensors <= SUI%nsensors) THEN
            BACKSPACE(1)
            READ(1,*) nom_var, egal, SUI%COORD_SENSORS(id_sensors,1), SUI%COORD_SENSORS(id_sensors,2), SUI%COORD_SENSORS(id_sensors,3)
        END IF
        
    CASE("backup") ! Export backup output on(1) / off(0)
        BACKSPACE(1)
        READ(1,*) nom_var, egal, SUI%backup

    CASE("dt_backup") ! PAS DE TEMPS DES EXPORTS BACKUP  (MULTIPLE DE dt !!!) (0 SI AUCUN)
        IF (SUI%backup == 1) THEN
            BACKSPACE(1)
            READ(1,*) nom_var, egal, SUI%dt_backup
        END IF
        
!....
END SELECT


END SUBROUTINE dat_read_sui
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_dat_read_sui
