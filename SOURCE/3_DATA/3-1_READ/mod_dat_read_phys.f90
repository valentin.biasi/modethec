!> Module de récupération du fichier de paramètres PHYSIQUES
!> \todo Gestion des cas degeneres (pas de reactions, pas de gaz,...)
MODULE mod_dat_read_phys
!
USE mod_cst, ONLY : IP
!
USE mod_phys
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Récupération des informations du fichier de paramètres PHYSIQUES
SUBROUTINE dat_read_phys( PHYS, nom_var, id_esp, id_reac )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_phys) :: PHYS
CHARACTER (len=32) :: nom_var
CHARACTER (len=1) :: egal
INTEGER(IP) :: id_esp, id_reac

!---- Récupérations ---------------------------------------------------!

! Cas suivant le mot clé associé
SELECT CASE (nom_var)
    
    CASE ("nesp") ! Nombre d'espèces
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%nesp
        ALLOCATE( PHYS%ESP( PHYS%nesp ) )
        
    CASE ("ngaz") ! Nombre d'espèces gaz
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ngaz
        
    CASE ("nsol") ! Nombre de solides
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%nsol
        
    CASE ("nreac") ! Nombre de réactions
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%nreac
        ALLOCATE( PHYS%REAC( PHYS%nreac ) )
        
    CASE ("ID_gaz") ! ID DES ESPECES GAZEUSES
        ALLOCATE( PHYS%ID_gaz( PHYS%ngaz ) )
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ID_gaz
        
    CASE ("ID_sol") ! ID DES ESPECES SOLIDES
        ALLOCATE( PHYS%ID_sol( PHYS%nsol ) )
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ID_sol
        
    CASE ("atmo") ! COMPOSITION DE L'ATMOSPHERE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%atmo
    
    
    !------------------------------------------------------------------!
    !---- PROP MATERIAU -----------------------------------------------!
    CASE ("satur_T") ! SATURATION DES PROPRIETES ACTIF on(1) / off(0)
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%satur_T
        
    CASE ("T_sat") ! TEMPERATURE DE SATURATION DES PROPRIETES
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%T_sat
        
    
    !---- PARAMETRES PERMEABILITE -------------------------------------!
    CASE ("poro_ini") ! POROSITE DU MATERIAU VIERGE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%poro_ini
        
    CASE ("poro_fin") ! POROSITE DU MATERIAU DEGRADE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%poro_fin
    
    CASE ("Kp_typ") ! TYPE PERMEABILITE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%Kp_typ
        
    CASE ("Kp_ini") ! PERMEABILITE VIERGE (HENDEERSON)
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%Kp_ini
    
    CASE ("Kp_fin") ! PERMEABILITE DEGRADE (HENDEERSON)
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%Kp_fin
        
    CASE ("Kp_0") ! COEF PERMEABILITE KOZENY
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%Kp_0
        
    CASE ("Kp_x") ! COEF PERMEABILITE KOZENY-ORTHO EN X
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%Kp_x
        
    CASE ("Kp_y") ! COEF PERMEABILITE KOZENY-ORTHO EN Y
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%Kp_y
        
    CASE ("Kp_z") ! COEF PERMEABILITE KOZENY-ORTHO EN Z
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%Kp_z
        
    CASE ("Kp_max") ! Pemeabilité max acceptable (KOZENY-CARMAN)
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%Kp_max
                
        
    !---- PARAMETRES CONDUCT. MATERIAU --------------------------------!
    CASE ("k_homog") ! TYPE D'HOMOGENEISATION DE CONDUCTIVITE
    BACKSPACE(1)
    READ(1,*) nom_var, egal, PHYS%MAT%k_homog
    
    CASE ("k0_nb_poly") ! NB DE COEF. POLYNOMIAUX -> GLOBAL
    BACKSPACE(1)
    READ(1,*) nom_var, egal, PHYS%MAT%k0_nb_poly
    
    CASE ("k0_poly") ! COEF. POLYNOMIAUX -> GLOBAL
    ALLOCATE( PHYS%MAT%k0_poly( PHYS%MAT%k0_nb_poly ) )
    BACKSPACE(1)
    READ(1,*) nom_var, egal, PHYS%MAT%k0_poly
    
    
    !---- PARAMETRES CAPACITE CAL. MATERIAU ---------------------------!
    CASE ("Cp_homog") ! TYPE D'HOMOGENEISATION DE CAPACITE CAL.
    BACKSPACE(1)
    READ(1,*) nom_var, egal, PHYS%MAT%Cp_homog 
    
    CASE ("Cp0_nb_poly") ! NB DE COEF. POLYNOMIAUX CP0 -> GLOBAL
    BACKSPACE(1)
    READ(1,*) nom_var, egal, PHYS%MAT%Cp0_nb_poly
    
    CASE ("Cp0_poly") ! COEF. POLYNOMIAUX Cp0  -> GLOBAL
    ALLOCATE( PHYS%MAT%Cp0_poly( PHYS%MAT%Cp0_nb_poly ) )
    BACKSPACE(1)
    READ(1,*) nom_var, egal, PHYS%MAT%Cp0_poly
    
        
    !---- PARAMETRES STRUCTURES ---------------------------------------!
    CASE ("meca_typ") ! TYPE DE STRUCTURE MECA
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%meca_typ
        
    CASE ("E_iso") ! MODULE D'YOUNG ISOTROPE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%E_iso
        
    CASE ("P_iso") ! COEFFICIENT DE POISSON ISOTROPE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%P_iso
        
    CASE ("E_x") ! MODULE D'YOUNG DIRECTION X TRANSVERSE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%E_x
        
    CASE ("E_y") ! MODULE D'YOUNG DIRECTION Y TRANSVERSE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%E_y
        
    CASE ("P_xy") ! COEFFICIENT DE POISSON TRANSVERSE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%P_xy
        
    CASE ("G_xy") ! MODULE DE CISAILLEMENT TRANSVERSE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%G_xy
        
    CASE ("beta_iso") ! COEFFICIENT DE THERMO-ELASTICITE ISOTROPE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%beta_iso   
    
    CASE ("beta_x") ! COEFFICIENT DE THERMO-ELASTICITE EN X
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%beta_x 
        
    CASE ("beta_y") ! COEFFICIENT DE THERMO-ELASTICITE EN Y
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%beta_y
    
    !---- VISCOSITE DYNAMIQUE -----------------------------------------!
    CASE ("mu_typ") ! TYPE : 'constant' - 'polyn' - ...
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%mu_typ
    
    CASE ("mu_cst") ! VALEUR CONSTANTE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%mu_cst
        
    CASE ("mu_nb_poly") ! NB DE COEF POLYNOMIAUX
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%mu_nb_poly
        
    CASE ("mu_poly") ! COEF POLYNOMIAUX
        ALLOCATE( PHYS%MAT%mu_poly( PHYS%MAT%mu_nb_poly ) )
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%mu_poly
    
    !---- ABLATION-----------------------------------------------------!
    CASE ("T_abla")   ! TEMPERATURE D'ABLATION DU MATERIAU
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%T_abla
        
    CASE ("h_abla")   ! ENTHALPIE D'ABLATION
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%MAT%h_abla
    
    !------------------------------------------------------------------!
    ! DONNEES DE LA ID_ESP --------------------------------------------!
    CASE ("id_esp")
        BACKSPACE(1)
        READ(1,*) nom_var, egal, id_esp
        PHYS%ESP(id_esp)%id_esp = id_esp    ! ID DE L'ESPECE
        
    CASE ("nom_esp") ! NOM COURANT DE l'ESPECE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%nom_esp
        
    CASE ("typ_esp") ! TYPE : 'solide' OU 'gaz'
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%typ_esp 
    
    
    ! --> MASSE VOLUMIQUE ---------------------------------------------!
    CASE ("rho_typ") ! TYPE : 'constant' - 'polyn' - ...
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%rho_typ
        
    CASE ("rho_cst") ! VALEUR CONSTANTE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%rho_cst
        
    CASE ("rho_nb_poly") ! NB DE COEF. POLYNOMIAUX
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%rho_nb_poly
        
    CASE ("rho_poly") ! COEF. POLYNOMIAUX
        ALLOCATE( PHYS%ESP(id_esp)%rho_poly( PHYS%ESP(id_esp)%rho_nb_poly ) )
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%rho_poly


    ! --> CONDUCTIVITE ------------------------------------------------!
    CASE ("k_typ") ! TYPE : 'constant' - 'polyn' - 'otho' - ...
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%k_typ
    
    CASE ("k_cst") ! VALEUR CONSTANTE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%k_cst
        
    CASE ("k_nb_poly") ! NB DE COEF. POLYNOMIAUX
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%k_nb_poly
        
    CASE ("k_poly") ! COEF. POLYNOMIAUX
        ALLOCATE( PHYS%ESP(id_esp)%k_poly( PHYS%ESP(id_esp)%k_nb_poly ) )
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%k_poly
        
    CASE ("k_nb_otho") ! NB DE COEF. POLYNOMIAUX 3x [XYZ]
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%k_nb_otho

    CASE ("k_ortho_x") ! COEF. POLY EN X
        ALLOCATE( PHYS%ESP(id_esp)%k_ortho_x( PHYS%ESP(id_esp)%k_nb_otho ) )
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%k_ortho_x

    CASE ("k_ortho_y") ! COEF. POLY EN Y
        ALLOCATE( PHYS%ESP(id_esp)%k_ortho_y( PHYS%ESP(id_esp)%k_nb_otho ) )
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%k_ortho_y
        
    CASE ("k_ortho_z") ! COEF. POLY EN Z
        ALLOCATE( PHYS%ESP(id_esp)%k_ortho_z( PHYS%ESP(id_esp)%k_nb_otho ) )
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%k_ortho_z
        
    CASE ("k_Eshelby") ! TENSEUR d'ESHELBY DE CONDUCTION
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%k_Eshelby
        
        
    ! --> CAPACITE CALORIFIQUE ----------------------------------------!  
    CASE ("Cp_typ") ! TYPE : 'constant' - 'polyn' - ...
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%Cp_typ
        
    CASE ("Cp_cst") ! VALEUR CONSTANTE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%Cp_cst
        
    CASE ("Cp_nb_poly") ! NB DE COEF. POLYNOMIAUX
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%Cp_nb_poly
        
    CASE ("Cp_poly") ! COEF. POLYNOMIAUX
        ALLOCATE( PHYS%ESP(id_esp)%Cp_poly( PHYS%ESP(id_esp)%Cp_nb_poly ) )
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%Cp_poly
        
        
    ! --> DIFFUSIVITE FICKIENNE----------------------------------------!  
    CASE ("D_typ") ! TYPE : 'constant' - 'polyn' - ...
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%D_typ
        
    CASE ("D_cst") ! VALEUR CONSTANTE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%D_cst
        
    CASE ("D_nb_poly") ! NB DE COEF. POLYNOMIAUX
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%D_nb_poly
        
    CASE ("D_poly") ! COEF. POLYNOMIAUX
        ALLOCATE( PHYS%ESP(id_esp)%D_poly( PHYS%ESP(id_esp)%D_nb_poly ) )
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%D_poly
       
        
    ! --> EMISSIVTE ET ABSRORPTIVITE-----------------------------------!      
    CASE ("eps") ! EMISSIVTE CONSTANTE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%eps
        
    CASE ("alpha") ! ABSRORPTIVITE CONSTANTE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%alpha
        
        
    ! --> MASSE MOLAIRE------------------------------------------------!      
    CASE ("M") ! EMISSIVTE CONSTANTE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%ESP(id_esp)%M


    !------------------------------------------------------------------!
    ! DONNEES DE LA ID_REAC--------------------------------------------!
    CASE ("id_reac")  ! ID DE LA REACTION
        BACKSPACE(1)
        READ(1,*) nom_var, egal, id_reac
        PHYS%REAC(id_reac)%id_reac = id_reac
        
    CASE ("nom_reac") ! NOM COURANT DE LA REACTION
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%REAC(id_reac)%nom_reac

    CASE ("id_R") ! ID DU REACTIF SOLIDE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%REAC(id_reac)%id_R

    CASE ("id_P") ! ID DU PRODUIT SOLIDE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%REAC(id_reac)%id_P
        
    CASE ("id_O2") ! ID DE O2
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%REAC(id_reac)%id_O2
        
    CASE ("ngazP") ! NOMBRE DE PRODUITS GAZEUX
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%REAC(id_reac)%ngazP

    CASE ("ID_gazP") ! ID DES PRODUITS GAZEUX
        ALLOCATE( PHYS%REAC(id_reac)%ID_gazP( PHYS%REAC(id_reac)%ngazP ))
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%REAC(id_reac)%ID_gazP
        
    CASE ("nu_R") ! nu MASSIQUE REACTIF
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%REAC(id_reac)%nu_R
        
    CASE ("nu_P") ! nu MASSIQUE PRODUIT
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%REAC(id_reac)%nu_P
        
    CASE ("nu_O2") ! nu MASSIQUE O2
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%REAC(id_reac)%nu_O2
        
    CASE ("nu_gazP") ! ID DES PRODUITS GAZEUX
        ALLOCATE( PHYS%REAC(id_reac)%nu_gazP( PHYS%REAC(id_reac)%ngazP ))
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%REAC(id_reac)%nu_gazP
        
    CASE ("Q") ! CHALEUR MASSIQUE DE REACTION
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%REAC(id_reac)%Q
    
    CASE ("n") ! ORDRE CINETIQUE DE REACTION
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%REAC(id_reac)%n
    
    CASE ("n_O2") ! ORDRE CINETIQUE DE REACTION EN O2
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%REAC(id_reac)%n_O2
        
    CASE ("A") ! COEF PRE-EXP ARRHENIUS
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%REAC(id_reac)%A
        
    CASE ("E_A") ! ENERGIE D'ACTIVATION ARRHENIUS
        BACKSPACE(1)
        READ(1,*) nom_var, egal, PHYS%REAC(id_reac)%E_A
    
!....
END SELECT


END SUBROUTINE dat_read_phys
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_dat_read_phys
