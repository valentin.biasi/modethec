!> Module général de récupération du fichier de paramètres
MODULE mod_dat_read
!
USE mod_cst, ONLY : IP
USE mod_dat_read_phys, ONLY : dat_read_phys
USE mod_dat_read_num, ONLY : dat_read_num
USE mod_dat_read_cdt, ONLY : dat_read_cdt
USE mod_dat_read_sui, ONLY : dat_read_sui
USE mod_dat_read_exp, ONLY : dat_read_exp
USE mod_dat_read_cls, ONLY : dat_read_cls
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Récupérations des informations du fichier de paramètres
SUBROUTINE dat_read( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
CHARACTER(len=32) :: file_param
INTEGER(IP) :: iligne
CHARACTER (len=32) :: nom_var
INTEGER(IP) :: id_esp, id_reac
INTEGER(IP) :: id_dom, id_lim
INTEGER(IP) :: id_expt, id_isend, id_irecv, id_sensors

!---- Récupérations ---------------------------------------------------!
file_param = DOM%file_param

!---- Initialisation --------------------------------------------------!
id_esp = 0
id_reac = 0
id_lim = 0
id_dom = 0
id_expt = 0
id_isend = 0
id_irecv = 0
id_sensors = 0

! Ouverture de file_param
OPEN(1, FILE=file_param, STATUS='old', ACTION='read' )

!----------------------------------------------------------------------!
! Récupérations des paramètres du fichier principal

! Boucle sur toutes les lignes de file_param
DO iligne = 1,DOM%nl_param
    
    READ(1,*) nom_var  ! Nom de la variable à récupérer
    
    ! Paramètres PHYSIQUES
    CALL dat_read_phys( DOM%PHYS, nom_var, id_esp, id_reac )
    
    ! Paramètres NUMERIQUES
    CALL dat_read_num( DOM%NUM, nom_var )
    
    ! Paramètres CONDITIONS
    CALL dat_read_cdt( DOM%CDT, nom_var, DOM%PHYS%nesp, id_dom, id_lim, id_isend, id_irecv )
    
    ! Paramètres SUIVI
    CALL dat_read_sui( DOM%SUI, nom_var, id_sensors )
    
    ! Paramètres EXPORT
    CALL dat_read_exp( DOM%EXPT, nom_var, id_expt )

END DO

REWIND(1)  ! Remontée au début de fichier

! Fermeture du fichier
CLOSE(1)

!----------------------------------------------------------------------!
! Récupérations des paramètres des fichiers secondaires : conditions limites speciales
CALL dat_read_cls( DOM )
    


END SUBROUTINE dat_read
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_dat_read