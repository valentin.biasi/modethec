!> Module de récupération du fichier de paramètres EXPORT
MODULE mod_dat_read_exp
!
USE mod_cst, ONLY : IP
USE mod_print, ONLY : print_err
!
USE mod_exp
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Récupération des informations du fichier de paramètres d'EXPORT
SUBROUTINE dat_read_exp( EXPT, nom_var, id_expt )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_exp) :: EXPT
CHARACTER (len=32) :: nom_var
CHARACTER (len=1) :: egal
INTEGER(IP) :: id_expt

!---- Récupérations ---------------------------------------------------!

! Cas suivant le mot clé associé
SELECT CASE (nom_var)
    
    CASE ("export") ! EXPORT ACTIF on(1) / off(0)
        BACKSPACE(1)
        READ(1,*) nom_var, egal, EXPT%export

    CASE ("nom_exp") ! NOM DU FICHIER D'EXPORT
        BACKSPACE(1)
        READ(1,*) nom_var, egal, EXPT%nom_exp
        
    CASE ("dt_exp") ! TEMPS AUQUELS S'EFFECTUE L'EXPORT
        BACKSPACE(1)
        READ(1,*) nom_var, egal, EXPT%dt_exp
        
    CASE ("exp_ini") ! EXPORT DE L'ETAT INITIAL on(1) / off(0)
        BACKSPACE(1)
        READ(1,*) nom_var, egal, EXPT%exp_ini
        
    CASE ("deform_mlg") ! DEFORMATION DU MAILLAGE POUR CALCUL STRUCTURE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, EXPT%deform_mlg
        
    CASE ("deform_amp") ! AMPLITUDE DE DEFORMATION DU MAILLAGE
        BACKSPACE(1)
        READ(1,*) nom_var, egal, EXPT%deform_amp
    
    CASE ("FEmode") ! ACTIVE OU NON LE MODE FE DE TECPLOT on(1) / off(0)
        BACKSPACE(1)
        READ(1,*) nom_var, egal, EXPT%FEmode
        
    CASE ("n_tricks") ! NOMBRE DE TRICKS DANS L'AFFICHAGE EN X ET Y
        BACKSPACE(1)
        READ(1,*) nom_var, egal, EXPT%n_tricks
        
    CASE ("nexpt") ! NOMBRE DE VARIABLES A EXPORTER
        BACKSPACE(1)
        READ(1,*) nom_var, egal, EXPT%nexpt
         ! Au moins une variable d'export
        IF (EXPT%nexpt <= 0) CALL print_err('Export demande mais nexpt est nul ou negatif')
            
        ! ALLOCATATION DES TABLEAUX D'EXPORT A nexpt
        ALLOCATE( EXPT%NOM_VAR_ETAT( EXPT%nexpt ))
        ALLOCATE( EXPT%COL_VAR_EXP( EXPT%nexpt ))
        ALLOCATE( EXPT%NOM_VAR_EXP( EXPT%nexpt ))

    CASE ("tecplot_bin") ! FORMAT TECPLOT BINAIRE .plt
        BACKSPACE(1)
        READ(1,*) nom_var, egal, EXPT%tecplot_bin
        
    CASE ("tecplot_ascii") ! FORMAT TECPLOT ASCII .dat
        BACKSPACE(1)
        READ(1,*) nom_var, egal, EXPT%tecplot_ascii

    CASE ("vtk_ascii") ! FORMAT PARAVIEW ASCII .vtk
        BACKSPACE(1)
        READ(1,*) nom_var, egal, EXPT%vtk_ascii
        
    CASE ("sommet") ! EXPORT DES VALEURS AUX SOMMETS
        BACKSPACE(1)
        READ(1,*) nom_var, egal, EXPT%sommet
        
    CASE ("cellule") ! EXPORT DES VALEURS AUX CENTRES DES CELLULES
        BACKSPACE(1)
        READ(1,*) nom_var, egal, EXPT%cellule

    CASE ("volume") ! EXPORT DES CHAMPS SUR LE VOLUME
        BACKSPACE(1)
        READ(1,*) nom_var, egal, EXPT%volume
        
    CASE ("surface") ! EXPORT DES VALEURS SURFACIQUES (LIMITES)
        BACKSPACE(1)
        READ(1,*) nom_var, egal, EXPT%surface

    ! LIGNE DESCRIPTION VARIABLE D'EXPORT
    CASE ("var_exp")
        id_expt = id_expt + 1
        IF (id_expt <= EXPT%nexpt) THEN
            BACKSPACE(1)
            READ(1,*) nom_var, egal, EXPT%NOM_VAR_ETAT( id_expt ), EXPT%COL_VAR_EXP( id_expt ),EXPT%NOM_VAR_EXP( id_expt )
        END IF
        
        ! VERIFICATION COHERENCE DE var_exp PAR RAPPORT A nexpt
        READ(1,*) nom_var
        IF ((nom_var /= "var_exp") .AND. (id_expt < EXPT%nexpt)) THEN
            CALL print_err('Configured exported variables number is incoherent with nexpt' ,1)
        END IF
        BACKSPACE(1)

!....
END SELECT


END SUBROUTINE dat_read_exp
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_dat_read_exp