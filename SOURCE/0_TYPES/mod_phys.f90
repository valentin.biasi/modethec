!> Module physique : Définition de la structure de données physiques issues de caréactérisation
MODULE mod_phys
!
USE mod_cst, ONLY : DP, IP
IMPLICIT NONE
!
!----------------------------------------------------------------------!
!> Définition des prop. matériau
TYPE typ_mat

    REAL(DP) :: poro_ini = 0.0_dp                                       !< Porosite du materiau vierge
    REAL(DP) :: poro_fin = 0.0_dp                                       !< Porosite du materiau degrade
    CHARACTER (len=32) :: Kp_typ = ''                                   !< Type de permeabilite : 'Henderson' - 'Puiroux' - ...
    REAL(DP) :: Kp_ini = 0.0_dp                                         !< Permeabilite vierge (Henderson)
    REAL(DP) :: Kp_fin = 0.0_dp                                         !< Permeabilite degrade (Henderson)
    REAL(DP) :: Kp_0 = 0.0_dp                                           !< Coef permeabilité Kozeny
    REAL(DP) :: Kp_x = 0.0_dp                                           !< Coef permeabilité Kozeny-ortho en X
    REAL(DP) :: Kp_y = 0.0_dp                                           !< Coef permeabilité Kozeny-ortho en Y
    REAL(DP) :: Kp_z = 0.0_dp                                           !< Coef permeabilité Kozeny-ortho en Z
    REAL(DP) :: Kp_max = 1.0_dp                                         !< Pemeabilité max acceptable (Valeur par defaut = 1)
    
    ! Saturation en température des propriétés
    INTEGER(IP) :: satur_T = 0                                          !< Saturation des proprietes actif on(1) / off(0)
    REAL(DP) :: T_sat = 1000.0_dp                                       !< Temperature de saturation des proprietes
    
    ! Parametres k materiau
    CHARACTER (len=32) :: k_homog                                       !< Type d'homogeneisation de conductivite : 'global' - 'melange' - ...
    INTEGER(IP) :: k0_nb_poly                                           !< Nb de coef. polynomiaux, si k0_typ='global'
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: k0_poly                      !< Coef. polynomiaux, si k0_typ='global'
    
    ! Parametres Cp materiau
    CHARACTER (len=32) :: Cp_homog                                      !< Type d'homogeneisation de capacite cal. : 'global' - 'melange' - ...
    INTEGER(IP) :: Cp0_nb_poly                                          !< Nb de coef. polynomiaux Cp0 -> global
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: Cp0_poly                     !< Coef. polynomiaux Cp0  -> global
    
    ! Parametres structures
    CHARACTER (len=32) :: meca_typ                                      !< Type de structure meca : 'isotrope', 'transverse', ...
    REAL(DP) :: E_iso                                                   !< Module d'Young isotrope
    REAL(DP) :: P_iso                                                   !< Coefficient de Poisson isotrope
    REAL(DP) :: E_x                                                     !< Module d'Young direction x transverse
    REAL(DP) :: E_y                                                     !< Module d'Young direction y transverse
    REAL(DP) :: P_xy                                                    !< Coefficient de Poisson transverse
    REAL(DP) :: G_xy                                                    !< Module de cisaillement transverse
    REAL(DP) :: beta_iso = 0.0_dp                                       !< Coefficient de thermo-elasticite isotrope
    REAL(DP) :: beta_x = 0.0_dp                                         !< Coefficient de thermo-elasticite en x
    REAL(DP) :: beta_y = 0.0_dp                                         !< Coefficient de thermo-elasticite en y
    
    ! Viscosité dynamique
    CHARACTER (len=32) :: mu_typ                                        !< Type : 'constant' - 'polyn' - ...
    REAL(DP) :: mu_cst                                                  !< Valeur constante
    INTEGER(IP) :: mu_nb_poly                                           !< Nb de coef polynomiaux
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: mu_poly                      !< Coef. polynomiaux

    ! Paramètres d'ablation du matériau
    REAL(DP) :: T_abla = 1e6                                            !< temperature d'abaltion de la paroi(modele ablation a temperature constante)
    REAL(DP) :: h_abla = 0.0_dp                                         !< enthalpie d'ablation (modele ablation a temperature constante )
END TYPE typ_mat
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Définition d'une espèce
TYPE typ_espece

    INTEGER(IP) :: id_esp                                               !< Id de l'espece
    CHARACTER (len=32) :: nom_esp                                       !< Nom courant de l'espece
    CHARACTER (len=32) :: typ_esp                                       !< Type : 'solide' ou 'gaz'
    
    ! Masse volumique
    CHARACTER (len=32) :: rho_typ                                       !< Type : 'constant' - 'polyn' - ...
    REAL(DP) :: rho_cst                                                 !< Valeur constante
    INTEGER(IP) :: rho_nb_poly                                          !< Nb de coef. polynomiaux
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: rho_poly                     !< Coef. polynomiaux
    
    ! Conductivite
    CHARACTER (len=32) :: k_typ                                         !< Type : 'constant' - 'polyn' - 'otho' - ...
    REAL(DP) :: k_cst                                                   !< Valeur constante
    INTEGER(IP) :: k_nb_poly                                            !< Nb de coef. polynomiaux
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: k_poly                       !< Coef. polynomiaux
    INTEGER(IP) :: k_nb_otho                                            !< Nb de coef. polynomiaux 3x [xyz]
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: k_ortho_x                    !< Coef. poly en x
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: k_ortho_y                    !< Coef. poly en y
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: k_ortho_z                    !< Coef. poly en z
    REAL(DP), DIMENSION(3) :: k_Eshelby                                 !< Tenseur d'Eshelby de conduction
    
    ! Capacite calorifique
    CHARACTER (len=32) :: Cp_typ                                        !< Type : 'constant' - 'polyn' - ...
    REAL(DP) :: Cp_cst                                                  !< Valeur constante
    INTEGER(IP) :: Cp_nb_poly                                           !< Nb de coef. polynomiaux
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: Cp_poly                      !< Coef. polynomiaux
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: int_Cp_poly                  !< Coef. polynomiaux INTEGRES
    
    ! Diffusivite fickienne
    CHARACTER (len=32) :: D_typ                                         !< Type : 'constant' - 'polyn' - ...
    REAL(DP) :: D_cst                                                   !< Valeur constante
    INTEGER(IP) :: D_nb_poly                                            !< Nb de coef. polynomiaux
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: D_poly                       !< Coef. polynomiaux
    
    ! Emissivte et absrorptivite
    REAL(DP) :: eps                                                     !< Emissivte constante
    REAL(DP) :: alpha                                                   !< Absrorptivite constante
    
    ! Masse molaire
    REAL(DP) :: M                                                       !< Masse molaire
    
END TYPE typ_espece
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Définition d'une réaction
TYPE typ_reac

    INTEGER(IP) :: id_reac = 0                                          !< Id de la reaction
    CHARACTER (len=32) :: nom_reac = ''                                 !< Nom courant de la reaction
    INTEGER(IP) :: id_R = 0                                             !< Id du reactif solide
    INTEGER(IP) :: id_P = 0                                             !< Id du produit solide
    INTEGER(IP) :: id_O2 = 0                                            !< Id de O2 (zero si absent)
    INTEGER(IP) :: ngazP = 0                                            !< Nombre de produits gazeux
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_gazP                   !< Id des produits gazeux
    REAL(DP) :: nu_R = 0.0_dp                                           !< nu massique reactif
    REAL(DP) :: nu_P = 0.0_dp                                           !< nu massique produit
    REAL(DP) :: nu_O2 = 0.0_dp                                          !< nu massique o2
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: nu_gazP                      !< nu massique produits gazeux
    REAL(DP) :: Q = 0.0_dp                                              !< Chaleur massique de reaction
    REAL(DP) :: n = 0.0_dp                                              !< Ordre cinetique de reaction
    REAL(DP) :: n_O2 = 0.0_dp                                           !< Ordre cinetique de reaction en O2
    REAL(DP) :: A = 0.0_dp                                              !< Coef pre-exp Arrhenius
    REAL(DP) :: E_A = 0.0_dp                                            !< Energie d'activation Arrhenius
    
END TYPE typ_reac
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Type physique : Contient les données physiques du domaine courant (matériau, espèces, réactions,...)
TYPE typ_phys

    INTEGER(IP) :: nesp                                                 !< Nombre total d'especes
    INTEGER(IP) :: ngaz                                                 !< Nombre d'especes gazeuses
    INTEGER(IP) :: nsol                                                 !< Nombre d'especes solides
    INTEGER(IP) :: nreac                                                !< Nombre de reactions chimiques
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_gaz                    !< Id des especes gazeuses
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_sol                    !< Id des especes solides
    CHARACTER (len=32) :: atmo                                          !< Composition de l'atmosphere : 'air' - 'vide' - 'O2' - 'N2'
    
    !> Sous-type typ_mat pour les propriétés matériaux
    TYPE(typ_mat) :: MAT
    
    !> Sous-type typ_esp pour chaque espèce définie - Structure dynamique
    TYPE(typ_espece), ALLOCATABLE, DIMENSION(:) :: ESP
    
    !> Sous-type typ_reac pour chaque réaction définie - Structure dynamique
    TYPE(typ_reac), ALLOCATABLE, DIMENSION(:) :: REAC
    
END TYPE typ_phys
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_phys
