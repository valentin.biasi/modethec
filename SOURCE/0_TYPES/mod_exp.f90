!> Module export : Définition de la structure d'export
MODULE mod_exp
!
USE mod_cst, ONLY : DP, IP
IMPLICIT NONE
!
!----------------------------------------------------------------------!
!> Type export : Contient les paramètres d'export
TYPE typ_exp

    INTEGER(IP) :: export                                               !< Export actif on(1) / off(0)
    CHARACTER (len=32) :: nom_exp                                       !< Nom du fichier d'export
    REAL(DP) :: dt_exp                                                  !< Temps auquels s'effectue l'export
    INTEGER(IP) :: iiter_exp                                            !< Nb d'it. entre chaque export
    INTEGER(IP) :: file_ini = 1                                         !< Passe a zero lorsque le premier export a ete fait
    INTEGER(IP) :: exp_ini                                              !< Export de l'etat initial on(1) / off(0)
    INTEGER(IP) :: deform_mlg = 0                                       !< Deformation du maillage pour calcul structure
    REAL(DP) :: deform_amp                                              !< Amplitude de deformation du maillage
    
    INTEGER(IP) :: FEmode                                               !< Active ou non le mode fe de tecplot on(1) / off(0)
    INTEGER(IP), DIMENSION(2) :: n_tricks                               !< Nombre de tricks dans l'affichage en x et y

    ! Format d'export
    INTEGER(IP) :: tecplot_bin                                          !< Format tecplot binaire .plt
    INTEGER(IP) :: tecplot_ascii                                        !< Format tecplot ascii .dat
    INTEGER(IP) :: vtk_ascii                                            !< Format paraview ascii .vtk
    
    ! Points a exporter
    INTEGER(IP) :: sommet                                               !< Export des valeurs aux sommets
    INTEGER(IP) :: cellule                                              !< Export des valeurs aux centres des cellules
    
    ! Type d'export
    INTEGER(IP) :: volume                                               !< Export des champs sur le volume
    INTEGER(IP) :: surface                                              !< Export des valeurs surfaciques (limites)
    
    ! Variables à exporter
    INTEGER(IP) :: nexpt                                                !< Nombre de variables a exporter
    CHARACTER (len=32), ALLOCATABLE, DIMENSION(:) :: NOM_VAR_ETAT       !< Nom des variables à exporter dans ETAT
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: COL_VAR_EXP               !< No de colonne des variables à exporter dans ETAT
    CHARACTER (len=64), ALLOCATABLE, DIMENSION(:) :: NOM_VAR_EXP        !< Nom affiché dans le fichier d'export

END TYPE typ_exp
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_exp
