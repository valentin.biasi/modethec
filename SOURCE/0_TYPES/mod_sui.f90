!>  Module suivi : Définition de la structure de suivi écran de la résolution
MODULE mod_sui
!
USE mod_cst, ONLY : DP, IP
IMPLICIT NONE
!
!----------------------------------------------------------------------!
!> Type suivi : Contient les données liées au suivi (preint ecran, fichier log)
TYPE typ_sui

    INTEGER(IP) :: suivi = 1                                            !< Suivi actif on(1) / off(0)
    INTEGER(IP) :: file_log = 1                                         !< Fichier .log enregistre on(1) / off(0)
    REAL(DP) :: dt_prt = 0.0                                            !< Print ecran (multiple de dt !!!) (0 si aucun)
    INTEGER(IP) :: iiter_prt = 1                                        !< Nb d'it entre chaque prt ecran
    INTEGER(IP) :: file_hdf5                                            !< Export des valeurs integrales au format HDF5 on(1) / off(0)
    REAL(DP) :: dt_hdf5 = 0.0                                           !< Pas de temps des exports HDF5 (multiple de dt !!!) (0 si aucun)
    INTEGER(IP) :: iiter_hdf5 = 1                                       !< Nb d'it entre chaque export HDF5
    INTEGER(IP) :: file_id_hdf5                                         !< Identifiant machine du fichier HDF5
    CHARACTER(len=32):: output_file_name                                !< Nom du fichier log des sorties
    CHARACTER(len=32) :: error_file_name                                !< Nom du fichier log des erreurs
    
    ! Extraction en des capteurs précis
    INTEGER(IP) :: sensors                                              !< Capteur actif  on(1) / off(0)
    INTEGER(IP) :: nsensors                                             !< Nombre de capteurs
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: COORD_SENSORS              !< Matrice des coordonnées des capteurs
    INTEGER(DP), ALLOCATABLE, DIMENSION(:) :: SENS2CELL                 !< Tableau qui renvoit le numéro de la cellule la plus proche des capteurs

    ! Mode backup output
    INTEGER(IP) :: backup                                               !< Option backup output  on(1) / off(0)
    REAL(DP) :: dt_backup                                               !< Pas de temps des sauvegardes si l'option est activee
    INTEGER(IP) :: iiter_backup = 1                                     !< Nb d'it entre chaque backup
    INTEGER(IP) :: file_id_backup_hdf5                                  !< Identifiant machine du fichier backup HDF5
    
END TYPE typ_sui
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_sui
