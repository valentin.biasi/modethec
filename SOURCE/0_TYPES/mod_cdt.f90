!> Module conditions : Définition de la structure conditions initiales et limites
MODULE mod_cdt
!
USE mod_cst, ONLY : DP, IP
!
IMPLICIT NONE
!
!----------------------------------------------------------------------!
!> Type conditions initiales pour tout le domaine
TYPE typ_ini
    
    INTEGER(IP) :: id_dom = 0                                           !< No du domaine physique
    CHARACTER (len=32) :: nom_dom = ""                                  !< Nom du domaine physique
    REAL(DP) :: Tini = 0.0_dp                                           !< Temperature initiale
    REAL(DP) :: Pini = 0.0_dp                                           !< Pression initiale
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: phi_ini                      !< Fractions volumiques initiales

END TYPE typ_ini
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Type conditions pour chaque limite
TYPE typ_lim
    
    INTEGER(IP) :: id_lim = 0                                           !< No de la limite
    CHARACTER (len=32) :: nom_lim = ""                                  !< Nom de la limite
    INTEGER(IP) :: nfac_limi = 0                                        !< Nombre de faces limites appartenant à id_lim
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_FACLIMi                !< Indices des faces
    INTEGER(IP), ALLOCATABLE, DIMENSION(:,:) :: SOM_ADJ_LIMi            !< Tableau des sommets adjacents aux sommets internes à la limte i (cf tableau SOM_ADJ du type MLG)
    INTEGER(IP), DIMENSION(2) :: SOM_COINi                              !< Vecteur contenant l'id des deux sommets situes a l'extremité de la limite.
    REAL(DP), ALLOCATABLE, DIMENSION (:,:) :: SPLINEi                   !< Tableau contenant les infos d'interpolation par spline de la limite  || abscisse sommet prec || ord sommet prec || coef a || coef b || tan sommet (3 coefs) || vecteur v (3 coefs) ||
    INTEGER(IP), ALLOCATABLE, DIMENSION (:,:) :: IDSPLINEi              !< Tableau contenant les entiers relatifs a SPLINEi || id_sommet || id_sommet precedent || 0 si droite, 1 si cubique exacte 2 si cubique approchee en cas de tangente verticale ||
    REAL(DP), ALLOCATABLE, DIMENSION (:,:) :: PSi                       !< Tableau contenant les points secondaire de sous echantillonage d'une limite de domaine interpolee par spline || coord (3 col) ||
    INTEGER(IP), ALLOCATABLE, DIMENSION (:,:) :: IDPSi                  !< Tableau contenant les entiers relatifs aux points secondaires de PSi || id_sommet si pt primaire 0 si pt secondaire || id de la face limite courante (IDFACLIMi) ou suivante si point jonction || indice point precedent || indice point suivant ||
    INTEGER(IP), ALLOCATABLE, DIMENSION (:,:) :: PS2FACLIMi             !< Tableau contenant les relations entre les points secondaires de PSi et les faces limites de FACLIMi || nombre de faces limites associees au point secondaire || ID face llimite 1 || ID face limite 2 ||    
    INTEGER(IP), ALLOCATABLE, DIMENSION (:,:) :: PP2PSi                 !< Tableau contenant les relations entre les points principaux et les points secondaires de la limite courante || ID sommet || indice dans PSi du point secondaire precedent || indice dans PSi du point secondaire suivant ||
    INTEGER(IP), ALLOCATABLE, DIMENSION (:) :: ACTIVATION_ABLA          !< Tableau contenant pour chaque face limite 1 si l'ablation est activé 0 sinon    
    
    ! Conditions Energie
    CHARACTER (len=32) :: ener_typ = ""                                 !< Type de limite ener (flux - temp - mixte)
    REAL(DP), ALLOCATABLE, DIMENSION (:) :: Fimp                        !< flux impose pour flux ou mixte [W/m2]
    REAL(DP), ALLOCATABLE, DIMENSION (:) :: Timp                        !< Temperature impose pour temp [K]
    REAL(DP), ALLOCATABLE, DIMENSION (:) :: T_conv                      !< Temperature echange convectif (mixte) [K]
    REAL(DP), ALLOCATABLE, DIMENSION (:) :: h_conv                      !< Coef echange convectif (mixte) [W/m2/K]
    REAL(DP), ALLOCATABLE, DIMENSION (:) :: T_rad                       !< Temperature echange radiatif (mixte) [K]
    
    ! Conditions masse
    CHARACTER (len=32) :: masse_typ = ""                                !< Type des limite masse (pression - debit)
    REAL(DP), ALLOCATABLE, DIMENSION (:) :: Pimp                        !< Pression imposee >0 (pression)
    REAL(DP), ALLOCATABLE, DIMENSION (:) :: Dimp                        !< Debit impose >0 (debit)
    
    ! Paramètres de lecture de condition limite spéciale
    INTEGER(IP) :: use_cls = 0                                          !< Activation de l'utilisation d'un fichier externe de condition limite speciale
    CHARACTER (len=32) :: file_cls = ""                                 !< Fichier de conditions limites speciale
    CHARACTER (len=32) :: ener_typ_cls = ""                             !< Type de CLS ener (gaussien, nuage, ...)

    ! Conditions Energie type flux gaussien
    REAL(DP) :: Fmax = 0.0_dp                                           !< Intensite max du flux (sur l'axe pour "gaussien")
    REAL(DP) :: omega = 0.0_dp                                          !< Rayon du laser (demi-largeur en 1/e^2) (pour "gaussien")
    REAL(DP) :: tf_gauss = 1.0e6_dp                                     !< Temps final d'application du faisceau gaussien
    REAL(DP), DIMENSION(6) :: axe                                       !< Origine et axe du flux gaussien et directionnel
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: dist_axe                     !< Distance du centre de la face limite à l'axe
    
    ! Conditions Energie type flux nuage de points
    INTEGER(IP) :: npt_nuage = 0                                        !< Nombre de points du nuage de points
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: val_nuage                  ! Valeurs de flux + Positions du nuage de points

    ! Condiions structure
    CHARACTER (len=32) :: struct_typ = ""                               !< Type des limite structure (fixe - axe - pression - contrainte)
    REAL(DP), DIMENSION(2) :: stress_imp = 0.0_dp                       !< Contrainte imposee a la limite en Pascal suivant la normale, puis en cisaillement
    
    ! Conditions ablation
    CHARACTER (len=32) :: abla_typ = ""                                 !< type de modele d'abaltion utilise (vitesse, temp)
    REAL(DP) :: Vreg_imp = 0.0_dp                                       !< Vitesse de recession de la paroi (cas modele d'ablation a vitesse constante)    
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: Vreg_meca                    !< Tableau contenant les vitesse de scalaire de regression de chaque face de la limite courante.
    
    ! Conditions couplage sur limites
    INTEGER(IP) :: couplage_lim = 0                                     !< Activation du couplage de la limite on(1) / off(0)
    CHARACTER (len=32) :: nom_couplage = ""                             !< Nom du fichier de couplage exporte -> pour cedre : fichier *.psl !! OBSOLETE - à remplacer par application_couplage !!

    CHARACTER (len=32) :: application_couplage = ""                     !< Nom de l'application CWIPI avec laquelle la limite est couplee (pour un couplage modethec-CWIPI-modethec, mettre le nom du fichier de maillage)
    
    INTEGER(IP) :: nvar_isend_coupling = 0                              !< Number of sent variables for external coupling
    CHARACTER (len=32), ALLOCATABLE, DIMENSION(:) :: ISEND_INT_NAME     !< Nom des variables modethec a envoyer dans couplage CWIPI
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ISEND_DIM                 !< Dimension (ou no de colonne) des variables a envoyer dans couplage CWIPI
    CHARACTER (len=64), ALLOCATABLE, DIMENSION(:) :: ISEND_COUPLING_NAME !< Nom de transfert des variables modethec a envoyer dans couplage CWIPI

    INTEGER(IP) :: nvar_irecv_coupling = 0                              !< Number of received variables for external coupling
    CHARACTER (len=32), ALLOCATABLE, DIMENSION(:) :: IRECV_INT_NAME     !< Nom des variables modethec a recevoir dans couplage CWIPI
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: IRECV_DIM                 !< Dimension (ou no de colonne) des variables a recevoir dans couplage CWIPI
    CHARACTER (len=64), ALLOCATABLE, DIMENSION(:) :: IRECV_COUPLING_NAME !< Nom de transfert des variables modethec a recevoir dans couplage CWIPI

    REAL(DP), ALLOCATABLE, DIMENSION(:) :: ISEND_values                 !< Vecteur des champs de couplage envoyés à l'interface
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: IRECV_values                 !< Vecteur des champs de couplage reçus à l'interface
    
END TYPE typ_lim
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Type conditions : Contient les conditions initiales et limites
TYPE typ_cdt

    INTEGER(IP) :: nlim                                                 !< Nombre total de limites
    INTEGER(IP) :: ndom                                                 !< Nombre total de domaines
    TYPE(typ_ini), ALLOCATABLE, DIMENSION(:) :: INI                     !< Conditions initiales par domaine - Structure dynamique
    TYPE(typ_lim), ALLOCATABLE, DIMENSION(:) :: LIM                     !< Conditions limites - Structure dynamique

END TYPE typ_cdt
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_cdt
