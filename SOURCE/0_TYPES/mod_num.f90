!> Module numérique : Définition de la structure numérique
MODULE mod_num
!
USE mod_cst, ONLY : DP, IP
IMPLICIT NONE
!
!----------------------------------------------------------------------!
!> Type numérique : Contient les données liées à la résol. numérique
TYPE typ_num

    CHARACTER (len=32) :: DIFFUSION = 'off'                             !< Diffusion thermique active
    CHARACTER (len=32) :: ADVECTION = 'off'                             !< Advection (transport gaz) active
    CHARACTER (len=32) :: REACTION = 'off'                              !< Reaction active
    CHARACTER (len=32) :: STRUCTURE = 'off'                             !< Structure meca active
    CHARACTER (len=32) :: ABLATION = 'off'                              !< Ablation active
    CHARACTER (len=32) :: COUPLAGE = 'off'                              !< Couplage actif
    
    REAL(DP) :: dt                                                      !< Pas de temps standart
    REAL(DP) :: t0                                                      !< Temps simu initial
    REAL(DP) :: ti                                                      !< Temps simu courant
    REAL(DP) :: tf                                                      !< Temps simu final
    INTEGER(IP) :: iiter                                                !< Iteration courante
    INTEGER(IP) :: niter                                                !< Nombre d'iterations total
    REAL(DP) :: l = 1.0                                                 !< Prolongement des cellules dans la diraction invariante (2D_PLAN)
    CHARACTER (len=32) :: schema_A                                      !< Schema d'integration des termes advectifs 'euler-exp', 'RK2', 'theta-imp' ou 'theta-lin'
    CHARACTER (len=32) :: schema_DR                                     !< Schema d'integration des termes diffusifs-reactifs 'euler-exp', 'theta-imp' ou 'theta-lin'
    CHARACTER (len=32) :: schema_S                                      !< Schema d'integration des termes structures 'euler-exp', ...
    CHARACTER (len=32) :: solveur_NLIN                                  !< Solveur non lineaire 'newton'
    CHARACTER (len=32) :: solveur_LIN                                   !< Solveur lineaire 'GMRES' - 'BICGSTAB' - 'TFQMR'
    CHARACTER (len=32) :: slope_limiter = "upwind"                      !< Slope limiter (or flux limiter) for the advective terms
    REAL(DP) :: theta_imp                                               !< Theta du schema theta-implicite
    INTEGER(IP) :: pt_bdf                                               !< Nombre de points en schema bdf ( 2<= <=6 )
    INTEGER(IP) :: jacobien                                             !< Evaluation de la mat. jacobienne (0 non 1 si demande 2 toujours)
    REAL(DP) :: delta_max                                               !< Variation maximum de quantite par iteration
    REAL(DP) :: CFL_max                                                 !< CFL maximum en explicite
    REAL(DP) :: CFL_AB                                                  !< CFL maximum relativement a la vitesse de deformation de maillage
    REAL(DP) :: tol_abs                                                 !< Tolerance absolue >0
    REAL(DP) :: tol_rel                                                 !< Tolerance relative >0
    REAL(DP) :: tol_int                                                 !< Tolerance iterations internes >0
    REAL(DP) :: tol_def                                                 !< Tolerance deplacement relatif maximal du sommet par rapport a la plus petite arete adjacente 
    INTEGER(IP) :: niter_max                                            !< Nombre maximum d'iterations non-lineaires
    INTEGER(IP) :: niter_max_int                                        !< Nombre maximum d'iterations internes

    REAL(DP) :: dt_couplage                                             !< Pas de temps de couplage avec autre solveur
    INTEGER(IP) :: iiter_couplage                                       !< Nb d'ite entre chaque couplage
    CHARACTER (len=128) :: cmd_couplage                                 !< Commande de couplage a chaque dt_couplage

    INTEGER(IP) :: niter_def                                            !< Nombre maximum d'itérations de l'operateur de deformation de maillage 
    INTEGER(IP) :: pas_ech    ! INUTILE AVEC LA VERSION 3D              !< Nombre de point secondaire cree par face limite dans le cadre de l'interpolation par spline
    INTEGER(IP) :: nvois_def                                            !< Nombre de voisins à aller chercher pour équilibrer
    
    INTEGER(IP) :: pas_reduit        !NON-UTILISE                       !< Definit si le pas est reduit par rapport a l'etat standart
    INTEGER(IP) :: pre_eval          !NON-UTILISE                       !< Definit si l'on est a la 1ere evaluation du cycle ou non
    REAL(DP) :: dt_S                 !NON-UTILISE                       !< Pas de temps local pour les equations de mecanique solide

    INTEGER(IP) :: restart_backup                                       !< Reprise du calcul avec le fichier backup_*.hdf

    
END TYPE typ_num
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_num
