!>  Module résolution : Définition de la structure de résolution numérique
MODULE mod_rsl
!
USE mod_cst, ONLY : DP, IP
IMPLICIT NONE
!
!----------------------------------------------------------------------!
!> Type résolution : Contient les données de résolution numérique (qtés conservées, résidus,...)
TYPE typ_rsl
    
    REAL(DP) :: dt_CPU = 0.0_dp                                         !< Temps CPU par iteration
    REAL(DP) :: total_CPU_time = 0.0_dp                                 !< Temps CPU passe total
    REAL(DP) :: hrl_cpu = 0.0_dp                                        !< Horloge CPU
    REAL(DP) :: tps_mlg = 0.0_dp                                        !< Temps CPU pour construire les données maillage
    REAL(DP) :: tps_don = 0.0_dp                                        !< Temps CPU pour construire et initialiser la structure de données
    REAL(DP) :: tps_coupl = 0.0_dp                                      !< Temps de reponse solveur externe
    
    ! Nombre d'equations, de degres de libertes et de coeff non-zeros
    INTEGER(IP) :: neq_A                                                !< Nombre d'équations du systeme Advectif
    INTEGER(IP) :: ndl_A                                                !< Nombre de degres de libertes du systeme Advectif
    INTEGER(IP) :: nnz_A                                                !< Nombre de coef non-zeros pour la jacobienne du systeme Advectif
    !
    INTEGER(IP) :: nnz_P                                                !< Nombre de coef non-zeros pour la jacobienne du systeme Pression
    !
    INTEGER(IP) :: neq_DR                                               !< Nombre d'équations du systeme Diffusion-Reactions
    INTEGER(IP) :: ndl_DR                                               !< Nombre de degres de libertes du systeme Diffusion-Reactions
    INTEGER(IP) :: nnz_DR                                               !< Nombre de coef non-zeros pour la jacobienne du systeme Diffusion-Reactions
    !
    INTEGER(IP) :: neq_S                                                !< Nombre d'équations du systeme Structure
    INTEGER(IP) :: ndl_S                                                !< Nombre de degres de libertes du systeme Structure
    INTEGER(IP) :: nnz_S                                                !< Nombre de coef non-zeros pour la jacobienne du systeme Structure
    
    ! Nombres d'iterations et valeurs des residus
    INTEGER(IP) :: niter_A                                              !< Nombre d'it solveur nlin par it temporelle partie ADVEC
    INTEGER(IP) :: niter_A_lin                                          !< Nombre d'it lineaire par it nlin pour systeme ADVEC
    REAL(DP) :: err_it_A                                                !< Residu survenu dans l'it. temporelle partie ADVEC
    !
    INTEGER(IP) :: niter_DR                                             !< Nombre d'it solveur nlin par it temporelle partie DIFF-REAC
    INTEGER(IP) :: niter_DR_lin                                         !< Nombre d'it lineaire par it nlin pour systeme DIFF-REAC
    REAL(DP) :: err_it_DR                                               !< Residu survenu dans l'it. temporelle partie DIFF-REAC
    !
    INTEGER(IP) :: niter_S                                              !< Nombre d'it solveur nlin par it temporelle partie STRUCTURE
    INTEGER(IP) :: niter_S_lin                                          !< Nombre d'it lineaire par it nlin pour systeme STRUCTURE
    REAL(DP) :: err_it_S                                                !< Residu survenu dans l'it. temporelle partie STRUCTURE
    LOGICAL :: converg_S                                                !< Booleen pour savoir si la resolution STRUCTURE est convergee
  
    ! Déplacement de maillage - Ablation
    INTEGER(IP) :: niter_AB ! POUR LA VERSION 2D                        !< Nombre d'it de l'operateur de deformation de maillage
    REAL(DP) :: err_it_AB   ! POUR LA VERSION 2D                        !< Deplacement maximum d'un sommet de maillage relativement a la plus petite des aretes adjacentes.  
    INTEGER(IP), DIMENSION(3) :: niter_eq_AB                            !< Nombre d'itération pour équilibrer 1) Ntag=2 2) Ntag=1 3) sommet int
    REAL(DP), DIMENSION(3) :: err_eq_AB                                 !< Tolérence finale après equilibrage 1) Ntag=2 2) Ntag=1 3) sommet int
    INTEGER(IP), DIMENSION(2) :: niter_proj_AB                          !< Nombre d'itération pour projeter 1) Ntag=2 2) Ntag=1
    REAL(DP), DIMENSION(2) :: err_proj_AB                               !< Tolérence finale après projection 1) Ntag=2 2) Ntag=1
    LOGICAL :: update_somvois = .TRUE.                                  !< Est ce qu'on doit mettre a jour les sommets voisins a equilibrer
    LOGICAL :: remaillage_ab = .TRUE.                                   !< Est ce qu'on vient de faire un remaillage ? ie est ce qu'il faut refaire les connectivités                               
    LOGICAL :: nvoisin_plus_1 = .FALSE.                                 !< Est ce qu'il faut augmenter le nombre de voisins utilisés pour équilibrer le maillage
    LOGICAL :: nvois_max = .FALSE.                                      !< 0 ou 1 pour savoir si on a atteint le max de nvois
    LOGICAL :: equilibrage = .FALSE.                                    !< Pour savoir si on est pendant l'ablation ou l'equilibrage
    REAL(DP) :: max_dep_som = 0.0_dp                                    !< Deplacement max d'un sommet avant equilibrage   
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: m_ini                        !< Masses initiales
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: volume_ini                   !< Volume initial
    LOGICAL :: remail = .FALSE. ! temporaire ! pour ne pas avoir le message a chaque itération ...

    ! Variables conservees
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: E                            !< Energie interne par cellule
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: dE                           !< Variation d'energie interne dans le pas de temps
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: m                            !< Masses totales
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: m_e                        !< Masses des especes
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: dm_e                       !< Variations de masses des especes
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: dalpha                     !< Variations d'avancement des reactions
    
    ! Flux et termes sources a chaque pas de temps
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: FL_cond                      !< Flux diffusifs ener de conduction thermique par face
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: FL_darcy                   !< Flux advectifs massiques(loi de Darcy) par face
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: FL_E_darcy                   !< Flux advectifs ener par face
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: FL_struct                  !< Flux qte de mouvement en calcul structure
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: FL_masse_def               !< Flux de masse du a la deformation du maillage
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: FL_ener_def                  !< Flux d'energie du a la deformation du maillage 
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: FL_Vol                       !< Flux de volume du a la deformation du maillage
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: FL_abla                      !< Flux d'énergie d'ablation à la paroi
        
    ! Deplacements de structure en equilibre mecanique
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: U                          !< Deplacements de la structure solide aux cellules
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: U_cou                      !< Deplacements de la structure solide aux cellules a l'instant courant (n)
    !REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: U_pre                     !< Deplacements de la structure solide aux cellules a l'instant (n-1)
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: U_itp                      !< Deplacements de la structure solide aux faces
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: dU                         !< Variation de deplacements de la structure solide aux cellules
    
    ! Intermédiaires de résolution IMPLICITE
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: Q_A                          !< Ensemble des quantites conservees du systeme A
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: F_A                          !< Ensemble des termes de droite des edp du systeme A
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: Q_DR                         !< Ensemble des quantites conservees du systeme DR    
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: F_DR                         !< Ensemble des termes de droite des edp du systeme DR
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: Q_S                          !< Ensemble des quantites conservees du systeme S   
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: F_S                          !< Ensemble des termes de droite des edp du systeme S
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: F_S_tan                      !< Ensemble des termes tangentiels (cisaillement) du systeme S
    
    
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: residu_A                     !< Residu dus solveur advection par cellule !!!TEST!!!

    ! Parametres du solveur lineaire PARDISO
    INTEGER(8)                          :: PT(64) = 0
    INTEGER(IP)                         :: MTYPE = 11
    INTEGER(IP)                         :: solver = 1
    INTEGER(IP)                         :: IPARM(64)
    REAL(DP)                            :: DPARM(64)
    INTEGER(IP)                         :: error = 0
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) ::  PERM

    ! Matrices Jacobiennnes
    ! --- Systeme A
    REAL(DP), ALLOCATABLE, DIMENSION(:,:,:) :: JAC_A                    !< Matrice jacobienne des termes advectifs
    INTEGER(IP), ALLOCATABLE, DIMENSION(:,:,:) :: id_JAC_A              !< Shape identique à JAC_A, contient les positions de chaque valeur sur le vecteur JAC_A_stval
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: JAC_A_stval                  !< Matrice sparse format Sparse Triplet : VALEUR de JAC_A
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: JAC_A_stcol               !< Matrice sparse format Sparse Triplet : COLONNE de JAC_A
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: JAC_A_csrow               !< Matrice sparse format CSR : LIGNE de JAC_A
    ! --- Systeme DR
    REAL(DP), ALLOCATABLE, DIMENSION(:,:,:) :: JAC_DR                   !< Matrice jacobienne des termes diffusion-reactions
    INTEGER(IP), ALLOCATABLE, DIMENSION(:,:,:) :: id_JAC_DR             !< Shape identique à JAC_DR, contient les positions de chaque valeur sur le vecteur JAC_DR_stval
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: JAC_DR_stval                 !< Matrice sparse format Sparse Triplet : VALEUR de JAC_DR
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: JAC_DR_stcol              !< Matrice sparse format Sparse Triplet : COLONNE de JAC_DR
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: JAC_DR_csrow              !< Matrice sparse format CSR : LIGNE de JAC_DR
    ! --- Systeme P
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: JAC_P                      !< Matrice jacobienne des termes equation de pression
    INTEGER(IP), ALLOCATABLE, DIMENSION(:,:) :: id_JAC_P                !< Shape identique à JAC_P, contient les positions de chaque valeur sur le vecteur JAC_P_stval
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: JAC_P_stval                  !< Matrice sparse format Sparse Triplet : VALEUR de JAC_P
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: JAC_P_strow               !< Matrice sparse format Sparse Triplet : LIGNE de JAC_P
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: JAC_P_stcol               !< Matrice sparse format Sparse Triplet : COLONNE de JAC_P
    ! --- Systeme S
    REAL(DP), ALLOCATABLE, DIMENSION(:,:,:,:) :: JAC_S                  !< Matrice jacobienne des termes structures
    INTEGER(IP), ALLOCATABLE, DIMENSION(:,:,:,:) :: id_JAC_S            !< Shape identique à JAC_S, contient les positions de chaque valeur sur le vecteur JAC_S_stval
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: JAC_S_stval                  !< Matrice sparse format Sparse Triplet : VALEUR de JAC_S
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: JAC_S_strow               !< Matrice sparse format Sparse Triplet : LIGNE de JAC_S
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: JAC_S_stcol               !< Matrice sparse format Sparse Triplet : COLONNE de JAC_S

END TYPE typ_rsl
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_rsl
