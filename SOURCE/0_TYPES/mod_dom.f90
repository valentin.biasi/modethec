!> Module domaine : Définition de la structure dynamique domaine
MODULE mod_dom
!
USE mod_mlg
USE mod_phys
USE mod_num
USE mod_cdt
USE mod_sui
USE mod_exp
USE mod_etat
USE mod_rsl
USE mod_cst, ONLY : IP
IMPLICIT NONE
!
!----------------------------------------------------------------------!
!> Type domaine : Contient les données propres au(x) domaine(s) courant(s) - Structure dynamique
TYPE typ_dom

    CHARACTER (len=32) :: nom_dom                                       !< Nom du domaine courant
    CHARACTER (len=32) :: auteur = ""                                   !< Auteur du cas
    CHARACTER (len=32) :: version = ""                                  !< Version actuelle de MoDeTheC
    CHARACTER (len=32) :: file_param = ""                               !< Nom du fichier de paramètres
    CHARACTER (len=32) :: file_mesh = ""                                !< Nom du fichier de maillage
    CHARACTER (len=128) :: dir = ""                                     !< Répertoire courant
    CHARACTER (len=32) :: logname = ""                                  !< Utilisateur du cas
    CHARACTER (len=32) :: hostname = ""                                 !< Station hote
    INTEGER(IP) :: irank = 0                                            !< Rang de la communication MPI
    INTEGER(IP) :: comm_size = 0                                        !< Dimension du communicateur MPI
    INTEGER(IP) :: nl_param = 0                                         !< Nombre de lignes dans file_param

    !> Ensemble des données géométriques
    TYPE(typ_mlg) :: MLG

    !> Ensemble des données physiques 
    TYPE(typ_phys) :: PHYS
    
    !> Ensemble des données numériques 
    TYPE(typ_num) :: NUM
    
    !> Ensemble des données conditions initiales et limites
    TYPE(typ_cdt) :: CDT
    
    !> Ensemble des données de suivi
    TYPE(typ_sui) :: SUI
    
    !> Ensemble des données d'export
    TYPE(typ_exp) :: EXPT
    
    !> Ensemble des données sur l'état du système au temps ti
    TYPE(typ_etat) :: ETAT
    
    !> Ensemble des données sur la résolution du système au temps ti
    TYPE(typ_rsl) :: RSL
    
END TYPE typ_dom
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_dom
