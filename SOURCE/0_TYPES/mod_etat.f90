!> Module état : Définition de la structure ETAT
MODULE mod_etat
!
USE mod_cst, ONLY : DP
IMPLICIT NONE
!
!----------------------------------------------------------------------!
!> Type état : Contient les variables naturelles du système
TYPE typ_etat
    
    ! Températures
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: T                            !< Temperatures
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: T_itp                        !< Temperature interpolees aux faces
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: grad_T_itp                 !< Gradient de temp. aux faces
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: T_ref                        !< Temperatures de reference (instant t=0s)
    
    ! Fractions massiques
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: Y                          !< Fractions massiques des especes
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: Y_g                          !< Fractions massiques de phase gaz
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: Y_s                          !< Fractions massiques de phase solide
    
    ! Fractions volumiques
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: phi                        !< Fractions volumiques
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: phi_itp                    !< Fractions volumiques interpolees aux faces
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: phi_g                        !< Fractions vol. de gaz
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: phi_g_itp                    !< Fractions vol. de gaz interpolees aux faces
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: phi_s                        !< Fractions vol. de solide
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: phi_s_itp                    !< Fractions vol. de solide interpolees aux faces
    
    ! Masses volumiques
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: rho                          !< Masses volumiques
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: rho_e                      !< Masses vol. abs. par especes
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: rho_g                        !< Masses vol. de la phase gaz
    
    ! Pressions
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: P                            !< Pressions
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: P_itp                        !< Pressions interpolees aux faces
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: grad_P_itp                 !< Gradient de p interpolees aux faces
    
    ! Vitesses de gaz
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: v_g_itp                    !< Vitesses de darcy interpolees aux faces
    
    ! Avancements réactions
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: w_e                        !< Debit massique d'especes conso/prod.
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: w_g                          !< Debit massique de phase gaz conso/prod.
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: wQ                           !< Source vol energ. globale liee aux reactions
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: alpha_reac                 !< Etat d'avancement des reactions
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: alpha_reac0                 !< Etat d'avancement des reactions au debut de l'it
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: rho_rel_0                  !< Masses volumiques relatives par espece a l'etat 0 (calcul des alpha_reac)
    
    ! Variables naturelles pour calcul structure
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: eps_strain                 !< Tenseur des deformations (forme contractée) aux cellules
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: eps_strain_itp             !< Tenseur des deformations (forme contractée) aux faces
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: sig_stress                 !< Tenseur des contraintes (forme contractée) aux cellules
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: sig_stress_itp             !< Tenseur des contraintes (forme contractée) aux faces
    
    ! Variables naturelles pour calcul ablation
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: D_SOM                      !< Vecteur deplacement des sommets (ablation & deformation de maillage)
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: D_SOM_ABL                  !< Vecteur deplacement des sommets (ablation)
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: D_SOM_BRUT                 !< Vecteur deplacement des sommets sans corrections
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: taux_reg                     !< scalaire pour chaque face limite donnant le taux de régression de la paroi (m/s)
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: taux_reg_prec
    
    ! Propriétés du système
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: k                          !< Conductivite th. 3 dir.
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: k_itp                      !< Conductivite th. interpolees aux faces 3 dir.
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: Cp                           !< Capacite calo.
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: h                            !< Enthalpie
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: h_g                          !< Enthalpie phase gaz
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: mu                           !< Viscosite dynamique
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: mu_itp                       !< Viscosite dynamique interpolees aux faces
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: D_itp                        !< Diffusivite massique interpolees aux faces
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: Kp                         !< Permeabilite
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: Kp_itp                     !< Permeabilite interpolees aux faces
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: M                            !< Masse molaire du melange gazeux
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: eps_rad                      !< Emissivité
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: alpha_rad                    !< Absorptivité
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: Crig                       !< Matrice de rigidite pour le calcul structure
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: Crig_itp                   !< Matrice de rigidite pour le calcul structure interpole aux faces
    
    ! Criteres de stabilite
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: Fo                           ! Critere de Fourier (Diffusion)
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: CFL                          ! Citere CFL (Advection)
    
END TYPE typ_etat
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_etat
