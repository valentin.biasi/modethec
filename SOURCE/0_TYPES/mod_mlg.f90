!> Module maillage : Définition de la structure de données du maillage
MODULE mod_mlg
!
USE mod_cst, ONLY : DP, IP
IMPLICIT NONE
!
!----------------------------------------------------------------------!
!> Données constantes d'interpolation et gradients aux centres des cellules
TYPE typ_cel_itp

    INTEGER(IP) :: ncel_adj                                             !< Nombres de cellules adjacentes à la icel
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: CEL_ADJ                   !< ID des cel adjacentes à icel
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: M_itp                      !< Matrice de calcul des gradients
    REAL(DP), DIMENSION(3) :: G_XYZ                                     !< Position de la cellule et des cel. adjacentes
    
END TYPE typ_cel_itp
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Données constantes d'interpolation et gradients aux sommets
TYPE typ_som_itp

    INTEGER(IP) :: ncel_adj                                             !< Nombres de cellules adjacentes à isom
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: CEL_ADJ                   !< ID des cel adjacentes à isom
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: M_itp                      !< Matrice de calcul des gradients
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: A_itp                        !< Vecteur de calcul des interpolations
    REAL(DP), DIMENSION(3) :: S_XYZ                                     !< Position du sommet à interpoler
    REAL(DP), DIMENSION(3) :: vec_GS                                    !< Vecteur de G(cellule) vers S(sommet) pour extrapol.
    
END TYPE typ_som_itp
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Données constantes d'interpolation et gradients aux centres des faces
TYPE typ_fac_itp

    INTEGER(IP) :: ncel_adj                                             !< Nombres de cellules adjacentes à ifac
    INTEGER(IP) :: nsom_adj                                             !< Nombres de sommets adjacents à ifac =2 tout le temps ;)
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: CEL_ADJ                   !< ID des cel adjacentes à ifac
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: SOM_ADJ                   !< ID des som adjacentes à ifac
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: M_itp                      !< Matrice de calcul des gradients
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: A_itp                        !< Vecteur de calcul des interpolations
    REAL(DP), DIMENSION(3) :: H_XYZ                                     !< Position de la face à interpoler
    
END TYPE typ_fac_itp
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Données sur les sommets limites
TYPE typ_som_lim

    INTEGER(IP) :: nb_tags                                              !< Nombres de tags autour du sommet
    INTEGER(IP) :: nvoisin                                              !< Nombre de sommets limites voisins
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: SOMMETS_LIM               !< Numéro des sommets limites voisins liés par une arete limite
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ARETES                    !< Indices des aretes limites liées au sommet limite
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: RAIDEUR                      !< Raideurs autour du sommet l'equilibrage 
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: NBTAG_ARET_SOM            !< Nombre de tag du sommet lié par l'arete
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: FACES                     !< Indices des faces limites liées au sommet limite
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: NORMALES                   !< Vecteurs des normales de chaque tag au sommet
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: SOM_COURB                 !< Indices des sommets limites pour le calcul du rayon de courbure
    INTEGER(IP) :: nb_som_courb                                         !< Nombre de sommets dans SOM_COURB
    REAL(DP) :: r_courb = -1.0                                          !< Rayon de courbure au sommet
    REAL(DP) :: dist_caract                                             !< Distance moyenne autour du sommet
    LOGICAL :: dist_upd = .FALSE.                                       !< Est ce que la distance autour du sommet est à jour
    LOGICAL :: moved_abl = .FALSE.                                      !< Est ce que le sommet limite a été déplacé lors de l'ablation
    LOGICAL :: moved_abl_N1 = .FALSE.                                   !< Est ce que le sommet limite a été déplacé lors de l'ablation à l'itération n-1
    LOGICAL :: projection_surf = .FALSE.                                !< Est ce que le sommet limite doit etre projeté sur le patch surfacique
    LOGICAL :: projection_courb = .FALSE.                               !< Est ce que le sommet limite doit etre projeté sur la courbe (arete)
    LOGICAL :: equilibrage = .FALSE.                                    !< Est ce qu'il faut equilibrer le sommet

END TYPE typ_som_lim
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Données sur les aretes limites
TYPE typ_aret_lim

    INTEGER(IP), DIMENSION(2) :: SOMMETS                                !< Numero des sommets limites autour de l'arete (de 1 à nsom_lim)
    REAL(DP), DIMENSION(7,3) :: PTS_CTR                                 !< Positions des points de controles de l'arete
    REAL(DP) :: raideur                                                 !< Raideur de l'arete pour l'equilibrage des sommets

END TYPE typ_aret_lim
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Données sur les faces limites
TYPE typ_face_lim

    INTEGER(IP) :: nb_som                                               !< Nombre de sommets de la face (3 ou 4)
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: SOMMETS                   !< Sommets autour de la face
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ARETES                    !< Indices des aretes limites liées à la face limite
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: SENS_ARET                 !< 0 ou 1 : pour savoir si l'ordre des sommets et le meme entre la face et les aretes
    REAL(DP), DIMENSION(3,3) :: MAT_PLAN                                !< Matrice de changement de base entre le repere local et le repere de la face
    REAL(DP), DIMENSION(3,3) :: MAT_TRIANGLE                            ! -- (S1, S1S2, S1S3, S1S2^S1S3)
    LOGICAL :: matrice_upd = .FALSE.                                    !< 0 ou 1 pour savoir si les matrices sont a jour
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: TAG_SOMMET                !< Tag de la face autour de chaque sommet
    
END TYPE typ_face_lim
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Données sur le patch limite en cours
TYPE typ_patch_lim

    REAL(DP), DIMENSION(4,3) :: TWIST                                   !< Twist vector pour la construction des patchs (Farouki 2009)
    REAL(DP), DIMENSION(4) :: ANGLES                                    !< Angles entre les tangentes en chaque sommet
    REAL(DP), DIMENSION(8,3) :: RP                                      !< Vecteur r' sur l'arete 1 en t=0 et t=1, 2 en t=0 et t=1, 3 en t=0 et t=1 (4 en t=0 et t=1 si rectangle)
    REAL(DP), DIMENSION(8,3) :: ET                                      !< Vecteur r'/norm(r') sur l'arete 1 en t=0 et t=1, 2 en t=0 et t=1, 3 en t=0 et t=1 (4 en t=0 et t=1 si rectangle)
    REAL(DP), DIMENSION(8,3) :: BT                                      !< Vecteur r'^r''/norm(r'^r'') sur l'arete 1 en t=0 et t=1, 2 en t=0 et t=1, 3 en t=0 et t=1 (4 en t=0 et t=1 si rectangle)
    REAL(DP), DIMENSION(8,3) :: NT                                      !< Vecteur bt^et sur l'arete 1 en t=0 et t=1, 2 en t=0 et t=1, 3 en t=0 et t=1 (4 en t=0 et t=1 si rectangle)
    REAL(DP), DIMENSION(8) :: KT                                        !< Scalaire ||r'^r''||/||r'||^3 sur l'arete 1 en t=0 et t=1, 2 en t=0 et t=1, 3 en t=0 et t=1 (4 en t=0 et t=1 si rectangle)
    REAL(DP), DIMENSION(8) :: TAUT                                      !< Scalaire ||det(r',r'',r''')/||r'^r''||^2 sur l'arete 1 en t=0 et t=1, 2 en t=0 et t=1, 3 en t=0 et t=1 (4 en t=0 et t=1 si rectangle)
    INTEGER(IP), DIMENSION(4,2) :: SIGMA                                !< Scalaire sigma = [S1L,S1R]&[S2L,S2R]&[S3L,S3R] (&[S4L,S4R] si rectangle)
    
END TYPE typ_patch_lim
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Données sur les sommets intérieurs
TYPE typ_som_int

    INTEGER(IP) :: nvoisin                                              !< Nombre de sommets voisins
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ARETES                    !< Aretes autour du sommet
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: RAIDEUR                      !< Raideurs autour du sommet l'equilibrage 
    LOGICAL :: equilibrage = .FALSE.                                    !< Est ce qu'il faut equilibrer le sommet

END TYPE typ_som_int
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Données sur les aretes intérieurs
TYPE typ_aret_int

    INTEGER(IP), DIMENSION(2) :: SOMMETS                                !< Sommets autour de l'arete

END TYPE typ_aret_int
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Type maillage : Contient les données géométriques du domaine courant
TYPE typ_mlg

    CHARACTER (len=32) :: dim_simu = ""                                 !< Dimension de simulation
    REAL(DP) :: ech                                                     !< Echelle de reduction ( sommet = .* ech )
    INTEGER(IP) :: nsom                                                 !< Nombre de sommets
    INTEGER(IP) :: nfac                                                 !< Nombre de faces
    INTEGER(IP) :: ncel                                                 !< Nombre de cellules
    INTEGER(IP) :: nlim                                                 !< Nombre de limites
    INTEGER(IP) :: nfac_lim                                             !< Nombre de faces limites
    INTEGER(IP) :: nfac_int                                             !< Nombre de faces internes
    INTEGER(IP) :: nsom_lim                                             !< Nombre de sommets limites
    INTEGER(IP) :: nsom_int                                             !< Nombre de sommets internes
    REAL(DP), DIMENSION(3) :: DIR_PLAN = (/0.0,0.0,1.0/)                !< Direction du plan d'invariance (EN 2D_PLAN SEULEMENT) [ux,uy,uz]
    REAL(DP), DIMENSION(6) :: AXE_2D = (/0.0,0.0,0.0,0.0,0.0,0.0/)      !< Axe de symétrie (EN 2D_AXI SEULEMENT) [x,y,z,ux,uy,uz]
    REAL(DP) :: angle_AXE                                               !< Angle entre l'axe de symetrie en 2D_AXI et l'axe X
    INTEGER(IP) :: ncel_adj_max                                         !< Nombre maximal de cellules adjacentes par cel.
    INTEGER(IP) :: nfac_adj_max                                         !< Nombre maximal de faces adjacentes par cel.
    
    ! Extrait direct du fichier maillage
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: SOMMET                     !< Position des sommets [x,y,z]
    INTEGER(IP), ALLOCATABLE, DIMENSION(:,:) :: FAC                     !< Relations face/sommet [no_fac,nb_som,som1,som2,...]
    INTEGER(IP), ALLOCATABLE, DIMENSION(:,:) :: FAC2CEL                 !< Relations face/cellule [no_fac,nb_cel,cel1,...]
    INTEGER(IP), ALLOCATABLE, DIMENSION(:,:) :: FACLIM                  !< Relations face_limites/faces [no_faclim,no_fac]
    
    CHARACTER (len=32), ALLOCATABLE, DIMENSION(:) :: FACLIM_MARQ        !< Nom des id des faces limites
    CHARACTER (len=32), ALLOCATABLE, DIMENSION(:) :: CEL2DOM_MARQ       !< Nom des id de domaine des cellules
    
    ! Aire et Volume
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: AIRE                         !< Aires des faces
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: VOLUME                       !< Volumes des cellules
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: VOLUME_TEMP                  !< Volumes des cellules (temporaire pour l'ablation a température fixée)
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: PERIM                        !< Perimetres des cellules
    !REAL(DP), ALLOCATABLE, DIMENSION(:) :: AIREp                       !< Aires des cellules en 2D plan
    !REAL(DP), ALLOCATABLE, DIMENSION(:) :: VOLUMEp                     !< Volumes des cellules en 2D plan
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: AIREs                        !< Aires des cellules pour calcul structure (modif sur l'axe)
        
    ! Positions
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: G_XYZ                      !< Positions des centres des cellules
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: H_XYZ                      !< Positions des centres des faces
    !REAL(DP), ALLOCATABLE, DIMENSION(:,:,:) :: FAC_XYZ                 !< Positions des sommets des faces
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: dist_AXE_G                   !< Distances entre les centres des cellules et l'axe de symetrie si 2D_AXI
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: dist_AXE_H                   !< Distances entre les centres des faces et l'axe de symetrie si 2D_AXI

    ! Internes / Limites
    !INTEGER, ALLOCATABLE, DIMENSION(:,:) :: FACLIMr                    ! Relations faces / faces limites
    CHARACTER (len=32), ALLOCATABLE, DIMENSION(:) :: ID_LIM             !< Identifiants des limites
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_FACINT                 !< Identifiants des faces internes
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_FACLIM                 !< Identifiants des faces limites
    
    ! Relations
    INTEGER(IP), ALLOCATABLE, DIMENSION(:,:) :: CEL2FAC                 !< Relations cellules / faces
    INTEGER(IP), ALLOCATABLE, DIMENSION(:,:) :: CEL_ADJ                 !< Relations cellules / cellules adjascentes
    INTEGER(IP), ALLOCATABLE, DIMENSION(:,:) :: CEL2SOM                 !< Relations cellules / sommets
    INTEGER(IP), ALLOCATABLE, DIMENSION(:,:) :: SOM_ADJ                 !< Relations sommets / sommets adjacents
    INTEGER(IP), ALLOCATABLE, DIMENSION(:,:) :: SOM_ADJ_INT             !< Relations sommets interieurs / sommets adjacents
    INTEGER(IP), ALLOCATABLE, DIMENSION(:,:) :: SOM_ADJ_LIM             !< Relations sommets limites / sommets adjacents
    INTEGER(IP), ALLOCATABLE, DIMENSION(:,:) :: SOMLIM                  !< Relations sommets / sommets_int(0)-sommets_lim(1) [no_som,0 ou 1] (seulement pour la 3D)
    INTEGER(IP), ALLOCATABLE, DIMENSION(:,:) :: SOMLIM2FACLIM           !< Relations sommets limites / faces limites
    INTEGER(IP), ALLOCATABLE, DIMENSION(:,:) :: FACLIM_GRP              !< Tag pour associer les faces autour d'un sommet en fonction de leur normal : pour le deplacement de maillage
    INTEGER(IP), ALLOCATABLE, DIMENSION(:,:) :: FACLIM_GRP_SURF         !< Tag pour associer les faces autour d'un sommet en fonction de leur normal : pour l'équilibrage des sommets
    INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: IDSOM2ISOM                !< Relation indice sommet / no_sommet limite ou int
    
    ! Vecteurs
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: G1G2                       !< Vecteurs G1G2 pour chaque face
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: n2_G1G2                      !< Norme au carre de G1G2
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: G1H                        !< Vecteurs G1H pour chaque face
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: n2_G1H                       !< Norme au carre de G1H  
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: HG2                        !< Vecteurs HG2 pour chaque face interne
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: n2_HG2                       !< Norme au carre de HG2
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: VFAC                       !< Vecteurs normaux aux faces (sens G1G2)
    INTEGER(IP), ALLOCATABLE, DIMENSION(:,:) :: dir_VFAC                !< Orientation de VFAC par rapport à chaque ifac : +1=ext -1=int 0=lim
    REAL(DP), ALLOCATABLE, DIMENSION(:) :: dL_min                       !< Longueur caracteristique min des cellules
    !REAL(DP), ALLOCATABLE, DIMENSION(:) :: AIRE_VFAC                   !< Produit AIRE * VFAC
    
    ! Qualite de maillage
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: QUALITE                    !< Qualite du maillage en cas d'ablation pour chaque cellule || facteur de forme max || angle max entre la normale des face et G1H || 
    REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: QUALITE_INI                !< Qualite initiale du maillage en cas d'ablation pour chaque cellule || facteur de forme max || angle max entre la normale des face et G1H || 
    
    ! Interpolation et gradients
    TYPE(typ_cel_itp), ALLOCATABLE, DIMENSION(:) :: CEL_ITP             !< pour la valeur aux centres des cellules
    TYPE(typ_som_itp), ALLOCATABLE, DIMENSION(:) :: SOM_ITP             !< pour la valeur aux sommets
    TYPE(typ_fac_itp), ALLOCATABLE, DIMENSION(:) :: FAC_ITP             !< pour la valeur aux centres des faces

    ! Structure de données pour l'ablation 3D - Structure dynamique
    TYPE(typ_som_int), ALLOCATABLE, DIMENSION(:) :: SOM_INT             !< pour les informations sur les sommets intérieurs
    TYPE(typ_aret_int), ALLOCATABLE, DIMENSION(:) :: ARET_INT           !< pour les informations sur les aretes intérieures
    TYPE(typ_som_lim), ALLOCATABLE, DIMENSION(:) :: SOM_LIM             !< pour les informations sur les sommets limites
    TYPE(typ_aret_lim), ALLOCATABLE, DIMENSION(:) :: ARET_LIM           !< pour les informations sur les aretes limites
    TYPE(typ_face_lim), ALLOCATABLE, DIMENSION(:) :: FAC_LIM            !< pour les informations sur les faces limites
    TYPE(typ_patch_lim) :: PATCH_LIM                                    !< pour les informations sur le patch limite en cours    
    INTEGER(IP) :: nbr_aret_int                                         !< Nombre d'arete à l'interieur du domaine
    INTEGER(IP) :: nbr_aret_lim                                         !< Nombre d'arete à la limite du domaine

END TYPE typ_mlg
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mlg
