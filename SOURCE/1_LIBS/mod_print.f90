!> Module contenant les sous-routines de print écran
MODULE mod_print
!
USE mod_cst, ONLY : DP, IP, output_file_name, error_file_name, output_file_id, error_file_id
USE mod_algebra, ONLY : inttostr
!
USE, INTRINSIC :: iso_fortran_env, ONLY : output_unit, error_unit
USE MPI
USE OMP_LIB
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Routine de print des erreurs
SUBROUTINE print_err( MSG, ARRET )
!
CHARACTER(len=*) :: MSG
CHARACTER(len=32) :: prefix_MSG
INTEGER(IP), OPTIONAL :: ARRET
INTEGER(IP) :: irank, comm_size, ierr
LOGICAL :: ouvert

! Récupération irank et comm_size
CALL MPI_COMM_RANK(MPI_COMM_WORLD, irank, ierr)
CALL MPI_COMM_SIZE(MPI_COMM_WORLD, comm_size, ierr)

! Prefixe du message
IF (PRESENT(ARRET)) THEN
    IF (ARRET == 1) THEN
        prefix_MSG = '    ** ERROR: '
    END IF
ELSE
    prefix_MSG = '    ** WARNING: '
END IF

! Affichage a l'ecran
WRITE(output_unit,*) TRIM(prefix_MSG)//' '//MSG

! Ecriture dans le fichier log error
IF (comm_size == 1) THEN
    INQUIRE(FILE=error_file_name, OPENED=ouvert)
ELSE
    INQUIRE(FILE=TRIM(error_file_name)//'.'//ADJUSTL(inttostr(irank)), OPENED=ouvert)
END IF

! Ecriture dans le fichier log error
IF (ouvert) THEN
        WRITE(error_file_id,*) TRIM(prefix_MSG)//MSG
END IF

! Affiche messages d'arret demandes et arret
IF (PRESENT(ARRET)) THEN
    IF (ARRET == 1) THEN
        WRITE(output_unit,*) TRIM(prefix_MSG)//' '//'Modethec crashed due to critical errors.'
        IF (ouvert) WRITE(error_file_id,*) TRIM(prefix_MSG)//' '//'Modethec crashed due to critical errors.'

        CLOSE(error_file_id) ! Fermerture du fichier log erreurs
        CLOSE(output_file_id) ! Fermerture du fichier log outputs

        ! Arret definitif !!!!
        IF (comm_size == 1) THEN
            STOP
        ELSE
            CALL MPI_ABORT(MPI_COMM_WORLD, ierr, ierr)
        END IF

    END IF
END IF

END SUBROUTINE print_err
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de print des sorties output
SUBROUTINE print_out( MSG, suivi, file_log )
!
CHARACTER(len=*) :: MSG
INTEGER(IP) :: suivi, file_log
CHARACTER(len=256) :: new_MSG
LOGICAL :: ouvert
INTEGER(IP) :: irank, comm_size, ierr

! Récupération irank et comm_size
CALL MPI_COMM_RANK(MPI_COMM_WORLD, irank, ierr)
CALL MPI_COMM_SIZE(MPI_COMM_WORLD, comm_size, ierr)

! Modification du MSG en MPI
IF (comm_size > 1) THEN
    WRITE(new_MSG,'(I2,A,A)') irank, ':', MSG
ELSE
    WRITE(new_MSG,'(A)') MSG
END IF

! Affichage a l'ecran standard
IF (suivi == 1) WRITE(output_unit,*),TRIM(new_MSG)

! Ecriture dans le fichier log output
IF (file_log == 1) THEN
    INQUIRE(output_file_id,OPENED=ouvert)
    IF (ouvert) THEN
        WRITE(output_file_id,*),TRIM(new_MSG)
    END IF
END IF

END SUBROUTINE print_out
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de print à l'initialisation
SUBROUTINE print_prelim( DOM )
TYPE (typ_dom) :: DOM
INTEGER(IP) :: date_time(8)
CHARACTER (len=10) :: abc(3)
CHARACTER (len=128) :: MSG

DOM%RSL%hrl_cpu = OMP_GET_WTIME() ! Debut du comptage du temps CPU

WRITE(MSG,'(A)'),'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
WRITE(MSG,'(A)'),'+                  __  __  ___  ___  ___ _____ _  _ ___ ___                +'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
WRITE(MSG,'(A)'),'+                 |  \/  |/ _ \|   \| __|_   _| || | __/ __|               +'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
WRITE(MSG,'(A)'),'+                 | |\/| | (_) | |) | _|  | | | __ | _| (__                +'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
WRITE(MSG,'(A)'),'+                 |_|  |_|\___/|___/|___| |_| |_||_|___\___|               +'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
WRITE(MSG,'(A)'),'+--------------------------------------------------------------------------+'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A,A)') '+ Author:         ', TRIM(DOM%auteur)
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
WRITE(MSG,'(A,A)') '+ Version:        ', TRIM(DOM%version)
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
WRITE(MSG,'(A,A)') '+ Login:          ', TRIM(DOM%logname)
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
WRITE(MSG,'(A,A)') '+ Station:        ', TRIM(DOM%hostname)
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

CALL date_and_time(abc(1), abc(2), abc(3), date_time)
WRITE(MSG,'(A,i2,A,i2,A,i4,A,i2,A,i2,A,i2)'), '+ Date:           ', &
        date_time(3),'/',date_time(2),'/',date_time(1), &
        ' - ',date_time(5),':',date_time(6),':',date_time(7)
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A,A)') '+ Directory:      ', TRIM(DOM%dir)
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
WRITE(MSG,'(A,A)') '+ Parameter file: ', TRIM(DOM%file_param)
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
WRITE(MSG,'(A,A)') '+ Mesh file:      ', TRIM(DOM%file_mesh)
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
WRITE(MSG,'(A)') '+--------------------------------------------------------------------------+'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
WRITE(MSG,'(A)') '+--> STAGE I - MESH CONSTRUCTION'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
WRITE(MSG,'(A)'),'+'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

END SUBROUTINE
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de print après lecture maillage
SUBROUTINE print_mesh_read( DOM )
TYPE (typ_dom) :: DOM
REAL(DP) :: new_cpu
CHARACTER (len=128) :: MSG

! MAJ du temps CPU
new_cpu = OMP_GET_WTIME()
DOM%RSL%tps_mlg = DOM%RSL%tps_mlg + (new_cpu - DOM%RSL%hrl_cpu)

WRITE(MSG,'(A,F11.3,A)'),'+------------> I.1 - Mesh reading              (CPU time =',DOM%RSL%tps_mlg,'s)'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

DOM%RSL%hrl_cpu = new_cpu

END SUBROUTINE print_mesh_read

!----------------------------------------------------------------------!
!> Routine de print après relation maillage
SUBROUTINE print_mesh_bld( DOM )
TYPE (typ_dom) :: DOM
REAL(DP) :: new_cpu
CHARACTER (len=128) :: MSG

! MAJ du temps CPU
new_cpu = OMP_GET_WTIME()
DOM%RSL%tps_mlg = DOM%RSL%tps_mlg + (new_cpu - DOM%RSL%hrl_cpu)

WRITE(MSG,'(A,F10.3,A)'),'+------------> I.2 - Mesh relations building   (CPU time = ',new_cpu - DOM%RSL%hrl_cpu,'s)'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

DOM%RSL%hrl_cpu = new_cpu

END SUBROUTINE print_mesh_bld
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de print calculs maillage
SUBROUTINE print_mesh_cpt( DOM )
TYPE (typ_dom) :: DOM
REAL(DP) :: new_cpu
CHARACTER (len=128) :: MSG

! MAJ du temps CPU
new_cpu = OMP_GET_WTIME()
DOM%RSL%tps_mlg = DOM%RSL%tps_mlg + (new_cpu - DOM%RSL%hrl_cpu)

WRITE(MSG,'(A,F10.3,A)'),'+------------> I.3 - Mesh computing            (CPU time = ',new_cpu - DOM%RSL%hrl_cpu,'s)'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

DOM%RSL%hrl_cpu = new_cpu

WRITE(MSG,'(A)'),'+'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A,A8)'),'   + Dimension:       ',TRIM(DOM%MLG%dim_simu)
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A,i9)'), '   + Vertices number:',DOM%MLG%nsom
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A,i12)'),'   + Cells number:',DOM%MLG%ncel
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A,i12)'),'   + Faces number:',DOM%MLG%nfac
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A,F8.3,A,F8.3,A)'),'   + [ Xmin, Xmax ]: [',&
minval(DOM%MLG%SOMMET(:,1)),',',maxval(DOM%MLG%SOMMET(:,1)),']'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A,F8.3,A,F8.3,A)'),'   + [ Ymin, Ymax ]: [',&
minval(DOM%MLG%SOMMET(:,2)),',',maxval(DOM%MLG%SOMMET(:,2)),']'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A,F8.3,A,F8.3,A)'),'   + [ Zmin, Zmax ]: [',&
minval(DOM%MLG%SOMMET(:,3)),',',maxval(DOM%MLG%SOMMET(:,3)),']'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A)'),'+'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A)'),'+--------------------------------------------------------------------------+'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
WRITE(MSG,'(A)'),'+--> STAGE II - DATA PREPARATION'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
WRITE(MSG,'(A)'),'+'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

END SUBROUTINE print_mesh_cpt
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de print apres recuperations file_param
SUBROUTINE print_dat_read( DOM )
TYPE (typ_dom) :: DOM
REAL(DP) :: new_cpu
CHARACTER (len=128) :: MSG

! MAJ du temps CPU
new_cpu = OMP_GET_WTIME()
DOM%RSL%tps_don = DOM%RSL%tps_don + (new_cpu - DOM%RSL%hrl_cpu)

WRITE(MSG,'(A,F10.3,A)'),'+------------> II.1 - Parameter file reading   (CPU time = ',DOM%RSL%tps_don,'s)'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

DOM%RSL%hrl_cpu = new_cpu

END SUBROUTINE print_dat_read
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de print après construction structure de donnees
SUBROUTINE print_dat_bld( DOM )
TYPE (typ_dom) :: DOM
REAL(DP) :: new_cpu
CHARACTER (len=128) :: MSG

! MAJ du temps CPU
new_cpu = OMP_GET_WTIME()
DOM%RSL%tps_don = DOM%RSL%tps_don + (new_cpu - DOM%RSL%hrl_cpu)

WRITE(MSG,'(A,F10.3,A)'),'+------------> II.2 - Variables building       (CPU time = ',new_cpu - DOM%RSL%hrl_cpu,'s)'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

DOM%RSL%hrl_cpu = new_cpu

END SUBROUTINE print_dat_bld
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de print après initialisation
SUBROUTINE print_dat_init( DOM )
TYPE (typ_dom) :: DOM
REAL(DP) :: new_cpu
CHARACTER (len=128) :: MSG

! MAJ du temps CPU
new_cpu = OMP_GET_WTIME()
DOM%RSL%tps_don = DOM%RSL%tps_don + (new_cpu - DOM%RSL%hrl_cpu)

WRITE(MSG,'(A,F10.3,A)'),'+------------> II.3 - Variables initialization (CPU time = ',new_cpu - DOM%RSL%hrl_cpu,'s)'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

DOM%RSL%hrl_cpu = new_cpu

WRITE(MSG,'(A)'),'+'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A,F8.2)'),'+ Initial time: ',DOM%NUM%t0
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A,F8.4)'),'+ Time step:    ',DOM%NUM%dt
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A,F8.2)'),'+ Final time:   ',DOM%NUM%tf
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A)'),'+'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A,A)') '+ Heat diffusion solver:  ',TRIM(DOM%NUM%DIFFUSION)
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A,A)') '+ Gaz transport solver:   ',TRIM(DOM%NUM%ADVECTION)
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A,A)') '+ Reactions solver:       ',TRIM(DOM%NUM%REACTION)
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A,A)') '+ Mechanical solver:      ',TRIM(DOM%NUM%STRUCTURE)
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A,A)') '+ Ablation solver:        ',TRIM(DOM%NUM%ABLATION)
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A,A)') '+ External coupling:      ',TRIM(DOM%NUM%COUPLAGE)
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A)'),'+'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A)'),'+--------------------------------------------------------------------------+'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A)'),'+--> STAGE III - TIME CYCLES'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

END SUBROUTINE print_dat_init
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de print au debut de cycle
SUBROUTINE print_cyc_ini( DOM )
TYPE (typ_dom) :: DOM
CHARACTER (len=128) :: MSG
REAL(DP) :: new_dt

new_dt = DOM%NUM%ti+DOM%NUM%dt

WRITE(MSG,'(A)'),'+'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
WRITE(MSG,'(A,I7,A,F10.4,A)'),'+--> Cycle no ',DOM%NUM%iiter, ' - Time = ',new_dt,' s'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

END SUBROUTINE print_cyc_ini
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de print a la fin de chaque cycle
SUBROUTINE print_cyc_end( DOM )
TYPE (typ_dom) :: DOM
REAL(DP) :: new_cpu
CHARACTER (len=128) :: MSG

new_cpu = OMP_GET_WTIME()

DOM%RSL%dt_CPU = (new_cpu - DOM%RSL%hrl_cpu)/DOM%NUM%iiter ! Temps par it.
DOM%RSL%total_CPU_time = DOM%RSL%total_CPU_time + (new_cpu - DOM%RSL%hrl_cpu) ! Comptage du temps passé
DOM%RSL%hrl_cpu = new_cpu ! MAJ du temps CPU

WRITE(MSG,'(A,ES9.2,A,ES9.2,A,ES9.2,A)'),'+--> CPU time / it.: ',DOM%RSL%dt_CPU,'s - Total: ',DOM%RSL%total_CPU_time,&
                    's - Left: ',DOM%RSL%dt_CPU*(DOM%NUM%niter-DOM%NUM%iiter),'s'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

IF (DOM%NUM%DIFFUSION == 'on') THEN
    WRITE(MSG,'(A)'),'   +'
    CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
    WRITE(MSG,'(A,ES9.2)'),'   + Heat diffusion residue = ',DOM%RSL%err_it_DR
    CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
    WRITE(MSG,'(A,I6)'),'   + Nb it. Diffusion-Reaction = ',DOM%RSL%niter_DR
    CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
    WRITE(MSG,'(A,F12.2)'),'   + Max. Fourier number = ',MAXVAL(ABS(DOM%ETAT%Fo))
    CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
END IF

IF (DOM%NUM%ADVECTION == 'on') THEN
    WRITE(MSG,'(A)'),'   +'
    CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
    WRITE(MSG,'(A,ES10.2)'),'   + Gaz transport residue = ',DOM%RSL%err_it_A
    CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
    WRITE(MSG,'(A,I7)'),'   + Gaz transport it. number = ',DOM%RSL%niter_A
    CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
    WRITE(MSG,'(A,F10.2)'),'   + Max. CFL number =       ',MAXVAL(ABS(DOM%ETAT%CFL))
    CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
END IF

! nombre d'it. et erreur STRUCTURE
IF (DOM%NUM%STRUCTURE == 'on') THEN
    WRITE(MSG,'(A)'),'   +'
    CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
    WRITE(MSG,'(A,ES9.2)'),'   + Mechanical residue =     ',DOM%RSL%err_it_S
    CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
    WRITE(MSG,'(A,I5)'),'   + Mechanical it. number =      ',DOM%RSL%niter_S
    CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
END IF

! nombre d'it. et erreur ABLATION
IF (DOM%NUM%ABLATION == 'on') THEN
    WRITE(MSG,'(A)'),'   +'
    CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
    WRITE(MSG,'(A,ES9.2,A,ES9.2,A,ES9.2,A)'),'   + Max. relative mesh deformation = [',DOM%RSL%err_eq_AB(1),', ',DOM%RSL%err_eq_AB(2),', ',DOM%RSL%err_eq_AB(3),']'
    CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
    WRITE(MSG,'(A,I5,A,I5,A,I5,A)'),'   + Mesh deformation it. number = [',DOM%RSL%niter_eq_AB(1),', ',DOM%RSL%niter_eq_AB(2),', ',DOM%RSL%niter_eq_AB(3),']'
    CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
    IF (DOM%RSL%nvois_max) THEN
        WRITE(MSG,'(A,I5,A)'),'   + Smoothing with all nodes.'
    ELSE
        WRITE(MSG,'(A,I5,A)'),'   + Smoothing with ',DOM%NUM%nvois_def,' neighboring nodes.'
    END IF
    CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)   
    WRITE(MSG,'(A,ES9.2,A,ES9.2,A)'),'   + Max. projection relative error = [',DOM%RSL%err_proj_AB(1),', ',DOM%RSL%err_proj_AB(2),']'
    CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
    WRITE(MSG,'(A,I5,A,I5,A)'),'   + Mesh projection it. number = [',DOM%RSL%niter_proj_AB(1),', ',DOM%RSL%niter_proj_AB(2),']'
    CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
    WRITE(MSG,'(A,f5.2,A,f5.3)'),'   + Max. form factor = ',maxval ( DOM%MLG%QUALITE( :,1 ) ) , ' ; obliquite equiangle max = ', maxval ( DOM%MLG%QUALITE( :,2 ) )
    CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
END IF


END SUBROUTINE print_cyc_end
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de print en fin d'export - finalisation des calculs
!> \todo Multidomaines dans print_cyc, dom de ref = no1
SUBROUTINE print_expt( DOM )
TYPE (typ_dom) :: DOM
INTEGER(IP) :: iexpt, nexpt
CHARACTER (len=128) :: MSG

WRITE(MSG,'(A)'),'+--------------------------------------------------------------------------+'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A)'),'+--> STAGE IV - EXPORT'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
WRITE(MSG,'(A)'),'+'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

WRITE(MSG,'(A,A)'),'+ Export name: ',DOM%EXPT%nom_exp
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

IF (DOM%EXPT%tecplot_bin == 1) THEN
    WRITE(MSG,'(A)'),'+ Binary Tecplot output: ON'
    CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
END IF

IF (DOM%EXPT%tecplot_ascii == 1) THEN
    WRITE(MSG,'(A)'),'+ ASCII Tecplot output:  ON'
    CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
END IF

IF (DOM%EXPT%vtk_ascii == 1) THEN
    WRITE(MSG,'(A)'),'+ ASCII VTK output:      ON'
    CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
END IF

IF (DOM%SUI%file_hdf5 == 1) THEN
    WRITE(MSG,'(A)'),'+ HDF5 output:           ON'
    CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
END IF

WRITE(MSG,'(A)'),'+ Exported variables:'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

nexpt = DOM%EXPT%nexpt
DO iexpt = 1,nexpt
    WRITE(MSG,'(A,I3,A,A)'),'   + ',iexpt,' : ',DOM%EXPT%NOM_VAR_EXP(iexpt)
    CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)
END DO

WRITE(MSG,'(A)'),'+--------------------------------------------------------------------------+'
CALL print_out(MSG, DOM%SUI%suivi, DOM%SUI%file_log)

END SUBROUTINE print_expt
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de print en debut de couplage CWIPI
SUBROUTINE print_solve_C_ini( couplingID, exch_name, tag, nvar_isend_coupling, nvar_irecv_coupling, isend_coupling_name, irecv_coupling_name, suivi, file_log )
INTEGER(IP) :: suivi, file_log
CHARACTER (len=128) :: MSG
CHARACTER (len=128) :: couplingID, exch_name
INTEGER(IP) :: tag, nvar_isend_coupling, nvar_irecv_coupling
CHARACTER (len=256) :: isend_coupling_name, irecv_coupling_name
!
WRITE(MSG,'(A)'),'+------------> CWIPI coupling:'
CALL print_out(MSG, suivi, file_log)
WRITE(MSG,'(A,A)'),'+              Coupling name: ',TRIM(couplingID)
CALL print_out(MSG, suivi, file_log)
WRITE(MSG,'(A,A)'),'+              Exchange name: ',TRIM(exch_name)
CALL print_out(MSG, suivi, file_log)
WRITE(MSG,'(A,I2,A,I3,A)'),'+              <--',nvar_isend_coupling,' send variables (tag = ',tag,')'
CALL print_out(MSG, suivi, file_log)
WRITE(MSG,'(A,A)'),'+              <-- Fields: ',TRIM(isend_coupling_name)
CALL print_out(MSG, suivi, file_log)
WRITE(MSG,'(A,I2,A,I3,A)'),'+              -->',nvar_irecv_coupling,' received variables (tag = ',tag,')'
CALL print_out(MSG, suivi, file_log)
WRITE(MSG,'(A,A)'),'+              --> Fields: ',TRIM(irecv_coupling_name)
CALL print_out(MSG, suivi, file_log)
!
END SUBROUTINE print_solve_C_ini
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de print en fin de couplage CWIPI
SUBROUTINE print_solve_C_end( couplingID, suivi, file_log )
INTEGER(IP) :: suivi, file_log
CHARACTER (len=128) :: MSG
CHARACTER (len=128) :: couplingID
!
WRITE(MSG,'(A,A,A)'),'+<----------- Successful exchange (Coupling name: ',TRIM(couplingID),')'
CALL print_out(MSG, suivi, file_log)
!
END SUBROUTINE print_solve_C_end
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_print
