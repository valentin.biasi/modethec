!> Module des fonctions d'interpolations
MODULE mod_interpolate
!
USE mod_cst, ONLY : IP, DP, eps_min
USE mod_print, ONLY : print_err
!
USE mod_mlg
USE mod_cdt
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Interpolations aux faces du champ de température aux cellules
!> Modification de interp_fac pour les cellules limites avec température
!> imposée
SUBROUTINE interp_T( T, ncel, T_itp, grad_T_itp, nfac, MLG, CDT )
!
!---- Arguments -------------------------------------------------------!
TYPE (typ_cdt), INTENT(IN) :: CDT
TYPE (typ_mlg), INTENT(IN) :: MLG
INTEGER(IP), INTENT(IN):: ncel
REAL(DP), DIMENSION(ncel), INTENT(IN) :: T
INTEGER(IP), INTENT(IN) :: nfac
REAL(DP), DIMENSION(nfac), INTENT(OUT) :: T_itp
REAL(DP), DIMENSION(nfac,3), INTENT(OUT) :: grad_T_itp
!
!---- Déclarations ----------------------------------------------------!
INTEGER(IP) :: ilim, nlim
CHARACTER (len=32) :: ener_typ
INTEGER(IP) :: nfac_limi
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_FACLIMi
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_CELLIMi
!
!---- Récupérations ---------------------------------------------------!
nlim = CDT%nlim
!
!
! Valeurs et gradients aux faces
CALL grad_fac( T, T_itp, grad_T_itp, nfac, ncel, MLG%nsom, MLG%FAC_ITP(:), MLG%CEL_ITP(:), MLG%SOM_ITP(:), MLG%dim_simu )
!
! Valeurs interpolées aux faces
! CALL interp_fac( T, T_itp, nfac, ncel, MLG%nsom, MLG%FAC_ITP(:), MLG%CEL_ITP(:), MLG%SOM_ITP(:), MLG%dim_simu )
!
!
! Boucle par limite
DO ilim = 1,nlim
    
    ! Nombre de faces par limite
    nfac_limi = CDT%LIM(ilim)%nfac_limi
    
    ! Tableaux locaux des ID des faces et cel associées
    ALLOCATE( ID_FACLIMi(nfac_limi) )
    ALLOCATE( ID_CELLIMi(nfac_limi) )
    ID_FACLIMi = CDT%LIM(ilim)%ID_FACLIMi
    ID_CELLIMi = MLG%FAC2CEL(ID_FACLIMi,3)
    
    ! Suivant le type de limite ener. ...
    ener_typ = CDT%LIM(ilim)%ener_typ
    SELECT CASE (ener_typ)

        !... on impose le flux aux parois
        CASE ("flux")
            ! IF (ALL(ABS(CDT%LIM(ilim)%Fimp(:)) < 2.0_dp*eps_min)) THEN
            !     grad_T_itp( ID_FACLIMi,: ) = 0.0_dp
            ! END IF
            
        !... ou on impose la température aux parois
        CASE ("temp")
            
            ! température de paroi
            T_itp( ID_FACLIMi ) = CDT%LIM(ilim)%Timp(:)
            
            ! et gradient aux parois = (Tlim - Tcel) * vec_G1H / n2_G1H
            grad_T_itp( ID_FACLIMi,1 ) = CDT%LIM(ilim)%Timp(:) - T( ID_CELLIMi )
            grad_T_itp( ID_FACLIMi,2 ) = CDT%LIM(ilim)%Timp(:) - T( ID_CELLIMi )
            grad_T_itp( ID_FACLIMi,3 ) = CDT%LIM(ilim)%Timp(:) - T( ID_CELLIMi )
            
            grad_T_itp( ID_FACLIMi,:) = grad_T_itp( ID_FACLIMi,:) * MLG%G1H(ID_FACLIMi,:)
            grad_T_itp( ID_FACLIMi,1 ) = grad_T_itp( ID_FACLIMi,1 ) / MLG%n2_G1H(ID_FACLIMi)
            grad_T_itp( ID_FACLIMi,2 ) = grad_T_itp( ID_FACLIMi,2 ) / MLG%n2_G1H(ID_FACLIMi)
            grad_T_itp( ID_FACLIMi,3 ) = grad_T_itp( ID_FACLIMi,3 ) / MLG%n2_G1H(ID_FACLIMi)


        CASE DEFAULT
            ! Rien sinon
            
    END SELECT
    
    DEALLOCATE( ID_FACLIMi, ID_CELLIMi ) ! Désallocation pour nouvelle limite
    
END DO

END SUBROUTINE interp_T
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Interpolations aux faces du champ de pression aux cellules
!> Modification de interp_fac pour les cellules limites avec pression
!> imposée
SUBROUTINE interp_P( P, ncel, P_itp, grad_P_itp, nfac, MLG, CDT )
!
!---- Arguments -------------------------------------------------------!
TYPE (typ_cdt), INTENT(IN) :: CDT
TYPE (typ_mlg), INTENT(IN) :: MLG
INTEGER(IP) , INTENT(IN):: ncel
REAL(DP), DIMENSION(ncel), INTENT(IN) :: P
INTEGER(IP), INTENT(IN) :: nfac
REAL(DP), DIMENSION(nfac), INTENT(OUT) :: P_itp
REAL(DP), DIMENSION(nfac,3), INTENT(OUT) :: grad_P_itp
!
!---- Déclarations ----------------------------------------------------!
INTEGER(IP) :: ilim, nlim
CHARACTER (len=32) :: masse_typ
INTEGER(IP) :: nfac_limi
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_FACLIMi
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ID_CELLIMi
!
!---- Récupérations ---------------------------------------------------!
nlim = CDT%nlim
!
!
! Valeurs et gradients aux faces
CALL grad_fac( P, P_itp, grad_P_itp, nfac, ncel, MLG%nsom, MLG%FAC_ITP(:), MLG%CEL_ITP(:), MLG%SOM_ITP(:), MLG%dim_simu )
!
! Valeurs interpolées aux faces
! CALL interp_fac( P, P_itp, nfac, ncel, MLG%nsom, MLG%FAC_ITP(:), MLG%CEL_ITP(:), MLG%SOM_ITP(:), MLG%dim_simu )
!
! Boucle par limite
DO ilim = 1,nlim
    
    ! Nombre de faces par limite
    nfac_limi = CDT%LIM(ilim)%nfac_limi
    
    ! Tableaux locaux des ID des faces et cel associées
    ALLOCATE( ID_FACLIMi(nfac_limi) )
    ALLOCATE( ID_CELLIMi(nfac_limi) )
    ID_FACLIMi = CDT%LIM(ilim)%ID_FACLIMi
    ID_CELLIMi = MLG%FAC2CEL(ID_FACLIMi,3)
    
    ! Suivant le type de limite masse ...
    masse_typ = CDT%LIM(ilim)%masse_typ
    SELECT CASE (masse_typ)

        !... on impose le debit massique aux parois
        CASE ("debit")
            IF (ALL(CDT%LIM(ilim)%Dimp(:) == 0)) THEN
                grad_P_itp( ID_FACLIMi,: ) = 0.0_dp
            END IF
            
        !... ou on impose la pression aux parois
        CASE ("pression")
            
            ! pression de paroi
            P_itp( ID_FACLIMi ) = CDT%LIM(ilim)%Pimp(:)
            
            ! et gradient aux parois = (Plim - Pcel) * vec_G1H / n2_G1H
            grad_P_itp( ID_FACLIMi,1 ) = CDT%LIM(ilim)%Pimp(:) - P( ID_CELLIMi )
            grad_P_itp( ID_FACLIMi,2 ) = CDT%LIM(ilim)%Pimp(:) - P( ID_CELLIMi )
            grad_P_itp( ID_FACLIMi,3 ) = CDT%LIM(ilim)%Pimp(:) - P( ID_CELLIMi )
            
            grad_P_itp( ID_FACLIMi,:) = grad_P_itp( ID_FACLIMi,:) * MLG%G1H(ID_FACLIMi,:)
            grad_P_itp( ID_FACLIMi,1 ) = grad_P_itp( ID_FACLIMi,1 ) / MLG%n2_G1H(ID_FACLIMi)
            grad_P_itp( ID_FACLIMi,2 ) = grad_P_itp( ID_FACLIMi,2 ) / MLG%n2_G1H(ID_FACLIMi)
            grad_P_itp( ID_FACLIMi,3 ) = grad_P_itp( ID_FACLIMi,3 ) / MLG%n2_G1H(ID_FACLIMi)

    END SELECT

    DEALLOCATE( ID_FACLIMi, ID_CELLIMi ) ! Désallocation pour nouvelle limite
    
END DO

END SUBROUTINE interp_P
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul du gradient de U uniquement sur une cellule
SUBROUTINE grad_one_cel( U, grad_U, ncel_adj, CEL_ITP, dim_simu )
!
!---- Arguments -------------------------------------------------------!
TYPE (typ_cel_itp), INTENT(IN) :: CEL_ITP
CHARACTER (len=32), INTENT(IN) :: dim_simu
INTEGER(IP), INTENT(IN) :: ncel_adj
REAL(DP), DIMENSION(3), INTENT(INOUT) :: grad_U
REAL(DP), DIMENSION(ncel_adj), INTENT(IN) :: U
!
! X_itp = [a b c d] = Coefficients linéires de U en fonction de X, Y et Z
! U_itp = a*X + b*Y + c*Z + d
!X_itp = MATMUL( CEL_ITP(icel)%M_itp, B_itp(1:ncel_adj) )

! Donc dU/dX = a
grad_U(1) = DOT_PRODUCT( CEL_ITP%M_itp(1,:), U )
! Et dU/dY = b
grad_U(2) = DOT_PRODUCT( CEL_ITP%M_itp(2,:), U )
! Et dU/dZ = c
IF (dim_simu == "3D") THEN
    grad_U(3) = DOT_PRODUCT( CEL_ITP%M_itp(3,:), U )
ELSE
    grad_U(3) = 0.0_dp
END IF 

END SUBROUTINE grad_one_cel
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul des gradients du champ U aux cellules
SUBROUTINE grad_cel( U, grad_U, ncel, CEL_ITP, dim_simu )
!
!---- Arguments -------------------------------------------------------!
TYPE (typ_cel_itp), DIMENSION(ncel), INTENT(IN) :: CEL_ITP
CHARACTER (len=32), INTENT(IN) :: dim_simu
INTEGER(IP), INTENT(IN) :: ncel
REAL(DP), DIMENSION(ncel,3), INTENT(INOUT) :: grad_U
REAL(DP), DIMENSION(ncel), INTENT(IN) :: U
!
!---- Déclarations ----------------------------------------------------!
INTEGER(IP) :: icel
REAL(DP), DIMENSION(9) :: B_itp ! On fixe la taille maximale de B_itp pour eviter les alloc/dealloc
INTEGER(IP) :: ncel_adj
REAL(DP), DIMENSION(3) :: grad_U_icel
!
!
! Boucles sur les cellules
DO icel = 1,ncel

    ncel_adj = CEL_ITP(icel)%ncel_adj       ! Nombre de cel adj à icel

    ! Vecteur B_itp remplies des valeurs du champs U aux cellules adjacentes
    B_itp(:) = 0.0_dp
    B_itp(1:ncel_adj) = U( CEL_ITP(icel)%CEL_ADJ )

    !--- Appel à la fonction grad_one_cel pour calcul du gradient uniquement sur icel
    CALL grad_one_cel( B_itp(1:ncel_adj), grad_U_icel, ncel_adj, CEL_ITP(icel), dim_simu )
    grad_U(icel,:) = grad_U_icel
    
END DO

END SUBROUTINE grad_cel
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Valeurs interpolées au sommet V du champ U aux cellules
SUBROUTINE interp_one_som( U, V, ncel, CEL_ITP, SOM_ITP, dim_simu )
!
!---- Arguments -------------------------------------------------------!
TYPE (typ_som_itp), INTENT(IN) :: SOM_ITP
TYPE (typ_cel_itp), DIMENSION(ncel), INTENT(IN) :: CEL_ITP
CHARACTER (len=32), INTENT(IN) :: dim_simu
INTEGER(IP), INTENT(IN) :: ncel
REAL(DP), DIMENSION(ncel), INTENT(IN) :: U
REAL(DP), INTENT(OUT) :: V
!
!---- Déclarations ----------------------------------------------------!
INTEGER(IP), PARAMETER :: ncel_adj_max = 90 ! Nombre max de cel voisines par sommets a modifier si bug
REAL(DP), DIMENSION(ncel_adj_max) :: B_itp ! On fixe la taille maximale de B_itp pour eviter les alloc/dealloc
INTEGER(IP) :: icel_adj, ncel_adj, ncel_adj_icel, id_cel_adj
REAL(DP), DIMENSION(3) :: grad_U_ncel, grad_U_icel
!

ncel_adj = SOM_ITP%ncel_adj ! Nb de cellules adjacentes

! Interpolations aux sommets internes
IF (ALLOCATED(SOM_ITP%A_itp)) THEN
    
    ! Vecteur B_itp remplies des valeurs du champs U aux cellules adjacentes
    B_itp = 0.0_dp
    B_itp(1:ncel_adj) = U( SOM_ITP%CEL_ADJ )
    
    ! X_itp = [a b c d] = Coefficients linéires de U en fonction de X, Y et Z
    ! ! Projection de U sur le plan formé par les coefficients grad_V
    ! ! U = a*X + b*Y + c*Z + d
    V = DOT_PRODUCT( SOM_ITP%A_itp(:), B_itp(1:ncel_adj) )

! Extrapolation aux sommets limites
ELSE

    ! U moyen aux 1 ou 2 ou 3 aux points adjacents
    V = SUM( U( SOM_ITP%CEL_ADJ ) ) / SOM_ITP%ncel_adj

    ! Gradien moyen des ncel_adj du sommet courant
    grad_U_ncel(:) = 0.0_dp

    ! Pour chaque cellule adjacente au sommet courant
    DO icel_adj = 1,ncel_adj

        id_cel_adj = SOM_ITP%CEL_ADJ(icel_adj) ! Indice de la cellule icel_adj

        ncel_adj_icel = CEL_ITP(id_cel_adj)%ncel_adj ! Nombre de cel adj à icel_adj

        ! Vecteur B_itp remplies des valeurs du champs U aux cellules adjacentes
        B_itp(:) = 0.0_dp
        B_itp(1:ncel_adj_icel) = U( CEL_ITP(id_cel_adj)%CEL_ADJ )

        ! Calcul du gradient sur la cellule id_cel_adj
        CALL grad_one_cel( B_itp(1:ncel_adj_icel), grad_U_icel, ncel_adj_icel, CEL_ITP(id_cel_adj), dim_simu )

        grad_U_ncel = grad_U_ncel + grad_U_icel ! Somme des gradients pour moyenne

    END DO ! Fin icel_adj

    grad_U_ncel = grad_U_ncel / ncel_adj ! Moyenne des gradients des cellules adjacentes de isom

    V = V + DOT_PRODUCT( grad_U_ncel, SOM_ITP%vec_GS ) ! Extrapolation grâce au gradient moyen

END IF

END SUBROUTINE interp_one_som
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Valeurs interpolées aux sommets V du champ U aux cellules
SUBROUTINE interp_som( U, V, ncel, nsom, CEL_ITP, SOM_ITP, dim_simu )
!
!---- Arguments -------------------------------------------------------!
TYPE (typ_som_itp), DIMENSION(nsom), INTENT(IN) :: SOM_ITP
TYPE (typ_cel_itp), DIMENSION(ncel), INTENT(IN) :: CEL_ITP
CHARACTER (len=32), INTENT(IN) :: dim_simu
INTEGER(IP), INTENT(IN) :: ncel
INTEGER(IP), INTENT(IN) :: nsom
REAL(DP), DIMENSION(ncel), INTENT(IN) :: U
REAL(DP), DIMENSION(nsom), INTENT(OUT) :: V
!
!---- Déclarations ----------------------------------------------------!
INTEGER(IP) :: isom
INTEGER(IP), PARAMETER :: ncel_adj_max = 90 ! Nombre max de cel voisines par sommets a modifier si bug
INTEGER(IP) :: ncel_adj
!

! Boucle sur les sommets
DO isom = 1,nsom

    ncel_adj = SOM_ITP(isom)%ncel_adj ! Nb de cellules adjacentes

    ! Gestion erreur : depacement ncel_adj_max
    IF (ncel_adj > ncel_adj_max) THEN
        CALL print_err('Le nombre max de cel_adj par sommet est depasse, veuillez augmenter la limite', 1)
    ENDIF

    !--- Appel à la fonction interp_one_som pour calcul du champ interpolé uniquement sur isom
    CALL interp_one_som( U, V(isom), ncel, CEL_ITP, SOM_ITP(isom), dim_simu )

END DO

END SUBROUTINE interp_som
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul des gradients grad_W du champ U aux cellules
SUBROUTINE grad_fac( U, W, grad_W, nfac, ncel, nsom, FAC_ITP, CEL_ITP, SOM_ITP, dim_simu )
!
!---- Arguments -------------------------------------------------------!
TYPE (typ_fac_itp), DIMENSION(nfac), INTENT(IN) :: FAC_ITP
TYPE (typ_som_itp), DIMENSION(nsom), INTENT(IN) :: SOM_ITP
TYPE (typ_cel_itp), DIMENSION(ncel), INTENT(IN) :: CEL_ITP
CHARACTER (len=32), INTENT(IN) :: dim_simu
INTEGER(IP), INTENT(IN) :: nfac, ncel, nsom
REAL(DP), DIMENSION(ncel), INTENT(IN) :: U
REAL(DP), DIMENSION(nfac), INTENT(OUT) :: W
REAL(DP), DIMENSION(nfac,3), INTENT(OUT) :: grad_W
!
!---- Déclarations ----------------------------------------------------!
INTEGER(IP) :: ifac
INTEGER(IP) :: npoint
INTEGER(IP) :: ncel_adj, nsom_adj
REAL(DP), DIMENSION(6) :: B_itp ! On fixe la taille maximale de B_itp pour eviter les alloc/dealloc
REAL(DP), DIMENSION(nsom) :: V
!

! Valeurs interpolées aux sommets
CALL interp_som( U, V, ncel, nsom, CEL_ITP, SOM_ITP, dim_simu )

! Boucle sur les faces
DO ifac = 1,nfac
    
    ! Récupérations
    ncel_adj = FAC_ITP(ifac)%ncel_adj
    nsom_adj = FAC_ITP(ifac)%nsom_adj
    
    ! Nombre de points servant à l'interpolation
    npoint = ncel_adj + nsom_adj
        
    ! Vecteur B_itp remplies des valeurs du champs U aux cellules adjacentes
    B_itp(:) = 0.0_dp
    B_itp(1:ncel_adj) = U( FAC_ITP(ifac)%CEL_ADJ )
    ! et V aux sommets adjacents
    B_itp(ncel_adj+1:ncel_adj+nsom_adj) = V( FAC_ITP(ifac)%SOM_ADJ )
    
    ! X_itp = [a b c d] = Coefficients linéires de U en fonction de X, Y et Z
    ! U_itp = a*X + b*Y + c*Z + d
    grad_W(ifac,1) = DOT_PRODUCT( FAC_ITP(ifac)%M_itp(1,:), B_itp(1:npoint) )
    grad_W(ifac,2) = DOT_PRODUCT( FAC_ITP(ifac)%M_itp(2,:), B_itp(1:npoint) )
    IF (dim_simu == "3D") THEN
        grad_W(ifac,3) = DOT_PRODUCT( FAC_ITP(ifac)%M_itp(3,:), B_itp(1:npoint) )
    ELSE
        grad_W(ifac,3) = 0.0_dp
    END IF

    ! ! X_itp = [a b c d] = Coefficients linéires de U en fonction de X, Y et Z
    ! ! Projection du champ sur le plan formé par les coefficients X_itp
    ! ! W = a*X + b*Y + c*Z + d
    W(ifac) =  DOT_PRODUCT( FAC_ITP(ifac)%A_itp, B_itp(1:npoint) )

END DO

END SUBROUTINE grad_fac
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul des interpolations W du champ U aux cellules
SUBROUTINE interp_fac( U, W, nfac, ncel, nsom, FAC_ITP, CEL_ITP, SOM_ITP, dim_simu )
!
!---- Arguments -------------------------------------------------------!
TYPE (typ_fac_itp), DIMENSION(nfac), INTENT(IN) :: FAC_ITP
TYPE (typ_som_itp), DIMENSION(nsom), INTENT(IN) :: SOM_ITP
TYPE (typ_cel_itp), DIMENSION(ncel), INTENT(IN) :: CEL_ITP
CHARACTER (len=32), INTENT(IN) :: dim_simu
INTEGER(IP), INTENT(IN) :: nfac, ncel, nsom
REAL(DP), DIMENSION(ncel), INTENT(IN) :: U
REAL(DP), DIMENSION(nfac), INTENT(OUT) :: W
!
!---- Déclarations ----------------------------------------------------!
INTEGER(IP) :: ifac
INTEGER(IP) :: npoint
INTEGER(IP) :: ncel_adj, nsom_adj
REAL(DP), DIMENSION(6) :: B_itp ! On fixe la taille maximale de B_itp pour eviter les alloc/dealloc
REAL(DP), DIMENSION(nsom) :: V
!

! Valeurs interpolées aux sommets
CALL interp_som( U, V, ncel, nsom, CEL_ITP, SOM_ITP, dim_simu )

! Boucle sur les faces
DO ifac = 1,nfac
    
    ! Récupérations
    ncel_adj = FAC_ITP(ifac)%ncel_adj
    nsom_adj = FAC_ITP(ifac)%nsom_adj
    
    ! Nombre de points servant à l'interpolation
    npoint = ncel_adj + nsom_adj
        
    ! Vecteur B_itp remplies des valeurs du champs U aux cellules adjacentes
    B_itp(:) = 0.0_dp
    B_itp(1:ncel_adj) = U( FAC_ITP(ifac)%CEL_ADJ )
    ! et V aux sommets adjacents
    B_itp(ncel_adj+1:ncel_adj+nsom_adj) = V( FAC_ITP(ifac)%SOM_ADJ )
    
    ! ! X_itp = [a b c d] = Coefficients linéires de U en fonction de X, Y et Z
    ! ! Projection du champ sur le plan formé par les coefficients X_itp
    ! ! W = a*X + b*Y + c*Z + d
    W(ifac) =  DOT_PRODUCT( FAC_ITP(ifac)%A_itp, B_itp(1:npoint) )

END DO

END SUBROUTINE interp_fac
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Calcul de l'interpolations W de la face ifac par rapport au champ U aux cellules
SUBROUTINE interp_one_fac( U, w, FAC_ITP, MLG)
!
!---- Arguments -------------------------------------------------------!
TYPE (typ_mlg), INTENT(IN) :: MLG
REAL(DP), DIMENSION(MLG%ncel), INTENT(IN) :: U
REAL(DP), INTENT(OUT) :: w
TYPE (typ_fac_itp), INTENT(IN) :: FAC_ITP
!
!---- Déclarations ----------------------------------------------------!
INTEGER(IP) :: npoint
INTEGER(IP) :: ncel_adj, isom_adj, nsom_adj, id_som_adj
REAL(DP), DIMENSION(6) :: B_itp ! On fixe la taille maximale de B_itp pour eviter les alloc/dealloc
REAL(DP) :: v
!
! Récupérations
ncel_adj = FAC_ITP%ncel_adj
nsom_adj = FAC_ITP%nsom_adj

! Nombre de points servant à l'interpolation
npoint = ncel_adj + nsom_adj

! Vecteur B_itp remplies des valeurs du champs U aux cellules adjacentes
B_itp(:) = 0.0_dp
B_itp(1:ncel_adj) = U( FAC_ITP%CEL_ADJ )

DO isom_adj = 1,nsom_adj

    id_som_adj = FAC_ITP%SOM_ADJ(isom_adj)

    !--- Appel à la fonction interp_one_som pour calcul du champ interpolé uniquement sur isom
    CALL interp_one_som( U, v, MLG%ncel, MLG%CEL_ITP, MLG%SOM_ITP(id_som_adj), MLG%dim_simu )

    ! et V aux sommets adjacents
    B_itp(ncel_adj+isom_adj) = v


END DO

! ! X_itp = [a b c d] = Coefficients linéires de U en fonction de X, Y et Z
! ! Projection du champ sur le plan formé par les coefficients X_itp
! ! W = a*X + b*Y + c*Z + d
w =  DOT_PRODUCT( FAC_ITP%A_itp, B_itp(1:npoint) )


END SUBROUTINE interp_one_fac
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_interpolate
