!> Declaration des constantes du solveur
MODULE mod_cst
! 
INTEGER, PARAMETER :: DP = 8                                            !< DP : Double Precision
INTEGER, PARAMETER :: IP = 4                                            !< IP : Integer Precision
REAL(DP),PARAMETER :: Pi = 3.141592653589793_dp                         !< Pi
REAL(DP),PARAMETER :: sigmaSB = 5.670373e-8_dp                          !< Stefan Boltzman constant (W.m^-2.K^-4)
REAL(DP),PARAMETER :: R_GP = 8.314462_dp                                !< Ideal gases constant
REAL(DP),PARAMETER :: lambdaN2 = 3.31_dp                                !< Mass ratio N2/O2 in atmosphere
REAL(DP),PARAMETER :: tinym = TINY(1.0_dp)                              !< Minimum positive real
REAL(DP),PARAMETER :: hugem = HUGE(1.0_dp)                              !< Maximum positive real
REAL(DP),PARAMETER :: eps_min = EPSILON(1.0_dp)                         !< Machine epsilon
REAL(DP),PARAMETER :: P_ref = 1.0e5_dp                                  !< Reference pressure to stabilize the advection solver

CHARACTER(len=32), PARAMETER :: output_file_name = 'mdtc_output.log'    !< Nom du fichier log des sorties
CHARACTER(len=32), PARAMETER :: error_file_name = 'mdtc_error.log'      !< Nom du fichier log des erreurs

INTEGER, PARAMETER :: prm_file_id = 1                                   !< IUNIT du fichier de paramètres
INTEGER, PARAMETER :: mesh_file_id = 2                                  !< IUNIT du fichier de maillage
INTEGER, PARAMETER :: tecplot_file_id = 3                               !< IUNIT du fichier de sortie tecplot
INTEGER, PARAMETER :: vtk_file_id = 3                                   !< IUNIT du fichier de sortie vtk
INTEGER, PARAMETER :: output_file_id = 4                                !< IUNIT du fichier log sorties
INTEGER, PARAMETER :: error_file_id = 5                                 !< IUNIT du fichier log erreurs

! Données pour l'ablation
INTEGER, PARAMETER, DIMENSION(7) :: coef_bin = (/1,6,15,20,15,6,1/)     !< Valeurs du coefficient binomial k parmi n pour n = 6, k=0,6
REAL, PARAMETER, DIMENSION(7) :: WEIGHT_CTR = (/ 1.0, 0.7, 0.7**2 ,0.7**3 ,0.7**2 ,0.7 ,1.0 /) !< Poids des points de contrôles (courbe de Bezier)
REAL :: angle_surf_abl = 45                                             !< Angle (degre) entre 2 faces a partir duquel elles n'auront pas le meme tag pour la construction des patchs
REAL :: amorti_eq_abl_C = 0.9                                           !< Amortissement artificiel pour l'equilibrage des sommets sur les courbes(Laplacien)
REAL :: amorti_eq_abl_S = 0.9                                           !< Amortissement artificiel pour l'equilibrage des sommets sur les surfaces(Laplacien)
REAL :: amorti_eq_abl_V = 1.0                                           !< Amortissement artificiel pour l'equilibrage des sommets dans le volume (Laplacien)
REAL :: pourcentage_qualite_1 = 1.1                                     !< Variation du facteur de qualité de maillage 1 acceptable avant nvois = nvois + 1
REAL :: pourcentage_qualite_2 = 1.1                                     !< Variation du facteur de qualité de maillage 2 acceptable avant nvois = nvois + 1

!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_cst
