!> Module contentant toutes les routines d'algèbre linéaire classique
!> \author Maxime Piqueras
!> \author Ghislain Blanchard
!> \author Pierre Trontin
!> \author Valentin Biasi
MODULE mod_algebra
!
USE mod_cst, ONLY : IP, DP, eps_min, tinym
!
IMPLICIT NONE
CONTAINS

  !--------------------------------------------------------------------!
  ! Function int_in_list
  !
  !     Check if an integer value is in a inetger list
  !
  ! Input Variable:
  !     -A : a vector (one dimensionnal array if integers)
  !     -ia : the integer to check in A
  !
  ! Output Variable:
  !     -bool : True if ia is in A False otherwise
  !--------------------------------------------------------------------!
  FUNCTION int_in_list(A, ia) result(bool)

    INTEGER(KIND=IP), DIMENSION(:), INTENT(in) :: A
    INTEGER(KIND=IP), INTENT(in) :: ia
    INTEGER(KIND=IP) :: i
    LOGICAL :: bool

    bool = .FALSE.
    DO i = 1,SIZE(A)
    
        IF (A(i) == ia) THEN
            bool = .TRUE.
            EXIT
        END IF
    END DO

    RETURN
  END FUNCTION int_in_list
  
  !--------------------------------------------------------------------!
  ! Function twice_in_list
  !
  !     Find what values are common to A and B and are different from ia :
  !     ib /= ia and ia is in A and B
  !
  ! Input Variable:
  !     -A : a vector (one dimensionnal array if integers)
  !     -B : a vector (one dimensionnal array if integers)
  !     -ia : the integer to avoid
  !
  ! Output Variable:
  !     -ib : the integer found (0 means no common values)
  !--------------------------------------------------------------------!
  FUNCTION twice_in_list(A, B, ia) result(ib)

    INTEGER(KIND=IP), DIMENSION(:), INTENT(in) :: A, B
    INTEGER(KIND=IP), INTENT(in) :: ia
    INTEGER(KIND=IP) :: ib
    INTEGER(KIND=IP) :: i
    LOGICAL :: bool
    
    ib = 0
    bool = .FALSE.
    
    DO i = 1,SIZE(A)
        
        bool = int_in_list(B, A(i))
        IF (bool) THEN
            
            IF (A(i) /= 0) THEN
            IF (A(i) /= ia) THEN
                ib = A(i)
                EXIT
            END IF
            END IF
            
        END IF
        
    END DO
    

    RETURN
  END FUNCTION twice_in_list
  
  !--------------------------------------------------------------------!
  ! Subroutine mean
  !
  !     Treat the column matrix (two dimensionnal array) as vectors and calculate the mean values of these vectors
  !
  !
  ! Input Variable:
  !     -A : a two dimensionnal array
  !
  ! Output Variable:
  !     -V : a vector (one dimensionnal array)
  !--------------------------------------------------------------------!
  SUBROUTINE mean(A,V)

    REAL(KIND=DP),DIMENSION(:,:),INTENT(in):: A
    REAL(KIND=DP),DIMENSION(:),ALLOCATABLE,INTENT(out) :: V
    INTEGER(KIND=IP) :: i,j

    i=SIZE(A,1)
    j=SIZE(A,2)
    ALLOCATE(V(1:j))
    V(:)=SUM(A,1)/REAL(i,DP)

    RETURN
  END SUBROUTINE mean

  !--------------------------------------------------------------------!
  ! Function cross
  !
  !     Calculate the cross produt of two vectors (3D)
  !
  ! Input Variable:
  !     -V1,V2 : two vector (one dimensionnal array whith 3 cells).
  !
  ! Output Variable:
  !     -V : a vector (one dimensionnal array whith 3 cells).
  !--------------------------------------------------------------------!
  FUNCTION cross(V1,V2) result(V)

    REAL(KIND=DP),DIMENSION(1:3),INTENT(in)::V1,V2
    REAL(KIND=DP),DIMENSION(1:3) :: V

    V(1)=V1(2)*V2(3) - V1(3)*V2(2)
    V(2)=V1(3)*V2(1) - V1(1)*V2(3)
    V(3)=V1(1)*V2(2) - V1(2)*V2(1)

    RETURN
  END FUNCTION cross

  !--------------------------------------------------------------------!
  ! Function dot
  !
  !     Calculate the scalar product of two vectors (3D)
  !
  ! Input Variable:
  !     -V1 : a vector (one dimensionnal array whith 3 cells).
  !     -V2 : a vector (one dimensionnal array whith 3 cells).
  !
  ! Output Variable:
  !     -d : a real
  !--------------------------------------------------------------------!
  FUNCTION dot(V1,V2) result(d)

    REAL(KIND=DP), EXTERNAL :: DDOT
    REAL(KIND=DP),DIMENSION(:),INTENT(in) :: V1,V2
    REAL(KIND=DP) :: d
    INTEGER(KIND=IP) :: n

    n = SIZE(V1)

    d = DDOT(n, V1, 1, V2, 1)

    RETURN
  END FUNCTION dot

  !--------------------------------------------------------------------!
  ! Function norm
  !
  !     Calculate the Euclidian norm of a vector (3D)
  !
  ! Input Variable:
  !     -V : a vector (one dimensionnal array whith 3 cells).
  !
  ! Output Variable:
  !     -nv : a real
  !--------------------------------------------------------------------!
  FUNCTION norm(V) result(nv)

    REAL(KIND=DP),DIMENSION(1:3),INTENT(in) :: V
    REAL(KIND=DP) :: nv,d

    nv = NORM2(V)
    RETURN
  END FUNCTION norm

  !--------------------------------------------------------------------!
  ! Subroutine sort
  !
  !     Sort the values of a one dimensionnal array.
  !     Only for INTEGER values.
  !
  ! Input Variable:
  !     -V : a one dimensionnal array
  !
  ! Output Variable:
  !     -V_sorted : a one dimensionnal array (with the same dimension)
  !--------------------------------------------------------------------!
  SUBROUTINE sort(V,V_sorted)

    INTEGER(KIND=IP),DIMENSION(:),INTENT(in) :: V
    INTEGER(KIND=IP),DIMENSION(:),POINTER :: V_sorted
    INTEGER(KIND=IP),DIMENSION(:),ALLOCATABLE :: V_copy
    INTEGER(KIND=IP) :: nbr,i,min_val
    INTEGER(KIND=IP),DIMENSION(1:1) :: pos

    nbr=SIZE(V)
    ALLOCATE(V_sorted(1:nbr))
    ALLOCATE(V_copy(1:nbr))
    V_copy=V
    DO i=1,nbr
       min_val=MINVAL(V_copy)
       pos=MINLOC(V_copy)
       V_copy(pos(1))=HUGE(i)
       V_sorted(i)=min_val
    END DO
    DEALLOCATE(V_copy)

    RETURN
  END SUBROUTINE sort

  !--------------------------------------------------------------------!
  ! Subroutine sort_real
  !
  !     Sort the values of a one dimensionnal array.
  !     Only for REAL(8) values.
  !
  ! Input Variable:
  !     -V : a one dimensionnal array
  !
  ! Output Variable:
  !     -V_sorted : a one dimensionnal array (with the same dimension)
  !--------------------------------------------------------------------!
  SUBROUTINE sort_real(V,V_sorted)

    REAL(KIND=DP),DIMENSION(:),INTENT(in) :: V
    REAL(KIND=DP),DIMENSION(:),ALLOCATABLE,INTENT(out) :: V_sorted
    REAL(KIND=DP),DIMENSION(:),ALLOCATABLE :: V_copy
    REAL(KIND=DP) :: min_val
    INTEGER(KIND=IP) :: nbr,i
    INTEGER(KIND=IP),DIMENSION(1:1) :: pos

    nbr=SIZE(V)
    ALLOCATE(V_sorted(1:nbr))
    ALLOCATE(V_copy(1:nbr))
    V_copy=V
    DO i=1,nbr
       min_val=MINVAL(V_copy)
       pos=MINLOC(V_copy)
       V_copy(pos(1))=HUGE(i)
       V_sorted(i)=min_val
    END DO
    DEALLOCATE(V_copy)

    RETURN
  END SUBROUTINE sort_real

  !--------------------------------------------------------------------!
  ! Subroutine multiple_free
  !
  !     Treats a one dimensionnal array and return it without multiple elements.
  !     Only for INTEGER values.
  !
  ! Input Variable:
  !     -U : a one dimensionnal array
  !
  ! Output Variable:
  !     -V : a one dimensionnal array without multiple elements
  !          (the output array size will be necessarily shorter ou equal than the input array one)
  !--------------------------------------------------------------------!
  SUBROUTINE multiple_free(V,U)

    INTEGER(KIND=IP),DIMENSION(:),INTENT(in) :: V
    INTEGER(KIND=IP),DIMENSION(:),ALLOCATABLE,INTENT(out) :: U
    INTEGER(KIND=IP),DIMENSION(:),ALLOCATABLE :: V_copy
    INTEGER(KIND=IP) :: nbr,i,val,j,new_nbr

    nbr=SIZE(V)
    new_nbr=nbr
    ALLOCATE(V_copy(1:nbr))
    V_copy=V
    DO i=1,nbr
       val=V(i)
       DO j=i+1,nbr
          IF (V_copy(j)==val) THEN
             V_copy(j)=HUGE(i)
             new_nbr=new_nbr-1
          END IF
       END DO
    END DO
    ALLOCATE(U(1:new_nbr))
    j=1
    i=1
    DO WHILE(j<=new_nbr .AND. i<=nbr)
       IF (V_copy(i)/=HUGE(i)) THEN
          U(j)=V_copy(i)
          j=j+1
       END IF
       i=i+1
    END DO
    DEALLOCATE(V_copy)

    RETURN
  END SUBROUTINE multiple_free

  !--------------------------------------------------------------------!
  ! Subroutine find_int
  !
  !     For an integr value n, return two one dimensionnal arrays :
  !         -the first one contains all the row number where n appear in the input array
  !         -the second one contains the colmn number
  !     Only for INTEGER array.
  !
  ! Input Variable:
  !     -A : a two dimensionnal array of integer
  !     -n : an integer value
  !
  ! Output Variable:
  !     -R : a one dimensionnal array of integer
  !     -C : a one dimensionnal array of integer (OPTIONAL argument)
  !--------------------------------------------------------------------!
  SUBROUTINE find_int(A,n,ROW,COL)

    INTEGER(KIND=IP),DIMENSION(:,:),INTENT(in) :: A
    INTEGER(KIND=IP),INTENT(in) :: n
    INTEGER(KIND=IP),DIMENSION(:),ALLOCATABLE,INTENT(out) :: ROW
    INTEGER(KIND=IP),DIMENSION(:),ALLOCATABLE,OPTIONAL,INTENT(out) :: COL
    INTEGER(KIND=IP) :: r,c,i,j,nbn,p

    r=SIZE(A,1)
    c=SIZE(A,2)
    nbn=0
    DO i=1,r
       DO j=1,c
          IF (A(i,j)==n) THEN
             nbn=nbn+1
          END IF
       END DO
    END DO
    IF (nbn==0) THEN
       WRITE(*,*)'CAUTION :',n,'does not appear in the input array'
       ALLOCATE(ROW(1:1))
       ROW(1)=0
       IF (PRESENT(COL)) THEN
          ALLOCATE(COL(1:1))
          COL(1)=0
       END IF
    ELSE
       ALLOCATE(ROW(1:nbn))
       IF (PRESENT(COL)) THEN
          ALLOCATE(COL(1:nbn))
       END IF
       p=1
       DO i=1,r
          DO j=1,c
             IF (A(i,j)==n) THEN
                ROW(p)=i
                IF (PRESENT(COL)) THEN
                   COL(p)=j
                END IF
                p=p+1
             END IF
          END DO
       END DO
    END IF

    RETURN
  END SUBROUTINE find_int

  !--------------------------------------------------------------------!
  ! Subroutine find_real
  !
  !     For an real value x, return two one dimensionnal arrays :
  !         -the first one contains all the row number where x appear in the input array
  !         -the second one contains the colmn number
  !     Only for REAL(KIND=DP) array.
  !
  ! Input Variable:
  !     -A : a two dimensionnal array of real
  !     -n : a real value
  !
  ! Output Variable:
  !     -R : a one dimensionnal array of integer
  !     -C : a one dimensionnal array of integer
  !--------------------------------------------------------------------!
  SUBROUTINE find_real(A,x,ROW,COL)

    REAL(KIND=DP),DIMENSION(:,:),INTENT(in) :: A
    REAL(KIND=DP),INTENT(in) :: x
    INTEGER(KIND=IP),DIMENSION(:),ALLOCATABLE,INTENT(out) :: ROW,COL
    INTEGER(KIND=IP) :: r,c,i,j,nbx,p

    r=SIZE(A,1)
    c=SIZE(A,2)
    nbx=0
    DO i=1,r
       DO j=1,c
          IF (A(i,j)==x) THEN
             nbx=nbx+1
          END IF
       END DO
    END DO
    IF (nbx==0) THEN
       WRITE(*,*)'CAUTION :',x,'does not appear in the input array'
       ALLOCATE(ROW(1:1))
       ALLOCATE(COL(1:1))
       ROW(1)=0
       COL(1)=0
    ELSE
       ALLOCATE(ROW(1:nbx))
       ALLOCATE(COL(1:nbx))
       p=1
       DO i=1,r
          DO j=1,c
             IF (A(i,j)==x) THEN
                ROW(p)=i
                COL(p)=j
                p=p+1
             END IF
          END DO
       END DO
    END IF
    RETURN
  END SUBROUTINE find_real

  !--------------------------------------------------------------------!
  ! Function fac
  !
  !     Calculate the factorial of an integer
  !
  ! Input Variable:
  !     -n : an integer value
  !
  ! Output Variable:
  !     -f : an integer value
  !--------------------------------------------------------------------!
  FUNCTION fac(n) result(f)

    INTEGER(KIND=IP), INTENT(in) :: n
    INTEGER(KIND=IP) :: f,p

    IF (n==0) THEN
       f=1
    ELSE
       f=1
       DO p=2,n
          f=f*p
       END DO
    END IF

    RETURN
  END FUNCTION fac

  !--------------------------------------------------------------------!
  ! Subroutine switch_col
  !
  !     Switch two colums in a matrix (two dimensionnal array)
  !
  ! Input Variable:
  !     -A      : a two dimensionnal array
  !     -c1,c2  : two integers
  !
  ! Output Variable:
  !     -A : a two dimensionnal array
  !--------------------------------------------------------------------!
  SUBROUTINE switch_col(A,c1,c2)

    REAL(KIND=DP),DIMENSION(:,:),INTENT(inout) :: A
    INTEGER(KIND=IP),INTENT(in) :: c1,c2
    REAL(KIND=DP),DIMENSION(:),ALLOCATABLE :: col

    ALLOCATE(col(1:SIZE(A,1)))
    col=A(:,c1)
    A(:,c1)=A(:,c2)
    A(:,c2)=col
    DEALLOCATE(col)

    RETURN
  END SUBROUTINE switch_col

  !--------------------------------------------------------------------!
  ! Subroutine switch_row
  !
  !     Switch two rows in a matrix (two dimensionnal array)
  !
  ! Input Variable:
  !     -A      : a two dimensionnal array
  !     -r1,r2  : two integers
  !
  ! Output Variable:
  !     -A : a two dimensionnal array
  !--------------------------------------------------------------------!
  SUBROUTINE switch_row(A,r1,r2)

    REAL(KIND=DP),DIMENSION(:,:),INTENT(inout) :: A
    INTEGER(KIND=IP),INTENT(in) :: r1,r2
    REAL(KIND=DP),DIMENSION(:),ALLOCATABLE :: row

    ALLOCATE(row(1:SIZE(A,2)))
    row=A(r1,:)
    A(r1,:)=A(r2,:)
    A(r2,:)=row
    DEALLOCATE(row)

    RETURN
  END SUBROUTINE switch_row

  !--------------------------------------------------------------------!
  ! Subroutine gauss
  !
  !     Alter a matrix using gaussian elimination
  !
  ! Input Variable:
  !     -M  : a two dimensionnal array
  !
  ! Output Variable:
  !     -A  : a two dimensionnal array
  !--------------------------------------------------------------------!
  SUBROUTINE gauss(A,A1,B,B1)

    REAL(KIND=DP),DIMENSION(:,:),POINTER :: A
    REAL(KIND=DP),DIMENSION(:,:),POINTER,OPTIONAL :: B
    INTEGER(KIND=IP) :: nbr,nbc,i,j
    LOGICAL :: exit
    REAL(KIND=DP) :: c
    REAL(KIND=DP),DIMENSION(:,:),POINTER :: A1
    REAL(KIND=DP),DIMENSION(:,:),POINTER,OPTIONAL :: B1

    nbr=SIZE(A,1)

    nbc=SIZE(A,2)

    ALLOCATE(A1(1:nbr,1:nbc))
    A1=A

    IF (PRESENT(B)) THEN
       ALLOCATE(B1(1:SIZE(B,1),1:SIZE(B,2)))
       B1=B
    END IF
    j=nbc
    DO i=1,nbc                                  !for each column of A
       IF (ALL(A1(:,i)==0) .AND. i<j) THEN      !if a column contains only zeros
          CALL switch_col(A1,i,j)               !then put it on the right of A
          j=j-1
       END IF
    END DO


    DO i=1,min(nbr,nbc)
       exit=.FALSE.
       IF (A1(i,i)==0) THEN
          j=i+1
          DO WHILE (j<=nbr .AND. .NOT.(exit))   !we search on the i-th colum an not null element
             IF (A1(j,i)==0) THEN
                j=j+1
             ELSE
                CALL switch_row(A1,i,j)
                exit=.TRUE.
                IF (PRESENT(B)) THEN
                   CALL switch_row(B1,i,j)
                END IF
             END IF
          END DO
          !at the end of this loop :
          !A1(i,i)/=0 (beacause we used switch_row) & j<=nbr & exit=.TRUE.
          !OR
          !j=nbr+1 & exit=.FALSE.
          IF (j>nbr) THEN       !it means the i-th column is full of zero from i-th row to the ultime row
             j=i+1
             DO WHILE (j<=nbc .AND. .NOT.(exit))    !then we search on the i-th row
                IF (A1(i,j)==0) THEN
                   j=j+1
                ELSE
                   CALL switch_col(A1,i,j)
                   exit=.TRUE.
                END IF
             END DO
             !at the end of this loop :
             !A1(i,j)/=0 (beacause we used switch_col) & j<=nbC & exit=.TRUE.
             !OR
             !j=nbc+1 & exit=.FALSE.
             IF (j>nbc) THEN        !it means the i-th row is full of zero
                CALL switch_row(A1,i,nbr)
                IF (PRESENT(B)) THEN
                   CALL switch_row(B1,i,nbr)    !note : A and B have the same number of rows
                END IF
             END IF
          END IF
       END IF
       DO j=i+1,nbr
          c=A1(j,i)/A1(i,i)
          A1(j,:)=A1(j,:)-c*A1(i,:)
          IF (PRESENT(B)) THEN
             B1(j,:)=B1(j,:)-c*B1(i,:)
          END IF
       END DO
    END DO

    RETURN
  END SUBROUTINE gauss

  !--------------------------------------------------------------------!
  ! Function rank
  !
  !     Give the rank of a matrix (two dimensionnal array)
  !
  ! Input Variable:
  !     -A      : a two dimensionnal array
  !
  ! Output Variable:
  !     -r : an integer
  !--------------------------------------------------------------------!
  FUNCTION rank(A) RESULT(r)

    REAL(KIND=DP),DIMENSION(:,:),POINTER :: A
    INTEGER(KIND=IP) :: r,i,j
    REAL(KIND=DP),DIMENSION(:,:),POINTER :: B

    r=SIZE(A,1)
    CALL gauss(A,B)
    DO i=1,SIZE(B,1)
       DO j=1,SIZE(B,2)
          IF (ABS(B(i,j))<=1.0D-12) THEN
             B(i,j)=0.0_dp
          END IF
       END DO
    END DO
    DO i=1,SIZE(B,1)
       IF (ALL(B(i,:)==0)) THEN
          r=r-1
       END IF
    END DO
    DEALLOCATE(B)

    RETURN
  END FUNCTION rank

  !--------------------------------------------------------------------!
  ! Function det
  !
  !     Calculate the determinant of a matrix (two dimensionnal array)
  !
  ! Input Variable:
  !     -A      : a two dimensionnal array
  !
  ! Output Variable:
  !     -d : a real
  !--------------------------------------------------------------------!
  FUNCTION det(A) RESULT(d)

    REAL(KIND=DP), DIMENSION(:,:),POINTER :: A
    REAL(KIND=DP) :: d
    REAL(KIND=DP), DIMENSION(:,:),POINTER :: M
    INTEGER(KIND=IP) :: i
    
    d=0.0_dp
    
    IF (SIZE(A,1)/=SIZE(A,2))THEN
       WRITE(*,*) 'ERROR : the input matrix is not square'
    ELSE
       CALL gauss(A,M)
       d=1.0_dp
       DO i=1,SIZE(M,1)
          d=d*M(i,i)
       END DO
       DEALLOCATE(M)
    END IF


    RETURN
  END FUNCTION det

  !--------------------------------------------------------------------!
  ! Function interp_lin
  !
  !     Get two list of real (X,Y) and compute the value yi corresponding to xi
  !     usinf a linear interpolation
  !
  ! Input Variable:
  !     -X      : list of x-axis values
  !     -Y      : list of y-axis values
  !     -xi     : x-axis value of researching point
  !
  ! Output Variable:
  !     -yi     : y-axis value of researching point
  !--------------------------------------------------------------------!
  FUNCTION interp_lin(X,Y,xi) RESULT(yi)

    REAL(KIND=DP), DIMENSION(:),INTENT(in) :: X,Y
    REAL(KIND=DP),INTENT(in) :: xi
    REAL(KIND=DP) :: yi,t,a,b
    INTEGER(KIND=IP) :: sz,i,j
    REAL(KIND=DP), DIMENSION(:),ALLOCATABLE :: Xs,Ys,YY
    LOGICAL :: found,compute
    
    yi=0.0_dp
    
    sz=SIZE(X)
    IF (SIZE(Y)/=sz) THEN
       WRITE(*,*) 'ERROR : This subroutine needs two input array of the same size'
       WRITE(*,*) 'The program is stopped'
       STOP
    ELSE
       CALL sort_real(X,Xs)
       IF (xi<Xs(1) .OR. xi>Xs(sz)) THEN
          WRITE(*,*)'CAUTION : xi=',xi,'is out of range for the linear interpolation'
       ELSE IF (ANY(X/=Xs)) THEN !it means X was not sorted
          ALLOCATE(Ys(1:sz)) !so we replace y-axis values in right order
          DO i=1,sz
             t=Xs(i)
             j=1
             found=.FALSE.
             DO WHILE (.NOT.(found)) !X and Xs contain the same values, so a condition on j (dealing whith an exceed of Xs-range) is unecessary
                IF (X(j)==t) THEN
                   Ys(i)=Y(j)
                   found=.TRUE.
                ELSE
                   j=j+1
                END IF
             END DO
          END DO
       END IF
       ALLOCATE(YY(1:sz))
       IF (ALLOCATED(Ys)) THEN
          YY=Ys
          DEALLOCATE(Ys)
       ELSE
          YY=Y
       END IF
       i=1
       compute=.FALSE.
       DO WHILE (i<=sz-1 .AND. .NOT.(compute))
          IF (xi>=Xs(i) .AND. xi<Xs(i+1)) THEN !if xi is equal to a bound, the "upper" break is used
             a=(YY(i)-YY(i+1))/(Xs(i)-Xs(i+1))
             b=YY(i) - a*Xs(i)
             yi=a*xi+b
             compute=.TRUE.
          ELSE
             i=i+1
          END IF
       END DO
       DEALLOCATE(Xs)
       DEALLOCATE(YY)
    END IF

    RETURN
  END FUNCTION interp_lin

  !--------------------------------------------------------------------!
  ! Subroutine polyval
  !
  !     Return the value of a polynomial of degree n evaluated at x. The input argument P is a
  !     vector of length n+1 whose elements are the coefficients in descending powers of the
  !     polynomial to be evaluated.
  !
  ! Input Variable:
  !     -P  : an one dimensionnal array whose elements are the polynomial coefficients
  !     -x  : the real value where P will be evaluated
  !
  ! Output Variable:
  !     -Px : a real ( Px=P(x) )
  !
  ! Edit 06/04/17 : Inversion de l'ordre du polynome
  !--------------------------------------------------------------------!
  SUBROUTINE polyval(P,x,Px)

    REAL(KIND=DP),DIMENSION(:),INTENT(in) :: P
    REAL(KIND=DP),DIMENSION(:),INTENT(in) :: x
    REAL(KIND=DP),DIMENSION(:),INTENT(out) :: Px
    INTEGER(KIND=IP) :: n,i

    n=SIZE(P)
    Px(:)=0.0_dp
    DO i=1,n
        Px(:) = Px(:) + P(i)*x(:)**(i-1)
    END DO

    RETURN
  END SUBROUTINE polyval

  !--------------------------------------------------------------------!
  ! Subroutine polyint
  !
  !     For a n order polynom, the input argument P is a vector of length n+1 whose elements are the
  !     coefficients in descending powers of the polynomial to be evaluated.
  !     The output vector intP is a n+2 vector whose elments are coefficients of the integrete polynom
  !
  ! Input Variable:
  !     -P  : an one dimensionnal array whose elements are the polynomial coefficients
  !
  ! Output Variable:
  !     -intP : an one dimensionnal array whose elements are the integral polynomial coefficients
  !--------------------------------------------------------------------!
  SUBROUTINE polyint(P,intP)

    REAL(KIND=DP),DIMENSION(:),INTENT(in) :: P ! Dimension n
    REAL(KIND=DP),DIMENSION(:),INTENT(out) :: intP ! Dimension n+1
    INTEGER(KIND=IP) :: n,i

    n=SIZE(P)
    intP(:)=0.0_dp
    DO i=1,n
        intP(i+1) = P(i) / REAL(i,DP)
    END DO

    RETURN
  END SUBROUTINE polyint

  !--------------------------------------------------------------------!
  ! Subroutine realtostr
  !
  !     Convert a real into a string character.
  !
  ! Input Variable:
  !     -x  : a real
  !
  ! Output Variable:
  !     -xstring : a string character
  !--------------------------------------------------------------------!
  CHARACTER(8) FUNCTION realtostr(x)

    REAL(KIND=DP),INTENT(in) :: x
    !CHARACTER(50),INTENT(out) :: xstring

   WRITE( realtostr, '(F8.2)' )  x

   RETURN
 END FUNCTION realtostr

 !---------------------------------------------------------------------!
 !  Subroutine inttostr
 !
 !      Convert an integer into a string character.
 !
 !  Input Variable:
 !      -i  : an interger
 !
 !  Output Variable:
 !      -istring : a string character
 !---------------------------------------------------------------------!
 CHARACTER(8) FUNCTION inttostr(i)

   INTEGER(KIND=IP),INTENT(in) :: i
   !CHARACTER(50),INTENT(out) :: istring

   WRITE( inttostr, '(i8)' )  i

   RETURN
 END FUNCTION inttostr

 !---------------------------------------------------------------------!
 !  Subroutine QR_DECOMPOSITION
 !
 !      Use the Gram-Shmidt process to decompose the
 !      matrix A into the product QR, where Q is an
 !      orthogonal matrix and R is an upper triangular matrix
 !
 !  Input Variable:
 !      -A  : input matrix
 !
 !  Output Variable:
 !      -Q    : Q output matrix
 !      -R    : R output matrix
 !      -ierr : an error code
 !---------------------------------------------------------------------!
SUBROUTINE QR_DECOMPOSITION(A,Q,R,ierr)
    !
    !=== arguments ===
    REAL(KIND=DP) , DIMENSION(:,:),INTENT(in)  :: A     !size (m*n)
    REAL(KIND=DP) , DIMENSION(:,:),INTENT(out) :: Q,R
    INTEGER(KIND=IP) , INTENT(inout), OPTIONAL :: ierr
    !
    !=== local variables ===
    INTEGER(KIND=IP) :: Nmin,Nmax   !number of columns
    INTEGER(KIND=IP) :: Mmin,Mmax   !number of lines
    INTEGER(KIND=IP) :: n,nn,m,k,kk
    !
    !=== body ===
    !
    IF (PRESENT(ierr)) ierr = 0
    nn = 0
    !
    Mmin = LBOUND(A,DIM=1)
    Mmax = UBOUND(A,DIM=1)
    Nmin = LBOUND(A,DIM=2)
    Nmax = UBOUND(A,DIM=2)
    !
    IF ( Mmax-Mmin .LT. Nmax-Nmin ) THEN
        WRITE(*,*) "Mmin=",Mmin," Mmax=",Mmax," Nmin=",Nmin," Nmax=",Nmax
        STOP "Error : QR decomposition fails due to the size of the matrix"
    ENDIF
    !
    ! compute Q
    Q = 0.0_dp
!    WRITE(*,*) " compute Q",Mmin,Mmax,Nmin,Nmax
    !
    Q(:,Nmin) = A(:,Nmin) / normL2(A(:,Nmin))
    !
    DO k=Nmin+1,Nmax
        !
        Q(:,k) = A(:,k)
        !
        DO kk = Nmin, k-1   ! substract of the linearly dependant part
            !
            Q(:,k)  = Q(:,k) - DOT_PRODUCT(A(:,k),Q(:,kk))*Q(:,kk)
            !
        ENDDO
        !
        Q(:,k) = Q(:,k) / normL2(Q(:,k))
        !
    ENDDO
    !
    ! compute R
    R = 0.0_dp
!    WRITE(*,*) " compute R"
    !
    DO n = Nmin,Nmax
        !
        DO m = Mmin,MIN(n,Mmax)
            !
            R(m,n) = DOT_PRODUCT(A(:,n),Q(:,m))
            IF (ABS(R(m,n)) .LT. 2.0_dp*tinym) R(m,n) = eps_min
            !
        ENDDO
        !
    ENDDO
    !
!DO k=LBOUND(R,DIM=1),UBOUND(R,DIM=1)
!write(*,*) R(k,:)
!ENDDO
!STOP
    Q= -Q
    R= -R
    !
END SUBROUTINE QR_DECOMPOSITION

 !---------------------------------------------------------------------!
 !  Function normL2
 !
 !      Compute the L2 norm of a vector
 !
 !  Input Variable:
 !      -norm  : input vector
 !
 !  Output Variable:
 !      -u    : real output value
 !---------------------------------------------------------------------!
 FUNCTION normL2(u) RESULT (norm)
    !
    !=== arguments ===
    REAL(KIND=DP), DIMENSION(:), INTENT(in) :: u
    !
    !=== local variables ===
    REAL(KIND=DP) :: norm
    !
    norm = NORM2(u)
    !
    RETURN
 END FUNCTION normL2

 !---------------------------------------------------------------------!
 !  Function INV_MAT2
 !
 !      Compute the inverse of a 2x2 matrix
 !
 !  Input Variable:
 !      -u  : input matrix
 !
 !  Output Variable:
 !      -v    : inversed matrix
 !---------------------------------------------------------------------!
 FUNCTION INV_MAT2(u) RESULT (v)
    !
    !=== arguments ===
    REAL(KIND=DP), DIMENSION(:,:), INTENT(in) :: u
    !
    !=== local variables ===
    REAL(KIND=DP), DIMENSION(2,2) :: v
    REAL(KIND=DP) :: det
    !
    det = (u(1,1)*u(2,2)-u(1,2)*u(2,1))
    IF (ABS(det) < 10*eps_min) WRITE(*,*) 'ERROR : the mtrix determinant is null' 
    v(1,1) = (1/det)*u(2,2)
    v(2,1) = -(1/det)*u(2,1)
    v(1,2) = -(1/det)*u(1,2)
    v(2,2) = (1/det)*u(1,1)
    !
 END FUNCTION INV_MAT2

 !---------------------------------------------------------------------!
 !  Function INV_MAT3
 !
 !      Compute the inverse of a 3x3 matrix
 !
 !  Input Variable:
 !      -u  : input matrix
 !
 !  Output Variable:
 !      -v    : inversed matrix
 !---------------------------------------------------------------------!
 FUNCTION INV_MAT3(u) RESULT (v)
    !
    !=== arguments ===
    REAL(KIND=DP), DIMENSION(:,:), INTENT(in) :: u
    !
    !=== local variables ===
    REAL(KIND=DP), DIMENSION(3,3) :: v
    REAL(KIND=DP) :: detinv
    !
    detinv =  u(1,1)*u(2,2)*u(3,3) - u(1,1)*u(2,3)*u(3,2)&
            - u(1,2)*u(2,1)*u(3,3) + u(1,2)*u(2,3)*u(3,1)&
            + u(1,3)*u(2,1)*u(3,2) - u(1,3)*u(2,2)*u(3,1)

    IF (ABS(detinv) < 10*eps_min) WRITE(*,*) 'ERROR : the mtrix determinant is null'
    !
    detinv = 1.0/detinv
    !
    v(1,1) = +detinv * (u(2,2)*u(3,3) - u(2,3)*u(3,2))
    v(2,1) = -detinv * (u(2,1)*u(3,3) - u(2,3)*u(3,1))
    v(3,1) = +detinv * (u(2,1)*u(3,2) - u(2,2)*u(3,1))
    v(1,2) = -detinv * (u(1,2)*u(3,3) - u(1,3)*u(3,2))
    v(2,2) = +detinv * (u(1,1)*u(3,3) - u(1,3)*u(3,1))
    v(3,2) = -detinv * (u(1,1)*u(3,2) - u(1,2)*u(3,1))
    v(1,3) = +detinv * (u(1,2)*u(2,3) - u(1,3)*u(2,2))
    v(2,3) = -detinv * (u(1,1)*u(2,3) - u(1,3)*u(2,1))
    v(3,3) = +detinv * (u(1,1)*u(2,2) - u(1,2)*u(2,1))
    !
 END FUNCTION INV_MAT3

 !---------------------------------------------------------------------!
 !  Subroutine INV_R_MATRIX
 !
 !      Compute the inverse of the matrix R,
 !      where R is a upper triangular matrix
 !      of dimension 2*2 or 3*3
 !
 !  Input Variable:
 !      -R  : input matrix
 !
 !  Output Variable:
 !      -R    : R inverted output matrix
 !---------------------------------------------------------------------!
 SUBROUTINE INV_R_MATRIX(R)
    !
    !=== Arguments ===
    !
    REAL(KIND=DP), DIMENSION(:,:),INTENT(inout) :: R
    !
    !=== Local variables ===
    !
    INTEGER(KIND=IP) :: Nmin,Nmax,i,j,k,kk
    REAL(KIND=DP)    :: tmpr
    REAL(KIND=DP), DIMENSION(:,:), ALLOCATABLE :: tmp
    !
    !=== body ===
    !
    Nmin = LBOUND(R,DIM=1)
    Nmax = UBOUND(R,DIM=1)
    !
    IF (LBOUND(R,DIM=2) .NE. Nmin) STOP "Error : R is not a square matrix"
    IF (UBOUND(R,DIM=2) .NE. Nmax) STOP "Error : R is not a square matrix"
    !
    !check if the algorithm can handle the matrix R
    DO i=Nmin,Nmax
        IF(ABS(R(i,i)) .LT. 2.0_dp*tinym) STOP "Error : the matrix R is singular"
    ENDDO
    !
    ALLOCATE(tmp(1:Nmax-Nmin+1,1:Nmax-Nmin+1)); tmp = R
    !
    R = 0.0_dp
    !
    IF ( (Nmax-Nmin+1) .EQ. 3) THEN
        !
        R(Nmin  ,Nmin  ) = 1.0_dp / tmp(1,1)
        R(Nmin  ,Nmin+1) = - tmp(1,2) / (tmp(1,1)* tmp(2,2))
        R(Nmin  ,Nmin+2) = -(tmp(1,3)*tmp(2,2)-tmp(2,3)*tmp(1,2))/(tmp(1,1)*tmp(2,2)*tmp(3,3))
        R(Nmin+1,Nmin+1) = 1.0_dp/tmp(2,2)
        R(Nmin+1,Nmin+2) = -tmp(2,3)/(tmp(2,2)*tmp(3,3))
        R(Nmin+2,Nmin+2) = 1.0_dp/tmp(3,3)
        !
    ELSEIF ((Nmax-Nmin+1) .EQ. 2) THEN
        !
        R(Nmin  ,Nmin  ) = 1.0_dp/tmp(1,1)
        R(Nmin  ,Nmin+1) = -tmp(1,2)/(tmp(1,1)*tmp(2,2))
        R(Nmin+1,Nmin+1) = 1.0_dp/tmp(2,2)
        !
    ELSEIF ((Nmax-Nmin+1) .EQ. 4) THEN
        !
!========ancienne version
!        R(Nmin  ,Nmin  ) = 1.0_rp / tmp(1,1)
!        R(Nmin  ,Nmin+1) = - tmp(1,2) / (tmp(1,1)* tmp(2,2))
!        R(Nmin  ,Nmin+2) = -(tmp(1,3)*tmp(2,2)-tmp(2,3)*tmp(1,2))/(tmp(1,1)*tmp(2,2)*tmp(3,3))
!        R(Nmin  ,Nmin+3) = (1.0_rp/tmp(4,4))* ( - tmp(1,4)/tmp(1,1) &
!        &                                       + (tmp(2,4)*tmp(1,2))/(tmp(1,1)*tmp(2,2)) &
!        &                                       + (tmp(3,4)*tmp(1,3))/(tmp(3,3)*tmp(1,1)) &
!        &                                       - (tmp(1,2)*tmp(2,3)*tmp(3,4))/(tmp(1,1)*tmp(2,2)*tmp(3,3)) )
!        R(Nmin+1,Nmin+1) = 1.0_rp/tmp(2,2)
!        R(Nmin+1,Nmin+2) = -tmp(2,3)/(tmp(2,2)*tmp(3,3))
!        R(Nmin+1,Nmin+3) = -tmp(2,4)/(tmp(2,2)*tmp(4,4)) + tmp(3,4)*tmp(2,3)/(tmp(2,2)*tmp(3,3)*tmp(4,4))
!        R(Nmin+2,Nmin+2) = 1.0_rp/tmp(3,3)
!        R(Nmin+2,Nmin+3) = -tmp(3,4)/(tmp(3,3)*tmp(4,4))
!        R(Nmin+3,Nmin+3) = 1.0_rp/tmp(4,4)
!========
        !
        R(Nmin  ,Nmin  ) = 1.0_dp / tmp(1,1)
        R(Nmin+1,Nmin+1) = 1.0_dp / tmp(2,2)
        R(Nmin+2,Nmin+2) = 1.0_dp / tmp(3,3)
        R(Nmin+3,Nmin+3) = 1.0_dp / tmp(4,4)
        !
        R(Nmin+2,Nmin+3) = - R(Nmin+2,Nmin+2)*tmp(3,4)/tmp(4,4)
        R(Nmin+1,Nmin+2) = - R(Nmin+1,Nmin+1)*tmp(2,3)/tmp(3,3)
        R(Nmin+1,Nmin+3) = - (R(Nmin+1,Nmin+1)*tmp(2,4)+R(Nmin+1,Nmin+2)*tmp(3,4))/tmp(4,4)
        R(Nmin+0,Nmin+1) = - R(Nmin+0,Nmin+0)*tmp(1,2)/tmp(2,2)
        R(Nmin+0,Nmin+2) = - (R(Nmin+0,Nmin+0)*tmp(1,3)+R(Nmin+0,Nmin+1)*tmp(2,3))/tmp(3,3)
        R(Nmin+0,Nmin+3) = - (R(Nmin+0,Nmin+0)*tmp(1,4)+R(Nmin+0,Nmin+1)*tmp(2,4)+R(Nmin+0,Nmin+2)*tmp(3,4))/tmp(4,4)
        !
    ELSE
        STOP "Error : dimension of R > 4*4 is not supported."
    ENDIF
    !
    !=== verif
    tmp = MATMUL(R,tmp) ! tmp = Identity ??
    !
    DO j = Nmin, (Nmax-Nmin+1)
    DO i = Nmin, (Nmax-Nmin+1)
        IF (i == j) THEN !diagonal
            tmpr = (tmp(i,i) - 1.0_dp)/(1.0_dp)
        ELSE
            tmpr = tmp(i,j)
        ENDIF
        IF (ABS(tmpr) > 1.0E-10_dp) THEN
             WRITE(*,*) "Fail to inverse matrix R"
             WRITE(*,*) " (i,j)=(",i,",",j,")"
             WRITE(*,*) "ABS(tmpr)=",ABS(tmpr)
             WRITE(*,*) "R="
             DO k=Nmin,Nmax
                WRITE(*,*) (R(k,kk),kk=Nmin,(Nmax-Nmin+1))
             ENDDO
             WRITE(*,*) "R*R-1="
             DO k=Nmin,Nmax
                WRITE(*,*) (tmp(k,kk),kk=Nmin,(Nmax-Nmin+1))
             ENDDO
             STOP
        ENDIF
    ENDDO
    ENDDO
    !=== end verif
    !
    IF (ALLOCATED(tmp)) DEALLOCATE(tmp)
    !
END SUBROUTINE INV_R_MATRIX
!----------------------------------------------------------------------!
!
!---------------------------------------------------------------------!
!  Function ANGLE_VECT
!
!      Compute the angle between 2 vectors
!
!  Input Variable:
!      -u,v  : input vectors
!
!  Output Variable:
!      -alpha    : angle in radian
!---------------------------------------------------------------------!
 FUNCTION angle_vect(u,v) RESULT (alpha)
    !
    !=== arguments ===
    REAL(KIND=DP), DIMENSION(3), INTENT(in) :: u, v
    REAL(KIND=DP) :: alpha
    !
    !=== local variables ===
    REAL(KIND=DP) :: PS
    REAL(KIND=DP), DIMENSION(3) :: V1, V2
    !
    V1 = u/NORM(u)
    V2 = v/NORM(v)
    PS = DOT_PRODUCT(V1,V2)
    IF (PS > 1.0) THEN
        PS = 1.0
    ELSE IF (PS < -1.0) THEN
        PS = -1.0
    END IF
    !
    alpha = ACOS(PS)
    RETURN
    !
 END FUNCTION angle_vect

  !--------------------------------------------------------------------!
  ! Function det3D
  !
  !     Calculate the determinant of a 3x3 matrix 
  !
  ! Input Variable:
  !     -A      : a two dimensionnal array
  !
  ! Output Variable:
  !     -d : a real
  !--------------------------------------------------------------------!
  FUNCTION det3D(A) RESULT(d)

    REAL(KIND=DP), DIMENSION(3,3) :: A
    REAL(KIND=DP) :: d
    
    d=0.0_dp
    
    d =   A(1,1)*A(2,2)*A(3,3)  &
          - A(1,1)*A(2,3)*A(3,2)  &
          - A(1,2)*A(2,1)*A(3,3)  &
          + A(1,2)*A(2,3)*A(3,1)  &
          + A(1,3)*A(2,1)*A(3,2)  &
          - A(1,3)*A(2,2)*A(3,1)

    RETURN
  END FUNCTION det3D


!---- Fin du module ---------------------------------------------------!
END MODULE mod_algebra