!> Module regroupant les operateurs de deformation de maillage
MODULE mod_deform
!
USE mod_algebra, ONLY : cross, norm, dot
USE mod_cst, ONLY : IP, DP, eps_min
USE mod_print, ONLY : print_err
USE mod_ablation, ONLY : cyc_ablation
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Opérateur Laplacien utilisé pour le déplacement des sommets internes du maillage
SUBROUTINE deform_laplacien ( DOM, SOM_ADJ )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
REAL(DP), DIMENSION (3) :: pos_som
INTEGER(IP), DIMENSION(:,:) :: SOM_ADJ
REAL(DP), ALLOCATABLE, DIMENSION(:) :: CONV_DEF ! Tableau utilisé pour evaluer la convergence de la deformation : pour chaque sommet ||dep_som||aire min||aire face adj1||aire face adj 2||...||
REAL(DP) :: dep_som, taux_def, tol_def
INTEGER(IP) :: nsom_int, isom_int, isom_adj, nsom_adj, iiter, niter, ncol
!
!---- Récupérations ---------------------------------------------------!
nsom_int = size( SOM_ADJ( :,1 ) )
ncol = size( SOM_ADJ( 1,: ) )
niter = DOM%NUM%niter_def
tol_def = DOM%NUM%tol_def

! Boucle sur les iterations de deformation
DO iiter = 1, niter 
    taux_def = 0.0_dp
    ! Boucle sur les sommets interieurs
    DO isom_int = 1, nsom_int
        ALLOCATE( CONV_DEF( ncol ) )
        ! Recuperation du nombre de sommets adjacents du sommet courant
        nsom_adj = SOM_ADJ( isom_int,2 )
        pos_som = 0.0_dp
        
        ! Boucle sur les sommets adjacents au sommet courant
        DO isom_adj = 1, nsom_adj
            pos_som = pos_som + DOM%MLG%SOMMET( SOM_ADJ( isom_int,2+isom_adj ),: )
            CONV_DEF( isom_adj+2 ) = norm( DOM%MLG%SOMMET( SOM_ADJ( isom_int,1 ),: ) - DOM%MLG%SOMMET( SOM_ADJ( isom_int,2+isom_adj ),: ) )
        END DO
        ! determination de l'aire minimale des faces adjacentes stockée dans la colonne 2 de CONV_DEF
        CONV_DEF( 2 ) = minval ( CONV_DEF( 3:ncol ), mask = CONV_DEF( 3:ncol ) > 100.0_dp*eps_min )
        ! normalisation par le nombre de facteurs du barycentre
        pos_som = pos_som / REAL(nsom_adj,DP)
        ! mesure de l'ecart entre les 2 positions
        dep_som = norm( DOM%MLG%SOMMET( SOM_ADJ( isom_int,1 ),: ) - pos_som( : ) )
        IF( CONV_DEF( 2 ) > eps_min ) THEN
            CONV_DEF( 1 ) = ABS( dep_som / CONV_DEF( 2 ) )
        ELSE
            CONV_DEF( 1 ) = 1.0_dp
            CALL print_err( "ablation err1 (deform) : deux sommets du maillage sont confondus",1 )
        END IF
        ! deplacement du sommet
        DOM%MLG%SOMMET( SOM_ADJ( isom_int,1 ),: ) = pos_som( : )   
        taux_def = max( taux_def,CONV_DEF( 1 ) )
        DEALLOCATE ( CONV_DEF )
    END DO
    ! Evaluation de la convergence de la deformation de maillage
    IF( taux_def < tol_def ) THEN
        EXIT
    END IF
    
END DO

! Enregistrement des donnes de resolution de l'operateur de deformation de maillage
DOM%RSL%niter_AB = max( min( iiter,niter ),DOM%RSL%niter_AB )
DOM%RSL%err_it_AB = max( taux_def,DOM%RSL%err_it_AB )

END SUBROUTINE deform_laplacien
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine de mise à jour de l'interpolation par spline de la limite courante
SUBROUTINE deform_MAJ_spline ( DOM, ilim )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: TANGENTE
REAL(DP) :: n_P2P1, n_P1P3, x, y, yp, abs_ps, ord_ps
INTEGER(IP) :: nsom, isom_int, nsom_int, ilim, ifac_lim, nfac_limi
INTEGER(IP) :: id_som, id_som_prec, ifac
INTEGER(IP) :: idepart, iarrivee, ips, i, j, pas_ech, id_fac
REAL(DP), DIMENSION(3) :: P1, P2, P3, T1, T2, P2P1, v, vect_to_norm, T_to_cross

!---- Récupérations ---------------------------------------------------!
nsom_int = size( DOM%CDT%LIM(ilim)%SOM_ADJ_LIMi( :,1 ) )
nsom = DOM%MLG%nsom
pas_ech = DOM%NUM%pas_ech
n_P2P1 = 0.0_dp
n_P1P3 = 0.0_dp
nfac_limi = DOM%CDT%LIM( ilim )%nfac_limi

! Allocation des tableaux
ALLOCATE ( TANGENTE( nsom,5 ) )

! RAZ des tableaux
TANGENTE = 0.0_dp
DOM%CDT%LIM( ilim )%PSi = 0.0_dp
DOM%CDT%LIM( ilim )%SPLINEi( :,: ) = 0.0_dp
DOM%CDT%LIM( ilim )%IDSPLINEi( :,: ) = 0
DOM%CDT%LIM( ilim )%PS2FACLIMi( :,: ) = 0
DOM%CDT%LIM( ilim )%PP2PSi( :,: ) = 0

! Calcul de la tangente en chaque point interieur de la limite.
DO isom_int = 1, nsom_int
    P1 = DOM%MLG%SOMMET( DOM%CDT%LIM(ilim)%SOM_ADJ_LIMi( isom_int,3 ),: )
    P3 = DOM%MLG%SOMMET( DOM%CDT%LIM(ilim)%SOM_ADJ_LIMi( isom_int,4 ),: )
    vect_to_norm = P3-P1
    n_P1P3 = norm( vect_to_norm )
    IF ( n_P1P3 < 100.0_dp*eps_min ) THEN
        CALL print_err( "ablation err2 (deform) : 2 points du maillage sont confondus ; interpolation par spline impossible",1 )
    END IF
    TANGENTE( DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( isom_int,1 ),1 ) = 1.0_dp
    TANGENTE( DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( isom_int,1 ),2:4 ) = ( P3 - P1 ) / n_P1P3
    ! Enregistrement du sommet vers lequel pointe le vecteur directeur de la tangente
    TANGENTE( DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( isom_int,1 ),5 ) = DOM%CDT%LIM( ilim )%SOM_ADJ_LIMi( isom_int,4 )
END DO

! Calcul des coefficients du polynome interpolateur local pour chaque face de la limite
    ! Interpolation dans le repere local du deuxieme sommet de la face courante centre sur ce sommet et dirige par sa tangente et le vecteur normal dirigé vers le centre de courbure (si il existe ie hors cas de la droite ) 
    ! P(x) = ax + bx ; a col3, b col4, du tableau SPLINE (la courbe passe par le point courant donc ordonnee a l'origine nulle et la tangente est nulle à l'origine par construction donc coef de x = 0)

IF( nfac_limi == 0 ) THEN ! cas de la limite vide
    CALL print_err ( "ablation err3 (deform) : impossible d'interpoler par une spline ; l'une des limites ne contient aucune face",1 )

ELSE IF ( nfac_limi == 1 ) THEN ! cas de la limite monoface
    id_som = DOM%MLG%FAC( DOM%CDT%LIM( ilim )%ID_FACLIMi( 1 ),4 )
    id_som_prec =  DOM%MLG%FAC( DOM%CDT%LIM( ilim )%ID_FACLIMi( 1 ),3 )
    P1 = DOM%MLG%SOMMET( id_som_prec,: )
    P2 = DOM%MLG%SOMMET( id_som,: )
    P2P1 = P1-P2
    n_P2P1 = norm( P2P1 )
    T2 = P2P1 / n_P2P1
    ! MAJ du tableau SPLINEi (interpolation par une droite) 
    DOM%CDT%LIM( ilim )%IDSPLINEi( 1,1 ) = id_som
    DOM%CDT%LIM( ilim )%IDSPLINEi( 1,2 ) = id_som_prec
    DOM%CDT%LIM( ilim )%IDSPLINEi( 1,3 ) = 0
    DOM%CDT%LIM( ilim )%SPLINEi( 1,1 ) = -n_P2P1
    DOM%CDT%LIM( ilim )%SPLINEi( 1,2 ) = 0.0_dp
    DOM%CDT%LIM( ilim )%SPLINEi( 1,3 ) = 0.0_dp
    DOM%CDT%LIM( ilim )%SPLINEi( 1,4 ) = 0.0_dp
    DOM%CDT%LIM( ilim )%SPLINEi( 1,5:7 ) = T2          
        
ELSE ! cas standard
    DO ifac_lim = 1, nfac_limi
        ! Determination du sommet courant et de son predecesseur
        id_som = DOM%MLG%FAC( DOM%CDT%LIM( ilim )%ID_FACLIMi( ifac_lim ),4 )
        id_som_prec =  DOM%MLG%FAC( DOM%CDT%LIM( ilim )%ID_FACLIMi( ifac_lim ),3 )
        P1 = DOM%MLG%SOMMET( id_som_prec,: )
        P2 = DOM%MLG%SOMMET( id_som,: )
        P2P1 = P1-P2
        n_P2P1 = norm( P2P1 )
        
        ! Affectation des tangentes   
            ! Cas des tangentes aux sommets coins du domaine
        IF ( ( id_som == DOM%CDT%LIM( ilim )%SOM_COINi( 1 ) ) .OR. ( id_som == DOM%CDT%LIM( ilim )%SOM_COINi( 2 ) )  ) THEN ! Cas ou id_som est un sommet coin
            IF ( ( n_P2P1 < 100.0_dp*eps_min ) .OR. ( TANGENTE ( id_som_prec,1 ) < 0.5_dp ) ) THEN ! erreur les deux sommets sont confondues ou le point interne n'a pas de tangente definie
                CALL print_err ( "ablation err4 (deform) : impossible de determiner la tangente a l'un des coins du domaines car 2 sommets sont confondus" )
            ELSE 
                T1 = TANGENTE ( id_som_prec,2:4 )
                T2 = - P2P1 / n_P2P1 ! la tangente au coin est dirigee par l'arete courante du sommet precedent vers le sommet courant
            END IF
        ELSE IF ( ( id_som_prec == DOM%CDT%LIM( ilim )%SOM_COINi( 1 ) ) .OR. ( id_som_prec == DOM%CDT%LIM( ilim )%SOM_COINi( 2 ) ) ) THEN ! idem pour le cas ou id_som_prec est un sommet coin
            IF ( ( n_P2P1 < 100.0_dp*eps_min ) .OR. ( TANGENTE ( id_som,1 ) < 0.5_dp ) ) THEN
                CALL print_err ( "ablation err5 (deform) : impossible de determiner la tangente a l'un des coins du domaines car 2 sommets sont confondus" )
            ELSE 
                T2 = TANGENTE ( id_som,2:4 )
                T1 = - P2P1 / n_P2P1
            END IF
            
        ELSE  ! cas d'une face interieure a la limite            
            IF ( TANGENTE ( id_som_prec,1 ) < 0.5_dp ) THEN
                CALL print_err ( "ablation err6 (deform) : impossible de determiner la tangente de l'un des sommets de la frontiere." )
            ELSE 
                T1 = TANGENTE ( id_som_prec,2:4 )
            END IF
            IF ( TANGENTE (id_som,1) < 0.5_dp ) THEN
                CALL print_err ( "ablation err7 (deform) : impossible de determiner la tangente de l'un des sommets de la frontiere." )
            ELSE 
                T2 = TANGENTE ( id_som,2:4 )
            END IF            
        END IF
        
!~         ! Inversion du vecteur directeur de T2 si celui-ci pointe vers le sommet precedent
        IF ( id_som_prec == TANGENTE(id_som,5) ) THEN
        T2 = - T2
        END IF
        
        ! Calcul de l'abcisse et de l'ordonnée du predecesseur dans le repere local
        x = dot( P2P1,T2 )
        vect_to_norm = P2P1 - x*T2
        y = norm ( vect_to_norm )
        
            ! Recuperation de la valeur de la dérivée de la courbe au sommet predecesseur a l'aide de la tangente
        
        IF ( y < 100.0_dp*eps_min ) THEN ! ordonnée du prédecesseur nulle dans le repère local
            vect_to_norm = cross( T1,T2 )
            IF ( norm( vect_to_norm ) < 100.0_dp*eps_min ) THEN! cas de l'interpolation par une droite (les deux tangentes et la face sont alignées)
                yp = 0.0_dp
                DOM%CDT%LIM( ilim )%IDSPLINEi( ifac_lim,1 ) = id_som
                DOM%CDT%LIM( ilim )%IDSPLINEi( ifac_lim,2 ) = id_som_prec
                DOM%CDT%LIM( ilim )%IDSPLINEi( ifac_lim,3 ) = 0
                DOM%CDT%LIM( ilim )%SPLINEi( ifac_lim,1 ) = x
                DOM%CDT%LIM( ilim )%SPLINEi( ifac_lim,2 ) = 0.0_dp
                DOM%CDT%LIM( ilim )%SPLINEi( ifac_lim,3 ) = 0.0_dp
                DOM%CDT%LIM( ilim )%SPLINEi( ifac_lim,4 ) = 0.0_dp
                DOM%CDT%LIM( ilim )%SPLINEi( ifac_lim,5:7 ) = T2
            ELSE ! la tangente au sommet predecesseur n'est pas colinéaire à celle du sommet de référence, on utilise cette tangente pour définir le deuxième axe du repère local.
                T_to_cross = cross( T1,T2 )
                v = cross ( T2,T_to_cross )
                v = v / norm( v )
                ! determination du coefficient directeur de T1 dans le repere local (T2,v)
                yp = dot( T1,v ) / dot( T1,T2 )
                ! Calcul des coefficients a et b (syst lineaires 2 eq deux inc)
                ! | x x | | a | = | 0 |
                ! | 2x 3x| | b |   | yp|
                ! MAJ du tableau SPLINEi
                DOM%CDT%LIM( ilim )%IDSPLINEi( ifac_lim,1 ) = id_som
                DOM%CDT%LIM( ilim )%IDSPLINEi( ifac_lim,2 ) = id_som_prec
                DOM%CDT%LIM( ilim )%IDSPLINEi( ifac_lim,3 ) = 1
                DOM%CDT%LIM( ilim )%SPLINEi( ifac_lim,1 ) = x
                DOM%CDT%LIM( ilim )%SPLINEi( ifac_lim,2 ) = 0.0_dp
                DOM%CDT%LIM( ilim )%SPLINEi( ifac_lim,3 ) = -x*yp / x**2
                DOM%CDT%LIM( ilim )%SPLINEi( ifac_lim,4 ) =  x*yp / x**3
                DOM%CDT%LIM( ilim )%SPLINEi( ifac_lim,5:7 ) = T2
                DOM%CDT%LIM( ilim )%SPLINEi( ifac_lim,8:10 ) = v
            END IF
            
        ELSE IF ( dot( T1,T2 ) < 100.0_dp*eps_min ) THEN ! Cas des deux tangentes T1 et T2 perpendiculaires, cas dégénéré ou l'interpolation par spline n'est pas exacte (dérivée infinie)
        CALL print_err("ablation (deform) : l'interpolation par spline est localement dégradee car deux tangentes consecutives sont perpendiculaires")
            ! Calcul du deuxieme vecteur directeur
            v = ( P2P1 - x*T2 ) / y
            
            IF ( ABS( x ) > 100.0_dp*eps_min ) THEN
                DOM%CDT%LIM( ilim )%IDSPLINEi( ifac_lim,1 ) = id_som
                DOM%CDT%LIM( ilim )%IDSPLINEi( ifac_lim,2 ) = id_som_prec
                DOM%CDT%LIM( ilim )%IDSPLINEi( ifac_lim,3 ) = 2
                DOM%CDT%LIM( ilim )%SPLINEi( ifac_lim,1 ) = x
                DOM%CDT%LIM( ilim )%SPLINEi( ifac_lim,2 ) = y
                DOM%CDT%LIM( ilim )%SPLINEi( ifac_lim,3 ) = 0.0_dp
                ! Approximation de l'interpolation par une fonction x |-> x passant par les deux sommets
                DOM%CDT%LIM( ilim )%SPLINEi( ifac_lim,4 ) = y / ( x**3 )
                DOM%CDT%LIM( ilim )%SPLINEi( ifac_lim,5:7 ) = T2
                DOM%CDT%LIM( ilim )%SPLINEi( ifac_lim,8:10 ) = v 
            ELSE
                CALL print_err ("ablation err8 (deform) : impossible d'interpoler par une spline : 3 points sont alignés mais non consecutifs (rebroussement)",1)
            END IF
            
        ELSE ! Cas standard
            ! Calcul du deuxieme vecteur directeur
            v = ( P2P1 - x*T2 ) / y
            ! determination du coefficient directeur de T1 dans le repere local (T2,v)
            yp = dot( T1,v ) / dot( T1,T2 )
            
            ! Calcul des coefficients a et b (syst lineaires 2 eq deux inc)
            ! | x x | | a | = | y |
            ! | 2x 3x | | b |   | yp|
            ! MAJ du tableau SPLINEi
            IF ( ABS( x ) > 100.0_dp*eps_min ) THEN
                DOM%CDT%LIM( ilim )%IDSPLINEi( ifac_lim,1 ) = id_som
                DOM%CDT%LIM( ilim )%IDSPLINEi( ifac_lim,2 ) = id_som_prec
                DOM%CDT%LIM( ilim )%IDSPLINEi( ifac_lim,3 ) = 1
                DOM%CDT%LIM( ilim )%SPLINEi( ifac_lim,1 ) = x
                DOM%CDT%LIM( ilim )%SPLINEi( ifac_lim,2 ) = y
                DOM%CDT%LIM( ilim )%SPLINEi( ifac_lim,3 ) = ( 3.0_dp*y - x* yp ) / x**2
                DOM%CDT%LIM( ilim )%SPLINEi( ifac_lim,4 ) = ( x*yp - 2.0_dp*y ) / x**3
                DOM%CDT%LIM( ilim )%SPLINEi( ifac_lim,5:7 ) = T2
                DOM%CDT%LIM( ilim )%SPLINEi( ifac_lim,8:10 ) = v            
            ELSE ! erreur matrice non inversible 
                CALL print_err ("ablation err9 (deform) : impossible d'interpoler par une spline : 3 points sont alignés mais non consecutifs (rebroussement)",1)
            END IF    
        END IF     
    END DO
END IF

! Contrôle de l'interpolation du chacun des points de la limite
IF (count ( DOM%CDT%LIM(ilim)%IDSPLINEi(:,1) == 0 ) > 0 ) THEN
    CALL print_err( "ablation err 10 (deform) : des points d'une des limites ne sont pas interpoles par une spline",1 )
END IF

DEALLOCATE ( TANGENTE )

! Constructuction du tableau des points secondaires de la limite courante PSi et IDPSi
    
idepart = 0
iarrivee = 0

! Recherche du "premier" sommet de la limite courante
IF ( DOM%CDT%LIM( ilim )%SOM_COINi( 1 ) == 0 ) THEN ! cas de la limite fermee
    idepart = DOM%MLG%FAC( DOM%CDT%LIM( ilim )%ID_FACLIMi( 1 ),3 ) ! on choisit le premier sommet de la premiere face
    iarrivee = idepart
ELSE 
    idepart = DOM%CDT%LIM( ilim )%SOM_COINi( 1 )
    iarrivee = DOM%CDT%LIM( ilim )%SOM_COINi( 2 )
END IF

! Controle de la determination du premier sommet
IF ( idepart == 0 ) THEN
    CALL print_err( "ablation err11 (deform) : impossible de determiner le sommet de depart d'une des limites du domaine",1 )
END IF

! initialisation du sommet de depart
id_som = idepart

! Initialisation de la face de depart
ifac = 0
    DO ifac_lim = 1, nfac_limi
        IF ( ( DOM%MLG%FAC( DOM%CDT%LIM( ilim )%ID_FACLIMi( ifac_lim ),3 ) == id_som ) .OR. ( DOM%MLG%FAC( DOM%CDT%LIM( ilim )%ID_FACLIMi( ifac_lim ),4 ) == id_som ) ) THEN 
        ifac = ifac_lim
        EXIT
        END IF
    END DO

! Inscription du premier point secondaire copie du sommet de depart et des relations associees
DOM%CDT%LIM( ilim )%PSi( 1,1:3 ) = DOM%MLG%SOMMET( id_som,: )
DOM%CDT%LIM( ilim )%PP2PSi( 1,1 ) = id_som
DOM%CDT%LIM( ilim )%PP2PSi( 1,2 ) = 1
DOM%CDT%LIM( ilim )%PP2PSi( 1,3 ) = 2

! Initialisation des compteurs de points secondaires (i) et de points principaux (j)
i = 1
j = 2
ifac = 0
! Boucle sur les sommets de la limite (points principaux)
DO WHILE ( ( id_som /= iarrivee ) .OR. ( i == 1 ) )

! Passage a la face suivante
    DO ifac_lim = 1, nfac_limi
        IF ( (( DOM%MLG%FAC( DOM%CDT%LIM( ilim )%ID_FACLIMi( ifac_lim ),3 ) == id_som ) .OR. ( DOM%MLG%FAC( DOM%CDT%LIM( ilim )%ID_FACLIMi( ifac_lim ),4 ) == id_som )) .AND. ( ifac_lim /= ifac ) ) THEN 
        ifac = ifac_lim
        EXIT
        END IF
    END DO
    id_fac = DOM%CDT%LIM( ilim )%ID_FACLIMi( ifac )
    ! Controle de la determination de la face correspondante
    IF ( ifac_lim > nfac_limi+1) THEN
          CALL print_err( "ablation err12 (deform): impossible de faire le lien entre un sommet et une face d'une des limites",1 )
    END IF
      
    
    ! MAJ du tableau PS2FACLIMi pour le point principal precedent
    DOM%CDT%LIM( ilim )%PS2FACLIMi( i,1 ) = DOM%CDT%LIM( ilim )%PS2FACLIMi( i,1 ) + 1
    DOM%CDT%LIM( ilim )%PS2FACLIMi( i,DOM%CDT%LIM( ilim )%PS2FACLIMi( i,1 ) +1 ) = ifac
    
    ! Incrementation
    i = i+1
    
     ! Creation des points secondaires de la face limite courante
    DO ips = 1, pas_ech - 1
        DOM%CDT%LIM( ilim )%PS2FACLIMi( i,1 ) = 1
        DOM%CDT%LIM( ilim )%PS2FACLIMi( i,2 ) = ifac
        ! Calcul des coordonnees du point secondaire sur la spline de la face courante.
        ! determination de l'abcisse et de l'ordonnee du point secondaire dans le repere local de la spline
        IF (id_som == DOM%MLG%FAC( id_fac,3 ) ) THEN ! cas ou le sommet courant est a "gauche" de la face        
            abs_ps = ( REAL( pas_ech,DP ) - REAL( ips,DP ) ) / REAL( pas_ech,DP ) * DOM%CDT%LIM( ilim )%SPLINEi( ifac,1 ) ! l'abcisse est subdivisée en pas_ech sous-parties
        ELSE IF (id_som == DOM%MLG%FAC( id_fac,4 ) ) THEN ! cas ou le sommet courant est a "droite" de la face
            abs_ps = ( REAL( ips,DP ) ) / REAL( pas_ech,DP ) * DOM%CDT%LIM( ilim )%SPLINEi( ifac,1 )
        ELSE 
            CALL print_err( "ablation err13 (deform) : erreur de correspondace face/sommets lors de la creation des points secondaires",1 )
        END IF
        ord_ps = DOM%CDT%LIM( ilim )%SPLINEi( ifac,3 ) * abs_ps**2 + DOM%CDT%LIM( ilim )%SPLINEi( ifac,4 ) * abs_ps**3 ! y = ax + bx
            ! Passage dans le repere absolu
        DOM%CDT%LIM( ilim )%PSi( i,1:3 ) = DOM%MLG%SOMMET(DOM%CDT%LIM( ilim )%IDSPLINEi( ifac,1 ),:) + DOM%CDT%LIM( ilim )%SPLINEi( ifac,5:7 ) * abs_ps + DOM%CDT%LIM( ilim )%SPLINEi( ifac,8:10 ) * ord_ps
        ! Incrementation
        i = i+1     
    END DO    
     
    ! Passage au point principal suivant    
    IF ( DOM%MLG%FAC(id_fac,3) == id_som ) THEN
        id_som = DOM%MLG%FAC(id_fac,4)
    ELSE
        id_som = DOM%MLG%FAC(id_fac,3)
    END IF   

    ! Inscription de la copie du point principal courant dans PSi et des relations associees
    DOM%CDT%LIM( ilim )%PSi( i,1:3 ) = DOM%MLG%SOMMET( id_som,: )
    DOM%CDT%LIM( ilim )%PS2FACLIMi( i,1 ) = 1
    DOM%CDT%LIM( ilim )%PS2FACLIMi( i,2 ) = ifac
    DOM%CDT%LIM( ilim )%PP2PSi( j,1 ) = id_som
    DOM%CDT%LIM( ilim )%PP2PSi( j,2 ) = i-1
    DOM%CDT%LIM( ilim )%PP2PSi( j,3 ) = i

    ! Verification pour eviter une boucle infinie
    IF ( i > pas_ech*nfac_limi+1 ) THEN
        CALL print_err( "ablation err14 (deform) : impossible de trouver le dernier sommet d'une des limites" )
    END IF
    ! Incrementation
    j = j+1

END DO

END SUBROUTINE deform_MAJ_spline
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Routine utilisée pour redistribuer les sommets de la limite courante
!> suivant la courbe définie par les points secondaires générés
!> par deform_MAJ_spline
SUBROUTINE deform_spline ( DOM, ilim )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: ilim, ipt, npt, id_som, i
INTEGER(IP) :: idps_g, idps_d, idps_prec, idps_suiv
INTEGER(IP) :: niter, idps, id_som_prec, id_som_suiv
REAL(DP) :: dist_g, dist_d, delta, seg_g, seg_d, conv, convmax
REAL(DP), DIMENSION (3) :: PP, PS, diff_pos

!---- Récuérations ---------------------------------------------------!
npt = SIZE( DOM%CDT%LIM( ilim )%PP2PSi( :,1 ) )
niter = DOM%NUM%niter_def

! Boucle sur les points principaux hors coins
DO i = 1, niter
    ! RAZ de l'indicateur de convergence
    convmax = 0.0_dp
    conv = 0.0_dp
    DO ipt = 2, npt - 1        
        ! RAZ des distances
        dist_g = 0.0_dp
        dist_d = 0.0_dp
        seg_g = 0.0_dp
        seg_d = 0.0_dp
        delta = 0.0_dp    
    
        ! Determination des indices des points secondaires relatifs au point principal courant
        id_som = DOM%CDT%LIM( ilim )%PP2PSi( ipt,1 )
        id_som_prec = DOM%CDT%LIM( ilim )%PP2PSi( ipt-1,1 )
        id_som_suiv = DOM%CDT%LIM( ilim )%PP2PSi( ipt+1,1 )   
        idps_g = DOM%CDT%LIM( ilim )%PP2PSi( ipt-1,3 )
        idps_d = DOM%CDT%LIM( ilim )%PP2PSi( ipt+1,2 )
        idps_prec = DOM%CDT%LIM( ilim )%PP2PSi( ipt,2 )
        idps_suiv = DOM%CDT%LIM( ilim )%PP2PSi( ipt,3 ) 
          
        ! Calcul de la distance avec le point principal precedent
        ! premier segment
        diff_pos = DOM%MLG%SOMMET( id_som,: ) - DOM%CDT%LIM( ilim )%PSi( idps_prec,: )
        seg_g = norm ( diff_pos )
        dist_g = seg_g
        
        ! segments intermediaires
        DO idps = idps_prec, idps_g+1, -1
            diff_pos = DOM%CDT%LIM( ilim )%PSi( idps,: ) - DOM%CDT%LIM( ilim )%PSi( idps-1,: )
            dist_g = dist_g + norm( diff_pos )
        END DO
        
        ! dernier segment
        diff_pos = DOM%MLG%SOMMET( id_som_prec,: ) - DOM%CDT%LIM( ilim )%PSi( idps_g,: )
        dist_g = dist_g + norm ( diff_pos )
        
        ! Calcul de la distance avec le point principal suivant
        diff_pos = DOM%MLG%SOMMET( id_som,: ) - DOM%CDT%LIM( ilim )%PSi( idps_suiv,: )
        seg_d = norm ( diff_pos )
        dist_d = seg_d
        DO idps = idps_suiv, idps_d-1
            diff_pos = DOM%CDT%LIM( ilim )%PSi( idps+1,: ) - DOM%CDT%LIM( ilim )%PSi( idps,: )
            dist_d = dist_d + norm( diff_pos )
        END DO
        diff_pos = DOM%MLG%SOMMET( id_som_suiv,: ) - DOM%CDT%LIM( ilim )%PSi( idps_d,: )
        dist_d = dist_d + norm ( diff_pos )    
        
        delta = dist_d - dist_g 
                   
        ! MAJ de l'indicateur de convergence
        conv = ABS( 0.5_dp*delta )/ max( 100.0_dp*eps_min, min( ABS( dist_g ), ABS( dist_d ) ) )
        IF ( conv > convmax ) THEN
            convmax = conv
        END IF
        
        ! Deplacement de connectivite avec les points secondaires si besoin et MAJ de la position du point courant     
        IF ( delta > seg_d ) THEN ! le point est trop a gauche, le decalage est superieur a la valeur du premier segment de droite : decalage d'un cran vers la droite 
            DOM%CDT%LIM( ilim )%PP2PSi( ipt,2 ) = idps_suiv
            DOM%CDT%LIM( ilim )%PP2PSi( ipt,3 ) = idps_suiv + 1
            DOM%MLG%SOMMET( id_som,: ) = DOM%CDT%LIM( ilim )%PSi( idps_suiv,: )
            
        ELSE IF (delta < - seg_g) THEN ! le point est trop a droite, le decalage est superieur a la valeur du premier segment de gauche : decalage d'un cran vers la gauche
            DOM%CDT%LIM( ilim )%PP2PSi( ipt,2 ) = idps_prec -1
            DOM%CDT%LIM( ilim )%PP2PSi( ipt,3 ) = idps_prec
            DOM%MLG%SOMMET( id_som,: ) = DOM%CDT%LIM( ilim )%PSi( idps_prec,: )
            
        ELSE IF (delta > 0.0_dp) THEN ! le point est trop a droite mais il est situe correctement par rapport aux points secondaires : ajustement de position 
            PP = DOM%MLG%SOMMET( id_som,: )
            PS = DOM%CDT%LIM( ilim )%PSi( idps_suiv,: )
            diff_pos = PS - PP
            DOM%MLG%SOMMET( id_som,: ) = PP + 0.5_dp * delta * ( PS - PP ) / norm ( diff_pos )
            
        ELSE IF (delta < 0.0_dp) THEN ! le point est trop a gauche mais il est situe correctement par rapport aux points secondaires : ajustement de position 
            PP = DOM%MLG%SOMMET( id_som,: )
            PS = DOM%CDT%LIM( ilim )%PSi( idps_prec,: )
            diff_pos = PP - PS
            DOM%MLG%SOMMET( id_som,: ) = PP + 0.5_dp * delta * ( PP - PS ) / norm ( diff_pos )
        END IF
    END DO

    ! Controle de la convergence
    IF (convmax < DOM%NUM%tol_def) THEN
        EXIT
    END IF
    
    ! MAJ des donnees d'iterations
    IF ( i > DOM%RSL%niter_AB ) THEN
        DOM%RSL%niter_AB = i
    END IF
END DO

END SUBROUTINE deform_spline
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_deform
