!> Module regroupant les operateurs de deformation de maillage
MODULE mod_deform3d
!
USE mod_algebra, ONLY : cross, norm, dot, INV_MAT3, angle_vect, det3D
USE mod_cst, ONLY : IP, DP, eps_min, coef_bin, Pi, WEIGHT_CTR, amorti_eq_abl_C, amorti_eq_abl_S, amorti_eq_abl_V
USE mod_print, ONLY : print_err
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Calcul de la fonction de Bernstein et de sa dérivée 1ere et 2nd
SUBROUTINE bernstein_f ( t , bernst , bernst_d1 , bernst_d2 )
!
!---- Déclarations ----------------------------------------------------!
INTEGER(IP) :: i, ii, n, bin
REAL(DP) :: t
REAL(DP), DIMENSION(7) :: bernst, bernst_d1, bernst_d2
!
n = 6 ! On utilise les courbes de Bezier avec 7 points de controles donc n=6
bernst = 0.0_dp
bernst_d1 = 0.0_dp
bernst_d2 = 0.0_dp

DO ii = 1, n+1
    i = ii - 1
    bin = coef_bin(ii)

    bernst(ii) = bin*( (1-t)**(n-i) )*( t**i )
    IF ( i == 0 ) THEN
        bernst_d1(ii) = bin*( -(n-i) * (1-t)**(n-i-1) * (t**i) )
        bernst_d2(ii) = bin*( (n-i) * (n-i-1) * (1-t)**(n-i-2) * (t**i) )

    ELSE IF ( i == n ) THEN
        bernst_d1(ii) = bin*( i * (1-t)**(n-i) * (t**(i-1) ) )
        bernst_d2(ii) = bin*( i * (i-1) * (1-t)**(n-i) * (t**(i-2) ) )        

    ELSE 
        bernst_d1(ii) = bin*( i * (1-t)**(n-i) * (t**(i-1) ) - (n-i) * (1-t)**(n-i-1) * (t**i) )
        IF ( i == 1 ) THEN
            bernst_d2(ii) = bin*( (n-i) * (n-i-1) * (1-t)**(n-i-2) * (t**i) - 2 * (n-i) * i * (1-t)**(n-i-1) * (t**(i-1) ) )
        ELSE IF ( i == n-1 ) THEN
            bernst_d2(ii) = bin*( i * (i-1) * (1-t)**(n-i) * (t**(i-2) ) - 2 * (n-i) * i * (1-t)**(n-i-1) * (t**(i-1) ) )
        ELSE
            bernst_d2(ii) = bin*( (n-i) * (n-i-1) * (1-t)**(n-i-2) * (t**i) + i * (i-1) * (1-t)**(n-i) * (t**(i-2) ) - 2 * (n-i) * i * (1-t)**(n-i-1) * (t**(i-1) ) )
        END IF

    END IF

END DO
!
END SUBROUTINE bernstein_f
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Calcul du polynome rationel de Bezier, de sa dérivée 1ere et 2nd
SUBROUTINE rational_bezier ( pts , t , rbez , rbez_d , rbez_d2 )
!
!---- Déclarations ----------------------------------------------------!
REAL(DP) :: t, den, num1, num2, num3, dend, num1d, num2d, num3d
REAL(DP) :: num1d2, num2d2, num3d2, dend2
REAL(DP), DIMENSION(7) :: bernst, bernst_d1, bernst_d2, PTS_TEMP, PROD
REAL(DP), DIMENSION(7,3) :: pts
REAL(DP), DIMENSION(3) :: rbez, rbez_d, rbez_d2
!
! On calcule les fonctions de Bernstein pour la valeur de t
CALL bernstein_f ( t, bernst, bernst_d1, bernst_d2)
!
PROD = WEIGHT_CTR*bernst
PTS_TEMP = pts(:,1)
num1 = DOT_PRODUCT(PROD,PTS_TEMP)
PTS_TEMP = pts(:,2)
num2 = DOT_PRODUCT(PROD,PTS_TEMP)
PTS_TEMP = pts(:,3)
num3 = DOT_PRODUCT(PROD,PTS_TEMP)
den = DOT_PRODUCT(WEIGHT_CTR,bernst)

PROD = WEIGHT_CTR*bernst_d1
PTS_TEMP = pts(:,1)
num1d = DOT_PRODUCT(PROD,PTS_TEMP)
PTS_TEMP = pts(:,2)
num2d = DOT_PRODUCT(PROD,PTS_TEMP)
PTS_TEMP = pts(:,3)
num3d = DOT_PRODUCT(PROD,PTS_TEMP)
dend = DOT_PRODUCT(WEIGHT_CTR,bernst_d1)

PROD = WEIGHT_CTR*bernst_d2
PTS_TEMP = pts(:,1)
num1d2 = DOT_PRODUCT(PROD,PTS_TEMP)
PTS_TEMP = pts(:,2)
num2d2 = DOT_PRODUCT(PROD,PTS_TEMP)
PTS_TEMP = pts(:,3)
num3d2 = DOT_PRODUCT(PROD,PTS_TEMP)
dend2 = DOT_PRODUCT(WEIGHT_CTR,bernst_d2)

rbez = (/ num1/den , num2/den , num3/den /)
rbez_d = (/ ( num1d - dend*num1/den ) / den , ( num2d - dend*num2/den ) / den , ( num3d - dend*num3/den ) / den /)
rbez_d2 = (/ ( num1d2 - dend2*num1/den - dend*( (num1d-dend*num1/den) / den ) - dend*num1d + num1/den*dend**2 )/den , &
            & ( num2d2 - dend2*num2/den - dend*( (num2d-dend*num2/den) / den ) - dend*num2d + num2/den*dend**2 )/den , &
            & ( num3d2 - dend2*num3/den - dend*( (num3d-dend*num3/den) / den ) - dend*num3d + num3/den*dend**2 )/den   /)
!
END SUBROUTINE rational_bezier
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Calcul du polynome rationel de Bezier
SUBROUTINE rational_bezier_pol ( pts, t, rbez )
!
!---- Déclarations ----------------------------------------------------!
REAL(DP) :: t, den, num1, num2, num3
REAL(DP), DIMENSION(7) :: bernst, bernst_d1, bernst_d2, PTS_TEMP, PROD
REAL(DP), DIMENSION(7,3) :: pts
REAL(DP), DIMENSION(3) :: rbez
!
! On calcule les fonctions de Bernstein pour la valeur de t
CALL bernstein_f ( t, bernst, bernst_d1, bernst_d2)
!
PROD = WEIGHT_CTR*bernst
PTS_TEMP = pts(:,1)
num1 = DOT_PRODUCT(PROD,PTS_TEMP)
PTS_TEMP = pts(:,2)
num2 = DOT_PRODUCT(PROD,PTS_TEMP)
PTS_TEMP = pts(:,3)
num3 = DOT_PRODUCT(PROD,PTS_TEMP)
den = DOT_PRODUCT(WEIGHT_CTR,bernst)
!
rbez = (/ num1/den , num2/den , num3/den /)
!
END SUBROUTINE rational_bezier_pol
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Calcul des informations aux sommets pour les patch de Bezier (triangle)
SUBROUTINE update_bezier_face_tri ( DOM, iface )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: iface, iarete, ii, nbez
REAL(DP), DIMENSION(3) :: VECT, rbez1_0, rbez1_1, rbez2_0, rbez2_1, rbez3_0, rbez3_1
REAL(DP), DIMENSION(3) :: et_0, et_1, bt_0, bt_1, nt_0, nt_1, P02, P01, Wi
REAL(DP), DIMENSION(3,3) :: MATRIX_R
REAL(DP), DIMENSION(7,3) :: PTS_CTR, PTS_CTR0
REAL(DP) :: VAL, DET_R, PS
!
nbez = 6
!
DO iarete = 1, 3
    PTS_CTR = DOM%MLG%ARET_LIM( DOM%MLG%FAC_LIM(iface)%ARETES(iarete) )%PTS_CTR
    IF ( DOM%MLG%FAC_LIM(iface)%SENS_ARET(iarete) == 0 ) THEN
        ! Si les sommets ne sont pas dans le meme ordre entre la face et l'arete, on inverse l'ordre des points de controles
        DO ii = 1, 7
            PTS_CTR0(ii,:) = PTS_CTR( 8-ii, : )
        END DO
        PTS_CTR = PTS_CTR0
    END IF

    rbez1_0 = (/ nbez * WEIGHT_CTR(2) / WEIGHT_CTR(1) * ( PTS_CTR(2,1) - PTS_CTR(1,1) ) , &
                & nbez * WEIGHT_CTR(2) / WEIGHT_CTR(1) * ( PTS_CTR(2,2) - PTS_CTR(1,2) ) , &
                & nbez * WEIGHT_CTR(2) / WEIGHT_CTR(1) * ( PTS_CTR(2,3) - PTS_CTR(1,3) ) /)
    rbez1_1 = (/ nbez * WEIGHT_CTR(6) / WEIGHT_CTR(7) * ( PTS_CTR(7,1) - PTS_CTR(6,1) ) , &
            & nbez * WEIGHT_CTR(6) / WEIGHT_CTR(7) * ( PTS_CTR(7,2) - PTS_CTR(6,2) ) , &
            & nbez * WEIGHT_CTR(6) / WEIGHT_CTR(7) * ( PTS_CTR(7,3) - PTS_CTR(6,3) ) /)

    DOM%MLG%PATCH_LIM%RP( 2*(iarete-1)+1, : ) = rbez1_0
    DOM%MLG%PATCH_LIM%RP( 2*iarete, : ) = rbez1_1

    rbez2_0 = (/ nbez*(nbez-1)*WEIGHT_CTR(3)/WEIGHT_CTR(1)*(PTS_CTR(3,1)-PTS_CTR(1,1)) + 2*nbez*(WEIGHT_CTR(2)*(WEIGHT_CTR(1)-nbez*WEIGHT_CTR(2))/(WEIGHT_CTR(1)**2))*(PTS_CTR(2,1)-PTS_CTR(1,1)) , & 
                & nbez*(nbez-1)*WEIGHT_CTR(3)/WEIGHT_CTR(1)*(PTS_CTR(3,2)-PTS_CTR(1,2)) + 2*nbez*(WEIGHT_CTR(2)*(WEIGHT_CTR(1)-nbez*WEIGHT_CTR(2))/(WEIGHT_CTR(1)**2))*(PTS_CTR(2,2)-PTS_CTR(1,2)) , &
                & nbez*(nbez-1)*WEIGHT_CTR(3)/WEIGHT_CTR(1)*(PTS_CTR(3,3)-PTS_CTR(1,3)) + 2*nbez*(WEIGHT_CTR(2)*(WEIGHT_CTR(1)-nbez*WEIGHT_CTR(2))/(WEIGHT_CTR(1)**2))*(PTS_CTR(2,3)-PTS_CTR(1,3)) /)
    rbez2_1 = (/ nbez*(nbez-1)*WEIGHT_CTR(5)/WEIGHT_CTR(7)*(PTS_CTR(7,1)-PTS_CTR(5,1)) + 2*nbez*(WEIGHT_CTR(6)*(WEIGHT_CTR(7)-nbez*WEIGHT_CTR(6))/(WEIGHT_CTR(7)**2))*(PTS_CTR(7,1)-PTS_CTR(6,1)) , & 
                & nbez*(nbez-1)*WEIGHT_CTR(5)/WEIGHT_CTR(7)*(PTS_CTR(7,2)-PTS_CTR(5,2)) + 2*nbez*(WEIGHT_CTR(6)*(WEIGHT_CTR(7)-nbez*WEIGHT_CTR(6))/(WEIGHT_CTR(7)**2))*(PTS_CTR(7,2)-PTS_CTR(6,2)) , &
                & nbez*(nbez-1)*WEIGHT_CTR(5)/WEIGHT_CTR(7)*(PTS_CTR(7,3)-PTS_CTR(5,3)) + 2*nbez*(WEIGHT_CTR(6)*(WEIGHT_CTR(7)-nbez*WEIGHT_CTR(6))/(WEIGHT_CTR(7)**2))*(PTS_CTR(7,3)-PTS_CTR(6,3)) /)

    rbez3_0 = (/ nbez*(nbez-1)*(nbez-2)*WEIGHT_CTR(4)/WEIGHT_CTR(1)*(PTS_CTR(4,1)-PTS_CTR(1,1)) + 3*nbez*(nbez-1)*(WEIGHT_CTR(3)*(2*WEIGHT_CTR(1)-nbez*WEIGHT_CTR(2))/(WEIGHT_CTR(1)**2))*(PTS_CTR(3,1)-PTS_CTR(1,1)) + 3*nbez*(WEIGHT_CTR(2)/WEIGHT_CTR(1)**3)*( nbez**2*(2*WEIGHT_CTR(2)**2-WEIGHT_CTR(1)*WEIGHT_CTR(3)) + nbez*WEIGHT_CTR(1)*(WEIGHT_CTR(3)-4*WEIGHT_CTR(2)) + 2*WEIGHT_CTR(1)**2 )*(PTS_CTR(2,1)-PTS_CTR(1,1)) , & 
                & nbez*(nbez-1)*(nbez-2)*WEIGHT_CTR(4)/WEIGHT_CTR(1)*(PTS_CTR(4,2)-PTS_CTR(1,2)) + 3*nbez*(nbez-1)*(WEIGHT_CTR(3)*(2*WEIGHT_CTR(1)-nbez*WEIGHT_CTR(2))/(WEIGHT_CTR(1)**2))*(PTS_CTR(3,2)-PTS_CTR(1,2)) + 3*nbez*(WEIGHT_CTR(2)/WEIGHT_CTR(1)**3)*( nbez**2*(2*WEIGHT_CTR(2)**2-WEIGHT_CTR(1)*WEIGHT_CTR(3)) + nbez*WEIGHT_CTR(1)*(WEIGHT_CTR(3)-4*WEIGHT_CTR(2)) + 2*WEIGHT_CTR(1)**2 )*(PTS_CTR(2,2)-PTS_CTR(1,2)) , &
                & nbez*(nbez-1)*(nbez-2)*WEIGHT_CTR(4)/WEIGHT_CTR(1)*(PTS_CTR(4,3)-PTS_CTR(1,3)) + 3*nbez*(nbez-1)*(WEIGHT_CTR(3)*(2*WEIGHT_CTR(1)-nbez*WEIGHT_CTR(2))/(WEIGHT_CTR(1)**2))*(PTS_CTR(3,3)-PTS_CTR(1,3)) + 3*nbez*(WEIGHT_CTR(2)/WEIGHT_CTR(1)**3)*( nbez**2*(2*WEIGHT_CTR(2)**2-WEIGHT_CTR(1)*WEIGHT_CTR(3)) + nbez*WEIGHT_CTR(1)*(WEIGHT_CTR(3)-4*WEIGHT_CTR(2)) + 2*WEIGHT_CTR(1)**2 )*(PTS_CTR(2,3)-PTS_CTR(1,3)) /)
    rbez3_1 = (/ nbez*(nbez-1)*(nbez-2)*WEIGHT_CTR(4)/WEIGHT_CTR(7)*(PTS_CTR(7,1)-PTS_CTR(4,1)) + 3*nbez*(nbez-1)*(WEIGHT_CTR(5)*(2*WEIGHT_CTR(7)-nbez*WEIGHT_CTR(6))/(WEIGHT_CTR(7)**2))*(PTS_CTR(7,1)-PTS_CTR(5,1)) + 3*nbez*(WEIGHT_CTR(6)/WEIGHT_CTR(7)**3)*( nbez**2*(2*WEIGHT_CTR(6)**2-WEIGHT_CTR(7)*WEIGHT_CTR(5)) + nbez*WEIGHT_CTR(7)*(WEIGHT_CTR(5)-4*WEIGHT_CTR(6)) + 2*WEIGHT_CTR(7)**2 )*(PTS_CTR(7,1)-PTS_CTR(6,1)) , & 
                & nbez*(nbez-1)*(nbez-2)*WEIGHT_CTR(4)/WEIGHT_CTR(7)*(PTS_CTR(7,2)-PTS_CTR(4,2)) + 3*nbez*(nbez-1)*(WEIGHT_CTR(5)*(2*WEIGHT_CTR(7)-nbez*WEIGHT_CTR(6))/(WEIGHT_CTR(7)**2))*(PTS_CTR(7,2)-PTS_CTR(5,2)) + 3*nbez*(WEIGHT_CTR(6)/WEIGHT_CTR(7)**3)*( nbez**2*(2*WEIGHT_CTR(6)**2-WEIGHT_CTR(7)*WEIGHT_CTR(5)) + nbez*WEIGHT_CTR(7)*(WEIGHT_CTR(5)-4*WEIGHT_CTR(6)) + 2*WEIGHT_CTR(7)**2 )*(PTS_CTR(7,2)-PTS_CTR(6,2)) , &
                & nbez*(nbez-1)*(nbez-2)*WEIGHT_CTR(4)/WEIGHT_CTR(7)*(PTS_CTR(7,3)-PTS_CTR(4,3)) + 3*nbez*(nbez-1)*(WEIGHT_CTR(5)*(2*WEIGHT_CTR(7)-nbez*WEIGHT_CTR(6))/(WEIGHT_CTR(7)**2))*(PTS_CTR(7,3)-PTS_CTR(5,3)) + 3*nbez*(WEIGHT_CTR(6)/WEIGHT_CTR(7)**3)*( nbez**2*(2*WEIGHT_CTR(6)**2-WEIGHT_CTR(7)*WEIGHT_CTR(5)) + nbez*WEIGHT_CTR(7)*(WEIGHT_CTR(5)-4*WEIGHT_CTR(6)) + 2*WEIGHT_CTR(7)**2 )*(PTS_CTR(7,3)-PTS_CTR(6,3)) /)

    ! e = r'/NORM(r')
    et_0 = rbez1_0 / NORM( rbez1_0 )
    DOM%MLG%PATCH_LIM%ET( 2*(iarete-1)+1 , : ) = et_0
    et_1 = rbez1_1 / NORM( rbez1_1 )
    DOM%MLG%PATCH_LIM%ET( 2*iarete , : ) = et_1

    ! b = (r'^r'')/NORM(r'^r'')
    VECT = CROSS( rbez1_0 , rbez2_0 )
    IF ( NORM(VECT) > eps_min ) THEN
        bt_0 = VECT / NORM( VECT )
    ELSE 
        bt_0 = (/ 0.0, 0.0, 0.0 /)
    END IF
    DOM%MLG%PATCH_LIM%BT( 2*(iarete-1)+1 , : ) = bt_0
    VECT = CROSS( rbez1_1 , rbez2_1 )
    IF ( NORM(VECT) > eps_min ) THEN
        bt_1 = VECT / NORM( VECT )
    ELSE 
        bt_1 = (/ 0.0, 0.0, 0.0 /)
    END IF    
    DOM%MLG%PATCH_LIM%BT( 2*iarete , : ) = bt_1

    ! n = b^e
    nt_0 = CROSS( et_0 , bt_0 )
    DOM%MLG%PATCH_LIM%NT( 2*(iarete-1)+1 , : ) = nt_0
    nt_1 = CROSS( et_1 , bt_1 )
    DOM%MLG%PATCH_LIM%NT( 2*iarete , : ) = nt_1

    ! k = (r'^r'')/r'^3
    VECT = CROSS( rbez1_0 , rbez2_0 )
    VAL = NORM(VECT) / ( NORM(rbez1_0)**3 )
    DOM%MLG%PATCH_LIM%KT( 2*(iarete-1)+1 ) = VAL
    VECT = CROSS( rbez1_1 , rbez2_1 )
    VAL = NORM(VECT) / ( NORM(rbez1_1)**3 )
    DOM%MLG%PATCH_LIM%KT( 2*iarete ) = VAL   

    ! tau = det(r',r'',r''')/||r'^r''||^2
    MATRIX_R(1,:) = rbez1_0(:)
    MATRIX_R(2,:) = rbez2_0(:)
    MATRIX_R(3,:) = rbez3_0(:)
    DET_R = det3D( MATRIX_R )
    VECT = CROSS( rbez1_0 , rbez2_0 )
    VAL = NORM(VECT)**2
    IF ( VAL > eps_min ) THEN
        DOM%MLG%PATCH_LIM%TAUT( 2*(iarete-1)+1 ) = DET_R / VAL
    ELSE
        DOM%MLG%PATCH_LIM%TAUT( 2*(iarete-1)+1 ) = 0.0
    END IF
    MATRIX_R(1,:) = rbez1_1(:)
    MATRIX_R(2,:) = rbez2_1(:)
    MATRIX_R(3,:) = rbez3_1(:)
    DET_R = det3D( MATRIX_R )
    VECT = CROSS( rbez1_1 , rbez2_1 )
    VAL = NORM(VECT)**2
    IF ( VAL > eps_min ) THEN
        DOM%MLG%PATCH_LIM%TAUT(2*iarete) = DET_R / VAL
    ELSE
        DOM%MLG%PATCH_LIM%TAUT(2*iarete) = 0.0
    END IF    

    ! sigma_iL & sigma_iR
    P01(:) = PTS_CTR(2,:) - PTS_CTR(1,:) 
    P02(:) = PTS_CTR(3,:) - PTS_CTR(1,:)
    VECT = CROSS( P02 , P01 )
    VAL = NORM(VECT)
    PS = DOT_PRODUCT( nt_0 , DOM%MLG%SOM_LIM( DOM%MLG%FAC_LIM(iface)%SOMMETS( MODULO(iarete,3)+1) )%NORMALES( DOM%MLG%FAC_LIM(iface)%TAG_SOMMET( MODULO(iarete,3)+1) , : ) )
    IF (VAL < eps_min) THEN
        DOM%MLG%PATCH_LIM%SIGMA(iarete,1) = 0
    ELSEIF (PS > eps_min) THEN
        DOM%MLG%PATCH_LIM%SIGMA(iarete,1) = 1
    ELSE
        DOM%MLG%PATCH_LIM%SIGMA(iarete,1) = -1
    END IF
    P01(:) = PTS_CTR(7,:) - PTS_CTR(6,:) 
    P02(:) = PTS_CTR(7,:) - PTS_CTR(5,:)
    VECT = CROSS(P02,P01)
    VAL = NORM(VECT)
    PS = DOT_PRODUCT( nt_1 , DOM%MLG%SOM_LIM( DOM%MLG%FAC_LIM(iface)%SOMMETS( MODULO(iarete+1,3)+1) )%NORMALES( DOM%MLG%FAC_LIM(iface)%TAG_SOMMET( MODULO(iarete+1,3)+1) , : ) )
    IF ( VAL < eps_min ) THEN
        DOM%MLG%PATCH_LIM%SIGMA(iarete,2) = 0
    ELSEIF ( PS > eps_min ) THEN
        DOM%MLG%PATCH_LIM%SIGMA(iarete,2) = 1
    ELSE
        DOM%MLG%PATCH_LIM%SIGMA(iarete,2) = -1
    END IF    
END DO
!
! Angle entre les tangentes
!
et_0 = DOM%MLG%PATCH_LIM%ET(5,:)
et_1 = DOM%MLG%PATCH_LIM%ET(4,:)
VAL = angle_vect( et_1 , et_0 )
VECT = CROSS( et_1 , et_0 )
PS = DOT_PRODUCT( VECT , DOM%MLG%SOM_LIM( DOM%MLG%FAC_LIM(iface)%SOMMETS(1) )%NORMALES( DOM%MLG%FAC_LIM(iface)%TAG_SOMMET(1), : ) )
IF ( PS > eps_min ) THEN
    DOM%MLG%PATCH_LIM%ANGLES(1) = VAL
ELSE
    DOM%MLG%PATCH_LIM%ANGLES(1) = -VAL
END IF
et_0 = DOM%MLG%PATCH_LIM%ET(1,:)
et_1 = DOM%MLG%PATCH_LIM%ET(6,:)
VAL = angle_vect( et_1 , et_0 )
VECT = CROSS( et_1 , et_0 )
PS = DOT_PRODUCT( VECT , DOM%MLG%SOM_LIM( DOM%MLG%FAC_LIM(iface)%SOMMETS(2) )%NORMALES( DOM%MLG%FAC_LIM(iface)%TAG_SOMMET(2), : ) )
IF ( PS > eps_min ) THEN
    DOM%MLG%PATCH_LIM%ANGLES(2) = VAL
ELSE
    DOM%MLG%PATCH_LIM%ANGLES(2) = -VAL
END IF
et_0 = DOM%MLG%PATCH_LIM%ET(3,:)
et_1 = DOM%MLG%PATCH_LIM%ET(2,:)
VAL = angle_vect( et_1 , et_0 )
VECT = CROSS( et_1 , et_0 )
PS = DOT_PRODUCT( VECT , DOM%MLG%SOM_LIM( DOM%MLG%FAC_LIM(iface)%SOMMETS(3) )%NORMALES( DOM%MLG%FAC_LIM(iface)%TAG_SOMMET(3), : ) )
IF ( PS > eps_min ) THEN
    DOM%MLG%PATCH_LIM%ANGLES(3) = VAL
ELSE
    DOM%MLG%PATCH_LIM%ANGLES(3) = -VAL
END IF
!
! Twist vector Wi
!
DO iarete = 1, 3
    CALL twist_vector_tri ( DOM , iarete , iface , Wi )
    DOM%MLG%PATCH_LIM%TWIST( iarete,: ) = Wi(:)
END DO
!
END SUBROUTINE update_bezier_face_tri
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Calcul des informations aux sommets pour les patch de Bezier (rectangle)
SUBROUTINE update_bezier_face_quad ( DOM , iface )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: iface, iarete, ii, nbez
REAL(DP), DIMENSION(3) :: VECT, rbez1_0, rbez1_1, rbez2_0, rbez2_1, rbez3_0, rbez3_1
REAL(DP), DIMENSION(3) :: et_0, et_1, bt_0, bt_1, nt_0, nt_1, P02, P01, Wi
REAL(DP), DIMENSION(3,3) :: MATRIX_R
REAL(DP), DIMENSION(7,3) :: PTS_CTR, PTS_CTR0
REAL(DP) :: VAL, DET_R, PS
INTEGER(IP), DIMENSION(4,2) :: relation_ind ! tableau pour avoir le bon indice des sommets autour de chaque arete
!
nbez = 6
relation_ind(:,1) = (/ 1, 1, 4, 2 /)
relation_ind(:,2) = (/ 2, 4, 3, 3 /)
!
DO iarete = 1, 4
    PTS_CTR = DOM%MLG%ARET_LIM( DOM%MLG%FAC_LIM(iface)%ARETES(iarete) )%PTS_CTR
    IF ( DOM%MLG%FAC_LIM(iface)%SENS_ARET(iarete) == 0 ) THEN
        ! Si les sommets ne sont pas dans le meme ordre entre la face et l'arete, on inverse l'ordre des points de controles
        DO ii = 1, 7
            PTS_CTR0(ii,:) = PTS_CTR(8-ii,:)
        END DO
        PTS_CTR = PTS_CTR0
    END IF

    rbez1_0 = (/ nbez*WEIGHT_CTR(2) / WEIGHT_CTR(1)*( PTS_CTR(2,1)-PTS_CTR(1,1) ) , &
                & nbez*WEIGHT_CTR(2) / WEIGHT_CTR(1)*( PTS_CTR(2,2)-PTS_CTR(1,2) ) , &
                & nbez*WEIGHT_CTR(2) / WEIGHT_CTR(1)*( PTS_CTR(2,3)-PTS_CTR(1,3) ) /)
    rbez1_1 = (/ nbez*WEIGHT_CTR(6) / WEIGHT_CTR(7)*( PTS_CTR(7,1)-PTS_CTR(6,1) ) , &
            & nbez*WEIGHT_CTR(6) / WEIGHT_CTR(7)*( PTS_CTR(7,2)-PTS_CTR(6,2) ) , &
            & nbez*WEIGHT_CTR(6) / WEIGHT_CTR(7)*( PTS_CTR(7,3)-PTS_CTR(6,3) ) /)

    DOM%MLG%PATCH_LIM%RP( 2*(iarete-1)+1 , : ) = rbez1_0
    DOM%MLG%PATCH_LIM%RP( 2*iarete , : ) = rbez1_1

    rbez2_0 = (/ nbez*(nbez-1)*WEIGHT_CTR(3)/WEIGHT_CTR(1)*(PTS_CTR(3,1)-PTS_CTR(1,1)) + 2*nbez*(WEIGHT_CTR(2)*(WEIGHT_CTR(1)-nbez*WEIGHT_CTR(2))/(WEIGHT_CTR(1)**2))*(PTS_CTR(2,1)-PTS_CTR(1,1)) , & 
                & nbez*(nbez-1)*WEIGHT_CTR(3)/WEIGHT_CTR(1)*(PTS_CTR(3,2)-PTS_CTR(1,2)) + 2*nbez*(WEIGHT_CTR(2)*(WEIGHT_CTR(1)-nbez*WEIGHT_CTR(2))/(WEIGHT_CTR(1)**2))*(PTS_CTR(2,2)-PTS_CTR(1,2)) , &
                & nbez*(nbez-1)*WEIGHT_CTR(3)/WEIGHT_CTR(1)*(PTS_CTR(3,3)-PTS_CTR(1,3)) + 2*nbez*(WEIGHT_CTR(2)*(WEIGHT_CTR(1)-nbez*WEIGHT_CTR(2))/(WEIGHT_CTR(1)**2))*(PTS_CTR(2,3)-PTS_CTR(1,3)) /)
    rbez2_1 = (/ nbez*(nbez-1)*WEIGHT_CTR(5)/WEIGHT_CTR(7)*(PTS_CTR(7,1)-PTS_CTR(5,1)) + 2*nbez*(WEIGHT_CTR(6)*(WEIGHT_CTR(7)-nbez*WEIGHT_CTR(6))/(WEIGHT_CTR(7)**2))*(PTS_CTR(7,1)-PTS_CTR(6,1)) , & 
                & nbez*(nbez-1)*WEIGHT_CTR(5)/WEIGHT_CTR(7)*(PTS_CTR(7,2)-PTS_CTR(5,2)) + 2*nbez*(WEIGHT_CTR(6)*(WEIGHT_CTR(7)-nbez*WEIGHT_CTR(6))/(WEIGHT_CTR(7)**2))*(PTS_CTR(7,2)-PTS_CTR(6,2)) , &
                & nbez*(nbez-1)*WEIGHT_CTR(5)/WEIGHT_CTR(7)*(PTS_CTR(7,3)-PTS_CTR(5,3)) + 2*nbez*(WEIGHT_CTR(6)*(WEIGHT_CTR(7)-nbez*WEIGHT_CTR(6))/(WEIGHT_CTR(7)**2))*(PTS_CTR(7,3)-PTS_CTR(6,3)) /)

    rbez3_0 = (/ nbez*(nbez-1)*(nbez-2)*WEIGHT_CTR(4)/WEIGHT_CTR(1)*(PTS_CTR(4,1)-PTS_CTR(1,1)) + 3*nbez*(nbez-1)*(WEIGHT_CTR(3)*(2*WEIGHT_CTR(1)-nbez*WEIGHT_CTR(2))/(WEIGHT_CTR(1)**2))*(PTS_CTR(3,1)-PTS_CTR(1,1)) + 3*nbez*(WEIGHT_CTR(2)/WEIGHT_CTR(1)**3)*( nbez**2*(2*WEIGHT_CTR(2)**2-WEIGHT_CTR(1)*WEIGHT_CTR(3)) + nbez*WEIGHT_CTR(1)*(WEIGHT_CTR(3)-4*WEIGHT_CTR(2)) + 2*WEIGHT_CTR(1)**2 )*(PTS_CTR(2,1)-PTS_CTR(1,1)) , & 
                & nbez*(nbez-1)*(nbez-2)*WEIGHT_CTR(4)/WEIGHT_CTR(1)*(PTS_CTR(4,2)-PTS_CTR(1,2)) + 3*nbez*(nbez-1)*(WEIGHT_CTR(3)*(2*WEIGHT_CTR(1)-nbez*WEIGHT_CTR(2))/(WEIGHT_CTR(1)**2))*(PTS_CTR(3,2)-PTS_CTR(1,2)) + 3*nbez*(WEIGHT_CTR(2)/WEIGHT_CTR(1)**3)*( nbez**2*(2*WEIGHT_CTR(2)**2-WEIGHT_CTR(1)*WEIGHT_CTR(3)) + nbez*WEIGHT_CTR(1)*(WEIGHT_CTR(3)-4*WEIGHT_CTR(2)) + 2*WEIGHT_CTR(1)**2 )*(PTS_CTR(2,2)-PTS_CTR(1,2)) , &
                & nbez*(nbez-1)*(nbez-2)*WEIGHT_CTR(4)/WEIGHT_CTR(1)*(PTS_CTR(4,3)-PTS_CTR(1,3)) + 3*nbez*(nbez-1)*(WEIGHT_CTR(3)*(2*WEIGHT_CTR(1)-nbez*WEIGHT_CTR(2))/(WEIGHT_CTR(1)**2))*(PTS_CTR(3,3)-PTS_CTR(1,3)) + 3*nbez*(WEIGHT_CTR(2)/WEIGHT_CTR(1)**3)*( nbez**2*(2*WEIGHT_CTR(2)**2-WEIGHT_CTR(1)*WEIGHT_CTR(3)) + nbez*WEIGHT_CTR(1)*(WEIGHT_CTR(3)-4*WEIGHT_CTR(2)) + 2*WEIGHT_CTR(1)**2 )*(PTS_CTR(2,3)-PTS_CTR(1,3)) /)
    rbez3_1 = (/ nbez*(nbez-1)*(nbez-2)*WEIGHT_CTR(4)/WEIGHT_CTR(7)*(PTS_CTR(7,1)-PTS_CTR(4,1)) + 3*nbez*(nbez-1)*(WEIGHT_CTR(5)*(2*WEIGHT_CTR(7)-nbez*WEIGHT_CTR(6))/(WEIGHT_CTR(7)**2))*(PTS_CTR(7,1)-PTS_CTR(5,1)) + 3*nbez*(WEIGHT_CTR(6)/WEIGHT_CTR(7)**3)*( nbez**2*(2*WEIGHT_CTR(6)**2-WEIGHT_CTR(7)*WEIGHT_CTR(5)) + nbez*WEIGHT_CTR(7)*(WEIGHT_CTR(5)-4*WEIGHT_CTR(6)) + 2*WEIGHT_CTR(7)**2 )*(PTS_CTR(7,1)-PTS_CTR(6,1)) , & 
                & nbez*(nbez-1)*(nbez-2)*WEIGHT_CTR(4)/WEIGHT_CTR(7)*(PTS_CTR(7,2)-PTS_CTR(4,2)) + 3*nbez*(nbez-1)*(WEIGHT_CTR(5)*(2*WEIGHT_CTR(7)-nbez*WEIGHT_CTR(6))/(WEIGHT_CTR(7)**2))*(PTS_CTR(7,2)-PTS_CTR(5,2)) + 3*nbez*(WEIGHT_CTR(6)/WEIGHT_CTR(7)**3)*( nbez**2*(2*WEIGHT_CTR(6)**2-WEIGHT_CTR(7)*WEIGHT_CTR(5)) + nbez*WEIGHT_CTR(7)*(WEIGHT_CTR(5)-4*WEIGHT_CTR(6)) + 2*WEIGHT_CTR(7)**2 )*(PTS_CTR(7,2)-PTS_CTR(6,2)) , &
                & nbez*(nbez-1)*(nbez-2)*WEIGHT_CTR(4)/WEIGHT_CTR(7)*(PTS_CTR(7,3)-PTS_CTR(4,3)) + 3*nbez*(nbez-1)*(WEIGHT_CTR(5)*(2*WEIGHT_CTR(7)-nbez*WEIGHT_CTR(6))/(WEIGHT_CTR(7)**2))*(PTS_CTR(7,3)-PTS_CTR(5,3)) + 3*nbez*(WEIGHT_CTR(6)/WEIGHT_CTR(7)**3)*( nbez**2*(2*WEIGHT_CTR(6)**2-WEIGHT_CTR(7)*WEIGHT_CTR(5)) + nbez*WEIGHT_CTR(7)*(WEIGHT_CTR(5)-4*WEIGHT_CTR(6)) + 2*WEIGHT_CTR(7)**2 )*(PTS_CTR(7,3)-PTS_CTR(6,3)) /)

    ! e = r'/NORM(r')
    et_0 = rbez1_0 / NORM( rbez1_0 )
    DOM%MLG%PATCH_LIM%ET( 2*(iarete-1)+1 , : ) = et_0
    et_1 = rbez1_1 / NORM( rbez1_1 )
    DOM%MLG%PATCH_LIM%ET( 2*iarete , : ) = et_1
     

    ! b = (r'^r'')/NORM(r'^r'')
    VECT = CROSS( rbez1_0 , rbez2_0 )
    IF ( NORM(VECT) > eps_min ) THEN
        bt_0 = VECT / NORM(VECT)
    ELSE 
        bt_0 = (/ 0.0, 0.0, 0.0 /)
    END IF
    DOM%MLG%PATCH_LIM%BT( 2*(iarete-1)+1, : ) = bt_0
    VECT = CROSS( rbez1_1 , rbez2_1 )
    IF ( NORM(VECT) > eps_min ) THEN
        bt_1 = VECT / NORM(VECT)
    ELSE 
        bt_1 = (/ 0.0, 0.0, 0.0 /)
    END IF    
    DOM%MLG%PATCH_LIM%BT( 2*iarete, : ) = bt_1

    ! n = b^e
    nt_0 = CROSS( et_0 , bt_0 )
    DOM%MLG%PATCH_LIM%NT( 2*(iarete-1)+1 , : ) = nt_0
    nt_1 = CROSS( et_1 , bt_1 )
    DOM%MLG%PATCH_LIM%NT( 2*iarete , : ) = nt_1

    ! k = (r'^r'')/r'^3
    VECT = CROSS( rbez1_0 , rbez2_0 )
    VAL = NORM(VECT) / ( NORM(rbez1_0)**3 )
    DOM%MLG%PATCH_LIM%KT( 2*(iarete-1)+1 ) = VAL
    VECT = CROSS( rbez1_1 , rbez2_1 )
    VAL = NORM(VECT) / ( NORM(rbez1_1)**3 )
    DOM%MLG%PATCH_LIM%KT( 2*iarete ) = VAL   

    ! tau = det(r',r'',r''')/||r'^r''||^2
    MATRIX_R(1,:) = rbez1_0(:)
    MATRIX_R(2,:) = rbez2_0(:)
    MATRIX_R(3,:) = rbez3_0(:)
    DET_R = det3D( MATRIX_R )
    VECT = CROSS( rbez1_0 , rbez2_0 )
    VAL = NORM(VECT)**2
    IF ( VAL > eps_min ) THEN
        DOM%MLG%PATCH_LIM%TAUT( 2*(iarete-1)+1 ) = DET_R / VAL
    ELSE
        DOM%MLG%PATCH_LIM%TAUT( 2*(iarete-1)+1 ) = 0.0
    END IF
    MATRIX_R(1,:) = rbez1_1(:)
    MATRIX_R(2,:) = rbez2_1(:)
    MATRIX_R(3,:) = rbez3_1(:)
    DET_R = det3D( MATRIX_R )
    VECT = CROSS( rbez1_1 , rbez2_1 )
    VAL = NORM(VECT)**2
    IF ( VAL > eps_min ) THEN
        DOM%MLG%PATCH_LIM%TAUT(2*iarete) = DET_R / VAL
    ELSE
        DOM%MLG%PATCH_LIM%TAUT(2*iarete) = 0.0
    END IF    

    ! sigma_iL & sigma_iR
    P01(:) = PTS_CTR(2,:) - PTS_CTR(1,:) 
    P02(:) = PTS_CTR(3,:) - PTS_CTR(1,:)
    VECT = CROSS( P02 , P01 )
    VAL = NORM( VECT )
    PS = DOT_PRODUCT( nt_0 , DOM%MLG%SOM_LIM( DOM%MLG%FAC_LIM(iface)%SOMMETS( relation_ind(iarete,1) ) )%NORMALES( DOM%MLG%FAC_LIM(iface)%TAG_SOMMET( relation_ind(iarete,1) ) , : ) )
    IF ( VAL < eps_min ) THEN
        DOM%MLG%PATCH_LIM%SIGMA( iarete, 1 ) = 0
    ELSEIF ( PS > eps_min ) THEN
        DOM%MLG%PATCH_LIM%SIGMA( iarete, 1 ) = 1
    ELSE
        DOM%MLG%PATCH_LIM%SIGMA( iarete, 1 ) = -1
    END IF
    P01(:) = PTS_CTR(7,:) - PTS_CTR(6,:) 
    P02(:) = PTS_CTR(7,:) - PTS_CTR(5,:)
    VECT = CROSS( P02 , P01 )
    VAL = NORM( VECT )
    PS = DOT_PRODUCT( nt_1 , DOM%MLG%SOM_LIM( DOM%MLG%FAC_LIM(iface)%SOMMETS( relation_ind(iarete,2) ) )%NORMALES( DOM%MLG%FAC_LIM(iface)%TAG_SOMMET( relation_ind(iarete,2) ) , : ) )
    IF ( VAL < eps_min ) THEN
        DOM%MLG%PATCH_LIM%SIGMA( iarete, 2 ) = 0
    ELSEIF ( PS > eps_min ) THEN
        DOM%MLG%PATCH_LIM%SIGMA( iarete, 2 ) = 1
    ELSE
        DOM%MLG%PATCH_LIM%SIGMA( iarete, 2 ) = -1
    END IF    
END DO
!
! Angle entre les tangentes
!
et_0 = DOM%MLG%PATCH_LIM%ET(1,:) ! e1(0)
et_1 = DOM%MLG%PATCH_LIM%ET(3,:) ! e2(0)
VAL = angle_vect( et_1 , et_0 )
VECT = CROSS( et_0 , et_1 )
PS = DOT_PRODUCT( VECT , DOM%MLG%SOM_LIM( DOM%MLG%FAC_LIM(iface)%SOMMETS(1) )%NORMALES( DOM%MLG%FAC_LIM(iface)%TAG_SOMMET(1) , : ) )
IF ( PS > eps_min ) THEN
    DOM%MLG%PATCH_LIM%ANGLES(1) = VAL
ELSE
    DOM%MLG%PATCH_LIM%ANGLES(1) = -VAL
END IF
et_0 = DOM%MLG%PATCH_LIM%ET(2,:) ! e1(1)
et_1 = DOM%MLG%PATCH_LIM%ET(7,:) ! e4(0)
VAL = angle_vect( et_1 , et_0 )
VECT = CROSS( et_0 , et_1 )
PS = DOT_PRODUCT( VECT , DOM%MLG%SOM_LIM( DOM%MLG%FAC_LIM(iface)%SOMMETS(2) )%NORMALES( DOM%MLG%FAC_LIM(iface)%TAG_SOMMET(2) , : ) )
IF ( PS > eps_min ) THEN
    DOM%MLG%PATCH_LIM%ANGLES(2) = VAL
ELSE
    DOM%MLG%PATCH_LIM%ANGLES(2) = -VAL
END IF
et_0 = DOM%MLG%PATCH_LIM%ET(6,:) ! e3(1)
et_1 = DOM%MLG%PATCH_LIM%ET(8,:) ! e4(1)
VAL = angle_vect( et_1 , et_0 )
VECT = CROSS( et_0 , et_1 )
PS = DOT_PRODUCT( VECT , DOM%MLG%SOM_LIM( DOM%MLG%FAC_LIM(iface)%SOMMETS(3) )%NORMALES( DOM%MLG%FAC_LIM(iface)%TAG_SOMMET(3) , : ) )
IF ( PS > eps_min ) THEN
    DOM%MLG%PATCH_LIM%ANGLES(3) = VAL
ELSE
    DOM%MLG%PATCH_LIM%ANGLES(3) = -VAL
END IF
et_0 = DOM%MLG%PATCH_LIM%ET(5,:) ! e3(0)
et_1 = DOM%MLG%PATCH_LIM%ET(4,:) ! e2(1)
VAL = angle_vect( et_1 , et_0 )
VECT = CROSS( et_0 , et_1 )
PS = DOT_PRODUCT( VECT , DOM%MLG%SOM_LIM( DOM%MLG%FAC_LIM(iface)%SOMMETS(4) )%NORMALES( DOM%MLG%FAC_LIM(iface)%TAG_SOMMET(4) , : ) )
IF ( PS > eps_min ) THEN
    DOM%MLG%PATCH_LIM%ANGLES(4) = VAL
ELSE
    DOM%MLG%PATCH_LIM%ANGLES(4) = -VAL
END IF
!
! Twist vector Wi
!
! W1
rbez1_0 = DOM%MLG%PATCH_LIM%RP(3,:)
Wi = ( NORM(rbez1_0)**2 )* &
        & ( COS( DOM%MLG%PATCH_LIM%ANGLES(1) )*DOM%MLG%PATCH_LIM%SIGMA(1,1)*DOM%MLG%PATCH_LIM%KT(1) + &
        & SIN( DOM%MLG%PATCH_LIM%ANGLES(1) )*DOM%MLG%PATCH_LIM%TAUT(1) )*DOM%MLG%PATCH_LIM%NT(1,:)
DOM%MLG%PATCH_LIM%TWIST(1,:) = Wi
! W2
rbez1_0 = DOM%MLG%PATCH_LIM%RP(4,:)
Wi = ( NORM(rbez1_0)**2 )* &
        & ( COS( DOM%MLG%PATCH_LIM%ANGLES(2) )*DOM%MLG%PATCH_LIM%SIGMA(4,1)*DOM%MLG%PATCH_LIM%KT(7) - &
        & SIN( DOM%MLG%PATCH_LIM%ANGLES(2) )*DOM%MLG%PATCH_LIM%TAUT(7) )*DOM%MLG%PATCH_LIM%NT(7,:)
DOM%MLG%PATCH_LIM%TWIST(2,:) = Wi
! W3
rbez1_0 = DOM%MLG%PATCH_LIM%RP(8,:)
Wi = ( NORM(rbez1_0)**2 )* &
        & ( COS( DOM%MLG%PATCH_LIM%ANGLES(3) )*DOM%MLG%PATCH_LIM%SIGMA(3,2)*DOM%MLG%PATCH_LIM%KT(6) + &
        & SIN( DOM%MLG%PATCH_LIM%ANGLES(3) )*DOM%MLG%PATCH_LIM%TAUT(6) )*DOM%MLG%PATCH_LIM%NT(6,:)
DOM%MLG%PATCH_LIM%TWIST(3,:) = Wi
! W4
rbez1_0 = DOM%MLG%PATCH_LIM%RP(4,:)
Wi = ( NORM(rbez1_0)**2 )* &
        & ( COS( DOM%MLG%PATCH_LIM%ANGLES(4) )*DOM%MLG%PATCH_LIM%SIGMA(3,1)*DOM%MLG%PATCH_LIM%KT(5) + &
        & SIN( DOM%MLG%PATCH_LIM%ANGLES(4) )*DOM%MLG%PATCH_LIM%TAUT(5) )*DOM%MLG%PATCH_LIM%NT(5,:)
DOM%MLG%PATCH_LIM%TWIST(4,:) = Wi
!
END SUBROUTINE update_bezier_face_quad
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Calcul des matrices de passage aux coordonnées locales
SUBROUTINE ablation_matrices_passage ( DOM , iface )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: iface
REAL(DP) :: delta
REAL(DP), DIMENSION(3) :: SOM1, SOM2, SOM3, VB1, VB2, VB3
REAL(DP), DIMENSION(3) :: P1B, P2B, P3B
REAL(DP), DIMENSION(3,3) :: mat_plan, mat_triangle
!
SOM1 = DOM%MLG%SOMMET( DOM%MLG%FAC_LIM(iface)%SOMMETS(1), : )
SOM2 = DOM%MLG%SOMMET( DOM%MLG%FAC_LIM(iface)%SOMMETS(2), : )
SOM3 = DOM%MLG%SOMMET( DOM%MLG%FAC_LIM(iface)%SOMMETS(3), : )
VB1 = SOM2 - SOM1
VB1 = VB1 / NORM( VB1 )
VB2 = SOM3 - SOM1
VB2 = CROSS (VB1 , VB2 )
VB2 = VB2 / NORM( VB2 )
VB2 = CROSS( VB2 , VB1 )
VB3 = CROSS( VB1 , VB2 )
!
mat_plan(:,:) = 0.0_dp
mat_plan(:,1) = VB1(:)
mat_plan(:,2) = VB2(:)
mat_plan(:,3) = VB3(:)
mat_plan = INV_MAT3( mat_plan )
!
P1B = (/ 0.0, 0.0, 0.0 /)
P2B = SOM2 - SOM1
P3B = SOM3 - SOM1
P2B = MATMUL( mat_plan, P2B )
P3B = MATMUL( mat_plan, P3B )
!
delta = P1B(1)*P2B(2) - P2B(1)*P1B(2) + P2B(1)*P3B(2) - P3B(1)*P2B(2) + P3B(1)*P1B(2) - P1B(1)*P3B(2)
mat_triangle(:,:) = 0.0_dp
mat_triangle(1,1) = ( P2B(2) - P3B(2) ) / delta
mat_triangle(1,2) = ( P3B(1) - P2B(1) ) / delta
mat_triangle(1,3) = ( P2B(1)*P3B(2) - P3B(1)*P2B(2) ) / delta
mat_triangle(2,1) = ( P3B(2) - P1B(2) ) / delta
mat_triangle(2,2) = ( P1B(1) - P3B(1) ) / delta
mat_triangle(2,3) = ( P3B(1)*P1B(2) - P1B(1)*P3B(2) ) / delta
mat_triangle(3,1) = ( P1B(2) - P2B(2) ) / delta
mat_triangle(3,2) = ( P2B(1) - P1B(1) ) / delta
mat_triangle(3,3) = ( P1B(1)*P2B(2) - P2B(1)*P1B(2) ) / delta
!
DOM%MLG%FAC_LIM(iface)%MAT_PLAN = mat_plan
DOM%MLG%FAC_LIM(iface)%MAT_TRIANGLE = mat_triangle
!
END SUBROUTINE ablation_matrices_passage
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Calcul du vecteur lambda_i
SUBROUTINE ablation_lambdai ( coord_tri_P , lambda_v )
!
!---- Déclarations ----------------------------------------------------!
REAL(DP), DIMENSION(3) :: coord_tri_P, lambda_v
!
lambda_v(1) = coord_tri_P(1)*coord_tri_P(1)*( 3 - 2*coord_tri_P(1) + 6*coord_tri_P(3)*coord_tri_P(2) )
lambda_v(2) = coord_tri_P(2)*coord_tri_P(2)*( 3 - 2*coord_tri_P(2) + 6*coord_tri_P(1)*coord_tri_P(3) )
lambda_v(3) = coord_tri_P(3)*coord_tri_P(3)*( 3 - 2*coord_tri_P(3) + 6*coord_tri_P(1)*coord_tri_P(2) )
!
END SUBROUTINE ablation_lambdai
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Calcul des coordonnées locales sur la face (triangle) du sommet 
SUBROUTINE  ablation_coord_loc( DOM , proj_face , SOM_D , coord_tri_P )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: proj_face
REAL(DP), DIMENSION(3) :: reg_som, SOM_D, SOM_D_plan, SOM1, coord_tri_P
REAL(DP), DIMENSION(3,3) :: mat_plan, mat_triangle
!
mat_plan = DOM%MLG%FAC_LIM(proj_face)%MAT_PLAN
mat_triangle = DOM%MLG%FAC_LIM(proj_face)%MAT_TRIANGLE
SOM1 = DOM%MLG%SOMMET( DOM%MLG%SOM_ADJ_LIM( DOM%MLG%FAC_LIM(proj_face)%SOMMETS(1) , 1 ) , : )
coord_tri_P(:) = 0.0_dp
!
reg_som = SOM_D - SOM1
SOM_D_plan = MATMUL( mat_plan , reg_som )
!
coord_tri_P(1) = mat_triangle(1,1)*SOM_D_plan(1) + mat_triangle(1,2)*SOM_D_plan(2) + mat_triangle(1,3)
coord_tri_P(2) = mat_triangle(2,1)*SOM_D_plan(1) + mat_triangle(2,2)*SOM_D_plan(2) + mat_triangle(2,3)
coord_tri_P(3) = mat_triangle(3,1)*SOM_D_plan(1) + mat_triangle(3,2)*SOM_D_plan(2) + mat_triangle(3,3)
!
END SUBROUTINE ablation_coord_loc
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Calcul du "twist vecteur" aux sommets de la face
SUBROUTINE twist_vector_tri ( DOM , iarete , iface , Wi )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: sigma, iarete, iface
REAL(DP) :: Ai, Aim1, Aip1, alphaim1_0, dim1_0, tauip1, kip1, zero, t
REAL(DP), DIMENSION(3) :: VECT_N, Wi, rip1_0, rip1_1, rim1_0, rim1_1
!
rip1_0 = DOM%MLG%PATCH_LIM%RP( 2*MODULO( iarete,3 ) +1 , : )
rip1_1 = DOM%MLG%PATCH_LIM%RP( 2*( MODULO( iarete,3 ) +1 ) , : )
rim1_0 = DOM%MLG%PATCH_LIM%RP( 2*( MODULO( iarete+1,3 ) +1 ) - 1 , : )
rim1_1 = DOM%MLG%PATCH_LIM%RP( 2*( MODULO( iarete+1,3) +1 ) , : )
Aip1 = DOM%MLG%PATCH_LIM%ANGLES( MODULO( iarete,3 ) +1 )
Ai = DOM%MLG%PATCH_LIM%ANGLES( iarete )
Aim1 = DOM%MLG%PATCH_LIM%ANGLES( MODULO( iarete+1,3 ) +1 )
sigma = DOM%MLG%PATCH_LIM%SIGMA( MODULO( iarete,3 ) +1 , 2 )
kip1 = DOM%MLG%PATCH_LIM%KT( 2*( MODULO(iarete,3) +1 ) )
tauip1 = DOM%MLG%PATCH_LIM%TAUT( 2*( MODULO(iarete,3) +1 ) )
VECT_N = DOM%MLG%SOM_LIM( DOM%MLG%FAC_LIM(iface)%SOMMETS(iarete) )%NORMALES( DOM%MLG%FAC_LIM(iface)%TAG_SOMMET(iarete), : )
!
zero = 0.0
t = 0.0
CALL abl_di_p( NORM(rim1_1) , zero , zero , NORM(rip1_0) , t , dim1_0 )
CALL abl_alphai_p ( Pi - Aip1 , zero , zero , Aim1 , t , alphaim1_0 )
!
Wi = ( -dim1_0 + NORM(rip1_1)*alphaim1_0*COS(Ai)/SIN(Ai) )*DOM%MLG%PATCH_LIM%ET( 2*( MODULO(iarete,3) +1 ), : ) - &
        & NORM(rip1_1)*alphaim1_0*( 1/SIN(Ai) )*DOM%MLG%PATCH_LIM%ET( 2*( MODULO(iarete+1,3) +1) -1, : ) + &
        & NORM(rip1_1)*NORM(rim1_0)*( COS(Ai)*sigma*kip1 + SIN(Ai)*tauip1 )*VECT_N
!
END SUBROUTINE twist_vector_tri
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Calcul de alpha_i(t)
SUBROUTINE abl_alphai ( alpha_iL , alpha_i1 , alpha_i2 , alpha_iR , t , alphai )
!
!---- Déclarations ----------------------------------------------------!
REAL(DP) :: alpha_iL, alpha_i1, alpha_i2, alpha_iR, t, alphai
REAL(DP), DIMENSION(2,4) :: H
!
H = hermite(t)
!
alphai = alpha_iL*H(1,1) + alpha_i1*H(1,2) + alpha_i2*H(1,3) + alpha_iR*H(1,4)
!
END SUBROUTINE abl_alphai
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Calcul de alpha_i'(t)
SUBROUTINE abl_alphai_p ( alpha_iL , alpha_i1 , alpha_i2 , alpha_iR , t , alphaip )
!
!---- Déclarations ----------------------------------------------------!
REAL(DP) :: alpha_iL, alpha_i1, alpha_i2, alpha_iR, t, alphaip
REAL(DP), DIMENSION(2,4) :: H
!
H = hermite(t)
!
alphaip = alpha_iL*H(2,1) + alpha_i1*H(2,2) + alpha_i2*H(2,3) + alpha_iR*H(2,4)
!
END SUBROUTINE abl_alphai_p
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Calcul de d_i(t)
SUBROUTINE abl_di ( d_iL , d_i1 , d_i2 , d_iR , t , di )
!
!---- Déclarations ----------------------------------------------------!
REAL(DP) :: d_iL, d_i1, d_i2, d_iR, t, di
REAL(DP), DIMENSION(2,4) :: H
!
H = hermite(t)
!
di = d_iL*H(1,1) + d_i1*H(1,2) + d_i2*H(1,3) + d_iR*H(1,4)
!
END SUBROUTINE abl_di
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
SUBROUTINE abl_di_p ( d_iL , d_i1 , d_i2 , d_iR , t , dip )
!> Calcul de d_i'(t)
!
!---- Déclarations ----------------------------------------------------!
REAL(DP) :: d_iL, d_i1, d_i2, d_iR, t, dip
REAL(DP), DIMENSION(2,4) :: H
!
H = hermite(t)
!
dip = d_iL*H(2,1) + d_i1*H(2,2) + d_i2*H(2,3) + d_iR*H(2,4)
!
END SUBROUTINE abl_di_p
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
! > Equilibrage des sommets limites donc le tag = 2
SUBROUTINE deform3D_ntag2 ( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: nsom_lim, isom, id_som
INTEGER(IP) :: ivois1, ivois2, iare1, iare2, iiter, niter, tag_lim_dimension
REAL(DP) :: DIST, DIST1, DIST2, K1, K2, L_DEP, tol_def, taux_def, max_dep_som, DIIF_DEP
REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: SOM_POS, SOMPOS0
REAL(DP), ALLOCATABLE, DIMENSION(:) :: L_DEP_MEM
REAL(DP), DIMENSION(3) :: SOM, SOM1, SOM2, VEC1, VEC2
REAL(DP), DIMENSION(3) :: SOM0, SOM10, SOM20, VEC
LOGICAL :: SVOIS_NTAG3
!
nsom_lim = DOM%MLG%nsom_lim
max_dep_som = DOM%RSL%max_dep_som
!
ALLOCATE( SOM_POS( DOM%MLG%nsom,3 ) )
ALLOCATE( SOMPOS0( DOM%MLG%nsom,3 ) )
ALLOCATE( L_DEP_MEM( DOM%MLG%nsom ) )
L_DEP_MEM = 0.0_dp
SOM_POS = DOM%MLG%SOMMET + DOM%ETAT%D_SOM
SOMPOS0 = DOM%MLG%SOMMET + DOM%ETAT%D_SOM
IF ( DOM%MLG%dim_simu == '3D' ) THEN
    tag_lim_dimension = 2
ELSE
    tag_lim_dimension = 1
END IF
!
niter = DOM%NUM%niter_def
tol_def = DOM%NUM%tol_def
!
! Boucle sur les iterations de deformation
DO iiter = 1, niter 
    taux_def = 0.0_dp
    DO isom = 1, nsom_lim
        IF ( DOM%MLG%SOM_LIM(isom)%nb_tags /= tag_lim_dimension ) CYCLE
        ivois1 = -1
        ivois2 = -1
        id_som = DOM%MLG%SOMLIM2FACLIM( isom,1 )
        ! On vérifie que le sommet de tag2 est à équilibrer
        IF ( .NOT. DOM%MLG%SOM_LIM(isom)%equilibrage ) CYCLE
        SOM = SOM_POS(id_som,:)
        SOM0 = SOMPOS0(id_som,:)

        ! On récupère les deux sommets voisins sur la "courbe limite" qui sépare les tags différents
        IF ( DOM%MLG%dim_simu == '3D' ) THEN 
            CALL find_voisins_ntag2 ( DOM , isom , iare1 , iare2 , SVOIS_NTAG3 )

            IF ( DOM%MLG%ARET_LIM(iare1)%SOMMETS(2) == isom ) THEN
                SOM1 = SOM_POS( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare1)%SOMMETS(1), 1 ) , : )
                SOM10 = SOMPOS0( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare1)%SOMMETS(1), 1 ) , : )
            ELSE
                SOM1 = SOM_POS( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare1)%SOMMETS(2), 1 ) , : )
                SOM10 = SOMPOS0( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare1)%SOMMETS(2), 1 ) , : )
            END IF      
            IF ( DOM%MLG%ARET_LIM(iare2)%SOMMETS(2) == isom ) THEN
                SOM2 = SOM_POS( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare2)%SOMMETS(1), 1 ) , : )
                SOM20 = SOMPOS0( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare2)%SOMMETS(1), 1 ) , : )
            ELSE
                SOM2 = SOM_POS( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare2)%SOMMETS(2), 1 ) , : )
                SOM20 = SOMPOS0( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare2)%SOMMETS(2), 1 ) , : )
            END IF          
        ELSE
            SOM1 = SOM_POS( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%SOM_LIM(isom)%SOMMETS_LIM(1), 1 ) , : )
            SOM10 = SOMPOS0( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%SOM_LIM(isom)%SOMMETS_LIM(1), 1 ) , : )
            SOM2 = SOM_POS( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%SOM_LIM(isom)%SOMMETS_LIM(2), 1 ) , : )
            SOM20 = SOMPOS0( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%SOM_LIM(isom)%SOMMETS_LIM(2), 1 ) , : )
            iare1 = 1
            iare2 = 2
        END IF  

        VEC1 = SOM1 - SOM
        VEC2 = SOM2 - SOM
        DIST1 = NORM(VEC1)
        DIST2 = NORM(VEC2)
        VEC1 = VEC1 / NORM(VEC1)
        VEC2 = VEC2 / NORM(VEC2)
        DIST = DIST1 + DIST2
        K1 = DOM%MLG%ARET_LIM(iare1)%raideur
        K2 = DOM%MLG%ARET_LIM(iare2)%raideur        
        L_DEP = amorti_eq_abl_C*( K2*DIST2 - K1*DIST1 ) / ( K1 + K2 )
        IF ( ABS(L_DEP) / DOM%MLG%SOM_LIM(isom)%dist_caract < 0.01*tol_def ) THEN
            CYCLE
        ELSE
            VEC1 = SOM10 - SOM0
            VEC2 = SOM20 - SOM0
            VEC1 = VEC1 / NORM(VEC1)
            VEC2 = VEC2 / NORM(VEC2)
            L_DEP = L_DEP_MEM(isom) + L_DEP
            ! Limitation du deplacement
            IF ( L_DEP > 5.0*max_dep_som ) THEN 
                L_DEP = 5.0*max_dep_som
            ELSE IF ( L_DEP < -5.0*max_dep_som ) THEN
                L_DEP = -5.0*max_dep_som
            END IF    
            DIIF_DEP = L_DEP - L_DEP_MEM(isom)
            taux_def = MAX( taux_def , ABS(DIIF_DEP) / DOM%MLG%SOM_LIM(isom)%dist_caract )                
            IF ( L_DEP < -100*eps_min ) THEN
                SOM_POS(id_som,:) = SOMPOS0(id_som,:) - L_DEP*VEC1
            ELSE IF ( L_DEP > 100*eps_min ) THEN
                SOM_POS(id_som,:) = SOMPOS0(id_som,:) + L_DEP*VEC2
            ELSE
                SOM_POS(id_som,:) = SOMPOS0(id_som,:)
            END IF
            L_DEP_MEM(isom) = L_DEP
        END IF
    END DO
    IF ( taux_def < tol_def ) EXIT
END DO
DOM%RSL%niter_eq_AB(1) = iiter
DOM%RSL%err_eq_AB(1) = taux_def
DOM%ETAT%D_SOM = SOM_POS - DOM%MLG%SOMMET
!
! On sauvegarde les sommets a projeter sur les courbes
DO isom = 1, nsom_lim
    IF ( DOM%MLG%SOM_LIM(isom)%nb_tags /= tag_lim_dimension ) CYCLE
    id_som = DOM%MLG%SOMLIM2FACLIM(isom,1)
    VEC = DOM%ETAT%D_SOM(id_som,:)
    IF ( NORM(VEC) > 100*eps_min ) THEN
        DOM%MLG%SOM_LIM(isom)%projection_courb = .TRUE.
    END IF
END DO
!
DEALLOCATE( SOM_POS )
DEALLOCATE( SOMPOS0 )
!
END SUBROUTINE deform3D_ntag2
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Equilibrage des sommets limites donc le tag = 1
SUBROUTINE deform3D_ntag ( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: isom, ivois, id_som, iare, iiter, niter, idsomvois, proj_face
INTEGER(IP) :: proj_arete, proj_arete2, idarete, iarete, nsom_lim
REAL(DP) :: RAID, PS, LONG_DEP, A_NORM, taux_def, taux_defm1, tol_def, max_dep_som, lambda
REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: SOM_POS, DEP_MEM, SOMPOS0
REAL(DP), DIMENSION(3) :: DEP, DIR, SOM, SOM1, ARET1, ARET2, N_DIR
REAL(DP), DIMENSION(3) :: SOM0, NORM_SOM, DIIF_DEP, coord_tri_P
!
ALLOCATE( SOM_POS( DOM%MLG%nsom,3 ) )
ALLOCATE( SOMPOS0( DOM%MLG%nsom,3 ) )
ALLOCATE( DEP_MEM( DOM%MLG%nsom,3 ) )
SOM_POS = DOM%MLG%SOMMET + DOM%ETAT%D_SOM
SOMPOS0 = DOM%MLG%SOMMET + DOM%ETAT%D_SOM
DEP_MEM = 0.0_dp
!
nsom_lim = DOM%MLG%nsom_lim
niter = DOM%NUM%niter_def
tol_def = DOM%NUM%tol_def
max_dep_som = DOM%RSL%max_dep_som
!
! Boucle sur les iterations de deformation
taux_defm1 = 0.0_dp
DO iiter = 1, niter 
    taux_def = 0.0_dp
    DO isom = 1, nsom_lim
        IF ( .NOT. DOM%MLG%SOM_LIM(isom)%equilibrage ) CYCLE
        id_som = DOM%MLG%SOMLIM2FACLIM(isom,1)
        IF ( DOM%MLG%SOM_LIM(isom)%nb_tags > 1 ) CYCLE
        DEP(:) = 0.0
        RAID = 0.0
        SOM = SOM_POS(id_som,:)
        SOM0 = SOMPOS0(id_som,:)

        ! Pour équilibrer le sommet, on prend en compte tous ses sommets voisins limites 
        ! On fait converger le systeme de ressorts
        DO ivois = 1, DOM%MLG%SOM_LIM(isom)%nvoisin
            iare = DOM%MLG%SOM_LIM(isom)%ARETES(ivois)
            IF ( DOM%MLG%ARET_LIM(iare)%SOMMETS(2) == isom ) THEN
                SOM1 = SOM_POS( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare)%SOMMETS(1), 1) , : )
            ELSE
                SOM1 = SOM_POS( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare)%SOMMETS(2), 1) , : )
            END IF
            DEP = DEP + DOM%MLG%SOM_LIM(isom)%RAIDEUR(ivois) * ( SOM1 -  SOM )
            RAID = RAID + DOM%MLG%SOM_LIM(isom)%RAIDEUR(ivois)
        END DO
        DEP = amorti_eq_abl_S*DEP/RAID

        IF ( ABS( NORM(DEP) / DOM%MLG%SOM_LIM(isom)%dist_caract ) < 0.01*tol_def ) THEN 
            CYCLE
        ELSE
            ! On trouve les deux aretes dont le produit scalaire est le plus proche 
            ! et on projette le déplacement total sur la face qu'elles délimitent
            NORM_SOM = DOM%MLG%SOM_LIM(isom)%NORMALES(1,:)
            ! On utilise DEP_MEM pour toujours considérer le déplacement depuis le sommet initial
            ! Cela nous permet de rester sur les surfaces (aux erreurs numériques près...)
            DEP = DEP_MEM(isom,:) + DEP 
            DIR = DEP / NORM(DEP)
            PS = DOT_PRODUCT( NORM_SOM , DIR )
            A_NORM = ACOS(PS)
            LONG_DEP = SIN(A_NORM)*NORM(DEP)
            !
            CALL find_face_lim_som ( DOM , isom , DEP , proj_face , coord_tri_P , proj_arete , proj_arete2 )
            !
            ! On récupère ARET1
            IF ( DOM%MLG%ARET_LIM(proj_arete)%SOMMETS(1) == isom ) THEN
                idsomvois = DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(proj_arete)%SOMMETS(2), 1 )
                SOM1 = SOMPOS0( idsomvois, : )            
                ARET1 = SOM1 - SOM0
                ARET1 = ARET1 / NORM(ARET1)
            ELSE IF ( DOM%MLG%ARET_LIM(proj_arete)%SOMMETS(2) == isom ) THEN
                idsomvois = DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(proj_arete)%SOMMETS(1), 1)
                SOM1 = SOMPOS0( idsomvois, : )
                ARET1 = SOM1 - SOM0
                ARET1 = ARET1 / NORM(ARET1)
            ELSE
                CALL print_err('Pas normal ...',1)
            END IF
            ! puis ARET2
            IF ( DOM%MLG%FAC_LIM(proj_face)%nb_som == 3 ) THEN
                DO iarete = 1, 3
                    idarete = DOM%MLG%FAC_LIM(proj_face)%ARETES(iarete)
                    IF ( idarete == proj_arete ) CYCLE
                    IF ( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(idarete)%SOMMETS(1), 1 ) == id_som ) THEN
                        idsomvois = DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(idarete)%SOMMETS(2), 1 ) 
                        SOM1 = SOMPOS0( idsomvois,: )
                        ARET2 = SOM1 - SOM0
                        ARET2 = ARET2 / NORM(ARET2)
                    ELSE IF ( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(idarete)%SOMMETS(2), 1 ) == id_som ) THEN
                        idsomvois = DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(idarete)%SOMMETS(1), 1 )
                        SOM1 = SOMPOS0( idsomvois, : )
                        ARET2 = SOM1 - SOM0
                        ARET2 = ARET2 / NORM(ARET2)
                    END IF  
                END DO
            ELSE IF ( DOM%MLG%FAC_LIM(proj_face)%nb_som == 4 ) THEN
                IF ( DOM%MLG%ARET_LIM(proj_arete2)%SOMMETS(1) == isom ) THEN
                    idsomvois = DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(proj_arete2)%SOMMETS(2), 1 )
                    SOM1 = SOMPOS0( idsomvois, : )
                    ARET2 = SOM1 - SOM0
                    ARET2 = ARET2 / NORM(ARET2)
                ELSE IF ( DOM%MLG%ARET_LIM(proj_arete2)%SOMMETS(2) == isom ) THEN
                    idsomvois = DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(proj_arete2)%SOMMETS(1), 1 )
                    SOM1 = SOMPOS0( idsomvois, : )
                    ARET2 = SOM1 - SOM0
                    ARET2 = ARET2 / NORM(ARET2)
                ELSE
                    CALL print_err('Pas normal ...',1)
                END IF                
            END IF
            !
            ! Vecteur normal à la face
            N_DIR = CROSS( ARET1 , ARET2 )
            N_DIR = N_DIR / NORM(N_DIR)
            lambda = - ( N_DIR(1)*DEP(1) + N_DIR(2)*DEP(2) + N_DIR(3)*DEP(3) ) / ( N_DIR(1)**2 + N_DIR(2)**2 + N_DIR(3)**2 )
            ! Direction du deplacement projeté sur la face
            ARET1 = (/ DEP(1) + lambda*N_DIR(1) , DEP(2) + lambda*N_DIR(2) , DEP(3) + lambda*N_DIR(3) /)
            ARET1 = ARET1 / NORM(ARET1)

            ! LIMITATION DU DEPLACMENT DES SOMMETS LIMITES
            IF ( LONG_DEP > 5.0*max_dep_som ) THEN 
                LONG_DEP = 5.0*max_dep_som
            END IF      
            DEP = LONG_DEP*ARET1
            DIIF_DEP = DEP - DEP_MEM(isom,:)
            taux_def = MAX( taux_def, NORM(DIIF_DEP) / DOM%MLG%SOM_LIM(isom)%dist_caract )
            DEP_MEM(isom,:) = DEP
            SOM_POS(id_som,:) = SOMPOS0(id_som,:) + DEP
        END IF
    END DO
    IF ( taux_def < tol_def ) EXIT
    taux_defm1 = taux_def
END DO
DOM%RSL%niter_eq_AB(2) = iiter
DOM%RSL%err_eq_AB(2) = taux_def
DOM%ETAT%D_SOM = SOM_POS - DOM%MLG%SOMMET
!
DO isom = 1, nsom_lim
    id_som = DOM%MLG%SOMLIM2FACLIM( isom, 1 )
    IF ( DOM%MLG%SOM_LIM(isom)%nb_tags > 1 ) CYCLE
    DEP = DOM%ETAT%D_SOM( id_som, : )
    IF ( NORM(DEP) > 100*eps_min ) THEN
        DOM%MLG%SOM_LIM(isom)%projection_surf = .TRUE.
    END IF
END DO
!
DEALLOCATE( SOM_POS )
DEALLOCATE( DEP_MEM )
DEALLOCATE( SOMPOS0 )
!
END SUBROUTINE deform3D_ntag
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Equilibrage des sommets internes
SUBROUTINE deform3D_int ( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: isom, ivois, id_som, iare, iiter, niter, nsom_int
REAL(DP) :: RAID, taux_def, taux_defm1, tol_def, max_dep_som
REAL(DP), ALLOCATABLE, DIMENSION(:) :: max_dep_dl
REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: SOM_POS, DEP_MEM, SOMPOS0
REAL(DP), DIMENSION(3) :: DEP, SOM, SOM1
REAL(DP), DIMENSION(3) :: SOM0, DIIF_DEP
!
ALLOCATE( SOM_POS( DOM%MLG%nsom, 3 ) )
ALLOCATE( SOMPOS0( DOM%MLG%nsom, 3 ) )
ALLOCATE( DEP_MEM( DOM%MLG%nsom, 3 ) )
ALLOCATE( max_dep_dl( DOM%MLG%nsom_int ) )
SOM_POS = DOM%MLG%SOMMET + DOM%ETAT%D_SOM
SOMPOS0 = DOM%MLG%SOMMET + DOM%ETAT%D_SOM
DEP_MEM = 0.0_dp
!
nsom_int = DOM%MLG%nsom_int
niter = DOM%NUM%niter_def
tol_def = DOM%NUM%tol_def
max_dep_som = DOM%RSL%max_dep_som
max_dep_dl = 0.0_dp
!
! Boucle sur les iterations de deformation
taux_defm1 = 0.0_dp
DO iiter = 1, niter 
    taux_def = 0.0_dp
    DO isom = 1, nsom_int
        IF ( .NOT. DOM%MLG%SOM_INT(isom)%equilibrage ) CYCLE

        id_som = DOM%MLG%SOM_ADJ_INT( isom, 1 )
        DEP = 0.0_dp
        RAID = 0.0_dp
        SOM = SOM_POS( id_som, : )
        SOM0 = SOMPOS0( id_som, : )

        DO ivois = 1, DOM%MLG%SOM_INT(isom)%nvoisin
            iare = DOM%MLG%SOM_INT(isom)%ARETES(ivois)
            IF ( DOM%MLG%ARET_INT(iare)%SOMMETS(2) == id_som ) THEN
                SOM1 = SOM_POS( DOM%MLG%ARET_INT(iare)%SOMMETS(1), : )
            ELSE
                SOM1 = SOM_POS( DOM%MLG%ARET_INT(iare)%SOMMETS(2), : )
            END IF
            DEP = DEP + DOM%MLG%SOM_INT(isom)%RAIDEUR(ivois) * ( SOM1 - SOM )
            RAID = RAID + DOM%MLG%SOM_INT(isom)%RAIDEUR(ivois)
            IF ( iiter == 1 ) THEN
                DIIF_DEP = SOM1 - SOM0
                max_dep_dl(isom) = max_dep_dl(isom) + NORM( DIIF_DEP )
            END IF
        END DO
        DEP = amorti_eq_abl_V * DEP / RAID
        IF ( iiter == 1 ) max_dep_dl(isom) = max_dep_dl(isom) / DOM%MLG%SOM_INT(isom)%nvoisin

        IF ( ABS( NORM(DEP) / max_dep_dl(isom) ) < 0.01*tol_def ) THEN
            CYCLE
        ELSE
            DEP = DEP_MEM(isom,:) + DEP 
            ! On limite le deplacement des sommets int au deplacement max des sommets limites ablatés avant equilibrage
            ! Cela permet de ne pas avoir un reequilibrage trop brutal des sommets int
            IF ( NORM(DEP) > 5.0*max_dep_som ) THEN 
                DEP = 5.0 * max_dep_som * DEP / NORM(DEP)
            END IF
            DIIF_DEP = DEP - DEP_MEM(isom,:)
            taux_def = MAX( taux_def , NORM(DIIF_DEP) / max_dep_dl(isom) )
            DEP_MEM(isom,:) = DEP
            SOM_POS(id_som,:) = SOMPOS0(id_som,:) + DEP
        END IF
    END DO
    IF ( taux_def < tol_def ) EXIT
    taux_defm1 = taux_def
END DO
DOM%RSL%niter_eq_AB(3) = iiter
DOM%RSL%err_eq_AB(3) = taux_def
DOM%ETAT%D_SOM = SOM_POS - DOM%MLG%SOMMET
!
DEALLOCATE( SOM_POS )
DEALLOCATE( DEP_MEM )
DEALLOCATE( SOMPOS0 )
!
END SUBROUTINE deform3D_int
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Avant la premiere iteration, on fait converger pour chaque sommet
! limite son systeme masse ressort afin que le laplacien ne modifie pas
! le maillage initial
! A la fin de la routine, on a les bonnes raideurs de ressort 
SUBROUTINE deform3D_ntag_init_raideur ( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: isom, ivois, id_som, iare, iiter, idsomvois, proj_face
INTEGER(IP) :: proj_arete, proj_arete2, idarete, iarete, nsom_lim
REAL(DP) :: RAID, PS, LONG_DEP, A_NORM, taux_def, tol_def, lambda, dist1, dist2, taux_defm1
REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: SOMPOS0, DEP_MEM
REAL(DP), DIMENSION(3) :: DEP, DIR, SOM1, ARET1, ARET2, N_DIR
REAL(DP), DIMENSION(3) :: SOM0, NORM_SOM, coord_tri_P
!
ALLOCATE( SOMPOS0( DOM%MLG%nsom,3 ) )
ALLOCATE( DEP_MEM( DOM%MLG%nsom,3 ) )
SOMPOS0 = DOM%MLG%SOMMET
!
nsom_lim = DOM%MLG%nsom_lim
tol_def = DOM%NUM%tol_def
!
taux_def = 1.0
taux_defm1 = 0.0
iiter = 0
DEP_MEM = 0.0_dp
DO WHILE ( taux_def > tol_def  .AND. ABS(taux_defm1 - taux_def)/taux_def > tol_def ) 
    taux_defm1 = taux_def    
    iiter = iiter + 1
    taux_def = 0.0_dp
    DO isom = 1, nsom_lim
        id_som = DOM%MLG%SOMLIM2FACLIM(isom,1)
        IF ( DOM%MLG%SOM_LIM(isom)%nb_tags > 1 ) CYCLE
        DEP(:) = 0.0
        RAID = 0.0
        SOM0 = SOMPOS0(id_som,:)

        ! Pour équilibrer le sommet, on prend en compte tous ses sommets voisins limites 
        ! On fait converger le systeme de ressorts
        DO ivois = 1, DOM%MLG%SOM_LIM(isom)%nvoisin
            iare = DOM%MLG%SOM_LIM(isom)%ARETES(ivois)
            IF ( DOM%MLG%ARET_LIM(iare)%SOMMETS(2) == isom ) THEN
                SOM1 = SOMPOS0( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare)%SOMMETS(1), 1) , : )
            ELSE
                SOM1 = SOMPOS0( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare)%SOMMETS(2), 1) , : )
            END IF
            DEP = DEP + DOM%MLG%SOM_LIM(isom)%RAIDEUR(ivois) * ( SOM1 -  SOM0 )
            RAID = RAID + DOM%MLG%SOM_LIM(isom)%RAIDEUR(ivois)                
        END DO
        DEP = DEP/RAID

        IF ( ABS( NORM(DEP) ) < 100*eps_min ) THEN 
            CYCLE
        ELSE
            ! On trouve les deux aretes dont le produit scalaire est le plus proche 
            ! et on projette le déplacement total sur la face qu'elles délimitent
            NORM_SOM = DOM%MLG%SOM_LIM(isom)%NORMALES(1,:)
            ! On utilise DEP_MEM pour toujours considérer le déplacement depuis le sommet initial
            ! Cela nous permet de rester sur les surfaces (aux erreurs numériques près...)
            DIR = DEP / NORM(DEP)
            PS = DOT_PRODUCT( NORM_SOM , DIR )
            A_NORM = ACOS(PS)
            LONG_DEP = SIN(A_NORM)*NORM(DEP)
            !
            CALL find_face_lim_som ( DOM , isom , DEP , proj_face , coord_tri_P , proj_arete , proj_arete2 )
            !
            ! On récupère ARET1
            IF ( DOM%MLG%ARET_LIM(proj_arete)%SOMMETS(1) == isom ) THEN
                idsomvois = DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(proj_arete)%SOMMETS(2), 1 )
                SOM1 = SOMPOS0( idsomvois, : )            
                ARET1 = SOM1 - SOM0
                ARET1 = ARET1 / NORM(ARET1)
            ELSE IF ( DOM%MLG%ARET_LIM(proj_arete)%SOMMETS(2) == isom ) THEN
                idsomvois = DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(proj_arete)%SOMMETS(1), 1)
                SOM1 = SOMPOS0( idsomvois, : )
                ARET1 = SOM1 - SOM0
                ARET1 = ARET1 / NORM(ARET1)
            ELSE
                CALL print_err('Pas normal ...',1)
            END IF
            ! puis ARET2
            IF ( DOM%MLG%FAC_LIM(proj_face)%nb_som == 3 ) THEN
                DO iarete = 1, 3
                    idarete = DOM%MLG%FAC_LIM(proj_face)%ARETES(iarete)
                    IF ( idarete == proj_arete ) CYCLE
                    IF ( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(idarete)%SOMMETS(1), 1 ) == id_som ) THEN
                        idsomvois = DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(idarete)%SOMMETS(2), 1 ) 
                        SOM1 = SOMPOS0( idsomvois,: )
                        ARET2 = SOM1 - SOM0
                        ARET2 = ARET2 / NORM(ARET2)
                    ELSE IF ( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(idarete)%SOMMETS(2), 1 ) == id_som ) THEN
                        idsomvois = DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(idarete)%SOMMETS(1), 1 )
                        SOM1 = SOMPOS0( idsomvois, : )
                        ARET2 = SOM1 - SOM0
                        ARET2 = ARET2 / NORM(ARET2)
                    END IF  
                END DO
            ELSE IF ( DOM%MLG%FAC_LIM(proj_face)%nb_som == 4 ) THEN
                IF ( DOM%MLG%ARET_LIM(proj_arete2)%SOMMETS(1) == isom ) THEN
                    idsomvois = DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(proj_arete2)%SOMMETS(2), 1 )
                    SOM1 = SOMPOS0( idsomvois, : )
                    ARET2 = SOM1 - SOM0
                    ARET2 = ARET2 / NORM(ARET2)
                ELSE IF ( DOM%MLG%ARET_LIM(proj_arete2)%SOMMETS(2) == isom ) THEN
                    idsomvois = DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(proj_arete2)%SOMMETS(1), 1 )
                    SOM1 = SOMPOS0( idsomvois, : )
                    ARET2 = SOM1 - SOM0
                    ARET2 = ARET2 / NORM(ARET2)
                ELSE
                    CALL print_err('Pas normal ...',1)
                END IF                
            END IF
            !
            ! Vecteur normal à la face
            N_DIR = CROSS( ARET1 , ARET2 )
            N_DIR = N_DIR / NORM(N_DIR)
            lambda = - ( N_DIR(1)*DEP(1) + N_DIR(2)*DEP(2) + N_DIR(3)*DEP(3) ) / ( N_DIR(1)**2 + N_DIR(2)**2 + N_DIR(3)**2 )
            ! Direction du deplacement projeté sur la face
            ARET1 = (/ DEP(1) + lambda*N_DIR(1) , DEP(2) + lambda*N_DIR(2) , DEP(3) + lambda*N_DIR(3) /)
            ARET1 = ARET1 / NORM(ARET1)

            ! LIMITATION DU DEPLACMENT DES SOMMETS LIMITES
            DEP = LONG_DEP*ARET1
            N_DIR = DEP_MEM( isom , : ) - DEP
            DO ivois = 1, DOM%MLG%SOM_LIM(isom)%nvoisin
                iare = DOM%MLG%SOM_LIM(isom)%ARETES(ivois)
                IF ( DOM%MLG%ARET_LIM(iare)%SOMMETS(2) == isom ) THEN
                    SOM1 = SOMPOS0( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare)%SOMMETS(1), 1) , : )
                ELSE
                    SOM1 = SOMPOS0( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare)%SOMMETS(2), 1) , : )
                END IF
                DIR = SOM1 - SOM0
                dist1 = NORM(DIR)
                DIR = SOM1 - ( SOM0 + DEP )
                dist2 = NORM(DIR)
                IF ( ABS(dist1-dist2) < 1E-10 ) CYCLE
                DOM%MLG%SOM_LIM(isom)%RAIDEUR(ivois) = MAX(DOM%MLG%SOM_LIM(isom)%RAIDEUR(ivois) - SIN(A_NORM)*(dist1 - dist2) / dist1 , 0.1*DOM%MLG%SOM_LIM(isom)%RAIDEUR(ivois))
                taux_def = MAX( taux_def , NORM(N_DIR) / dist1 )
            END DO   
            DEP_MEM( isom , : )  = DEP
        END IF
    END DO
END DO
!
DEALLOCATE( SOMPOS0 )
DEALLOCATE( DEP_MEM )
!
END SUBROUTINE deform3D_ntag_init_raideur
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Avant la premiere iteration, on fait converger pour chaque sommet
! interne son systeme masse ressort afin que le laplacien ne modifie pas
! le maillage initial
! A la fin de la routine, on a les bonnes raideurs de ressort 
SUBROUTINE deform3D_int_init_raideur ( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: isom, ivois, id_som, iare, iiter, nsom_int
REAL(DP) :: RAID, taux_def, tol_def, dist1, dist2
REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: SOMPOS0
REAL(DP), DIMENSION(3) :: DEP, SOM0, SOM1, DIR
!
ALLOCATE( SOMPOS0( DOM%MLG%nsom, 3 ) )
SOMPOS0 = DOM%MLG%SOMMET
!
nsom_int = DOM%MLG%nsom_int
tol_def = DOM%NUM%tol_def
!
taux_def = 1.0
iiter = 0
DO WHILE ( taux_def > tol_def ) 
    iiter = iiter + 1
    taux_def = 0.0_dp
    DO isom = 1, nsom_int

        id_som = DOM%MLG%SOM_ADJ_INT( isom, 1 )
        DEP = 0.0_dp
        RAID = 0.0_dp
        SOM0 = SOMPOS0( id_som, : )

        DO ivois = 1, DOM%MLG%SOM_INT(isom)%nvoisin
            iare = DOM%MLG%SOM_INT(isom)%ARETES(ivois)
            IF ( DOM%MLG%ARET_INT(iare)%SOMMETS(2) == id_som ) THEN
                SOM1 = SOMPOS0( DOM%MLG%ARET_INT(iare)%SOMMETS(1), : )
            ELSE
                SOM1 = SOMPOS0( DOM%MLG%ARET_INT(iare)%SOMMETS(2), : )
            END IF
            DEP = DEP + DOM%MLG%SOM_INT(isom)%RAIDEUR(ivois) * ( SOM1 - SOM0 )
            RAID = RAID + DOM%MLG%SOM_INT(isom)%RAIDEUR(ivois)
        END DO
        DEP = DEP / RAID

        IF ( ABS( NORM(DEP) ) < 100*eps_min ) THEN
            CYCLE
        ELSE
            ! On limite le deplacement des sommets int au deplacement max des sommets limites ablatés avant equilibrage
            ! Cela permet de ne pas avoir un reequilibrage trop brutal des sommets int
            DO ivois = 1, DOM%MLG%SOM_INT(isom)%nvoisin
                iare = DOM%MLG%SOM_INT(isom)%ARETES(ivois)
                IF ( DOM%MLG%ARET_INT(iare)%SOMMETS(2) == id_som ) THEN
                    SOM1 = SOMPOS0( DOM%MLG%ARET_INT(iare)%SOMMETS(1), : )
                ELSE
                    SOM1 = SOMPOS0( DOM%MLG%ARET_INT(iare)%SOMMETS(2), : )
                END IF
                DIR = SOM1 - SOM0
                dist1 = NORM(DIR)
                DIR = SOM1 - ( SOM0 + DEP )
                dist2 = NORM(DIR)
                IF ( ABS(dist1-dist2) < 0.01*tol_def ) CYCLE
                DOM%MLG%SOM_INT(isom)%RAIDEUR(ivois) = MAX(DOM%MLG%SOM_INT(isom)%RAIDEUR(ivois) - (dist1 - dist2) / dist1 , 0.1*DOM%MLG%SOM_INT(isom)%RAIDEUR(ivois))
                taux_def = MAX( taux_def , ABS(dist1 - dist2) / dist1 )
            END DO            
        END IF
    END DO
END DO
!
DEALLOCATE( SOMPOS0 )
!
END SUBROUTINE deform3D_int_init_raideur
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Trouve la face limite la "plus proche" d'un sommet limite déplacé
SUBROUTINE find_face_lim_som ( DOM , isom , reg_som , proj_face , coord_tri_P, proj_arete, proj_arete2 )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: isom, ifac_vois, proj_arete, proj_arete2, idfac_vois, proj_face, iarete, idarete, id_som_vois, id_som
INTEGER(IP) :: ii, isomvois, iproj_face, id_proj_face
REAL(DP) :: dist, PS
REAL(DP), DIMENSION(3) :: SOM_D, reg_som, coord_tri_P, cross_vect, second_terme3, SOM1, SOM2, SOM12, SOM13
REAL(DP), DIMENSION(3,3) :: mat_verif
LOGICAL :: notfound, first
!
! On cherche sur quelle face projeter le sommmet déplacé
id_som = DOM%MLG%SOMLIM2FACLIM( isom, 1 )
SOM1 = DOM%MLG%SOMMET( id_som, : )
SOM_D = SOM1 + reg_som 
proj_arete = 0
proj_arete2 = 0
proj_face = 0
first = .TRUE.
dist = 0.0_dp
PS = 0.0_dp
!
! On regarde le produit scalaire entre le déplacement et les aretes
DO iarete = 1, DOM%MLG%SOM_LIM(isom)%nvoisin
    idarete = DOM%MLG%SOM_LIM(isom)%ARETES(iarete)
    IF ( DOM%MLG%ARET_LIM(idarete)%SOMMETS(1) == isom ) THEN
        id_som_vois = DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(idarete)%SOMMETS(2), 1 )
        SOM2 = DOM%MLG%SOMMET( id_som_vois, : )
    ELSE
        id_som_vois = DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(idarete)%SOMMETS(1), 1 )
        SOM2 = DOM%MLG%SOMMET( id_som_vois, : )
    END IF
    SOM2 = SOM2 - SOM1
    SOM2 = SOM2 / NORM(SOM2)
    PS = DOT_PRODUCT( reg_som, SOM2 )
    IF ( PS > dist .OR. first ) THEN 
        first = .FALSE.
        dist = PS
        proj_arete = idarete
        SOM12 = SOM2
    END IF
END DO
!
! Le sommet est deja sur la face, on ne peut pas calculer la distance du sommet à la face
! On cherche le triangle ou rectangle où se trouve le sommet déplacé
!
notfound = .TRUE.
DO ifac_vois = 1, DOM%MLG%SOM_LIM(isom)%nvoisin
    IF ( ANY( DOM%MLG%FAC_LIM( DOM%MLG%SOM_LIM(isom)%FACES(ifac_vois) )%ARETES == proj_arete ) .AND. notfound ) THEN
        idfac_vois = DOM%MLG%FACLIM( DOM%MLG%SOM_LIM(isom)%FACES(ifac_vois), 2 )
        proj_face = DOM%MLG%SOM_LIM(isom)%FACES(ifac_vois)
        id_proj_face = DOM%MLG%ID_FACLIM(proj_face)
        IF ( DOM%MLG%FAC_LIM(proj_face)%nb_som == 3 ) THEN
            ! Cas triangle : on exprime les coordonnées du sommet dans le repere du triangle
            ! Si u1,u2,u3 > 0 et u1+u2+u3==1 alors c'est la bonne face
            IF ( .NOT. DOM%MLG%FAC_LIM(proj_face)%matrice_upd ) THEN
                CALL ablation_matrices_passage( DOM , proj_face )
                DOM%MLG%FAC_LIM(proj_face)%matrice_upd = .TRUE.
            END IF
            CALL ablation_coord_loc( DOM , proj_face , SOM_D , coord_tri_P )
            IF ( coord_tri_P(1) > eps_min .AND. coord_tri_P(2) > eps_min .AND. coord_tri_P(3) > eps_min .AND. ABS(coord_tri_P(1) + coord_tri_P(2) + coord_tri_P(3) - 1) < 1E-10 ) THEN
                PS = DOT_PRODUCT( DOM%ETAT%D_SOM_BRUT( id_som, : ) , DOM%MLG%VFAC( id_proj_face, : ) )
                IF ( PS > eps_min ) notfound = .FALSE.
            END IF
        ELSE IF ( DOM%MLG%FAC_LIM(proj_face)%nb_som == 4 ) THEN
            ! Cas rectangle : on exprime les coordonnées du point déplacé en fonction
            ! des 2 aretes, avec origine le sommet limite avant déplacement
            ! Si on a 2 coordonnées positives, le sommet déplacé appartient a la face en question
            DO iarete = 1, 4 ! on boucle sur les aretes de la face pour trouver le sommet limite
                idarete = DOM%MLG%FAC_LIM(proj_face)%ARETES(iarete)
                IF ( idarete == proj_arete ) CYCLE 
                SOM13 = 0.0_dp
                IF ( DOM%MLG%ARET_LIM(idarete)%SOMMETS(1) == isom ) THEN
                    SOM13 = DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(idarete)%SOMMETS(2), 1 ) , : ) - DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(idarete)%SOMMETS(1) , 1 ) , : )
                    EXIT
                ELSE IF (DOM%MLG%ARET_LIM(idarete)%SOMMETS(2) == isom) THEN
                    SOM13 = DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(idarete)%SOMMETS(1), 1 ) , : ) - DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(idarete)%SOMMETS(2) , 1 ) , : )
                    EXIT
                END IF
                IF ( iarete == 4 ) CALL print_err('Les 2 aretes n''ont pas été trouvées',1)
            END DO
            ! On calcule les coordonnées du sommet déplacé dans le repère SOM12,SOM13
            SOM13 = SOM13 / NORM(SOM13)
            cross_vect = cross( SOM12 , SOM13 )
            mat_verif(:,1) = SOM12
            mat_verif(:,2) = SOM13
            mat_verif(:,3) = cross_vect
            second_terme3 = reg_som
            second_terme3 = second_terme3 / NORM(second_terme3)
            mat_verif = INV_MAT3( mat_verif )
            second_terme3 = MATMUL( mat_verif , second_terme3 )
            IF ( second_terme3(1) > -1E-8 .AND. second_terme3(2) > -1E-8 ) THEN 
                PS = DOT_PRODUCT( DOM%ETAT%D_SOM_BRUT(id_som,:) , DOM%MLG%VFAC(id_proj_face,:) )
                IF ( PS > eps_min .OR. DOM%RSL%equilibrage ) THEN
                    proj_arete2 = idarete
                    notfound = .FALSE.
                END IF
            END IF
        ELSE
            CALL print_err('Une face limite n''a pas 3 ou 4 aretes',1)
        END IF
    END IF
END DO
!
IF ( ifac_vois == DOM%MLG%SOM_LIM(isom)%nvoisin + 1 .AND. notfound ) THEN
    ! Procédure de secours si on trouve de face
    ! On récupère la face avec le produit scalaire le plus grand entre le déplacement et :
    ! - la diagonale si quad
    ! - l'autre arete si tri
    dist = 0.0_dp
    PS = 0.0_dp
    first = .TRUE.

    DO ifac_vois = 1, DOM%MLG%SOM_LIM(isom)%nvoisin
        IF ( ANY( DOM%MLG%FAC_LIM( DOM%MLG%SOM_LIM(isom)%FACES(ifac_vois) )%ARETES == proj_arete ) .AND. notfound ) THEN
            idfac_vois = DOM%MLG%FACLIM( DOM%MLG%SOM_LIM(isom)%FACES(ifac_vois), 2 )
            iproj_face = DOM%MLG%SOM_LIM(isom)%FACES(ifac_vois)
            IF ( DOM%MLG%FAC_LIM(iproj_face)%nb_som == 3 ) THEN
                DO iarete = 1, DOM%MLG%FAC_LIM(iproj_face)%nb_som
                    idarete = DOM%MLG%FAC_LIM(iproj_face)%ARETES(iarete)
                    IF ( idarete == proj_arete ) CYCLE 
                    IF ( DOM%MLG%ARET_LIM(idarete)%SOMMETS(1) == isom ) THEN
                        SOM13 = DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(idarete)%SOMMETS(2), 1 ) , : ) - DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(idarete)%SOMMETS(1), 1 ) , : )
                        EXIT
                    ELSE IF (DOM%MLG%ARET_LIM(idarete)%SOMMETS(2) == isom) THEN
                        SOM13 = DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(idarete)%SOMMETS(1), 1 ) , : ) - DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(idarete)%SOMMETS(2), 1 ) , : )
                        EXIT
                    END IF
                END DO
                PS = DOT_PRODUCT( SOM13 , reg_som )
                IF ( PS > dist .OR. first ) THEN
                    first = .FALSE.
                    dist = PS
                    proj_arete2 = idarete
                    proj_face = iproj_face 
                END IF
            ELSE IF ( DOM%MLG%FAC_LIM(iproj_face)%nb_som == 4 ) THEN
                DO ii = 1, DOM%MLG%FAC_LIM(iproj_face)%nb_som
                    IF ( DOM%MLG%FAC_LIM(iproj_face)%SOMMETS(ii) == isom ) THEN
                        isomvois = DOM%MLG%FAC_LIM(iproj_face)%SOMMETS( MODULO(ii+1,4) + 1 )
                        EXIT
                    END IF
                END DO
                SOM13 = DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM(isomvois,1) , : )
                IF ( PS > dist .OR. first ) THEN
                    first = .FALSE.
                    dist = PS
                    proj_arete2 = idarete
                    proj_face = iproj_face 
                END IF
            END IF
        END IF
    END DO
    IF ( proj_arete2 /= 0 ) notfound = .FALSE.
    IF ( DOM%MLG%FAC_LIM(iproj_face)%nb_som == 3 ) CALL ablation_coord_loc( DOM , proj_face , SOM_D , coord_tri_P )
END IF
!
IF ( notfound ) CALL print_err('Aucune face trouvée pour projeter le sommet',1)
!
END SUBROUTINE find_face_lim_som
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> INFO SUBROUTINE
SUBROUTINE find_voisins_ntag2 ( DOM , isom , iare1 , iare2 , SVOIS_NTAG3 )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: isom, ii, ivois1, ivois2, iare1, iare2
REAL(DP) :: PS, PS_ref
REAL(DP), DIMENSION(3) :: SOM, SOM1, SOM2, VEC, VEC_ref
LOGICAL :: SVOIS_NTAG3
!
SVOIS_NTAG3 = .FALSE.
ivois1 = -1
ivois2 = -1
SOM = DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( isom,1 ) , : )

! On récupère les deux sommets voisins sur la "courbe limite" qui sépare les tags différents
DO ii = 1, DOM%MLG%SOM_LIM(isom)%nvoisin
    IF ( DOM%MLG%SOM_LIM(isom)%NBTAG_ARET_SOM(ii) >= 1 ) THEN
        IF ( ivois1 == -1 ) THEN
            iare1 = DOM%MLG%SOM_LIM(isom)%ARETES(ii)
            ivois1 = ii
        ELSE IF ( ivois2 == -1 ) THEN
            iare2 = DOM%MLG%SOM_LIM(isom)%ARETES(ii)
            ivois2 = ii
        ELSE 
            ! Notre sommet a plus de 2 sommets limites voisins avec Ntag >= 2
            ! C'est un sommet type "coin"
            ! Pour équilibrer ce sommet, on va utiliser son sommet voisin à Ntag > 2
            ! et le sommet sommmet voisin à Ntag == 2 le plus aligner avec la 1ere arete
            SVOIS_NTAG3 = .TRUE.
        END IF
    END IF
END DO
IF ( SVOIS_NTAG3 ) THEN
    ivois1 = -1
    ivois2 = -1
    SOM1 = 0.0_dp
    SOM2 = 0.0_dp
    ! 1ere boucle pour avoir le sommet Ntag == 3
    DO ii = 1, DOM%MLG%SOM_LIM(isom)%nvoisin
        IF ( DOM%MLG%SOM_LIM(isom)%NBTAG_ARET_SOM(ii) > 1 ) THEN
            SVOIS_NTAG3 = .FALSE.
            iare1 = DOM%MLG%SOM_LIM(isom)%ARETES(ii)
            ivois1 = ii
            IF ( DOM%MLG%ARET_LIM(iare1)%SOMMETS(2) == isom ) THEN
                SOM1 = DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare1)%SOMMETS(1), 1 ) , : ) 
            ELSE
                SOM1 = DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare1)%SOMMETS(2), 1 ) , : ) 
            END IF
        END IF
    END DO
    IF ( .NOT. SVOIS_NTAG3 ) THEN
        ! 2eme boucle pour trouver le "bon" sommet à Ntag == 2
        PS = 0.0_dp
        PS_ref = -10.0
        VEC = 0.0_dp
        VEC_ref = SOM - SOM1
        VEC_ref = VEC_ref / NORM(VEC_ref)
        DO ii = 1, DOM%MLG%SOM_LIM(isom)%nvoisin
            IF ( DOM%MLG%SOM_LIM(isom)%NBTAG_ARET_SOM(ii) == 1 ) THEN     
                IF (DOM%MLG%ARET_LIM(DOM%MLG%SOM_LIM(isom)%ARETES(ii))%SOMMETS(2) == isom) THEN
                    VEC = DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM( DOM%MLG%SOM_LIM(isom)%ARETES(ii) )%SOMMETS(1), 1) , : )
                ELSE
                    VEC = DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM( DOM%MLG%SOM_LIM(isom)%ARETES(ii) )%SOMMETS(2), 1) , : )
                END IF     
                VEC = VEC - SOM
                VEC = VEC / NORM(VEC)
                PS = DOT_PRODUCT( VEC_ref , VEC )
                IF ( PS > PS_ref ) THEN
                    ! Alors la seconde arete est celle ci (en attendant le PS suivant)
                    iare2 = DOM%MLG%SOM_LIM(isom)%ARETES(ii)
                    ivois2 = ii
                    PS_ref = PS
                END IF
            END IF
        END DO
    ELSE
        write(6,*) isom
        write(6,*) SOM(1),SOM(2),SOM(3)
        CALL print_err('pas encore fait',1) ! faire un cas pour quand un sommet tag2 est lié a au moins 3 tag2
    END IF
END IF
!
END SUBROUTINE find_voisins_ntag2
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> INFO SUBROUTINE
! SUBROUTINE template (  )
! !
! !---- Déclarations ----------------------------------------------------!
! INTEGER(IP) ::
! REAL(DP) ::


! !
! END SUBROUTINE template
!----------------------------------------------------------------------!

!---------------------------------------------------------------------!
!  Function Polynome d'Hermite
!
!      Compute the 4 Hermite polynomial
!
!  Input Variable:
!      -t  : input scalar
!
!  Output Variable:
!      -H    : matrix with all four polynomial values and its derivatives
!---------------------------------------------------------------------!
FUNCTION hermite(t) RESULT (H)
    !
    !=== arguments ===
    REAL(KIND=DP), INTENT(in) :: t
    REAL(KIND=DP), DIMENSION(2,4) :: H
    !
    H(:,:) = 0.0_dp
    H(1,1) = 2*(t**3) - 3*(t**2) + 1
    H(1,2) = t**3 - 2*(t**2) + t
    H(1,3) = t**3 - t**2
    H(1,4) = 1 - (2*(t**3) - 3*(t**2) + 1)
    H(2,1) = 6*(t**2) - 6*t
    H(2,2) = 3*(t**2) - 4*t + 1
    H(2,3) = 3*(t**2) - 2*t
    H(2,4) = -(6*(t**2) - 6*t)
    !
    RETURN
    !
END FUNCTION hermite
!---------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_deform3d
