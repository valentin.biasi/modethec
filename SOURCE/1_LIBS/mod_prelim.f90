!> Module d'entree du programme : Existance et creation des fichiers
MODULE mod_prelim
!
USE mod_cst, ONLY : IP, DP, output_file_name, error_file_name, output_file_id, error_file_id
USE mod_print, ONLY : print_prelim, print_err
USE mod_algebra, ONLY : inttostr
!
USE, INTRINSIC :: iso_fortran_env, ONLY : output_unit, error_unit
USE MPI
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Fonction préliminaire a l'ensemble des calculs
!> ID des fichiers entrees/sorties
!> 1 : Fichier parametres
!> 2 : Fichier maillage
!> 3 : Fichier export
!> 4 : Log output
!> 5 : Log error
SUBROUTINE prelim( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
CHARACTER(len=32) :: file_param
CHARACTER (len=128) :: dir
CHARACTER (len=32) :: nom_var
INTEGER(IP) :: nligne, EOF, iligne
CHARACTER (len=1) :: egal
LOGICAL :: file_exist
INTEGER :: ierr
CHARACTER (len=512) :: ligne

nligne = 0
EOF = 0

DOM%RSL%total_CPU_time = 0.0_dp ! Initialisation du temps CPU total

!---- Récupération du nom de fichier paramètres -----------------------!
CALL GETARG(1, file_param)
DOM%file_param = TRIM(file_param)

!---- Répertoire courant ----------------------------------------------!
CALL GETCWD( dir )
DOM%dir = TRIM(dir)

!---- Nom utilisateur : login -----------------------------------------!
CALL GETLOG( DOM%logname )
!---- Nom station -----------------------------------------------------!
DOM%hostname = " "
CALL HOSTNM( DOM%hostname )


!---- Test existance du fichier ---------------------------------------!
IF (LEN(TRIM(file_param)) == 0) THEN
    CALL print_err('No input parameter file, usage is: modethec input_file.prm', 1)
END IF
INQUIRE(FILE=file_param, EXIST=file_exist)
IF (.NOT.(file_exist)) THEN
    CALL print_err('Parameter file "'//TRIM(file_param)//'" does not exist in "'//TRIM(DOM%dir)//'"', 1)
END IF


!---- Ouverture de file_param -----------------------------------------!
OPEN(1, FILE=DOM%file_param, &
 STATUS='old', ACTION='read')
    

!---- Nombre de lignes de file_param ----------------------------------!
DO WHILE ( EOF == 0 )
 READ( 1,*, IOSTAT=EOF )
 nligne = nligne + 1
END DO
DOM%nl_param = nligne-1
REWIND(1)  ! Remontée au début de fichier

!---- Boucle sur toutes les lignes ------------------------------------!
DO iligne = 1, DOM%nl_param
 READ(1,*) nom_var  ! Nom de la variable à récupérer
 
 ! Cas suivant le mot clé associé
 SELECT CASE (nom_var)
  
  ! Nom de l'auteur du cas
  CASE ("auteur")
   BACKSPACE(1)
   READ(1,*) nom_var, egal, DOM%auteur

  ! No de la version
  CASE ("version")
   BACKSPACE(1)
   READ(1,*) nom_var, egal, DOM%version
 
  ! Fichier maillage associé
  CASE ("file_mesh")
   BACKSPACE(1)
   READ(1,*) nom_var, egal, DOM%file_mesh
            
  ! Dimension de simulation
  CASE ("dim_simu")
   BACKSPACE(1)
   READ(1,*) nom_var, egal, DOM%MLG%dim_simu
            
  ! Dimension de simulation
  CASE ("DIR_PLAN")
   BACKSPACE(1)
   READ(1,*) nom_var, egal, DOM%MLG%DIR_PLAN(1:3)
            
  ! Dimension de simulation
  CASE ("AXE_2D")
   BACKSPACE(1)
   READ(1,*) nom_var, egal, DOM%MLG%AXE_2D(1:6)
            
  ! Fichiers log
  CASE ("file_log")
   BACKSPACE(1)
   READ(1,*) nom_var, egal, DOM%SUI%file_log
        
  ! Fichiers log
  CASE ("suivi")
   BACKSPACE(1)
   READ(1,*) nom_var, egal, DOM%SUI%suivi
   
  ! Diffusion active
  CASE ("DIFFUSION")
   BACKSPACE(1)
   READ(1,*) nom_var, egal, DOM%NUM%DIFFUSION
     
  ! Advection active
  CASE ("ADVECTION")
   BACKSPACE(1)
   READ(1,*) nom_var, egal, DOM%NUM%ADVECTION
      
  ! Reaction active
  CASE ("REACTION")
   BACKSPACE(1)
   READ(1,*) nom_var, egal, DOM%NUM%REACTION
      
  ! Structure active
  CASE ("STRUCTURE")
   BACKSPACE(1)
   READ(1,*) nom_var, egal, DOM%NUM%STRUCTURE
      
  ! Ablation active
  CASE ("ABLATION")
   BACKSPACE(1)
   READ(1,*) nom_var, egal, DOM%NUM%ABLATION
   
  ! Couplage actif
  CASE ("COUPLAGE")
   BACKSPACE(1)
   READ(1,*) nom_var, egal, DOM%NUM%COUPLAGE

                 
 END SELECT
END DO

REWIND(1) ! Remontée au début du fichier


!---- Rang et dimension de la communication MPI -----------------------!
CALL MPI_COMM_RANK(MPI_COMM_WORLD, DOM%irank, ierr)
CALL MPI_COMM_SIZE(MPI_COMM_WORLD, DOM%comm_size, ierr)

!---- Nom des fichiers logs -------------------------------------------!
IF (DOM%comm_size == 1) THEN
    DOM%SUI%error_file_name = TRIM(error_file_name)
    DOM%SUI%output_file_name = TRIM(output_file_name)
ELSE
    DOM%SUI%error_file_name = TRIM(error_file_name)//'.'//ADJUSTL(inttostr(DOM%irank))
    DOM%SUI%output_file_name = TRIM(output_file_name)//'.'//ADJUSTL(inttostr(DOM%irank))
END IF


!--- Gestion des fichiers log (output et error) -----------------------!
!--- Création et ouverture --------------------------------------------!
IF (DOM%SUI%file_log == 1) THEN

    ! Création du fichier log avec tout dedans
    OPEN(output_file_id, FILE=DOM%SUI%output_file_name, STATUS='replace', ACTION='write')
    ! Et du fichier erreur avec rien dedans... ou pas 
    OPEN(error_file_id, FILE=DOM%SUI%error_file_name, STATUS='replace', ACTION='write')

    ! Recopie du fichier de paramètres
    DO iligne = 1, DOM%nl_param
        READ(1,'(A)') ligne
        WRITE(output_file_id,'(A)') TRIM(ligne)
    END DO

END IF 

CLOSE(1) ! Fermeture du fichier

! Print ecran des infos prelim
CALL print_prelim( DOM )

END SUBROUTINE prelim
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_prelim
