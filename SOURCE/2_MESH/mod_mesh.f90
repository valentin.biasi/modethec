!> Module de reconstruction du maillage et des données associées
MODULE mod_mesh
!
USE mod_print, ONLY : print_mesh_read, print_mesh_bld, print_mesh_cpt
USE mod_mesh_read, ONLY : mesh_read
USE mod_mesh_bld, ONLY : mesh_bld
USE mod_mesh_cpt, ONLY : mesh_cpt
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Fonction de reconstruction du maillage et des données associées
SUBROUTINE mesh( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM

!----------------------------------------------------------------------!
! --> Phase 1 : Lecture du fichier de maillage
CALL mesh_read( DOM )
CALL print_mesh_read( DOM )

!----------------------------------------------------------------------!
! --> Phase 2 : Relations entre les composantes du maillage
CALL mesh_bld( DOM )
CALL print_mesh_bld( DOM )

!----------------------------------------------------------------------!
! --> Phase 3 : Calculs propres au maillage (aires, volumes,...)
CALL mesh_cpt( DOM )
CALL print_mesh_cpt( DOM )

END SUBROUTINE mesh
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh
