!> Module contenant les appels aux routines de lecture 
!> et de pre-traitement du maillage
!> au format CEDRE (*.dat) ou GMSH (*.msh)
MODULE mod_mesh_read
!
USE mod_cst, ONLY : DP, IP, eps_min
USE mod_print, ONLY : print_err
USE mod_mesh_read_dat, ONLY : mesh_read_dat
USE mod_mesh_read_msh, ONLY : mesh_read_msh
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Phase de lecture du fichier de maillage
SUBROUTINE mesh_read( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: idx_ext
LOGICAL :: file_exist


! Test existance du fichier
INQUIRE( FILE=DOM%file_mesh, EXIST=file_exist)
IF (.NOT.(file_exist)) THEN
    CALL print_err('Le fichier maillage '//TRIM(DOM%file_mesh)//' n''existe pas',1)
END IF


!---- Cas maillage CEDRE ----------------------------------------------!
idx_ext = INDEX(DOM%file_mesh,".dat")
IF (idx_ext /= 0) THEN
    CALL mesh_read_dat( DOM )
END IF

!---- Cas maillage GMSH -----------------------------------------------!
idx_ext = INDEX(DOM%file_mesh,".msh")
IF (idx_ext /= 0) THEN
    CALL mesh_read_msh( DOM )
END IF

!---- Cas maillage ...

!
END SUBROUTINE mesh_read
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_read
