!> Module de lecture du maillage au format CEDRE (*.dat)
MODULE mod_mesh_read_dat
!
USE mod_cst, ONLY : DP, IP, eps_min
USE mod_print, ONLY : print_err
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Lecture du maillage au format CEDRE (*.dat)
!> (Pas de pré-traitement dans cette routine, lecture seulement)
!> Attention : un seul domaine géré dans cette version
SUBROUTINE mesh_read_dat( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: ndom, nsom, nfac, ncel, nfac_lim
INTEGER(IP) :: isom, ifac, i
INTEGER(IP) :: ncel_fac, nsom_fac
REAL(DP) :: trash

!---- Ouverture de file_mesh ------------------------------------------!
OPEN(2, FILE=DOM%file_mesh, &
 STATUS='old', ACTION='read' )
    
!---- Nombre de domaines ----------------------------------------------!
READ(2,*) ndom
! Test de bonne lecture
IF (ndom <= 0) THEN
    CALL print_err('Le nombre de domaines ne peut etre lu dans '//DOM%file_mesh)
END IF

! Passage de l'en-tête du domaine
READ(2,*) ; READ(2,*)

! Nom courant du domaine
READ(2,*) DOM%nom_dom

! Espace de simulation
READ(2,*) DOM%MLG%dim_simu

! Plan d'invariance (2D_PLAN) ou Axe de symétrie (2D_AXI)
SELECT CASE (DOM%MLG%dim_simu)
    CASE ("2D_PLAN", "1D")
        READ(2,*) DOM%MLG%DIR_PLAN
        IF ((DOM%MLG%DIR_PLAN(1) .ne. 0.0_dp) .or. (DOM%MLG%DIR_PLAN(2) .ne. 0.0_dp)) THEN
                CALL print_err('MoDetheC ne traite pas les cas 2D plans invariants suivant Z')
                CALL print_err('Veuillez modifier le fichier de maillage')
        END IF
        
    CASE ("2D_AXI")
        READ(2,*) DOM%MLG%AXE_2D
        
    CASE ("3D")
        ! Rien
        
    CASE DEFAULT
            CALL print_err('MoDetheC ne traite pas la dimension de simulation '//DOM%MLG%dim_simu)
END SELECT

! Echelle du domaine
READ(2,*) DOM%MLG%ech

! Nombre de sommets
READ(2,*) nsom
DOM%MLG%nsom = nsom

! Nombre de faces
READ(2,*) nfac
DOM%MLG%nfac = nfac

! Nombre de cellules
READ(2,*) ncel
DOM%MLG%ncel = ncel

! Nombre de faces limites et internes
READ(2,*) nfac_lim
DOM%MLG%nfac_lim = nfac_lim
DOM%MLG%nfac_int = DOM%MLG%nfac - DOM%MLG%nfac_lim

! Allocations des tableaux
ALLOCATE( DOM%MLG%SOMMET( nsom,3 ) )
ALLOCATE( DOM%MLG%FAC( nfac,6 ) )
ALLOCATE( DOM%MLG%FAC2CEL( nfac,4 ) )
ALLOCATE( DOM%MLG%FACLIM( nfac_lim,2 ) )
ALLOCATE( DOM%MLG%FACLIM_MARQ( nfac_lim ) )    

! Position des sommets [no,x,y,z] :: SOMMET
READ(2,*) ! Ligne en-tete
DO isom = 1,nsom
    READ(2,*) trash, (DOM%MLG%SOMMET( isom,i ), i=1,3)
END DO

! Mise à l'échelle
DOM%MLG%SOMMET(:,:) = DOM%MLG%ech * DOM%MLG%SOMMET(:,:)


! Relations face/sommet [no_fac,nb_som,som1,som2,...] :: FAC
READ(2,*) ! Ligne en-tete
DO ifac = 1,nfac
    READ(2,*) trash, nsom_fac
    BACKSPACE(2)
    READ(2,*) (DOM%MLG%FAC( ifac,i ), i=1,2+nsom_fac)
    !print*, DOM%MLG%FAC( ifac,: )
END DO

! Relations face/cellule [no_fac,nb_cel,cel1,(cel2)] :: FAC2CEL
READ(2,*) ! Ligne en-tete
DO ifac = 1,nfac
    READ(2,*) trash, ncel_fac
    BACKSPACE(2)
    READ(2,*) (DOM%MLG%FAC2CEL( ifac,i ), i=1,ncel_fac+2)
END DO

! Relations face_lim/fac/id_fac :: FACLIM et FACLIM_MARQ
READ(2,*) ! Ligne en-tete
DO ifac = 1,nfac_lim
    READ(2,*) (DOM%MLG%FACLIM( ifac,i ), i=1,2), &
    DOM%MLG%FACLIM_MARQ(ifac)
END DO


CLOSE (2) ! Fermeture du fichier

! Allocation de CEL2DOM_MARQ
ALLOCATE( DOM%MLG%CEL2DOM_MARQ( ncel ) )
DOM%MLG%CEL2DOM_MARQ(:) = DOM%nom_dom

END SUBROUTINE mesh_read_dat
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_read_dat
