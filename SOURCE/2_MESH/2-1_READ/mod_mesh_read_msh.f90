!> Module de lecture du maillage au format GMSH (*.msh)
MODULE mod_mesh_read_msh
!
USE mod_cst, ONLY : DP, IP, eps_min
USE mod_print, ONLY : print_err
USE mod_algebra, ONLY : multiple_free
!
USE mod_dom
!
IMPLICIT NONE
!
!----------------------------------------------------------------------!
!> Type Ordre des faces dans format gmsh : a partir de la liste des sommets 
!> gmsh d'un element, on connait la composition des sommets des faces :
!> Exemple pour un tetraedre de sommet 1234, les faces sont 123, 124, 134, et 234
TYPE typ_fac_order_in_gmsh

    INTEGER(IP), DIMENSION(2,1) :: segment = (/ 1,2 /)
                                            
    INTEGER(IP), DIMENSION(2,3) :: tri = (/ 1,2, &
                                            2,3, &
                                            1,3 /)
    
    INTEGER(IP), DIMENSION(2,4) :: quad = (/ 1,2, &
                                             2,3, &
                                             3,4, &
                                             4,1 /)
                                            
    INTEGER(IP), DIMENSION(3,4) :: tetra = (/ 1,2,3, &
                                              1,2,4, &
                                              1,3,4, &
                                              2,3,4 /)
                                              
    INTEGER(IP), DIMENSION(4,6) :: hexa = (/ 1,2,3,4, &
                                             1,2,6,5, &
                                             1,4,8,5, &
                                             2,3,7,6, &
                                             3,4,8,7, &
                                             5,6,7,8 /)
        
    INTEGER(IP), DIMENSION(4,3) :: prism = (/ 1,2,5,4, &
                                              1,3,6,4, &
                                              2,3,6,5 /)
                                              
    INTEGER(IP), DIMENSION(3,2) :: prism_base = (/ 1,2,3, &
                                                   4,5,6 /)
                                                   
    INTEGER(IP), DIMENSION(3,4) :: pyram = (/ 1,2,5, &
                                              1,4,5, &
                                              2,3,5, &
                                              3,4,5 /)
                                        
    INTEGER(IP), DIMENSION(4,1) :: pyram_base = (/ 1,2,3,4 /)

END TYPE typ_fac_order_in_gmsh
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Type derive sommet : Liste temporaire des faces identifiees par sommet
TYPE typ_sommet
    
    INTEGER(IP) :: nfac_adj = 0                     ! nombre de faces identifiees
    INTEGER(IP), DIMENSION(4,80) :: list_fac = 0    ! Liste des sommets des faces identifiees
    INTEGER(IP), DIMENSION(80) :: list_id_fac = 0   ! Liste des identifiants des faces identifiees

END TYPE typ_sommet
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
CONTAINS
!
!
!> Lecture du maillage au format GMSH ASCII (*.msh)
!> (Pre-traitement dans cette routine pour obtenir les memes donnees
!> que les fichiers *.dat)
!> \todo Gestion des multi-domaines en .msh
SUBROUTINE mesh_read_msh( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: i_phys, n_phys
INTEGER(IP) :: isom, nsom
INTEGER(IP) :: i_elm, n_elm
INTEGER(IP) :: elm_typ, ntag
INTEGER(IP) :: i, nfac, nfac_max
INTEGER(IP) :: nfac_lim
INTEGER(IP) :: i_cel, ncel
REAL(DP) :: version
INTEGER(IP) :: file_type
REAL(DP) :: trash
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: phys_no
CHARACTER (len=32), ALLOCATABLE, DIMENSION(:) :: phys_name
INTEGER(IP), DIMENSION(8) :: SOM_i
INTEGER(IP), DIMENSION(4) :: FAC_i
INTEGER(IP) :: id_som, id_fac
INTEGER(IP) :: i_fac, n_fac, i_som_fac, n_som_fac, i_tab, i_faclim
TYPE(typ_fac_order_in_gmsh),TARGET :: fac_order
INTEGER(IP), POINTER, DIMENSION(:,:) :: tab_id_fac1, tab_id_fac2, tab_id_fac
LOGICAL :: new_fac, find_fac, new_cel
!
INTEGER(IP), ALLOCATABLE, DIMENSION(:,:) :: ELEMENTS, FAC, FAC2CEL
TYPE(typ_sommet), ALLOCATABLE, DIMENSION(:) :: sommet


!---- Ouverture de file_mesh ------------------------------------------!
OPEN(2, FILE=DOM%file_mesh, &
 STATUS='old', ACTION='read' )


! Passage de l'en-tête du fichier
READ(2,*)
READ(2,*)  version, file_type
READ(2,*)
IF (ABS(version - 2.2_dp) > 2.0_dp*eps_min) THEN
    CALL print_err('La version du fichier .msh ne peut etre lue')
    CALL print_err('avec la version actuelle de MoDeTheC',1)
END IF
IF (file_type /= 0) THEN
    CALL print_err('Le fichier .msh doit etre au format ASCII',1)
END IF
    
! Nom courant du domaine
DOM%nom_dom = DOM%file_mesh


!---- Vérification dim_simu est bien défini ---------------------------!
IF (TRIM(ADJUSTL(DOM%MLG%dim_simu)) == "") THEN
    CALL print_err('dim_simu doit etre defini dans le fichier')
    CALL print_err('.prm pour les maillages .msh',1)
END IF


! Nombre d'entites physiques
READ(2,*) ! en-tete
READ(2,*) n_phys

! Allocations des no et nom d'entites physiques
ALLOCATE( phys_no(n_phys) )
ALLOCATE( phys_name(n_phys) )
! et remplissage de ces tableaux
DO i_phys = 1,n_phys
    READ(2,*) trash, phys_no(i_phys), phys_name(i_phys)
END DO
READ(2,*)

! Nombre de sommets du maillage
READ(2,*) ! en-tete
READ(2,*) nsom
DOM%MLG%nsom = nsom

! Tableau des positions sommets
ALLOCATE( DOM%MLG%SOMMET(nsom,3) )
DO isom = 1,nsom
    READ(2,*) trash, (DOM%MLG%SOMMET( isom,i ), i=1,3)
END DO
READ(2,*)

! Nombre d'éléments = faces lim + cel internes (tri ou quad ou tetra ou hexa ou prisme ou pyra)
READ(2,*) ! en-tete
READ(2,*) n_elm
ALLOCATE( ELEMENTS(n_elm,15) )

! Tableau ELEMENTS du format .msh :
! id - elm_typ - ntag - tag1 - tag2 - ... - som1 - som2 - ...
DO i_elm = 1,n_elm
    READ(2,*) trash, elm_typ, ntag
    BACKSPACE(2)
    nsom = 0
    SELECT CASE (elm_typ)
        CASE(1) ! segment
            nsom = 2
        CASE(2) ! triangle
            nsom = 3
        CASE(3) ! quadrangle
            nsom = 4
        CASE(4) ! tetraedre
            nsom = 4
        CASE(5) ! hexaedre
            nsom = 8
        CASE(6) ! prisme
            nsom = 6
        CASE(7) ! pyramide
            nsom = 5
        CASE DEFAULT
            CALL print_err('Type d element non pris en charge lors de la lecture du fichier .msh.',1)
    ENDSELECT
    READ(2,*) (ELEMENTS( i_elm,i ), i=1,(3+ntag+nsom))
END DO
READ(2,*)

CLOSE (2) ! Fermeture du fichier


!----------------------------------------------------------------------!
!---- Fin de lecture
!---- Pre-traitement vers format type CEDRE

!---- 1ere boucle : compte les ncel, nfac_max (dimensionnement) et nfac_lim
nfac_max = 0
ncel = 0
nfac_lim = 0
DO i_elm = 1,n_elm
    elm_typ = ELEMENTS(i_elm,2)
    
    ! Elements de type 3D
    IF (DOM%MLG%dim_simu == '3D') THEN
        SELECT CASE (elm_typ)
            CASE(2) ! Type triangle = face limite
                nfac_lim = nfac_lim + 1
            CASE(3) ! Type quad = face limite
                nfac_lim = nfac_lim + 1
            CASE(4) ! Type tetra = +4 faces
                ncel = ncel + 1
                nfac_max = nfac_max + 4
            CASE(5) ! Type hexa = +6 faces
                ncel = ncel + 1
                nfac_max = nfac_max + 6
            CASE(6) ! Type prisme = +5 faces
                ncel = ncel + 1
                nfac_max = nfac_max + 5
            CASE(7) ! Type pyramide = +5 faces
                ncel = ncel + 1
                nfac_max = nfac_max + 5
            CASE DEFAULT
                CALL print_err('Type d element non pris en charge pour le comptage des parametres lors de la lecture du fichier .msh.',1)
        ENDSELECT
        
    ! Elements de type 1D et 2D
    ELSE
        ! Type segment = face limite
        IF (elm_typ == 1) THEN
            nfac_lim = nfac_lim + 1
        END IF
    
        ! Type triangle = +3 faces
        IF (elm_typ == 2) THEN
            ncel = ncel + 1
            nfac_max = nfac_max + 3
        END IF
    
        ! Type quad = +4 faces
        IF (elm_typ == 3) THEN
            ncel = ncel + 1
            nfac_max = nfac_max + 4
        END IF
    ENDIF
END DO
! Remplissage de  ncel et nfac_lim
DOM%MLG%ncel = ncel
DOM%MLG%nfac_lim = nfac_lim


!----------------------------------------------------------------------!
!---- 2eme boucle : remplit FAC des listes de faces uniques et FAC2CEL
!---- grace a une methode de bouclage sur les elements 2D/3D et un
!---- remplissage de la structure intermediaire sommet(:)%list_fac

! Allocation des tableaux temporaires FAC et FAC2CEL
ALLOCATE(FAC(nfac_max, 6))
FAC(:,:) = 0
ALLOCATE(FAC2CEL(nfac_max, 4))
FAC2CEL(:,:) = 0
ALLOCATE(sommet(DOM%MLG%nsom)) ! <= sommet stocke toutes les faces repertoriees pour chaque sommet

! Initialisation des variables
nfac = 0
i_cel = 0

! Boucle pour chaque element
DO i_elm = 1,n_elm
    
    ! Re-initialisation des sommets de l'element i_elm
    SOM_i(:) = 0
    
    ntag = ELEMENTS(i_elm, 3)
    elm_typ = ELEMENTS(i_elm,2)
    
    ! Suivant dim_simu et le type d'element, on ajoute une cellule, on indique 
    ! le nombre de sommets et on pointe vers les tableaux de relation fac_order ...
    
    !--- pour tous les cas 1D/2D/2D_AXI ...
    IF (DOM%MLG%dim_simu /= '3D') THEN
        
        ! Cas des triangles 2D
        IF (elm_typ == 2) THEN
            i_cel = i_cel + 1
            nsom=3
            tab_id_fac1 => fac_order%tri
            NULLIFY( tab_id_fac2 )
        
        ! Cas des quadrangles 2D
        ELSE IF (elm_typ == 3) THEN
            i_cel = i_cel + 1
            nsom=4
            tab_id_fac1 => fac_order%quad
            NULLIFY( tab_id_fac2 )
        
        ! Sinon : Ne rien faire
        ELSE
            NULLIFY( tab_id_fac1 )
            NULLIFY( tab_id_fac2 )
        END IF
    
    ! .. ou pour tous les cas 3D ...
    ELSE
    
        ! Cas des tetrahedres 3D
        IF (elm_typ == 4) THEN
            i_cel = i_cel + 1
            nsom=4
            tab_id_fac1 => fac_order%tetra
            NULLIFY( tab_id_fac2 )
        
        ! Cas des hexahedres 3D
        ELSE IF (elm_typ == 5) THEN
            i_cel = i_cel + 1
            nsom=8
            tab_id_fac1 => fac_order%hexa
            NULLIFY( tab_id_fac2 )
        
        ! Cas des prismes 3D
        ELSE IF (elm_typ == 6) THEN
            i_cel = i_cel + 1
            nsom=6
            tab_id_fac1 => fac_order%prism
            tab_id_fac2 => fac_order%prism_base
            
        ! Cas des pyramides 3D
        ELSE IF (elm_typ == 7) THEN
            i_cel = i_cel + 1
            nsom=5
            tab_id_fac2 => fac_order%pyram_base
            tab_id_fac1 => fac_order%pyram
            
            
        ! Sinon : Ne rien faire
        ELSE
            NULLIFY( tab_id_fac1 )
            NULLIFY( tab_id_fac2 )
        END IF
        
    END IF
    
    ! Pour chaque tableau de relation sommet des cell => sommets des faces ...
    DO i_tab = 1,2
        IF (i_tab == 1) tab_id_fac => tab_id_fac1
        IF (i_tab == 2) tab_id_fac => tab_id_fac2
        
        ! Si le tableau a ete pointe
        IF (ASSOCIATED( tab_id_fac )) THEN
            
            ! On recupere les sommets de la cellules
            SOM_i(1:nsom) = ELEMENTS(i_elm,4+ntag:3+ntag+nsom)
            
            ! On recupere le nombre de faces et le nombre de sommets par face
            n_fac = SIZE(tab_id_fac,2)
            n_som_fac = SIZE(tab_id_fac,1)
            
            ! Pour chaque face de la cellule
            DO i_fac = 1,n_fac
                
                ! Sommet de la face i_fac
                FAC_i(:) = 0
                DO i_som_fac = 1,n_som_fac
                    FAC_i(i_som_fac) = SOM_i( tab_id_fac(i_som_fac, i_fac) )
                END DO
                
                ! Reodonnancement des sommets des faces pour comparaison "facile"
                CALL reorder_faci( FAC_i, n_som_fac)
                
                ! Test si la face est nouvelle en comparant avec les sommet(:)%list_fac
                new_fac = .TRUE.    ! Si new_fac == TRUE -> La face n'est pas dans la liste des faces des sommets de i_fac
                DO i_som_fac = 1,n_som_fac
                    
                    id_som = FAC_i(i_som_fac)   ! Indice du sommet
                    
                    ! Pour chaque face repertorie du sommet id_som
                    DO i = 1,sommet(id_som)%nfac_adj
                        
                        ! Si tous les sommets concordent, la face est deja repertorie et on sort
                        IF (ALL( sommet(id_som)%list_fac(:,i) == FAC_i )) THEN
                            new_fac = .FALSE.
                            IF (.NOT.new_fac) EXIT
                        END IF
                        IF (.NOT.new_fac) EXIT ! Sortie des 2 boucles
                        
                    END DO ! Fin boucle pour chaque face
                    
                END DO ! Fin boucle pour chaque sommet
                
                ! Si nouvelle face trouvée ...
                IF (new_fac) THEN
                    
                    ! ... On remplit le tableau FAC ...
                    nfac = nfac + 1
                    FAC(nfac,1) = nfac
                    FAC(nfac,2) = n_som_fac
                    FAC(nfac,3:6) = FAC_i
                    
                    ! Et on repertorie la face dans tous les sommets de la face
                    DO i_som_fac = 1,n_som_fac
                        
                        id_som = FAC_i(i_som_fac)
                        sommet(id_som)%nfac_adj = sommet(id_som)%nfac_adj + 1
                        
                        ! Check si nombre de faces adjacentes est inferieur a la taille du tableau
                        IF ( sommet(id_som)%nfac_adj > SIZE(sommet(id_som)%list_fac, 2) ) THEN
                            CALL print_err("Le nombre max de faces par sommet est depasse")
                            CALL print_err("Augmentez cette limite dans mod_mesh_lect_msh.f90",1)
                        END IF
                        
                        sommet(id_som)%list_fac(:,sommet(id_som)%nfac_adj) = FAC_i
                        sommet(id_som)%list_id_fac(sommet(id_som)%nfac_adj) = nfac
                        
                    END DO ! Fin sommets de la face
                    
                END IF ! Fin remplissage

                ! Si nouvelle face trouvée ...
                IF (new_fac) THEN
                    
                    ! ... On remplit le tableau FAC2CEL avec la premiere cellule adjacente ...
                    FAC2CEL(nfac,1) = nfac
                    FAC2CEL(nfac,2) = 1
                    FAC2CEL(nfac,3) = i_cel
                
                ! Sinon, on la cellule existe deja et on doit ajouter i_cel a FAC2CEL dans le bon id_fac
                ELSE
                    
                    ! Pour chaque sommet de la face ...
                    find_fac = .FALSE.
                    DO i_som_fac = 1,n_som_fac
                    
                        id_som = FAC_i(i_som_fac)   ! Indice du sommet
                        
                        ! ... et pour chaque face repertorie du sommet id_som ...
                        DO i = 1,sommet(id_som)%nfac_adj
                            
                            ! Si tous les sommets concordent, on a trouve id_fac
                            IF (ALL( sommet(id_som)%list_fac(:,i) == FAC_i )) THEN
                                
                                id_fac = sommet(id_som)%list_id_fac(i) ! Indice id_fac
                                
                                ! Remplissage de FAC2CEL en 2eme cellule
                                FAC2CEL(id_fac,4) = i_cel
                                FAC2CEL(id_fac,2) = FAC2CEL(id_fac,2) + 1
                                
                                ! Si on a boucle sur la face plus de 2 fois, il y a qqchose de faux dans le maillage
                                IF (FAC2CEL(id_fac,2) > 2) THEN
                                    print*, FAC2CEL(id_fac,:)
                                    CALL print_err('Plus de 2 cellules ont ete trouvees pour une cellule du maillage')
                                END IF 
                                
                                find_fac = .TRUE.
                                IF (find_fac) EXIT ! Sortie des 2 boucles
                                
                            END IF
                            
                        END DO ! Fin boucle pour chaque face du sommet
                        IF (find_fac) EXIT ! Sortie des 2 boucles
                        
                    END DO ! Fin boucle pour chaque sommet
                    
                END IF ! Fin remplissage FAC2CEL
                
            END DO ! Fin boucle face de la cellule
        
        END IF
    END DO ! Fin boucle tableaux tab_id_fac
    
END DO ! Fin boucle ELEMENTS

! Remplissage nfac ...
DOM%MLG%nfac = nfac
! ... et nfac_int = nfac - nfac_lim
DOM%MLG%nfac_int = DOM%MLG%nfac - DOM%MLG%nfac_lim

! Allocation et remplissage de FAC dans la structure de donnees
ALLOCATE( DOM%MLG%FAC(nfac,6) )
DOM%MLG%FAC(:,:) = FAC(1:nfac,:)
DEALLOCATE( FAC )

! Allocation et remplissage de FAC2CEL dans la structure de donnees
ALLOCATE( DOM%MLG%FAC2CEL(nfac,4) )
DOM%MLG%FAC2CEL(:,:) = FAC2CEL(1:nfac,:)
DEALLOCATE( FAC2CEL )

! Allocation de FACLIM et FACLIM_MARQ
ALLOCATE( DOM%MLG%FACLIM( nfac_lim,2 ) )
DOM%MLG%FACLIM = 0
ALLOCATE( DOM%MLG%FACLIM_MARQ( nfac_lim ) )
DOM%MLG%FACLIM_MARQ = ''


!----------------------------------------------------------------------!
!---- 3eme boucle : remplit FACLIM et FACLIM_MARQ
!---- grace a une methode de bouclage sur les elements 2D/3D et comparaison
!---- avec la structure intermediaire sommet(:)%list_fac

! Init du nombre de faces limites
i_faclim = 0

! Boucle pour chaque element
DO i_elm = 1,n_elm
    
    ! Re-initialisation des sommets de la face limite
    FAC_i(:) = 0
    
    ntag = ELEMENTS(i_elm, 3)
    elm_typ = ELEMENTS(i_elm,2)
    
    
    !--- pour tous les cas 1D/2D/2D_AXI ...
    IF (DOM%MLG%dim_simu /= '3D') THEN
        
        ! Cas des segments en 2D
        IF (elm_typ == 1) THEN
            i_faclim = i_faclim + 1
            n_som_fac=2
        
        ! Sinon : Ne rien faire
        ELSE
            n_som_fac=0
        END IF
    
    ! .. ou pour tous les cas 3D ...
    ELSE
    
        ! Cas des triangles en 3D
        IF (elm_typ == 2) THEN
            i_faclim = i_faclim + 1
            n_som_fac=3
        
        ! Cas des quadrangles en 3D
        ELSE IF (elm_typ == 3) THEN
            i_faclim = i_faclim + 1
            n_som_fac=4
        
        ! Sinon : Ne rien faire
        ELSE
            n_som_fac=0
        END IF
        
    END IF
    
    ! Si une face limite est trouvee
    IF (n_som_fac/=0) THEN
    
        ! On recupere les sommets de la fac limite
        FAC_i(1:n_som_fac) = ELEMENTS(i_elm,4+ntag:3+ntag+n_som_fac)
    
        ! Reodonnancement des sommets de la face pour comparaison "facile"
        CALL reorder_faci( FAC_i, n_som_fac)
        

        ! Pour chaque sommet de la face ...
        find_fac = .FALSE.
        DO i_som_fac = 1,n_som_fac
        
            id_som = FAC_i(i_som_fac)   ! Indice du sommet
            
            ! ... et pour chaque face repertorie du sommet id_som ...
            DO i = 1,sommet(id_som)%nfac_adj
                
                ! Si tous les sommets concordent, on a trouve id_fac
                IF (ALL( sommet(id_som)%list_fac(:,i) == FAC_i )) THEN
                    
                    id_fac = sommet(id_som)%list_id_fac(i) ! Indice id_fac
                    
                    ! Complete FACLIM
                    DOM%MLG%FACLIM( i_faclim,1 ) = i_faclim
                    DOM%MLG%FACLIM( i_faclim,2 ) = id_fac
                    
                    ! Complete FACLIM_MARQ
                    DOM%MLG%FACLIM_MARQ(i_faclim) = phys_name(ELEMENTS(i_elm,4))
                    
                    find_fac = .TRUE.
                    IF (find_fac) EXIT ! Sortie des 2 boucles
                    
                END IF
                
            END DO ! Fin boucle pour chaque face du sommet
            IF (find_fac) EXIT ! Sortie des 2 boucles
            
        END DO ! Fin boucle pour chaque sommet

        ! Si on a bouclé sur toutes les face limites : on se casse
        IF (i_faclim == nfac_lim) EXIT
        
        
    END IF
    
END DO ! Fin boucle ELEMENTS


! Allocation de CEL2DOM_MARQ
ALLOCATE( DOM%MLG%CEL2DOM_MARQ( ncel ) )
DOM%MLG%CEL2DOM_MARQ = ''

!----------------------------------------------------------------------!
!---- 4eme boucle : remplit CEL2DOM_MARQ
!---- suivant le tag no 4 du tableau ELEMENTS

! Init du nombre de cellules
i_cel = 0

! Boucle pour chaque element
DO i_elm = 1,n_elm
    
    ! Re-initialisation
    new_cel = .FALSE.

    ! Elements est de type cellule si type de cellule correspond au 3D (ou au 2D)
    elm_typ = ELEMENTS(i_elm,2)
    IF (DOM%MLG%dim_simu == '3D') THEN
        IF (elm_typ > 3) new_cel = .TRUE.
    ELSE
        IF (elm_typ > 1) new_cel = .TRUE.
    END IF

    ! Si type cellule, on remplit
    IF (new_cel) THEN
        i_cel = i_cel  + 1
        DOM%MLG%CEL2DOM_MARQ( i_cel ) = phys_name( ELEMENTS(i_elm,4) )
    END IF

    ! Si on a bouclé sur toutes les cellules : on se casse
    IF (i_cel == ncel) EXIT

END DO ! Fin boucle ELEMENTS


! De-allocations
DEALLOCATE( ELEMENTS )
DEALLOCATE( sommet )
DEALLOCATE( phys_no, phys_name )

END SUBROUTINE mesh_read_msh
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Reordonnancement des sommets de la face FAC_i en prenant comme ref
!> le plus petit ID des sommets
SUBROUTINE reorder_faci( FAC_i, n_som_fac )
!
!---- Arguments -------------------------------------------------------!
INTEGER(IP) :: n_som_fac
INTEGER(IP), DIMENSION(4) :: FAC_i
!
!---- Déclarations ----------------------------------------------------!
INTEGER(IP) :: id_a, id_b, id_c, id_d, id_min
LOGICAL :: left_to_right

! Indice du premier element de la face
id_min = MINLOC( FAC_i(1:n_som_fac), 1 )
id_a = FAC_i(id_min)

! Indice a droite du 1er sommet
IF (id_min==n_som_fac) THEN
    id_b = FAC_i(1)
ELSE
    id_b = FAC_i(id_min+1)
END IF

! Cas n_som_fac > 2 (pour les triangles et les quandrangles)
IF (n_som_fac > 2) THEN

    ! Indice a gauche du 1er sommet
    IF (id_min==1) THEN
        id_d = FAC_i(n_som_fac)
    ELSE
        id_d = FAC_i(id_min-1)
    END IF

    ! sens dans lequel on tourne :
    ! .TRUE. => de droite a gauche
    ! .FALSE. => de gauche a droite
    IF (MIN( id_b, id_d ) == id_b) THEN
        left_to_right = .TRUE.
    ELSE
        left_to_right = .FALSE.
    END IF

END IF

! Cas n_som_fac > 3 (quadrangle)
! NB : Le troisieme indice est toujours celui a 2 positions du 1er sommet
IF (n_som_fac > 3) THEN
                
    IF (id_min<=2) THEN
        id_c = FAC_i(id_min+2)
    ELSE
        id_c = FAC_i(id_min-2)
    END IF
    
END IF

! On reordonne FAC_i : 1er indice
FAC_i(1) = id_a

! 2eme et 4eme indice : suivant la valeur de left_to_right
IF (n_som_fac > 2) THEN
    IF (left_to_right) THEN
        FAC_i(2) = id_b
        FAC_i(n_som_fac) = id_d
    ELSE
        FAC_i(2) = id_d
        FAC_i(n_som_fac) = id_b
    END IF
END IF

! Si 2D : second sommet = b
IF (n_som_fac == 2) THEN
    FAC_i(2) = id_b
END IF

! Si triangle : troisieme sommet = c
IF (n_som_fac == 4) THEN
    FAC_i(3) = id_c
END IF


END SUBROUTINE reorder_faci
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_read_msh
