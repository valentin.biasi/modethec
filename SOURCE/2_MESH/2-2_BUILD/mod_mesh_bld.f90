!>---- Phase 2 : Relations entre les composantes du maillage ----------!
MODULE mod_mesh_bld
!
USE mod_mesh_cel2som, ONLY : mesh_cel2som
USE mod_mesh_cel2fac, ONLY : mesh_cel2fac
USE mod_mesh_idlim, ONLY : mesh_idlim
USE mod_mesh_idfac, ONLY : mesh_idfac
USE mod_mesh_celadj, ONLY : mesh_celadj
USE mod_mesh_somadj, ONLY : mesh_somadj
USE mod_mesh_somlim2faclim, ONLY : mesh_somlim2faclim
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Routine phase 2 : Relations entre les composantes du maillage
SUBROUTINE mesh_bld( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM

! Construction du tableau CEL2FAC
CALL mesh_cel2fac( DOM )

! Construction du tableau FAC_XYZ
! CALL mesh_facxyz( DOM )

! Construction du tableau ID_LIM + nlim (nombre de limites)
CALL mesh_idlim( DOM )

! Construction des tableaux ID_FACINT + ID_FACLIM + FACLIMr
CALL mesh_idfac( DOM )

! Construction des tableaux CEL_ADJ
CALL mesh_celadj( DOM )

! Construction du tableau CEL2SOM
CALL mesh_cel2som( DOM )

! Construction du tableau SOM_ADJ
CALL mesh_somadj( DOM )

! Construction du tableau SOMLIM2FACLIM
CALL mesh_somlim2faclim( DOM )


END SUBROUTINE mesh_bld
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_bld
