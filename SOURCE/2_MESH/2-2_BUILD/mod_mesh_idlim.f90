!> Module de construction du tableau ID_LIM(nlim) (nombre de limites)
!> ID_LIM = identifiant des limites (string)
MODULE mod_mesh_idlim
!
USE mod_cst, ONLY : DP, IP
!
USE mod_dom
!
IMPLICIT NONE
!
!----------------------------------------------------------------------!
!> Liste chainée des identifiants de limites
TYPE Limi
    INTEGER(IP) :: ilim
    CHARACTER (len=32) :: id_lim
    TYPE(Limi), POINTER :: Suiv => NULL()
END TYPE Limi
!----------------------------------------------------------------------!
!
CONTAINS
!
!----------------------------------------------------------------------!
!> Routine principale de construction du tableau ID_LIM
SUBROUTINE mesh_idlim( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: nfac_lim, ifac_lim
INTEGER(IP) :: nlim, ilim
CHARACTER (len=32) :: id_lim
LOGICAL :: dejavu
TYPE(Limi), POINTER :: Prem_lim, Cour_lim ! Premier maillon et maillon courant
!
!---- Récupérations ---------------------------------------------------!
nfac_lim = DOM%MLG%nfac_lim
nlim = 1

! Remplissage de Prem_lim = 1er identifiant de FACLIM_MARQ
ALLOCATE( Prem_lim )
Prem_lim%ilim = 1
Prem_lim%id_lim = DOM%MLG%FACLIM_MARQ(1)
Cour_lim => Prem_lim ! 1er maillon de Cour_lim = Prem_lim
ALLOCATE( Cour_lim%Suiv ) ! Prem_lim a un suivant

! Boucle sur les faces limites
DO ifac_lim = 1,nfac_lim
    
    dejavu = .FALSE.
    id_lim = DOM%MLG%FACLIM_MARQ(ifac_lim)
    Cour_lim => Prem_lim ! RAZ de la liste chainee
    
    ! Parcours de la liste et determine si id_lim est deja vu dans Cour_lim
    DO ilim = 1,nlim
        IF (Cour_lim%id_lim == id_lim) THEN
            dejavu = .TRUE.
        END IF       
        Cour_lim => Cour_lim%Suiv
    END DO
    
    ! Si id_lim n'a jamais été vu, on ajoute et complete un maillon de la chaine
    IF (.NOT.(dejavu)) THEN
        nlim = nlim +1
        Cour_lim%ilim = nlim
        Cour_lim%id_lim = id_lim
    
        ALLOCATE( Cour_lim%Suiv )
        Cour_lim => Cour_lim%Suiv
    END IF

END DO

! La chaine Cour_lim contient toutes les infos de ID_LIM
! On remplit nlim et ID_LIM
DOM%MLG%nlim = nlim
ALLOCATE( DOM%MLG%ID_LIM(nlim) )

Cour_lim => Prem_lim ! Retour au debut de la chaine

DO ilim = 1,nlim
    DOM%MLG%ID_LIM(ilim) = Cour_lim%id_lim
    Cour_lim => Cour_lim%Suiv
END DO

END SUBROUTINE mesh_idlim
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_idlim
