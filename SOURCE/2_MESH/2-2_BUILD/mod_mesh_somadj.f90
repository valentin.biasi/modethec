!> Module de construction du tableau SOM_ADJ des sommets adjacents a chaque sommet
!> et de son extraction pour les sommets interieurs SOM_ADJ_INT et 
!> les sommets limites SOM_ADJ_LIM
!> 1: no som ||| 2: nb som adj ||| 3: no somadj1 ||| 4: no somadj2 |||....
MODULE mod_mesh_somadj
!
USE mod_cst, ONLY : DP, IP
USE mod_print, ONLY : print_err
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Routine principale de cconstruction du tableau SOM_ADJ
SUBROUTINE mesh_somadj( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: isom, nsom, ifac, nfac, nbsomadj, ifac_lim, nfac_lim, i
INTEGER(IP) :: id_fac, id_som, isom_fac, nsom_fac
INTEGER(IP) :: max_somadj, id_som1, id_som2
INTEGER(IP), ALLOCATABLE, DIMENSION(:,:) :: SOM_ADJ
LOGICAL :: find_som
!
!---- Récupérations ---------------------------------------------------!
nsom = DOM%MLG%nsom
nfac = DOM%MLG%nfac
nfac_lim = DOM%MLG%nfac_lim
!
!---- Allocation de SOM_ADJ temporaire --------------------------------!
IF (DOM%MLG%dim_simu == '3D') THEN
    ALLOCATE( SOM_ADJ(nsom,30) )
ELSE
    ALLOCATE( SOM_ADJ(nsom,12) )
END IF

! Initialisation de SOM_ADJ
SOM_ADJ(:,:) = 0
DO isom = 1,nsom
    SOM_ADJ(isom,1) = isom
END DO

! Boucle sur les faces
DO ifac = 1, nfac

    ! Pour chaque sommet de la face
    nsom_fac = DOM%MLG%FAC(ifac,2)
    DO isom_fac = 1,nsom_fac
        
        ! On boucle 2 fois, pour que id_som1 <=> id_som2
        DO i = 1,2
            IF (i==1) THEN
                id_som1 = DOM%MLG%FAC(ifac,2+isom_fac)
                id_som2 = DOM%MLG%FAC(ifac,3+MOD(isom_fac,nsom_fac))
            ELSE
                id_som2 = DOM%MLG%FAC(ifac,2+isom_fac)
                id_som1 = DOM%MLG%FAC(ifac,3+MOD(isom_fac,nsom_fac))
            END IF
       
        ! Nombre de sommets actuellement repertories voisins de id_som1
        nbsomadj =  SOM_ADJ(id_som1,2)
        
        ! Test pour savoir si parmi les voisins de id_som1, il existe id_som2
        find_som = ANY(SOM_ADJ(id_som1, 3:) == id_som2)
        
        ! Si pas trouve, on ajoute a la liste
        IF (.NOT.find_som) THEN
            nbsomadj = nbsomadj + 1
            SOM_ADJ(id_som1, 2) = nbsomadj
            SOM_ADJ(id_som1,2+nbsomadj) = id_som2
        END IF
        
        END DO ! Fin boucle 2 fois
        
    ! Sortie pour les cas 1D/2D car une seule arrete
    IF (DOM%MLG%dim_simu /= '3D') EXIT
    END DO

END DO ! Fin boucle sur les faces


! Recopie du tableau temporaire dans le tableau définitif
max_somadj = maxval(SOM_ADJ(:,2))
ALLOCATE(DOM%MLG%SOM_ADJ(nsom, 2+max_somadj))
DOM%MLG%SOM_ADJ(:, 1:2+max_somadj) = SOM_ADJ(:,1:max_somadj+2)


! Creation de SOM_ADJ_INT en elimiant les sommets attaches a des faces limites
! Pour chaque face limite
DO ifac_lim = 1,nfac_lim
    
    ! Indice absolu de la face et nombre de sommets de la face
    id_fac = DOM%MLG%FACLIM(ifac_lim,2)
    nsom_fac = DOM%MLG%FAC(id_fac,2)
    
    ! Pour chacun des sommets limites
    DO isom_fac = 1,nsom_fac
        ! Extraction du numero de sommet de la face limite
        id_som = DOM%MLG%FAC(id_fac, 2 + isom_fac)
        ! Mise a 0 de la ligne correspondante
        SOM_ADJ(id_som,:) = 0
    ENDDO
ENDDO
    

! Compte du nombre de lignes non-nulles de SOM_ADJ (nombre de sommets interieurs)
DOM%MLG%nsom_int = COUNT(SOM_ADJ(:,1) /= 0, dim = 1)
! Compte du nombre de lignes nulles de SOM_ADJ (nombre de sommets exterieurs)
DOM%MLG%nsom_lim = COUNT(SOM_ADJ(:,1) == 0, dim = 1)


! Recopie des lignes non-nulles dans SOM_ADJ_INT
ALLOCATE (DOM%MLG%SOM_ADJ_INT(DOM%MLG%nsom_int,2 + max_somadj))
id_som = 1
DO isom = 1,nsom
    IF (SOM_ADJ(isom,1) /= 0) THEN
        DOM%MLG%SOM_ADJ_INT(id_som,:) = SOM_ADJ(isom,1:2 + max_somadj)
        id_som = id_som + 1
    ENDIF
ENDDO
    
    
! Recopie des lignes correspondantes aux sommets limites dans SOM_ADJ_LIM
ALLOCATE (DOM%MLG%SOM_ADJ_LIM( DOM%MLG%nsom_lim, 2 + max_somadj ))
ALLOCATE (DOM%MLG%SOMLIM( DOM%MLG%nsom, 2 ))
id_som = 1
DO isom = 1,nsom
    DOM%MLG%SOMLIM(isom,1) = isom
    
    ! Complete SOM_ADJ_LIM
    IF (SOM_ADJ(isom,1) == 0) THEN
        DOM%MLG%SOM_ADJ_LIM(id_som,:) = DOM%MLG%SOM_ADJ(isom,:)
        id_som = id_som + 1
    ENDIF
    
    ! Tableau SOMLIM 0/1 suivant cas limite/interne
    IF (SOM_ADJ(isom,1) == 0) THEN
        DOM%MLG%SOMLIM(isom,2) = 1 ! Remplit SOMLIM - 1 = sommet limite
    ELSE
        DOM%MLG%SOMLIM(isom,2) = 0 ! Remplit SOMLIM - 0 = sommet interne
    END IF
    
ENDDO

! Desallocation
DEALLOCATE( SOM_ADJ )

END SUBROUTINE mesh_somadj
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_somadj
