!> Module de construction du tableau CEL2FAC similaire a FAC2CEL
!> 1: N° CEL ||| 2: NB FAC ||| 3: N° FAC1 ||| 4: N° FAC2 |||....
!> Avec pour les cas 2D un signe+/- sur les id faces pour avoir sens ferme
MODULE mod_mesh_cel2fac
!
USE mod_cst, ONLY : DP, IP
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Routine principale de construction de CEL2FAC
SUBROUTINE mesh_cel2fac( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: ncel, icel, id_cel, nfac_cel
INTEGER(IP) :: nfac, ifac, iifac, sens
INTEGER(IP) :: jcel
INTEGER(IP) :: id_fac ! id absolu de la face pour CEL2FAC
INTEGER(IP), DIMENSION(2) :: id_som ! sommets de la id_fac
INTEGER(IP), DIMENSION(:,:), ALLOCATABLE :: CEL2FAC ! Tableau CEL2FAC intermédiaire
INTEGER(IP), DIMENSION(:,:), ALLOCATABLE :: CEL2FAC_sens ! Tableau CEL2FAC intermédiaire avec sens

!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
nfac = DOM%MLG%nfac

!---- Allocations -----------------------------------------------------!
ALLOCATE( DOM%MLG%CEL2FAC(ncel,8) )
ALLOCATE( CEL2FAC(ncel,8) )
ALLOCATE( CEL2FAC_sens(ncel,6) )

! Initialisation de CEL2FAC
CEL2FAC(:,:) = 0
DO icel =1,ncel
    CEL2FAC(icel,1) = icel
END DO

!---- Construction de CEL2FAC initiale --------------------------------!
! Boucle pour toutes les faces
DO ifac = 1,nfac
    
    ! Pour toutes les cellules adjacentes a ifac
    DO jcel = 1,DOM%MLG%FAC2CEL(ifac,2)
        
        ! Indice absolu de la face
        id_cel = DOM%MLG%FAC2CEL(ifac,jcel+2)
        
        ! Nombre de faces adj a la cellule id_cel
        nfac_cel = CEL2FAC(id_cel,2) + 1
        CEL2FAC(id_cel,2) = nfac_cel
        
        ! Ecriture de l'indice de la face
        CEL2FAC(id_cel, 2+nfac_cel) = ifac
    
    END DO

END DO
!----------------------------------------------------------------------!

! Ecriture dans structure de donnees
DOM%MLG%CEL2FAC = CEL2FAC

!---- Construction de CEL2FAC_sens ------------------------------------!
! Boucle pour toutes les cellules pour les cas 2D uniquement
IF (DOM%MLG%dim_simu /= '3D') THEN

DO icel = 1,ncel
    ! REINIT DES VARIABLES POUR CHAQUE icel
    id_fac = CEL2FAC(icel,3) ! ID de la 1ere face
    id_som = DOM%MLG%FAC(id_fac,3:4) ! ID des 2 sommets de la 1ere face
    CEL2FAC_sens(icel,1) = id_fac ! 1ere face determine le sens
    sens = 2

    ! Nombre de faces de icel
    nfac_cel = CEL2FAC(icel,2)

    DO ifac = 2,nfac_cel ! Pour les i faces
        DO iifac = 2,nfac_cel ! On cherche la cellule correspondante suivante

            ! id_fac : la cellule à rechercher
            id_fac = CEL2FAC(icel,iifac+2) ! ID de la ieme face

            ! si id_fac different de la cellule courante
            IF (id_fac .ne. ABS(CEL2FAC_sens(icel,ifac-1))) THEN

                ! si cellule suivante correspond (sens standard)
                IF (DOM%MLG%FAC(id_fac,4) == id_som(sens)) THEN
                    CEL2FAC_sens(icel,ifac) = - id_fac
                    sens = 1
                    id_som = DOM%MLG%FAC(id_fac,3:4) ! ID des 2 sommets de la ifac
                    EXIT

                ! si cellule suivante correspond (sens inverse)    
                ELSE IF (DOM%MLG%FAC(id_fac,3) == id_som(sens)) THEN
                    CEL2FAC_sens(icel,ifac) = id_fac
                    sens = 2
                    id_som = DOM%MLG%FAC(id_fac,3:4) ! ID des 2 sommets de la ifac
                    EXIT

                END IF
            END IF

        END DO
    END DO

END DO

!---- CEL2FAC final
DOM%MLG%CEL2FAC(:,3:8) = CEL2FAC_sens
ENDIF
!----------------------------------------------------------------------!

! Nombre maximal de faces adjacentes par cel.
DOM%MLG%nfac_adj_max = MAXVAL( DOM%MLG%CEL2FAC(:,2) )

! Desallocations
DEALLOCATE( CEL2FAC, CEL2FAC_sens )

END SUBROUTINE mesh_cel2fac
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_cel2fac
