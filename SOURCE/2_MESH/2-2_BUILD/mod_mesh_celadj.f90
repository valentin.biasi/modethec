!> Module de construction du tableau CEL2FAC similaire a FAC2CEL
!> Module de construction du tableau CEL_ADJ des cellules adjacentes 
!> 1: n° cel ||| 2: nb cel adj ||| 3: n° celadj1 ||| 4: n° celadj2 |||....
MODULE mod_mesh_celadj
!
USE mod_cst, ONLY : DP, IP
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Routine principale de construction de CEL2FAC
SUBROUTINE mesh_celadj( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: nfac, ifac
INTEGER(IP) :: ncel, icel
INTEGER(IP) :: id_fac
INTEGER(IP), DIMENSION(2) :: ID_cel
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
!
!---- Allocation de CEL_ADJ -------------------------------------------!
ALLOCATE( DOM%MLG%CEL_ADJ(ncel,8) )
DOM%MLG%CEL_ADJ(:,:) = 0

! Boucle pour les cellules 
DO icel = 1,ncel

    DOM%MLG%CEL_ADJ(icel,1) = icel ! ID de la cellule centrale
    DOM%MLG%CEL_ADJ(icel,2) = DOM%MLG%CEL2FAC(icel,2) ! nb de cel adjacentes
    nfac = DOM%MLG%CEL_ADJ(icel,2) ! nb de cel à retrouver

    DO ifac = 1,nfac

        id_fac = ABS( DOM%MLG%CEL2FAC(icel,ifac+2) ) ! face mitoyenne
        ID_cel = DOM%MLG%FAC2CEL(id_fac,3:4) ! cellules rattachees à id_fac

        IF ( ID_cel(2) == icel ) THEN
            DOM%MLG%CEL_ADJ(icel,ifac+2) = ID_cel(1)
        ELSE IF ( ID_cel(1) == icel ) THEN
            DOM%MLG%CEL_ADJ(icel,ifac+2) = ID_cel(2)
        END IF

        ! Attention aux cellules limites
        IF ( ANY(ID_cel == 0) ) THEN
            DOM%MLG%CEL_ADJ(icel,ifac+2) = 0
            DOM%MLG%CEL_ADJ(icel,2) = DOM%MLG%CEL_ADJ(icel,2) - 1
        END IF

    END DO

END DO

! Nombre maximal de cellules adjacentes par cel.
DOM%MLG%ncel_adj_max = MAXVAL( DOM%MLG%CEL_ADJ(:,2) )

END SUBROUTINE mesh_celadj
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_celadj
