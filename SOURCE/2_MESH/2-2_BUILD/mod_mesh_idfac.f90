!> Module de construction des tableaux ID_FACINT + ID_FACLIM (+ FACLIMr)
MODULE mod_mesh_idfac
!
USE mod_cst, ONLY : DP, IP
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Routine de construction des tableaux ID_FACINT + ID_FACLIM (+ FACLIMr)
SUBROUTINE mesh_idfac( DOM )
! 
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: nfac_lim, nfac_int
INTEGER(IP) :: ifac_int
INTEGER(IP) :: nfac, ifac
INTEGER(IP), DIMENSION(:), ALLOCATABLE :: ID_FAC
!
!---- Récupérations ---------------------------------------------------!
nfac = DOM%MLG%nfac
nfac_lim = DOM%MLG%nfac_lim
nfac_int = DOM%MLG%nfac_int
!
!---- Allocation de ID_FACLIM, ID_FACINT (et FACLIMr) -----------------!
ALLOCATE( DOM%MLG%ID_FACLIM(nfac_lim) )
ALLOCATE( DOM%MLG%ID_FACINT(nfac_int) )
ALLOCATE( ID_FAC(nfac) )

ifac_int = 1

! Remplissage de ID_FACLIM
DOM%MLG%ID_FACLIM(:) = DOM%MLG%FACLIM(:,2)

! Remplissage de ID_FACINT
ID_FAC = DOM%MLG%FAC(:,1)

ID_FAC( DOM%MLG%ID_FACLIM ) = 0 ! ID_FAC = 0 si face limite
! Boucle sur les faces
DO ifac = 1,nfac
    IF (ID_FAC(ifac) .ne. 0) THEN
        DOM%MLG%ID_FACINT(ifac_int) = ID_FAC(ifac)
        ifac_int = ifac_int +1
    END IF
END DO

DEALLOCATE( ID_FAC )

END SUBROUTINE mesh_idfac
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_idfac
