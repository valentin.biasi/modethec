!< Module de construction du vecteur SOMLIM2FACLIM
!< Ce vecteur contient l'indice des sommets limites et l'indice des faces limites associées.
!< Ces faces sont ordonnées de sorte que deux indices consécutif décrivent des faces en contact.
!> 1: no som ||| 2: nb fac adj ||| 3: no fac1 ||| 4: no fac2 |||....
MODULE mod_mesh_somlim2faclim
!
USE mod_cst, ONLY : DP, IP
USE mod_print, ONLY : print_err
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Routine principale de cconstruction du tableau SOMLIM2FACLIM
SUBROUTINE mesh_somlim2faclim( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: i, isom, id_som, nsom_lim, ifac, id_fac, nb_fac_max, nfac_lim
INTEGER(IP) :: id_fac_vois, nsom
INTEGER(IP) :: nb_sommet_face, nb_sommet_face_vois, compt_sommet, nb_sommet_classe
INTEGER(IP), ALLOCATABLE, DIMENSION(:,:) :: id_faces_voisines
INTEGER(IP), DIMENSION(4) :: sommet_temp
!
!---- Récupérations ---------------------------------------------------!
nsom = DOM%MLG%nsom
nsom_lim = DOM%MLG%nsom_lim
nfac_lim = DOM%MLG%nfac_lim
ALLOCATE(DOM%MLG%SOM_LIM(nsom_lim))
ALLOCATE(DOM%MLG%IDSOM2ISOM(nsom))

DOM%MLG%IDSOM2ISOM(:) = -1

! Premiere boucle pour recuperer les faces limites associees aux 
! sommets limites
ALLOCATE(id_faces_voisines(DOM%MLG%nsom,30))
id_faces_voisines(:,1) = 0
DO ifac = 1,nfac_lim
    id_fac = DOM%MLG%FACLIM(ifac,2) ! Indice absolu de la face
    DO isom = 1,DOM%MLG%FAC(id_fac,2)
        id_som = DOM%MLG%FAC(id_fac,2+isom)
        id_faces_voisines(id_som,1) = id_faces_voisines(id_som,1) + 1
        id_faces_voisines(id_som,id_faces_voisines(id_som,1)+1) = id_fac
    END DO
END DO
nb_fac_max = MAXVAL(id_faces_voisines(:,1))

! On trie ensuite les faces pour que deux indices de face consécutif 
! correspondent à deux faces en contact
! On parcourt les faces dans id_faces_voisines, une fois que l'on a trié une face, 
! on change son indice en "-1"
ALLOCATE(DOM%MLG%SOMLIM2FACLIM(nsom_lim,nb_fac_max+2))

DO isom = 1,nsom_lim
    id_som = DOM%MLG%SOM_ADJ_LIM(isom,1)
    DOM%MLG%IDSOM2ISOM(id_som) = isom ! Création du tableau IDSOM2ISOM

    DOM%MLG%SOMLIM2FACLIM(isom,1) = id_som
    DOM%MLG%SOMLIM2FACLIM(isom,2) = id_faces_voisines(id_som,1)
    DOM%MLG%SOM_LIM(isom)%nvoisin = id_faces_voisines(id_som,1)

    ALLOCATE(DOM%MLG%SOM_LIM(isom)%ARETES(DOM%MLG%SOM_LIM(isom)%nvoisin))
    ALLOCATE(DOM%MLG%SOM_LIM(isom)%RAIDEUR(DOM%MLG%SOM_LIM(isom)%nvoisin))
    ALLOCATE(DOM%MLG%SOM_LIM(isom)%NBTAG_ARET_SOM(DOM%MLG%SOM_LIM(isom)%nvoisin))
    ALLOCATE(DOM%MLG%SOM_LIM(isom)%FACES(DOM%MLG%SOM_LIM(isom)%nvoisin))
    ALLOCATE(DOM%MLG%SOM_LIM(isom)%SOMMETS_LIM(DOM%MLG%SOM_LIM(isom)%nvoisin))

    DOM%MLG%SOM_LIM(isom)%RAIDEUR = 1.0

    DOM%MLG%SOMLIM2FACLIM(isom,3) = id_faces_voisines(id_som,2)
    IF (id_faces_voisines(id_som,1) < 4) THEN
        ! Si on a 2 ou 3 faces voisines, l'ordre est quelconque
        DO ifac = 1,id_faces_voisines(id_som,1)-1
            DOM%MLG%SOMLIM2FACLIM(isom,3+ifac) = id_faces_voisines(id_som,2+ifac)
        END DO
    ELSE
        nb_sommet_classe = 1
        id_fac = id_faces_voisines(id_som,2)
        id_faces_voisines(id_som,2) = -1
        nb_sommet_face = DOM%MLG%FAC(id_fac,2)
        sommet_temp = -1
        sommet_temp(1:nb_sommet_face) = DOM%MLG%FAC(id_fac,3:2+nb_sommet_face)
        ifac = 0

        ! On parcourt toutes les faces autour du sommet jusqu'à trouver la face voisine
        DO WHILE (nb_sommet_classe /= id_faces_voisines(id_som,1))
            ifac = ifac + 1
            id_fac_vois = id_faces_voisines(id_som,ifac+1)
            IF (id_fac_vois /= -1) THEN
                nb_sommet_face_vois = DOM%MLG%FAC(id_fac_vois,2)
                compt_sommet = 0
                DO i = 1,nb_sommet_face_vois
                    IF (ANY(sommet_temp == DOM%MLG%FAC(id_fac_vois,2+i))) compt_sommet = compt_sommet + 1
                END DO
                ! Si il y a 2 sommets communs c'est que les faces sur la limite sont en contact
                IF (compt_sommet == 2) THEN
                    DOM%MLG%SOMLIM2FACLIM(isom,3+nb_sommet_classe) = id_fac_vois
                    id_faces_voisines(id_som,ifac+1) = -1
                    ifac = 0 ! On remet à 0 l'indice de recherche des faces
                    ! On remplace la face sur laquelle on cherche le voisin
                    nb_sommet_classe = nb_sommet_classe + 1
                    sommet_temp = -1
                    sommet_temp(1:nb_sommet_face_vois) = DOM%MLG%FAC(id_fac_vois,3:2+nb_sommet_face_vois)
                    CYCLE
                END IF
            END IF
        END DO
    END IF
END DO

DEALLOCATE(id_faces_voisines)

END SUBROUTINE mesh_somlim2faclim
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_somlim2faclim