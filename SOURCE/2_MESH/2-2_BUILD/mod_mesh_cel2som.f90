!> Module construction du tableau CEL2SOM
!> Construction du tableau CEL2SOM similaire a CEL2FAC mais pour les sommets
!> 1: N° CEL ||| 2: NB SOM ||| 3: N° SOM1 ||| 4: N° SOM2 |||....
!>
!> Sommets ordonnes en boucle fermes pour 1D et 2D (ordre du polygone)
!> Sommets orientes suivant le format vtk ou gmsh pour 3D (voir doc vtk format)
!> -> doc/img/format_vtk_connetivite.png
MODULE mod_mesh_cel2som
!
USE mod_cst, ONLY : IP
USE mod_print, ONLY : print_err
USE mod_algebra, ONLY : int_in_list, twice_in_list
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Routine principale de construction du tableau CEL2SOM
SUBROUTINE mesh_cel2som( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: ncel, icel
INTEGER(IP) :: nfac_cel, ifac
INTEGER(IP) :: isom, nsom
INTEGER(IP) :: id_fac ! id relative (pos ou neg) de la face pour CEL2FAC en 2D
INTEGER(IP) :: id_fac_base
INTEGER(IP) :: nsom_base, nsom_fac
INTEGER(IP) :: id_som
INTEGER(IP) :: n_search, i_search
LOGICAL :: bool_in_list, bool_fac_adj
INTEGER(IP), DIMENSION(4) :: tmp_fac1, tmp_fac2

!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel

!----------------------------------------------------------------------!
! Pour le cas 3D, on reconstruit l'ordre des sommets au format vtk
IF (DOM%MLG%dim_simu == '3D') THEN

    !---- Allocations -------------------------------------------------!
    ALLOCATE( DOM%MLG%CEL2SOM(ncel,10) )
    DOM%MLG%CEL2SOM = 0
    
    ! Boucle sur les cellules
    DO icel = 1,ncel

        ! Nombre de faces par cellule
        nfac_cel = DOM%MLG%CEL2FAC(icel,2)
        
        ! On cherche la face base (triangle pour prisme et tetra, quadrangle pour pyramide et hexa)
        ! Pour les faces de la icel
        
        ! Initialisation
        DOM%MLG%CEL2SOM(icel,1) = icel
        nsom = 0

        ! Pour chaque face de la cellule, on cherche le nombre de sommets associes
        DO ifac = 1,nfac_cel
            
            ! Indice et nombre de sommets de la face
            id_fac = DOM%MLG%CEL2FAC(icel,ifac+2)
            nsom_fac = DOM%MLG%FAC(id_fac,2)
            
            ! Liste les sommets de la face et ajoute a liste cellule si non present
            DO isom = 1,nsom_fac
                IF (ALL(DOM%MLG%FAC(id_fac,2 + isom) /= DOM%MLG%CEL2SOM(icel,3:))) THEN
                    nsom = nsom + 1
                    DOM%MLG%CEL2SOM(icel,2 + nsom) = DOM%MLG%FAC(id_fac,2 + isom)
                END IF
            END DO
            
        END DO
        ! Ecriture nombre de sommets
        DOM%MLG%CEL2SOM(icel,2) = nsom
        
        ! En fonction du type de cellule :
        ! Nombre de sommet de la base (nsom_base)
        ! Nombre de sommets en plus a chercher (n_search)
        SELECT CASE (nsom)
            ! tetra
            CASE(4)
                nsom_base = 3
                n_search = 1
            ! pyram
            CASE(5)
                nsom_base = 4
                n_search = 1
            ! prisme
            CASE(6)
                nsom_base = 3
                n_search = 3
            ! hexa
            CASE(8)
                nsom_base = 4
                n_search = 4
        END SELECT
        
        ! Pour chaque face de la cellule, on a la face "base"
        DO ifac = 1,nfac_cel
            
            ! Indice et nombre de sommets de la face
            id_fac = DOM%MLG%CEL2FAC(icel,ifac+2)
            nsom_fac = DOM%MLG%FAC(id_fac,2)
            
            ! Si base trouve, on ecrit l'indice et les nsom_base sommets
            IF (nsom_fac == nsom_base) THEN
            
                id_fac_base = id_fac
                DOM%MLG%CEL2SOM( icel, 3:2+nsom_fac ) = DOM%MLG%FAC( id_fac_base,3:2+nsom_fac )
                EXIT ! Sortie de boucle
                
            END IF            
        END DO
        
        ! On cherche les n_search sommets complementaires :
        ! Pour sommet 1, on cherche le sommet au dessus (partage 2 faces differentes de id_fac_base)
        ! Idem pour sommet 2, 3,...
        DO i_search = 1,n_search
            
            ! Initialisation
            bool_fac_adj = .FALSE.
            tmp_fac1(:) = 0
            tmp_fac2(:) = 0
            
            ! Pour chaque face de la cellule
            DO ifac = 1,nfac_cel
            
                ! Indice et nombre de sommets de la face
                id_fac = DOM%MLG%CEL2FAC(icel,ifac+2)
                nsom_fac = DOM%MLG%FAC(id_fac,2)
                
                ! Si face n'est pas base et...
                IF (id_fac /= id_fac_base) THEN
                    
                    !... et si face contient sommet i_search de la base
                    bool_in_list = int_in_list( DOM%MLG%FAC( id_fac,3:2+nsom_fac ) , DOM%MLG%CEL2SOM(icel,2+i_search) )
                    IF (bool_in_list) THEN
                    
                        ! On ecrit tmp_fac1 et tmp_fac2 (sommets des faces trouves)
                        IF (bool_fac_adj .EQV. .FALSE.) THEN
                            tmp_fac1(1:nsom_fac) = DOM%MLG%FAC( id_fac,3:2+nsom_fac )
                            bool_fac_adj = .TRUE.
                        ELSE
                            tmp_fac2(1:nsom_fac) = DOM%MLG%FAC( id_fac,3:2+nsom_fac )
                            EXIT ! Sortie de boucle
                        END IF
                        
                    END IF
                    
                END IF
            
            END DO ! Fin recherche des 2 faces adjacentes
            
            ! Avec sommets des 2 faces tmp_fac1 et tmp_fac2...
            ! ...on trouve id_som, le sommet commun qui n'est pas deja dans la base
            id_som = twice_in_list(tmp_fac1, tmp_fac2, DOM%MLG%CEL2SOM(icel,2+i_search))
            IF (id_som == 0) THEN
                CALL print_err('Probleme dans la routine cel2som : sommets impossibles a ordonner')
            END IF
            
            ! Suivant le format vtk id_som est en position nsom_base + i_search
            ! dans le tableau CEL2SOM
            DOM%MLG%CEL2SOM(icel,2+nsom_base+i_search) = id_som
            
        END DO ! Fin boucle sommets complementaires
        
    END DO ! Fin boucles cellules


!----------------------------------------------------------------------!
! Pour le 2D,1D on ordonne les sommets des cellules
ELSE

    !---- Allocations -------------------------------------------------!
    ALLOCATE( DOM%MLG%CEL2SOM(ncel,6) )
    DOM%MLG%CEL2SOM = 0
    
    ! Boucle sur les cellules
    DO icel = 1,ncel
        
        ! Nombre de faces par cellule
        nfac_cel = DOM%MLG%CEL2FAC(icel,2)
        
        DOM%MLG%CEL2SOM(icel,1) = icel
        DOM%MLG%CEL2SOM(icel,2) = nfac_cel
        
        ! Pour les faces de la icel
        DO ifac = 1,nfac_cel
            
            id_fac = DOM%MLG%CEL2FAC(icel,ifac+2)
            
            ! En fonction de l'orientation de la face
            ! sens positif : choix sommet 1
            IF (id_fac > 0) THEN
                DOM%MLG%CEL2SOM(icel,ifac+2) = DOM%MLG%FAC(id_fac,3)
            ! sens negatif : choix sommet 2
            ELSE IF (id_fac < 0) THEN
                DOM%MLG%CEL2SOM(icel,ifac+2) = DOM%MLG%FAC(-id_fac,4)
            ! aucun : probleme
            ELSE IF (id_fac == 0) THEN
                CALL print_err('Des sommets du tableau CEL2SOM ont un indice nul')
            END IF
        
        END DO
        
    END DO ! Fin boucles cellules


ENDIF
!----------------------------------------------------------------------!
!
END SUBROUTINE mesh_cel2som
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_cel2som
