!> Phase 3 : Calculs propres au maillage (aires, volumes,...)
MODULE mod_mesh_cpt
!
USE mod_cst, ONLY : IP
USE mod_mesh_aire, ONLY : mesh_aire, mesh_aire_str
USE mod_mesh_volu, ONLY : mesh_volu
USE mod_mesh_perim, ONLY : mesh_perim
USE mod_mesh_axe, ONLY : mesh_axe
USE mod_mesh_dL, ONLY : mesh_dL
USE mod_mesh_vect, ONLY : mesh_vect
USE mod_mesh_vfac, ONLY : mesh_vfac
USE mod_mesh_interp, ONLY : mesh_interp
USE mod_mesh_jac, ONLY : mesh_jac
USE mod_mesh_qualite, ONLY : mesh_qualite, mesh_faclim_grp
USE mod_mesh_limite, ONLY : mesh_face2som2aretes, mesh_build_bezier, mesh_sommet_courbure
USE mod_mesh_interieur, ONLY : mesh_sommet2arete
USE mod_mesh_rcourb, ONLY : mesh_courbure
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Routine phase 3 : Calculs propres au maillage (aires, volumes,...)
SUBROUTINE mesh_cpt( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: nfac, ncel, nsom
!
!---- Récupérations ---------------------------------------------------!
nfac = DOM%MLG%nfac
ncel = DOM%MLG%ncel
nsom = DOM%MLG%nsom

! Calcul des aires et des centres des faces
CALL mesh_aire( DOM )

! Calcul des volumes et centres de gravités
CALL mesh_volu( DOM )

! Calcul des perimetres des cellules
IF (DOM%MLG%dim_simu == '3D') THEN        
    ALLOCATE( DOM%MLG%PERIM(ncel) )
    CALL mesh_perim( DOM )
END IF

! Distance entre les centres de cellules et l'axe + angle entre axe et X
IF (DOM%MLG%dim_simu == '2D_AXI') THEN        
    ALLOCATE( DOM%MLG%dist_AXE_G(ncel) )
    ALLOCATE( DOM%MLG%dist_AXE_H(nfac) )
    CALL mesh_axe( DOM )
END IF

! Calcul des distances caracteristiques par cellule : dL_min   
ALLOCATE( DOM%MLG%dL_min(ncel) )
CALL mesh_dL( DOM )

! Calcul des vecteurs G1G2, G1H , HG2 et normes associées
ALLOCATE( DOM%MLG%G1G2(nfac,3) )
ALLOCATE( DOM%MLG%n2_G1G2(nfac) )
ALLOCATE( DOM%MLG%G1H(nfac,3) )
ALLOCATE( DOM%MLG%n2_G1H(nfac) )
ALLOCATE( DOM%MLG%HG2(nfac,3) )
ALLOCATE( DOM%MLG%n2_HG2(nfac) )
CALL mesh_vect( DOM )

! Calcul des vecteurs normaux aux faces VFAC
ALLOCATE( DOM%MLG%VFAC(nfac,3) )
CALL mesh_vfac( DOM )

! Calcul des matrices d'interpolations dans CEL_ITP, FAC_ITP et SOM_ITP
CALL mesh_interp( DOM )

! Préparation pour le calcul des matrices jacobiennes (orientation des faces,...)
CALL mesh_jac( DOM )

! Modifications des aires pour le calcul structure : variable MLG%AIREs
IF (DOM%NUM%STRUCTURE == 'on') THEN
    CALL mesh_aire_str( DOM )
END IF

! Estimation de la qualite de maillage en cas de deformation (ablation)
! et groupement des faces limites autour de chaque sommet limite
IF (DOM%NUM%ABLATION == 'on') THEN
    ALLOCATE( DOM%MLG%QUALITE( ncel,2 ) )   
    ALLOCATE( DOM%MLG%QUALITE_INI( ncel,2 ) )  
    CALL mesh_qualite( DOM ) 
    ! On enregistre les valeurs initiales de la qualité du maillage
    DOM%MLG%QUALITE_INI = DOM%MLG%QUALITE
    CALL mesh_faclim_grp( DOM )  
    CALL mesh_face2som2aretes( DOM ) 
    ! On initialise le rayon de courbure en chaque sommet
    ! CALL mesh_sommet_courbure( DOM )
    ! CALL mesh_courbure( DOM )
    DOM%MLG%SOM_LIM%r_courb = 1E21
    ! On initialise les courbes de Bezier
    CALL mesh_build_bezier( DOM )
    CALL mesh_sommet2arete ( DOM )
    DOM%RSL%remaillage_ab = .FALSE.
END IF

END SUBROUTINE mesh_cpt
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_cpt
