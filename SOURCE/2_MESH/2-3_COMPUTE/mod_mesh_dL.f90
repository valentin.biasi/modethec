!> Module de calcul de dL_min des cellules (distance caracteristique)
MODULE mod_mesh_dL
!
USE mod_cst, ONLY : IP, DP
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> dL_min des cellules (distance caracteristique)
SUBROUTINE mesh_dL( DOM )
! dL_min des cellules (distance caracteristique)
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: ifac, nfac
INTEGER(IP) :: icel, ncel
INTEGER(IP) :: id_fac
REAL(DP) :: perim
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
!
!---- Allocation de dL_min --------------------------------------------!
!~ ALLOCATE( DOM%MLG%dL_min(ncel) )
        
! Boucle sur les cellules
DO icel = 1,ncel

    ! Cherche la distance caracteristique de icel
    ! par rapport aux centres des faces adjacentes
    ! dL_min = 2 * VOLUME(icel) / PERIMETRE(icel)
    perim = 0.0_dp
    
    nfac = DOM%MLG%CEL2FAC(icel,2) ! Nb de faces de la icel
    
    ! Some des aires des faces adj.
    DO ifac = 1,nfac
        id_fac = ABS(DOM%MLG%CEL2FAC(icel,ifac+2)) ! id absolu de ifac
        perim = perim + DOM%MLG%AIRE(id_fac)
    END DO
    
    ! Calcul
    DOM%MLG%dL_min(icel) = 2.0_dp * DOM%MLG%VOLUME(icel) / perim

END DO

END SUBROUTINE mesh_dL
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_dL
