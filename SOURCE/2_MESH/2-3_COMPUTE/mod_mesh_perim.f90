!> Module de calcul des perimetres des cellules
MODULE mod_mesh_perim
!
USE mod_cst, ONLY : IP, DP
USE mod_algebra, ONLY : norm
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Calcul des perimetres des cellules
!> ATTENTION: ne prend en compte que les cellules de type 
!> tetra, hexa, prisme et pyram (et ne fait pas le distingo avec 
!> d'autres types de cellules qui pourraient comporter le meme nbre de sommets
SUBROUTINE mesh_perim( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: icel, ncel
INTEGER(IP) :: i1, i2
REAL(DP) :: perim
INTEGER(IP) ,DIMENSION(2) :: ID_som
REAL(DP), DIMENSION(3) :: PT1, PT2, VEC ! Point 1 et 2 de l arrete, Vecteur 1->2
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel

        
! Boucle sur les cellules
DO icel = 1,ncel
    perim = 0.0_dp
    SELECT CASE (DOM%MLG%CEL2SOM(icel,2))
        CASE(4) ! tetra
            ! Somme des longueurs des arretes de icel
            DO i1 = 1,3
                DO i2 = 1,4 - i1
                    ID_som(1) = DOM%MLG%CEL2SOM(icel,2 + i1) ! ID du premier sommet de l arrete
                    ID_som(2) = DOM%MLG%CEL2SOM(icel,7 - i2) ! ID du second sommet de l arrete
                    PT1 =  DOM%MLG%SOMMET(ID_som(1),:) ! Position du point 1
                    PT2 =  DOM%MLG%SOMMET(ID_som(2),:) ! Position du point 2
                    VEC = PT2 - PT1 ! Vecteur pt1 -> pt2
                    perim = perim + norm(VEC)
                ENDDO
            ENDDO
        CASE(8) ! hexa
            ! Somme des longueurs des arretes de icel
            DO i1 = 1,8
                ID_som(1) = DOM%MLG%CEL2SOM(icel,2 + i1) ! ID du premier sommet de l arrete
                IF (i1 == 4 .OR. i1 == 8) THEN
                    ID_som(2) = DOM%MLG%CEL2SOM(icel,i1 - 1)
                ELSE
                    ID_som(2) = DOM%MLG%CEL2SOM(icel,3 + i1) ! ID du second sommet de l arrete
                ENDIF
                PT1 =  DOM%MLG%SOMMET(ID_som(1),:) ! Position du point 1
                PT2 =  DOM%MLG%SOMMET(ID_som(2),:) ! Position du point 2
                VEC = PT2 - PT1 ! Vecteur pt1 -> pt2
                perim = perim + norm(VEC)
            ENDDO
            DO i1 = 1,4
                ID_som(1) = DOM%MLG%CEL2SOM(icel,2 + i1) ! ID du premier sommet de l arrete
                ID_som(2) = DOM%MLG%CEL2SOM(icel,6 + i1) ! ID du second sommet de l arrete
                PT1 =  DOM%MLG%SOMMET(ID_som(1),:) ! Position du point 1
                PT2 =  DOM%MLG%SOMMET(ID_som(2),:) ! Position du point 2
                VEC = PT2 - PT1 ! Vecteur pt1 -> pt2
                perim = perim + norm(VEC)
            ENDDO
        CASE(6) ! prisme
            ! Somme des longueurs des arretes de icel
            DO i1 = 1,6
                ID_som(1) = DOM%MLG%CEL2SOM(icel,2 + i1) ! ID du premier sommet de l arrete
                IF (i1 == 3 .OR. i1 == 6) THEN
                    ID_som(2) = DOM%MLG%CEL2SOM(icel,i1)
                ELSE
                    ID_som(2) = DOM%MLG%CEL2SOM(icel,3 + i1) ! ID du second sommet de l arrete
                ENDIF
                PT1 =  DOM%MLG%SOMMET(ID_som(1),:) ! Position du point 1
                PT2 =  DOM%MLG%SOMMET(ID_som(2),:) ! Position du point 2
                VEC = PT2 - PT1 ! Vecteur pt1 -> pt2
                perim = perim + norm(VEC)
            ENDDO
            DO i1 = 1,3
                ID_som(1) = DOM%MLG%CEL2SOM(icel,2 + i1) ! ID du premier sommet de l arrete
                ID_som(2) = DOM%MLG%CEL2SOM(icel,5 + i1) ! ID du second sommet de l arrete
                PT1 =  DOM%MLG%SOMMET(ID_som(1),:) ! Position du point 1
                PT2 =  DOM%MLG%SOMMET(ID_som(2),:) ! Position du point 2
                VEC = PT2 - PT1 ! Vecteur pt1 -> pt2
                perim = perim + norm(VEC)
            ENDDO
        CASE(5) ! pyram
            ! Somme des longueurs des arretes de icel
            DO i1 = 1,4
                ID_som(1) = DOM%MLG%CEL2SOM(icel,2 + i1) ! ID du premier sommet de l arrete
                IF (i1 == 4) THEN
                    ID_som(2) = DOM%MLG%CEL2SOM(icel,i1 - 1)
                ELSE
                    ID_som(2) = DOM%MLG%CEL2SOM(icel,3 + i1) ! ID du second sommet de l arrete
                ENDIF
                PT1 =  DOM%MLG%SOMMET(ID_som(1),:) ! Position du point 1
                PT2 =  DOM%MLG%SOMMET(ID_som(2),:) ! Position du point 2
                VEC = PT2 - PT1 ! Vecteur pt1 -> pt2
                perim = perim + norm(VEC)
            ENDDO
            DO i1 = 1,4
                ID_som(1) = DOM%MLG%CEL2SOM(icel,2 + i1) ! ID du premier sommet de l arrete
                ID_som(2) = DOM%MLG%CEL2SOM(icel,7) ! ID du second sommet de l arrete
                PT1 =  DOM%MLG%SOMMET(ID_som(1),:) ! Position du point 1
                PT2 =  DOM%MLG%SOMMET(ID_som(2),:) ! Position du point 2
                VEC = PT2 - PT1 ! Vecteur pt1 -> pt2
                perim = perim + norm(VEC)
            ENDDO
    END SELECT
    DOM%MLG%PERIM(icel) = perim
END DO

END SUBROUTINE mesh_perim
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_perim