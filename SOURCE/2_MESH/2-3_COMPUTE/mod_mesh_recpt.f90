!> Module de MAJ des calculs associes au maillage en cas d'ablation ou de 
!> deformation du maillage generale
MODULE mod_mesh_recpt
!
USE mod_cst, ONLY : pourcentage_qualite_1, pourcentage_qualite_2
USE mod_mesh_aire, ONLY : mesh_aire, mesh_aire_str
USE mod_mesh_axe, ONLY : mesh_axe
USE mod_mesh_dL, ONLY : mesh_dL
USE mod_mesh_vect, ONLY : mesh_vect
USE mod_mesh_vfac, ONLY : mesh_vfac
USE mod_mesh_interp, ONLY : mesh_interp
USE mod_mesh_qualite, ONLY : mesh_qualite, mesh_faclim_grp
USE mod_mesh_limite, ONLY : mesh_face2som2aretes, mesh_build_bezier, mesh_sommet_courbure
USE mod_mesh_rcourb, ONLY : mesh_courbure
USE mod_print, ONLY : print_err
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Routine MAJ des calculs associes au maillage en cas d'ablation
SUBROUTINE mesh_recpt( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM

!--- Relance des routines nécessaires à la mise à jour du maillage mobile
   
! Calcul des aires et des centres des faces
CALL mesh_aire( DOM )

! Distance entre les centres de cellules et l'axe + angle entre axe et X
IF (ALLOCATED( DOM%MLG%dist_AXE_G )) THEN
    CALL mesh_axe( DOM )
END IF

! Calcul des distances caracteristiques par cellule : dL_min
CALL mesh_dL( DOM )

! Calcul des vecteurs G1G2, G1H , HG2 et normes associées
CALL mesh_vect( DOM )

! Calcul des vecteurs normaux aux faces VFAC
CALL mesh_vfac( DOM )

! Calcul des matrices d'interpolations dans CEL_ITP, FAC_ITP et SOM_ITP
DEALLOCATE( DOM%MLG%CEL_ITP )
DEALLOCATE( DOM%MLG%FAC_ITP )
DEALLOCATE( DOM%MLG%SOM_ITP )
CALL mesh_interp( DOM )

! Préparation pour le calcul des matrices jacobiennes (orientation des faces,...)
! DEALLOCATE( DOM%MLG%dir_VFAC )
! CALL mesh_jac( DOM )

! Modifications des aires pour le calcul structure : variable MLG%AIREs
IF (DOM%NUM%STRUCTURE == 'on') THEN
    DEALLOCATE( DOM%MLG%AIREs )
    CALL mesh_aire_str( DOM )
END IF 

! Mise à jour des parametres de qualite du maillage
CALL mesh_qualite ( DOM )  

! Estimation de la qualite de maillage en cas de deformation (ablation)
! et groupement des faces limites autour de chaque sommet limite
IF ( DOM%NUM%ABLATION == 'on' ) THEN
    IF ( maxval ( DOM%MLG%QUALITE( :,1 ) ) > pourcentage_qualite_1*maxval(DOM%MLG%QUALITE_INI( :,1 ) ) ) DOM%RSL%nvoisin_plus_1 = .TRUE.
    IF ( maxval ( DOM%MLG%QUALITE( :,2 ) ) > pourcentage_qualite_2*maxval(DOM%MLG%QUALITE_INI( :,2 ) ) ) DOM%RSL%nvoisin_plus_1 = .TRUE.
    IF ( DOM%RSL%nvoisin_plus_1 .AND. .NOT. DOM%RSL%nvois_max ) THEN
        DOM%NUM%nvois_def = DOM%NUM%nvois_def + 1
        DOM%RSL%update_somvois = .TRUE.
        DOM%RSL%nvoisin_plus_1 = .FALSE.
    ELSE IF ( DOM%RSL%nvoisin_plus_1 .AND. DOM%RSL%nvois_max .AND. .NOT. DOM%RSL%remail ) THEN
        CALL print_err('Remaillage nécessaire...')
        DOM%RSL%remail = .TRUE. ! pour ne pas avoir le message a chaque itération ...
    END IF
    ! On recalcule les données en surface (TO DO : que sur les parties qui ont bougé)
    CALL mesh_faclim_grp( DOM ) 
    CALL mesh_face2som2aretes( DOM ) 
    ! CALL mesh_sommet_courbure( DOM )
    ! CALL mesh_courbure( DOM )
    CALL mesh_build_bezier( DOM ) 
    DOM%MLG%FAC_LIM%matrice_upd = .FALSE.
END IF

 
END SUBROUTINE mesh_recpt
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_recpt
