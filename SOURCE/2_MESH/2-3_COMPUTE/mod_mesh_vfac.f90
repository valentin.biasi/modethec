!> Module de calcul des vecteurs normaux aux faces VFAC
MODULE mod_mesh_vfac
!
USE mod_cst, ONLY : IP, DP, eps_min
USE mod_print, ONLY : print_err
USE mod_algebra, ONLY : norm, cross
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Calcul des vecteurs normaux aux faces VFAC
!> NB : VFAC suit la direction de G1G2
SUBROUTINE mesh_vfac( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: ifac, nfac
REAL(DP), DIMENSION(3) :: A, B, C, D
REAL(DP), DIMENSION(3) :: G1G2
REAL(DP), DIMENSION(3) :: VFAC
REAL(DP) :: scal
!
!---- Récupérations ---------------------------------------------------!
nfac = DOM%MLG%nfac
!
!
! Boucle sur les faces
DO ifac = 1,nfac

    ! Position de trois points de la face ifac
    A = DOM%MLG%SOMMET(DOM%MLG%FAC(ifac,3),:)
    B = DOM%MLG%SOMMET(DOM%MLG%FAC(ifac,4),:)
    IF (DOM%MLG%dim_simu == '3D') THEN
        C = DOM%MLG%SOMMET(DOM%MLG%FAC(ifac,5),:)
    END IF
    
    IF (DOM%MLG%FAC(ifac,2)<4) THEN
        ! Normale à la face = produit vectoriel de deux vecteurs-arretes de la face
        B = B - A
        IF (DOM%MLG%dim_simu == '3D') THEN
            C = C - A
        ELSE
            C = DOM%MLG%DIR_PLAN
        END IF
        VFAC = cross(B, C)
    ELSE IF (DOM%MLG%FAC(ifac,2)==4) THEN
    ! Normale à la face = produit vectoriel des deux diagonales
        D = DOM%MLG%SOMMET(DOM%MLG%FAC(ifac,6),:)
        A = C - A
        B = D - B
        VFAC = CROSS(A,B)
    END IF
    
    VFAC = VFAC/norm(VFAC) ! Normalisation
    G1G2 = DOM%MLG%G1G2(ifac,:) ! Produit scalaire de VFAC par G1G2
    scal = DOT_PRODUCT(VFAC,G1G2)
    
    ! Renversement si de sens opposé
    IF (scal < 0.0_dp) THEN
        VFAC = - VFAC
    END IF
    
    IF (ABS(scal) < 2.0_dp*eps_min) THEN
        CALL print_err('Certains vecteurs normaux sont perpendiculaires a G1G2')
    ENDIF

    DOM%MLG%VFAC(ifac,:) = VFAC ! Ecriture

ENDDO

END SUBROUTINE mesh_vfac
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_vfac
