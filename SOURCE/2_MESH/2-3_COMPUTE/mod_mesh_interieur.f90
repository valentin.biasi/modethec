!< Module de construction des structures de données à l'itérieur du domaine (pour l'ablation)
!
!
MODULE mod_mesh_interieur
!
USE mod_cst, ONLY : DP, IP
USE mod_print, ONLY : print_err
USE mod_algebra, ONLY : cross, norm, INV_MAT2
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Construction des liens entre DOM%MLG% : ARET_INT et SOM_INT
!> On enregistre dans ARET_TEMP les indices des sommets intérieurs
SUBROUTINE mesh_sommet2arete( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: IDSOM_DONE, NBR_ARET_SOM
INTEGER(IP), ALLOCATABLE, DIMENSION(:,:) :: ARET_TEMP
REAL(DP), DIMENSION(3) :: SOM1, SOM2
INTEGER(IP) :: nsom_int, nsom, isom, idsom, ivois, nvoisin, iarete, idvois
INTEGER(IP) :: iivois, iiarete, num_arete, first
!
!---- Récupérations ---------------------------------------------------!
nsom = DOM%MLG%nsom
nsom_int = DOM%MLG%nsom_int
ALLOCATE( DOM%MLG%SOM_INT( nsom_int ) )
ALLOCATE( NBR_ARET_SOM( nsom_int ) )
ALLOCATE( IDSOM_DONE( DOM%MLG%nsom ) )
! On ne peut pas avoir plus de nsom(nsom-1)/2 aretes dans le domaine 
ALLOCATE( ARET_TEMP( nsom*(nsom-1)/2 , 2 ) )
!
iarete = 0
IDSOM_DONE = 0
NBR_ARET_SOM = 0
!
DO isom = 1, nsom_int
    idsom = DOM%MLG%SOM_ADJ_INT( isom, 1 )

    ! Remplissage du tableau IDSOM2ISOM
    DOM%MLG%IDSOM2ISOM(idsom) = isom

    nvoisin = DOM%MLG%SOM_ADJ_INT( isom, 2 )
    DOM%MLG%SOM_INT(isom)%nvoisin = nvoisin
    ALLOCATE( DOM%MLG%SOM_INT(isom)%ARETES( nvoisin ) )
    ALLOCATE( DOM%MLG%SOM_INT(isom)%RAIDEUR( nvoisin ) )
    DOM%MLG%SOM_INT(isom)%RAIDEUR = 1.0

    DO ivois = 1, nvoisin
        first = 0
        idvois = DOM%MLG%SOM_ADJ_INT( isom, 2+ivois )
        SOM1 = DOM%MLG%SOMMET( idsom, : )
        iivois = DOM%MLG%IDSOM2ISOM( idvois )
        IF ( ANY( IDSOM_DONE(1:isom) == idvois ) ) THEN ! On a deja vu cette arete
            ! On la cherche parmis les aretes déjà enregistrées
            DO iiarete = 1, NBR_ARET_SOM(iivois)
                num_arete = DOM%MLG%SOM_INT(iivois)%ARETES(iiarete)
                IF ( ARET_TEMP( num_arete, 2 ) == idsom ) THEN 
                    NBR_ARET_SOM(isom) = NBR_ARET_SOM(isom) + 1
                    DOM%MLG%SOM_INT(isom)%ARETES( NBR_ARET_SOM(isom) ) = num_arete
                END IF
            END DO
        ELSE
            iarete = iarete + 1
            ARET_TEMP( iarete, 1 ) = idsom
            ARET_TEMP( iarete, 2 ) = idvois
            SOM2 = DOM%MLG%SOMMET( idvois, : )
            SOM2 = SOM1 - SOM2
            NBR_ARET_SOM(isom) = NBR_ARET_SOM(isom) + 1
            DOM%MLG%SOM_INT(isom)%ARETES( NBR_ARET_SOM(isom) ) = iarete
        END IF
    END DO
    IDSOM_DONE(isom) = idsom
END DO
! 
DOM%MLG%nbr_aret_int = iarete
ALLOCATE( DOM%MLG%ARET_INT( DOM%MLG%nbr_aret_int ) )
DO iiarete = 1, DOM%MLG%nbr_aret_int
    DOM%MLG%ARET_INT(iiarete)%SOMMETS(1) = ARET_TEMP( iiarete, 1 )
    DOM%MLG%ARET_INT(iiarete)%SOMMETS(2) = ARET_TEMP( iiarete, 2 )
END DO
!
DEALLOCATE( IDSOM_DONE )
DEALLOCATE( ARET_TEMP )
DEALLOCATE( NBR_ARET_SOM )
!
END SUBROUTINE mesh_sommet2arete
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_interieur