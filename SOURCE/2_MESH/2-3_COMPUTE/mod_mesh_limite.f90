!< Module de construction des structures de données liées aux limites (pour l'ablation)
!
!
MODULE mod_mesh_limite
!
USE mod_cst, ONLY : DP, IP, WEIGHT_CTR
USE mod_print, ONLY : print_err
USE mod_algebra, ONLY : cross, norm, INV_MAT2
USE mod_deform3d, ONLY : find_voisins_ntag2
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Construction des liens entre DOM%MLG% : SOM_LIM ; ARET_LIM et FAC_LIM
! A optimiser pour ne modifier que ce qui bouge
SUBROUTINE mesh_face2som2aretes( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ARRETE_DONE, ARETES_NBR, FACES_NBR
INTEGER(IP) :: nsom, nsom_lim, nfac_lim, nsom_fac, isom
INTEGER(IP) :: iarete, idone, ifac_lim, id_fac, iiarete, narete
INTEGER(IP) :: ifac, id_fac_tag
INTEGER(IP) :: id_som1, id_som2, id_som3, id_som4, no_som1, no_som2, no_som3, no_som4
INTEGER(IP) :: multi_som, arete_tag1, arete_tag2, icherche, tag_lim_dimension
REAL(DP), DIMENSION(3) :: SOM1, SOM2, SOM3, SOM4, DSOM
!
!---- Récupérations ---------------------------------------------------!
nsom = DOM%MLG%nsom
nsom_lim = DOM%MLG%nsom_lim
nfac_lim = DOM%MLG%nfac_lim

IF ( .NOT. ALLOCATED( DOM%MLG%ARET_LIM ) ) ALLOCATE( DOM%MLG%ARET_LIM( 3*(nsom_lim-2) ) ) ! Valeur atteinte si que des triangles en surface
IF ( .NOT. ALLOCATED( DOM%MLG%FAC_LIM ) ) ALLOCATE( DOM%MLG%FAC_LIM( nfac_lim ) )

ALLOCATE( ARRETE_DONE( 6*(nsom_lim-2) ) )
ALLOCATE( ARETES_NBR( nsom_lim ) )
ALLOCATE( FACES_NBR( nsom_lim ) )
ARRETE_DONE = 0
ARETES_NBR = 1
FACES_NBR = 1

multi_som = 10
DO WHILE ( nsom_lim > multi_som )
    multi_som = multi_som * 10
END DO

iarete = 0 ! indice de l'arete
idone = 1

DO ifac_lim = 1,nfac_lim
    ! Indice absolu de la face et nombre de sommets de la face
    id_fac = DOM%MLG%FACLIM( ifac_lim , 2 )
    nsom_fac = DOM%MLG%FAC( id_fac , 2 )
    DOM%MLG%FAC_LIM(ifac_lim)%nb_som = nsom_fac

    IF ( nsom_fac == 2 ) THEN
        id_som1 = DOM%MLG%FAC( id_fac , 3 )
        id_som2 = DOM%MLG%FAC( id_fac , 4 )    
        SOM1 = DOM%MLG%SOMMET( id_som1 , : )
        SOM2 = DOM%MLG%SOMMET( id_som2 , : )        
        no_som1 = DOM%MLG%IDSOM2ISOM( id_som1 )
        no_som2 = DOM%MLG%IDSOM2ISOM( id_som2 )   
        IF (DOM%RSL%remaillage_ab) THEN ! Est ce que la topologie du mailllage a changé ?
            IF ( .NOT. ALLOCATED( DOM%MLG%FAC_LIM(ifac_lim)%SOMMETS ) ) ALLOCATE( DOM%MLG%FAC_LIM(ifac_lim)%SOMMETS(2) )
            IF ( .NOT. ALLOCATED( DOM%MLG%FAC_LIM(ifac_lim)%ARETES ) ) ALLOCATE( DOM%MLG%FAC_LIM(ifac_lim)%ARETES(1) )
            IF ( .NOT. ALLOCATED( DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET ) ) ALLOCATE( DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(1) )    
            IF ( .NOT. ALLOCATED( DOM%MLG%FAC_LIM(ifac_lim)%TAG_SOMMET ) ) ALLOCATE( DOM%MLG%FAC_LIM(ifac_lim)%TAG_SOMMET(2) )                
            DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET = 1                
            ! On enregistre les numero des sommets limites
            DOM%MLG%FAC_LIM(ifac_lim)%SOMMETS = (/no_som1, no_som2/)
            ! On enregistre le numero de la face pour les 3 sommets
            DOM%MLG%SOM_LIM(no_som1)%FACES( FACES_NBR(no_som1) ) = ifac_lim
            DOM%MLG%SOM_LIM(no_som2)%FACES( FACES_NBR(no_som2) ) = ifac_lim
            FACES_NBR( no_som1 ) = FACES_NBR( no_som1 ) + 1
            FACES_NBR( no_som2 ) = FACES_NBR( no_som2 ) + 1

            !------ On enregistre les infos de l'unique arete ------!
            iarete = iarete + 1
            DOM%MLG%FAC_LIM(ifac_lim)%ARETES(1) = iarete
            DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(1) = 1
            DOM%MLG%ARET_LIM(iarete)%SOMMETS = (/no_som2, no_som1/)
            DOM%MLG%SOM_LIM(no_som2)%ARETES( ARETES_NBR(no_som2) ) = iarete
            DOM%MLG%SOM_LIM(no_som1)%ARETES( ARETES_NBR(no_som1) ) = iarete
            DOM%MLG%SOM_LIM(no_som2)%SOMMETS_LIM( ARETES_NBR(no_som2) ) = no_som1
            DOM%MLG%SOM_LIM(no_som1)%SOMMETS_LIM( ARETES_NBR(no_som1) ) = no_som2
            ARETES_NBR( no_som2 ) = ARETES_NBR( no_som2 ) + 1
            ARETES_NBR( no_som1 ) = ARETES_NBR( no_som1 ) + 1
            DSOM = SOM2 - SOM1
            DOM%MLG%ARET_LIM(iarete)%raideur = 1.0 / NORM( DSOM )
        END IF

        ! On associe chaque sommet avec l'indice du tag de la face autour de ce sommet
        DO ifac = 1, DOM%MLG%SOMLIM2FACLIM( no_som1,2 ) + 1 
        ! Le +1 sert à faire crasher la simu si on ne trouve pas car c'est pas normal
            id_fac_tag = DOM%MLG%SOMLIM2FACLIM( no_som1, 2+ifac )
            IF ( id_fac_tag == id_fac ) EXIT
        END DO
        DOM%MLG%FAC_LIM(ifac_lim)%TAG_SOMMET(1) = DOM%MLG%FACLIM_GRP_SURF( no_som1, ifac )
        DO ifac = 1, DOM%MLG%SOMLIM2FACLIM( no_som2,2 ) + 1 
        ! Le +1 sert à faire crasher la simu si on ne trouve pas car c'est pas normal
            id_fac_tag = DOM%MLG%SOMLIM2FACLIM( no_som2, 2+ifac )
            IF ( id_fac_tag == id_fac ) EXIT
        END DO
        DOM%MLG%FAC_LIM(ifac_lim)%TAG_SOMMET(2) = DOM%MLG%FACLIM_GRP_SURF( no_som2, ifac )        

        ! CALL print_err('CAS 2D',1)
    ELSE IF (nsom_fac == 3) THEN 
        id_som1 = DOM%MLG%FAC( id_fac , 3 )
        id_som2 = DOM%MLG%FAC( id_fac , 4 )
        id_som3 = DOM%MLG%FAC( id_fac , 5 )
        SOM1 = DOM%MLG%SOMMET( id_som1 , : )
        SOM2 = DOM%MLG%SOMMET( id_som2 , : )
        SOM3 = DOM%MLG%SOMMET( id_som3 , : )
        no_som1 = DOM%MLG%IDSOM2ISOM( id_som1 )
        no_som2 = DOM%MLG%IDSOM2ISOM( id_som2 )
        no_som3 = DOM%MLG%IDSOM2ISOM( id_som3 )

        IF (DOM%RSL%remaillage_ab) THEN ! Est ce que la topologie du mailllage a changé ?
            IF ( .NOT. ALLOCATED( DOM%MLG%FAC_LIM(ifac_lim)%SOMMETS ) ) ALLOCATE( DOM%MLG%FAC_LIM(ifac_lim)%SOMMETS(3) )
            IF ( .NOT. ALLOCATED( DOM%MLG%FAC_LIM(ifac_lim)%ARETES ) ) ALLOCATE( DOM%MLG%FAC_LIM(ifac_lim)%ARETES(3) )
            IF ( .NOT. ALLOCATED( DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET ) ) ALLOCATE( DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(3) )    
            IF ( .NOT. ALLOCATED( DOM%MLG%FAC_LIM(ifac_lim)%TAG_SOMMET ) ) ALLOCATE( DOM%MLG%FAC_LIM(ifac_lim)%TAG_SOMMET(3) )                
            DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET = -1                
            ! On enregistre les numero des sommets limites
            DOM%MLG%FAC_LIM(ifac_lim)%SOMMETS = (/no_som1, no_som2, no_som3/)
            ! On enregistre le numero de la face pour les 3 sommets
            DOM%MLG%SOM_LIM(no_som1)%FACES( FACES_NBR(no_som1) ) = ifac_lim
            DOM%MLG%SOM_LIM(no_som2)%FACES( FACES_NBR(no_som2) ) = ifac_lim
            DOM%MLG%SOM_LIM(no_som3)%FACES( FACES_NBR(no_som3) ) = ifac_lim
            FACES_NBR( no_som1 ) = FACES_NBR( no_som1 ) + 1
            FACES_NBR( no_som2 ) = FACES_NBR( no_som2 ) + 1
            FACES_NBR( no_som3 ) = FACES_NBR( no_som3 ) + 1

            !------ On enregistre les infos de l'arete 1 ------!
            arete_tag1 = id_som3*multi_som + id_som2
            arete_tag2 = id_som2*multi_som + id_som3
            ! On vérifie que l'arete n'est pas encore enregistree
            IF ( ANY( ARRETE_DONE(1:idone-1) == arete_tag1 ) .OR. ANY( ARRETE_DONE(1:idone-1) == arete_tag2 ) ) THEN
                ! Arete deja enregistree, on la cherche autour du sommet 2
                iiarete = -1
                DO icherche = 1, ARETES_NBR(no_som2)
                    iiarete = DOM%MLG%SOM_LIM(no_som2)%ARETES( icherche )
                    IF ( DOM%MLG%ARET_LIM(iiarete)%SOMMETS(1) == no_som3 .OR. DOM%MLG%ARET_LIM(iiarete)%SOMMETS(2) == no_som3 ) EXIT
                END DO  
                DOM%MLG%FAC_LIM(ifac_lim)%ARETES(1) = iiarete
                IF ( DOM%MLG%ARET_LIM(iiarete)%SOMMETS(1) == no_som2 .AND. DOM%MLG%ARET_LIM(iiarete)%SOMMETS(2) == no_som3 ) THEN
                    DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(1) = 1
                ELSE
                    DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(1) = 0
                END IF   
            ELSE
                iarete = iarete + 1
                DOM%MLG%FAC_LIM(ifac_lim)%ARETES(1) = iarete
                DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(1) = 1
                DOM%MLG%ARET_LIM(iarete)%SOMMETS = (/no_som2, no_som3/)
                DOM%MLG%SOM_LIM(no_som2)%ARETES( ARETES_NBR(no_som2) ) = iarete
                DOM%MLG%SOM_LIM(no_som3)%ARETES( ARETES_NBR(no_som3) ) = iarete
                DOM%MLG%SOM_LIM(no_som2)%SOMMETS_LIM( ARETES_NBR(no_som2) ) = no_som3
                DOM%MLG%SOM_LIM(no_som3)%SOMMETS_LIM( ARETES_NBR(no_som3) ) = no_som2
                ARETES_NBR( no_som2 ) = ARETES_NBR( no_som2 ) + 1
                ARETES_NBR( no_som3 ) = ARETES_NBR( no_som3 ) + 1
                DSOM = SOM2 - SOM3
                DOM%MLG%ARET_LIM(iarete)%raideur = 1.0 / NORM( DSOM )

                ARRETE_DONE(idone) = arete_tag1
                ARRETE_DONE(idone+1) = arete_tag2
                idone = idone + 2
            END IF 

            !------ On enregistre les infos de l'arete 2 ------!
            arete_tag1 = id_som1*multi_som + id_som3
            arete_tag2 = id_som3*multi_som + id_som1
            ! On vérifie que l'arete n'est pas encore enregistree
            IF ( ANY( ARRETE_DONE(1:idone-1) == arete_tag1 ) .OR. ANY( ARRETE_DONE(1:idone-1) == arete_tag2 ) ) THEN
                ! Arete deja enregistree, on la cherche autour du sommet 3
                iiarete = -1
                DO icherche = 1, ARETES_NBR(no_som3)
                    iiarete = DOM%MLG%SOM_LIM(no_som3)%ARETES( icherche )
                    IF ( DOM%MLG%ARET_LIM(iiarete)%SOMMETS(1) == no_som1 .OR. DOM%MLG%ARET_LIM(iiarete)%SOMMETS(2) == no_som1 ) EXIT
                END DO  
                DOM%MLG%FAC_LIM(ifac_lim)%ARETES(2) = iiarete
                IF ( DOM%MLG%ARET_LIM(iiarete)%SOMMETS(1) == no_som3 .AND. DOM%MLG%ARET_LIM(iiarete)%SOMMETS(2) == no_som1 ) THEN
                    DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(2) = 1
                ELSE
                    DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(2) = 0
                END IF           
            ELSE
                iarete = iarete + 1
                DOM%MLG%FAC_LIM(ifac_lim)%ARETES(2) = iarete
                DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(2) = 1
                DOM%MLG%ARET_LIM(iarete)%SOMMETS = (/no_som3, no_som1/)
                DOM%MLG%SOM_LIM(no_som3)%ARETES( ARETES_NBR(no_som3) ) = iarete
                DOM%MLG%SOM_LIM(no_som1)%ARETES( ARETES_NBR(no_som1) ) = iarete
                DOM%MLG%SOM_LIM(no_som1)%SOMMETS_LIM( ARETES_NBR(no_som1) ) = no_som3
                DOM%MLG%SOM_LIM(no_som3)%SOMMETS_LIM( ARETES_NBR(no_som3) ) = no_som1                
                ARETES_NBR( no_som3 ) = ARETES_NBR( no_som3 ) + 1
                ARETES_NBR( no_som1 ) = ARETES_NBR( no_som1 ) + 1
                DSOM = SOM3 - SOM1
                DOM%MLG%ARET_LIM(iarete)%raideur = 1.0 / NORM( DSOM )

                ARRETE_DONE(idone) = arete_tag1
                ARRETE_DONE(idone+1) = arete_tag2
                idone = idone + 2
            END IF 

            !------ On enregistre les infos de l'arete 3 ------!
            arete_tag1 = id_som1*multi_som + id_som2
            arete_tag2 = id_som2*multi_som + id_som1
            ! On vérifie que l'arete n'est pas encore enregistree
            IF ( ANY( ARRETE_DONE(1:idone-1) == arete_tag1 ) .OR. ANY( ARRETE_DONE(1:idone-1) == arete_tag2 ) ) THEN
                ! Arete deja enregistree, on la cherche autour du sommet 2
                iiarete = -1
                DO icherche = 1, ARETES_NBR(no_som2)
                    iiarete = DOM%MLG%SOM_LIM(no_som2)%ARETES( icherche )
                    IF ( DOM%MLG%ARET_LIM(iiarete)%SOMMETS(1) == no_som1 .OR. DOM%MLG%ARET_LIM(iiarete)%SOMMETS(2) == no_som1 ) EXIT
                END DO  
                DOM%MLG%FAC_LIM(ifac_lim)%ARETES(3) = iiarete
                IF ( DOM%MLG%ARET_LIM(iiarete)%SOMMETS(1) == no_som1 .AND. DOM%MLG%ARET_LIM(iiarete)%SOMMETS(2) == no_som2 ) THEN
                    DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(3) = 1
                ELSE
                    DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(3) = 0
                END IF         
            ELSE
                iarete = iarete + 1
                DOM%MLG%FAC_LIM(ifac_lim)%ARETES(3) = iarete
                DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(3) = 1
                DOM%MLG%ARET_LIM(iarete)%SOMMETS = (/no_som1, no_som2/)
                DOM%MLG%SOM_LIM(no_som2)%ARETES( ARETES_NBR(no_som2) ) = iarete
                DOM%MLG%SOM_LIM(no_som1)%ARETES( ARETES_NBR(no_som1) ) = iarete
                DOM%MLG%SOM_LIM(no_som1)%SOMMETS_LIM( ARETES_NBR(no_som1) ) = no_som2
                DOM%MLG%SOM_LIM(no_som2)%SOMMETS_LIM( ARETES_NBR(no_som2) ) = no_som1                  
                ARETES_NBR( no_som2 ) = ARETES_NBR( no_som2 ) + 1
                ARETES_NBR( no_som1 ) = ARETES_NBR( no_som1 ) + 1
                DSOM = SOM1 - SOM2
                DOM%MLG%ARET_LIM(iarete)%raideur = 1.0 / NORM( DSOM )

                ARRETE_DONE(idone) = arete_tag1
                ARRETE_DONE(idone+1) = arete_tag2
                idone = idone + 2
            END IF    
        END IF

        ! On associe chaque sommet avec l'indice du tag de la face autour de ce sommet
        DO ifac = 1, DOM%MLG%SOMLIM2FACLIM( no_som1,2 ) + 1 
        ! Le +1 sert à faire crasher la simu si on ne trouve pas car c'est pas normal
            id_fac_tag = DOM%MLG%SOMLIM2FACLIM( no_som1, 2+ifac )
            IF ( id_fac_tag == id_fac ) EXIT
        END DO
        DOM%MLG%FAC_LIM(ifac_lim)%TAG_SOMMET(1) = DOM%MLG%FACLIM_GRP_SURF( no_som1, ifac )
        DO ifac = 1, DOM%MLG%SOMLIM2FACLIM( no_som2,2 ) + 1 
        ! Le +1 sert à faire crasher la simu si on ne trouve pas car c'est pas normal
            id_fac_tag = DOM%MLG%SOMLIM2FACLIM( no_som2, 2+ifac )
            IF ( id_fac_tag == id_fac ) EXIT
        END DO
        DOM%MLG%FAC_LIM(ifac_lim)%TAG_SOMMET(2) = DOM%MLG%FACLIM_GRP_SURF( no_som2, ifac )
        DO ifac = 1, DOM%MLG%SOMLIM2FACLIM( no_som3,2 ) + 1 
        ! Le +1 sert à faire crasher la simu si on ne trouve pas car c'est pas normal
            id_fac_tag = DOM%MLG%SOMLIM2FACLIM( no_som3, 2+ifac )
            IF ( id_fac_tag == id_fac ) EXIT
        END DO
        DOM%MLG%FAC_LIM(ifac_lim)%TAG_SOMMET(3) = DOM%MLG%FACLIM_GRP_SURF( no_som3, ifac )
    
    ELSE IF (nsom_fac == 4) THEN
        id_som1 = DOM%MLG%FAC( id_fac, 3 )
        id_som2 = DOM%MLG%FAC( id_fac, 4 )
        id_som3 = DOM%MLG%FAC( id_fac, 5 )
        id_som4 = DOM%MLG%FAC( id_fac, 6 )
        SOM1 = DOM%MLG%SOMMET( id_som1, : )
        SOM2 = DOM%MLG%SOMMET( id_som2, : )
        SOM3 = DOM%MLG%SOMMET( id_som3, : )
        SOM4 = DOM%MLG%SOMMET( id_som4, : )
        no_som1 = DOM%MLG%IDSOM2ISOM( id_som1 )
        no_som2 = DOM%MLG%IDSOM2ISOM( id_som2 )
        no_som3 = DOM%MLG%IDSOM2ISOM( id_som3 )
        no_som4 = DOM%MLG%IDSOM2ISOM( id_som4 )
        IF ( DOM%RSL%remaillage_ab ) THEN    
            IF ( .NOT. ALLOCATED( DOM%MLG%FAC_LIM(ifac_lim)%SOMMETS ) ) ALLOCATE( DOM%MLG%FAC_LIM(ifac_lim)%SOMMETS(4) )
            IF ( .NOT. ALLOCATED( DOM%MLG%FAC_LIM(ifac_lim)%ARETES ) ) ALLOCATE( DOM%MLG%FAC_LIM(ifac_lim)%ARETES(4) )
            IF ( .NOT. ALLOCATED( DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET ) ) ALLOCATE( DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(4) ) 
            IF ( .NOT. ALLOCATED( DOM%MLG%FAC_LIM(ifac_lim)%TAG_SOMMET ) ) ALLOCATE( DOM%MLG%FAC_LIM(ifac_lim)%TAG_SOMMET(4) )                      
            DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET = -1                
            ! On enregistre les numero des sommets limites
            DOM%MLG%FAC_LIM(ifac_lim)%SOMMETS = (/no_som1, no_som2, no_som3, no_som4/)
            ! On enregistre le numero de la face pour les 4 sommets
            DOM%MLG%SOM_LIM(no_som1)%FACES( FACES_NBR(no_som1) ) = ifac_lim
            DOM%MLG%SOM_LIM(no_som2)%FACES( FACES_NBR(no_som2) ) = ifac_lim
            DOM%MLG%SOM_LIM(no_som3)%FACES( FACES_NBR(no_som3) ) = ifac_lim
            DOM%MLG%SOM_LIM(no_som4)%FACES( FACES_NBR(no_som4) ) = ifac_lim
            FACES_NBR( no_som1 ) = FACES_NBR( no_som1 ) + 1
            FACES_NBR( no_som2 ) = FACES_NBR( no_som2 ) + 1
            FACES_NBR( no_som3 ) = FACES_NBR( no_som3 ) + 1    
            FACES_NBR( no_som4 ) = FACES_NBR( no_som4 ) + 1 

            !------ On enregistre les infos de l'arete 1 ------! entre les sommets P1 et P2
            arete_tag1 = id_som1*multi_som + id_som2
            arete_tag2 = id_som2*multi_som + id_som1
            ! On vérifie que l'arete n'est pas encore enregistree
            IF ( ANY( ARRETE_DONE(1:idone-1) == arete_tag1 ) .OR. ANY( ARRETE_DONE(1:idone-1) == arete_tag2 ) ) THEN
                ! Arete deja enregistree, on la cherche autour du sommet 2
                iiarete = -1
                DO icherche = 1, ARETES_NBR( no_som2 )
                    iiarete = DOM%MLG%SOM_LIM(no_som2)%ARETES(icherche)
                    IF ( DOM%MLG%ARET_LIM(iiarete)%SOMMETS(1) == no_som1 .OR. DOM%MLG%ARET_LIM(iiarete)%SOMMETS(2) == no_som1 ) EXIT
                END DO  
                DOM%MLG%FAC_LIM(ifac_lim)%ARETES(1) = iiarete
                IF ( DOM%MLG%ARET_LIM(iiarete)%SOMMETS(1) == no_som1 .AND. DOM%MLG%ARET_LIM(iiarete)%SOMMETS(2) == no_som2 ) THEN
                    DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(1) = 1
                ELSE
                    DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(1) = 0
                END IF   
            ELSE
                iarete = iarete + 1
                DOM%MLG%FAC_LIM(ifac_lim)%ARETES(1) = iarete
                DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(1) = 1
                DOM%MLG%ARET_LIM(iarete)%SOMMETS = (/no_som1, no_som2/)
                DOM%MLG%SOM_LIM(no_som1)%ARETES( ARETES_NBR(no_som1) ) = iarete
                DOM%MLG%SOM_LIM(no_som2)%ARETES( ARETES_NBR(no_som2) ) = iarete
                DOM%MLG%SOM_LIM(no_som1)%SOMMETS_LIM( ARETES_NBR(no_som1) ) = no_som2
                DOM%MLG%SOM_LIM(no_som2)%SOMMETS_LIM( ARETES_NBR(no_som2) ) = no_som1                
                ARETES_NBR( no_som1 ) = ARETES_NBR( no_som1 ) + 1
                ARETES_NBR( no_som2 ) = ARETES_NBR( no_som2 ) + 1
                DSOM = SOM1 - SOM2
                DOM%MLG%ARET_LIM(iarete)%raideur = 1.0 / NORM( DSOM )

                ARRETE_DONE(idone) = arete_tag1
                ARRETE_DONE(idone+1) = arete_tag2
                idone = idone + 2
            END IF 

            !------ On enregistre les infos de l'arete 2 ------! entre les sommets P1 et P4
            arete_tag1 = id_som1*multi_som + id_som4
            arete_tag2 = id_som4*multi_som + id_som1
            ! On vérifie que l'arete n'est pas encore enregistree
            IF ( ANY( ARRETE_DONE(1:idone-1) == arete_tag1 ) .OR. ANY( ARRETE_DONE(1:idone-1) == arete_tag2 ) ) THEN
                ! Arete deja enregistree, on la cherche autour du sommet 4
                iiarete = -1
                DO icherche = 1, ARETES_NBR(no_som4)
                    iiarete = DOM%MLG%SOM_LIM(no_som4)%ARETES(icherche)
                    IF ( DOM%MLG%ARET_LIM(iiarete)%SOMMETS(1) == no_som1 .OR. DOM%MLG%ARET_LIM(iiarete)%SOMMETS(2) == no_som1 ) EXIT
                END DO  
                DOM%MLG%FAC_LIM(ifac_lim)%ARETES(2) = iiarete
                IF ( DOM%MLG%ARET_LIM(iiarete)%SOMMETS(1) == no_som1 .AND. DOM%MLG%ARET_LIM(iiarete)%SOMMETS(2) == no_som4 ) THEN
                    DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(2) = 1
                ELSE
                    DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(2) = 0
                END IF           
            ELSE
                iarete = iarete + 1
                DOM%MLG%FAC_LIM(ifac_lim)%ARETES(2) = iarete
                DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(2) = 1        
                DOM%MLG%ARET_LIM(iarete)%SOMMETS = (/no_som1, no_som4/)
                DOM%MLG%SOM_LIM(no_som1)%ARETES( ARETES_NBR(no_som1) ) = iarete
                DOM%MLG%SOM_LIM(no_som4)%ARETES( ARETES_NBR(no_som4) ) = iarete
                DOM%MLG%SOM_LIM(no_som1)%SOMMETS_LIM( ARETES_NBR(no_som1) ) = no_som4
                DOM%MLG%SOM_LIM(no_som4)%SOMMETS_LIM( ARETES_NBR(no_som4) ) = no_som1                     
                ARETES_NBR( no_som1 ) = ARETES_NBR( no_som1 ) + 1
                ARETES_NBR( no_som4 ) = ARETES_NBR( no_som4 ) + 1
                DSOM = SOM4 - SOM1
                DOM%MLG%ARET_LIM(iarete)%raideur = 1.0 / NORM( DSOM ) 

                ARRETE_DONE(idone) = arete_tag1
                ARRETE_DONE(idone+1) = arete_tag2
                idone = idone + 2
            END IF 

            !------ On enregistre les infos de l'arete 3 ------! entre les sommets P4 et P3
            arete_tag1 = id_som3*multi_som + id_som4
            arete_tag2 = id_som4*multi_som + id_som3
            ! On vérifie que l'arete n'est pas encore enregistree
            IF ( ANY( ARRETE_DONE(1:idone-1) == arete_tag1 ) .OR. ANY( ARRETE_DONE(1:idone-1) == arete_tag2 ) ) THEN
                ! Arete deja enregistree, on la cherche autour du sommet 4
                iiarete = -1
                DO icherche = 1, ARETES_NBR(no_som4)
                    iiarete = DOM%MLG%SOM_LIM(no_som4)%ARETES(icherche)
                    IF ( DOM%MLG%ARET_LIM(iiarete)%SOMMETS(1) == no_som3 .OR. DOM%MLG%ARET_LIM(iiarete)%SOMMETS(2) == no_som3 ) EXIT
                END DO  
                DOM%MLG%FAC_LIM(ifac_lim)%ARETES(3) = iiarete
                IF ( DOM%MLG%ARET_LIM(iiarete)%SOMMETS(1) == no_som4 .AND. DOM%MLG%ARET_LIM(iiarete)%SOMMETS(2) == no_som3 ) THEN
                    DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(3) = 1
                ELSE
                    DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(3) = 0
                END IF         
            ELSE
                iarete = iarete + 1
                DOM%MLG%FAC_LIM(ifac_lim)%ARETES(3) = iarete
                DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(3) = 1
                DOM%MLG%ARET_LIM(iarete)%SOMMETS = (/no_som4, no_som3/)
                DOM%MLG%SOM_LIM(no_som4)%ARETES( ARETES_NBR(no_som4) ) = iarete
                DOM%MLG%SOM_LIM(no_som3)%ARETES( ARETES_NBR(no_som3) ) = iarete
                DOM%MLG%SOM_LIM(no_som4)%SOMMETS_LIM( ARETES_NBR(no_som4) ) = no_som3
                DOM%MLG%SOM_LIM(no_som3)%SOMMETS_LIM( ARETES_NBR(no_som3) ) = no_som4                     
                ARETES_NBR( no_som4 ) = ARETES_NBR( no_som4 ) + 1
                ARETES_NBR( no_som3 ) = ARETES_NBR( no_som3 ) + 1
                DSOM = SOM4 - SOM3
                DOM%MLG%ARET_LIM(iarete)%raideur = 1.0 / NORM( DSOM )

                ARRETE_DONE(idone) = arete_tag1
                ARRETE_DONE(idone+1) = arete_tag2
                idone = idone + 2
            END IF    

            !------ On enregistre les infos de l'arete 4 ------! entre les sommets P2 et P3
            arete_tag1 = id_som3*multi_som + id_som2
            arete_tag2 = id_som2*multi_som + id_som3
            ! On vérifie que l'arete n'est pas encore enregistree
            IF ( ANY( ARRETE_DONE(1:idone-1) == arete_tag1 ) .OR. ANY( ARRETE_DONE(1:idone-1) == arete_tag2 ) ) THEN
                ! Arete deja enregistree, on la cherche autour du sommet 2
                iiarete = -1
                DO icherche = 1, ARETES_NBR(no_som2)
                    iiarete = DOM%MLG%SOM_LIM(no_som2)%ARETES(icherche)
                    IF ( DOM%MLG%ARET_LIM(iiarete)%SOMMETS(1) == no_som3 .OR. DOM%MLG%ARET_LIM(iiarete)%SOMMETS(2) == no_som3 ) EXIT
                END DO  
                DOM%MLG%FAC_LIM(ifac_lim)%ARETES(4) = iiarete
                IF ( DOM%MLG%ARET_LIM(iiarete)%SOMMETS(1) == no_som2 .AND. DOM%MLG%ARET_LIM(iiarete)%SOMMETS(2) == no_som3 ) THEN
                    DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(4) = 1
                ELSE
                    DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(4) = 0
                END IF         
            ELSE
                iarete = iarete + 1
                DOM%MLG%FAC_LIM(ifac_lim)%ARETES(4) = iarete
                DOM%MLG%FAC_LIM(ifac_lim)%SENS_ARET(4) = 1
                DOM%MLG%ARET_LIM(iarete)%SOMMETS = (/no_som2, no_som3/)
                DOM%MLG%SOM_LIM(no_som2)%ARETES( ARETES_NBR(no_som2) ) = iarete
                DOM%MLG%SOM_LIM(no_som3)%ARETES( ARETES_NBR(no_som3) ) = iarete
                DOM%MLG%SOM_LIM(no_som2)%SOMMETS_LIM( ARETES_NBR(no_som2) ) = no_som3
                DOM%MLG%SOM_LIM(no_som3)%SOMMETS_LIM( ARETES_NBR(no_som3) ) = no_som2                            
                ARETES_NBR( no_som2 ) = ARETES_NBR( no_som2 ) + 1
                ARETES_NBR( no_som3 ) = ARETES_NBR( no_som3 ) + 1
                DSOM = SOM2 - SOM3
                DOM%MLG%ARET_LIM(iarete)%raideur = 1.0 / NORM( DSOM )

                ARRETE_DONE(idone) = arete_tag1
                ARRETE_DONE(idone+1) = arete_tag2
                idone = idone + 2
            END IF    
        END IF

        ! On associe chaque sommet avec l'indice du tag de la face autour de ce sommet
        DO ifac = 1, DOM%MLG%SOMLIM2FACLIM( no_som1,2 ) + 1 
        ! Le +1 sert à faire crasher la simu si on ne trouve pas car c'est pas normal
            id_fac_tag = DOM%MLG%SOMLIM2FACLIM( no_som1, 2+ifac )
            IF ( id_fac_tag == id_fac ) EXIT
        END DO
        DOM%MLG%FAC_LIM(ifac_lim)%TAG_SOMMET(1) = DOM%MLG%FACLIM_GRP_SURF( no_som1, ifac )
        DO ifac = 1, DOM%MLG%SOMLIM2FACLIM( no_som2,2 ) + 1 
        ! Le +1 sert à faire crasher la simu si on ne trouve pas car c'est pas normal
            id_fac_tag = DOM%MLG%SOMLIM2FACLIM( no_som2, 2+ifac )
            IF ( id_fac_tag == id_fac ) EXIT
        END DO
        DOM%MLG%FAC_LIM(ifac_lim)%TAG_SOMMET(2) = DOM%MLG%FACLIM_GRP_SURF( no_som2, ifac )
        DO ifac = 1, DOM%MLG%SOMLIM2FACLIM( no_som3,2 ) + 1 
        ! Le +1 sert à faire crasher la simu si on ne trouve pas car c'est pas normal
            id_fac_tag = DOM%MLG%SOMLIM2FACLIM( no_som3, 2+ifac )
            IF ( id_fac_tag == id_fac ) EXIT
        END DO
        DOM%MLG%FAC_LIM(ifac_lim)%TAG_SOMMET(3) = DOM%MLG%FACLIM_GRP_SURF( no_som3, ifac )
        DO ifac = 1, DOM%MLG%SOMLIM2FACLIM( no_som4,2 ) + 1 
        ! Le +1 sert à faire crasher la simu si on ne trouve pas car c'est pas normal
            id_fac_tag = DOM%MLG%SOMLIM2FACLIM( no_som4, 2+ifac )
            IF ( id_fac_tag == id_fac ) EXIT
        END DO
        DOM%MLG%FAC_LIM(ifac_lim)%TAG_SOMMET(4) = DOM%MLG%FACLIM_GRP_SURF( no_som4, ifac )        

    ELSE
        CALL print_err('Une des faces limites n''a pas 3 ou 4 sommets.',1)
    END IF
END DO

IF ( DOM%RSL%remaillage_ab ) DOM%MLG%nbr_aret_lim = iarete

! Pour les sommets au tag == 2, on associe leurs aretes au nombre de tag du sommet lié
! Dans le cas 2D, le nbr de tag limite est diminué de 1
IF ( DOM%MLG%dim_simu == '3D' ) THEN
    tag_lim_dimension = 2
ELSE
    tag_lim_dimension = 1
END IF
DO isom = 1, nsom_lim
    IF ( DOM%MLG%SOM_LIM(isom)%nb_tags /= tag_lim_dimension ) CYCLE
    DO iarete = 1, DOM%MLG%SOM_LIM(isom)%nvoisin
        narete = DOM%MLG%SOM_LIM(isom)%ARETES(iarete)
        IF ( DOM%MLG%SOM_LIM( DOM%MLG%ARET_LIM(narete)%SOMMETS(1) )%nb_tags > tag_lim_dimension .OR. DOM%MLG%SOM_LIM( DOM%MLG%ARET_LIM(narete)%SOMMETS(2) )%nb_tags > tag_lim_dimension ) THEN
            DOM%MLG%SOM_LIM(isom)%NBTAG_ARET_SOM(iarete) = 2          
        ELSE IF ( DOM%MLG%SOM_LIM( DOM%MLG%ARET_LIM(narete)%SOMMETS(1) )%nb_tags > tag_lim_dimension-1 .AND. DOM%MLG%SOM_LIM( DOM%MLG%ARET_LIM(narete)%SOMMETS(2) )%nb_tags > tag_lim_dimension-1) THEN
            DOM%MLG%SOM_LIM(isom)%NBTAG_ARET_SOM(iarete) = 1
        ELSE
            DOM%MLG%SOM_LIM(isom)%NBTAG_ARET_SOM(iarete) = 0
        END IF
    END DO
END DO

DEALLOCATE(ARRETE_DONE)
DEALLOCATE(ARETES_NBR)
DEALLOCATE(FACES_NBR)

END SUBROUTINE mesh_face2som2aretes
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Construction des courbes de Bezier pour les surfaces
SUBROUTINE mesh_build_bezier( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: iivois, iiarete, iifac, ifac, isom
INTEGER(IP) :: id_som1, id_som2, no_som1, no_som2, nbez
INTEGER(IP) :: tag1s1, tag1s2, tag2s1, tag2s2, iare1, iare2
REAL(DP) :: courbure1, courbure2
REAL(DP), DIMENSION(3) :: DIFF_SOM, TAND, TANF, V1, V2, ND1, ND2, CR
REAL(DP), DIMENSION(3) :: SOM1, SOM2, NOR1, NOR2, COURB1, COURB2, PTS4, SOM3
REAL(DP), DIMENSION(7,3) :: PTS
LOGICAL :: SVOIS_NTAG3, frontiere
!
!---- Récupérations ---------------------------------------------------!
nbez = 6
!
DOM%MLG%SOM_LIM%dist_upd = .FALSE.
!
DO iiarete = 1, DOM%MLG%nbr_aret_lim
    frontiere = .FALSE.
    PTS = 0.0
    no_som1 = DOM%MLG%ARET_LIM(iiarete)%SOMMETS(1)
    no_som2 = DOM%MLG%ARET_LIM(iiarete)%SOMMETS(2)
    id_som1 = DOM%MLG%SOM_ADJ_LIM( no_som1, 1 )
    id_som2 = DOM%MLG%SOM_ADJ_LIM( no_som2, 1 )    
    SOM1 = DOM%MLG%SOMMET( id_som1, : )
    SOM2 = DOM%MLG%SOMMET( id_som2, : )

    ! On calcule la distance moyenne autour des sommets
    IF ( .NOT. DOM%MLG%SOM_LIM(no_som1)%dist_upd ) THEN
        DOM%MLG%SOM_LIM(no_som1)%dist_caract = 0.0_dp
        DO iivois = 1, DOM%MLG%SOM_LIM(no_som1)%nvoisin
            DIFF_SOM = DOM%MLG%SOMMET( DOM%MLG%SOM_ADJ_LIM( DOM%MLG%SOM_LIM(no_som1)%SOMMETS_LIM(iivois) ,1 ), : ) - SOM1
            DOM%MLG%SOM_LIM(no_som1)%dist_caract = DOM%MLG%SOM_LIM(no_som1)%dist_caract +  NORM( DIFF_SOM )
        END DO
        DOM%MLG%SOM_LIM(no_som1)%dist_caract = DOM%MLG%SOM_LIM(no_som1)%dist_caract / DOM%MLG%SOM_LIM(no_som1)%nvoisin
        DOM%MLG%SOM_LIM(no_som1)%dist_upd = .TRUE.
    END IF
    IF ( .NOT. DOM%MLG%SOM_LIM(no_som2)%dist_upd ) THEN
        DOM%MLG%SOM_LIM(no_som2)%dist_caract = 0.0_dp
        DO iivois = 1, DOM%MLG%SOM_LIM(no_som2)%nvoisin
            DIFF_SOM = DOM%MLG%SOMMET( DOM%MLG%SOM_ADJ_LIM( DOM%MLG%SOM_LIM(no_som2)%SOMMETS_LIM(iivois) ,1 ), : ) - SOM2
            DOM%MLG%SOM_LIM(no_som2)%dist_caract = DOM%MLG%SOM_LIM(no_som2)%dist_caract +  NORM( DIFF_SOM )
        END DO
        DOM%MLG%SOM_LIM(no_som2)%dist_caract = DOM%MLG%SOM_LIM(no_som2)%dist_caract / DOM%MLG%SOM_LIM(no_som2)%nvoisin
        DOM%MLG%SOM_LIM(no_som2)%dist_upd = .TRUE.
    END IF

    IF ( DOM%MLG%SOM_LIM(no_som1)%nb_tags == 1 .AND. DOM%MLG%SOM_LIM(no_som2)%nb_tags == 1 ) THEN
        NOR1 = DOM%MLG%SOM_LIM(no_som1)%NORMALES(1,:)
        NOR2 = DOM%MLG%SOM_LIM(no_som2)%NORMALES(1,:)
        ! Calcul des tangentes
        CALL mesh_tangente( SOM1, SOM2, NOR1, NOR2, DOM%MLG%SOM_LIM(no_som1)%dist_caract, DOM%MLG%SOM_LIM(no_som2)%dist_caract, TAND, TANF )
        ! Calcul de la courbure en chaque sommet
        IF (DOM%MLG%SOM_LIM(no_som1)%r_courb > 1E15) THEN
            COURB1 = (/ 0.0 , 0.0 , 0.0 /)
        ELSE
            courbure1 = 1.0 / DOM%MLG%SOM_LIM(no_som1)%r_courb
        COURB1 = -courbure1*( NORM( TAND )**2 )*NOR1
        END IF
        IF (DOM%MLG%SOM_LIM(no_som2)%r_courb > 1E15) THEN
            COURB2 = (/ 0.0 , 0.0 , 0.0 /)
        ELSE 
            courbure2 = 1.0 / DOM%MLG%SOM_LIM(no_som2)%r_courb
        COURB2 = -courbure2*( NORM( TANF )**2 )*NOR2           
        END IF 
        
    ELSE IF ( DOM%MLG%dim_simu == '2D_PLAN' ) THEN
        frontiere = .TRUE.

        IF ( DOM%MLG%SOM_LIM(no_som1)%nb_tags == 2 ) THEN ! alors c'est un coin
            V1 = DOM%MLG%SOM_LIM(no_som1)%NORMALES( 1, : )
            V2 = DOM%MLG%SOM_LIM(no_som1)%NORMALES( 2, : )
            TAND = SOM2 - SOM1
            NOR1 = ( V1 + V2 ) / 2.0
        ELSE
            NOR1 = DOM%MLG%SOM_LIM(no_som1)%NORMALES( 1, : )
            IF ( DOM%MLG%SOM_LIM(no_som1)%SOMMETS_LIM(1) == no_som2 ) THEN
                SOM3 = DOM%MLG%SOMMET( DOM%MLG%SOM_LIM(no_som1)%SOMMETS_LIM(2) , : )
            ELSE
                SOM3 = DOM%MLG%SOMMET( DOM%MLG%SOM_LIM(no_som1)%SOMMETS_LIM(1) , : )
            END IF
            TAND = SOM2 - SOM3
        END IF

        IF ( DOM%MLG%SOM_LIM(no_som2)%nb_tags == 2 ) THEN ! alors c'est un coin
            V1 = DOM%MLG%SOM_LIM(no_som2)%NORMALES( 1, : )
            V2 = DOM%MLG%SOM_LIM(no_som2)%NORMALES( 2, : )
            TANF = SOM2 - SOM1
            NOR2 = ( V1 + V2 ) / 2.0
        ELSE
            NOR2 = DOM%MLG%SOM_LIM(no_som2)%NORMALES( 1, : )
            IF ( DOM%MLG%SOM_LIM(no_som2)%SOMMETS_LIM(1) == no_som1 ) THEN
                SOM3 = DOM%MLG%SOMMET( DOM%MLG%SOM_LIM(no_som2)%SOMMETS_LIM(2) , : )
            ELSE
                SOM3 = DOM%MLG%SOMMET( DOM%MLG%SOM_LIM(no_som2)%SOMMETS_LIM(1) , : )
            END IF
            TANF = SOM2 - SOM3
        END IF        

        TAND = DOM%MLG%SOM_LIM(no_som1)%dist_caract * TAND / NORM( TAND )
        IF ( DOT_PRODUCT( TAND, (SOM2-SOM1) ) < 0.0 ) TAND = -TAND
        TANF = DOM%MLG%SOM_LIM(no_som2)%dist_caract * TANF / NORM( TANF )
        IF ( DOT_PRODUCT( TANF, (SOM2-SOM1) ) < 0.0 ) TANF = -TANF
        COURB1 = (/ 0.0 , 0.0 , 0.0 /) 
        COURB2 = (/ 0.0 , 0.0 , 0.0 /) 

    ELSE
        ! On cherche si l'arete separe deux faces de tag différents et si oui quel est chaque tag
        tag1s1 = -1
        tag2s1 = -1
        tag1s2 = -1
        tag2s2 = -1
        ! On recupere les tags du sommets 1
        DO iifac = 1, DOM%MLG%SOM_LIM(no_som2)%nvoisin
            ifac = DOM%MLG%SOM_LIM(no_som2)%FACES(iifac)
            DO isom = 1, DOM%MLG%FAC_LIM(ifac)%nb_som
                IF ( DOM%MLG%FAC_LIM(ifac)%SOMMETS(isom) == no_som1 ) THEN
                    IF ( tag1s1 == -1 ) THEN
                        tag1s1 = DOM%MLG%FAC_LIM(ifac)%TAG_SOMMET(isom)
                    ELSE IF ( tag2s1 == -1 ) THEN
                        tag2s1 = DOM%MLG%FAC_LIM(ifac)%TAG_SOMMET(isom)
                    ELSE 
                        CALL print_err('error 3 sommets pour 1 arete',1)
                    END IF
                END IF
            END DO
        END DO
        ! On recupere les tags du sommets 2
        DO iifac = 1, DOM%MLG%SOM_LIM(no_som1)%nvoisin
            ifac = DOM%MLG%SOM_LIM(no_som1)%FACES(iifac)
            DO isom = 1, DOM%MLG%FAC_LIM(ifac)%nb_som
                IF ( DOM%MLG%FAC_LIM(ifac)%SOMMETS(isom) == no_som2 ) THEN
                    IF ( tag1s2 == -1 ) THEN
                        tag1s2 = DOM%MLG%FAC_LIM(ifac)%TAG_SOMMET(isom)
                    ELSE IF ( tag2s2 == -1 ) THEN
                        tag2s2 = DOM%MLG%FAC_LIM(ifac)%TAG_SOMMET(isom)
                    ELSE 
                        CALL print_err('error 3 sommets pour 1 arete bis',1)
                    END IF
                END IF
            END DO
        END DO  
        IF ( tag1s1 == tag2s1 ) THEN
            NOR1 = DOM%MLG%SOM_LIM(no_som1)%NORMALES( tag1s1, : )
            NOR2 = DOM%MLG%SOM_LIM(no_som2)%NORMALES( tag1s2, : )
            ! Calcul des tangentes
            CALL mesh_tangente( SOM1, SOM2, NOR1, NOR2, DOM%MLG%SOM_LIM(no_som1)%dist_caract, DOM%MLG%SOM_LIM(no_som2)%dist_caract, TAND, TANF )
            ! Calcul de la courbure en chaque sommet
            IF (DOM%MLG%SOM_LIM(no_som1)%r_courb > 1E15) THEN
                COURB1 = (/ 0.0 , 0.0 , 0.0 /)
            ELSE
                courbure1 = 1.0 / DOM%MLG%SOM_LIM(no_som1)%r_courb
                COURB1 = -courbure1*( NORM( TAND )**2 )*NOR1
            END IF
            IF (DOM%MLG%SOM_LIM(no_som2)%r_courb > 1E15) THEN
                COURB2 = (/ 0.0 , 0.0 , 0.0 /)
            ELSE 
                courbure2 = 1.0 / DOM%MLG%SOM_LIM(no_som2)%r_courb
                COURB2 = -courbure2*( NORM( TANF )**2 )*NOR2            
            END IF          
            ! Calcul de la courbure en chaque sommet
            ! IF (DOM%MLG%SOM_LIM(no_som1)%r_courb > 1E15 .AND. DOM%MLG%SOM_LIM(no_som1)%r_courb > 1E15 ) THEN
            !     COURB1 = (/ 0.0 , 0.0 , 0.0 /)
            !     COURB2 = (/ 0.0 , 0.0 , 0.0 /)
            ! ELSE IF ( DOM%MLG%SOM_LIM(no_som1)%r_courb > 1E15 ) THEN
            !     courbure2 = 1.0 / DOM%MLG%SOM_LIM(no_som2)%r_courb
            !     COURB1 = -courbure2*( NORM( TAND )**2 )*NOR1
            !     COURB2 = -courbure2*( NORM( TANF )**2 )*NOR2            
            ! ELSE IF ( DOM%MLG%SOM_LIM(no_som1)%r_courb > 1E15 ) THEN
            !     courbure1 = 1.0 / DOM%MLG%SOM_LIM(no_som1)%r_courb
            !     COURB1 = -courbure1*( NORM( TAND )**2 )*NOR1
            !     COURB2 = -courbure1*( NORM( TANF )**2 )*NOR2  
            ! ELSE
            !     courbure1 = 1.0 / DOM%MLG%SOM_LIM(no_som1)%r_courb
            !     COURB1 = -courbure1*( NORM( TAND )**2 )*NOR1
            !     courbure2 = 1.0 / DOM%MLG%SOM_LIM(no_som2)%r_courb
            !     COURB2 = -courbure2*( NORM( TANF )**2 )*NOR2            
            ! END IF             
        ELSE 
            frontiere = .TRUE.
            
            ! Pour le calcul des tangentes, on utilise les deux sommets voisins sur la courbe
            ! Tangente du sommet 1
            IF ( DOM%MLG%SOM_LIM(no_som1)%nb_tags < 3 ) THEN
                CALL find_voisins_ntag2 ( DOM , no_som1 , iare1 , iare2 , SVOIS_NTAG3 )
                IF ( DOM%MLG%ARET_LIM(iare1)%SOMMETS(2) == no_som1 .AND. DOM%MLG%ARET_LIM(iare1)%SOMMETS(1) /= no_som2 ) THEN
                    SOM3 = DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare1)%SOMMETS(1), 1 ) , : )
                ELSE IF ( DOM%MLG%ARET_LIM(iare1)%SOMMETS(1) == no_som1 .AND. DOM%MLG%ARET_LIM(iare1)%SOMMETS(2) /= no_som2 ) THEN
                    SOM3 = DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare1)%SOMMETS(2), 1 ) , : )
                ELSE IF ( DOM%MLG%ARET_LIM(iare2)%SOMMETS(2) == no_som1 .AND. DOM%MLG%ARET_LIM(iare2)%SOMMETS(1) /= no_som2 ) THEN
                    SOM3 = DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare2)%SOMMETS(1), 1 ) , : )
                ELSE IF ( DOM%MLG%ARET_LIM(iare2)%SOMMETS(1) == no_som1 .AND. DOM%MLG%ARET_LIM(iare2)%SOMMETS(2) /= no_som2 ) THEN
                    SOM3 = DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare2)%SOMMETS(2), 1 ) , : )   
                ELSE 
                    write(6,*) no_som1, id_som1
                    write(6,*) no_som2, id_som2
                    write(6,*) DOM%MLG%SOM_LIM(no_som1)%nb_tags , DOM%MLG%SOM_LIM(no_som2)%nb_tags
                    write(6,*) tag1s1, tag2s1
                    write(6,*) tag1s2, tag2s2
                    CALL print_err('Erreur pour calculer la tangente sur la courbe1',1)
                END IF      
                TAND = SOM2 - SOM3
            ELSE
                TAND = SOM2 - SOM1
            END IF

            ! Tangente du sommet 2
            IF ( DOM%MLG%SOM_LIM(no_som2)%nb_tags < 3 ) THEN
                CALL find_voisins_ntag2 ( DOM , no_som2 , iare1 , iare2 , SVOIS_NTAG3 )
                IF ( DOM%MLG%ARET_LIM(iare1)%SOMMETS(2) == no_som2 .AND. DOM%MLG%ARET_LIM(iare1)%SOMMETS(1) /= no_som1 ) THEN
                    SOM3 = DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare1)%SOMMETS(1), 1 ) , : )
                ELSE IF ( DOM%MLG%ARET_LIM(iare1)%SOMMETS(1) == no_som2 .AND. DOM%MLG%ARET_LIM(iare1)%SOMMETS(2) /= no_som1 ) THEN
                    SOM3 = DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare1)%SOMMETS(2), 1 ) , : )
                ELSE IF ( DOM%MLG%ARET_LIM(iare2)%SOMMETS(2) == no_som2 .AND. DOM%MLG%ARET_LIM(iare2)%SOMMETS(1) /= no_som1 ) THEN
                    SOM3 = DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare2)%SOMMETS(1), 1 ) , : )
                ELSE IF ( DOM%MLG%ARET_LIM(iare2)%SOMMETS(1) == no_som2 .AND. DOM%MLG%ARET_LIM(iare2)%SOMMETS(2) /= no_som1 ) THEN
                    SOM3 = DOM%MLG%SOMMET( DOM%MLG%SOMLIM2FACLIM( DOM%MLG%ARET_LIM(iare2)%SOMMETS(2), 1 ) , : )   
                ELSE 
                    write(6,*) no_som2, id_som2
                    write(6,*) no_som1, id_som1
                    write(6,*) DOM%MLG%SOM_LIM(no_som1)%nb_tags , DOM%MLG%SOM_LIM(no_som2)%nb_tags
                    write(6,*) tag1s1, tag2s1
                    write(6,*) tag1s2, tag2s2                    
                    CALL print_err('Erreur pour calculer la tangente sur la courbe2',1)
                END IF      
                TANF = SOM1 - SOM3            
            ELSE
                TANF = SOM1 - SOM2  
            END IF
            
            V1 = DOM%MLG%SOM_LIM(no_som1)%NORMALES( tag1s1, : )
            V2 = DOM%MLG%SOM_LIM(no_som1)%NORMALES( tag2s1, : )
            TAND = DOM%MLG%SOM_LIM(no_som1)%dist_caract * TAND / NORM( TAND )
            IF ( DOT_PRODUCT( TAND, (SOM2-SOM1) ) < 0.0 ) TAND = -TAND
            NOR1 = ( V1 + V2 ) / 2.0

            V1 = DOM%MLG%SOM_LIM(no_som2)%NORMALES( tag1s2, : )
            V2 = DOM%MLG%SOM_LIM(no_som2)%NORMALES( tag2s2, : )
            TANF = DOM%MLG%SOM_LIM(no_som2)%dist_caract * TANF / NORM( TANF )
            IF ( DOT_PRODUCT( TANF, (SOM2-SOM1) ) < 0.0 ) TANF = -TANF
            NOR2 = ( V1 + V2 ) / 2.0
            COURB1 = (/ 0.0 , 0.0 , 0.0 /) 
            COURB2 = (/ 0.0 , 0.0 , 0.0 /) 
        END IF
    END IF

    ! Points origines des courbes de beziers
    PTS(1,:) = SOM1
    PTS(7,:) = SOM2

    ! Premier voisin des origines, on utilise les tangentes
    PTS(2,:) = PTS(1,:) + TAND/(nbez*WEIGHT_CTR(2))
    PTS(6,:) = PTS(7,:) - TANF/(nbez*WEIGHT_CTR(6))

    ! Second voisin avec les courbures
    ! ################# POURQUOI UN - a la fin des courbes pour la courbures
    PTS(3,:) = PTS(1,:) + (1/(nbez*(nbez-1)*WEIGHT_CTR(3)))*(COURB1(:)-2*nbez*WEIGHT_CTR(2)*(1-nbez*WEIGHT_CTR(2))*(PTS(2,:)-PTS(1,:)))
    PTS(5,:) = PTS(7,:) - (1/(nbez*(nbez-1)*WEIGHT_CTR(5)))*(-COURB2(:)-2*nbez*WEIGHT_CTR(6)*(1-nbez*WEIGHT_CTR(6))*(PTS(7,:)-PTS(6,:)))

    ! On determine le dernier point de controle
    ND1 = CROSS(NOR1,TAND)
    ND2 = CROSS(NOR2,TANF)
    CR = CROSS(ND1,ND2)
    IF (NORM(CR) < 1E-10 .OR. frontiere) THEN
        PTS(4,:) = ( PTS(3,:) + PTS(5,:) ) / 2
    ELSE
        CALL mesh_inter_PTS4(SOM1,SOM2,ND1,ND2,PTS4)
        PTS(4,:) = PTS4(:)
    END IF  

    DOM%MLG%ARET_LIM(iiarete)%PTS_CTR = PTS
END DO

END SUBROUTINE mesh_build_bezier
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Calcul des tangentes
SUBROUTINE mesh_tangente( SOM1, SOM2, NOR1, NOR2, DIS1, DIS2, TAND, TANF )
!
!---- Déclarations ----------------------------------------------------!
REAL(DP), DIMENSION(3) :: SOM1, SOM2, NOR1, NOR2, TAND, TANF, VECT_TEMP
REAL(DP) :: dis1, dis2
!
! on considere les plans normaux aux droites normales aux sommets 
! l'intersection est dirigee par dir3 = normale1^normale2
VECT_TEMP = SOM2 - SOM1
VECT_TEMP = CROSS( NOR1 , VECT_TEMP )
TAND = CROSS( VECT_TEMP , NOR1 )
IF ( DOT_PRODUCT( TAND , (SOM2-SOM1) ) < 0.0 ) THEN
    TAND = -TAND
END IF
VECT_TEMP = SOM2 - SOM1
VECT_TEMP = CROSS( NOR2 , VECT_TEMP )
TANF = CROSS( VECT_TEMP , NOR2 )
IF ( DOT_PRODUCT( TANF , (SOM2-SOM1) ) < 0.0 ) THEN
    TANF = -TANF
END IF
TAND = TAND / NORM( TAND )
TAND = dis1 * TAND
TANF = TANF / NORM( TANF )
TANF = dis2 * TANF

END SUBROUTINE mesh_tangente
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Calcul du dernier point de controle des courbes de Beziers
SUBROUTINE mesh_inter_PTS4(SOM1, SOM2, ND1, ND2, PTS)
!
!---- Déclarations ----------------------------------------------------!
REAL(DP), DIMENSION(3) :: SOM1, SOM2, ND1, ND2, DIR3, O_DROITE, PTS
REAL(DP) :: lambda_min
REAL(DP), DIMENSION(2) :: second_terme
REAL(DP), DIMENSION(2,2) :: mat_inverse
!
! on considere les plans normaux aux droites normales aux sommets 
! l'intersection est dirigee par dir3 = normale1^normale2
DIR3 = CROSS( ND1 , ND2 )
DIR3 = DIR3 / NORM( DIR3 )
! on cherche un point de la droite pour avoir un point d'origine 
! second terme depend des sommets consideres
second_terme(1) = ND1(1)*SOM1(1) + ND1(2)*SOM1(2) + ND1(3)*SOM1(3)
second_terme(2) = ND2(1)*SOM2(1) + ND2(2)*SOM2(2) + ND2(3)*SOM2(3)
O_DROITE = (/ 0.0, 0.0, 0.0 /)
IF ( ABS( DIR3(3) ) > 0.5 ) THEN
    mat_inverse(1,1) = ND1(1)
    mat_inverse(2,1) = ND1(2)
    mat_inverse(1,2) = ND2(1)
    mat_inverse(2,2) = ND2(2) 
    mat_inverse = INV_MAT2( mat_inverse )
    second_terme = MATMUL( second_terme , mat_inverse )
    O_DROITE(1) = second_terme(1)
    O_DROITE(2) = second_terme(2)
ELSE IF ( ABS( DIR3(2) ) > 0.5 ) THEN
    mat_inverse(1,1) = ND1(1)
    mat_inverse(2,1) = ND1(3)
    mat_inverse(1,2) = ND2(1)
    mat_inverse(2,2) = ND2(3) 
    mat_inverse = INV_MAT2( mat_inverse )
    second_terme = MATMUL( second_terme , mat_inverse )
    O_DROITE(1) = second_terme(1) 
    O_DROITE(3) = second_terme(2)
ELSE IF ( ABS( DIR3(1) ) > 0.5 ) THEN
    mat_inverse(1,1) = ND1(2)
    mat_inverse(2,1) = ND1(3)
    mat_inverse(1,2) = ND2(2)
    mat_inverse(2,2) = ND2(3) 
    mat_inverse = INV_MAT2( mat_inverse )
    second_terme = MATMUL( second_terme , mat_inverse )
    O_DROITE(2) = second_terme(1)
    O_DROITE(3) = second_terme(2)
END IF

    lambda_min = - ( DIR3(1)*( 2*O_DROITE(1) - SOM1(1) - SOM2(1) ) + DIR3(2)*( 2*O_DROITE(2) - SOM1(2) - SOM2(2) ) + &
            & DIR3(3)*( 2*O_DROITE(3) - SOM1(3) - SOM2(3) ) ) / ( 2*( DIR3(1)**2 + DIR3(2)**2 + DIR3(3)**2) )
    PTS = (/ O_DROITE(1) + lambda_min*DIR3(1) , O_DROITE(2) + lambda_min*DIR3(2) , O_DROITE(3) + lambda_min*DIR3(3) /)
    

END SUBROUTINE mesh_inter_PTS4
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> On enregistre les sommets limites qui servent a calculer le rayon de 
!> courbure pour chaque sommet limite
SUBROUTINE mesh_sommet_courbure ( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: num_som, id_som_ref, nsom_lim, nvois_min, ndonem1, ndonen, ivois, isom
INTEGER(IP) :: iisom, idsom, nbr_voisin, boucle
INTEGER(IP), DIMENSION(50) :: SDONE ! a DIMINUER OU BORNER LE SUP
!
nsom_lim = DOM%MLG%nsom_lim
!
nvois_min = 10 ! Nombre de voisins minimum nécessaire pour la méthode "finale"
!
! On boucle sur tous les sommets limites
DO num_som = 1, nsom_lim
    id_som_ref = DOM%MLG%SOM_ADJ_LIM( num_som , 1 )

    ! On regarde si le sommet sépare 2 plans. Si oui, on en calcule pas son rayon de courbure
    IF ( DOM%MLG%SOM_LIM(num_som)%nb_tags > 1 ) THEN
        DOM%MLG%SOM_LIM(num_som)%nb_som_courb = -1
        CYCLE
    END IF

    SDONE = 0 ! remise à zero du vecteur temporaire des sommets voisins à utiliser
    nbr_voisin = 0
    boucle = 0
    
    ! Avant de boucler sur les N voisins, on enregistre le premier cercle de voisins (sans les bords)
    DO isom = 1, DOM%MLG%SOM_LIM(num_som)%nvoisin
        IF ( DOM%MLG%SOM_LIM(DOM%MLG%IDSOM2ISOM( DOM%MLG%SOM_ADJ_LIM( DOM%MLG%SOM_LIM(num_som)%SOMMETS_LIM(isom) , 1 ) ) )%nb_tags > 1 ) CYCLE
        nbr_voisin = nbr_voisin + 1
        SDONE( nbr_voisin ) = DOM%MLG%SOM_ADJ_LIM( DOM%MLG%SOM_LIM(num_som)%SOMMETS_LIM(isom) , 1 )
    END DO    

    ndonem1 = 1
    DO WHILE ( nbr_voisin < nvois_min )
        boucle = boucle + 1
        ndonen = nbr_voisin
        DO isom = ndonem1, ndonen
            idsom = SDONE( isom )
            iisom = DOM%MLG%IDSOM2ISOM( idsom ) 
            IF ( DOM%MLG%SOM_LIM(iisom)%nb_tags > 1 ) CYCLE
            DO ivois = 1, DOM%MLG%SOM_LIM(iisom)%nvoisin
                IF ( .NOT. ANY( SDONE(1:nbr_voisin) == DOM%MLG%SOM_ADJ_LIM( DOM%MLG%SOM_LIM(iisom)%SOMMETS_LIM(ivois) , 1 ) ) .AND. DOM%MLG%SOM_ADJ_LIM( DOM%MLG%SOM_LIM(iisom)%SOMMETS_LIM(ivois) , 1 ) /= id_som_ref) THEN
                    IF ( DOM%MLG%SOM_LIM(DOM%MLG%IDSOM2ISOM( DOM%MLG%SOM_ADJ_LIM( DOM%MLG%SOM_LIM(iisom)%SOMMETS_LIM(ivois) , 1 ) ) )%nb_tags < 2 ) THEN
                        nbr_voisin = nbr_voisin + 1
                        SDONE(nbr_voisin) = DOM%MLG%SOM_ADJ_LIM( DOM%MLG%SOM_LIM(iisom)%SOMMETS_LIM(ivois) , 1 )
                    END IF
                END IF
            END DO
        END DO
        ndonem1 = ndonen + 1
    END DO

    IF ( nbr_voisin < nvois_min ) CALL print_err('Pas assez de sommets pour le calcul du rayon de courbure',1)

    ! On copie le tableau dans DOM%MLG%
    DOM%MLG%SOM_LIM(num_som)%nb_som_courb = nbr_voisin
    IF ( ALLOCATED( DOM%MLG%SOM_LIM(num_som)%SOM_COURB ) ) DEALLOCATE( DOM%MLG%SOM_LIM(num_som)%SOM_COURB )
    ALLOCATE( DOM%MLG%SOM_LIM(num_som)%SOM_COURB(nbr_voisin) )
    ! IF ( .NOT. ALLOCATED( DOM%MLG%SOM_LIM(num_som)%SOM_COURB ) ) ALLOCATE( DOM%MLG%SOM_LIM(num_som)%SOM_COURB(nbr_voisin) )
    DOM%MLG%SOM_LIM(num_som)%SOM_COURB = SDONE(1:nbr_voisin)
    
END DO
!
END SUBROUTINE mesh_sommet_courbure
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_limite