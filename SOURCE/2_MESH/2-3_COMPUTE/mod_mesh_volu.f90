!> Module de alcul des volumes et des centres des cellules
MODULE mod_mesh_volu
!
USE mod_cst, ONLY : IP, DP, eps_min, Pi
USE mod_print, ONLY : print_err
USE mod_algebra, ONLY : cross, norm, normL2
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Routine principale de calcul des volumes et des centres des cellules
SUBROUTINE mesh_volu( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: ifac, nfac_cel, nsom_cel, nsom_fac, i_triangle
INTEGER(IP) :: icel, ncel
INTEGER(IP) :: isom
INTEGER(IP) :: id_fac
INTEGER(IP), DIMENSION(2) :: ID_som
INTEGER(IP) :: sens_pt1, sens_pt2
REAL(DP), DIMENSION(3) :: PT1, PT2, PT3, VEC, VEC2 ! Point 1 et 2 de la face
REAL(DP), DIMENSION(3) :: E1, E2, E3 ! Vecteurs de base
REAL(DP), DIMENSION(3) :: n, G, H, GH, G_bary ! normale a la face, point dans la cellule, vecteur GH
REAL(DP), DIMENSION(3) :: AB, AG ! AB axe de symétrie, AG point A axe -> G
REAL(DP), DIMENSION(3) :: crossABG ! Produit vectoriel de AB sur AG
REAL(DP) :: dist ! Distance H sur axe AB
REAL(DP) :: Vol, normVEC ! Variable de calcul du volume
REAL(DP) :: Gx, Gy, Gz ! Position du centre de gravité
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
!
!---- Allocation de VOLUME, VOLUME_TEMP et G_XYZ ----------------------!
IF (.NOT.ALLOCATED( DOM%MLG%VOLUME )) THEN
    ALLOCATE( DOM%MLG%VOLUME(ncel) )
    ALLOCATE( DOM%MLG%VOLUME_TEMP(ncel) )
    ALLOCATE( DOM%MLG%G_XYZ(ncel,3) )
END IF


AB = DOM%MLG%AXE_2D(4:6)
E1 = (/1.0_dp,0.0_dp,0.0_dp/)
E2 = (/0.0_dp,1.0_dp,0.0_dp/)
E3 = (/0.0_dp,0.0_dp,1.0_dp/)

! Boucle sur les cellules
IF (DOM%MLG%dim_simu == '3D') THEN
    DO icel = 1,ncel
        Vol = 0.0_dp ! Réinit du volume
        Gx = 0.0_dp
        Gy = 0.0_dp
        Gz = 0.0_dp ! Réinit du centre de gravité
        G(:) = 0.0_dp
        
        ! Calcul de G_bary : barycentre des sommets de icel
        G_bary(:) = 0.0_dp
        nsom_cel = DOM%MLG%CEL2SOM(icel,2)
        DO isom = 1,nsom_cel
            G_bary = G_bary + DOM%MLG%SOMMET( DOM%MLG%CEL2SOM(icel,2 + isom), : )
        ENDDO
        G_bary = G_bary / REAL(DOM%MLG%CEL2SOM(icel,2),DP)
        nfac_cel = DOM%MLG%CEL2FAC(icel,2) ! Nb de faces de la icel

        ! Boucle sur les faces de la cellule icel
        DO ifac = 1,nfac_cel
        
            id_fac = DOM%MLG%CEL2FAC(icel,2 + ifac) ! id absolu de ifac
            H = DOM%MLG%H_XYZ(id_fac,:)
            GH = H - G_bary
            
            PT1 = DOM%MLG%SOMMET( DOM%MLG%FAC(id_fac,3), : )
            PT2 = DOM%MLG%SOMMET( DOM%MLG%FAC(id_fac,4), : )
            PT3 = DOM%MLG%SOMMET( DOM%MLG%FAC(id_fac,5), : )

            VEC = PT2 - PT1                       ! Vecteur pt1 -> pt2
            VEC2 = PT3 - PT1                      ! Vecteur pt2 -> pt3
            
            VEC = cross( VEC, VEC2 )
            normVEC = normL2( VEC )
            VEC = VEC / normVEC
            
            IF ( DOT_PRODUCT( GH, VEC ) < 0.0_dp ) THEN
                VEC = - VEC
            END IF
            
            Vol = Vol + DOT_PRODUCT( PT1, VEC ) * DOM%MLG%AIRE(id_fac)
            
            
            nsom_fac = DOM%MLG%FAC(id_fac, 2)         ! Nombre de sommets de la face
            DO i_triangle = 1,nsom_fac - 2
            
                PT2 = DOM%MLG%SOMMET( DOM%MLG%FAC(id_fac, 3+i_triangle), : ) ! Position du point 2
                PT3 = DOM%MLG%SOMMET( DOM%MLG%FAC(id_fac, 4+i_triangle), : ) ! Position du point 3
                
                PT2 = PT2 - PT1
                PT3 = PT3 - PT1
                n = cross(PT2, PT3)

                IF ( DOT_PRODUCT( GH, n ) < 0.0_dp ) n = - n
            
                Gx = Gx + 1./24 * ((PT1(1)+PT2(1))**2 + (PT2(1)+PT3(1))**2 + (PT1(1)+PT3(1))**2) * n(1)
                Gy = Gy + 1./24 * ((PT1(2)+PT2(2))**2 + (PT2(2)+PT3(2))**2 + (PT1(2)+PT3(2))**2) * n(2)
                Gz = Gz + 1./24 * ((PT1(3)+PT2(3))**2 + (PT2(3)+PT3(3))**2 + (PT1(3)+PT3(3))**2) * n(3)
                
            ENDDO
            
        ENDDO
        DOM%MLG%VOLUME(icel) = ABS(Vol) / 3.0_dp
        DOM%MLG%G_XYZ(icel,:) = G_bary
        
    ENDDO

ELSE
    DO icel = 1,ncel
        
        Vol = 0.0_dp ! Réinit du volume
        Gx = 0.0_dp
        Gy = 0.0_dp ! Réinit du centre de gravité
        nfac_cel = DOM%MLG%CEL2FAC(icel,2) ! Nb de faces de la icel

        ! Boucle sur les faces de la cellule icel
        DO ifac = 1,nfac_cel
        
            id_fac = ABS(DOM%MLG%CEL2FAC(icel,ifac+2)) ! id absolu de ifac
            IF (DOM%MLG%CEL2FAC(icel,ifac+2) > 0.0) THEN
                sens_pt1 = 1
                sens_pt2 = 2
            ELSE
                sens_pt1 = 2
                sens_pt2 = 1
            END IF

            ID_som = DOM%MLG%FAC(id_fac,3:4) ! ID des 2 sommets de ifac
            PT1 =  DOM%MLG%SOMMET( ID_som(sens_pt1),1:3 ) ! Position du point 1
            PT2 =  DOM%MLG%SOMMET( ID_som(sens_pt2),1:3 ) ! Position du point 2
        
            ! Formule des trapezes pour calculer les volumes
            Vol = Vol + (PT1(1)*PT2(2) - PT1(2)*PT2(1))
            
            ! Idem calcul de Gx Gy
            Gx= Gx + (PT1(1)+PT2(1)) * (PT1(1)*PT2(2) - PT1(2)*PT2(1))
            Gy = Gy + (PT1(2)+PT2(2)) * (PT1(1)*PT2(2) - PT1(2)*PT2(1))
            
            ! Version barycentre
            ! Gx= Gx + PT1(1)
            ! Gy = Gy + PT1(2)

        END DO

        ! Ecriture du volume
        Vol = Vol / 2.0_dp
        DOM%MLG%VOLUME(icel) = ABS(Vol)
        
        ! Ecriture du centre de gravite
        Gx = Gx / (6.0_dp*Vol)
        Gy = Gy / (6.0_dp*Vol)
        ! Version barycentre
        ! Gx = Gx / nfac_cel
        ! Gy = Gy / nfac_cel
        
        DOM%MLG%G_XYZ(icel,1) = Gx
        DOM%MLG%G_XYZ(icel,2) = Gy
        DOM%MLG%G_XYZ(icel,3) = 0.0_dp

        ! Modifications dans le cas 2D_AXI
        IF (DOM%MLG%dim_simu == '2D_AXI') THEN

            ! Distance entre axe et point H
            AG = DOM%MLG%G_XYZ(icel,1:3) - DOM%MLG%AXE_2D(1:3)
            crossABG = cross(AB,AG)
            dist = norm(crossABG) / norm(AB)
            ! Volume des cels proportionnels à la distance à l'axe de symétrie
            DOM%MLG%VOLUME(icel) = DOM%MLG%VOLUME(icel) * 2.0_dp*Pi * dist

        END IF
    END DO
ENDIF

END SUBROUTINE mesh_volu
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_volu
