!> Module de calcul des vecteurs G1G2, G1H , HG2 et normes associées
MODULE mod_mesh_vect
!
USE mod_cst, ONLY : IP, DP
USE mod_algebra, ONLY : norm
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Calcul des vecteurs G1G2, G1H , HG2 et normes associées
SUBROUTINE mesh_vect( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: ifac, nfac
INTEGER(IP) :: id_cel1, id_cel2
REAL(DP), DIMENSION(3) :: G1, G2, H
REAL(DP), DIMENSION(3) :: G1G2, G1H, HG2
!
!---- Récupérations ---------------------------------------------------!
nfac = DOM%MLG%nfac
!
! Boucle sur les faces
DO ifac = 1,nfac
    
    H = DOM%MLG%H_XYZ(ifac,:) ! Position de H pour ifac
        
    id_cel1 = DOM%MLG%FAC2CEL(ifac,3)
    G1 = DOM%MLG%G_XYZ(id_cel1,:) ! Position de G1
    
    ! Position de G2 pour ifac ( = H si ifac est limite )
    IF (DOM%MLG%FAC2CEL(ifac,2) == 2) THEN
        id_cel2 = DOM%MLG%FAC2CEL(ifac,4)
        G2 = DOM%MLG%G_XYZ(id_cel2,:)
    ELSE
        G2 = DOM%MLG%H_XYZ(ifac,:)
    END IF
    
    ! Calculs des vecteurs
    G1G2 = G2 - G1
    G1H = H - G1
    HG2 = G2 - H
    
    ! Ecriture dans MLG
    DOM%MLG%G1G2(ifac,:) = G1G2
    DOM%MLG%G1H(ifac,:) = G1H
    DOM%MLG%HG2(ifac,:) = HG2
    
    ! Ecriture et calcul des normes
    DOM%MLG%n2_G1G2(ifac) = G1G2(1)**2 + G1G2(2)**2 + G1G2(3)**2
    DOM%MLG%n2_G1H(ifac) = G1H(1)**2 + G1H(2)**2 + G1H(3)**2
    DOM%MLG%n2_HG2(ifac) = HG2(1)**2 + HG2(2)**2 + HG2(3)**2
    
END DO

END SUBROUTINE mesh_vect
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_vect