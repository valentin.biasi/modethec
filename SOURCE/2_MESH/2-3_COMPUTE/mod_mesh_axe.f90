!> Module de calcul de l'angle entre un axe et un point (2D_AXI et ablation)
MODULE mod_mesh_axe
!
USE mod_cst, ONLY : IP, DP
USE mod_algebra, ONLY : cross, normL2
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Angle entre l'axe de symetrie en 2D_AXI et l'axe principal X
!> + Distance entre les centres de cellules et l'axe
SUBROUTINE mesh_axe( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
!
REAL(DP) :: L_axe
REAL(DP) :: scalX
REAL(DP) :: D_axe
REAL(DP), DIMENSION(3) :: AXE
REAL(DP), DIMENSION(3) :: AG, crossAXE_G
REAL(DP), DIMENSION(3) :: AH, crossAXE_H
INTEGER(IP) :: icel, ncel
INTEGER(IP) :: ifac, nfac

!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
nfac = DOM%MLG%nfac        

!----------------------------------------------------------------------!
!--- Angle axe <-> X

! Vecteur axe de symetrie
AXE = DOM%MLG%AXE_2D(4:6)

! Scalaire AXE . X
scalX = AXE(1)

! Longueur du vecteur axe
L_axe = SQRT( AXE(1)**2 + AXE(2)**2 )

! Angle = arccos( scalX / L_axe )
DOM%MLG%angle_axe = ACOS( scalX / L_axe )

!----------------------------------------------------------------------!
!--- Distance AXE <-> G_XYZ
DO icel = 1,ncel

    ! Vecteur Sommet 1 Axe => G(icel)
    AG = DOM%MLG%G_XYZ(icel,1:3) - DOM%MLG%AXE_2D(1:3)
    
    ! Distance Axe -> point G(icel)
    crossAXE_G = cross(AXE,AG)
    D_axe = normL2(crossAXE_G) / normL2(AXE)
    
    ! Enregistrement dans dist_AXE_G
    DOM%MLG%dist_AXE_G(icel) = D_axe
    
END DO

!----------------------------------------------------------------------!
!--- Distance AXE <-> H_XYZ
DO ifac = 1,nfac

    ! Vecteur Sommet 1 Axe => H(ifac)
    AH = DOM%MLG%H_XYZ(ifac,1:3) - DOM%MLG%AXE_2D(1:3)
    
    ! Distance Axe -> point H(ifac)
    crossAXE_H = cross(AXE,AH)
    D_axe = normL2(crossAXE_H) / normL2(AXE)
    
    ! Enregistrement dans dist_AXE_H
    DOM%MLG%dist_AXE_H(ifac) = D_axe
    
END DO

END SUBROUTINE mesh_axe
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Angle entre l'axe du laser et le centre des faces limites
SUBROUTINE mesh_axe_cls( DOM , ilim )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
!
REAL(DP) :: D_axe
REAL(DP), DIMENSION(3) :: AXE
REAL(DP), DIMENSION(3) :: AH, crossAXE_H
INTEGER(IP) :: ifac_lim, nfac_limi, id_fac, ilim

!---- Récupérations ---------------------------------------------------!
nfac_limi = DOM%CDT%LIM(ilim)%nfac_limi   

! Vecteur axe de symetrie
AXE = DOM%CDT%LIM(ilim)%axe(4:6)! - DOM%CDT%LIM(ilim)%axe(1:3)

!----------------------------------------------------------------------!
!--- Distance AXE <-> H_XYZ
DO ifac_lim = 1,nfac_limi

    id_fac = DOM%CDT%LIM(ilim)%ID_FACLIMi(ifac_lim)

    ! Vecteur Sommet 1 Axe => H(ifac)
    AH = DOM%MLG%H_XYZ(id_fac,1:3) - DOM%CDT%LIM(ilim)%axe(1:3)
    
    ! Distance Axe -> point H(ifac)
    crossAXE_H = cross(AXE,AH)
    D_axe = normL2(crossAXE_H) / normL2(AXE)
    
    ! Enregistrement dans dist_AXE_H
    DOM%CDT%LIM(ilim)%dist_axe(ifac_lim) = D_axe
    
END DO

END SUBROUTINE mesh_axe_cls
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_axe