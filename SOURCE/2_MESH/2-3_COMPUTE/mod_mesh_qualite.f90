!> Module de calcul de la qualite du maillage en cas d'ablation
MODULE mod_mesh_qualite
!
USE mod_cst, ONLY : IP, DP, eps_min, angle_surf_abl
USE mod_algebra, ONLY : norm
USE mod_print, ONLY : print_err
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Calcul de la qualite du maillage en cas d'ablation
SUBROUTINE mesh_qualite( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: icel, ncel, nsom, isom, id_som, id_som1, id_som2
REAL(DP), DIMENSION(3) :: v1, v2, diff_pos
REAL(DP) :: qmin, qmax, qe, q, aire_min, aire_max, aire1, aire2
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
nsom = 0
!
! Boucle sur les cellules
DO icel = 1,ncel
    nsom = DOM%MLG%CEL2SOM( icel,2 )
    qe = REAL(nsom - 2,DP)/REAL(nsom,DP) * 180.0_dp
    ! RAZ des valeurs
    qmin = 180.0_dp
    qmax = 0.0_dp
    aire_max = 0.0_dp
    diff_pos = DOM%MLG%SOMMET( DOM%MLG%CEL2SOM( icel,3),: ) - DOM%MLG%SOMMET( DOM%MLG%CEL2SOM( icel,4 ),: )
    aire_min = norm( diff_pos )     
    
    DO isom = 1, nsom
        ! Affectation des trois sommets liés à l'angle courant
        id_som = DOM%MLG%CEL2SOM( icel,2+isom )
        IF ( isom == 1 ) THEN            
            id_som1 = DOM%MLG%CEL2SOM( icel,2+nsom )
            id_som2 = DOM%MLG%CEL2SOM( icel,4 )
        ELSE IF ( isom == nsom ) THEN
            id_som1 = DOM%MLG%CEL2SOM( icel,1+nsom )
            id_som2 = DOM%MLG%CEL2SOM( icel,3 )
        ELSE
            id_som1 = DOM%MLG%CEL2SOM( icel,1 + isom )
            id_som2 = DOM%MLG%CEL2SOM( icel,3 + isom )
        END IF     
        v1 = DOM%MLG%SOMMET( id_som1,: ) - DOM%MLG%SOMMET( id_som,: )
        v2 = DOM%MLG%SOMMET( id_som2,: ) - DOM%MLG%SOMMET( id_som,: )
    
        ! calcul de l'angle et des aires
        aire1 = norm( v1 )
        aire2 = norm( v2 )
        q = 180.0_dp / 3.141592654_dp * acos( ABS( DOT_PRODUCT( v1 / aire1 ,v2 / aire2 ) ) ) 
        
        ! MAJ des aires et angles min/max
        IF ( q < qmin ) THEN
            qmin = q
        END IF
        IF ( q > qmax ) THEN
            qmax = q
        END IF
        IF ( min( aire1,aire2 ) < aire_min ) THEN
            aire_min = min( aire1,aire2 )
        END IF
        IF ( max( aire1,aire2 ) > aire_max ) THEN
            aire_max = max( aire1,aire2 )
           
        END IF
    END DO

    ! Calcul du facteur de forme
    IF ( ABS( aire_min ) < eps_min ) THEN
        DOM%MLG%QUALITE( icel,1 ) = 0.0_dp
    ELSE
        DOM%MLG%QUALITE( icel,1 ) = aire_max / aire_min
    END IF
   
    ! Calcul de l'obliquité équiangle
    DOM%MLG%QUALITE( icel,2 ) = max( ( qmax-qe ) / ( 180.0_dp - qe ), ( qe - qmin ) / qe ) 
END DO

END SUBROUTINE mesh_qualite
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!> Routine principale de construction du tableau FACLIM_GRP
! On calcule aussi les normales au sommets limites pour chaque tag
SUBROUTINE mesh_faclim_grp( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: nsom_lim, nb_fac_max, iisom, isom, tag, ifac, id_som, invert, itag, itag2
INTEGER(IP) :: tag_s, compt
REAL(DP), DIMENSION(3) :: VEC0, VEC1, VEC2
REAL(DP) :: PS, PS2
! INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: IDSOMLIM
! Pour les tableaux suivant, on fixe la taille pour éviter ALLOC/DEALLOC (NTAGS < 10 & NTAG < 6)
REAL(DP), DIMENSION(10,3) :: NORM_TAG
INTEGER(IP), DIMENSION(10) :: NBR_TAG
!
nsom_lim = DOM%MLG%nsom_lim
nb_fac_max = MAXVAL(DOM%MLG%SOMLIM2FACLIM(:,2))
!
IF ( .NOT. ALLOCATED( DOM%MLG%FACLIM_GRP ) ) ALLOCATE( DOM%MLG%FACLIM_GRP( nsom_lim , nb_fac_max ) )
IF ( .NOT. ALLOCATED( DOM%MLG%FACLIM_GRP_SURF ) ) ALLOCATE( DOM%MLG%FACLIM_GRP_SURF( nsom_lim , nb_fac_max ) )
!
! ALLOCATE( IDSOMLIM( nsom_lim ) )
itag2 = 0
compt = 0

! TO DO : Si on est à la première itération ou si on NVOIS augmente, on boucle sur tous les sommets limites
! Sinon, on ne boucle que sur les sommets limites déplacés et équilibrés
! IF (reequilibrage ou 1ere ite) THEN
! ELSE 
! END IF
! IDSOMLIM = DOM%MLG%SOMLIM2FACLIM(:,1) ! Comment ne faire cette boucle que sur les sommets modifiés ?
DO iisom = 1, nsom_lim
    compt = compt + 1
    NORM_TAG = 0.0_dp
    NBR_TAG = 0
    ! id_som = IDSOMLIM( iisom )
    id_som = DOM%MLG%SOMLIM2FACLIM( iisom, 1 )
    isom = DOM%MLG%IDSOM2ISOM( id_som )
    invert = 1
    tag = 1
    tag_s = 1
    DOM%MLG%FACLIM_GRP( isom , : ) = 0
    DOM%MLG%FACLIM_GRP_SURF( isom , : ) = 0    
    DOM%MLG%FACLIM_GRP( isom , 1 ) = tag
    DOM%MLG%FACLIM_GRP_SURF( isom , 1 ) = tag_s
    IF ( DOM%MLG%SOMLIM2FACLIM( isom,2 ) == 1 ) CALL print_err('Un sommet limite n'' qu''une face : impossible !',1)

    DO ifac = 1, DOM%MLG%SOMLIM2FACLIM( isom,2 ) - 1
        VEC0 = DOM%MLG%VFAC( DOM%MLG%SOMLIM2FACLIM( isom,2+ifac ), : )
        NORM_TAG( tag_s, : ) = NORM_TAG( tag_s, : ) + VEC0
        NBR_TAG( tag_s ) = NBR_TAG( tag_s ) + 1
        VEC1 = DOM%MLG%VFAC( DOM%MLG%SOMLIM2FACLIM( isom , 2+ifac+1 ) , : )
        
        ! On compare les normales à 2 faces voisines
        PS = DOT_PRODUCT( VEC0 , VEC1 )
        IF ( PS <= 1000*eps_min ) THEN    
        ! On separe le cas "pic" du cas "cavite"
            VEC2 = DOM%MLG%H_XYZ( DOM%MLG%SOMLIM2FACLIM( isom,2+ifac ), : ) - DOM%MLG%SOMMET( id_som,: )
            PS2 = DOT_PRODUCT( VEC1 , VEC2 )
            IF ( PS2 >= 1000*eps_min ) THEN
                invert = -1
            END IF
            tag = tag + 1
        END IF
        IF ( PS <= COSD( angle_surf_abl ) ) THEN
            tag_s = tag_s + 1
        END IF

        DOM%MLG%FACLIM_GRP( isom , ifac+1 ) = tag
        DOM%MLG%FACLIM_GRP_SURF( isom , ifac+1 ) = tag_s
    END DO

    ! On compare la normale de la premiere et derniere face
    VEC0 = DOM%MLG%VFAC( DOM%MLG%SOMLIM2FACLIM( isom,3 ), : )
    NORM_TAG( tag_s,: ) = NORM_TAG( tag_s,: ) + VEC1
    NBR_TAG( tag_s ) = NBR_TAG( tag_s ) + 1
    PS = DOT_PRODUCT( VEC0 , VEC1 )
    IF ( PS > 1000*eps_min .AND. tag /= 1 ) THEN
        DO ifac = 2, DOM%MLG%SOMLIM2FACLIM( isom,2 )
            IF ( DOM%MLG%FACLIM_GRP( isom,ifac ) == tag ) DOM%MLG%FACLIM_GRP(isom,ifac) = 1
        END DO
    END IF
    IF ( PS > COSD( angle_surf_abl ) .AND. tag_s /= 1 ) THEN
        DO ifac = 2, DOM%MLG%SOMLIM2FACLIM( isom,2 )
            IF ( DOM%MLG%FACLIM_GRP_SURF( isom,ifac ) == tag_s ) DOM%MLG%FACLIM_GRP_SURF( isom,ifac ) = 1
        END DO
        NORM_TAG( 1, : ) = NORM_TAG( 1,: ) + NORM_TAG( tag_s,: )
        NBR_TAG(1) = NBR_TAG(1) + NBR_TAG( tag_s ) 
    END IF    

    ! On inverse les indices des tags pour les cas cavités
    IF ( invert == -1 ) THEN
        DOM%MLG%FACLIM_GRP( isom, : ) = -1.0 * DOM%MLG%FACLIM_GRP( isom, : )
    END IF

    ! On sauvegarde la valeur de la normale au sommet
    DOM%MLG%SOM_LIM(isom)%nb_tags = MAXVAL( ABS( DOM%MLG%FACLIM_GRP_SURF( isom,: ) ) )
    IF ( .NOT. ALLOCATED( DOM%MLG%SOM_LIM(isom)%NORMALES ) ) ALLOCATE( DOM%MLG%SOM_LIM(isom)%NORMALES( 10,3 ) )
    DOM%MLG%SOM_LIM(isom)%NORMALES = 0.0_dp
    DO itag = 1, DOM%MLG%SOM_LIM(isom)%nb_tags
        DOM%MLG%SOM_LIM(isom)%NORMALES( itag,: ) = NORM_TAG( itag,: ) / NBR_TAG( itag )
    END DO
END DO
!
! DEALLOCATE( IDSOMLIM )
!
END SUBROUTINE mesh_faclim_grp
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_qualite
