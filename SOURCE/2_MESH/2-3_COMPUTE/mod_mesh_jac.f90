!> Préparation pour le calcul des matrices jacobiennes
MODULE mod_mesh_jac
!
USE mod_cst, ONLY : IP, DP
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Préparation pour le calcul des matrices jacobiennes (orientation des faces,...)
!> Construction de dir_VFAC(ncel,ncel_adj) tel que :
!> dir_VFAC(icel,jcel_adj) = +1=ext i>j / -1=int j>i / 0=face limite
SUBROUTINE mesh_jac( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: icel, ncel
INTEGER(IP) :: ifac_adj_max, nfac_adj_max
INTEGER(IP) :: jcel
INTEGER(IP) :: id_fac_ij
REAL(DP), DIMENSION(3) :: GiGj, VFACij
REAL(DP) :: direction
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
nfac_adj_max = DOM%MLG%nfac_adj_max
!
!---- Allocation de dir_VFAC ------------------------------------------!
ALLOCATE( DOM%MLG%dir_VFAC(ncel,nfac_adj_max) )
DOM%MLG%dir_VFAC(:,:) = 0

! Boucle sur les cellules
DO icel = 1,ncel
    
    ! Boucle sur les faces adjacentes
    DO ifac_adj_max = 1,nfac_adj_max
        
        ! id de la cel adjacente
        jcel = DOM%MLG%CEL_ADJ(icel,ifac_adj_max+2)
        ! id de la face adjacente
        id_fac_ij = ABS( DOM%MLG%CEL2FAC(icel,ifac_adj_max+2) )
        
        ! on evite les faces limites
        IF (jcel /= 0) THEN
            
            ! Vecteur VFAC oritenté quelquonque (G1G2 de la face)
            VFACij = DOM%MLG%VFAC(id_fac_ij,:)
            ! Vecteur Gi > Gj
            GiGj = DOM%MLG%G_XYZ(jcel,:) - DOM%MLG%G_XYZ(icel,:)
            
            ! Produit scalaire de GiGj par VFACij
            direction = DOT_PRODUCT( VFACij , GiGj )
            
            ! Si VFAC dans le sens de GiGj => dir_VFAC = 1
            IF (direction >= 0.0_dp) THEN
                DOM%MLG%dir_VFAC(icel,ifac_adj_max) = 1
            ! Si VFAC dans le sens opposé à GiGj => dir_VFAC = -1
            ELSE IF (direction <= 0.0_dp) THEN
                DOM%MLG%dir_VFAC(icel,ifac_adj_max) = -1
            END IF
            
        END IF
    
    END DO
END DO

!--- Fin
END SUBROUTINE mesh_jac
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_jac
