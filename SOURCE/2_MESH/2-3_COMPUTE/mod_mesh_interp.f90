!> Module de calcul des matrices d'interpolation et de calculs des gradients
MODULE mod_mesh_interp
!
USE mod_cst, ONLY : IP, DP, eps_min
USE mod_print, ONLY : print_err
USE mod_algebra, ONLY : cross, norm, inttostr, det, multiple_free, QR_DECOMPOSITION, INV_R_MATRIX
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Routine priciaple de calcul des matrices d'interpolation 
!> et de calculs des gradients
SUBROUTINE mesh_interp( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
!
!----- MATRICES D'INTERPOLATION DE CELLULES
CALL mesh_interp_cel( DOM )
!
!----- MATRICES D'INTERPOLATION DE SOMMETS
CALL mesh_interp_som( DOM )
!
!----- MATRICES D'INTERPOLATION DE FACES
CALL mesh_interp_fac( DOM )
!
END SUBROUTINE mesh_interp
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!----- MATRICES D'INTERPOLATION DE CELLULES
SUBROUTINE mesh_interp_cel( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: icel, ncel
INTEGER(IP) :: icel_adj, ncel_adj
INTEGER(IP) :: id_cel_adj, id_cel_center
INTEGER(IP) :: jcel_adj
INTEGER(IP) :: min_cel_adj
INTEGER, PARAMETER :: ncel_adj_max = 6
LOGICAL :: valid_alloc
REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: matA, matQ, invA, invQ, matR
REAL(DP), DIMENSION(3) :: GiGj
REAL(DP) :: n2_GiGj, sum_n2_GiGj
!
!---- Récupérations ---------------------------------------------------!
ncel = DOM%MLG%ncel
!
!---- Allocation de CEL_ITP -------------------------------------------!
ALLOCATE( DOM%MLG%CEL_ITP( ncel ) )
!
! Nombre minimum de cellules pour interpolation moindres carrés
IF (DOM%MLG%dim_simu == "3D") THEN
    min_cel_adj = 4
ELSE
    min_cel_adj = 3
END IF

! Boucle sur les cellules
DO icel = 1,ncel

    valid_alloc = .TRUE. ! Methode moindres  carrés possible, sinon interp lin

    ! Récupération du nombre de cellules adjacentes
    ncel_adj = DOM%MLG%CEL_ADJ(icel,2)
    DOM%MLG%CEL_ITP(icel)%ncel_adj = ncel_adj
    
    ! Verif au moins un voisin direct trouvé
    IF (ncel_adj < 1) THEN
        CALL print_err('La cellule '//TRIM(ADJUSTL(inttostr(icel)))//' a 0 cellule adjacente => Calcul des gradients')
        CALL print_err(' Cas non implemente - Voir la routine mesh_interp')
    END IF

    ! Allocation en fonction du nombre de points utilisés
    ! IF (ncel_adj >= min_cel_adj) THEN
    !     ALLOCATE(DOM%MLG%CEL_ITP(icel)%CEL_ADJ(ncel_adj))
    ! ELSE IF (ncel_adj < min_cel_adj) THEN
    ALLOCATE(DOM%MLG%CEL_ITP(icel)%CEL_ADJ(ncel_adj + 1))
    ! ENDIF

    ! On récupère les ID des cel adj grace au tableau CEL_ADJ
    ! Attention aux valeurs non contigues dans MLG%CEL_ADJ
    jcel_adj = 1
    DO icel_adj = 1,ncel_adj_max
        id_cel_adj = DOM%MLG%CEL_ADJ(icel,icel_adj + 2)
        IF (id_cel_adj .NE. 0) THEN
            DOM%MLG%CEL_ITP(icel)%CEL_ADJ(jcel_adj) = id_cel_adj
            jcel_adj = jcel_adj + 1
        END IF
    ENDDO

    ! Si on est dans un coin, on ajoute la cellule centrale pour le calcul des gradients
    ! IF (coin) THEN
    ncel_adj = ncel_adj + 1
    DOM%MLG%CEL_ITP(icel)%CEL_ADJ(ncel_adj) = icel
    DOM%MLG%CEL_ITP(icel)%ncel_adj = ncel_adj
    ! ENDIF

    ! Allocation des matrices utilisées pour le calcul
    ALLOCATE( matA(ncel_adj, min_cel_adj) )
    ALLOCATE( matQ(ncel_adj, min_cel_adj) )
    ALLOCATE( invA(min_cel_adj, ncel_adj) )
    ALLOCATE( invQ(min_cel_adj, ncel_adj) )
    ALLOCATE( matR(min_cel_adj, min_cel_adj) )

    ! On remplit la matrice A de AX=B
    DO icel_adj = 1,ncel_adj
        id_cel_adj = DOM%MLG%CEL_ITP(icel)%CEL_ADJ(icel_adj)

        matA(icel_adj,1) = DOM%MLG%G_XYZ(id_cel_adj,1)
        matA(icel_adj,2) = DOM%MLG%G_XYZ(id_cel_adj,2)

        IF (DOM%MLG%dim_simu == "3D") THEN
            matA(icel_adj,3) = DOM%MLG%G_XYZ(id_cel_adj,3)
            matA(icel_adj,4) = 1.0_dp
        ELSE
            matA(icel_adj,3) = 1.0_dp
        END IF
    ENDDO

    ! Si dim_simu == 1D ou nombre de cellule min non atteint, inversion de matA impossible
    IF (DOM%MLG%dim_simu == "1D")  valid_alloc = .FALSE.
    IF (ncel_adj < min_cel_adj)  valid_alloc = .FALSE.
    
    ! Dans le cas 3D, si tous les points sont coplanaires, interpolation impossible => interpolation linéaire
    IF ((DOM%MLG%dim_simu == "3D") .AND. check_coplanar( matA )) valid_alloc = .FALSE.

    ! Calcul des matrices d'interpolation si non 1D ou si non coin 2D/3D
    IF (valid_alloc == .TRUE.) THEN

        ! Décomposition de A en A = QR
        CALL QR_DECOMPOSITION(matA,matQ,matR)

        ! On inverse R
        CALL INV_R_MATRIX(matR)
        ! Et on transpose Q
        invQ = TRANSPOSE( matQ )
        ! Quasi-inverse de A
        ! A^-1 = R^-1 * Q^t
        invA = MATMUL( matR,invQ )

        ! ! Dans le cas 3D, si tous les points sont coplanaires, interpolation impossible => interpolation linéaire
        ! IF ((DOM%MLG%dim_simu == "3D") .AND. check_coplanar( matA )) valid_alloc = .FALSE.

    END IF

    ! Allocation de M_itp
    ALLOCATE( DOM%MLG%CEL_ITP(icel)%M_itp(min_cel_adj, ncel_adj) )
    DOM%MLG%CEL_ITP(icel)%M_itp(:,:) = 0.0_dp

    ! Si méthode moindres carrés est possible ...
    IF (valid_alloc == .TRUE.) THEN

        ! On remplit la matrice d'interpolation avec l'inverse de A
        DOM%MLG%CEL_ITP(icel)%M_itp = invA
    
    ! Sinon interpolaton linéaire
    ELSE

        id_cel_center = DOM%MLG%CEL_ITP(icel)%CEL_ADJ(ncel_adj)
        sum_n2_GiGj = 0.0_dp

        ! Pour chaque cellule adjacente de icel
        DO icel_adj = 1,ncel_adj-1

            id_cel_adj = DOM%MLG%CEL_ITP(icel)%CEL_ADJ(icel_adj)

            ! Calcul de GiGj en icel et id_cel_adj
            GiGj = DOM%MLG%G_XYZ(id_cel_adj,:)
            GiGj = GiGj - DOM%MLG%G_XYZ(id_cel_center,:)

            ! Norme de GiGj
            n2_GiGj = norm( GiGj )
            sum_n2_GiGj = sum_n2_GiGj + n2_GiGj

            ! Remplissage de la colonne icel_adj
            DOM%MLG%CEL_ITP(icel)%M_itp(1,icel_adj) = GiGj(1) / n2_GiGj
            DOM%MLG%CEL_ITP(icel)%M_itp(2,icel_adj) = GiGj(2) / n2_GiGj 
            IF (DOM%MLG%dim_simu == "3D") THEN
                DOM%MLG%CEL_ITP(icel)%M_itp(3,icel_adj) = GiGj(3) / n2_GiGj
            END IF

        END DO ! Fin boucle icel_adj

        ! Remplissage de la colonne ncel_adj (correspond à icel)
        DOM%MLG%CEL_ITP(icel)%M_itp(1,ncel_adj) = -SUM( DOM%MLG%CEL_ITP(icel)%M_itp(1, 1:ncel_adj-1) )
        DOM%MLG%CEL_ITP(icel)%M_itp(2,ncel_adj) = -SUM( DOM%MLG%CEL_ITP(icel)%M_itp(2, 1:ncel_adj-1) )
        IF (DOM%MLG%dim_simu == "3D") THEN
            DOM%MLG%CEL_ITP(icel)%M_itp(3,ncel_adj) = -SUM( DOM%MLG%CEL_ITP(icel)%M_itp(3, 1:ncel_adj-1) )
        END IF
        
        ! Division par somme(norme(GiGj))
        DOM%MLG%CEL_ITP(icel)%M_itp(:,:) = DOM%MLG%CEL_ITP(icel)%M_itp(:,:) / sum_n2_GiGj

        IF (DOM%MLG%dim_simu == "3D") THEN
            DOM%MLG%CEL_ITP(icel)%M_itp(4,ncel_adj) = 1.0_dp
        ELSE
            DOM%MLG%CEL_ITP(icel)%M_itp(3,ncel_adj) = 1.0_dp
        END IF

    END IF

    ! On désalloue les matrices pour la prochaine cellule
    DEALLOCATE(matA,matQ,invA,invQ,matR)

    DOM%MLG%CEL_ITP(icel)%G_XYZ(:) = DOM%MLG%G_XYZ(icel,:)

END DO ! Fin boucle icel

END SUBROUTINE mesh_interp_cel
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!----- MATRICES D'INTERPOLATION DE SOMMETS
SUBROUTINE mesh_interp_som( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: isom, nsom
INTEGER(IP) :: icel, ncel
INTEGER(IP) :: id_som, nsom_cel
INTEGER(IP) :: min_cel_adj
INTEGER(IP), ALLOCATABLE, DIMENSION(:) :: ncel_adj_som
INTEGER(IP) :: icel_adj, ncel_adj, id_cel_adj
REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: matA, matQ, invA, invQ, matR
REAL(DP), DIMENSION(3) :: pt_G
REAL(DP), ALLOCATABLE, DIMENSION(:) :: A
LOGICAL :: valid_alloc
!
!---- Récupérations ---------------------------------------------------!
nsom = DOM%MLG%nsom
ncel = DOM%MLG%ncel
!
!---- Allocations -----------------------------------------------------!
ALLOCATE( DOM%MLG%SOM_ITP( nsom ) )
ALLOCATE( ncel_adj_som( nsom ) )

! Nombre minimum de cellules pour interpolation moindres carrés
IF (DOM%MLG%dim_simu == "3D") THEN
    min_cel_adj = 4
ELSE
    min_cel_adj = 3
END IF

ALLOCATE( A(min_cel_adj) ) ! Allocation vecteur A (position d'interpolation)

! On cherche le nombre de cel adjacentes à chaque som -----------------!
ncel_adj_som(:) = 0
DO icel = 1,ncel
    nsom_cel = DOM%MLG%CEL2SOM(icel,2)

    DO isom = 1,nsom_cel
        id_som = DOM%MLG%CEL2SOM(icel,2 + isom)
        ncel_adj_som(id_som) = ncel_adj_som(id_som) + 1
    END DO
END DO

! On alloue la structure SOM_ITP(isom)%CEL_ADJ ------------------------!
DO isom = 1,nsom
    DOM%MLG%SOM_ITP(isom)%ncel_adj = ncel_adj_som(isom)
    ALLOCATE(DOM%MLG%SOM_ITP(isom)%CEL_ADJ(ncel_adj_som(isom)))
END DO

! On remplit la structure SOM_ITP -------------------------------------!
ncel_adj_som(:) = 0
DO icel = 1,ncel
    nsom_cel = DOM%MLG%CEL2SOM(icel,2)

    DO isom = 1,nsom_cel
        id_som = DOM%MLG%CEL2SOM(icel,2 + isom)    
        ncel_adj_som( id_som ) = ncel_adj_som( id_som ) + 1
        DOM%MLG%SOM_ITP( id_som )%CEL_ADJ( ncel_adj_som( id_som ) ) = icel
    ENDDO
ENDDO


! Boucle sur les sommets
DO isom = 1,nsom

    valid_alloc = .TRUE. ! Methode moindres  carrés possible, sinon interp lin

    ncel_adj = DOM%MLG%SOM_ITP(isom)%ncel_adj

    ! On remplit la position du sommet
    DOM%MLG%SOM_ITP(isom)%S_XYZ = DOM%MLG%SOMMET(isom,:)
    
    ! Allocation des matrices utilisées pour le calcul
    ALLOCATE( matA(ncel_adj, min_cel_adj) )
    ALLOCATE( matQ(ncel_adj, min_cel_adj) )
    ALLOCATE( invA(min_cel_adj, ncel_adj) )
    ALLOCATE( invQ(min_cel_adj, ncel_adj) )
    ALLOCATE( matR(min_cel_adj, min_cel_adj) )

    ! On remplit la matrice A de AX=B
    DO icel_adj = 1,ncel_adj
        id_cel_adj = DOM%MLG%SOM_ITP(isom)%CEL_ADJ(icel_adj)

        matA(icel_adj,1) = DOM%MLG%G_XYZ(id_cel_adj, 1)
        matA(icel_adj,2) = DOM%MLG%G_XYZ(id_cel_adj, 2)

        IF (DOM%MLG%dim_simu == "3D") THEN
            matA(icel_adj,3) = DOM%MLG%G_XYZ(id_cel_adj,3)
            matA(icel_adj,4) = 1.0_dp
        ELSE
            matA(icel_adj,3) = 1.0_dp
        END IF

    END DO

    ! Si sommet sur limite en 3D ou nombre de cellule min non atteint, inversion de matA impossible
    IF (DOM%MLG%dim_simu == "3D") THEN
        ! IF (DOM%MLG%SOMLIM(isom,2) /= 0) valid_alloc = .FALSE.
        IF (ncel_adj < min_cel_adj+3)  valid_alloc = .FALSE.
    ELSE
        IF (ncel_adj < min_cel_adj)  valid_alloc = .FALSE.
    END IF
    
    ! Calcul de la matrice d'interpolation ----------------------------!
    IF (valid_alloc == .TRUE.) THEN

        ! Décomposition de A en A = QR
        CALL QR_DECOMPOSITION(matA,matQ,matR)

        ! On inverse R
        CALL INV_R_MATRIX(matR)
        ! Et on transpose Q
        invQ = TRANSPOSE(matQ)
        ! Quasi-inverse de A
        ! A^-1 = R^-1 * Q^t
        invA = MATMUL(matR,invQ)

        ! Dans le cas 3D, si tous les points sont coplanaires, interpolation impossible => interpolation linéaire
        IF ((DOM%MLG%dim_simu == "3D") .AND. check_coplanar( matA )) valid_alloc = .FALSE.
        
    END IF

    ! Si méthode moindres carrés est possible ...
    IF (valid_alloc == .TRUE.) THEN

        !On remplit la matrice d'interpolation avec l'inverse de A
        ALLOCATE( DOM%MLG%SOM_ITP(isom)%M_itp(min_cel_adj, ncel_adj) )
        DOM%MLG%SOM_ITP(isom)%M_itp = invA

        ! Vecteur A de position d'interpolation
        A(1:2) = DOM%MLG%SOM_ITP(isom)%S_XYZ(1:2)
        IF (DOM%MLG%dim_simu == "3D") THEN
            A(3) = DOM%MLG%SOM_ITP(isom)%S_XYZ(3)
            A(4) = 1.0_dp
        ELSE
            A(3) = 1.0_dp
        END IF

        ! Vecteur d'interpolation directe
        ALLOCATE( DOM%MLG%SOM_ITP(isom)%A_itp( ncel_adj ) )
        DOM%MLG%SOM_ITP(isom)%A_itp(:) = MATMUL(TRANSPOSE(invA), A)

    ! Sinon extrapolation linéaire
    ELSE

        ! Point G pour extrapolation
        pt_G = 0.0_dp
        DO icel_adj = 1,ncel_adj
            pt_G = pt_G + DOM%MLG%G_XYZ(DOM%MLG%SOM_ITP(isom)%CEL_ADJ(icel_adj),:)
        ENDDO
        pt_G = pt_G/REAL(ncel_adj,DP)

        ! Vecteur de G(cellule) vers S(sommet) pour extrapol.
        DOM%MLG%SOM_ITP(isom)%vec_GS = DOM%MLG%SOMMET(isom,:) - pt_G

    END IF

    ! On désalloue les matrices pour la prochaine cellule
    DEALLOCATE(matA,matQ,invA,invQ,matR)

END DO ! Fin boucle isom

DEALLOCATE(A) ! Désallocation vecteur A de position d'interpolation

END SUBROUTINE mesh_interp_som
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!----- MATRICES D'INTERPOLATION DE FACES
SUBROUTINE mesh_interp_fac( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: ifac, nfac
INTEGER(IP) :: isom, nsom, id_som
INTEGER(IP) :: icel, ncel, id_cel
INTEGER(IP) :: npoint, min_npoint
REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: matA, matQ, invA, invQ, matR
REAL(DP), ALLOCATABLE, DIMENSION(:) :: A
!
!---- Récupérations ---------------------------------------------------!
nfac = DOM%MLG%nfac
!
!---- Allocation de CEL_SOM -------------------------------------------!
ALLOCATE( DOM%MLG%FAC_ITP( nfac ) )

! Nombre minimum de cellules pour interpolation moindres carrés
IF (DOM%MLG%dim_simu == "3D") THEN
    min_npoint = 4
ELSE
    min_npoint = 3
END IF

ALLOCATE( A(min_npoint) ) ! Allocation vecteur A (position d'interpolation)

! Boucle sur les faces
DO ifac = 1,nfac
    ! Position de la face à interpoler
    DOM%MLG%FAC_ITP(ifac)%H_XYZ = DOM%MLG%H_XYZ(ifac,:)

    ! Nombre de cellules voisines de chaque face (2 en interne, 1 aux limites)
    ncel = DOM%MLG%FAC2CEL(ifac,2)
    DOM%MLG%FAC_ITP(ifac)%ncel_adj = ncel

    ! ID des cellules voisines
    ALLOCATE( DOM%MLG%FAC_ITP(ifac)%CEL_ADJ(ncel) )
    DOM%MLG%FAC_ITP(ifac)%CEL_ADJ = DOM%MLG%FAC2CEL(ifac,3:2 + ncel)

    ! Nombre de sommets voisins de chaque face (en 3D : 4, 8, 6 ou 5)
    nsom = DOM%MLG%FAC(ifac,2)
    DOM%MLG%FAC_ITP(ifac)%nsom_adj = nsom

    ! ID des sommets voisins
    ALLOCATE( DOM%MLG%FAC_ITP(ifac)%SOM_ADJ(nsom) )
    DOM%MLG%FAC_ITP(ifac)%SOM_ADJ = DOM%MLG%FAC(ifac,3:2 + nsom)

    ! Total de points servant à l'interpolation
    npoint = ncel + nsom
    IF (npoint < min_npoint) THEN
            CALL print_err('L''interpolation aux faces necessite au moins 4 points')
            CALL print_err('Voir le fichier mesh_interp.f90')
    END IF

    ! Allocation des matrices utilisées pour le calcul
    ALLOCATE( matA(npoint, min_npoint) )
    ALLOCATE( matQ(npoint, min_npoint) )
    ALLOCATE( invA(min_npoint, npoint) )
    ALLOCATE( invQ(min_npoint, npoint) )
    ALLOCATE( matR(min_npoint,min_npoint) )

    ! On remplit la matrice A de AX=B
    ! Avec les coeffs liés aux cellules
    DO icel = 1,ncel
        id_cel = DOM%MLG%FAC_ITP(ifac)%CEL_ADJ(icel)

        matA(icel,1) = DOM%MLG%G_XYZ(id_cel,1)
        matA(icel,2) = DOM%MLG%G_XYZ(id_cel,2)

        IF (DOM%MLG%dim_simu == "3D") THEN
            matA(icel,3) = DOM%MLG%G_XYZ(id_cel,3)
            matA(icel,4) = 1.0_dp
        ELSE
            matA(icel,3) = 1.0_dp
        END IF
    END DO

    ! Puis avec ceux des sommets voisins
    DO isom = 1,nsom
        id_som = DOM%MLG%FAC_ITP(ifac)%SOM_ADJ(isom)

        matA(ncel+isom,1) = DOM%MLG%SOMMET(id_som,1)
        matA(ncel+isom,2) = DOM%MLG%SOMMET(id_som,2)

        IF (DOM%MLG%dim_simu == "3D") THEN
            matA(ncel+isom,3) = DOM%MLG%SOMMET(id_som,3)
            matA(ncel+isom,4) = 1.0_dp
        ELSE
            matA(ncel+isom,3) = 1.0_dp
        END IF
    END DO

    ! Décomposition de A en A = QR
    CALL QR_DECOMPOSITION(matA,matQ,matR)

    ! On inverse R
    CALL INV_R_MATRIX(matR)
    ! Et on transpose Q
    invQ = TRANSPOSE(matQ)
    ! Quasi-inverse de A
    ! A^-1 = R^-1 * Q^t
    invA = MATMUL(matR,invQ)

    ! On remplit la matrice d'interpolation avec l'inverse de A
    ALLOCATE( DOM%MLG%FAC_ITP(ifac)%M_itp(min_npoint, npoint) )
    DOM%MLG%FAC_ITP(ifac)%M_itp = invA

    ! Vecteur A de position d'interpolation
    A(1:2) = DOM%MLG%FAC_ITP(ifac)%H_XYZ(1:2)
    IF (DOM%MLG%dim_simu == "3D") THEN
        A(3) = DOM%MLG%FAC_ITP(ifac)%H_XYZ(3)
        A(4) = 1.0_dp
    ELSE
        A(3) = 1.0_dp
    END IF

    ! Vecteur d'interpolation directe
    ALLOCATE( DOM%MLG%FAC_ITP(ifac)%A_itp( npoint ) )
    DOM%MLG%FAC_ITP(ifac)%A_itp(:) = MATMUL(TRANSPOSE(invA), A)

    ! On désalloue les matrices pour la prochaine face
    DEALLOCATE(matA, matQ, invA, invQ, matR)
    
END DO ! Fin boucle ifac

END SUBROUTINE mesh_interp_fac
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!----- VERIFICATION COPLANARITE DES POINTS
FUNCTION check_coplanar( matA ) RESULT(res)
!
!---- Arguments -------------------------------------------------------!
REAL(DP), DIMENSION(:,:), INTENT(IN) :: matA
LOGICAL :: res
!
!---- Déclarations ----------------------------------------------------!
INTEGER(IP) :: ipoint, npoint, cpt
REAL(DP), DIMENSION(3) :: AB, AC, AD
!
! Initialisation et definition du plan par trois points (A,B,C) non alignes
res = .TRUE.
npoint = SIZE(matA,1)

AB = matA(2, 1:3) - matA(1, 1:3)
AB = AB / NORM(AB)
AC = matA(3, 1:3) - matA(1, 1:3)
cpt = 3
DO WHILE (abs(DOT_PRODUCT(AB,AC)) < 10.0_dp*eps_min) ! Si les 3 points choisis sont alignes, on en prend d autres
    cpt = cpt + 1
    AC = matA(cpt, 1:3) - matA(1, 1:3)
ENDDO
AC = AC / NORM(AC)
!
! Boucle sur les points
DO ipoint = 3,npoint
    IF (ipoint == cpt) CYCLE
    AD = matA(ipoint, 1:3) - matA(1, 1:3)
    AD = AD / NORM(AD)
    IF (abs(DOT_PRODUCT(AB,cross(AC,AD))) > 1E-6) THEN
        res = .FALSE.
        EXIT
    ENDIF
ENDDO

RETURN

END FUNCTION check_coplanar
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_interp