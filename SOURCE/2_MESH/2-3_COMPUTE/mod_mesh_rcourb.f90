!< Module de calcul du rayon de courbure (adpaté de FAST/MUSIC)
!
!
MODULE mod_mesh_rcourb
!
USE mod_cst, ONLY : DP, IP, eps_min
USE mod_print, ONLY : print_err
USE mod_algebra, ONLY : mean, cross, norm
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Calcul du rayon de courbure
SUBROUTINE mesh_courbure( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: isom, nsom_lim, ivois, axis_rev, nb_som_courb, i
REAL(DP) :: r_local
REAL(DP), DIMENSION(3) :: V, V1, V2, normale, VECT
REAL(DP), DIMENSION(:,:), POINTER :: VVV
REAL(DP), ALLOCATABLE, DIMENSION(:,:) :: VECTOR, PLANE
REAL(DP), ALLOCATABLE, DIMENSION(:) :: M 
!
!---- Récupérations ---------------------------------------------------!
!
nsom_lim = DOM%MLG%nsom_lim
axis_rev = 0 ! est ce qu'il y a un axe de revolution?
!
DO isom = 1, nsom_lim
    IF ( DOM%MLG%SOM_LIM(isom)%nb_tags > 1 ) THEN
        
        r_local = 1E21

    ELSE

        nb_som_courb = DOM%MLG%SOM_LIM(isom)%nb_som_courb
        ! Coordonnées du sommet
        V = DOM%MLG%SOMMET ( DOM%MLG%SOM_ADJ_LIM(isom,1) , :)

        ! Coordonnées des sommets voisins
        ALLOCATE( VVV( nb_som_courb , 3 ) )
        VVV = 0.0_dp
        DO ivois = 1, nb_som_courb
            VVV( ivois, : ) = DOM%MLG%SOMMET( DOM%MLG%SOM_LIM(isom)%SOM_COURB(ivois) , : )
        END DO
        
        ! On vérifie que les points n'appartiennent pas tous au meme plan
        ALLOCATE( VECTOR( nb_som_courb , 3 ) )
        DO i = 1, nb_som_courb
            VECT = VVV(i,:) - V
            VECTOR(i,:) = VECT / NORM(VECT)
        END DO
        
        !Calculation of normal vector at the plane
        V1 = VECTOR(1,:)
        V2 = VECTOR(2,:)
        i = 1
        normale = 0.0_dp
        DO WHILE ( ALL( ABS(normale) < eps_min ) .AND. i < SIZE( VECTOR,1 ) )
            i = i + 1
            V2 = VECTOR(i,:)
            normale = CROSS( V1 , V2 )
            IF ( NORM(normale) < 100*eps_min ) THEN
                normale = 0.0_dp
            ELSE
                normale = normale / NORM( normale )
            END IF
        END DO	!at the end of this loop, 'normale' is the first not null normale

        ALLOCATE( PLANE( nb_som_courb, 1 ) )

        DO i = 1, nb_som_courb
            V1 = VECTOR(i,:)
            V1 = V1 / NORM( V1 )
            PLANE(i,1) = DOT_PRODUCT( normale , V1 )
        END DO

        CALL mean(PLANE,M)

        ! -------- CHOIX DE L ANGLE POUR DIRE QUE C EST UN PLAN ??? ----------
        IF ( ( ABS( M(1) ) < 1.0E-6 ) .AND. ( ABS( MAXVAL(PLANE) ) < 1.0E-6 ) .AND. ( ABS( MINVAL(PLANE) ) < 1.0E-6 ) ) THEN
            ! Cas plan
            r_local = 1E21
        ELSE
            ! Cas non plan
            CALL quadra_finalMethod( VVV , V , axis_rev , r_local )
            IF (r_local > 1E3*DOM%MLG%SOM_LIM(isom)%dist_caract ) r_local = 1E21
        END IF

        DEALLOCATE( VVV )
        DEALLOCATE( VECTOR )
        DEALLOCATE( PLANE )

    END IF

    DOM%MLG%SOM_LIM(isom)%r_courb = 1E21!r_local!1E21!

END DO
!
END SUBROUTINE mesh_courbure
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!  Subroutine quadra_FinalMethod
!
!  This function take as input the matrix P, containing the in each line the
!  spatial coordinates of points belonging to the 2 rings around the
!  considered node zen, of spatial coordinates V. D it's simply a vector
!  that has as many components as the number rows of P, and that contains
!  the numbers used in the connectivity matrix
!
!  OUTPUT:
!     raggio: that is the local mean radius (=2/(mean curvature)) for
!             the zen node
!----------------------------------------------------------------------!
SUBROUTINE quadra_FinalMethod(VVV,V,axis_rev,raggio)
  
IMPLICIT NONE
REAL(KIND=DP),DIMENSION(1:3),INTENT(in) :: V
REAL(KIND=DP),DIMENSION(:,:),POINTER :: VVV
INTEGER,INTENT(in) :: axis_rev
REAL(KIND=DP),INTENT(out) :: raggio
REAL(KIND=DP) :: raggio2,raggio3,eps,a,b,c,g,h,i,Cx,Cy,Cz,x,y,z, un 
REAL(KIND=DP),DIMENSION(:,:),ALLOCATABLE :: VVVp,U,bb
REAL(KIND=DP),DIMENSION(1:3) :: Vp
INTEGER :: k,p,q,NRHS,INFO,LWORK,SMLSIZ,NLVL
INTEGER,DIMENSION(1:1) :: minimus
REAL(KIND=DP),DIMENSION(1:3) :: stock
REAL(KIND=DP),DIMENSION(:),ALLOCATABLE :: M,WORK
INTEGER ILAENV

un = 1.0

IF (axis_rev==1) THEN
    CALL compute(VVV,V,un,raggio)
ELSE
    !the two little pertubations of the point of translation
    eps=0.09
    g=1-eps
    CALL compute(VVV,V,g,raggio2)

    g=1+eps
    CALL compute(VVV,V,g,raggio3)

    CALL compute(VVV,V,un,raggio)

    IF (abs(raggio-raggio2)/raggio>0.5 .OR. abs(raggio-raggio3)/raggio>0.5) THEN

        CALL mean(VVV,M)
        M=M

        ALLOCATE(VVVp(1:SIZE(VVV,1),1:SIZE(VVV,2)))
        DO k=1,SIZE(VVVp,1)
            VVVp(k,:)=VVV(k,:)-M(:)
        END DO
        Vp=V-M

        x=Vp(1)
        y=Vp(2)
        z=Vp(3)

        !quadric of rotation x axis
        ALLOCATE(U(1:SIZE(VVV,1),1:5))
        p=SIZE(U,1)
        q=SIZE(U,2)
        ALLOCATE(bb(1:p,1:1))
        DO k=1,p
            U(k,1)=VVVp(k,1)**2
            U(k,2)=VVVp(k,2)**2 + VVVp(k,3)**2
            U(k,3:5)=VVVp(k,:)
            bb(k,1)=-1.0
        END DO

        NRHS=1
        SMLSIZ = ILAENV(9,'DGELSD',' ',p,q,NRHS,0.01D0)
        NLVL = MAX( 0, INT( LOG( REAL(MIN( p,q ))/REAL(SMLSIZ+1) )/LOG(2.0) ) + 1 )
        IF (p>=q) THEN
            LWORK=12*q + 2*q*SMLSIZ + 8*q*NLVL + q*NRHS + (SMLSIZ+1)**2
        ELSE
            LWORK=12*p + 2*p*SMLSIZ + 8*p*NLVL + p*NRHS + (SMLSIZ+1)**2
        END IF
        ALLOCATE(WORK(1:MAX(1,LWORK)))

        !coefficients of the quadric
        CALL DGELS( 'N', p, q, NRHS, U, p, bb, p, WORK, LWORK,INFO )
        a=bb(1,1)
        b=bb(2,1)
        g=bb(3,1)
        h=bb(4,1)
        i=bb(5,1)

        !curvature quadric of rotation x axis
        Cx=2.0*a/(4.0*a**2*x**2+4.0*a*x*g+g**2+4.0*b**2*y**2+4.0*b*y*h+h**2+4.0*b**2*z**2+4.0*b*z*i+i**2)**(1.0/2.0)&
            &-1.0/2.0*(2*a*x+g)&
            &/(4.0*a**2*x**2+4.0*a*x*g+g**2+4.0*b**2*y**2+4.0*b*y*h+h**2+4.0*b**2*z**2+4.0*b*z*i+i**2)**(3.0/2.0)&
            &*(8.0*a**2*x+4.0*a*g)&
            &+4.0*b/(4.0*a**2*x**2+4.0*a*x*g+g**2+4.0*b**2*y**2+4.0*b*y*h+h**2+4.0*b**2*z**2+4.0*b*z*i+i**2)**(1.0/2.0)&
            &-1.0/2.0*(2.0*b*y+h)&
            &/(4.0*a**2*x**2+4.0*a*x*g+g**2+4.0*b**2*y**2+4.0*b*y*h+h**2+4.0*b**2*z**2+4.0*b*z*i+i**2)**(3.0/2.0)&
            &*(8.0*b**2*y+4.0*b*h)-1.0/2.0*(2.0*b*z+i)&
            &/(4.0*a**2*x**2+4.0*a*x*g+g**2+4.0*b**2*y**2+4.0*b*y*h+h**2+4.0*b**2*z**2+4.0*b*z*i+i**2)**(3.0/2.0)&
            &*(8.0*b**2*z+4.0*b*i)
        !we stock the translation coefficient for doing after a confrontation
        IF ( ABS(a) < 1E-15) a = 1E-15
        IF ( ABS(b) < 1E-15) b = 1E-15
        IF (abs(a/b)>abs(b/a)) THEN
            stock(1)=abs(a/b)
        ELSE
            stock(1)=abs(b/a)
        END IF
        DEALLOCATE(U)
        DEALLOCATE(bb)
        DEALLOCATE(WORK)

        !quadric of rotation y axis
        ALLOCATE(U(1:SIZE(VVV,1),1:5))
        p=SIZE(U,1)
        q=SIZE(U,2)
        ALLOCATE(bb(1:p,1:1))
        DO k=1,p
            U(k,1)=VVVp(k,1)**2 + VVVp(k,3)**2
            U(k,2)=VVVp(k,2)**2
            U(k,3:5)=VVVp(k,:)
            bb(k,1)=-1.0
        END DO

        NRHS=1
        SMLSIZ = ILAENV(9,'DGELSD',' ',p,q,NRHS,0.01D0)
        NLVL = MAX( 0, INT( LOG( REAL(MIN( p,q ))/REAL(SMLSIZ+1) )/LOG(2.0) ) + 1 )
        IF (p>=q) THEN
            LWORK=12*q + 2*q*SMLSIZ + 8*q*NLVL + q*NRHS + (SMLSIZ+1)**2
        ELSE
            LWORK=12*p + 2*p*SMLSIZ + 8*p*NLVL + p*NRHS + (SMLSIZ+1)**2
        END IF
        ALLOCATE(WORK(1:MAX(1,LWORK)))

        !coefficients of the quadric
        CALL DGELS( 'N', p, q, NRHS, U, p, bb, p, WORK, LWORK,INFO )
        a=bb(1,1)
        b=bb(2,1)
        g=bb(3,1)
        h=bb(4,1)
        i=bb(5,1)

        !curvature quadric of rotation y axis
        Cy=4.0*a/(4.0*a**2*x**2+4.0*a*x*g+g**2+4.0*b**2*y**2+4.0*b*y*h+h**2+4.0*a**2*z**2+4.0*a*z*i+i**2)**(1.0/2.0)&
            &-1.0/2.0*(2.0*a*x+g)&
            &/(4.0*a**2*x**2+4.0*a*x*g+g**2+4.0*b**2*y**2+4.0*b*y*h+h**2+4.0*a**2*z**2+4.0*a*z*i+i**2)**(3.0/2.0)&
            &*(8.0*a**2*x+4.0*a*g)&
            &+2.0*b/(4.0*a**2*x**2+4.0*a*x*g+g**2+4.0*b**2*y**2+4.0*b*y*h+h**2+4.0*a**2*z**2+4.0*a*z*i+i**2)**(1.0/2.0)&
            &-1.0/2.0*(2*b*y+h)&
            &/(4.0*a**2*x**2+4.0*a*x*g+g**2+4.0*b**2*y**2+4.0*b*y*h+h**2+4.0*a**2*z**2+4.0*a*z*i+i**2)**(3.0/2.0)&
            &*(8.0*b**2*y+4.0*b*h)&
            &-1.0/2.0*(2.0*a*z+i)&
            &/(4.0*a**2*x**2+4.0*a*x*g+g**2+4.0*b**2*y**2+4.0*b*y*h+h**2+4.0*a**2*z**2+4.0*a*z*i+i**2)**(3.0/2.0)&
            &*(8.0*a**2*z+4.0*a*i)
        !we stock the translation coefficient for doing after a confrontation
        IF ( ABS(a) < 1E-15) a = 1E-15
        IF ( ABS(b) < 1E-15) b = 1E-15        
        IF (abs(a/b)>abs(b/a)) THEN
            stock(2)=abs(a/b)
        ELSE
            stock(2)=abs(b/a)
        END IF
        DEALLOCATE(U)
        DEALLOCATE(bb)
        DEALLOCATE(WORK)

        !quadric of rotation z axis
        ALLOCATE(U(1:SIZE(VVV,1),1:5))
        p=SIZE(U,1)
        q=SIZE(U,2)
        ALLOCATE(bb(1:p,1:1))
        DO k=1,p
            U(k,1)=VVVp(k,1)**2 + VVVp(k,2)**2
            U(k,2)=VVVp(k,3)**2
            U(k,3:5)=VVVp(k,:)
            bb(k,1)=-1.0
        END DO

        NRHS=1
        SMLSIZ = ILAENV(9,'DGELSD',' ',p,q,NRHS,0.01D0)
        NLVL = MAX( 0, INT( LOG( REAL(MIN( p,q ))/REAL(SMLSIZ+1) )/LOG(2.0) ) + 1 )
        IF (p>=q) THEN
            LWORK=12*q + 2*q*SMLSIZ + 8*q*NLVL + q*NRHS + (SMLSIZ+1)**2
        ELSE
            LWORK=12*p + 2*p*SMLSIZ + 8*p*NLVL + p*NRHS + (SMLSIZ+1)**2
        END IF
        ALLOCATE(WORK(1:MAX(1,LWORK)))

        !coefficients of the quadric
        CALL DGELS( 'N', p, q, NRHS, U, p, bb, p, WORK, LWORK,INFO )
        a=bb(1,1)
        c=bb(2,1)
        g=bb(3,1)
        h=bb(4,1)
        i=bb(5,1)

        !curvature quadric of rotation z axis
        Cz=4.0*a/(4.0*a**2*x**2+4.0*a*x*g+g**2+4.0*a**2*y**2+4.0*a*y*h+h**2+4.0*c**2*z**2+4.0*c*z*i+i**2)**(1.0/2.0)&
            &-1.0/2.0*(2.0*a*x+g)&
            &/(4.0*a**2*x**2+4.0*a*x*g+g**2+4.0*a**2*y**2+4.0*a*y*h+h**2+4.0*c**2*z**2+4.0*c*z*i+i**2)**(3.0/2.0)&
            &*(8.0*a**2*x+4.0*a*g)&
            &-1.0/2.0*(2.0*a*y+h)&
            &/(4.0*a**2*x**2+4.0*a*x*g+g**2+4.0*a**2*y**2+4.0*a*y*h+h**2+4.0*c**2*z**2+4.0*c*z*i+i**2)**(3.0/2.0)&
            &*(8.0*a**2*y+4.0*a*h)&
            &+2.0*c/(4.0*a**2*x**2+4.0*a*x*g+g**2+4.0*a**2*y**2+4.0*a*y*h+h**2+4.0*c**2*z**2+4.0*c*z*i+i**2)**(1.0/2.0)&
            &-1.0/2.0*(2*c*z+i)&
            &/(4.0*a**2*x**2+4.0*a*x*g+g**2+4.0*a**2*y**2+4.0*a*y*h+h**2+4.0*c**2*z**2+4.0*c*z*i+i**2)**(3.0/2.0)&
            &*(8.0*c**2*z+4.0*c*i)    
        !we stock the translation coefficient for doing after a confrontation
        IF ( ABS(a) < 1E-15) a = 1E-15
        IF ( ABS(c) < 1E-15) c = 1E-15        
        IF (abs(a/c)>abs(c/a)) THEN
            stock(3)=abs(a/c)
        ELSE
            stock(3)=abs(c/a)
        END IF
        DEALLOCATE(U)
        DEALLOCATE(bb)
        DEALLOCATE(WORK)

        !we see what it is of the three cases the quadric with the smallest
        !translation coefficient
        minimus=MINLOC(stock)
        IF (minimus(1)==1) THEN
            raggio=abs(2/Cx)
        ELSE IF (minimus(1)==2) THEN
            raggio=abs(2/Cy)
        ELSE
            raggio=abs(2/Cz)
        END IF
        DEALLOCATE(VVVp)
    END IF
END IF

RETURN
END SUBROUTINE quadra_FinalMethod
!----------------------------------------------------------------------!
!
!----------------------------------------------------------------------!
!Subroutine compute
!
!		This function compute radius curvature with the div-grad optimized contrained method.
!
!	Input variables :
!		-V		: a vector (one dimensionnal array) which contains the coordinate of the zen-th node
!		-VVV	: a two dimensionnal array. Each row contains the cordinates of the second neighbours of the zen-th node
!		-pert  	: a real which enable to simulate little variation on a point coordinate
!
!	Output variables :
!		-raggio : a real which represents a radius curvature
!----------------------------------------------------------------------!
SUBROUTINE compute(VVV,V,pert,raggio)

IMPLICIT NONE
REAL(KIND=DP),DIMENSION(1:3),INTENT(in) :: V
REAL(KIND=DP),DIMENSION(:,:),POINTER :: VVV
REAL(KIND=DP),INTENT(in) :: pert

REAL(KIND=DP),INTENT(out) :: raggio

REAL(KIND=DP) :: a,b,c,g,h,i,x,y,z,Coeff, un
REAL(KIND=DP),DIMENSION(:),ALLOCATABLE :: M
REAL(KIND=DP),DIMENSION(:,:),ALLOCATABLE ::VVVp,U,bb
REAL(KIND=DP),DIMENSION(1:3) :: Vp
INTEGER*8 :: k,p,q,NRHS,INFO,LWORK,SMLSIZ,NLVL
REAL(KIND=DP),DIMENSION(:),ALLOCATABLE :: WORK
INTEGER ILAENV

un = 1.0

CALL mean(VVV,M)
M=M*pert

ALLOCATE(VVVp(1:SIZE(VVV,1),1:SIZE(VVV,2)))

DO k=1,SIZE(VVVp,1)
    VVVp(k,:)=VVV(k,:)-M(:)
END DO

Vp = V - M

!DIV GRAD METHOD 6 coefficients
ALLOCATE(U(1:SIZE(VVVp,1),1:6))
p=SIZE(U,1)
q=SIZE(U,2)
ALLOCATE(bb(1:p,1:1))

DO k = 1, p
    U(k,1:3) = VVVp( k, : )**2
    U(k,4:6) = VVVp( k, : )
    bb(k,1) = -un	!un=1.0
END DO

NRHS=1

SMLSIZ = ILAENV(9,'DGELSD',' ',p,q,NRHS,0.01D0)

NLVL = MAX( 0, INT( LOG( REAL(MIN( p,q ))/REAL(SMLSIZ+1) )/LOG(2.0) ) + 1 )
IF (p>=q) THEN
    LWORK=12*q + 2*q*SMLSIZ + 8*q*NLVL + q*NRHS + (SMLSIZ+1)**2
ELSE
    LWORK=12*p + 2*p*SMLSIZ + 8*p*NLVL + p*NRHS + (SMLSIZ+1)**2
END IF
ALLOCATE(WORK(1:MAX(1,LWORK)))

!coefficients of the quadric
INFO = 0
CALL DGELS('N',p,q,NRHS,U,p,bb,p,WORK,LWORK,INFO)

 a=bb(1,1)
 b=bb(2,1)
 c=bb(3,1)
 g=bb(4,1)
 h=bb(5,1)
 i=bb(6,1)

x=Vp(1)
y=Vp(2)
z=Vp(3)

Coeff=2.0D0*a/(4.0D0*a**2*x**2+4.0D0*a*x*g+g**2+4.0D0*b**2*y**2+4.0D0*b*y*h+h**2+4.0D0*c**2*z**2 &
    & +4.0D0*c*z*i+i**2)**(1.0D0/2.0D0)-1.0D0/2.0D0*(2.0D0*a*x+g)/(4.0D0*a**2*x**2+4.0D0*a*x*g+g**2 &
    & +4.0D0*b**2*y**2+4.0D0*b*y*h+h**2+4.0D0*c**2*z**2+4.0D0*c*z*i+i**2)**(3.0D0/2.0D0)&
    & *(8.0D0*a**2*x+4.0D0*a*g)+2.0D0*b/(4.0D0*a**2*x**2+4.0D0*a*x*g+g**2+4.0D0*b**2*y**2+4.0D0*b*y*h &
    & +h**2+4.0D0*c**2*z**2+4.0D0*c*z*i+i**2)**(1.0D0/2.0D0)-1.0D0/2.0D0*(2.0D0*b*y+h)/(4.0D0*a**2*x**2 &
    & +4.0D0*a*x*g+g**2+4.0D0*b**2*y**2+4.0D0*b*y*h+h**2+4.0D0*c**2*z**2+4.0D0*c*z*i+i**2)**(3.0D0/2.0D0)&
    & *(8.0D0*b**2*y+4.0D0*b*h)+2.0D0*c/(4.0D0*a**2*x**2+4.0D0*a*x*g+g**2+4.0D0*b**2*y**2+4.0D0*b*y*h &
    & +h**2+4.0D0*c**2*z**2+4.0D0*c*z*i+i**2)**(1.0D0/2.0D0)-1.0D0/2.0D0*(2.0D0*c*z+i)/(4.0D0*a**2*x**2 &
    & +4.0D0*a*x*g+g**2+4.0D0*b**2*y**2+4.0D0*b*y*h+h**2+4.0D0*c**2*z**2+4.0D0*c*z*i+i**2)**(3.0D0/2.0D0) &
    & *(8.0D0*c**2*z+4.0D0*c*i)

IF( ABS(Coeff) < 10*eps_min )THEN
    raggio = 1.0E+21
ELSE
    raggio=ABS(2.0D0/Coeff)
ENDIF
 
DEALLOCATE(U)
DEALLOCATE(bb)
DEALLOCATE(WORK)
DEALLOCATE(VVVp)
DEALLOCATE(M)

RETURN
END SUBROUTINE compute
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_rcourb