!> Module de calcul des aires et des centres des faces
MODULE mod_mesh_aire
!
USE mod_cst, ONLY : IP, DP, eps_min, Pi
USE mod_print, ONLY : print_err
USE mod_algebra, ONLY : cross, normL2, NORM, DOT
!
USE mod_dom
!
IMPLICIT NONE
CONTAINS
!
!----------------------------------------------------------------------!
!> Routine principale de calcul des aires et des centres des faces
SUBROUTINE mesh_aire( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: ifac, nfac, nsom_fac, i_triangle
INTEGER(IP) ,DIMENSION(4) :: ID_som
REAL(DP), DIMENSION(3) :: PT1, PT2, PT3, VEC, VEC2 ! Point 1 et 2 de la face, Vecteur 1->2
REAL(DP), DIMENSION(3) :: AXE, AH, H ! AXE axe de symétrie, AH point A axe -> H
REAL(DP), DIMENSION(3) :: crossABH ! Produit vectoriel de AXE sur AH
REAL(DP) :: dist, normAXE, aire, normVEC ! Distance et normes
!
!---- Récupérations ---------------------------------------------------!
nfac = DOM%MLG%nfac
!
!---- Allocation de AIRE et H_XYZ -------------------------------------!
IF (.NOT.ALLOCATED( DOM%MLG%AIRE )) THEN
    ALLOCATE( DOM%MLG%AIRE(nfac) )
    ALLOCATE( DOM%MLG%H_XYZ(nfac,3) )
END IF
!
!--- Teste que l'axe de symétrie soit défini
IF (DOM%MLG%dim_simu == '2D_AXI') THEN

    AXE = DOM%MLG%AXE_2D(4:6)
    normAXE = normL2(AXE)
    IF (ALL(ABS(AXE) < 2.0_dp*eps_min)) THEN
        CALL print_err('Axe de symetrie non-defini (parametre AXE_2D)',1)
    END IF
    
END IF
!
! Boucle sur les faces
DO ifac = 1,nfac
    
    nsom_fac = DOM%MLG%FAC(ifac,2)         ! Nombre de sommets de la face
    ID_som = DOM%MLG%FAC(ifac,3:6)         ! ID des 4 sommets de ifac
    
    ! Cas 2D et 1D : 1 face = 1 segment
    IF (nsom_fac == 2) THEN
        
        PT1 = DOM%MLG%SOMMET( ID_som(1), : ) ! Position du point 1
        PT2 = DOM%MLG%SOMMET( ID_som(2), : ) ! Position du point 2
        VEC = PT2 - PT1                       ! Vecteur pt1 -> pt2
        
        ! AIRES
        DOM%MLG%AIRE(ifac) = normL2(VEC)

        ! H centre de gravite de ifac
        DOM%MLG%H_XYZ(ifac,:) = (PT1(:)+PT2(:)) / 2.0_dp

    ! Cas 3D
    ELSE
        
        ! Reinitialisation
        aire = 0.0_dp
        H(:) = 0.0_dp
        PT1 = DOM%MLG%SOMMET( ID_som(1), : ) ! Position du point 1
        
        ! Decoupage des faces en triangles avec comme sommet commun le 1er sommet de la face
        DO i_triangle = 1,nsom_fac-2
        
            PT2 = DOM%MLG%SOMMET( ID_som(i_triangle+1), : ) ! Position du point 2
            PT3 = DOM%MLG%SOMMET( ID_som(i_triangle+2), : ) ! Position du point 3
            
            VEC = PT2 - PT1                       ! Vecteur pt1 -> pt2
            VEC2 = PT3 - PT1                      ! Vecteur pt2 -> pt3
            
            ! Aire des sous-triangles
            VEC = cross( VEC, VEC2 )
            normVEC = normL2( VEC )
            aire = aire + normVEC
            
            ! Centre de gravite du sous-triangle
            VEC = (PT1 + PT2 + PT3) / 3.0_dp
            VEC = VEC * normVEC
            H = H + VEC
        
        END DO ! Fin du decoupage
        
        ! Ecriture aire de la face
        aire = aire / 2.0_dp
        DOM%MLG%AIRE(ifac) = aire
        
        ! Ecriture centre de gravite de la face
        H = H / (2.0_dp * aire)
        DOM%MLG%H_XYZ(ifac,:) = H
 
        
    END IF
    
    ! Modifications dans le cas 2D_AXI
    IF (DOM%MLG%dim_simu == '2D_AXI') THEN

        ! Distance entre axe et point H
        AH = DOM%MLG%H_XYZ(ifac,1:3) - DOM%MLG%AXE_2D(1:3)
        crossABH = cross(AXE,AH)
        dist = normL2(crossABH) / normAXE
        ! Aire des faces proportionnels à la distance à l'axe de symétrie
        DOM%MLG%AIRE(ifac) = DOM%MLG%AIRE(ifac) * 2.0_dp * Pi * dist

    END IF

END DO


END SUBROUTINE mesh_aire
!----------------------------------------------------------------------!

!----------------------------------------------------------------------!
!> Si cas STRUCTURE = on et 2D AXI-> Modification des aires
!> Decalage des sommets sur l'axe de eps_S
SUBROUTINE mesh_aire_str( DOM )
!
!---- Déclarations ----------------------------------------------------!
TYPE (typ_dom) :: DOM
INTEGER(IP) :: ifac, nfac
INTEGER(IP) :: id_cel
INTEGER(IP) ,DIMENSION(2) :: ID_som
REAL(DP), DIMENSION(3) :: PT1, PT2 ,VEC ! Point 1 et 2 de la face, Vecteur 1->2
REAL(DP), DIMENSION(3) :: AB, AP ! AB axe de symétrie, AH point A axe -> P
REAL(DP), DIMENSION(3) :: HG ! Vecteur HG, centre de face vers centre de cel
REAL(DP), DIMENSION(3) :: crossABP ! Produit vectoriel de AB sur AP
REAL(DP), DIMENSION(3) :: norm_axe ! Produit vectoriel de AB sur Z = Normale a l'axe
REAL(DP), DIMENSION(3) :: deriv ! Distance de deplacement des sommets par rapport a l'axe
REAL(DP) :: dist ! Distance H sur axe AB
REAL(DP) :: eps_decal ! Deplac
!
!---- Récupérations ---------------------------------------------------!
nfac = DOM%MLG%nfac
!
! Decalage par rapport a la distance de centre de cel adjacente
eps_decal = 1.0e-1_dp * MINVAL( DOM%MLG%G1G2 )
!
! 
!---- Allocation de AIREs ---------------------------------------------!
ALLOCATE( DOM%MLG%AIREs( nfac ))

! Cas 2D_PLAN => AIREs = AIRE
IF (DOM%MLG%dim_simu /= '2D_AXI') THEN
    DOM%MLG%AIREs(:) = DOM%MLG%AIRE(:)
END IF    


!--- Si cas 2D AXI-> Modification des aires
IF (DOM%MLG%dim_simu == '2D_AXI') THEN
    
    ! AB = Vecteur directeur de l'axe
    AB = DOM%MLG%AXE_2D(4:6)
    ! Vecteur normal a AB : norm_axe = AB ^ z
    norm_axe(1) = AB(2)
    norm_axe(2) = - AB(1)
    norm_axe(3) = 0.0_dp
    

    ! Boucle sur les faces
    DO ifac = 1,nfac

        ! Extraction des points 1 et 2 de ifac
        ID_som = DOM%MLG%FAC(ifac,3:4) ! ID des 2 sommets de ifac
        PT1 =  DOM%MLG%SOMMET( ID_som(1),1:3 ) ! Position du point 1
        PT2 =  DOM%MLG%SOMMET( ID_som(2),1:3 ) ! Position du point 2
        
        ! Cellule id_cel associée et Vecteur HG
        id_cel = DOM%MLG%FAC2CEL(ifac,3) ! id de la cellule 1 associée
        HG = DOM%MLG%G_XYZ(id_cel,:) - DOM%MLG%AXE_2D(1:3) !DOM%MLG%H_XYZ(ifac,:) ! Vec face vers cel
        
        ! Init de deriv
        deriv = 0.0_dp
        
        !--- Point 1
        ! Distance entre axe et point PT1
        AP = PT1 - DOM%MLG%AXE_2D(1:3)
        crossABP = CROSS(AB,AP)
        dist = NORM(crossABP) / NORM(AB)
        
        ! Si sur l'axe on deplace PT1
        IF (dist < 2.0_dp*eps_min) THEN
            
            ! deriv est normé
            deriv = norm_axe / NORM(norm_axe)
            ! et dans le sens de HG
            IF (DOT(HG,deriv) > 0.0_dp) THEN
                deriv = -deriv
            END IF
            deriv = deriv * eps_decal
            
            ! Ajout aux coordonnées de PT1
            PT1 = PT1 + deriv
        
        END IF
        
        !--- Point 2
        ! Distance entre axe et point PT2
        AP = PT2 - DOM%MLG%AXE_2D(1:3)
        crossABP = CROSS(AB,AP)
        dist = NORM(crossABP) / NORM(AB)
        
        ! Si sur l'axe on deplace PT2
        IF (dist < 2.0_dp*eps_min) THEN
        
            ! deriv est normé
            deriv = norm_axe / NORM(norm_axe)
            IF (DOT(HG,deriv) > 0.0_dp) THEN
                deriv = -deriv
            END IF
            ! et dans le sens de HG
            deriv = deriv * eps_decal
            
            ! Ajout aux coordonnées de PT1
            PT2 = PT2 + deriv
        
        END IF
        
        VEC = PT2-PT1 ! Nouveau Vecteur pt1 -> pt2
        ! AIREs modifiées
        DOM%MLG%AIREs(ifac) = norm(VEC)
        
        ! Distance entre axe et point H
        AP = (PT2+PT1)/2.0_dp  - DOM%MLG%AXE_2D(1:3)
        crossABP = cross(AB,AP)
        dist = norm(crossABP) / norm(AB)
        ! Aire des faces proportionnels à la distance à l'axe de symétrie
        DOM%MLG%AIREs(ifac) = DOM%MLG%AIREs(ifac) * 2.0_dp*Pi * dist
    
        
    END DO

END IF


END SUBROUTINE mesh_aire_str
!----------------------------------------------------------------------!
!
!---- Fin du module ---------------------------------------------------!
END MODULE mod_mesh_aire
