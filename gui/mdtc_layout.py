#­* ­coding: utf­8 ­*
#----------------------------------------------------------------------#
# Script de generation des layouts de l'interface graphique de MODETHEC
#
# Débuté le 06/02/2015
# Auteur : Valentin Biasi
#
import sys

# Import des widgets perso et de la structure de données
from mdtc_widget import *
import mdtc_str
  
#---------------------------------------------------------------------------------------------------------------#
# Fonction principale de construction du layout PARAMETRES
def modifierParam(mdtc_ui):
    # On efface le layout
    clearLayout(mdtc_ui.gridParam)
    
    # Pour chaque Top Item, on construit le layout choisi
    item = mdtc_ui.treeWidget.currentItem()
    try:
        parent = item.parent()
    except:
        pass
    
    # Pour chaque Item, on construit le layout standard (Physical, Numerical, ...)
    if (item and not(parent)):

      if (item.text(0) == u"Species"):
        layout_ESPECE(mdtc_ui)
          
      elif (item.text(0) == u"General"):
        layout_GENERAL(mdtc_ui)
        
      elif (item.text(0) == u"Physical"):
        layout_PHYSIQUE(mdtc_ui)
        
      elif (item.text(0) == u"Reactions"):
        layout_REACTION(mdtc_ui)

      elif (item.text(0) == u"Numerical"):
        layout_NUMERIQUE(mdtc_ui)
        
      elif (item.text(0) == u"Initial conditions"):
        layout_INIT(mdtc_ui)
        
      elif (item.text(0) == u"Boundary conditions"):
        layout_LIMITE(mdtc_ui)
    
      elif (item.text(0) == u"History"):
        layout_SUIVI(mdtc_ui)
        
      elif (item.text(0) == u"Export"):
        layout_EXPORT(mdtc_ui)
    
    # Pour chaque Child Item, on construit le layout (Espèces, Réactions, Cond. init. et Cond. limites)
    elif (item and parent):

        if (parent.text(0) == u"Species"):
            iesp = parent.indexOfChild(item)
            layout_ESPECE_iesp(mdtc_ui, iesp)
            
        if (parent.text(0) == u"Reactions"):
            ireac = parent.indexOfChild(item)
            layout_REACTION_ireac(mdtc_ui, ireac)    

        if (parent.text(0) == u"Initial conditions"):
            idom = parent.indexOfChild(item)
            layout_INIT_idom(mdtc_ui, idom) 

        if (parent.text(0) == u"Boundary conditions"):
            ilim = parent.indexOfChild(item)
            layout_LIMITE_ilim(mdtc_ui, ilim) 

    # Sinon rien n'est selectionne -> layout GENERAL par defaut
    else:
        layout_GENERAL(mdtc_ui)


#---------------------------------------------------------------------------------------------------------------#
# Efface le layout choisi : PARAMETRES
def clearLayout(layout):
    if layout != None:
        while layout.count():
            child = layout.takeAt(0)
            if child.widget() is not None:
                child.widget().deleteLater()
            elif child.layout() is not None:
                clearLayout(child.layout())
                

#---------------------------------------------------------------------------------------------------------------#
# Réinitialisation de l'arbre de paramètres : Mise à jour des Children (Species, Reactions et Cond. limites)
def reset_tree(mdtc_ui):
    for ip in range( mdtc_ui.treeWidget.topLevelItemCount() ):
        parent = mdtc_ui.treeWidget.topLevelItem(ip)
        parent.setExpanded(True)
        
        # Si parent est "Species"
        if (parent.text(0) == u"Species"):
            parent.takeChildren()

            for i in range( (mdtc_ui.PRM['PHYSIQUE']['nesp']).v ):
                item = QTreeWidgetItem( )
                item.setText(0,unicode(str(i+1)+': '+(mdtc_ui.PRM['PHYSIQUE']['nom_nesp']).v[i]))
                parent.addChild( item )
                (mdtc_ui.PRM['ESPECE'][i]['nom_esp']).v = (mdtc_ui.PRM['PHYSIQUE']['nom_nesp']).v[i]
            
        # Si parent est "Reactions"
        if (parent.text(0) == u"Reactions"):
            parent.takeChildren()

            if mdtc_ui.PRM['GENERAUX']['REACTION'].v == 'on':
                parent.setDisabled(False)
            
                for i in range( (mdtc_ui.PRM['PHYSIQUE']['nreac']).v ):
                    item = QTreeWidgetItem( )
                    item.setText(0,unicode(str(i+1)+': '+(mdtc_ui.PRM['PHYSIQUE']['nom_nreac']).v[i]))
                    parent.addChild( item )
                    (mdtc_ui.PRM['REACTION'][i]['nom_reac']).v = (mdtc_ui.PRM['PHYSIQUE']['nom_nreac']).v[i]
                
            else:
                parent.setDisabled(True)

        # Si parent est "Boundary conditions"
        if (parent.text(0) == u"Initial conditions"):
            parent.takeChildren()

            for i in range( (mdtc_ui.PRM['PHYSIQUE']['ndom']).v ):
                item = QTreeWidgetItem( )
                item.setText(0,unicode(str(i+1)+': '+(mdtc_ui.PRM['PHYSIQUE']['nom_ndom']).v[i]))
                parent.addChild( item )
                (mdtc_ui.PRM['INIT'][i]['nom_dom']).v = (mdtc_ui.PRM['PHYSIQUE']['nom_ndom']).v[i]

        # Si parent est "Boundary conditions"
        if (parent.text(0) == u"Boundary conditions"):
            parent.takeChildren()

            for i in range( (mdtc_ui.PRM['PHYSIQUE']['nlim']).v ):
                item = QTreeWidgetItem( )
                item.setText(0,unicode(str(i+1)+': '+(mdtc_ui.PRM['PHYSIQUE']['nom_nlim']).v[i]))
                parent.addChild( item )
                (mdtc_ui.PRM['LIM'][i]['nom_lim']).v = (mdtc_ui.PRM['PHYSIQUE']['nom_nlim']).v[i]
        

#---------------------------------------------------------------------------------------------------------------#
#----- GENERAL
def layout_GENERAL(mdtc_ui):
    i = [0]
    mdtc_ui.Fsolveurs = QFrame()
    mdtc_ui.Fsolveurs.setFrameShadow(QFrame.Plain)
    mdtc_ui.gridParam.addWidget(mdtc_ui.Fsolveurs, i[0], 0, 1, 4)
    mdtc_ui.Gsolveurs = QGridLayout(mdtc_ui.Fsolveurs)

    check_button_ico(mdtc_ui.Gsolveurs,mdtc_ui.PRM['GENERAUX']['DIFFUSION'],
                    u'Heat diffusion',":/img/img/diffusion.png",True,('on','off'),(i[0],0,1,1))
    check_button_ico(mdtc_ui.Gsolveurs,mdtc_ui.PRM['GENERAUX']['ADVECTION'],
                    u'Gas transport',":/img/img/advection.png",False,('on','off'),(i[0],1,1,1))
    check_button_ico(mdtc_ui.Gsolveurs,mdtc_ui.PRM['GENERAUX']['REACTION'],
                    u'Kinetics',":/img/img/reaction.png",False,('on','off'),(i[0],2,1,1))
    check_button_ico(mdtc_ui.Gsolveurs,mdtc_ui.PRM['GENERAUX']['STRUCTURE'],
                    u'Mechanics',":/img/img/meca.png",False,('on','off'),(i[0],3,1,1))
    check_button_ico(mdtc_ui.Gsolveurs,mdtc_ui.PRM['GENERAUX']['ABLATION'],
                    u'Ablation',":/img/img/ablation.png",False,('on','off'),(i[0],4,1,1))
    check_button_ico(mdtc_ui.Gsolveurs,mdtc_ui.PRM['GENERAUX']['COUPLAGE'],
                    u'External coupling',":/img/img/couplage.png",False,('on','off'),(i[0],5,1,1))
    i[0] += 1
    
    ((mdtc_ui.PRM['GENERAUX']['REACTION']).w).released.connect(lambda: reset_tree(mdtc_ui))

    prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['GENERAUX']['file_mesh'],'Mesh filename:','',i)

    prm_list(mdtc_ui.gridParam, mdtc_ui.PRM['GENERAUX']['dim_simu'],
            'Simulation dimension:',
            ('3D','2D Planar','2D Axisymmetric','1D'),
            ('3D','2D_PLAN','2D_AXI','1D'),i)
    (mdtc_ui.PRM['GENERAUX']['dim_simu']).w.currentIndexChanged.connect(lambda: modifierParam(mdtc_ui))
    
    horiz_line(mdtc_ui.gridParam,i)
    if (mdtc_ui.PRM['GENERAUX']['dim_simu']).v == '2D_AXI':

        prm_edit_multi(mdtc_ui.gridParam, mdtc_ui.PRM['GENERAUX']['AXE_2D'],
                        ('Symmetry axis : x0 :','y0 :','z0 :','u0 :','v0 :','w0 :'),
                        ('','','','','','',''),i)
        horiz_line(mdtc_ui.gridParam,i)
    
    prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['GENERAUX']['auteur'],'Author:','',i)

    vert_spacer(mdtc_ui.gridParam,i)


#---------------------------------------------------------------------------------------------------------------#
#----- PHYSIQUE
def layout_PHYSIQUE(mdtc_ui):
    i = [0]
    prm_check(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['satur_T'],u'Activate properties saturation above temperature threshold','',(1,0),i)
    ((mdtc_ui.PRM['PHYSIQUE']['satur_T']).w).toggled.connect(lambda: modifierParam(mdtc_ui) )
    if (mdtc_ui.PRM['PHYSIQUE']['satur_T']).v == 1:
        prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['T_sat'],u'Temperature threshold for properties saturation =','K',i)
        
    # homog k
    if mdtc_ui.PRM['GENERAUX']['DIFFUSION'].v == 'on':
        prm_list(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['k_homog'],
                u"Thermal conductivity homogenization method:",
                ('Law of mixture','Mori-Tanaka'),
                ('melange','Mori-Tanaka'),i)

    # Permeabilite
    if mdtc_ui.PRM['GENERAUX']['ADVECTION'].v == 'on':

        horiz_line(mdtc_ui.gridParam,i)
        prm_list(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['Kp_typ'],
            u"Permeability homogenization method:",
            ('Constant','Henderson','Kozeny-Carman','Kozeny-Carman (orthotropic model)'),
            ('constant','Henderson','Kozeny','Kozeny-ortho'),i)
        (mdtc_ui.PRM['PHYSIQUE']['Kp_typ']).w.currentIndexChanged.connect(lambda: modifierParam(mdtc_ui) )

        if mdtc_ui.PRM['PHYSIQUE']['Kp_typ'].v == 'constant':
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['Kp_ini'],'Kp0 =','m2',i)
        
        if mdtc_ui.PRM['PHYSIQUE']['Kp_typ'].v == 'Henderson':
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['Kp_ini'],'Kp initial =','m2',i)
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['Kp_fin'],'Kp final =','m2',i)

        if mdtc_ui.PRM['PHYSIQUE']['Kp_typ'].v == 'Kozeny':
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['Kp_0'],'Kp0 =','m2',i)

        if mdtc_ui.PRM['PHYSIQUE']['Kp_typ'].v == 'Kozeny-ortho':
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['Kp_x'],'Kp0 x =','m2',i)
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['Kp_y'],'Kp0 y =','m2',i)
            
            if (mdtc_ui.PRM['GENERAUX']['dim_simu']).v == '3D':
                prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['Kp_z'],'Kp0 z =','m2',i)

        prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['Kp_max'],'Kp max =','m2',i)

    # Viscosite
    if mdtc_ui.PRM['GENERAUX']['ADVECTION'].v == 'on':

        horiz_line(mdtc_ui.gridParam,i)
        prm_list(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['mu_typ'],
            u"Gas phase dynamic viscosity model:",
            ('Constant','Polynomial expression f(T)'),
            ('constant','polyn'),i)
        (mdtc_ui.PRM['PHYSIQUE']['mu_typ']).w.currentIndexChanged.connect(lambda: modifierParam(mdtc_ui) )

        if mdtc_ui.PRM['PHYSIQUE']['mu_typ'].v == 'constant':
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['mu_cst'],u'Gas phase dynamic viscosity =','Pa.s',i)
        
        if mdtc_ui.PRM['PHYSIQUE']['mu_typ'].v == 'polyn':
            prm_edit_int(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['mu_nb_poly'],u'Number of polynomial coefficients =','',i)
            (mdtc_ui.PRM['PHYSIQUE']['mu_nb_poly']).w.setMaximum(6)         # Borne sup. ordre polynome
            (mdtc_ui.PRM['PHYSIQUE']['mu_nb_poly']).w.valueChanged.connect(lambda: modifierParam(mdtc_ui) )
            mu_nb_poly = mdtc_ui.PRM['PHYSIQUE']['mu_nb_poly'].v
            mdtc_ui.PRM['PHYSIQUE']['mu_poly'].n = mu_nb_poly
            prm_edit_multi(mdtc_ui.gridParam,(mdtc_ui.PRM['PHYSIQUE']['mu_poly']),
                                [u'Gas phase dynamic viscosity:','','','','',''],
                                ['Pa.s','x T','x T^2','x T^3','x T^4','x T^5'],i)
    
    # Mecanique
    if mdtc_ui.PRM['GENERAUX']['STRUCTURE'].v == 'on':

        horiz_line(mdtc_ui.gridParam,i)
        prm_list(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['meca_typ'],
            u"Mechanical properties model:",
            ('Isotropic','Transverse orthotropic'),
            ('isotrope','transverse'),i)
        (mdtc_ui.PRM['PHYSIQUE']['meca_typ']).w.currentIndexChanged.connect(lambda: modifierParam(mdtc_ui) )

        if mdtc_ui.PRM['PHYSIQUE']['meca_typ'].v == 'isotrope':
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['E_iso'],"Isotropic Young Modulus =",'Pa',i)
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['P_iso'],"Isotropic Poisson coefficient =",'',i)
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['beta_iso'],"Isotropic thermal expansion coefficient =",'1/T',i)

        if mdtc_ui.PRM['PHYSIQUE']['meca_typ'].v == 'transverse':
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['E_x'],"Young Modulus along X-direction =",'Pa',i)
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['E_y'],"Young Modulus along Y-direction =",'Pa',i)        
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['P_xy'],"Planar XY Poisson coefficient =",'',i)
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['G_xy'],"Transverse Z shear modulus =",'',i)
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['beta_x'],"Longitudinal thermal expansion coefficient along X-direction =",'1/T',i)
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['beta_y'],"Transverse thermal expansion coefficient along Y-direction =",'1/T',i)

    vert_spacer(mdtc_ui.gridParam,i)
        

#---------------------------------------------------------------------------------------------------------------#
#----- ESPECE
def layout_ESPECE(mdtc_ui):
    i = [0]
    prm_edit_int(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['nesp'],u"Total number of species =",'',i)
    (mdtc_ui.PRM['PHYSIQUE']['nesp']).w.valueChanged.connect(lambda: nesp_resize(mdtc_ui) )
    (mdtc_ui.PRM['PHYSIQUE']['nesp']).w.valueChanged.connect(lambda: modifierParam(mdtc_ui) )
    (mdtc_ui.PRM['PHYSIQUE']['nesp']).w.valueChanged.connect(lambda: reset_tree(mdtc_ui) )

    if mdtc_ui.PRM['GENERAUX']['STRUCTURE'].v == 'on':
    
        prm_list(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['atmo'],
                u"Atmosphere type:",
                (u'Vacuum',u'Air',u'O\u2082',u'N\u2082'),
                ('vide','air','O2','N2'),i)
                
    prm_table(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['nom_nesp'],(mdtc_ui.PRM['PHYSIQUE']['nesp'].v,1),
            u"Species name:",'',i)
    (mdtc_ui.PRM['PHYSIQUE']['nom_nesp']).w.cellChanged.connect(lambda: reset_tree(mdtc_ui) )
            
    vert_spacer(mdtc_ui.gridParam,i)


def nesp_resize(mdtc_ui):
    if (mdtc_ui.PRM['PHYSIQUE']['nesp']).v < 1: 
        (mdtc_ui.PRM['PHYSIQUE']['nesp']).v = 1

    for j in range( mdtc_ui.PRM['PHYSIQUE']['ndom'].v ):
        mdtc_ui.PRM['INIT'][j]['phi_ini'].n = mdtc_ui.PRM['PHYSIQUE']['nesp'].v
    
    mdtc_ui.PRM['PHYSIQUE']['ID_gaz'].v = []
    mdtc_ui.PRM['PHYSIQUE']['ID_sol'].v = []
    mdtc_ui.PRM['PHYSIQUE']['ID_gaz'].n = 0
    mdtc_ui.PRM['PHYSIQUE']['ID_sol'].n = 0
    mdtc_ui.PRM['PHYSIQUE']['ngaz'].v = 0
    mdtc_ui.PRM['PHYSIQUE']['nsol'].v = 0

    for j in range( mdtc_ui.PRM['PHYSIQUE']['nesp'].v ):
        if j > len(mdtc_ui.PRM['PHYSIQUE']['nom_nesp'].v) - 1:
            (mdtc_ui.PRM['PHYSIQUE']['nom_nesp'].v).append('SPE_'+str(j+1))

            # for k in range( mdtc_ui.PRM['PHYSIQUE']['ndom'].v ):
            #     (mdtc_ui.PRM['INIT'][k]['phi_ini'].v).append(0.0)

            mdtc_str.add_espece(  mdtc_ui.PRM['ESPECE'] )
            (mdtc_ui.PRM['ESPECE'][j]['id_esp']).v = j+1
            (mdtc_ui.PRM['ESPECE'][j]['nom_esp']).v = 'SPE_'+str(j+1)
            
    for j in range( mdtc_ui.PRM['PHYSIQUE']['nesp'].v ):
        if (mdtc_ui.PRM['ESPECE'][j]['typ_esp']).v == 'gaz':
            (mdtc_ui.PRM['PHYSIQUE']['ID_gaz'].v).append( j+1 )
            mdtc_ui.PRM['PHYSIQUE']['ngaz'].v += 1
            mdtc_ui.PRM['PHYSIQUE']['ID_gaz'].n += 1
        if (mdtc_ui.PRM['ESPECE'][j]['typ_esp']).v == 'solide':
            (mdtc_ui.PRM['PHYSIQUE']['ID_sol'].v).append( j+1 )
            mdtc_ui.PRM['PHYSIQUE']['ID_sol'].n += 1
            mdtc_ui.PRM['PHYSIQUE']['nsol'].v += 1


#---------------------------------------------------------------------------------------------------------------#
#----- ESPECE iesp
def layout_ESPECE_iesp(mdtc_ui,iesp):
    # TYPE ESPECE
    i = [0]
    prm_list(mdtc_ui.gridParam, mdtc_ui.PRM['ESPECE'][iesp]['typ_esp'],
        u"Species phase:",
        ('Solid','Gas'),
        ('solide','gaz'),i)
    (mdtc_ui.PRM['ESPECE'][iesp]['typ_esp']).w.currentIndexChanged.connect(lambda: nesp_resize(mdtc_ui) )
    (mdtc_ui.PRM['ESPECE'][iesp]['typ_esp']).w.currentIndexChanged.connect(lambda: modifierParam(mdtc_ui) )

    # ESPECE RHO
    if mdtc_ui.PRM['ESPECE'][iesp]['typ_esp'].v == 'solide':
        horiz_line(mdtc_ui.gridParam,i)
        prm_list(mdtc_ui.gridParam, mdtc_ui.PRM['ESPECE'][iesp]['rho_typ'],
            u"Species density model:",
            ('Constant','Polynomial expression f(T)'),
            ('constant','polyn'),i)
        (mdtc_ui.PRM['ESPECE'][iesp]['rho_typ']).w.currentIndexChanged.connect(lambda: modifierParam(mdtc_ui) )
    
        if mdtc_ui.PRM['ESPECE'][iesp]['rho_typ'].v == 'constant':
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['ESPECE'][iesp]['rho_cst'],u'Species constant density =','kg/m3',i)
        
        if mdtc_ui.PRM['ESPECE'][iesp]['rho_typ'].v == 'polyn':
            prm_edit_int(mdtc_ui.gridParam, mdtc_ui.PRM['ESPECE'][iesp]['rho_nb_poly'],u'Number of polynomial coefficients =','',i)
            (mdtc_ui.PRM['ESPECE'][iesp]['rho_nb_poly']).w.setMaximum(6)      # Borne sup. ordre polynome
            (mdtc_ui.PRM['ESPECE'][iesp]['rho_nb_poly']).w.valueChanged.connect(lambda: modifierParam(mdtc_ui) )
            
            rho_nb_poly = mdtc_ui.PRM['ESPECE'][iesp]['rho_nb_poly'].v
            mdtc_ui.PRM['ESPECE'][iesp]['rho_poly'].n = rho_nb_poly
            prm_edit_multi(mdtc_ui.gridParam,(mdtc_ui.PRM['ESPECE'][iesp]['rho_poly']),
                                [u'Species density =','','','','',''],
                                ['kg/m3','x T','x T^2','x T^3','x T^4','x T^5'],i)
    
    # ESPECE MASSE MOL
    if mdtc_ui.PRM['ESPECE'][iesp]['typ_esp'].v == 'gaz':
        horiz_line(mdtc_ui.gridParam,i)
        prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['ESPECE'][iesp]['M'],u'Gas species molar mass =','kg/mol',i)
        
    # ESPECE CP
    if mdtc_ui.PRM['GENERAUX']['DIFFUSION'].v == 'on':

        horiz_line(mdtc_ui.gridParam,i)
        prm_list(mdtc_ui.gridParam, mdtc_ui.PRM['ESPECE'][iesp]['Cp_typ'],
            u"Species specific heat model:",
            ('Constant','Polynomial expression f(T)'),
            ('constant','polyn'),i)
        (mdtc_ui.PRM['ESPECE'][iesp]['Cp_typ']).w.currentIndexChanged.connect(lambda: modifierParam(mdtc_ui) )

        if mdtc_ui.PRM['ESPECE'][iesp]['Cp_typ'].v == 'constant':
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['ESPECE'][iesp]['Cp_cst'],u'Species constant specific heat =','J/kg/K',i)
        
        if mdtc_ui.PRM['ESPECE'][iesp]['Cp_typ'].v == 'polyn':
            prm_edit_int(mdtc_ui.gridParam, mdtc_ui.PRM['ESPECE'][iesp]['Cp_nb_poly'],u'Number of polynomial coefficients =','',i)
            (mdtc_ui.PRM['ESPECE'][iesp]['Cp_nb_poly']).w.setMaximum(6)     # Borne sup. ordre polynome
            (mdtc_ui.PRM['ESPECE'][iesp]['Cp_nb_poly']).w.valueChanged.connect(lambda: modifierParam(mdtc_ui) )
            Cp_nb_poly = mdtc_ui.PRM['ESPECE'][iesp]['Cp_nb_poly'].v
            mdtc_ui.PRM['ESPECE'][iesp]['Cp_poly'].n = Cp_nb_poly
            prm_edit_multi(mdtc_ui.gridParam,(mdtc_ui.PRM['ESPECE'][iesp]['Cp_poly']),
                                [u'Specific heat =','','','','',''],
                                ['J/kg/K','x T','x T^2','x T^3','x T^4','x T^5'],i)
    
    # ESPECE CONDUCTIVITE
    if mdtc_ui.PRM['GENERAUX']['DIFFUSION'].v == 'on':

        horiz_line(mdtc_ui.gridParam,i)
        prm_list(mdtc_ui.gridParam, mdtc_ui.PRM['ESPECE'][iesp]['k_typ'],
            u"Species thermal conductivity tensor model:",
            ('Constant','Isotropic f(T)','Orthotropic f(T)'),
            ('constant','polyn','ortho'),i)
        (mdtc_ui.PRM['ESPECE'][iesp]['k_typ']).w.currentIndexChanged.connect(lambda: modifierParam(mdtc_ui) )

        if mdtc_ui.PRM['ESPECE'][iesp]['k_typ'].v == 'constant':
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['ESPECE'][iesp]['k_cst'],u'Species constant thermal conductivity =','W/m/K',i)
        
        if mdtc_ui.PRM['ESPECE'][iesp]['k_typ'].v == 'polyn':
            prm_edit_int(mdtc_ui.gridParam, mdtc_ui.PRM['ESPECE'][iesp]['k_nb_poly'],u'Number of polynomial coefficients =','',i)
            (mdtc_ui.PRM['ESPECE'][iesp]['k_nb_poly']).w.setMaximum(6)      # Borne sup. ordre polynome
            (mdtc_ui.PRM['ESPECE'][iesp]['k_nb_poly']).w.valueChanged.connect(lambda: modifierParam(mdtc_ui) )
            k_nb_poly = mdtc_ui.PRM['ESPECE'][iesp]['k_nb_poly'].v
            mdtc_ui.PRM['ESPECE'][iesp]['k_poly'].n = k_nb_poly
            prm_edit_multi(mdtc_ui.gridParam,(mdtc_ui.PRM['ESPECE'][iesp]['k_poly']),
                                [u'Isotropic thermal conductivity =','','','','',''],
                                ['W/m/K','x T','x T^2','x T^3','x T^4','x T^5'],i)
        
        if mdtc_ui.PRM['ESPECE'][iesp]['k_typ'].v == 'ortho':
            prm_edit_int(mdtc_ui.gridParam, mdtc_ui.PRM['ESPECE'][iesp]['k_nb_otho'],u'Number of polynomial coefficients =','',i)
            (mdtc_ui.PRM['ESPECE'][iesp]['k_nb_otho']).w.setMaximum(6)      # Borne sup. ordre polynome
            (mdtc_ui.PRM['ESPECE'][iesp]['k_nb_otho']).w.valueChanged.connect(lambda: modifierParam(mdtc_ui) )
            k_nb_ortho_x = mdtc_ui.PRM['ESPECE'][iesp]['k_nb_otho'].v
            mdtc_ui.PRM['ESPECE'][iesp]['k_ortho_x'].n = k_nb_ortho_x
            prm_edit_multi(mdtc_ui.gridParam,(mdtc_ui.PRM['ESPECE'][iesp]['k_ortho_x']),
                                [u'Thermal conductivity kx =','','','','',''],
                                ['W/m/K','x T','x T^2','x T^3','x T^4','x T^5'],i)
            k_nb_ortho_y = mdtc_ui.PRM['ESPECE'][iesp]['k_nb_otho'].v
            mdtc_ui.PRM['ESPECE'][iesp]['k_ortho_y'].n = k_nb_ortho_y
            prm_edit_multi(mdtc_ui.gridParam,(mdtc_ui.PRM['ESPECE'][iesp]['k_ortho_y']),
                                [u'Thermal conductivity ky =','','','','',''],
                                ['W/m/K','x T','x T^2','x T^3','x T^4','x T^5'],i)
            k_nb_ortho_z = mdtc_ui.PRM['ESPECE'][iesp]['k_nb_otho'].v
            mdtc_ui.PRM['ESPECE'][iesp]['k_ortho_z'].n = k_nb_ortho_z
            prm_edit_multi(mdtc_ui.gridParam,(mdtc_ui.PRM['ESPECE'][iesp]['k_ortho_z']),
                                [u'Thermal conductivity kz =','','','','',''],
                                ['W/m/K','x T','x T^2','x T^3','x T^4','x T^5'],i)
       
        # ESHELBY
        if mdtc_ui.PRM['PHYSIQUE']['k_homog'].v == 'Mori-Tanaka':

            horiz_line(mdtc_ui.gridParam,i)
            prm_edit_multi(mdtc_ui.gridParam,(mdtc_ui.PRM['ESPECE'][iesp]['k_Eshelby']),
                                    [u"Eshelby tensor: Exx",'Eyy','Ezz'],
                                    ['','',''],i)
    
        # PROP RADIATIVES
        if mdtc_ui.PRM['ESPECE'][iesp]['typ_esp'].v == 'solide':
            horiz_line(mdtc_ui.gridParam,i)
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['ESPECE'][iesp]['eps'],u'Surface emissivity =','',i)
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['ESPECE'][iesp]['alpha'],u'Surface absorptivity =','',i)
        
    vert_spacer(mdtc_ui.gridParam,i)


#---------------------------------------------------------------------------------------------------------------#
#----- REACTION
def layout_REACTION(mdtc_ui):
    i = [0]
    prm_edit_int(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['nreac'],u"Total number of reactions =",'',i)
    (mdtc_ui.PRM['PHYSIQUE']['nreac']).w.valueChanged.connect(lambda: nreac_resize() )
    (mdtc_ui.PRM['PHYSIQUE']['nreac']).w.valueChanged.connect(lambda: modifierParam(mdtc_ui) )
    (mdtc_ui.PRM['PHYSIQUE']['nreac']).w.valueChanged.connect(lambda: reset_tree(mdtc_ui) )
    
    def nreac_resize():
        for j in range( mdtc_ui.PRM['PHYSIQUE']['nreac'].v ):
            if j > len(mdtc_ui.PRM['PHYSIQUE']['nom_nreac'].v) - 1:
                (mdtc_ui.PRM['PHYSIQUE']['nom_nreac'].v).append('REAC_'+str(j+1))
                mdtc_str.add_reaction(  mdtc_ui.PRM['REACTION'] )
                (mdtc_ui.PRM['REACTION'][j]['id_reac']).v = j+1
                (mdtc_ui.PRM['REACTION'][j]['nom_reac']).v = 'REAC_'+str(j+1)
    
    prm_table(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['nom_nreac'],(mdtc_ui.PRM['PHYSIQUE']['nreac'].v,1),
            u"Reaction name:",'',i)
    (mdtc_ui.PRM['PHYSIQUE']['nom_nreac']).w.cellChanged.connect(lambda: reset_tree(mdtc_ui) )
            
    vert_spacer(mdtc_ui.gridParam,i)
    

#---------------------------------------------------------------------------------------------------------------#
#----- REACTION ireac
def layout_REACTION_ireac(mdtc_ui,ireac):
    # TYPE REACTION : Arrehenius
    i = [0]
    prm_edit_int(mdtc_ui.gridParam, mdtc_ui.PRM['REACTION'][ireac]['id_R'],u"Solid reactant species ID:",'',i)
    prm_edit_int(mdtc_ui.gridParam, mdtc_ui.PRM['REACTION'][ireac]['id_P'],u"Solid product species ID:",'(0 if none)',i)
    prm_check(mdtc_ui.gridParam, mdtc_ui.PRM['REACTION'][ireac]['id_O2'],u"Oxidative reaction",'',(1,0),i)
    prm_edit_int(mdtc_ui.gridParam, mdtc_ui.PRM['REACTION'][ireac]['ngazP'],u"Number of gas phase product(s) =",'',i)
    (mdtc_ui.PRM['REACTION'][ireac]['ngazP']).w.setMaximum(6)           # Borne sup. nb gaz produits
    (mdtc_ui.PRM['REACTION'][ireac]['ngazP']).w.valueChanged.connect(lambda: modifierParam(mdtc_ui) )
            
    ngazP = mdtc_ui.PRM['REACTION'][ireac]['ngazP'].v
    mdtc_ui.PRM['REACTION'][ireac]['ID_gazP'].n = ngazP
    prm_edit_multi(mdtc_ui.gridParam,(mdtc_ui.PRM['REACTION'][ireac]['ID_gazP']),
                        ['Species ID of gas phase product(s) =','','','','',''],
                        ['','','','','',''],i)
   
    horiz_line(mdtc_ui.gridParam,i)

    prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['REACTION'][ireac]['nu_R'],u'Stoichiometric mass coefficients of solid reactant species =','',i)
    prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['REACTION'][ireac]['nu_P'],u'Stoichiometric mass coefficients of solid product species =','',i)
    
    mdtc_ui.PRM['REACTION'][ireac]['nu_gazP'].n = ngazP
    prm_edit_multi(mdtc_ui.gridParam,(mdtc_ui.PRM['REACTION'][ireac]['nu_gazP']),
                        [u'Stoichiometric mass coefficients of gas phase product species =','','','','',''],
                        ['','','','','',''],i)

    horiz_line(mdtc_ui.gridParam,i)

    prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['REACTION'][ireac]['Q'],u'Heat of reaction Q =','J/kg',i)
    prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['REACTION'][ireac]['n'],u'Arrhenius kinectics reaction order n =','',i)
    prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['REACTION'][ireac]['A'],u'Arrhenius kinectics pre-exponential coefficient A =','1/s',i)
    prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['REACTION'][ireac]['E_A'],u'Arrhenius kinectics activation energy E\u2090 =','J/mol',i)

    vert_spacer(mdtc_ui.gridParam,i)


#---------------------------------------------------------------------------------------------------------------#
#----- NUMERIQUE
def layout_NUMERIQUE(mdtc_ui):
    i = [0]
    prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['NUMERIQUE']['t0'],u'Initial time =','s',i)
    prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['NUMERIQUE']['dt'],u'Time step =','s',i)
    prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['NUMERIQUE']['tf'],u'Final time =','s',i)
    
    if (mdtc_ui.PRM['GENERAUX']['DIFFUSION'].v == 'on'):
        horiz_line(mdtc_ui.gridParam,i)
        prm_list(mdtc_ui.gridParam, mdtc_ui.PRM['NUMERIQUE']['schema_DR'],
            u"Time integration numerical method (diffusion/reaction solver):",
            ('Explicit Euler','Theta-implicit'),
            ('euler-exp','theta-imp'),i)

    if (mdtc_ui.PRM['GENERAUX']['ADVECTION'].v == 'on'):
        horiz_line(mdtc_ui.gridParam,i)
        prm_list(mdtc_ui.gridParam, mdtc_ui.PRM['NUMERIQUE']['schema_A'],
            u"Time integration numerical method (advection solver):",
            (u'Explicit Euler',u'Theta-implicit',u'CFL-based theta-implicit'),
            ('euler-exp','theta-imp','theta-lin'),i)
        (mdtc_ui.PRM['NUMERIQUE']['schema_A']).w.currentIndexChanged.connect(lambda: modifierParam(mdtc_ui) )
        if (mdtc_ui.PRM['NUMERIQUE']['schema_A'].v == 'theta-lin'):
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['NUMERIQUE']['CFL_max'],u'Maximum CFL criterion for advection solver =','',i)

    if (mdtc_ui.PRM['GENERAUX']['STRUCTURE'].v == 'on'):
        horiz_line(mdtc_ui.gridParam,i)
        prm_list(mdtc_ui.gridParam, mdtc_ui.PRM['NUMERIQUE']['schema_S'],
            u"Integration numerical method (mechanics solver):",
            ('Explicit Euler','Theta-implicit'),
            ('euler-exp','implicite'),i)

    if (mdtc_ui.PRM['GENERAUX']['ABLATION'].v == 'on'):
        horiz_line(mdtc_ui.gridParam,i)
        prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['NUMERIQUE']['CFL_AB'],u'Maximum CFL criterion for ALE ablation solver =','',i)
        prm_edit_int(mdtc_ui.gridParam, mdtc_ui.PRM['NUMERIQUE']['pas_ech'],u"Sub-sampling maximum point number for ablation boundaries =",'',i)
        prm_edit_int(mdtc_ui.gridParam, mdtc_ui.PRM['NUMERIQUE']['niter_def'],u"Mesh displacement iteration number for ablation solver =",'',i)
        prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['NUMERIQUE']['tol_def'],u'Mesh displacement convergence criterion for ablation solver =','(cell ratio)',i)
        prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['NUMERIQUE']['nvois_def'],u'Number of neighbors for ablation smoothing =','',i)
    
    if (mdtc_ui.PRM['GENERAUX']['COUPLAGE'].v == 'on'):
        horiz_line(mdtc_ui.gridParam,i)
        prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['NUMERIQUE']['dt_couplage'],u'Coupling time step =','s',i)

    horiz_line(mdtc_ui.gridParam,i)
    prm_check(mdtc_ui.gridParam, mdtc_ui.PRM['NUMERIQUE']['advanced_num_prm'],"Show advanced numerical parameters",'',(1,0),i)
    ((mdtc_ui.PRM['NUMERIQUE']['advanced_num_prm']).w).toggled.connect(lambda: modifierParam(mdtc_ui) )

    if (mdtc_ui.PRM['NUMERIQUE']['advanced_num_prm'].v == 1):
        prm_list(mdtc_ui.gridParam, mdtc_ui.PRM['NUMERIQUE']['solveur_LIN'],
            u"Linear solver:",
            ['PARDISO'],
            ['PARDISO'],i)
        prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['NUMERIQUE']['theta_imp'],u'\u03b8 =',u'(implicit method)',i)
        prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['NUMERIQUE']['delta_max'],u'Relative variation limit of conservative variables =',u'(for each iteration)',i)
        prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['NUMERIQUE']['tol_abs'],u'Absolute convergence criterion =','',i)
        prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['NUMERIQUE']['tol_rel'],u'Relative convergence criterion =','',i)
        prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['NUMERIQUE']['tol_int'],u'Internal iteration convergence criterion for linear solver =','',i)
        mdtc_ui.PRM['NUMERIQUE']['tol_int'].w.setDisabled(True)     # Cette variable n'est plus utilisée depuis passage de GMRES à PARDISO
        prm_edit_int(mdtc_ui.gridParam, mdtc_ui.PRM['NUMERIQUE']['niter_max'],u"Maximum number of non-linear iterations for each time step =",'(Newton-Raphson)',i)
    
        if (mdtc_ui.PRM['GENERAUX']['ADVECTION'].v == 'on'):
            prm_list(mdtc_ui.gridParam, mdtc_ui.PRM['NUMERIQUE']['slope_limiter'],
                    u"Flux limiter type:",
                    (u'Upwind',u'Downwind',u'Central',u'SOU - Second-order Upwind',u'MinMod',u'Superbee',u'Van Leer',u'Van Albada',u'OSPRE'),
                    ('upwind','downwind','central','sou','minmod','superbee','van_leer','van_albada','ospre'),i)

    horiz_line(mdtc_ui.gridParam,i)
    prm_check(mdtc_ui.gridParam, mdtc_ui.PRM['NUMERIQUE']['restart_backup'],"Restart from last backup computation",'',(1,0),i)

    vert_spacer(mdtc_ui.gridParam,i)
    
#---------------------------------------------------------------------------------------------------------------#
#----- CONDITIONS INITIALES
def layout_INIT(mdtc_ui):
    i = [0]
    prm_edit_int(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['ndom'],"Total number of physical domains =",'',i)
    (mdtc_ui.PRM['PHYSIQUE']['ndom']).w.valueChanged.connect(lambda: ndom_resize(mdtc_ui) )
    (mdtc_ui.PRM['PHYSIQUE']['ndom']).w.valueChanged.connect(lambda: modifierParam(mdtc_ui) )
    (mdtc_ui.PRM['PHYSIQUE']['ndom']).w.valueChanged.connect(lambda: reset_tree(mdtc_ui) )

    prm_table(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['nom_ndom'],(mdtc_ui.PRM['PHYSIQUE']['ndom'].v,1),
            "Physical domains names:",'',i)
    (mdtc_ui.PRM['PHYSIQUE']['nom_ndom']).w.cellChanged.connect(lambda: reset_tree(mdtc_ui) )
            
    vert_spacer(mdtc_ui.gridParam,i)

def ndom_resize(mdtc_ui):
    for j in range( mdtc_ui.PRM['PHYSIQUE']['ndom'].v ):
        if j > len(mdtc_ui.PRM['PHYSIQUE']['nom_ndom'].v) - 1:
            (mdtc_ui.PRM['PHYSIQUE']['nom_ndom'].v).append('DOM_'+str(j+1))
            mdtc_str.add_cond_ini(  mdtc_ui.PRM['INIT'] )
            (mdtc_ui.PRM['INIT'][j]['id_dom']).v = j+1
            (mdtc_ui.PRM['INIT'][j]['nom_dom']).v = 'DOM_'+str(j+1)

    for j in range( mdtc_ui.PRM['PHYSIQUE']['ndom'].v ):
        mdtc_ui.PRM['INIT'][j]['phi_ini'].n = mdtc_ui.PRM['PHYSIQUE']['nesp'].v


#---------------------------------------------------------------------------------------------------------------#
#----- CONDITIONS INITIALES idom
def layout_INIT_idom(mdtc_ui, idom):
    i = [0]
    prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['INIT'][idom]['Tini'],u'Initial temperature =','K',i)

    if (mdtc_ui.PRM['GENERAUX']['ADVECTION'].v == 'on'):
        prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['INIT'][idom]['Pini'],u'Initial pressure =','Pa',i)
    
    prm_table(mdtc_ui.gridParam, mdtc_ui.PRM['INIT'][idom]['phi_ini'],(mdtc_ui.PRM['PHYSIQUE']['nesp'].v,1),
            u"Initial species volume fractions =",'',i)
    
    vert_spacer(mdtc_ui.gridParam,i)


#---------------------------------------------------------------------------------------------------------------#
#----- LIMITES
def layout_LIMITE(mdtc_ui):
    i = [0]
    prm_edit_int(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['nlim'],"Total number of boundary conditions =",'',i)
    (mdtc_ui.PRM['PHYSIQUE']['nlim']).w.valueChanged.connect(lambda: nlim_resize() )
    (mdtc_ui.PRM['PHYSIQUE']['nlim']).w.valueChanged.connect(lambda: modifierParam(mdtc_ui) )
    (mdtc_ui.PRM['PHYSIQUE']['nlim']).w.valueChanged.connect(lambda: reset_tree(mdtc_ui) )
    
    def nlim_resize():
        for j in range( mdtc_ui.PRM['PHYSIQUE']['nlim'].v ):
            if j > len(mdtc_ui.PRM['PHYSIQUE']['nom_nlim'].v) - 1:
                (mdtc_ui.PRM['PHYSIQUE']['nom_nlim'].v).append('BOUND_'+str(j+1))
                mdtc_str.add_limite(  mdtc_ui.PRM['LIM'] )
                (mdtc_ui.PRM['LIM'][j]['id_lim']).v = j+1
                (mdtc_ui.PRM['LIM'][j]['nom_lim']).v = 'BOUND_'+str(j+1)
    
    prm_table(mdtc_ui.gridParam, mdtc_ui.PRM['PHYSIQUE']['nom_nlim'],(mdtc_ui.PRM['PHYSIQUE']['nlim'].v,1),
            "Boundary condition name:",'',i)
    (mdtc_ui.PRM['PHYSIQUE']['nom_nlim']).w.cellChanged.connect(lambda: reset_tree(mdtc_ui) )
            
    vert_spacer(mdtc_ui.gridParam,i)
    

#---------------------------------------------------------------------------------------------------------------#
#----- LIMITE ilim
def layout_LIMITE_ilim(mdtc_ui, ilim):
    i = [0]

    if (mdtc_ui.PRM['GENERAUX']['DIFFUSION'].v == 'on'):
        prm_list(mdtc_ui.gridParam, mdtc_ui.PRM['LIM'][ilim]['ener_typ'],
                u"Thermal boundary condition type:",
                (u'Imposed heat flux',u'Imposed temperature',u'Combined heat flux + convective/radiative exchanges'),
                ('flux','temp','mixte'),i)
        (mdtc_ui.PRM['LIM'][ilim]['ener_typ']).w.currentIndexChanged.connect(lambda: modifierParam(mdtc_ui) )
        
        if mdtc_ui.PRM['LIM'][ilim]['ener_typ'].v == 'flux':
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['LIM'][ilim]['Fimp'],u'Imposed heat flux =','W/m2',i)
        if mdtc_ui.PRM['LIM'][ilim]['ener_typ'].v == 'temp':
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['LIM'][ilim]['Timp'],u'Imposed temperature =','K',i)
        if mdtc_ui.PRM['LIM'][ilim]['ener_typ'].v == 'mixte':
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['LIM'][ilim]['Fimp'],u'Imposed heat flux =','W/m2',i)
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['LIM'][ilim]['T_rad'],u"Radiative heat transfer temperature =",'K',i)
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['LIM'][ilim]['T_conv'],u"Convective heat transfer temperature =",'K',i)
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['LIM'][ilim]['h_conv'],u"Convective heat transfer coefficient =",'W/m2/K',i)

        
    if (mdtc_ui.PRM['GENERAUX']['ADVECTION'].v == 'on'):
        horiz_line(mdtc_ui.gridParam,i)
        prm_list(mdtc_ui.gridParam, mdtc_ui.PRM['LIM'][ilim]['masse_typ'],
                u"Mass flow boundary condition type:",
                ('Imposed mass flow rate','Imposed external pressure'),
                ('debit','pression'),i)
        (mdtc_ui.PRM['LIM'][ilim]['masse_typ']).w.currentIndexChanged.connect(lambda: modifierParam(mdtc_ui) )

        if mdtc_ui.PRM['LIM'][ilim]['masse_typ'].v == 'debit':
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['LIM'][ilim]['Dimp'],u'Imposed mass flow rate =','kg/m2/s',i)
        if mdtc_ui.PRM['LIM'][ilim]['masse_typ'].v == 'pression':
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['LIM'][ilim]['Pimp'],u'Imposed external pressure =','Pa',i)
        
    if (mdtc_ui.PRM['GENERAUX']['STRUCTURE'].v == 'on'):
        horiz_line(mdtc_ui.gridParam,i)
        prm_list(mdtc_ui.gridParam, mdtc_ui.PRM['LIM'][ilim]['struct_typ'],
                u"Mechanical boundary type:",
                ('Stress','Constrained','Symmetry axis'),
                ('contrainte','fixe','axe'),i)
        (mdtc_ui.PRM['LIM'][ilim]['struct_typ']).w.currentIndexChanged.connect(lambda: modifierParam(mdtc_ui) )
        if (mdtc_ui.PRM['LIM'][ilim]['struct_typ']).v == 'contrainte':
            prm_edit_multi(mdtc_ui.gridParam,(mdtc_ui.PRM['LIM'][ilim]['stress_imp']),
                        ['Normal stress =','Tangential shear stress ='],
                        ['Pa','Pa'],i)
    
    if (mdtc_ui.PRM['GENERAUX']['ABLATION'].v == 'on'):
        horiz_line(mdtc_ui.gridParam,i)
        prm_list(mdtc_ui.gridParam, mdtc_ui.PRM['LIM'][ilim]['abla_typ'],
            "Ablation boundary type:",
            ('Fusion temperature','Constant ablation velocity (temperature dependent)','Imposed ablation velocity'),
            ('temp','meca','vitesse'),i)
        (mdtc_ui.PRM['LIM'][ilim]['abla_typ']).w.currentIndexChanged.connect(lambda: modifierParam(mdtc_ui) )

        if mdtc_ui.PRM['LIM'][ilim]['abla_typ'].v == 'vitesse':
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['LIM'][ilim]['Vreg_imp'],u'Ablation velocity =','m/s',i)
    
    if (mdtc_ui.PRM['GENERAUX']['COUPLAGE'].v == 'on'):
        horiz_line(mdtc_ui.gridParam,i)

        prm_check(mdtc_ui.gridParam, mdtc_ui.PRM['LIM'][ilim]['couplage_lim'],"Activate external coupling",'',(1,0),i)
        ((mdtc_ui.PRM['LIM'][ilim]['couplage_lim']).w).toggled.connect(lambda: modifierParam(mdtc_ui) )

        if (mdtc_ui.PRM['LIM'][ilim]['couplage_lim'].v == 1):
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['LIM'][ilim]['application_couplage'],u"Application name to be coupled via CWIPI:",'(mesh filename for modethec/modethec coupling)',i)
        
            nvar_resize( mdtc_ui.PRM['LIM'][ilim]['nvar_isend_coupling'], mdtc_ui.PRM['LIM'][ilim]['isend_coupling'] )
            prm_edit_int(mdtc_ui.gridParam, mdtc_ui.PRM['LIM'][ilim]['nvar_isend_coupling'],u"Number of variables to be sent =",'',i)

            (mdtc_ui.PRM['LIM'][ilim]['nvar_isend_coupling']).w.valueChanged.connect(lambda: nvar_list_resize( \
                mdtc_ui.PRM['LIM'][ilim]['nvar_isend_coupling'].v , \
                mdtc_ui.PRM['LIM'][ilim]['isend_coupling'].v, \
                'SENT' ) )
            (mdtc_ui.PRM['LIM'][ilim]['nvar_isend_coupling']).w.valueChanged.connect(lambda: modifierParam(mdtc_ui) )

            prm_table_multi(mdtc_ui.gridParam, \
                mdtc_ui.PRM['LIM'][ilim]['isend_coupling'], \
                (mdtc_ui.PRM['LIM'][ilim]['nvar_isend_coupling'].v,3), \
                u"List of variables to be sent:",'',[u'   Internal variable name   ',u'   Dimension index   ',u'Coupling name'], \
                mdtc_str.list_var_coupling, i)
            mdtc_ui.PRM['LIM'][ilim]['isend_coupling'].w.setMinimumSize(QSize(300, 30*min((mdtc_ui.PRM['LIM'][ilim]['nvar_isend_coupling'].v+1),4)))

            nvar_resize( mdtc_ui.PRM['LIM'][ilim]['nvar_irecv_coupling'], mdtc_ui.PRM['LIM'][ilim]['irecv_coupling'] )
            prm_edit_int(mdtc_ui.gridParam, mdtc_ui.PRM['LIM'][ilim]['nvar_irecv_coupling'],u"Number of variables to be received =",'',i)

            (mdtc_ui.PRM['LIM'][ilim]['nvar_irecv_coupling']).w.valueChanged.connect(lambda: nvar_list_resize( \
                mdtc_ui.PRM['LIM'][ilim]['nvar_irecv_coupling'].v , \
                mdtc_ui.PRM['LIM'][ilim]['irecv_coupling'].v, \
                'RECV' ) )
            (mdtc_ui.PRM['LIM'][ilim]['nvar_irecv_coupling']).w.valueChanged.connect(lambda: modifierParam(mdtc_ui) )

            prm_table_multi(mdtc_ui.gridParam, \
                mdtc_ui.PRM['LIM'][ilim]['irecv_coupling'], \
                (mdtc_ui.PRM['LIM'][ilim]['nvar_irecv_coupling'].v,3), \
                u"List of variables to be received:",'',[u'   Internal variable name   ',u'   Dimension index   ',u'Coupling name'], \
                mdtc_str.list_var_coupling, i)
            mdtc_ui.PRM['LIM'][ilim]['irecv_coupling'].w.setMinimumSize(QSize(300, 30*min((mdtc_ui.PRM['LIM'][ilim]['nvar_irecv_coupling'].v+1),4)))

    horiz_line(mdtc_ui.gridParam,i)
    prm_check(mdtc_ui.gridParam, mdtc_ui.PRM['LIM'][ilim]['use_cls'],"Activate user-specified boundary condition file",'',(1,0),i)
    ((mdtc_ui.PRM['LIM'][ilim]['use_cls']).w).toggled.connect(lambda: modifierParam(mdtc_ui) )
    if (mdtc_ui.PRM['LIM'][ilim]['use_cls'].v == 1):
        prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['LIM'][ilim]['file_cls'],u"User-specified boundary condition file:",'(Overwrite previous parameters)',i)



    vert_spacer(mdtc_ui.gridParam, i)

    

#---------------------------------------------------------------------------------------------------------------#
#----- SUIVI
def layout_SUIVI(mdtc_ui):
    i = [0]
    prm_check(mdtc_ui.gridParam, mdtc_ui.PRM['SUIVI']['suivi'],"Activate terminal history",'',(1,0),i)
    ((mdtc_ui.PRM['SUIVI']['suivi']).w).toggled.connect(lambda: modifierParam(mdtc_ui) )
    if (mdtc_ui.PRM['SUIVI']['suivi']).v == 1:
        prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['SUIVI']['dt_prt'],'Time step of on-screen prints =','s',i)
    prm_check(mdtc_ui.gridParam, mdtc_ui.PRM['SUIVI']['file_log'],"Record log file",'',(1,0),i)
    horiz_line(mdtc_ui.gridParam,i)
    
    prm_check(mdtc_ui.gridParam, mdtc_ui.PRM['SUIVI']['file_hdf5'],"Export integrated variables (HDF5 file format)",'',(1,0),i)
    ((mdtc_ui.PRM['SUIVI']['file_hdf5']).w).toggled.connect(lambda: modifierParam(mdtc_ui) )
    if (mdtc_ui.PRM['SUIVI']['file_hdf5']).v == 1:
        prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['SUIVI']['dt_hdf5'],'Time step of HDF5 export history =','s',i)

        prm_check(mdtc_ui.gridParam, mdtc_ui.PRM['SUIVI']['sensors'],u"Export variables at specific coordinates",'',(1,0),i)
        ((mdtc_ui.PRM['SUIVI']['sensors']).w).toggled.connect(lambda: modifierParam(mdtc_ui) )

        if (mdtc_ui.PRM['SUIVI']['sensors']).v == 1: 
            nvar_resize( mdtc_ui.PRM['SUIVI']['nsensors'], mdtc_ui.PRM['SUIVI']['coord_sensors'] )
            prm_edit_int(mdtc_ui.gridParam, mdtc_ui.PRM['SUIVI']['nsensors'],u"Number of sensors for specific outputs =",'',i)

            (mdtc_ui.PRM['SUIVI']['nsensors']).w.valueChanged.connect(lambda: ncoord_list_resize( \
            mdtc_ui.PRM['SUIVI']['nsensors'].v, \
            mdtc_ui.PRM['SUIVI']['coord_sensors'].v) )
            (mdtc_ui.PRM['SUIVI']['nsensors']).w.valueChanged.connect(lambda: modifierParam(mdtc_ui) ) 

            prm_table_multi(mdtc_ui.gridParam, mdtc_ui.PRM['SUIVI']['coord_sensors'],(mdtc_ui.PRM['SUIVI']['nsensors'].v,3),
                u"List of sensor coordinates to be extracted:",'',[u'     Coordinate X     ',u'     Coordinate Y     ','     Coordinate Z     '],'', i)
            mdtc_ui.PRM['SUIVI']['coord_sensors'].w.setMinimumSize(QSize(450, 30*min((mdtc_ui.PRM['SUIVI']['nsensors'].v+1),7)))        

    horiz_line(mdtc_ui.gridParam,i)

    prm_check(mdtc_ui.gridParam, mdtc_ui.PRM['SUIVI']['backup'],"Activate backup output",'',(1,0),i)
    ((mdtc_ui.PRM['SUIVI']['backup']).w).toggled.connect(lambda: modifierParam(mdtc_ui) )
    if (mdtc_ui.PRM['SUIVI']['backup']).v == 1:
        prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['SUIVI']['dt_backup'],'Time step between two backup outputs =','s',i)

    vert_spacer(mdtc_ui.gridParam,i)


#---------------------------------------------------------------------------------------------------------------#
#----- EXPORT
def layout_EXPORT(mdtc_ui):
    i = [0]

    prm_check(mdtc_ui.gridParam, mdtc_ui.PRM['EXPORT']['export'],u"Save export file",'',(1,0),i)
    ((mdtc_ui.PRM['EXPORT']['export']).w).toggled.connect(lambda: modifierParam(mdtc_ui) )

    # Activation du layout ssi export == 1
    if (mdtc_ui.PRM['EXPORT']['export']).v == 1:

        prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['EXPORT']['nom_exp'],u"Export filename prefix:",'',i)
        prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['EXPORT']['dt_exp'],u"Time step of export file =",'s (multiple of global time step)',i)
        prm_check(mdtc_ui.gridParam, mdtc_ui.PRM['EXPORT']['exp_ini'],u"Export initial state t=0",'',(1,0),i)
        
        prm_check(mdtc_ui.gridParam, mdtc_ui.PRM['EXPORT']['deform_mlg'],u"Mesh deformation for mechanical solver",'',(1,0),i)
        ((mdtc_ui.PRM['EXPORT']['deform_mlg']).w).toggled.connect(lambda: modifierParam(mdtc_ui) )
        if (mdtc_ui.PRM['EXPORT']['deform_mlg']).v == 1:
            prm_edit(mdtc_ui.gridParam, mdtc_ui.PRM['EXPORT']['deform_amp'],u"Mesh deformation magnitude factor =",'',i)
 
        prm_check(mdtc_ui.gridParam, mdtc_ui.PRM['EXPORT']['FEmode'],u"Cell connectivity (ON: Finite Elements Tecplot mode - OFF: Scatter mode)",'',(1,0),i)
        
        horiz_line(mdtc_ui.gridParam,i)
        prm_check(mdtc_ui.gridParam, mdtc_ui.PRM['EXPORT']['tecplot_bin'],u"Generate Tecplot binary file",'',(1,0),i)
        prm_check(mdtc_ui.gridParam, mdtc_ui.PRM['EXPORT']['tecplot_ascii'],u"Generate Tecplot ASCII file",'',(1,0),i)
        prm_check(mdtc_ui.gridParam, mdtc_ui.PRM['EXPORT']['vtk_ascii'],u"Generate VTK ASCII file",'',(1,0),i)
        
        horiz_line(mdtc_ui.gridParam,i)

        prm_check(mdtc_ui.gridParam, mdtc_ui.PRM['EXPORT']['sommet'],u"Export node-centered variables",'',(1,0),i)
        ((mdtc_ui.PRM['EXPORT']['sommet']).w).toggled.connect(lambda: invert_sommet() )
        ((mdtc_ui.PRM['EXPORT']['sommet']).w).toggled.connect(lambda: modifierParam(mdtc_ui) )
        prm_check(mdtc_ui.gridParam, mdtc_ui.PRM['EXPORT']['cellule'],u"Export cell-centered variables",'',(1,0),i)
        ((mdtc_ui.PRM['EXPORT']['cellule']).w).toggled.connect(lambda: invert_cellule() )
        ((mdtc_ui.PRM['EXPORT']['cellule']).w).toggled.connect(lambda: modifierParam(mdtc_ui) )

        def invert_sommet():
            if (mdtc_ui.PRM['EXPORT']['sommet']).v == 1: (mdtc_ui.PRM['EXPORT']['cellule']).v = 0
            else:  (mdtc_ui.PRM['EXPORT']['cellule']).v = 1
                
        def invert_cellule():
            if (mdtc_ui.PRM['EXPORT']['sommet']).v == 1: (mdtc_ui.PRM['EXPORT']['sommet']).v = 0
            else: (mdtc_ui.PRM['EXPORT']['sommet']).v = 1
        
        horiz_line(mdtc_ui.gridParam,i)
        
        nvar_resize( mdtc_ui.PRM['EXPORT']['nexpt'], mdtc_ui.PRM['EXPORT']['var_exp'] )
        prm_edit_int(mdtc_ui.gridParam, mdtc_ui.PRM['EXPORT']['nexpt'],u"Number of variables to be exported =",'',i)
        
        (mdtc_ui.PRM['EXPORT']['nexpt']).w.valueChanged.connect(lambda: nvar_list_resize( \
        mdtc_ui.PRM['EXPORT']['nexpt'].v, \
        mdtc_ui.PRM['EXPORT']['var_exp'].v, \
        'EXPT') )
        (mdtc_ui.PRM['EXPORT']['nexpt']).w.valueChanged.connect(lambda: modifierParam(mdtc_ui) )
        
        
        prm_table_multi(mdtc_ui.gridParam, mdtc_ui.PRM['EXPORT']['var_exp'],(mdtc_ui.PRM['EXPORT']['nexpt'].v,3),
                u"List of variables to be exported:",'',[u'   Internal variable name   ',u'   Dimension index   ','Export name'], mdtc_str.list_var_exp, i)
        mdtc_ui.PRM['EXPORT']['var_exp'].w.setMinimumSize(QSize(300, 30*min((mdtc_ui.PRM['EXPORT']['nexpt'].v+1),7)))
    
    vert_spacer(mdtc_ui.gridParam,i)


#---------------------------------------------------------------------------------------------------------------#
#----- Redimensionnement de la liste VAR à la dimension nvar avec le triplet de base definit par prefix
def nvar_list_resize( nvar, VAR, prefix ):
    for j in range( nvar ):
        if j > len(VAR) - 1:
            (VAR).append(list)
            (VAR)[j] = [prefix+'_'+str(j+1),'1',prefix+'_NAME_'+str(j+1)]

#---------------------------------------------------------------------------------------------------------------#
#----- Modification de la valeur nvar sir la liste VAR est sous-dimensionné
def nvar_resize( nvar, VAR ):
    if nvar.v > len(VAR.v):
        nvar.v = len(VAR.v)

#---------------------------------------------------------------------------------------------------------------#
#----- Redimensionnement de la liste VAR à la dimension nvar avec le triplet de base definit par prefix
def ncoord_list_resize( nvar, VAR ):
    for j in range( nvar ):
        if j > len(VAR) - 1:
            (VAR).append(list)
            (VAR)[j] = [0.0 , 0.0 , 0.0]
