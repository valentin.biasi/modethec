#! /usr/bin/env python
# -*- coding: utf-8 -*-
#----------------------------------------------------------------------#
# Programme principal de l'interface graphique de ADETHEC
#
# Débuté le 20/10/2016
# Auteur : Valentin Biasi
#
# Version : 0.10
# Version = Major release + Nombre de mois de dev. depuis 10/2016 + Debug release
#
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import sys
import os

# Librairies matplotlib pour integration dans pyqt
import matplotlib
matplotlib.use('Qt4agg') # need to call use() before importing plt
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt

# Import de la user interface générée avec Qt Designer et pyuic4
import adtc_UI

# Autres modules adtc
import adtc_io
import adtc_plot
import adtc_fit
import adtc_preferences

# Gestion des UTF8 en Qt
try:
    _fromUtf8 = QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s


# Classe mdtc_ui : Fenetre principale du programme
class adtc_ui(QMainWindow, adtc_UI.Ui_ADTC_window):

    # A l'initialisation d'un objet mdtc_ui :
    def __init__(self, parent=None):
        super(adtc_ui, self).__init__(parent)           # On hérite du constructeur parent
        self.setupUi(self)                              # On définit le contenu (widget) de mtdc_ui
        self.statusbar.showMessage("Logiciel d\'analyse de TGA-DSC Metler Tolledo")			# Petit message de bienvenue
        self.version = '0.10'                           # Version de software
        
        self.preferences = adtc_pref()                  # Initialisation de la fenetre 'preference'
        self.PRM = adtc_io.Prm()                        # Initialisation de la structure 'preferences'
        
        self.connectMenu()                              # On établit les connections avec le menu
        self.connectToolbar()                           # On établit les connections de la toolbar
        self.connectInterface()                         # On établit les connections de l'interface
        self.setPlotWidget()                            # On cree un widget pour base de plot
        
        # Mise en page finale
        self.tabifyDockWidget( self.dockWidget_database, self.dockWidget_plot_zone )
        self.dockWidget_database.raise_()
        self.database_widget.verticalHeader().hide()
        self.table_constituants.horizontalHeader().setStretchLastSection(True)
        self.table_reactions.horizontalHeader().setStretchLastSection(True)
        self.grid_heat_transfer.setVisible(False)
        
        self.species=[]
        self.reactions=[]
        
        self.horizontalHeader = self.database_widget.horizontalHeader()
        self.horizontalHeader.sectionClicked.connect( self.on_view_horizontalHeader_sectionClicked )
        self.keywords = dict([(i, []) for i in range(self.database_widget.columnCount())])
        self.checkBoxs = []
        self.col = None

        #  Si fichier en parametre argv : ouverture du fichier
        if len(sys.argv)>1:
            filename = sys.argv[1]
            filename = os.path.abspath(str(filename))
            self.filename = os.path.basename(filename)
            self.directory = os.path.dirname(filename)
            self.setWindowTitle( filename + " - ADeTheC" )

            self.DB=[]
            adtc_io.read_db( self.DB, self.database_widget, self.directory, self.filename )


    # Connexions avec la menubar
    def connectMenu(self):
        # Menu Fichier
        self.actionExit.triggered.connect(self.Exit)					# Action a la fermeture
        self.actionOpen_database.triggered.connect(self.Open_database)	# Action a l'ouverture d'un fichier
        self.actionReload_database.triggered.connect(self.Reload_database)	# Action a l'ouverture d'un fichier
        self.actionExport_HDF5.triggered.connect(self.Export_HDF5)	    # Action pour exporter plot et config en HDF5
        self.actionImport_HDF5.triggered.connect(self.Import_HDF5)	    # Action pour importer plot et config en HDF5

        
        # Menu Tools
        self.actionPlot.triggered.connect(lambda: adtc_plot.refresh_plot(self)) # Action pour mettre a jour les plot
        self.actionIntegrate_DSC.triggered.connect(lambda: adtc_plot.integrate_dsc(self)) # Action d'integration de l'energie de reaction
        self.actionTGA_integrate.triggered.connect(lambda: adtc_plot.tga_fitting(self, method='plot')) # Action de fitting des ATG selectionnees
        self.actionTGA_fitting.triggered.connect(lambda: adtc_plot.tga_fitting(self, method='fit')) # Action de fitting des ATG selectionnees
        self.actionTGA_simulate.triggered.connect(lambda: adtc_plot.tga_simulate(self)) # Action de reconstruction d'ATG en fonction des parametres
        
        # Menu Settings
        self.actionPreferences.triggered.connect(lambda: self.preferences.show() )
        self.preferences.button_OK.released.connect( self.Set_preferences )

        # Menu Aide
        self.actionAbout_Adethec.triggered.connect(self.About_Adethec)  # Action mini fenetre "a propos"
        self.actionReport_bug.triggered.connect(self.Report_bug)		# Action mini fenetre "a propos"

        # Menu affichage : plot_style
        self.group_plot_style = QActionGroup(self, exclusive=True)      # Créé un groupe avec des radios buttons pour le style de plot
        self.group_plot_style.addAction(self.actionClassic)             # Radio button 1
        self.group_plot_style.addAction(self.action538)                 # Radio button 2
        self.group_plot_style.addAction(self.actionBlack_White)         # Radio button 3
        
        # Menu affichage : legend position
        self.group_legend_location = QActionGroup(self, exclusive=True) # Créé un groupe avec des radios buttons pour la position de la legende
        self.group_legend_location.addAction(self.action_Best)          # Radio button 0
        self.group_legend_location.addAction(self.actionTop_Right)      # Radio button 1
        self.group_legend_location.addAction(self.actionTop_Left)       # Radio button 2
        self.group_legend_location.addAction(self.actionBottom_Right)   # Radio button 3
        self.group_legend_location.addAction(self.actionBottom_Left)    # Radio button 4
        self.legend_location = 0                                        # Valeur par defaut de legend location
        
    # Connexion de la toolbar principale
    def connectToolbar(self):
        pass

        # Connexions avec l'interface
    def connectInterface(self):
        # PLus et Moins Reactions
        self.button_plus_constituants.released.connect( self.insert_constituants )
        self.button_minus_constituants.released.connect( self.remove_constituants )
        self.button_plus_reactions.released.connect( self.insert_reactions )
        self.button_minus_reactions.released.connect( self.remove_reactions )
        # Reset fitting initial values
        self.push_reset_values.released.connect(lambda: adtc_fit.reset_fitting_values(self))
        
        
    # Mise en place d'un widget de plot
    def setPlotWidget(self):

        # Mise en place d'un widget de plot
        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)
        self.canvas.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.plot_layout.addWidget(self.canvas)

        self.plot_toolbar = NavigationToolbar(self.canvas, self)
        self.toolbar2 = self.addToolBar('MatplotLib')
        self.toolbar2.addWidget(self.plot_toolbar)


    # A la fermeture de adethec
    def Exit(self):
        # On ferme définitivement adethec
        self.close()


     # A l'ouverture d'un fichier
    def Open_database(self):
		# Boite de dialogue d'ouverture
        filename = QFileDialog.getOpenFileName(self,
                        u"Open a database",
                        QDir.currentPath(),
                        u"Excel format (*.xls);;Tous les fichiers (*)")

        # Si trouvé, on lit ce fichier
        if filename:
            filename = os.path.abspath(str(filename))
            self.filename = os.path.basename(filename)
            self.directory = os.path.dirname(filename)
            self.setWindowTitle( filename + " - ADeTheC" )

            self.DB=[]
            adtc_io.read_db( self.DB, self.database_widget, self.directory, self.filename )
    
    
    # A l'export de data HDF5
    def Export_HDF5(self):
		# Boite de dialogue d'enregistrement
        hdf5name = QFileDialog.getSaveFileName(self,
                u"Save data in HDF5 format",
                QDir.currentPath(),
                u"HDF5 format (*.hdf);;Tous les fichiers (*)")

        # Si trouvé, on exporte ce fichier
        if hdf5name:
            hdf5name = os.path.abspath(str(hdf5name))
            
            ### --- Lancement de export_hdf5
            try:
                adtc_io.export_hdf5( self, hdf5name )
            
            # Affiche erreur si echec
            except:
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Warning)
                msg.setWindowTitle(u"Export error")
                msg.setText(u"Unknown error during the export.")
                msg.exec_()
        
        
    # A l'import de data HDF5
    def Import_HDF5(self):
		# Boite de dialogue d'ouverture
        hdf5name = QFileDialog.getOpenFileName(self,
                u"Import parameters in HDF5 format",
                QDir.currentPath(),
                u"HDF5 format (*.hdf);;Tous les fichiers (*)")

        # Si trouvé, on importe ce fichier
        if hdf5name:
            hdf5name = os.path.abspath(str(hdf5name))
            
            ### --- Lancement de import_hdf5
            try:
                adtc_io.import_hdf5( self, hdf5name )
    
            # Affiche erreur si echec
            except:
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Warning)
                msg.setWindowTitle(u"Import error")
                msg.setText(u"Unknown error during the import.")
                msg.exec_()
    
    # Recharger le fichier database
    def Reload_database(self):
        
        try:
            self.database_widget.itemChanged.disconnect()
            self.DB=[]
            adtc_io.read_db( self.DB, self.database_widget, self.directory, self.filename )
        except:
            self.statusbar.showMessage("Oops : No database to reload")


    # Ecrit dans PRM les preferences utilisateurs
    def Set_preferences(self):
        self.PRM.Tmin = float(self.preferences.line_Tinterp_min.text())
        self.PRM.Tmax = float(self.preferences.line_Tinterp_max.text())
        self.PRM.n_interp = int(self.preferences.line_n_interp.text())
        
        self.PRM.automatic_colors = self.preferences.radio_automatic_colors.isChecked()
        self.PRM.predefined_colors = self.preferences.radio_predefined_colors.isChecked()
    
    
    # Message box de "About"
    def About_Adethec(self):
        Button_about = QMessageBox(QMessageBox.Information, u"About Adethec", u"""<h2>About Adethec</h2>
                    <p>Adthec is a mini progam coded in Python to plot and analyse experimental results
                     of thermo-gravimetric analysis (TGA) and differential scanning calorimetry (DSC). The analysis
                     is based on information given by an Excel format database.
                    </p>
                    <p><i>Programer : Valentin Biasi</i></p>
                    Version : 
                    """+self.version)
        ICO = QIcon()
        ICO.addPixmap(QPixmap(":/img/img/logo_adethec_128.png"))
        Button_about.setIconPixmap(QPixmap(":/img/img/logo_adethec_128.png"))
        
        Button_about.exec_()


    # Si utilisateur veux signaler un bug : il envoie un mail
    def Report_bug(self):
        import webbrowser
        webbrowser.open('mailto:valentin.biasi@onera.fr?subject=Report bug in Adethec', new=1)

    # Ajout ligne constituant par bouton "+"
    def insert_constituants(self):
        self.table_constituants.insertRow( self.table_constituants.rowCount() )
        
        item = QTableWidgetItem()
        item.setText('ESP_'+str(self.table_constituants.rowCount()))
        self.table_constituants.setItem( self.table_constituants.rowCount()-1, 0, item )
        item = QTableWidgetItem()
        item.setText('0.0')
        self.table_constituants.setItem( self.table_constituants.rowCount()-1, 1, item )
        item = QTableWidgetItem()
        item.setText('1000')
        self.table_constituants.setItem( self.table_constituants.rowCount()-1, 2, item )


    # Retrait ligne constituant par bouton "-"    
    def remove_constituants(self):
        self.table_constituants.removeRow( self.table_constituants.rowCount()-1 )
    
    # Ajout ligne reaction par bouton "+"
    def insert_reactions(self):
        self.table_reactions.insertRow( self.table_reactions.rowCount() )
        item = QTableWidgetItem()
        item.setText('1')
        self.table_reactions.setItem( self.table_reactions.rowCount()-1, 0, item )
        item = QTableWidgetItem()
        item.setText('')
        self.table_reactions.setItem( self.table_reactions.rowCount()-1, 1, item )
        item = QTableWidgetItem()
        item.setText('0')
        item.setCheckState(Qt.Unchecked)
        self.table_reactions.setItem( self.table_reactions.rowCount()-1, 2, item )
        item = QTableWidgetItem()
        item.setText('')
        item.setCheckState(Qt.Unchecked)
        self.table_reactions.setItem( self.table_reactions.rowCount()-1, 3, item )
        item = QTableWidgetItem()
        item.setText('1.0e5')
        item.setCheckState(Qt.Checked)
        self.table_reactions.setItem( self.table_reactions.rowCount()-1, 4, item )
        item = QTableWidgetItem()
        item.setText('1.0e5')
        item.setCheckState(Qt.Checked)
        self.table_reactions.setItem( self.table_reactions.rowCount()-1, 5, item )
        item = QTableWidgetItem()
        item.setText('1.0')
        item.setCheckState(Qt.Checked)
        self.table_reactions.setItem( self.table_reactions.rowCount()-1, 6, item )
        item = QTableWidgetItem()
        item.setText('0.0')
        item.setCheckState(Qt.Unchecked)
        self.table_reactions.setItem( self.table_reactions.rowCount()-1, 7, item )
    
    # Retrait ligne reaction par bouton "-"
    def remove_reactions(self):
        self.table_reactions.removeRow( self.table_reactions.rowCount()-1 )
        
        
    # Affichage principal
    def main(self):
        self.show()
    
    
    def slotSelect(self, state):
        for checkbox in self.checkBoxs:
            checkbox.setChecked(Qt.Checked == state)

    def on_view_horizontalHeader_sectionClicked(self, index):
        self.clearFilter()
        self.menu = QMenu(self)
        self.col = index

        data_unique = []

        self.checkBoxs = []

        checkBox = QCheckBox("Select all", self.menu)
        checkableAction = QWidgetAction(self.menu)
        checkableAction.setDefaultWidget(checkBox)
        self.menu.addAction(checkableAction)
        checkBox.setChecked(True)
        checkBox.stateChanged.connect(self.slotSelect)

        for i in range(self.database_widget.rowCount()):
            if not self.database_widget.isRowHidden(i):
                item = self.database_widget.item(i, index)
                if item.text() not in data_unique:
                    data_unique.append(item.text())
                    checkBox = QCheckBox(item.text(), self.menu)
                    checkBox.setChecked(True)
                    checkableAction = QWidgetAction(self.menu)
                    checkableAction.setDefaultWidget(checkBox)
                    self.menu.addAction(checkableAction)
                    self.checkBoxs.append(checkBox)

        btn = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel,
                                     Qt.Horizontal, self.menu)
        btn.accepted.connect(self.menuClose)
        btn.rejected.connect(self.menu.close)
        checkableAction = QWidgetAction(self.menu)
        checkableAction.setDefaultWidget(btn)
        self.menu.addAction(checkableAction)

        headerPos = self.database_widget.mapToGlobal(self.horizontalHeader.pos())

        posY = headerPos.y() + self.horizontalHeader.height()
        posX = headerPos.x() + self.horizontalHeader.sectionPosition(index)
        self.menu.exec_(QPoint(posX, posY))

    def menuClose(self):
        self.keywords[self.col] = []
        for element in self.checkBoxs:
            if element.isChecked():
                self.keywords[self.col].append(element.text())
        self.filterdata()
        self.menu.close()

    def clearFilter(self):
        for i in range(self.database_widget.rowCount()):
            self.database_widget.setRowHidden(i, False)

    def filterdata(self):

        columnsShow = dict([(i, True) for i in range(self.database_widget.rowCount())])

        for i in range(self.database_widget.rowCount()):
            for j in range(self.database_widget.columnCount()):
                item = self.database_widget.item(i, j)
                if self.keywords[j]:
                    if item.text() not in self.keywords[j]:
                        columnsShow[i] = False
        for key, value in columnsShow.iteritems():
            self.database_widget.setRowHidden(key, not value)
            self.database_widget.setRowHidden(key, not value)



# Classe adtc_pref : Fenetre de preferences utilisateurs de Adethec
class adtc_pref(QDialog, adtc_preferences.Ui_ADTC_pref_window):

    # A l'initialisation d'un objet adtc_pref :
    def __init__(self, parent=None):
        super(adtc_pref, self).__init__(parent)           # On hérite du constructeur parent
        self.setupUi(self)
        
    # Affichage principal
    def main(self):
        self.show()
        
        

# A l'appel du programme, on créé une entité mdtc_ui
if __name__=='__main__':
    APP = QApplication(sys.argv)
    MDTC = adtc_ui()
    MDTC.main()
    APP.exec_()
    #print MDTC.PRM
    
