#! /usr/bin/env python
#­* ­coding: utf­8 ­*
#----------------------------------------------------------------------#
# Programme de fitting pour une reconstruction type Arrhenius
#
# Débuté le 22/11/2016
# Auteur : Valentin Biasi
#
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import numpy as np
#
import adtc_plot
#
from scipy.optimize import minimize
#from scipy.optimize import root
#from scipy.optimize import differential_evolution
from scipy.optimize import basinhopping
from scipy.integrate import odeint
from scipy.integrate import ode
from numpy.linalg import inv

# Gestion des UTF8 en Qt
try:
    _fromUtf8 = QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s
        
# Classe espece chimique
class Species(object):
    
    def __init__(self, name, Y0, rho):
        
        self.name = _fromUtf8(name)
        self.Y0 = float(Y0)
        self.rho = int(rho) 
        
        self.Y = []
        
# Classe reaction chimique
class Reactions(object):
    
    def __init__(self, reactant, product, mu, oxy, A, Ea, n, gamma, bool_mu, bool_A, bool_Ea, bool_n, bool_gamma):
        
        self.reactant = int(reactant)   # ID reactif
        
        try:
            self.product = int(product) # ID produit solide
        except ValueError:
            self.product = 0            # Sauf si inexistant
            
        try:
            self.mu = float(mu)         # Coef stoechio. massique
        except ValueError:
            self.mu = 0.0               # Sauf si pas de produit

        if oxy == Qt.Checked:           # Bool reaction oxydante
            self.oxy = True
        else:
            self.oxy = False
            
        self.A = float(A)               # Param Arrhenius
        self.Ea = float(Ea)
        self.n = float(n)
        self.gamma = float(gamma)
        
        # Parametres a optimiser (checkbox)
        if bool_mu == Qt.Checked:
            self.bool_mu = True
        else:
            self.bool_mu = False
            
        if bool_A == Qt.Checked:
            self.bool_A = True
        else:
            self.bool_A = False
            
        if bool_Ea == Qt.Checked:
            self.bool_Ea = True
        else:
            self.bool_Ea = False
        
        if bool_n == Qt.Checked:
            self.bool_n = True
        else:
            self.bool_n = False
            
        if bool_gamma == Qt.Checked:
            self.bool_gamma = True
        else:
            self.bool_gamma = False

    
#----------------------------------------------------------------------#
# Lecture des parametres du modele de reactions d'Arrhenius
def get_fit_parameters( adtc_UI ):
    
    # Tableau : Especes chimiques solides
    adtc_UI.species = []
    for i in range( adtc_UI.table_constituants.rowCount() ):
        
        Esp = Species( adtc_UI.table_constituants.item(i,0).text(), \
                        adtc_UI.table_constituants.item(i,1).text(), \
                        adtc_UI.table_constituants.item(i,2).text())
                    
        if Esp.Y0 > 1.0:
            adtc_UI.statusbar.showMessage('Oops : Y0 values could not exceed 1')
            
        adtc_UI.species.append( Esp )
        
    # Tableau : Reactions chimiques
    adtc_UI.reactions = []
    for i in range( adtc_UI.table_reactions.rowCount() ):
        
        Reac = Reactions( adtc_UI.table_reactions.item(i,0).text(), \
                        adtc_UI.table_reactions.item(i,1).text(), \
                        adtc_UI.table_reactions.item(i,2).text(), \
                        adtc_UI.table_reactions.item(i,3).checkState(), \
                        adtc_UI.table_reactions.item(i,4).text(), \
                        adtc_UI.table_reactions.item(i,5).text(), \
                        adtc_UI.table_reactions.item(i,6).text(), \
                        adtc_UI.table_reactions.item(i,7).text(), \
                        adtc_UI.table_reactions.item(i,2).checkState(), \
                        adtc_UI.table_reactions.item(i,4).checkState(), \
                        adtc_UI.table_reactions.item(i,5).checkState(), \
                        adtc_UI.table_reactions.item(i,6).checkState(), \
                        adtc_UI.table_reactions.item(i,7).checkState() )
        
        if Reac.mu > 1.0:
            adtc_UI.statusbar.showMessage('Oops : Mu values could not exceed 1')
            
        adtc_UI.reactions.append( Reac )
    
    
#----------------------------------------------------------------------#
# Fonction ODE pour le modele generique d'Arrhenius
# Input : 
#        - vec_Y : Yi sous forme de vecteur en i
#        - scal_T : Temperature en K (scalaire)
# Output :
#        - dYdT : d/dT(Yi)  sous forme de vecteur en i
#
# d/dT(Yi) = sum( -1/beta * A * exp(Ea/R/T) * Yi**n )
# ou i est reactif et produit suivant les reactions
def ode_function( vec_Y, scal_T, species, reactions, beta, atmo, vec_Y0 ):

    vec_dYdT = vec_Y * 0. # Init dYdT
    Rgaz = 8.314
    
    # Pour chaque reaction
    for reac in reactions:
        
        # Si la reaction est active dans l'atmosphere
        if not( atmo=='N2' and reac.oxy ):
                
            reactant = reac.reactant
            product = reac.product
            
            # Calcul des nouveaux maximums de Y0
            vec_Y0[ reactant-1 ] = max(vec_Y0[ reactant-1 ] , vec_Y[ reactant-1 ] )
            vec_Y0[ reactant-1 ] = max(vec_Y0[ reactant-1 ] , 1e-7 )
            
            ### Formulation Arrhenius ###
            delta_Y = 1 / (beta/60.0) * reac.A * np.exp( - reac.Ea / Rgaz / scal_T ) * (vec_Y[ reactant-1 ] ** reac.n )  * vec_Y0[ reactant-1 ] ** (1.-reac.n) * ((beta/60.0) ** reac.gamma)
            
            # Elimine les NaN
            if delta_Y != delta_Y:
                delta_Y = 0.0
            
            # Variation en dYdT pour le reactif
            vec_dYdT[ reactant-1 ] += - delta_Y
            
            # Variation en dYdT pour le produit
            if product <> 0:
                vec_dYdT[ product-1 ] += + delta_Y * reac.mu
                
    return vec_dYdT
    
    
#----------------------------------------------------------------------#
# Fonction ODE pour le modele generique d'Arrhenius en isotherme
# Idem ode_function sauf que T=cst
def ode_function_isoT( vec_Y, scal_tps, species, reactions, isoT, atmo, vec_Y0 ):

    vec_dYdT = vec_Y * 0. # Init dYdT
    Rgaz = 8.314
    
    # Pour chaque reaction
    for reac in reactions:
        
        # Si la reaction est active dans l'atmosphere
        if not( atmo=='N2' and reac.oxy ):
                
            reactant = reac.reactant
            product = reac.product
            
            # Calcul des nouveaux maximums de Y0
            vec_Y0[ reactant-1 ] = max(vec_Y0[ reactant-1 ] , vec_Y[ reactant-1 ] )
            vec_Y0[ reactant-1 ] = max(vec_Y0[ reactant-1 ] , 1e-7 )
            
            ### Formulation Arrhenius ###
            delta_Y = reac.A * np.exp( - reac.Ea / Rgaz / isoT ) * (vec_Y[ reactant-1 ] ** reac.n) * vec_Y0[ reactant-1 ] ** (1.-reac.n)
            
            # Elimine les NaN
            if delta_Y != delta_Y:
                delta_Y = 0.0
            
            # Variation en dYdT pour le reactif
            vec_dYdT[ reactant-1 ] += - delta_Y
            
            # Variation en dYdT pour le produit
            if product <> 0:
                vec_dYdT[ product-1 ] += + delta_Y * reac.mu
                
    return vec_dYdT
    

#----------------------------------------------------------------------#
# Fonction evaluation du residu d'optimisation
def epsilon_function( adtc_UI ):
    
    eps = 0 # Init du residu
    
    # Pour chaque indice dans DB
    for x_db in adtc_UI.DB:
        
        # Si indice cheched et filename trouvé
        if (x_db.checked == True) and (x_db.filename):
            
            # Parametres 
            substract_blank = adtc_UI.checkbox_substract_blank.isChecked()
            bool_smoothing = adtc_UI.checkbox_smoothing.isChecked()
            smoothing_value = adtc_UI.spinbox_smoothing.value()
            
            # Prise en compte de la ligne de base
            if substract_blank and x_db.blank_id:
                m = x_db.m - x_db.m_blank
                HF = x_db.HF - x_db.HF_blank
            else:
                m = x_db.m
                HF = x_db.HF
            
            # Vecteur temperatures
            T = x_db.Ts + 273.15
            
            # Prise en compte du retard du a la conduction thermique
            if adtc_UI.check_heat_diffusion.isChecked():
                diffusivity = float(adtc_UI.line_diffusivity.text())
                sample_size = float(adtc_UI.line_sample_size.text())
                T += heat_transfer_delay( x_db.heat_velocity, adtc_UI.PRM.Tmin, adtc_UI.PRM.Tmax, diffusivity, sample_size )
                print heat_transfer_delay( x_db.heat_velocity, adtc_UI.PRM.Tmin, adtc_UI.PRM.Tmax, diffusivity, sample_size )
            
            # Valeur experimentale de Y
            Y_exp = m/m[0]
            Y_exp = adtc_plot.smooth_value(Y_exp, bool_smoothing, smoothing_value)
            # Valeur experimentale de dYdT
            dYdT_exp = np.gradient(Y_exp, (adtc_UI.PRM.Tmax - adtc_UI.PRM.Tmin)/adtc_UI.PRM.n_interp )
            
            # Valeurs initiales de Yi0
            Y0=[]
            for spec in adtc_UI.species:
                Y0.append(spec.Y0)
            
            ### Resolution du systeme d'ODE ###
            Y_solve = odeint( ode_function, Y0, T, args=(adtc_UI.species, adtc_UI.reactions, x_db.heat_velocity, x_db.atmo, Y0))#, atol=1.0e-3, hmin=1.0e-6)
            
            # Y_solve = np.nan_to_num( Y_solve )
            
            # Y = Somme Yi
            x_db.Y_fit = np.sum( Y_solve, 1 )
            # Calcul du gradient dYdT
            x_db.dYdT_fit = np.gradient(x_db.Y_fit, (adtc_UI.PRM.Tmax - adtc_UI.PRM.Tmin)/adtc_UI.PRM.n_interp )
            # Evaluation du residu au sens des moindres carres
            #x_db.chi2 = np.sqrt( 1.0 / adtc_UI.PRM.n_interp * np.sum(( x_db.dYdT_fit - dYdT_exp ) ** 2) )
            #x_db.chi2 = (1.0 / adtc_UI.PRM.n_interp) * np.sum( np.absolute(  x_db.Y_fit - Y_exp ) )
            x_db.chi2 = np.mean( np.absolute(  x_db.dYdT_fit - dYdT_exp ) ) / np.amax(np.absolute(dYdT_exp))
            # Residu total = Somme des residus pour chaque reaction
            #eps = eps + np.sum(( x_db.dYdT_fit - dYdT_exp ) ** 2)
            eps = eps + np.sum(( x_db.Y_fit - Y_exp ) ** 2)
            

    # Racine du residu
    eps = np.sqrt( eps )
    
    adtc_UI.statusbar.showMessage("TGA fitting in progress : epsilon = %.6f" % (eps))
        
    return eps


#----------------------------------------------------------------------#
# Fonction evaluation du residu d'optimisation mis en forme pour fonction 'minimize'
def residual( X, adtc_UI ):
    
    # Lecture du vecteur parametre d'optimisation et reinjection dans la structure de donnees
    j = 0
    for ireac in range(len(adtc_UI.reactions)):
        
        if adtc_UI.reactions[ireac].bool_A:
            adtc_UI.reactions[ireac].A = 10**( X[j] )
            j += 1
            
        if adtc_UI.reactions[ireac].bool_Ea:
            adtc_UI.reactions[ireac].Ea = ( X[j] ) * 1.e5
            j += 1
            
        if adtc_UI.reactions[ireac].bool_n:
            adtc_UI.reactions[ireac].n = X[j]
            j += 1
            
        if adtc_UI.reactions[ireac].bool_gamma:
            adtc_UI.reactions[ireac].gamma = X[j]
            j += 1
        
        if adtc_UI.reactions[ireac].bool_mu:
            adtc_UI.reactions[ireac].mu = X[j]
            j += 1
    
    # Calcul du residu brut a partir de la structure de donnees
    eps = epsilon_function( adtc_UI )
    
    # Renvoi du residu
    return eps


#----------------------------------------------------------------------#
# Fonction optimisation du modele d'Arrhenius
def optimize( adtc_UI ):
    
    # Creation vecteur des parametres a optimiser
    X0 = []
    bounds = []
    for ireac in range(len(adtc_UI.reactions)):
        
        if adtc_UI.reactions[ireac].bool_A:
            X0.append( np.log10( adtc_UI.reactions[ireac].A ) )
            bounds.append( (0.01, 20) )
            
        if adtc_UI.reactions[ireac].bool_Ea:
            X0.append( ( adtc_UI.reactions[ireac].Ea ) * 1.e-5 )
            bounds.append( (0.01, 20) )
            
        if adtc_UI.reactions[ireac].bool_n:
            X0.append( adtc_UI.reactions[ireac].n )
            bounds.append( (0.01, 20) )
        
        if adtc_UI.reactions[ireac].bool_gamma:
            X0.append( adtc_UI.reactions[ireac].gamma )
            bounds.append( (-10, 10) )
        
        if adtc_UI.reactions[ireac].bool_mu:
            try:
                X0.append( adtc_UI.reactions[ireac].mu )
                bounds.append( (0.0, 1.0) )
            except ValueError:
                adtc_UI.statusbar.showMessage("Mu values must be initialized for optimization")
                return
                

    ### Fonction de minimisation des residus ###
    if str(adtc_UI.combo_optim_method.currentText()) == 'Simplex (Nelder-Mead)':
        res = minimize( residual, X0, args=(adtc_UI,), method='Nelder-Mead')
        
    elif str(adtc_UI.combo_optim_method.currentText()) == 'Bounded Quasi-Newton (L-BFGS-B)':
        res = minimize( residual, X0, args=(adtc_UI,), method='L-BFGS-B', bounds=bounds, options={'ftol':1.0e-5} )
        
    elif str(adtc_UI.combo_optim_method.currentText()) == 'Stochastic + Quasi-Newton (Basin-Hopping)':
        #res = differential_evolution( residual, bounds=bounds, args=(adtc_UI,), maxiter=20)
        res = differential_evolution(residual, bounds=bounds, args=(adtc_UI,), strategy='best2bin', maxiter=200)
        
    elif str(adtc_UI.combo_optim_method.currentText()) == 'Stochastic + Bounded Quasi-Newton (Basin-Hopping)':
        options_solv={'ftol':1.0e-5}
        res = basinhopping( residual, X0, minimizer_kwargs={ "args":(adtc_UI,), "method": "L-BFGS-B", "bounds":bounds, "options":options_solv}, stepsize=0.1, niter_success=20)#, interval=10)
        
    else:
        adtc_UI.statusbar.showMessage("Oops : Unrecognized optimization method")
        
    #res = minimize( residual, X0, args=(adtc_UI,), method='TNC', bounds=bounds ) NOK
    #res = basinhopping( residual, X0, minimizer_kwargs={ "args":(adtc_UI,), "method": "Nelder-Mead"}, stepsize=1.0, niter_success=5, interval=10)
    #res = minimize( residual, X0, args=(adtc_UI,), method='SLSQP', bounds=bounds ) NOK
    
    # Renvoi des parametres optimises
    X0 = res.x
    
    # Lecture des parametres optimises et ecriture dans tableau Qt
    j = 0
    for ireac in range(len(adtc_UI.reactions)):
        
        if adtc_UI.reactions[ireac].bool_A:
            adtc_UI.reactions[ireac].A = 10**( X0[j] )
            adtc_UI.table_reactions.item(ireac, 4).setText( '%.3e' % (adtc_UI.reactions[ireac].A) )
            j += 1
            
        if adtc_UI.reactions[ireac].bool_Ea:
            adtc_UI.reactions[ireac].Ea = ( X0[j] ) * 1.e5
            adtc_UI.table_reactions.item(ireac, 5).setText( '%.3e' % (adtc_UI.reactions[ireac].Ea) )
            j += 1
            
        if adtc_UI.reactions[ireac].bool_n:
            adtc_UI.reactions[ireac].n = X0[j]
            adtc_UI.table_reactions.item(ireac, 6).setText( '%g' % (adtc_UI.reactions[ireac].n) )
            j += 1
            
        if adtc_UI.reactions[ireac].bool_gamma:
            adtc_UI.reactions[ireac].gamma = X0[j]
            adtc_UI.table_reactions.item(ireac, 7).setText( '%g' % (adtc_UI.reactions[ireac].gamma) )
            j += 1
            
        if adtc_UI.reactions[ireac].bool_mu:
            adtc_UI.reactions[ireac].mu = X0[j]
            adtc_UI.table_reactions.item(ireac, 2).setText( '%g' % (adtc_UI.reactions[ireac].mu) )
            j += 1

    print X0
    
    
    
#----------------------------------------------------------------------#
# Fonction resolution instationnaire de l'equation de la chaleur
# en coordonnees spheriques pour calculer le retard de temperature
# entre la temperature exterieure du materiau et le coeur
def heat_transfer_delay( heat_velocity, Tmin, Tmax, diffusivity, sample_size ):

    ### Parametres developpeurs ############################################
    nr = 20             # Nb de points spatiaux
    nt = 50             # Nb de points temporels

    heat_velocity = heat_velocity / 60.0      # beta en sec.
    R = sample_size * 1.0e-3 / 2.0                     # Rayon de l'echantillon suppose spherique

    dr = R / nr             # Pas d'espace
    t_final = (Tmax - Tmin) / heat_velocity # temps total d'integration
    dt = t_final / nt       # Pas temporel

    gamma = diffusivity * dt / dr

    # Vecteur r : position ds points spatiaux
    vec_r = np.linspace(1e-8, R, nr+1)
    # Vecteur t : temps espaces de dt
    vec_t = np.linspace(0.0, t_final, nt+1)
    # Vecteur Tr : profil de temperature pour tous les ri
    vec_Tr = vec_r * 0.0 + Tmin
    # Vecteur Text : Evolution de la temperature exterieure dans le temps
    vec_Text = np.linspace(Tmin, Tmax, nt+1)
    # Vecteur Tmoy : Evolution de la moyenne de temperature de l'echantillon
    vec_Tmoy = vec_Text * 0 + Tmin
    # Vecteur Tint : Evolution de la temperature au centre le l'echantillon
    vec_Tint = vec_Text * 0 + Tmin

    ### Creation de la matrice A : A*T=B

    # Diagonale i=j
    diag  = np.eye( nr+1 )
    diag = diag * ( 1 + 2 * gamma / dr )
    A = diag

    # Diagonale sup i=j+1
    vec_diag = - (gamma/vec_r[:-1]) - gamma/dr
    A += np.diag( vec_diag, k=1 )

    # Diagonale inf i=j-1
    vec_diag =  + (gamma/vec_r[1:]) - gamma/dr
    A += np.diag( vec_diag, k=-1 )

    # Condition limite de flux nul
    A[0,0] = 1.0
    A[0,1] = -1.0

    # Condition limite de temperature imposee
    A[-1,-1] = 1.0
    A[-1,-2] = 0.0

    # Inversion de A (vu que A est inchange au cours de l'integration)
    Ainv = inv(A)

    # Pour chaque pas de temps
    for j in range(1,len(vec_t)):
        
        ### Vecteur B =T(j-1)
        B = vec_Tr
        B[0] = 0    # Condition limite de flux nul
        B[-1] = vec_Text[j]     # Condition limite de temperature imposee

        ### T(j) = A^-1 * B
        vec_Tr = np.dot( Ainv, B )

        # Calcul de Tmoy par discretisation de la sphere
        Tmoy = 0.0
        for i in range(1,len(vec_r)):
            Tmoy += vec_Tr[i-1] * (vec_r[i] ** 3 - vec_r[i-1] ** 3)
            
        Tmoy = Tmoy / R ** 3
        vec_Tmoy[j] = Tmoy

        # Ecriture de Tint
        vec_Tint[j] = vec_Tr[0]
    
    
    delay_T = vec_Tmoy[-1]-vec_Text[-1]
    return delay_T
    

#----------------------------------------------------------------------#
# Reinitialisation des valeurs initiales de fitting
def reset_fitting_values( adtc_UI ):
    
    # Lecture des tableaux reactions et especes (securite pour eviter probleme de lecture)
    get_fit_parameters(adtc_UI)
    
    # Pour chaque reaction, pour chaque variable, generation d'un nombre aleatoire borne
    for ireac in range(len(adtc_UI.reactions)):
        if adtc_UI.reactions[ireac].bool_A:
            adtc_UI.table_reactions.item(ireac, 4).setText( '%.3e' % (10 ** np.random.uniform(0.01, 20)) )
        
        if adtc_UI.reactions[ireac].bool_Ea:
            adtc_UI.table_reactions.item(ireac, 5).setText( '%.3e' % (1.0e5 * np.random.uniform(0.01, 20)) )
        
        if adtc_UI.reactions[ireac].bool_n:
            adtc_UI.table_reactions.item(ireac, 6).setText( '%g' % ( np.random.uniform(0.4, 3)) )

        if adtc_UI.reactions[ireac].bool_gamma:
            adtc_UI.table_reactions.item(ireac, 7).setText( '%g' % ( np.random.uniform(-2, 2)) )
            
        if adtc_UI.reactions[ireac].bool_mu:
            adtc_UI.table_reactions.item(ireac, 2).setText( '%g' % ( np.random.uniform(0.0, 1.0)) )




