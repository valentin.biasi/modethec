#! /usr/bin/python
#­* ­coding: utf­8 ­*
#----------------------------------------------------------------------#
# Module de visualisation de l'aide modethec via fichier qhc
#
# Au préalable, compilez l'aide sphinx de modethec au format qthelp
# Puis lancez la génération la "collection Qt"
#
# cd doc/
# make qthelp
# qcollectiongenerator build/qthelp/modethec.qhcp
#
# Débuté le 19/11/2018
# Auteur : Valentin Biasi
#
import sys

from PyQt4.QtGui import *
from PyQt4.QtCore import *
from PyQt4.QtHelp import QHelpEngine

# Fichier qhc de l'aide modethec
qhc_modethec = "/d/vbiasi/modethec/doc/build/qthelp/modethec.qhc"
# Url Qthelp
basename_modehec = "qthelp://org.sphinx.modethec.2018.2.0/doc/index.html" 


#--- Fenêtre d'affichage de l'aide modethec ---------------------------#
class Help(QMainWindow):
    def __init__(self, mainWindow):
        QMainWindow.__init__(self)

        # create help window
        helpwindow = QDockWidget(self)
        helpwindow.setFloating(False)
        helpwindow.setFeatures(QDockWidget.NoDockWidgetFeatures)
        self.resize(1000, 700)
        self.setWindowTitle( "MoDeTheC Documentation" )

        # create QSplitter
        self.mainWidget = QSplitter(Qt.Horizontal)
        
        # create helper and set it up
        self.helper = QHelpEngine(qhc_modethec, self)
        self.helper.setupData()

        # Get the content widget and instanciate HelpBrowser (defined below).
        # The helper.contentWidget() displays the table of content of the help
        self.content = self.helper.contentWidget()
        self.helpBrowser = HelpBrowser(self.mainWidget, self.helper)

        # build widget
        self.mainWidget.insertWidget(0, self.content)
        self.mainWidget.insertWidget(1, self.helpBrowser)
        helpwindow.setWidget(self.mainWidget)
        self.addDockWidget(Qt.TopDockWidgetArea, helpwindow)
        self.mainWidget.setStretchFactor(1, 3)

        # connect the TOC to the help browser.
        self.content.linkActivated.connect( self.helpBrowser.setSource )

        # Show the index of the documentation
        self.helpBrowser.setSource(QUrl(basename_modehec))


#--- TextBrowser capable d'afficher les adresses qthelp et externes ---#
class   HelpBrowser(QTextBrowser):
        def __init__(self, parent, help_engine):
           QTextBrowser.__init__(self)
           self.helpEngine = help_engine

        # call QHelpEngine.fileData() for 'qthelp' resources and use the
        # default implementation for all others url. 'url' param is a QUrl.
        def loadResource(self, type, url):

            if url.scheme() == "qthelp":  
                return self.helpEngine.fileData(url)
            return QTextBrowser.loadResources(type, url)
 
# Affichage simple de l'aide
if __name__ == '__main__':
    APP = QApplication(sys.argv)
    HELPER = Help(QMainWindow)
    HELPER.show()
    sys.exit(APP.exec_()) 
