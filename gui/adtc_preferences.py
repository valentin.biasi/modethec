# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'adtc_preferences.ui'
#
# Created: Mon Feb 27 17:12:40 2017
#      by: PyQt4 UI code generator 4.11.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_ADTC_pref_window(object):
    def setupUi(self, ADTC_pref_window):
        ADTC_pref_window.setObjectName(_fromUtf8("ADTC_pref_window"))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(ADTC_pref_window.sizePolicy().hasHeightForWidth())
        ADTC_pref_window.setSizePolicy(sizePolicy)
        ADTC_pref_window.setMinimumSize(QtCore.QSize(500, 275))
        ADTC_pref_window.setMaximumSize(QtCore.QSize(500, 275))
        self.gridLayout = QtGui.QGridLayout(ADTC_pref_window)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.gridLayout_2 = QtGui.QGridLayout()
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.label = QtGui.QLabel(ADTC_pref_window)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout_2.addWidget(self.label, 0, 0, 1, 1)
        self.line_Tinterp_min = QtGui.QLineEdit(ADTC_pref_window)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.line_Tinterp_min.sizePolicy().hasHeightForWidth())
        self.line_Tinterp_min.setSizePolicy(sizePolicy)
        self.line_Tinterp_min.setMinimumSize(QtCore.QSize(60, 0))
        self.line_Tinterp_min.setObjectName(_fromUtf8("line_Tinterp_min"))
        self.gridLayout_2.addWidget(self.line_Tinterp_min, 0, 1, 1, 1)
        self.label_2 = QtGui.QLabel(ADTC_pref_window)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout_2.addWidget(self.label_2, 1, 0, 1, 1)
        self.line_Tinterp_max = QtGui.QLineEdit(ADTC_pref_window)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.line_Tinterp_max.sizePolicy().hasHeightForWidth())
        self.line_Tinterp_max.setSizePolicy(sizePolicy)
        self.line_Tinterp_max.setMinimumSize(QtCore.QSize(60, 0))
        self.line_Tinterp_max.setObjectName(_fromUtf8("line_Tinterp_max"))
        self.gridLayout_2.addWidget(self.line_Tinterp_max, 1, 1, 1, 1)
        self.label_3 = QtGui.QLabel(ADTC_pref_window)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.gridLayout_2.addWidget(self.label_3, 0, 2, 1, 1)
        self.label_4 = QtGui.QLabel(ADTC_pref_window)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.gridLayout_2.addWidget(self.label_4, 1, 2, 1, 1)
        self.button_OK = QtGui.QPushButton(ADTC_pref_window)
        self.button_OK.setObjectName(_fromUtf8("button_OK"))
        self.gridLayout_2.addWidget(self.button_OK, 5, 4, 1, 1)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem, 0, 3, 1, 1)
        self.label_5 = QtGui.QLabel(ADTC_pref_window)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.gridLayout_2.addWidget(self.label_5, 2, 0, 1, 1)
        self.line_n_interp = QtGui.QLineEdit(ADTC_pref_window)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.line_n_interp.sizePolicy().hasHeightForWidth())
        self.line_n_interp.setSizePolicy(sizePolicy)
        self.line_n_interp.setObjectName(_fromUtf8("line_n_interp"))
        self.gridLayout_2.addWidget(self.line_n_interp, 2, 1, 1, 1)
        self.groupBox = QtGui.QGroupBox(ADTC_pref_window)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.gridLayout_3 = QtGui.QGridLayout(self.groupBox)
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.radio_predefined_colors = QtGui.QRadioButton(self.groupBox)
        self.radio_predefined_colors.setObjectName(_fromUtf8("radio_predefined_colors"))
        self.gridLayout_3.addWidget(self.radio_predefined_colors, 1, 1, 1, 1)
        self.radio_automatic_colors = QtGui.QRadioButton(self.groupBox)
        self.radio_automatic_colors.setChecked(True)
        self.radio_automatic_colors.setObjectName(_fromUtf8("radio_automatic_colors"))
        self.gridLayout_3.addWidget(self.radio_automatic_colors, 0, 1, 1, 1)
        self.gridLayout_2.addWidget(self.groupBox, 3, 0, 1, 2)
        spacerItem1 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout_2.addItem(spacerItem1, 4, 4, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_2, 1, 2, 1, 1)

        self.retranslateUi(ADTC_pref_window)
        QtCore.QObject.connect(self.button_OK, QtCore.SIGNAL(_fromUtf8("released()")), ADTC_pref_window.close)
        QtCore.QMetaObject.connectSlotsByName(ADTC_pref_window)

    def retranslateUi(self, ADTC_pref_window):
        ADTC_pref_window.setWindowTitle(_translate("ADTC_pref_window", "Adethec preferences", None))
        self.label.setText(_translate("ADTC_pref_window", "Interpolation min temperature", None))
        self.line_Tinterp_min.setText(_translate("ADTC_pref_window", "50", None))
        self.label_2.setText(_translate("ADTC_pref_window", "Interpolation max temperature", None))
        self.line_Tinterp_max.setText(_translate("ADTC_pref_window", "1000", None))
        self.label_3.setText(_translate("ADTC_pref_window", "°C", None))
        self.label_4.setText(_translate("ADTC_pref_window", "°C", None))
        self.button_OK.setText(_translate("ADTC_pref_window", "OK", None))
        self.label_5.setText(_translate("ADTC_pref_window", "Interpolation point number", None))
        self.line_n_interp.setText(_translate("ADTC_pref_window", "1000", None))
        self.groupBox.setTitle(_translate("ADTC_pref_window", "Plot line colors", None))
        self.radio_predefined_colors.setText(_translate("ADTC_pref_window", "Predefined colors (Red at 2K/min, ...)", None))
        self.radio_automatic_colors.setText(_translate("ADTC_pref_window", "Automatic colors from the theme", None))

