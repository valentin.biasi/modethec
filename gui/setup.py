README

Installer pyinstaller

dans modethec/gui/

pyinstaller --windowed -i img/adethec.ico adethec.py
pyinstaller --windowed -i img/modethec.ico modethec_gui.py

lancer avec Advanced Installer le fichier auto_installer.aip

Résultat dans auto_installer-SetupFiles/



#! /usr/bin/env python
# -*- coding: utf-8 -*-
#----------------------------------------------------------------------------#
#
# Script pour la compilation cx_freeze de la suite modethec
# Compilation windows avec installateur msi :
# python setup.py bdist_msi
#
# Compilation windows exe simple
# python setup.py build 
"""
Icone sous Windows: il faut:
=> un xxx.ico pour integration dans le exe, avec "icon=xxx.ico"
=> un xxx.png pour integration avec PyQt4 + demander la recopie avec includefiles.
"""
 
import sys, os
from cx_Freeze import setup, Executable
import scipy
import matplotlib
 
#----------------------------------------------------------------------------#
# preparation des options

sys.setrecursionlimit(150000)
scipy_path = os.path.dirname(scipy.__file__)

	
# chemins de recherche des modules
# ajouter d'autres chemins (absolus) si necessaire: sys.path + ["chemin1", "chemin2"]
path = sys.path
 
# options d'inclusion/exclusion des modules
includes = ["matplotlib.backends.backend_tkagg"]  # nommer les modules non trouves par cx_freeze
excludes = ['collections.abc', 'traitlets.traitlets', 'tkinter', 'PyQt4.QtSql', 'PyQt4.QtNetwork', 'PyQt4.QtScript']
packages = ['scipy']  # nommer les packages utilises
 
                                  
                                  
                                  
                                  
                                  
# copier les fichiers non-Python et/ou repertoires et leur contenu:
includefiles = [(matplotlib.get_data_path(), "mpl-data")]
 
if sys.platform == "win32":
	includefiles = [] # : ajouter les recopies specifiques à Windows
	print includefiles
	
elif sys.platform == "linux2":
    pass
    # includefiles += [...] : ajouter les recopies specifiques à Linux
else:
    pass
    # includefiles += [...] : cas du Mac OSX non traite ici
 
# pour que les bibliotheques binaires de /usr/lib soient recopiees aussi sous Linux
binpathincludes = []
if sys.platform == "linux2":
    binpathincludes += ["/usr/lib"]
 
# niveau d'optimisation pour la compilation en bytecodes
optimize = 2
 
# si True, n'affiche que les warning et les erreurs pendant le traitement cx_freeze
silent = True
 
# construction du dictionnaire des options
options = {"path": path,
           "includes": includes,
           "excludes": excludes,
           "packages": packages,
           "include_files": includefiles,
           "bin_path_includes": binpathincludes,
           "create_shared_zip": False,  # <= ne pas generer de fichier zip
           "include_in_shared_zip": False,  # <= ne pas generer de fichier zip
           "compressed": False,  # <= ne pas generer de fichier zip
           "optimize": optimize,
           "silent": silent
           }
 
# pour inclure sous Windows les dll system de Windows necessaires
if sys.platform == "win32":
    options["include_msvcr"] = True
 
#----------------------------------------------------------------------------#
# preparation des cibles
base = None
if sys.platform == "win32":
    base = "Win32GUI"  # pour application graphique sous Windows
    # base = "Console" # pour application en console sous Windows

#----------------------------------------------------------------------------#
# Executable 1 : Adethec ----------------------------------------------------#
exe_adethec = Executable(
    script="adethec.py",
    base=base,
    compress=False,  # <= ne pas generer de fichier zip
    copyDependentFiles=True,
    appendScriptToExe=True,
    appendScriptToLibrary=False,  # <= ne pas generer de fichier zip
    shortcutName="Adethec",
    shortcutDir="DesktopFolder",
    icon='img/adethec.ico'
    )

bdist_msi_options_adethec = {
    'upgrade_code': '{66620F3A-DC3A-11E2-B341-002219E9B01E}', # Cle de logiciel windows unique
    'add_to_path': False,
    'initial_target_dir': r'[ProgramFilesFolder]\%s\%s' % ('Onera','adethec'),
    }

# Commenter cette partie pour ne pas creer d'executable
setup(
    name="adethec",
    version="0.10",
    description="TGA analysis tools",
    author="Onera",
    options={"build_exe": options, 'bdist_msi': bdist_msi_options_adethec},
    executables=[exe_adethec]
    )

#----------------------------------------------------------------------------#
# Executable 1 : Modethec ---------------------------------------------------#
exe_modethec = Executable(
    script="modethec_gui.py",
    base=base,
    compress=False,  # <= ne pas generer de fichier zip
    copyDependentFiles=True,
    appendScriptToExe=True,
    appendScriptToLibrary=False,  # <= ne pas generer de fichier zip
    shortcutName="Modethec",
    shortcutDir="DesktopFolder",
    icon='img/adethec.ico'
    )
	
bdist_msi_options_modethec = {
    'upgrade_code': '{66620F3A-DC3A-11E2-B341-002219E9B01F}', # Cle de logiciel windows unique
    'add_to_path': False,
    'initial_target_dir': r'[ProgramFilesFolder]\%s\%s' % ('Onera','modethec'),
    }

# Commenter cette partie pour ne pas creer d'executable
#setup(
#    name="modethec",
#    version="1.00",
#    description="Thermal degradation of materials analysis",
#    author="Onera",
#    options={"build_exe": options, 'bdist_msi': bdist_msi_options_modethec},
#    executables=[exe_modethec]
#    )
	
