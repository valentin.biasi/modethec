#­* ­coding: utf­8 ­*
#----------------------------------------------------------------------#
# Module de plot XY a partir de fichier HDF5
#
# Débuté le 11/10/2017
# Auteur : Valentin Biasi
#
import h5py
import sys, os
#
from PyQt4.QtGui import *
from PyQt4.QtCore import *
#
import silx.gui.hdf5
#
# Librairies matplotlib pour integration dans pyqt
#~ import matplotlib
#~ matplotlib.use('Qt4agg') # need to call use() before importing plt
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt
#
# Fonction de clean du lyaout
from mdtc_layout import clearLayout

#----------------------------------------------------------------------#
# Lecture du fichier HDF5, mise en place des widget treeview et plot
# et lien du treeview vers la fonction "displayData" qui permet de tracer
# les courbes
def plot_hdf_XY( mdtc_ui ):
    
    # Trouve nom des fichiers vtk et avertissement si fichiers inexistants
    filename = mdtc_ui.foldername + '/' + mdtc_ui.PRM['EXPORT']['nom_exp'].v + '.hdf'
    if not( os.path.isfile(filename) ):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setWindowTitle(u"Fichier HDF5 inexistant")
        msg.setText(u"MoDeTheC est incapable de trouver le fichier : \n"+filename+u"\n\nVérifier qu'il a été correctement exporté")
        msg.exec_()
        
        return
    
    # On efface le layout
    clearLayout(mdtc_ui.gridParam)

    # Création du treeview lisant le .hdf
    widget_treeview_hdf( filename, mdtc_ui )
    
    # Définition du plot style
    set_plot_style(mdtc_ui)
    
    # Création du widget "zone de plot"
    widget_plot_XY(mdtc_ui)
    
    
#----------------------------------------------------------------------#
# Création du treeview lisant le .hdf grace au plugin silx.gui.hdf5.Hdf5TreeView
def widget_treeview_hdf( filename, mdtc_ui ):
    
    # Initialisation du widget Hdf5TreeView vide
    mdtc_ui.treeview = silx.gui.hdf5.Hdf5TreeView(mdtc_ui)

    # Ajout au layout
    mdtc_ui.gridParam.addWidget(mdtc_ui.treeview, 0, 0, 2, 1)
    
    # Lecture du fichier "filename"
    mdtc_ui.treeview.findHdf5TreeModel().appendFile(filename)
    
    # Garde seulement la colonne 0 : Nom
    mdtc_ui.treeview.header().setSections( [0] )
    
    # Autorise la selection multiple de donnees
    mdtc_ui.treeview.setSelectionMode( QAbstractItemView.MultiSelection )
    
    # Connection a la fonction de MAJ du plot displayData
    mdtc_ui.treeview.activated.connect(lambda: displayData(mdtc_ui))
    mdtc_ui.treeview.clicked.connect(lambda: displayData(mdtc_ui))
    mdtc_ui.treeview.entered.connect(lambda: displayData(mdtc_ui))
    

#----------------------------------------------------------------------#
# Fonction de MAJ du plot displayData
def displayData(mdtc_ui):
    
    # Liste des elements selectionnes
    selected = list(mdtc_ui.treeview.selectedH5Nodes())

    # Si au moins un Dataset est selectionne...
    for data in selected:
        if data.ntype is h5py.Dataset:
            
            # ... on clear la figure
            mdtc_ui.figure.clf()
            continue
    
    # Ajout d'un nouveau plot
    mdtc_ui.axe = mdtc_ui.figure.add_subplot(111)
    
    # Label abscisse
    mdtc_ui.axe.set_xlabel('t [s]')

    # Pour chaque Dataset selcetionne ...
    for data in selected:
        if data.ntype is h5py.Dataset:
            
            # ... on trouve son nom
            name = data.name
            name = name.split('/')[-1]
            
            # ... et on le plot
            mdtc_ui.axe.plot(data.value[:,0], data.value[:,1], label=name)
            
            # Best fit location pour legende    
            mdtc_ui.axe.legend(loc=0)
    
    # Mise a jour du plot
    mdtc_ui.canvas.draw()
    
    # Recadrage du plot pour best fit de la fenetre
    mdtc_ui.figure.tight_layout()
    
    
#----------------------------------------------------------------------#
# Mise en place d'un widget de plot
def widget_plot_XY(mdtc_ui):

    # Mise en place d'un widget de plot
    mdtc_ui.figure = plt.figure()
    mdtc_ui.canvas = FigureCanvas(mdtc_ui.figure)
    
    # Streching entre treeview et le plot
    sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
    sizePolicy.setHorizontalStretch(2)
    mdtc_ui.canvas.setSizePolicy(sizePolicy)
        
    # Ajout au layout
    mdtc_ui.gridParam.addWidget(mdtc_ui.canvas, 1, 1, 1, 1)
    
    # Ajout de la toolbar
    mdtc_ui.plot_toolbar = NavigationToolbar(mdtc_ui.canvas, mdtc_ui)

    # Ajout au layout de la toolbar
    mdtc_ui.gridParam.addWidget(mdtc_ui.plot_toolbar, 0, 1, 1, 1)
    
    # Affichage d'un plot vide
    displayData(mdtc_ui)
    

#----------------------------------------------------------------------#
# Gestion des styles de plot
# Modifications generales appliquees partout
def set_plot_style(mdtc_ui):
    
    plt.style.use('fivethirtyeight')
    
    plt.rcParams['savefig.transparent'] = True
    plt.rcParams['lines.linewidth'] = 2.0
        
    plt.rcParams['xtick.major.size'] = 6.0
    plt.rcParams['xtick.major.width'] = 2.0
    plt.rcParams['ytick.major.size'] = 6.0
    plt.rcParams['ytick.major.width'] = 2.0
    
    plt.rcParams['xtick.direction'] = 'in'
    plt.rcParams['ytick.direction'] = 'in'
    
    plt.rcParams['axes.edgecolor'] = 'k'
    plt.rcParams['axes.linewidth'] = 2.0
    
    plt.rcParams['font.size'] = 10.0            
    plt.rcParams['xtick.labelsize'] = 14.0
    plt.rcParams['ytick.labelsize'] = 14.0
    plt.rcParams['axes.labelsize'] = 16.0
    plt.rcParams['legend.fontsize'] = 14.0
