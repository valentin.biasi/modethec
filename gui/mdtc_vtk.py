#­* ­coding: utf­8 ­*
#----------------------------------------------------------------------#
# Module de visalusation 2D/3D par mayavi
#
# Débuté le 02/05/2017
# Auteur : Valentin Biasi
#
#~ # Modules traits pour mayavi
from traitsui.api import View, Item
from traits.api import HasTraits, Instance, on_trait_change
#~ #
#~ # Fonctions mayavi (!minimum necessaire!)
from mayavi.core.ui.api import MayaviScene, MlabSceneModel, SceneEditor
from mayavi.sources.vtk_file_reader import VTKFileReader
from mayavi.modules.surface import Surface
from mayavi.modules.axes import Axes
#
#~ from PyQt4.QtGui import *
#~ from PyQt4.QtCore import *
from pyface.qt.QtGui import *
from pyface.qt.QtCore import *
#
import sys, os
#
# Fonction de clean du lyaout
from mdtc_layout import clearLayout

h_ligne = 30


#----------------------------------------------------------------------#
#--- Classe Visualization : initialise un visualisateur mayavi simple
class Visualization(HasTraits):
    
    scene = Instance(MlabSceneModel, ())

    view = View(Item(name=u'scene', editor=SceneEditor(scene_class=MayaviScene), height=250, width=300, show_label=False), resizable=True)

    @on_trait_change('scene.activated')
    def update_plot(self):

        pass


#----------------------------------------------------------------------#
#--- Classe MayaviQWidget : initialise un widget PyQt base sur la classe Visualiation
class MayaviQWidget(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        layout = QVBoxLayout(self)
        layout.setContentsMargins(0,0,0,0)
        layout.setSpacing(0)
        
        self.visualization = Visualization()
        
        self.ui = self.visualization.edit_traits(parent=self, kind='subpanel').control

        layout.addWidget(self.ui)
        self.ui.setParent(self)
        
        
#----------------------------------------------------------------------#
#--- Fonction principale de visualisation mayavi
def visualize_vtk(mdtc_ui):
    
    # Trouve nom des fichiers vtk et avertissement si fichiers inexistants
    filename = mdtc_ui.foldername + '/vtk/' + mdtc_ui.PRM['EXPORT']['nom_exp'].v + '.0.vtk'
    if not( os.path.isfile(filename) ):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setWindowTitle(u"Fichiers VTK inexistants")
        msg.setText(u"MoDeTheC est incapable de trouver les fichiers : \n"+filename+u"\n\nVérifier qu'ils ont été correctement exportés")
        msg.exec_()
        
        return
    
    # On efface le layout
    clearLayout(mdtc_ui.gridParam)
    
    # On efface la scene si ancienne existe
    clear_scene(mdtc_ui)

    # Ajout du widget mayavi + lecture du/des fichiers VTK
    widget_vtk(mdtc_ui, filename)
    
    # Mise en place des widgets de gauche
    widget_modif_vtk(mdtc_ui)
    
    # Affichage des axes
    mayavi_axes(mdtc_ui)
    
    # Affichage du maillage en noir
    mayavi_mesh(mdtc_ui)
    
    # Affichage de la legende
    mayavi_legend(mdtc_ui)

    #print dir(mdtc_ui.mayavi_widget.visualization.scene.engine)


#----------------------------------------------------------------------#
#--- Fonction de visualisation du fichier de maillage via gmsh et mayavi
def visualize_gmsh(mdtc_ui):
    
    # Trouve nom du fichier .msh et avertissement si fichier inexistant
    filename = mdtc_ui.foldername + '/' + mdtc_ui.PRM['GENERAUX']['file_mesh'].v
    extension = filename.split('.')[1]
    if not( os.path.isfile(filename) ) or extension<>'msh':
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setWindowTitle(u"Fichiers maillage inexistant")
        msg.setText(u"MoDeTheC est incapable de traiter le fichier : \n"+filename+u"\n\nVérifier qu'il a été correctement exporté et qu'il est en format .msh")
        msg.exec_()
        
        return
    
    try:
        os.system("gmsh -3 %s -format vtk" % filename)
    except:
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setWindowTitle(u"Erreur de conversion")
        msg.setText(u"MoDeTheC est incapable de traiter le fichier : \n"+filename+u"\n\nVérifier que gmsh est correctement installé et disponible dans le PATH")
        msg.exec_()
        
        return
    
    # On efface le layout
    clearLayout(mdtc_ui.gridParam)
    
    # On efface la scene si ancienne existe
    clear_scene(mdtc_ui)
    
    # Ajout du widget mayavi + lecture du/des fichiers VTK
    filename = filename.split('.')[0] + '.vtk'
    widget_vtk(mdtc_ui, filename)
    
    # Efface le fichier vtk temporaire
    try:
        os.remove(filename)
    except:
        pass
    
    # Mise en forme
    mdtc_ui.mayavi_surface.actor.property.color = (0.0, 0.0, 0.0)
    mdtc_ui.mayavi_surface.actor.property.representation = 'wireframe'
    
    # Mise en place des widgets de gauche
    widget_modif_vtk(mdtc_ui, gmsh=True)
    
    # BUG : impossible d'afficher axes sur un vtk genere par gmsh
    # Affichage des axes
    # mayavi_axes(mdtc_ui)


#----------------------------------------------------------------------#
#--- Ajout du widget mayavi + lecture du/des fichiers VTK
def widget_vtk(mdtc_ui, filename):
        
    # Creation du widget base sur la classe MayaviQWidget
    mdtc_ui.mayavi_widget = MayaviQWidget(mdtc_ui)
    
    # Inegration dans layout
    mdtc_ui.gridParam.addWidget(mdtc_ui.mayavi_widget, 0, 1, 11, 1)
    
    # Mise en forme
    mdtc_ui.mayavi_widget.visualization.scene.scene.background = (1.0, 1.0, 1.0)
    
    # Lecture des fichiers vtk
    mdtc_ui.mayavi_reader = VTKFileReader()
    mdtc_ui.mayavi_reader.initialize(filename)
    mdtc_ui.mayavi_widget.visualization.scene.engine.add_source(mdtc_ui.mayavi_reader)

    # Ajout d'un filtre Surface pour les contours
    mdtc_ui.mayavi_surface = Surface()
    mdtc_ui.mayavi_widget.visualization.scene.engine.add_filter(mdtc_ui.mayavi_surface, mdtc_ui.mayavi_reader)


#----------------------------------------------------------------------#
#--- Ajout des widgets auxiliaires au visaualisateur mayavi
def widget_modif_vtk(mdtc_ui, gmsh=False):
    
    # Checkbox de maillage
    check_mesh = QCheckBox('Plot mesh')
    check_mesh.setChecked(False)
    check_mesh.setMinimumSize( QSize(0, h_ligne) )
    mdtc_ui.gridParam.addWidget(check_mesh, 0, 0, 1, 1)
    
    # Comportement checkbox maillage
    check_mesh.toggled.connect(lambda: view_mesh(mdtc_ui, check_mesh) )
    def view_mesh(mdtc_ui, check_mesh):
        
        if check_mesh.isChecked():
            mdtc_ui.mayavi_mesh.actor.visible = True
        else:
            mdtc_ui.mayavi_mesh.actor.visible = False
    
    # Checkbox des axes   
    check_axes = QCheckBox('Show axes')
    check_axes.setChecked(True)
    check_axes.setMinimumSize( QSize(0, h_ligne) )
    mdtc_ui.gridParam.addWidget(check_axes, 1, 0, 1, 1)
    
    # Comportement checkbox axes
    check_axes.toggled.connect(lambda: view_axes(mdtc_ui, check_axes) )
    def view_axes(mdtc_ui, check_axes):
        
        if check_axes.isChecked():
            mdtc_ui.mayavi_axes.axes.visibility = True
        else:
            mdtc_ui.mayavi_axes.axes.visibility = False
            
    # Checkbox de la legende
    check_legend = QCheckBox('Show legend')
    check_legend.setChecked(True)
    check_legend.setMinimumSize( QSize(0, h_ligne) )
    mdtc_ui.gridParam.addWidget(check_legend, 2, 0, 1, 1)
    
    # Comportement checkbox legende
    check_legend.toggled.connect(lambda: view_legend(mdtc_ui, check_legend) )
    def view_legend(mdtc_ui, check_legend):
        
        if check_legend.isChecked():
            mdtc_ui.mayavi_surface.module_manager.scalar_lut_manager.show_legend = True
        else:
            mdtc_ui.mayavi_surface.module_manager.scalar_lut_manager.show_legend = False
    
    # Checkbox de contour classique
    check_contour = QCheckBox('Plot contour')
    check_contour.setChecked(True)
    check_contour.setMinimumSize( QSize(0, h_ligne) )
    mdtc_ui.gridParam.addWidget(check_contour, 3, 0, 1, 1)
    
    # Comportement checkbox contour
    check_contour.toggled.connect(lambda: view_contour(mdtc_ui, check_contour) )
    def view_contour(mdtc_ui, check_contour):
        
        if check_contour.isChecked():
            mdtc_ui.mayavi_surface.actor.visible = True
        else:
            mdtc_ui.mayavi_surface.actor.visible = False
    
    # Ligne de separation
    line = QFrame()
    line.setFrameShape(QFrame.HLine)
    line.setFrameShadow(QFrame.Sunken)
    mdtc_ui.gridParam.addWidget(line, 4, 0, 1, 1)
    
    # Label variable
    label = QLabel('Variable :')
    label.setMinimumSize( QSize(0, h_ligne) )
    mdtc_ui.gridParam.addWidget(label, 5, 0, 1, 1)
    
    # Combobox liste des variables : attention teste seulement avec variables aux sommets
    combo_var = QComboBox()
    combo_var.setMinimumSize( QSize(0, h_ligne) )
    mdtc_ui.gridParam.addWidget(combo_var, 6, 0, 1, 1)
    
    # Populate combobox avec liste de scalaires VTK du fichier
    combo_var.addItems( mdtc_ui.mayavi_reader._point_scalars_list[:-1] )
    combo_var.addItems( mdtc_ui.mayavi_reader._cell_scalars_list[:-1] )
    
    # Comportement combobox valeurs scalaires
    combo_var.currentIndexChanged.connect(lambda: view_var(mdtc_ui, combo_var))
    def view_var(mdtc_ui, combo_var):
        try:
            mdtc_ui.mayavi_reader.point_scalars_name = combo_var.currentText()
        except:
            mdtc_ui.mayavi_reader.cell_scalars_name = combo_var.currentText()
    
    # Ligne de separation
    line = QFrame()
    line.setFrameShape(QFrame.HLine)
    line.setFrameShadow(QFrame.Sunken)
    mdtc_ui.gridParam.addWidget(line, 7, 0, 1, 1)
    
    # Label variable
    label = QLabel('Timestep : 0 s')
    label.setMinimumSize( QSize(0, h_ligne) )
    mdtc_ui.gridParam.addWidget(label, 8, 0, 1, 1)
    
    # Slider de timestep
    slider_time = QSlider()
    slider_time.setOrientation(Qt.Horizontal)
    slider_time.setMaximum( mdtc_ui.mayavi_reader._max_timestep )       # Dertermine valeur max des timestep
    slider_time.setMinimum( mdtc_ui.mayavi_reader._min_timestep )       # Dertermine valeur min des timestep
    sizePolicy = QSizePolicy( QSizePolicy.Minimum, QSizePolicy.Minimum)
    slider_time.setSizePolicy(sizePolicy)
    mdtc_ui.gridParam.addWidget(slider_time, 9, 0, 1, 1)
    
    # 
    slider_time.valueChanged.connect(lambda: change_timestep(mdtc_ui, slider_time))    
    def change_timestep(mdtc_ui, slider_time):
        mdtc_ui.mayavi_reader.timestep = slider_time.value()
        real_time = slider_time.value() * mdtc_ui.PRM['EXPORT']['dt_exp'].v
        label.setText( 'Timestep : %i s' % (real_time) )
    
    # Spacer vertical
    spacer_V = QSpacerItem(10, 100, QSizePolicy.Minimum, QSizePolicy.Expanding)
    mdtc_ui.gridParam.addItem(spacer_V, 10, 0, 1, 1)

    # Dans le cas ou gmsh=True
    # c'est-a-dire que plot mesh est demande, on desactive les boutons
    if gmsh==True:
        check_mesh.setEnabled(False)
        check_axes.setEnabled(False)
        check_legend.setEnabled(False)
        check_contour.setEnabled(False)
        combo_var.setEnabled(False)
        slider_time.setEnabled(False)


#----------------------------------------------------------------------#
#--- Ajout des axes sur le plot VTK
def mayavi_axes(mdtc_ui):

    # Creation et affichage des axes
    mdtc_ui.mayavi_axes = Axes()
    mdtc_ui.mayavi_widget.visualization.scene.engine.add_filter(mdtc_ui.mayavi_axes, mdtc_ui.mayavi_reader)

    # Mise en forme
    mdtc_ui.mayavi_axes.property.color = (0.0, 0.0, 0.0)
    mdtc_ui.mayavi_axes.title_text_property.color = (0.0, 0.0, 0.0)
    mdtc_ui.mayavi_axes.label_text_property.color = (0.0, 0.0, 0.0)
    mdtc_ui.mayavi_axes.label_text_property.bold = False
    mdtc_ui.mayavi_axes.label_text_property.italic = False
    mdtc_ui.mayavi_axes.axes.number_of_labels = 7
    mdtc_ui.mayavi_axes.axes.y_axis_visibility = False
    

#----------------------------------------------------------------------#
#--- Ajout du maillage sur le plot VTK
def mayavi_mesh(mdtc_ui):

    # Creation et affichage du maillage
    mdtc_ui.mayavi_mesh = Surface()
    mdtc_ui.mayavi_widget.visualization.scene.engine.add_filter(mdtc_ui.mayavi_mesh, mdtc_ui.mayavi_reader)
    
    # Mise en forme
    mdtc_ui.mayavi_mesh.actor.mapper.scalar_visibility = False
    mdtc_ui.mayavi_mesh.actor.property.specular_color = (0.0, 0.0, 0.0)
    mdtc_ui.mayavi_mesh.actor.property.diffuse_color = (0.0, 0.0, 0.0)
    mdtc_ui.mayavi_mesh.actor.property.ambient_color = (0.0, 0.0, 0.0)
    mdtc_ui.mayavi_mesh.actor.property.color = (0.0, 0.0, 0.0)
    mdtc_ui.mayavi_mesh.actor.property.representation = 'wireframe'
    
    mdtc_ui.mayavi_mesh.actor.visible = False


#----------------------------------------------------------------------#
#--- Ajout de la legende sur le plot VTK
def mayavi_legend(mdtc_ui):
    
    # Affichage de la legende
    mdtc_ui.mayavi_surface.module_manager.scalar_lut_manager.show_legend = True
    
    # Mise en forme
    mdtc_ui.mayavi_surface.module_manager.scalar_lut_manager.title_text_property.color = (0.0, 0.0, 0.0)
    mdtc_ui.mayavi_surface.module_manager.scalar_lut_manager.label_text_property.color = (0.0, 0.0, 0.0)
    mdtc_ui.mayavi_surface.module_manager.scalar_lut_manager.label_text_property.italic = False
    mdtc_ui.mayavi_surface.module_manager.scalar_lut_manager.label_text_property.bold = False
    mdtc_ui.mayavi_surface.module_manager.scalar_lut_manager.scalar_bar_representation.position2 = [ 0.1,  0.9]
    mdtc_ui.mayavi_surface.module_manager.scalar_lut_manager.scalar_bar_representation.position = [ 0.85,  0.05]
    mdtc_ui.mayavi_surface.module_manager.scalar_lut_manager.scalar_bar.label_format = '%-#6.5g'
    mdtc_ui.mayavi_surface.module_manager.scalar_lut_manager.number_of_labels = 11


#----------------------------------------------------------------------#
#--- Efface la scene precendente pour ne pas se superposer avec la nouvelle
def clear_scene(mdtc_ui):
    
    # Permet d'empecher l'affichage de warnings temporairement
    _stderr = sys.stderr
    _stdout = sys.stdout
    null = open(os.devnull,'wb')
    sys.stdout = sys.stderr = null
    
    # Si scenes[0] existe on l'efface
    try:
        mdtc_ui.mayavi_widget.visualization.scene.engine.scenes[0].remove()
    except:
        pass
        
    # Remet l'affichage des warnings
    sys.stderr = _stderr
    sys.stdout = _stdout
    
