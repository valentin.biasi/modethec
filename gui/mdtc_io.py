#! /usr/bin/python
#­* ­coding: utf­8 ­*
#----------------------------------------------------------------------#
# Programme d'entrée / sortie des paramètres MODETHEC GUI
# Lis et écrit la structure de paramètres modethec
#
# Débuté le 18/02/2015
# Auteur : Valentin Biasi
#
import shlex
#
import io
#encoding = 'utf-8'
#
import mdtc_str


#----------------------------------------------------------------------#
# Enregistrement du fichier de parametres dans filesave
def save_str(STR,filesave):

    # Force la version du fichier de sortie
    STR['GENERAUX']['version'].v = mdtc_str.version

    # Ouverture de filesave
    file_prm = io.open(filesave, 'w+', encoding='utf-8')
    
    # Ecriture de l'en-tete
    entete_main(file_prm)
        
    # Boucle sur tous les elements du dictionnaire
    entete_section(file_prm,'GENERAUX')
    for key in STR['GENERAUX']:
        write_row(file_prm,key,STR['GENERAUX'])

    # Boucle sur tous les elements du dictionnaire
    entete_section(file_prm,'PHYSIQUE')
    for key in STR['PHYSIQUE']:
        write_row(file_prm,key,STR['PHYSIQUE'])
        
    entete_section(file_prm,'ESPECES')
    for iesp in range(STR['PHYSIQUE']['nesp'].v):
        entete_soussec(file_prm,'ESPECE',iesp)
        for key in STR['ESPECE'][iesp]:
            write_row(file_prm,key,STR['ESPECE'][iesp],indent=1)
            
    entete_section(file_prm,'REACTIONS')
    for ireac in range(STR['PHYSIQUE']['nreac'].v):
        entete_soussec(file_prm,'REACTION',ireac)
        for key in STR['REACTION'][ireac]:
            write_row(file_prm,key,STR['REACTION'][ireac],indent=1)
            
    # Boucle sur tous les elements du dictionnaire
    entete_section(file_prm,'NUMERIQUE')
    for key in STR['NUMERIQUE']:
        write_row(file_prm,key,STR['NUMERIQUE'])

    # Boucle sur tous les elements du dictionnaire
    entete_section(file_prm,'CONDITIONS INITIALES')
    for idom in range(STR['PHYSIQUE']['ndom'].v):
        entete_soussec(file_prm,'DOMAINE',idom)
        for key in STR['INIT'][idom]:
            write_row(file_prm,key,STR['INIT'][idom],indent=1)

    entete_section(file_prm,'CONDITIONS LIMITES')
    for ilim in range(STR['PHYSIQUE']['nlim'].v):
        entete_soussec(file_prm,'LIMITE',ilim)
        for key in STR['LIM'][ilim]:
            if key == 'isend_coupling':
                write_triplet(file_prm,STR['LIM'][ilim]['isend_coupling'],indent=2)
            elif key == 'irecv_coupling':
                write_triplet(file_prm,STR['LIM'][ilim]['irecv_coupling'],indent=2)
            else:
                write_row(file_prm,key,STR['LIM'][ilim],indent=1)
            
    # Boucle sur tous les elements du dictionnaire
    entete_section(file_prm,'SUIVI')
    for key in STR['SUIVI']:
        if key == 'coord_sensors':
            write_triplet_sensors(file_prm,STR['SUIVI'][key],indent=1)
        else:
            write_row(file_prm,key,STR['SUIVI'])

    # Boucle sur tous les elements du dictionnaire
    entete_section(file_prm,'EXPORT')
    for key in STR['EXPORT']:
        if key == 'var_exp': 
            write_triplet(file_prm,STR['EXPORT']['var_exp'],indent=1)
        else:
            write_row(file_prm,key,STR['EXPORT'])

    # Ecriture du 'pied de page'
    entete_end(file_prm)

    # Fermeture de filename
    file_prm.close()


#----------------------------------------------------------------------#
# Ouverture du fichier de parametres depuis fileopen
def open_str(STR,fileopen):
    dict_name = 'GENERAUX'
    iel = 0
    val_str = ''
    argname = ''
    
    # Ouverture de fileopen
    file_prm = io.open(fileopen, 'r', encoding='utf-8')

    # Pour chaque ligne du fichier source
    for ligne_prm in file_prm:
        newarg = False                          # Pas de nouvel argument pour le moment
        ligne_prm = ligne_prm.strip()           # Trim de la ligne
        
        #print ligne_prm
        # Est-ce-que la ligne contient une nouvelle en-tete ?
        dict_name,iel = getposition(ligne_prm,dict_name,iel)

        # Est-ce-que la ligne contient un paramètre ?
        val_str,argname,newarg = getargument(ligne_prm,argname,val_str,newarg)
        #print dict_name,iel,val_str,argname,newarg
        # Remplir les dicos si nouveau paramètre
        
        #print ligne_prm
        #print dict_name,iel,val_str,argname,newarg
        if newarg:
            try:
                fullfill(STR,dict_name,iel,argname,val_str)
            except:
                print u"L'élément suivant ne peu etre lu"
                print 'Entree : '+argname
                print 'Dictionnaire : '+dict_name
                print 'Element : '+str(iel)
                print 'Valeur : '+val_str
                print ''
              
    # Fermeture du fichier source
    file_prm.close()
    
    # Complete l'item nom_nesp après lecture
    STR['PHYSIQUE']['nom_nesp'].v = []
    for i in range( STR['PHYSIQUE']['nesp'].v ):
        STR['PHYSIQUE']['nom_nesp'].v.append( STR['ESPECE'][i]['nom_esp'].v )
        
    STR['PHYSIQUE']['nom_nreac'].v = []
    for i in range( STR['PHYSIQUE']['nreac'].v ):
        STR['PHYSIQUE']['nom_nreac'].v.append( STR['REACTION'][i]['nom_reac'].v )

    STR['PHYSIQUE']['nom_ndom'].v = []
    for i in range( STR['PHYSIQUE']['ndom'].v ):
        STR['PHYSIQUE']['nom_ndom'].v.append( STR['INIT'][i]['nom_dom'].v )

    STR['PHYSIQUE']['nom_nlim'].v = []
    for i in range( STR['PHYSIQUE']['nlim'].v ):
        STR['PHYSIQUE']['nom_nlim'].v.append( STR['LIM'][i]['nom_lim'].v )
        
        
#----------------------------------------------------------------------#
        
# Remplis la structure PRM avec le paramètre de la ligne courante
def fullfill(STR,dict_name,iel,argname,val_str):
    if dict_name == 'ESPECE':
        if iel > len( STR['ESPECE'] ):
            mdtc_str.add_espece( STR['ESPECE'] )
    if dict_name == 'REACTION':
        if iel > len( STR['REACTION'] ):
            mdtc_str.add_reaction( STR['REACTION'] )
    if dict_name == 'INIT':
        if iel > len( STR['INIT'] ):
            mdtc_str.add_cond_ini( STR['INIT'] )
    if dict_name == 'LIM':
        if iel > len( STR['LIM'] ):
            mdtc_str.add_limite( STR['LIM'] )
        
    # Appel a set_val : remplissage au bon format
    if iel==0:
        set_val(STR[dict_name][argname],val_str)

    elif iel > 0:
        set_val(STR[dict_name][iel-1][argname],val_str)


# remplissage de structure au bon format
def set_val(DICT,val_str):
    sp = shlex.split( val_str )
    n = len( sp )
    DICT.n = n
    
    # Cas special : liste à répétition multiple (var_exp, isend_coupling, irecv_coupling, coord_sensors)
    if DICT.i in ('var_exp', 'isend_coupling', 'irecv_coupling','coord_sensors'):
        list_expt = []
        for ip in sp:
            tmp = read_val( DICT.t , ip )
            list_expt.append( tmp )
        (DICT.v).append( list_expt )
        
    # Cas de liste (plusieurs éléments type qqe)
    elif type(DICT.v)==list:
        #DICT.v = []
        iiter = 0
        for ip in sp:
            #tmp = read_val( DICT.t , ip )
            #(DICT.v)[ip].append( tmp )
            (DICT.v)[iiter] = read_val( DICT.t , ip )
            iiter += 1
    
    # Cas standard 1 element
    else:
        DICT.v = read_val( DICT.t , val_str )

    
# Fonction : Lecture de val au format choisi
def read_val(typ,val):
    if typ == 'i':
        val = int(val)
    elif typ == 'f':
        val = float(val)
    elif typ == 's':
        val = val.replace("'","")
    return val
        

# Recherche une en-tete dans ligne_prm : ! ---> PARAM GENERAUX ---!
def getposition(ligne_prm,dict_name,iel):
    try:
        # Si le premier caractere est un "!"
        if (ligne_prm[0] == '!'):
            ligne_prm = ligne_prm.replace('-','')
            ligne_prm = ligne_prm.replace('!','')
            ligne_prm = ligne_prm.strip()
            
            if '> PARAM' in ligne_prm:
                ligne_split = ligne_prm.split()
                dict_file = ligne_split[-1]
            
                DICT_POS = { 'GENERAUX':'GENERAUX',
                             'PHYSIQUE':'PHYSIQUE',
                             'ESPECES':'ESPECE',
                             'REACTIONS':'REACTION',
                             'NUMERIQUE':'NUMERIQUE',
                             'INITIALES':'INIT',
                             'LIMITES':'LIM',
                             'SUIVI':'SUIVI',
                             'EXPORT':'EXPORT'}
                             
                if dict_file in DICT_POS.keys():
                    dict_name = DICT_POS[dict_file]
                    iel = 0
            
            if 'no' in ligne_prm:
                ligne_split = ligne_prm.split()
                iel = int( ligne_split[-1] )

    # Si erreur, on passe à la suite
    except:
        pass
    # Renvoi le nom du dictionnaire ou le numero d'élément
    return dict_name,iel


# Recherche de paramètre dans ligne_prm
def getargument(ligne_prm,argname,val_str,newarg):
    if '=' in ligne_prm:
        
        ligne_split = ligne_prm.split('=')              # On split suivant les espaces
        
        # Si 2eme mot est un égal, c'est un paramètre
        if (len(ligne_split) >= 2):
            argname = ligne_split[0].strip()    # Nom du parametre
            
            ligne_split = ligne_split[1].split('!')
            val_str = ligne_split[0].strip()          # Valeur du parametre sous forme str
            newarg = True

    # Retourne param sous forme de string
    return val_str,argname,newarg


# Ecrit dans FILE le key de la structure STR
def write_row(FILE,key,STR,indent=0):
    # Si valeur par defaut n<0 ce parametre ne doit pas etre ecrit dans le fichier
    if STR[key].n < 0: return
    
    if type(STR[key].v) == list:
        val = ''
        for i in range(STR[key].n):
            val += str(format_val(STR[key].t,STR[key].v[i]))+' '

    elif STR[key].n == 1:
        val = format_val(STR[key].t,STR[key].v)

    else:
        val = STR[key].v
    
    ligne_prm = u' '*indent*4
    ligne_prm += u"%s = %s" % (str(STR[key].i),val)
    ligne_prm += u' '*(71-len(ligne_prm))+u'! '+unicode(STR[key].d)+u'\n'
    
    FILE.write(unicode(ligne_prm))


# Ecrit dans FILE le key de la structure STR : Cas spécial pour triplet (VAR ndim NAME)
def write_triplet(FILE,STR,indent=0):
    
    for j in range(len(STR.v)):
        ligne_prm = u' '*indent*4
        ligne_prm += u"%s = '%s'" % (str(STR.i),str((STR.v[j])[0]))
        ligne_prm += u' '*(len(str(STR.i))+indent*4+20-len(ligne_prm))
        ligne_prm += unicode((STR.v[j])[1])
        ligne_prm += u' '*(len(str(STR.i))+indent*4+25-len(ligne_prm))
        ligne_prm += u"'%s'" % str((STR.v[j])[2])
        ligne_prm += u' '*(71-len(ligne_prm))+u'!\n'
        
        FILE.write(ligne_prm)

# Ecrit dans FILE le key de la structure STR : Cas spécial pour triplet de sensors(VAR ndim NAME)
def write_triplet_sensors(FILE,STR,indent=0):
    
    for j in range(len(STR.v)):
        ligne_prm = u' '*indent*4
        ligne_prm += u"%s = " % str(STR.i)
        ligne_prm += unicode((STR.v[j])[0])
        ligne_prm += u' '*(len(str(STR.i))+indent*4+15-len(ligne_prm))
        ligne_prm += unicode((STR.v[j])[1])
        ligne_prm += u' '*(len(str(STR.i))+indent*4+25-len(ligne_prm))
        ligne_prm += unicode((STR.v[j])[2])
        ligne_prm += u' '*(71-len(ligne_prm))+u'!\n'
        
        FILE.write(ligne_prm)    
    
# Fonction : Lecture de val au format choisi
def read_val(typ,val):
    if typ == 'i':
        val = int(val)
    elif typ == 'f':
        val = float(val)
    elif typ == 's':
        val = val.replace("'","")
    return val
        
    
# Fonction : Ecriture de val au format choisi
def format_val(typ,val):
    if typ == 'i':
        val = int(val)
    elif typ == 'f':
        val = float(val)
    elif typ == 's':
        val = "'"+str(val)+"'"
    return val

#----------------------------------------------------------------------#

# Ecriture d'en-tete principale
def entete_main(FILE):
    FILE.write(u'!----------------------------------------------------------------------!\n')
    FILE.write(u'!-----------FICHIER DE PARAMETRES POUR EXECUTION DE MoDeTheC-----------!\n')
    FILE.write(u'!----------------------------------------------------------------------!\n')

# Ecriture d'en-tete de section
def entete_section(FILE,section):
    FILE.write(u'!\n')
    FILE.write(u'!----------------------------------------------------------------------!\n')
    s = '! --> PARAM '+section+' '
    s += '-'*(71-len(s)) + '!'
    FILE.write('%s\n' % unicode(s))

# Ecriture d'en-tete de sous-section
def entete_soussec(FILE,section,isec):
    FILE.write(u'!\n')
    s = '! --> '+section+' no '+str(isec+1)+' '
    s += '-'*(71-len(s)) + '!'
    FILE.write('%s\n' % unicode(s) )

# Ecriture de fin de fichier
def entete_end(FILE):
    FILE.write(u'!----------------------------------------------------------------------!\n')
    FILE.write(u'! <-- END\n')
   
#----------------------------------------------------------------------#

# Test de comportement
if __name__=='__main__':
    pass
    #save_str(STR,'test')
    #open_str(STR,'test')
    #save_str(STR,'test2')

