#! /usr/bin/env python
#­* ­coding: utf­8 ­*
#----------------------------------------------------------------------#
# Programme de structure des paramètres MODETHEC
#
# Débuté le 04/02/2015
# Auteur : Valentin Biasi
#

# Dictionnaires ordonnés
from collections import OrderedDict

version = u"2018.2.0"

# Liste des variables internes modethec a exporter -> Tableau var_exp
list_var_exp = [ 'VOLUME', 'T', 'grad_T', 'P', 'grad_P', \
                'Y', 'Y_g', 'Y_s', 'phi', 'phi_g', 'phi_s', \
                'rho', 'rho_e', 'rho_g', 'v_g', 'w_e', 'w_g', \
                'wQ', 'alpha_reac', 'E', 'dE', 'm', 'm_e', \
                'dm_e', 'k', 'Cp', 'h', 'Kp', \
                'G_XYZ', 'U', 'eps_strain', 'sig_stress', 'r_courbure' ]

# Liste des variables internes modethec pour couplage -> Tableau isend_coupling et irecv_coupling
list_var_coupling = [ 'Fimp', 'Timp', 'T_conv', 'h_conv', 'T_rad', \
                'Dimp', 'Pimp' ]

# Booleen de gestion du besoin d'enregistrement
need_to_save = False

# Classe prm : Définit la structure de données des paramètres
class prm(object):

    # A l'initialisation
    def __init__(self, item, val, typ, nombre, doc):
        self.i = item
        self.v = val
        self.t = typ
        self.n = nombre
        self.d = doc
        self.w = None

    def __str__(self):
        return "Item name: {}, Value: {}, Type: {}, Length: {}".format(self.i, self.v, self.t, self.n)


def init_generaux(STR):
    STR['version'] = prm('version','','s',1,u'MoDeTheC version')
    STR['auteur'] = prm('auteur','Author','s',1,u'Author(s)')
    STR['file_mesh'] = prm('file_mesh','','s',1,u'Mesh filename')
    STR['dim_simu'] = prm('dim_simu','2D_PLAN','s',1,u'Simulation dimension (3D, 2D_PLANAR, 2D_AXI, 1D)')
    STR['AXE_2D'] = prm('AXE_2D',[0.,0.,0.,0.,0.,0.],'f',6,u'Symmetry axis: origin [x0 y0 z0], unitary orientation vector [ux uy uz]')

    STR['DIFFUSION'] = prm('DIFFUSION','on','s',1,u'Activate heat diffusion solver')
    STR['ADVECTION'] = prm('ADVECTION','off','s',1,u'Activate gas advection solver')
    STR['REACTION'] = prm('REACTION','off','s',1,u'Activate reaction kinetics solver')
    STR['STRUCTURE'] = prm('STRUCTURE','off','s',1,u'Activate static mechanics solver')
    STR['ABLATION'] = prm('ABLATION','off','s',1,u'Activate ALE ablation solver')
    STR['COUPLAGE'] = prm('COUPLAGE','off','s',1,'Activate external coupling solver')


def init_physique(STR):
    STR['ndom'] = prm('ndom',1,'i',1,u'Total number of physical domains')
    STR['nom_ndom'] = prm('nom_ndom',['DOM_1'],'s',-1,'')
    
    STR['nlim'] = prm('nlim',1,'i',1,u'Total number of boundary conditions')
    STR['nom_nlim'] = prm('nom_nlim',['BOUND_1'],'s',-1,'')

    STR['nesp'] = prm('nesp',1,'i',1,u'Number of solid and gas species')
    STR['nom_nesp'] = prm('nom_nesp',['SPE_1'],'s',-1,'')

    STR['ngaz'] = prm('ngaz',0,'i',1,u'Number of gas species')
    STR['nsol'] = prm('nsol',0,'i',1,u'Number of solid species')
    
    STR['ID_gaz'] = prm('ID_gaz',[0,0,0,0,0,0],'i',0,u'Gas species ID')
    STR['ID_sol'] = prm('ID_sol',[0,0,0,0,0,0],'i',0,u'Solid species ID')

    STR['nreac'] = prm('nreac',0,'i',1,u'Number of reactions')
    STR['nom_nreac'] = prm('nom_nreac',['REAC_1'],'s',-1,'')

    STR['atmo'] = prm('atmo','vide','s',1,u'Surrounding atmosphere type (air, vacuum, O2, N2)')

    STR['satur_T'] = prm('satur_T',0,'i',1,u'Activate properties saturation above temperature threshold "T_sat"')
    STR['T_sat'] = prm('T_sat',0.0,'f',1,u'Temperature threshold for properties saturation [K]')

    STR['k_homog'] = prm('k_homog','melange','s',1,u'Homogenization method for thermal conductivity tensor (law of mixture, Mori-Tanaka)')

    STR['Kp_typ'] = prm('Kp_typ','constant','s',1,u'Homogenization method for permeability tensor (constant, Henderson, Kozeny, Kozeny-ortho)')
    STR['Kp_ini'] = prm('Kp_ini',0.0,'f',1,u'Isotropic permeability value of the medium (constant or Henderson initial) [m2]')
    STR['Kp_fin'] = prm('Kp_fin',0.0,'f',1,u'Isotropic permeability value of the medium (Henderson final) [m2]')
    STR['Kp_0'] = prm('Kp_0',0.0,'f',1,u'Permeability value of the medium (Kozeny-Karman isotropic model) [m2]')
    STR['Kp_x'] = prm('Kp_x',0.0,'f',1,u'Permeability value of the medium along X-direction (Kozeny-Karman orthotropic model) [m2]')
    STR['Kp_y'] = prm('Kp_y',0.0,'f',1,u'Permeability value of the medium along Y-direction (Kozeny-Karman orthotropic model) [m2]')
    STR['Kp_z'] = prm('Kp_z',0.0,'f',1,u'Permeability value of the medium along Z-direction (Kozeny-Karman orthotropic 3D model) [m2]')
    STR['Kp_max'] = prm('Kp_max',0.0,'f',1,u'Permeability value of the medium (maximum threshold value) [m2]')

    STR['mu_typ'] = prm('mu_typ','constant','s',1,u'Gas phase dynamic viscosity model')
    STR['mu_cst'] = prm('mu_cst',0.0,'f',1,u'Gas phase dynamic viscosity constant value (no temperature-dependency assumption) [Pa.s]')
    STR['mu_nb_poly'] = prm('mu_nb_poly',2,'i',1,u'Number of polynomial coefficients for gas phase dynamic viscosity expression as a function of temperature')
    STR['mu_poly'] = prm('mu_poly',[0.,0.,0.,0.,0.,0.],'f',2,u'Polynomial coefficients values for gas phase dynamic viscosity expression as a function of temperature')

    STR['T_abla'] = prm('T_abla',0.0,'f',1,u'Fusion temperature for ablation model [K]')
    STR['h_abla'] = prm('h_abla',0.0,'f',1,u'Fusion mass enthalpy for ablation model [J/kg]')

    STR['meca_typ'] = prm('meca_typ','isotrope','s',1,u'Mechanical properties model (isotropic, transverse)')
    STR['E_iso'] = prm('E_iso',0.0,'f',1,u'Isotropic Young Modulus [Pa]')
    STR['P_iso'] = prm('P_iso',0.0,'f',1,u'Isotropic Poisson coefficient')
    STR['beta_iso'] = prm('beta_iso',0.0,'f',1,u'Isotropic thermal expansion coefficient [1/K]')
    STR['E_x'] = prm('E_x',0.0,'f',1,u'Young Modulus along X-direction (orthotropic model) [Pa]')
    STR['E_y'] = prm('E_y',0.0,'f',1,u'Young Modulus along Y-direction (orthotropic model) [Pa]')
    STR['P_xy'] = prm('P_xy',0.0,'f',1,u'Planar XY Poisson coefficient (orthotropic model)')
    STR['G_xy'] = prm('G_xy',0.0,'f',1,u'Transverse Z shear modulus (orthotropic model)')
    STR['beta_x'] = prm('beta_x',0.0,'f',1,u'Longitudinal thermal expansion coefficient along X-direction [1/K]')
    STR['beta_y'] = prm('beta_y',0.0,'f',1,u'Transverse thermal expansion coefficient along Y-direction [1/K]')



def init_espece(STR):
    STR['id_esp'] = prm('id_esp',1,'i',1,u'Species ID')
    STR['nom_esp'] = prm('nom_esp','SPE_X','s',1,u'Species name')
    STR['typ_esp'] = prm('typ_esp','solide','s',1,u'Species phase (solid or gas)')

    STR['rho_typ'] = prm('rho_typ','constant','s',1,u'Species density model')
    STR['rho_cst'] = prm('rho_cst',0.0,'f',1,u'Species constant density value (no temperature-dependency assumption) [kg/m3]')
    STR['rho_nb_poly'] = prm('rho_nb_poly',2,'i',1,u'Number of polynomial coefficients for species density expression as a function of temperature')
    STR['rho_poly'] = prm('rho_poly',[0.,0.,0.,0.,0.,0.],'f',2,u'Polynomial coefficients values for species density expression as a function of temperature')

    STR['Cp_typ'] = prm('Cp_typ','constant','s',1,u'Species specific heat model')
    STR['Cp_cst'] = prm('Cp_cst',0.0,'f',1,u'Species constant specific heat (no temperature-dependency assumption) [J/kg/K]')
    STR['Cp_nb_poly'] = prm('Cp_nb_poly',2,'i',1,u'Number of polynomial coefficients for specific heat expression as a function of temperature')
    STR['Cp_poly'] = prm('Cp_poly',[0.,0.,0.,0.,0.,0.],'f',2,u'Polynomial coefficients values for specific heat expression as a function of temperature')

    STR['k_typ'] = prm('k_typ','constant','s',1,u'Species thermal conductivity tensor model')
    STR['k_cst'] = prm('k_cst',0.0,'f',1,u'Species constant thermal conductivity (isotropic + no temperature-dependency assumptions) [W/m/K]')
    STR['k_nb_poly'] = prm('k_nb_poly',2,'i',1,u'Number of polynomial coefficients for species thermal conductivity expression as a function of temperature (isotropic assumption)')
    STR['k_poly'] = prm('k_poly',[0.,0.,0.,0.,0.,0.],'f',2,u'Polynomial coefficients values for species thermal conductivity expression as a function of temperature (isotropic assumption)')
    STR['k_nb_otho'] = prm('k_nb_otho',2,'i',3,u'Number of polynomial coefficients for species thermal conductivity tensor expression as a function of temperature (orthotropic model)')
    STR['k_ortho_x'] = prm('k_ortho_x',[0.,0.,0.,0.,0.,0.],'f',2,u'Polynomial coefficients values for species thermal conductivity tensor expression as a function of temperature along X-direction (orthotropic model)')
    STR['k_ortho_y'] = prm('k_ortho_y',[0.,0.,0.,0.,0.,0.],'f',2,u'Polynomial coefficients values for species thermal conductivity tensor expression as a function of temperature along Y-direction (orthotropic model)')
    STR['k_ortho_z'] = prm('k_ortho_z',[0.,0.,0.,0.,0.,0.],'f',2,u'Polynomial coefficients values for species thermal conductivity tensor expression as a function of temperature along Z-direction (orthotropic model)')
    STR['k_Eshelby'] = prm('k_Eshelby',[0.,0.,0.],'f',3,u'Diagonal coefficients of Eshelby tensor for ellipsoid inclusions')

    STR['M'] = prm('M',1.,'f',1,u'Gas species molar mass [kg/mol]')

    STR['eps'] = prm('eps',0.,'f',1,u'Surface emissivity (solid species)')
    STR['alpha'] = prm('alpha',0.,'f',1,u'Surface absorptivity (solid species)')

def add_espece(STR):
    ESP = OrderedDict()
    init_espece( ESP )
    STR.append( ESP )

def init_reaction(STR):
    STR['id_reac'] = prm('id_reac',1,'i',1,u'Reaction ID')
    STR['nom_reac'] = prm('nom_reac','REAC_X','s',1,u'Reaction name')

    STR['id_R'] = prm('id_R',0,'i',1,u'Solid reactant species ID')
    STR['id_P'] = prm('id_P',0,'i',1,u'Solid product species ID (0 if no solid product)')
    STR['id_O2'] = prm('id_O2',0,'i',1,u'Checked if oxidative reaction (1 if oxidative reaction - 0 otherwise)')
    STR['ngazP'] = prm('ngazP',0,'i',1,u'Number of gas phase products')
    STR['ID_gazP'] = prm('ID_gazP',[0,0,0,0,0,0],'i',0,u'Species ID of gas phase products')

    STR['nu_R'] = prm('nu_R',0.0,'f',1,u'Stoichiometric mass coefficients of solid reactant species')
    STR['nu_P'] = prm('nu_P',0.0,'f',1,u'Stoichiometric mass coefficients of solid product species')
    STR['nu_gazP'] = prm('nu_gazP',[0.,0.,0.,0.,0.,0.],'f',0,u'Stoichiometric mass coefficients of gas phase product species')
    STR['Q'] = prm('Q',0.0,'f',1,u'Heat of reaction [J/kg]')
    STR['n'] = prm('n',0.0,'f',1,u'Arrhenius kinectics reaction order [-]')
    STR['A'] = prm('A',0.0,'f',1,u'Arrhenius kinectics pre-exponential coefficient [1/s]')
    STR['E_A'] = prm('E_A',0.0,'f',1,u'Arrhenius kinectics activation energy [J/mol]')

def add_reaction(STR):
    REAC = OrderedDict()
    init_reaction( REAC )
    STR.append( REAC )

def init_numerique(STR):
    STR['t0'] = prm('t0',0.0,'f',1,u'Simulation initial time [s]')
    STR['tf'] = prm('tf',0.0,'f',1,u'Simulation final time [s]')
    STR['dt'] = prm('dt',0.0,'f',1,u'Simulation global time step [s]')

    STR['schema_A'] = prm('schema_A','theta-lin','s',1,u'Time integration numerical method for advection operator solver')
    STR['schema_DR'] = prm('schema_DR','theta-imp','s',1,u'Time integration numerical method for diffusion/reaction operator solver')
    STR['schema_S'] = prm('schema_S','implicite','s',1,u'Integration numerical method for static mechanics operator solver')
    STR['solveur_LIN'] = prm('solveur_LIN','PARDISO','s',1,u'Space integration linear solver (PARDISO, GMRES)')

    STR['theta_imp'] = prm('theta_imp',1.0,'f',1,u'Theta value for theta-implicit (linearized) method')
    STR['delta_max'] = prm('delta_max',1.0,'f',1,u'Variation limit of conservative variables for each iteration')
    STR['CFL_max'] = prm('CFL_max',1.0,'f',1,u'Maximum CFL criterion for advection operator solver (linearized theta-implicit scheme)')
    STR['slope_limiter'] = prm('slope_limiter','van_leer','s',1,u'Flux limiter type for advection fluxes - TVD scheme (upwind, downwind, central, sou, minmod, superbee, van_leer, van_albada, ospre)')
    STR['tol_abs'] = prm('tol_abs',1e-6,'f',1,u'Absolute convergence criterion for A+D/R operators solvers')
    STR['tol_rel'] = prm('tol_rel',1e-6,'f',1,u'Relative convergence criterion for A+D/R operators solvers')
    STR['tol_int'] = prm('tol_int',0.1,'f',1,u'Internal iteration convergence criterion for linear solver')
    STR['niter_max'] = prm('niter_max',50,'i',1,u'Maximum number of non-linear iterations for each time step')
    STR['niter_max_int'] = prm('niter_max_int',10,'i',1,u'Maximum number of linear iterations for each time step')
    
    STR['CFL_AB'] = prm('CFL_AB',1.0,'f',1,u'Maximum CFL criterion for ALE ablation solver (mesh deformation)')
    STR['pas_ech'] = prm('pas_ech',10,'i',1,u'Sub-sampling maximum point number for ablation boundaries')
    STR['niter_def'] = prm('niter_def',20,'i',1,u'Mesh displacement iteration number for ablation solver (Laplacian operator)')
    STR['tol_def'] = prm('tol_def',0.001,'f',1,u'Mesh displacement iteration convergence criterion for ablation solver')
    STR['nvois_def'] = prm('nvois_def',1.0,'i',1,u'Number of neighbors for ablation smoothing')
    
    STR['dt_couplage'] = prm('dt_couplage',0.0,'f',1,u'Coupling time step [s]')

    STR['advanced_num_prm'] = prm('advanced_num_prm',0,'i',1,u'Activate advanced numerical parameters (in the GUI)')

    STR['restart_backup'] = prm('restart_backup',0,'i',1,'Restart from last backup computation')

def init_cond_ini(STR):
    STR['id_dom'] = prm('id_dom',1,'i',1,u'Physical domain ID')
    STR['nom_dom'] = prm('nom_dom','DOM_X','s',1,u'Physical domain name (please respect mesh domain marks)')

    STR['Tini'] = prm('Tini',293.0,'f',1,u'Domain initial temperature [K]')
    STR['Pini'] = prm('Pini',101325.0,'f',1,u'Domain initial pressure [Pa]')
    STR['phi_ini'] = prm('phi_ini',[0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],'f',1,u'Initial species mass fractions (please respect ID order)')

def add_cond_ini(STR):
    INIT = OrderedDict()
    init_cond_ini( INIT )
    STR.append( INIT )

def init_limite(STR):
    STR['id_lim'] = prm('id_lim',1,'i',1,u'Boundary condition ID')
    STR['nom_lim'] = prm('nom_lim','BOUND_X','s',1,u'Boundary condition name (please respect mesh boundary marks)')

    STR['ener_typ'] = prm('ener_typ','temp','s',1,u'Thermal boundary condition type (heat flux, temperature, combined HF+conv+rad)')
    STR['Fimp'] = prm('Fimp',0.0,'f',1,u'Imposed heat flux (for heat flux or combined HF+conv+rad BC types) [W/m2]')
    STR['Timp'] = prm('Timp',293.0,'f',1,u'Imposed temperature (for temperature BC type) [K]')
    STR['T_conv'] = prm('T_conv',293.0,'f',1,u'Convective heat transfer temperature (for combined HF+conv+rad BC type) [K]')
    STR['h_conv'] = prm('h_conv',0.0,'f',1,u'Convective heat transfer coefficient (for combined HF+conv+rad BC type) [W/m2/K]')
    STR['T_rad'] = prm('T_rad',293.0,'f',1,u'Radiative heat transfer temperature (for combined HF+conv+rad BC type) [K]')

    STR['masse_typ'] = prm('masse_typ','debit','s',1,u'Mass flow boundary condition type (external pressure, surface mass flow rate)')
    STR['Dimp'] = prm('Dimp',0.0,'f',1,u'Imposed surface mass flow rate [kg/m2/s]')
    STR['Pimp'] = prm('Pimp',101325.0,'f',1,u'Imposed external pressure [Pa]')

    STR['struct_typ'] = prm('struct_typ','contrainte','s',1,u'Mechanical boundary type (constrained, stress, symmetry axis)')
    STR['stress_imp'] = prm('stress_imp',[0.,0.],'f',2,u'Imposed normal and tangential mechanical stress [Pa]')

    STR['abla_typ'] = prm('abla_typ','temp','s',1,u'Ablation boundary type (fusion temperature, ablation velocity)')
    STR['Vreg_imp'] = prm('Vreg_imp',0.0,'f',1,u'Ablation imposed velocity [m/s]')

    STR['use_cls'] = prm('use_cls',0,'i',1,u'Activate user-specified boundary condition [ON(1) - OFF(0)]')
    STR['file_cls'] = prm('file_cls','','s',1,u'User-specified boundary condition file')

    STR['couplage_lim'] = prm('couplage_lim',0,'i',1,u'Boundary external coupling activation (ON[1] - OFF[0])')
    STR['application_couplage'] = prm('application_couplage','','s',1,u'Application name (or domain name) to be coupled via CWIPI (mesh filename for modethec/CWIPI/modethec coupling)')
    STR['nvar_isend_coupling'] = prm('nvar_isend_coupling',0,'i',1,u'Number of variables to be sent to external coupling')
    STR['isend_coupling'] = prm('isend_coupling',[],'s',-1,u'List of variables to be sent [Internal variable name | Dimension index | Coupling name] (warning: internal modethec variables only)')
    STR['nvar_irecv_coupling'] = prm('nvar_irecv_coupling',0,'i',1,u'Number of variables to be received from external coupling')
    STR['irecv_coupling'] = prm('irecv_coupling',[],'s',-1,u'List of variables to be received [Internal variable name | Dimension index | Coupling name] (warning: internal modethec variables only)')

def add_limite(STR):
    LIM = OrderedDict()
    init_limite( LIM )
    STR.append( LIM )

def init_suivi(STR):
    STR['suivi'] = prm('suivi',1,'i',1,u'Activate terminal history [ON(1) - OFF(0)]')
    STR['file_log'] = prm('file_log',1,'i',1,u'Record log file [ON(1) - OFF(0)]')
    STR['dt_prt'] = prm('dt_prt',0.1,'f',1,u'Time step of on-screen prints (multiple of global time step)')
    STR['file_hdf5'] = prm('file_hdf5',0,'i',1,u'Export integrated variables [HDF5 file format]')
    STR['dt_hdf5'] = prm('dt_hdf5',0.1,'f',1,u'Time step of HDF5 export history')
    STR['sensors'] = prm('sensors',0,'i',1,'Export variables at specific coordinates [ON(1) - OFF(0)]')
    STR['nsensors'] = prm('nsensors',0,'i',1,'Number of sensors for specific outputs')
    STR['coord_sensors'] = prm('coord_sensors',[],'f',-1,'List of sensor coordinates to be extracted [X | Y | Z]')

    STR['backup'] = prm('backup',0,'i',1,'Activate backup output')
    STR['dt_backup'] = prm('dt_backup',0.0,'f',1,'Time step between two backup outputs [s]')

def init_export(STR):
    STR['export'] = prm('export',0,'i',1,u'Save export file [ON(1) - OFF(0)]')
    STR['nom_exp'] = prm('nom_exp','out_modethec','s',1,u'Export filename')
    STR['dt_exp'] = prm('dt_exp',0.1,'f',1,u'Time step of export file (multiple of global time step)')
    STR['exp_ini'] = prm('exp_ini',1,'i',1,u'Export initial state t=0 [ON(1) - OFF(0)]')
    STR['deform_mlg'] = prm('deform_mlg',0,'i',1,u'Mesh deformation for mechanical solver [ON(1) - OFF(0)]')
    STR['deform_amp'] = prm('deform_amp',1.0,'f',1,u'Mesh deformation magnitude factor')
    STR['FEmode'] = prm('FEmode',1,'i',1,u'Finite Elements Tecplot mode [ON(1):Cell connectivity - OFF(0):Scatter mode]')

    STR['tecplot_bin'] = prm('tecplot_bin',0,'i',1,u'Generate Tecplot binary file [ON(1) - OFF(0)]')
    STR['tecplot_ascii'] = prm('tecplot_ascii',0,'i',1,u'Generate Tecplot ASCII file [ON(1) - OFF(0)]')
    STR['vtk_ascii'] = prm('vtk_ascii',0,'i',1,u'Generate VTK ASCII file [ON(1) - OFF(0)]')
    STR['sommet'] = prm('sommet',0,'i',1,u'Export node-centered variables')
    STR['cellule'] = prm('cellule',1,'i',1,u'Export cell-centered variables')
    STR['volume'] = prm('volume',0,'i',1,u'Export volume results')
    STR['surface'] = prm('surface',0,'i',1,u'Export surface results')

    STR['nexpt'] = prm('nexpt',0,'i',1,u'Number of variables to be exported')
    STR['var_exp'] = prm('var_exp',[],'s',-1,u'List of variables to be exported [Internal variable name | Dimension index | Export name]')


#----------------------------------------------------------------------#

def init_STR():
    STR = OrderedDict()

    STR['GENERAUX'] = OrderedDict()
    init_generaux(STR['GENERAUX'])

    STR['PHYSIQUE'] = OrderedDict()
    init_physique(STR['PHYSIQUE'])

    STR['ESPECE'] = list()
    add_espece( STR['ESPECE'] )

    STR['REACTION'] = list()
    add_reaction( STR['REACTION'] )

    STR['NUMERIQUE'] = OrderedDict()
    init_numerique(STR['NUMERIQUE'])

    STR['INIT'] = list()
    add_cond_ini( STR['INIT'] )

    STR['LIM'] = list()
    add_limite( STR['LIM'] )

    STR['SUIVI'] = OrderedDict()
    init_suivi(STR['SUIVI'])

    STR['EXPORT'] = OrderedDict()
    init_export(STR['EXPORT'])

    return STR

#----------------------------------------------------------------------#
