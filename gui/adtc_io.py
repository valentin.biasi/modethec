#! /usr/bin/env python
#­* ­coding: utf­8 ­*
#----------------------------------------------------------------------#
# Programme de lecture des bases de donnees pour analyse TGA/DSC
#
# Débuté le 24/10/2016
# Auteur : Valentin Biasi
#
# Structure en base de donnees DB constituee en liste
# Classe : DB[0] <=> Exp n 0
# avec DB[0].ID
#      DB[0].filename
#      DB[0].date
#      DB[0].time
#
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from xlrd import open_workbook
import xlrd
import datetime
import re
import os
import numpy as np
import h5py
import inspect
#
# Gestion des UTF8 en Qt
try:
    _fromUtf8 = QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s
#
# Test de string convertible en integer
def IsInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False


#----------------------------------------------------------------------#
# Classe Prm : Définit la classe resumant les parametres
class Prm(object):

    # A l'initialisation
    def __init__(self):
        self.n_interp = 1000                # Nombre de points d'interpolations
        self.Tmin=50.0                      # Temperature min d'interpolation
        self.Tmax=1000.0                    # Temperature max d'interpolation
        
        self.automatic_colors=True          # Couleurs de plot automatique
        self.predefined_colors=False        # Couleurs de plot predefinies
        self.predefined_2K='orangered'              # Couleur de plot a 2K/min
        self.predefined_5K='forestgreen'             # Couleur de plot a 5K/min
        self.predefined_10K='dodgerblue'             # Couleur de plot a 10K/min
        self.predefined_20K='darkviolet'             # Couleur de plot a 20K/min
        self.predefined_50K='orchid'             # Couleur de plot a 50K/min
        self.predefined_100K='grey'            # Couleur de plot a 100K/min
    
#----------------------------------------------------------------------#
# Classe Exp : Définit UNE analyse de degradation
class Exp(object):

    # A l'initialisation
    def __init__(self, ID, date, time, operateur, etude, materiau, strat, \
        position, mass, crucible, method, atmo, heat_velocity, gas_rate, \
        blank_id, archimede, validity, export, comments):

        self.checked = False                # Activation par checkbox du qtable
        self.ID = ID                        # Identifiant courbe
        self.filename = ''                  # Nom du fichier .txt
        self.filename_blank = ''            # Nom du fichier ligne de base .txt

        self.Ts=[]                          # Valeurs enregistrees a partir de "filename"
        self.t=[]
        self.HF=[]
        self.m=[]
        self.Tr=[]

        self.Ts_blank=[]                    # Valeurs enregistrees a partir de "filename_blank"
        self.t_blank=[]
        self.HF_blank=[]
        self.m_blank=[]
        self.Tr_blank=[]
        
        self.Y_fit=[]                       # Resultat de fitting TGA pour une analyse donnee
        self.dYdT_fit = []
        self.chi2 = 0.0

        self.date = date                    # Valeurs enregistrees a partir du fichier excel
        self.time = time
        self.operateur = operateur
        self.etude = etude
        self.materiau = materiau
        self.strat = strat
        try:
            self.position = int(position)
        except:
            self.position = 0
        self.mass = mass
        self.crucible = crucible
        self.method = method
        self.atmo = atmo
        try:
            self.heat_velocity = int(heat_velocity)
        except:
            self.heat_velocity = 0
        try:
            self.gas_rate = int(gas_rate)
        except:
            self.gas_rate = 0
        try:
            self.blank_id = int(blank_id)
        except:
            self.blank_id = 0
            
        self.archimede = archimede
        self.validity = validity
        self.export = export
        self.comments = comments


#----------------------------------------------------------------------#
# Lecture d'une base de donnees
def read_db( DB, database_widget, directory, filename ):

    # Ouverture de la feuille 'Cahier de manip'
    wb = xlrd.open_workbook( directory+ '/'+ filename )
    sh = wb.sheet_by_name('Cahier de manip')
    nb_rows = sh.nrows - 5
    nb_columns = sh.ncols

    # Taille de tableau ajustee
    database_widget.setRowCount(nb_rows)

    # Pour chaque ligne du excel, on ajoute une classe Exp à DB
    for row in range(5,nb_rows+5):

        #  Creation d'une variable de classe Exp pour chaque ligne du excel
        value  = Exp( row+1, \
            date=sh.cell(row,0).value, \
            time=sh.cell(row,1).value, \
            operateur=sh.cell(row,2).value, \
            etude=sh.cell(row,3).value, \
            materiau=sh.cell(row,4).value, \
            strat=sh.cell(row,5).value, \
            position=sh.cell(row,6).value, \
            mass=sh.cell(row,7).value, \
            crucible=sh.cell(row,8).value, \
            method=sh.cell(row,10).value, \
            atmo=sh.cell(row,11).value, \
            heat_velocity=sh.cell(row,12).value, \
            gas_rate=sh.cell(row,13).value, \
            blank_id=sh.cell(row,15).value, \
            archimede=sh.cell(row,16).value, \
            validity=sh.cell(row,17).value, \
            export=sh.cell(row,18).value, \
            comments=sh.cell(row,19).value )

        # Transformation des valeurs date et temps du format excel vers format lisible
        try:
            value.date =  xlrd.xldate_as_tuple(value.date, wb.datemode)
            value.date = str(value.date[2])+' / '+str(value.date[1])+' / '+str(value.date[0])
        except:
            pass
        try:
            value.time =  xlrd.xldate_as_tuple(value.time, wb.datemode)
            value.time = str(value.time[3])+' : '+str(value.time[4])
        except:
            pass

        # Ajout a DB de la variable de classe Exp
        DB.append( value )

    # Pour chaque Exp du Excel, on ecrit dans le tableau Qt
    for row in range(nb_rows):

        # Ajout d'une checkbox sur la colonne 0 avec les proprietes "Unchecked" + "UserCheckable"
        item_ID = QTableWidgetItem(str(DB[row].ID))
        item_ID.setFlags(Qt.ItemIsUserCheckable | Qt.ItemIsEnabled)
        item_ID.setCheckState(Qt.Unchecked)
        database_widget.setItem(row, 0, item_ID)
        
        database_widget.setItem(row, 1, QTableWidgetItem(_fromUtf8(DB[row].materiau)))
        database_widget.setItem(row, 2, QTableWidgetItem(str(DB[row].blank_id)))
        database_widget.setItem(row, 3, QTableWidgetItem(str(DB[row].mass)))
        database_widget.setItem(row, 4, QTableWidgetItem(_fromUtf8(DB[row].method)))
        database_widget.setItem(row, 5, QTableWidgetItem(str(DB[row].atmo)))
        database_widget.setItem(row, 6, QTableWidgetItem(str(DB[row].heat_velocity)))
        database_widget.setItem(row, 7, QTableWidgetItem(str(DB[row].gas_rate)))
        database_widget.setItem(row, 8, QTableWidgetItem(_fromUtf8(DB[row].strat)))
        database_widget.setItem(row, 9, QTableWidgetItem(str(DB[row].date)))
        database_widget.setItem(row, 10, QTableWidgetItem(str(DB[row].time)))
        database_widget.setItem(row, 11, QTableWidgetItem(_fromUtf8(DB[row].operateur)))
        database_widget.setItem(row, 12, QTableWidgetItem(str(DB[row].position)))
        database_widget.setItem(row, 13, QTableWidgetItem(_fromUtf8(DB[row].crucible)))
        database_widget.setItem(row, 14, QTableWidgetItem(str(DB[row].archimede)))
        #database_widget.setItem(row, 15, QTableWidgetItem(str(DB[row].validity)))
        #database_widget.setItem(row, 16, QTableWidgetItem(str(DB[row].export)))
        database_widget.setItem(row, 15, QTableWidgetItem(_fromUtf8(DB[row].comments)))



    # Connection du tableau Qt pour modifier DB en fonction des clics sur les checkbox
    database_widget.itemChanged.connect(lambda: handle_click_checkbox(database_widget, DB))
    
    # Connection du tableau Qt pour modifier DB en fonction des modifications de l'utilisateur
    database_widget.itemChanged.connect(lambda: handle_change_checkbox(database_widget, DB))
    
    # Autofit du tableau
    database_widget.resizeColumnsToContents()
    database_widget.horizontalHeader().setStretchLastSection(True)


#----------------------------------------------------------------------#
# Connecteur du tableau Qt pour modifier DB en fonction des clics sur les checkbox
def handle_click_checkbox( database_widget, DB):

    # Pour chaque ligne du tableau en colonne 0
    for row in range(len(DB)):

        # On controle l'etat checkbox et on modifie la propriete "checked" de DB
        if database_widget.item(row,0).checkState() == Qt.Checked:
            DB[row].checked = True
        else:
            DB[row].checked = False


#----------------------------------------------------------------------#
# Connection du tableau Qt pour modifier DB en fonction des modifications de l'utilisateur
def handle_change_checkbox( database_widget, DB):
    
    for row in range(len(DB)):
        DB[row].materiau = str(database_widget.item(row,1).text())
        try:
            DB[row].blank_id = int(database_widget.item(row,2).text())
        except:
            DB[row].blank_id = 0
        DB[row].mass = database_widget.item(row,3).text()
        DB[row].method = database_widget.item(row,4).text()
        DB[row].atmo = database_widget.item(row,5).text()
        try:
            DB[row].heat_velocity = int(database_widget.item(row,6).text())
        except:
            DB[row].heat_velocity = 0
        try:
            DB[row].gas_rate = int(database_widget.item(row,7).text())
        except:
            DB[row].gas_rate = 0
        DB[row].strat = database_widget.item(row,8).text()
        DB[row].date = database_widget.item(row,9).text()
        DB[row].time = database_widget.item(row,10).text()
        DB[row].operateur = database_widget.item(row,11).text()
        DB[row].position = database_widget.item(row,12).text()
        DB[row].crucible = database_widget.item(row,13).text()
        DB[row].archimede = database_widget.item(row,14).text()
        DB[row].comments = database_widget.item(row,15).text()



#----------------------------------------------------------------------#
# Recherche des noms de fichiers .txt en fonction des variables dans DB
def get_filename_data(DB, directory, statusbar=None):

    # Tous les fichiers dans directory
    list_dir = os.listdir(directory)

    # Pour chaque indice dans DB
    for x_db in DB:
        Id = -1 # Initialisation de l'indice de recherche dans list_dir

        if (x_db.checked == True):  # Seulement si ligne du tableau est cochée

            for l in list_dir:  # Pour toutes les fichiers du répertoire

                # Si fichier est de type .txt et commence par des décimales
                if (re.search('(.txt)',l) <> None) and (re.search('^\d+',l) <> None):

                    m = re.split('[-_.,]',l) # Split par séparateur de type (,._-)

                    # Conversion en integer
                    try:
                        Id = int(m[0])
                    except:
                        statusbar.showMessage('Oops : Curve file not found')

                    # Ecriture de filename si correspond à .ID
                    if Id == x_db.ID:
                        x_db.filename = l

                    # Ecriture de filename_blank si correspond à .blank_id
                    if IsInt(x_db.blank_id):
                        if Id == int(x_db.blank_id):
                            x_db.filename_blank = l

            # Renvoi erreur si fichier non trouve
            if (not x_db.filename) and (statusbar <> None):
                x_db.checked == False
                statusbar.showMessage('Oops : Curve file with ID '+str(x_db.ID)+' not found')
            if (not x_db.filename_blank) and statusbar <> None:
                statusbar.showMessage('Oops : No data for blank line with ID '+str(x_db.ID))


#----------------------------------------------------------------------#
# Lecture des fichiers .txt en fonction des variables dans DB
def read_tga_data(DB, directory, PRM, statusbar=None):

    # Pour chaque indice dans DB
    for x_db in DB:

        if (x_db.checked == True):  # Seulement si ligne du tableau est cochée

            if x_db.filename:   # Si filename trouve

                Ts=[]
                t=[]
                HF=[]
                m=[]
                Tr=[]

                # Ouverture du fichier
                data_file = open(directory + '/' + x_db.filename,'r+')

                # Lecture de chaque ligne du fichier
                for line in data_file:

                    line = line.strip()             # Trim de la ligne
                    line_split = line.split()       # Split de la ligne

                    if IsInt(line_split[0]):        # Si 1ere valeur est de type Integer (Spécifique format Metler-Toledo)
                        Ts.append(float(line_split[1]))
                        t.append(float(line_split[2]))
                        HF.append(float(line_split[3]))
                        m.append(float(line_split[4]))
                        Tr.append(float(line_split[5]))

                # Fermeture du fichier
                data_file.close()

                # Interpolation en Tmin et Tmax avec n_interp points puis ecriture dans x_db
                #Ts_int = np.linspace(PRM.Tmin, PRM.Tmax, PRM.n_interp)
                #x_db.t = np.interp(Ts_int,Ts,t)
                #x_db.HF = np.interp(Ts_int,Ts,HF)
                #x_db.m = np.interp(Ts_int,Ts,m)
                #x_db.Tr = np.interp(Ts_int,Ts,Tr)
                #x_db.Ts = Ts_int

                t_int = np.linspace(np.min(t), np.max(t), PRM.n_interp)
                
                Ts = np.array(Ts)
                t = np.array(t)
                ID_int = (Ts >= PRM.Tmin) & (Ts <= PRM.Tmax)
                
                t_mm = t[ ID_int ]
                
                t_int = np.linspace(np.min(t_mm), np.max(t_mm), PRM.n_interp)
                
                x_db.Ts = np.interp(t_int, t, Ts)
                
                
                
                x_db.HF = np.interp(t_int, t, HF)
                x_db.m = np.interp(t_int, t, m)
                x_db.Tr = np.interp(t_int, t, Tr)
                x_db.t = t_int
                
                ID_int = (x_db.Ts >= PRM.Tmin) & (x_db.Ts <= PRM.Tmax)
                x_db.Ts = x_db.Ts[ID_int]
                x_db.t = x_db.t[ID_int]
                x_db.HF = x_db.HF[ID_int]
                x_db.m = x_db.m[ID_int]
                x_db.Tr = x_db.Tr[ID_int]
                

            if x_db.filename_blank: # Si filename_blank trouve

                Ts=[]
                t=[]
                HF=[]
                m=[]
                Tr=[]

                # Ouverture du fichier
                data_file = open(directory + '/' + x_db.filename_blank,'r+')

                # Lecture de chaque ligne du fichier
                for line in data_file:

                    line = line.strip()             # Trim de la ligne
                    line_split = line.split()       # Split de la ligne

                    if IsInt(line_split[0]):        # Si 1ere valeur est de type Integer (Spécifique format Metler-Toledo)
                        Ts.append(float(line_split[1]))
                        t.append(float(line_split[2]))
                        HF.append(float(line_split[3]))
                        m.append(float(line_split[4]))
                        Tr.append(float(line_split[5]))

                # Fermeture du fichier
                data_file.close()

                # Interpolation en Tmin et Tmax avec n_interp points puis ecriture dans x_db
                #Ts_int = np.linspace(PRM.Tmin, PRM.Tmax, PRM.n_interp)
                #x_db.t_blank = np.interp(Ts_int,Ts,t)
                #x_db.HF_blank = np.interp(Ts_int,Ts,HF)
                #x_db.m_blank = np.interp(Ts_int,Ts,m)
                #x_db.Tr_blank = np.interp(Ts_int,Ts,Tr)
                #x_db.Ts_blank = Ts_int
                
                
                x_db.Ts_blank = np.interp(t_int, t, Ts)
                x_db.HF_blank = np.interp(t_int, t, HF)
                x_db.m_blank = np.interp(t_int, t, m)
                x_db.Tr_blank = np.interp(t_int, t, Tr)
                x_db.t_blank = t_int
                
                x_db.Ts = x_db.Ts_blank
                



#----------------------------------------------------------------------#
# Export en hdf5 des objets de paramétrage de adtc_UI et export vers objet HDF
# Prise en compte des objets checkbox, line, combobox, spinbox et doublespinbox
def export_hdf5_param( adtc_UI, HDF ):
    
    # Pour chaque membre de adtc_UI, on interroge son nom et sa valeur et on enregistre
    for name, obj in inspect.getmembers(adtc_UI):
    
        if (isinstance(obj, QCheckBox) or isinstance(obj, QRadioButton)):
            HDF.attrs[ str(obj.objectName()) ] = int(obj.isChecked())
            
        if isinstance(obj, QLineEdit):
            HDF.attrs[ str(obj.objectName()) ] = str(obj.text())
            
        if isinstance(obj, QComboBox):
            HDF.attrs[ str(obj.objectName()) ] = int(obj.currentIndex())
            
        if isinstance(obj, QSpinBox):
            HDF.attrs[ str(obj.objectName()) ] = int(obj.value())
            
        if isinstance(obj, QDoubleSpinBox):
            HDF.attrs[ str(obj.objectName()) ] = float(obj.value())
    
    # et c'est tout...
    

#----------------------------------------------------------------------#
# Export en hdf5 du tableau TABLE sous format de string vers objet HDF
# Parametres optionnels 'save_table' et 'save_checkbox' :
# Definit si les valeurs et/ou les checkbox sont exportes
def export_hdf5_table( TABLE, HDF, save_table=True, save_checkbox=False ):
    
    dtyp = 'S32' # Type string de longueur 32
    
    # Initialisation des arrays string et boolean
    A = np.zeros( (TABLE.rowCount(), TABLE.columnCount()), dtype=dtyp )
    A_check = np.zeros( (TABLE.rowCount(), TABLE.columnCount()), dtype='b' )
    
    # Pour chaque cellule de TABLE:
    for (i,j), value in np.ndenumerate(A):
        
        # Verifie valeur
        if save_table:
            A[i,j] = str(TABLE.item(i,j).text())
        
        # Verifie etat checkbox
        if save_checkbox:
            if TABLE.item(i,j).checkState() == Qt.Checked:
                A_check[i,j] = True
            else:
                A_check[i,j] = False
    
    # Sauvegarde valeurs string si demande
    if save_table:
        HDF.create_dataset( str(TABLE.objectName()), data=A, dtype=dtyp )
    
    # Sauvegarde booleen checkbox si demande
    if save_checkbox:
        HDF.create_dataset( str(TABLE.objectName())+'_bool', data=A_check )


#----------------------------------------------------------------------#
# Routine de creation de tables XY en enregistrement vers objet HDF
def export_hdf5_lines( AXES, HDF, plot_type, abscissa ):
    
    # Verifie que au moins un plot est visible
    #~ try:
        #~ LINE =  AXES[0]
    #~ except:
        #~ msg = QMessageBox()
        #~ msg.setIcon(QMessageBox.Warning)
        #~ msg.setWindowTitle(u"Export error")
        #~ msg.setText(u"No line is plotted")
        #~ msg.exec_()
    
    # Pour chaque line de AXES
    for LINE in AXES:
        # Recupere valeurs de x et y
        x_line = LINE.get_xdata()
        y_line = LINE.get_ydata()
        
        # Remets en forme [x,y]
        DATA = np.column_stack((x_line,y_line))
        
        # Nom du dataset
        label  = LINE.get_label()
        label = label + ' - ' + plot_type
        label = label.replace("/", "")
        
        # Export dataset
        dataset = HDF.create_dataset(label, data=DATA)
        
        # Attributs : labels x et y
        dataset.attrs['x_label'] = abscissa.replace("/", "")
        dataset.attrs['y_label'] = plot_type.replace("/", "")



#----------------------------------------------------------------------#
# Routine d'export des donnees vers fichier HDF5 hdf5name
def export_hdf5( adtc_UI, hdf5name):
    

    # Remplace toute extension de hdf5name par hdf5
    if len(hdf5name.split('.')) == 1:
        hdf5name = hdf5name + '.hdf'
    else:
        hdf5name = hdf5name.split('.')[0] + '.hdf'
    
    # Creation fichier hdf5 vide
    F = h5py.File(hdf5name, "w")
    
    # 2 groupes : Results et Parameters
    D = F.create_group("Results")
    P = F.create_group("Parameters")
    
    # Teste si le subplot ax1 est actif
    ax1_active = False
    try:
        LINE =  adtc_UI.ax1.get_lines()[0]
        ax1_active = True
    except:
        pass
        
    # Export des lignes de ax1
    if ax1_active:
        export_hdf5_lines( adtc_UI.ax1.get_lines(), D, str(adtc_UI.plot_type_1.currentText()), str(adtc_UI.plot_abscissa.currentText()) )

    # Teste si le subplot ax2 est actif
    ax2_active = False
    try:
        LINE =  adtc_UI.ax2.get_lines()[0]
        ax2_active = True
    except:
        pass
        
    # Export des lignes de ax2    
    if ax2_active:
        export_hdf5_lines( adtc_UI.ax2.get_lines(), D, str(adtc_UI.plot_type_2.currentText()), str(adtc_UI.plot_abscissa.currentText()) )
    
    # Export des parametres de plot generaux (checkbox, spinbox, ...)
    export_hdf5_param( adtc_UI, P )

    # Export de la table 'constituants'
    export_hdf5_table( adtc_UI.table_constituants, P, save_table=True, save_checkbox=False )
    
    # Export de la table 'reactions' avec checkbox
    export_hdf5_table( adtc_UI.table_reactions, P, save_table=True, save_checkbox=True )
    
    # Export des checkboxs de 'database_widget' uniquement
    export_hdf5_table( adtc_UI.database_widget, P, save_table=False, save_checkbox=True )
    
    # Savegarde de la version
    F.attrs['software'] = 'ADeTheC'
    F.attrs['version'] = adtc_UI.version
    
    # Fermeture du fichier
    F.close()
    
    
#----------------------------------------------------------------------#
# Routine d'import des parametres depuis fichier HDF5 hdf5name
def import_hdf5( adtc_UI, hdf5name):
    
    # Ouverture du fichier hdf5
    F = h5py.File(hdf5name, "r")
    P = F['Parameters']
    
    # Test de version fichier vs. version adethec
    if not( F.attrs['version'] == adtc_UI.version ):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setWindowTitle(u"Import error")
        msg.setText(u"Imported file was created with an older version of ADeTheC. Compatibility problems may occur.")
        msg.exec_()
    
    # Import des objets de paramétrage via donnees dans groupe 'Parameters'
    # Pour chaque membre de adtc_UI, on interroge son nom et sa valeur
    # Et on associe a la valeur de meme nom dans fichier HDF5
    for name, obj in inspect.getmembers(adtc_UI):
        
        if (isinstance(obj, QCheckBox) or isinstance(obj, QRadioButton)):
            obj.setChecked( P.attrs[ str(obj.objectName()) ] )
        
        if isinstance(obj, QLineEdit):
            obj.setText( P.attrs[ str(obj.objectName()) ] )
            
        if isinstance(obj, QComboBox):
            obj.setCurrentIndex( P.attrs[ str(obj.objectName()) ] ) 
            
        if (isinstance(obj, QSpinBox) or isinstance(obj, QDoubleSpinBox)):
            obj.setValue( P.attrs[ str(obj.objectName()) ] )

    # Pour chaque cellule cochee dans database_widget
    A = P['database_widget_bool'][:]
    for i in range(adtc_UI.database_widget.rowCount()):
            
        if A[i,0] == 1:         # Uniquement pour 1ere colonne
            adtc_UI.database_widget.item(i,0).setCheckState( Qt.Checked )    # Ecriture dans item
    
    
    # Efface toutes les lignes de 'table_constituants'
    for i in range( adtc_UI.table_constituants.rowCount() ):
        adtc_UI.table_constituants.removeRow(0)
    
    # Pour chaque cellule dans 'table_constituants' HDF5 :
    A = P['table_constituants'][:]    
    for i in range( A.shape[0] ):
        # Ajoute une ligne dans interface
        adtc_UI.table_constituants.insertRow( adtc_UI.table_constituants.rowCount() )
        
        for j in range( A.shape[1] ):
            # Ajoute un item de valeur A[i,j] venant du HDF5
            item = QTableWidgetItem()
            item.setText( A[i,j] )
            adtc_UI.table_constituants.setItem( i, j, item )
    
    # Efface toutes les lignes de 'table_reactions'
    for i in range( adtc_UI.table_reactions.rowCount() ):
        adtc_UI.table_reactions.removeRow(0)
    
    # Pour chaque cellule dans 'table_reactions' HDF5 :
    A = P['table_reactions'][:]    
    A_bool = P['table_reactions_bool'][:]
    for i in range( A.shape[0] ):
        # Ajoute une ligne dans interface
        adtc_UI.table_reactions.insertRow( adtc_UI.table_reactions.rowCount() )
        
        for j in range( A.shape[1] ):
            # Ajoute un item de valeur A[i,j] venant du HDF5
            item = QTableWidgetItem()
            item.setText( A[i,j] )
            
            # Et une checkbox dont la valeur est stockee dans 'table_reactions_bool'
            if j >= 2:
                if A_bool[i,j] == 1:
                    item.setCheckState(Qt.Checked)
                else:
                    item.setCheckState(Qt.Unchecked)

            adtc_UI.table_reactions.setItem( i, j, item )
    
    # Fermeture du fichier
    F.close()

#----------------------------------------------------------------------#
# Test de comportement
if __name__=='__main__':
    pass

