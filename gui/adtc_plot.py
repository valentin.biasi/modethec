#! /usr/bin/env python
#­* ­coding: utf­8 ­*
#----------------------------------------------------------------------#
# Programme de plot de courbes pour l'analyseur ATG
#
# Débuté le 24/10/2016
# Auteur : Valentin Biasi
#
import adtc_io
import adtc_fit
#
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
from scipy import interpolate
from scipy.integrate import odeint
#
# Gestion des UTF8 en Qt
try:
    _fromUtf8 = QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s
#
# Test de string convertible en integer
def IsInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False


#----------------------------------------------------------------------#
# Rafraichissemet du plot grace à la fonction utilisateur "Plot"
def refresh_plot(adtc_UI):
    
    # Gestion des styles de plot
    set_plot_style(adtc_UI)
    
    adtc_UI.figure.clf()    # Clear figure
    adtc_UI.statusbar.showMessage("Refresh plot : OK")   # Clear statusbar
    
    # 1 ou 2 subplot suivant la valeur de plot_type_2
    if str(adtc_UI.plot_type_2.currentText()) == 'None':
        adtc_UI.ax1 = adtc_UI.figure.add_subplot(111)
        adtc_UI.ax1.hold(False)
    else:
        adtc_UI.ax1 = adtc_UI.figure.add_subplot(121)
        adtc_UI.ax2 = adtc_UI.figure.add_subplot(122)
        adtc_UI.ax1.hold(False)
        adtc_UI.ax2.hold(False)
    
    
    # Recherche des noms de fichiers data .txt
    adtc_io.get_filename_data(adtc_UI.DB, adtc_UI.directory, adtc_UI.statusbar)
    
    # Lecture des fichiers data .txt 
    adtc_io.read_tga_data(adtc_UI.DB, adtc_UI.directory, adtc_UI.PRM, adtc_UI.statusbar)
    
    # Pour chaque indice dans DB
    for x_db in adtc_UI.DB:
        # Si indice cheched et filename trouvé
        if (x_db.checked == True) and (x_db.filename):
            
            # On ajoute l'indice demandé dans le plot
            evalXY_to_plot(adtc_UI, x_db, statusbar=adtc_UI.statusbar)
    
    # Mise a jour du plot
    #adtc_UI.figure.tight_layout()
    #plt.tight_layout()
    plt.subplots_adjust(left=0.07, bottom=0.12, right=0.96, top=0.96, wspace=0.1, hspace=0.1)
    adtc_UI.canvas.draw()  
    
    
    # !!! A MODIFIER : ligne pour forcer un aspect ratio avant export !!!
    #figure.set_size_inches(800./figure.dpi,200./figure.dpi)


#----------------------------------------------------------------------#
# Integration de HF grace a la fonction utilisateur "Integrate DSC"
def integrate_dsc(adtc_UI):
    
    # Gestion des styles de plot
    set_plot_style(adtc_UI)
    
    adtc_UI.figure.clf()    # Clear figure
    adtc_UI.statusbar.showMessage("DSC integration : OK")   # Clear statusbar
    
    # 1 subplot cree
    adtc_UI.ax1 = adtc_UI.figure.add_subplot(111)
    adtc_UI.ax1.hold(False)
    
    # Recherche des noms de fichiers data .txt
    adtc_io.get_filename_data(adtc_UI.DB, adtc_UI.directory, adtc_UI.statusbar)
    
    # Lecture des fichiers data .txt 
    adtc_io.read_tga_data(adtc_UI.DB, adtc_UI.directory, adtc_UI.PRM, adtc_UI.statusbar)
    
    # Pour chaque indice dans DB
    for x_db in adtc_UI.DB:
        # Si indice cheched et filename trouvé
        if (x_db.checked == True) and (x_db.filename):
            
            # On integre l'indice demandé puis plot
            eval_DSC_plot(adtc_UI, x_db, adtc_UI.statusbar)
    
    # Mise a jour du plot
    adtc_UI.canvas.draw()
    

#----------------------------------------------------------------------#
# Fitting des ATG selectionnees pour determiner parametres d'arrhenius
def tga_fitting(adtc_UI, method):
# method = 'fit' : Optimisation des parametres d'Arrhenius puis plot normal
# method = 'plot' : Trace simple des TGA a partir des valeurs des tableaux de l'onglet TGA_fitting
    
    # Gestion des styles de plot
    set_plot_style(adtc_UI)
    
    adtc_UI.figure.clf()    # Clear figure
    adtc_UI.statusbar.showMessage("TGA fitting : OK")   # Clear statusbar
    
    # 1 ou 2 subplot suivant la valeur de plot_type_2
    if str(adtc_UI.plot_type_2.currentText()) == 'None':
        adtc_UI.ax1 = adtc_UI.figure.add_subplot(111)
        adtc_UI.ax1.hold(False)
    else:
        adtc_UI.ax1 = adtc_UI.figure.add_subplot(121)
        adtc_UI.ax2 = adtc_UI.figure.add_subplot(122)
        adtc_UI.ax1.hold(False)
        adtc_UI.ax2.hold(False)
        
    
    # Recherche des noms de fichiers data .txt
    adtc_io.get_filename_data(adtc_UI.DB, adtc_UI.directory, adtc_UI.statusbar)
    
    # Lecture des fichiers data .txt 
    adtc_io.read_tga_data(adtc_UI.DB, adtc_UI.directory, adtc_UI.PRM, adtc_UI.statusbar)
    
    # Lecture des parametres de fitting TGA
    adtc_fit.get_fit_parameters(adtc_UI)


    #### A COMPLETER ####
    if method == 'fit':
        adtc_fit.optimize(adtc_UI)
        
    elif method == 'plot':
        adtc_fit.epsilon_function(adtc_UI)
    else:
        adtc_UI.statusbar.showMessage("Unknow method of TGA fitting")
    
    
    
    # Pour chaque indice dans DB
    for x_db in adtc_UI.DB:
        # Si indice cheched et filename trouvé
        if (x_db.checked == True) and (x_db.filename):
            
            # On ajoute l'indice demandé dans le plot
            col_line = evalXY_to_plot(adtc_UI, x_db, linestyle ='--', statusbar=adtc_UI.statusbar)
            
            # On evalue le fit pour chaque x_db
            evalXY_fitting(adtc_UI, x_db, col_line, statusbar=adtc_UI.statusbar)
            

    # Mise a jour du plot
    adtc_UI.canvas.draw()


#----------------------------------------------------------------------#
# Simulation de 1 ou plusieurs ATG a beta ou T constant en fonction des parametres d'arrhenius du tableau
def tga_simulate(adtc_UI):

    # Gestion des styles de plot
    set_plot_style(adtc_UI)
    
    adtc_UI.figure.clf()    # Clear figure
    adtc_UI.statusbar.showMessage("TGA simulation : OK")   # Clear statusbar
    
    # 1 subplot cree
    adtc_UI.ax1 = adtc_UI.figure.add_subplot(111)
    adtc_UI.ax1.hold(False)
    
    # Lecture des parametres de fitting TGA
    adtc_fit.get_fit_parameters(adtc_UI)

    # Valeurs initiales de Y par espece
    Y0=[]
    for spec in adtc_UI.species:
        Y0.append(spec.Y0)
    
    # atmosphere de simulation
    atmo = str(adtc_UI.combo_atmo_tga_simu.currentText())
    
    
    ### Cas 1 : Simulation d'ATG isotherme
    if adtc_UI.isothermal_radio.isChecked():
        
        # Vecteur tps
        tps = np.linspace(0.0, float(adtc_UI.line_tga_duration.text())*3600.0, 1000)
        
        # Valeurs d'isothermes
        tab_isoT = np.linspace( adtc_UI.isothermal_min_spin.value(), adtc_UI.isothermal_max_spin.value(), adtc_UI.isothermal_nb_spin.value() )
        
        # Pour chaque isotherme
        for isoT in tab_isoT:
            
            # Resout les Yi par espece
            Y_solve = odeint( adtc_fit.ode_function_isoT, Y0, tps, args=(adtc_UI.species, adtc_UI.reactions, isoT, atmo, Y0) )
            
            Y_solve = np.nan_to_num( Y_solve )  # Supprime les NaN
            Y_fit = np.sum( Y_solve, 1 )        # Somme sur les Yi pour avoir Y total
            
            leg_label = 'T = %dK' % (isoT)      # Legende
            
            # Plot de Y_fit
            adtc_UI.ax1.hold(True)
            adtc_UI.ax1.set_xlabel('t [h]')
            adtc_UI.ax1.set_ylabel('m/m0 [-]')
            adtc_UI.ax1.plot(tps/3600.0, Y_fit, label=leg_label)
            
            # Affiche les concentrations d'especes en plot
            if adtc_UI.checkBox_simulate_constituents.isChecked():
                for i_spec in range(len(adtc_UI.species)):
                    adtc_UI.ax1.plot( tps/3600.0, Y_solve[:,i_spec], label=adtc_UI.species[i_spec].name )
                    
            adtc_UI.ax1.legend(loc=adtc_UI.legend_location) # Affichage legende
            
    
    ### Cas 2 : Simulation d'ATG a beta constant
    elif adtc_UI.constant_beta_radio.isChecked():
        
        
        
        # Valeurs de beta
        tab_beta = np.linspace( adtc_UI.constant_beta_min_spin.value(), adtc_UI.constant_beta_max_spin.value(), adtc_UI.constant_beta_nb_spin.value() )
        
        # Pour chaque beta
        for beta in tab_beta:
            
            # Vecteur temperature et temps
            T = np.linspace(300.0, float(adtc_UI.line_tga_Tmax.text()), 1000)
        
            # Resout les Yi par espece
            Y_solve = odeint( adtc_fit.ode_function, Y0, T, args=(adtc_UI.species, adtc_UI.reactions, beta, atmo, Y0) )
            
            Y_solve = np.nan_to_num( Y_solve )  # Supprime les NaN
            Y_fit = np.sum( Y_solve, 1 )        # Somme sur les Yi pour avoir Y total
        
            leg_label = '%dK/min' % (beta)      # Legende
    
            # Plot de Y_fit
            adtc_UI.ax1.hold(True)
            adtc_UI.ax1.set_xlabel('T [K]')
            adtc_UI.ax1.set_ylabel('m/m0 [-]')
            adtc_UI.ax1.plot(T, Y_fit, label=leg_label)
            
            # Affiche les concentrations d'especes en plot
            if adtc_UI.checkBox_simulate_constituents.isChecked():
                for i_spec in range(len(adtc_UI.species)):
                    adtc_UI.ax1.plot( T, Y_solve[:,i_spec], label=adtc_UI.species[i_spec].name )
            
            adtc_UI.ax1.legend(loc=adtc_UI.legend_location) # Affichage legende

    # Mise a jour du plot
    adtc_UI.canvas.draw()
    
    
#----------------------------------------------------------------------#
# Gestion des styles de plot
def set_plot_style(adtc_UI):
    
    # Dans menu "display", choix de style de plot :
    if adtc_UI.action538.isChecked():           # Style 538
        plt.style.use('fivethirtyeight')
    elif adtc_UI.actionClassic.isChecked():     # Style classic gnuplot
        plt.style.use('ggplot')
    elif adtc_UI.actionBlack_White.isChecked(): # Style black and white
        plt.style.use('grayscale')    
        plt.rcParams['grid.color'] = '#9c9c9c'
        
        
    # Modifications generales appliquees a tous les styles
    plt.rcParams['savefig.transparent'] = True
    plt.rcParams['lines.linewidth'] = 2.0
        
    plt.rcParams['xtick.major.size'] = 6.0
    plt.rcParams['xtick.major.width'] = 2.0
    plt.rcParams['ytick.major.size'] = 6.0
    plt.rcParams['ytick.major.width'] = 2.0
    
    plt.rcParams['xtick.direction'] = 'in'
    plt.rcParams['ytick.direction'] = 'in'
    
    plt.rcParams['axes.edgecolor'] = 'k'
    plt.rcParams['axes.linewidth'] = 2.0
    
    plt.rcParams['font.size'] = 10.0            
    plt.rcParams['xtick.labelsize'] = 14.0
    plt.rcParams['ytick.labelsize'] = 14.0
    plt.rcParams['axes.labelsize'] = 16.0
    
    # Dans menu "legend location", choix de la position des legendes
    if adtc_UI.action_Best.isChecked():
        adtc_UI.legend_location = 0
    elif adtc_UI.actionTop_Right.isChecked():
        adtc_UI.legend_location = 1
    elif adtc_UI.actionTop_Left.isChecked():
        adtc_UI.legend_location = 2
    elif adtc_UI.actionBottom_Right.isChecked():
        adtc_UI.legend_location = 4
    elif adtc_UI.actionBottom_Left.isChecked():
        adtc_UI.legend_location = 3


#----------------------------------------------------------------------#
# Evaluation de l'ordonnee Y en focntion de l'option "plot_type"
# Fonction output = Y : Valeur a plotter
def get_Y_value(plot_type, x_db, m, HF, PRM, mm0_scaled=False, mm0_scaling_min=0.0, statusbar=None):
    
    if mm0_scaled:
        mm0 = (m - m[-1]) / (m[0] - m[-1] + 0.0001) * (1.0 - mm0_scaling_min) + mm0_scaling_min
    else:
        mm0 = m / (m[0] +0.0001)

    if plot_type == 'm/m0 [-]':     # Cas m/m0
        Y = mm0
        
    elif plot_type == 'm [mg]':     # Cas m simple
        Y = m
        
    elif plot_type == 'm-m0 [mg]':  # Cas m - m0
        Y = m - m[0]
        
    elif plot_type == '-d/dT (m/m0) [1/K]':  # Derivee temperature de m/m0
        Y = - mm0
        Y = np.gradient(Y, (PRM.Tmax - PRM.Tmin)/PRM.n_interp )
        
    elif plot_type == 'MLR [1/s]':  # Derivee temporelle de m/m0
        Y = - mm0
        Y = np.gradient(Y, (PRM.Tmax - PRM.Tmin)/PRM.n_interp )
        Y = Y * x_db.heat_velocity / 60.0
        
    elif plot_type == 'HF [mW]':    # Flux DSC
        Y = HF
        
    else:                           # Sinon probleme
        statusbar.showMessage('Oops : Problem with plot option')
    
    return Y                        # renvoi de Y
    
    
#----------------------------------------------------------------------#
# Smoothing de l'ordonnee Y
# Fonction output = Y : Valeur a plotter
def smooth_value(Y, bool_smoothing, smoothing_value):
    
    if bool_smoothing:  # Si smoothing checked
        
        # Filtrage frequentiel
        b, a = signal.butter(3, 1./smoothing_value)
        Y = signal.filtfilt(b, a, Y)
    
    return Y                       # renvoi de Y


#----------------------------------------------------------------------#
# Smoothing de l'ordonnee Y
# Fonction output = Y : Valeur a plotter
def change_predefined_colors(Y_line, PRM, x_db):
    
    # Si couleurs imposee par l'utilisateur
    if PRM.predefined_colors:
        
        if x_db.heat_velocity == 2.0:           # beta = 2 K/min
            Y_line.set_color( PRM.predefined_2K )
        
        if x_db.heat_velocity == 5.0:           # beta = 5 K/min
            Y_line.set_color( PRM.predefined_5K )
            
        if x_db.heat_velocity == 10.0:           # beta = 10 K/min
            Y_line.set_color( PRM.predefined_10K )
            
        if x_db.heat_velocity == 20.0:           # beta = 20 K/min
            Y_line.set_color( PRM.predefined_20K )
            
        if x_db.heat_velocity == 50.0:           # beta = 50 K/min
            Y_line.set_color( PRM.predefined_50K )
            
        if x_db.heat_velocity == 100.0:           # beta = 100 K/min
            Y_line.set_color( PRM.predefined_100K )


#----------------------------------------------------------------------#
# Generation de plot pour une experience donnee
def evalXY_to_plot(adtc_UI, x_db, linestyle='-', statusbar=None):
    
    # Renvoi des options de l'interface graphique
    plot_type_1 = str(adtc_UI.plot_type_1.currentText())
    plot_type_2 = str(adtc_UI.plot_type_2.currentText())
    plot_abscissa = str(adtc_UI.plot_abscissa.currentText())
    #
    substract_blank = adtc_UI.checkbox_substract_blank.isChecked()
    plot_blank = adtc_UI.checkbox_plot_blank.isChecked()
    #
    bool_smoothing = adtc_UI.checkbox_smoothing.isChecked()
    smoothing_value = adtc_UI.spinbox_smoothing.value()
    mm0_scaled = adtc_UI.checkBox_mm0_scaled.isChecked()
    mm0_scaling_min = float(adtc_UI.line_scaling_min.text())
    
    # Renvoi de l'abscisse X en fonction de l'option "plot_abscissa"
    X = x_db.Ts
    if plot_abscissa == 'T [K]':    # Cas T Kelvin
        X = x_db.Ts + 273.15
    elif plot_abscissa == 'T [degC]':   # Cas T degC
        X = x_db.Ts
    elif plot_abscissa == 't [min]':    # Cas temps min
        X = x_db.t / 60.0
    elif plot_abscissa == 't [s]':      # Cas teps secondes
        X = x_db.t
    else:                               # Sinon probleme
        statusbar.showMessage('Oops : Problem with abscissa option')
    
    # Prise en compte de la ligne de base
    if substract_blank and x_db.blank_id:
        m = x_db.m - x_db.m_blank
        HF = x_db.HF - x_db.HF_blank
    else:
        m = x_db.m
        HF = x_db.HF
    
    #--- PLOT N°1 -----------------------------------------------------#
    # Labels X et Y
    adtc_UI.ax1.hold(True)
    adtc_UI.ax1.set_xlabel(plot_abscissa)
    adtc_UI.ax1.set_ylabel(plot_type_1)
    
    # Legende
    leg_label = 'Exp ' + str(x_db.ID)+' : '+str(x_db.method)+' - '+str(int(x_db.heat_velocity))+'K/min - '+str(int(x_db.gas_rate))+'mL/min'
    
    # Evaluation de Y puis plot X,Y
    Y = get_Y_value(plot_type_1, x_db, m, HF, adtc_UI.PRM, mm0_scaled, mm0_scaling_min, statusbar)
    Y = smooth_value(Y, bool_smoothing, smoothing_value)
    Y_line, = adtc_UI.ax1.plot(X, Y, linestyle, label=leg_label)
    
    # Modification de la couleur si demandee
    change_predefined_colors(Y_line, adtc_UI.PRM, x_db)

    col_line = Y_line.get_color()
    
    # Idem pour blank si demande
    if plot_blank and x_db.blank_id:
        Y = get_Y_value(plot_type_1, x_db, x_db.m_blank, x_db.HF_blank, adtc_UI.PRM, statusbar)
        Y = smooth_value(Y, bool_smoothing, smoothing_value)
        adtc_UI.ax1.plot(X, Y, '--', color=col_line, label=leg_label+' - Blank')
    
    adtc_UI.ax1.legend(loc=adtc_UI.legend_location)                     # MAJ legende
    
    #--- PLOT N°2 -----------------------------------------------------#
    if 'None' not in plot_type_2:
        
        # Labels X et Y
        adtc_UI.ax2.hold(True)
        adtc_UI.ax2.set_xlabel(plot_abscissa)
        adtc_UI.ax2.set_ylabel(plot_type_2)
        
        # Legende
        leg_label = 'Exp ' + str(x_db.ID)+' : '+str(x_db.method)+' - '+str(int(x_db.heat_velocity))+'K/min - '+str(int(x_db.gas_rate))+'mL/min'
        
        # Evaluation de Y puis plot X,Y
        Y = get_Y_value(plot_type_2, x_db, m, HF, adtc_UI.PRM, mm0_scaled, mm0_scaling_min, statusbar)
        Y = smooth_value(Y, bool_smoothing, smoothing_value)
        adtc_UI.ax2.plot(X, Y, linestyle, color=col_line, label=leg_label)
        
        # Idem pour blank si demande
        if plot_blank and x_db.blank_id:
            Y = get_Y_value(plot_type_2, x_db, x_db.m_blank, x_db.HF_blank, adtc_UI.PRM, statusbar)
            Y = smooth_value(Y, bool_smoothing, smoothing_value)
            adtc_UI.ax2.plot(X, Y, '--', color=col_line, label=leg_label+' - Blank')
        
        adtc_UI.ax2.legend(loc=adtc_UI.legend_location)                 # MAJ legende
    
    return col_line

#----------------------------------------------------------------------#
# Integration de HF puis plot de HF pour une experience donnee
def eval_DSC_plot(adtc_UI, x_db, statusbar=None):
    
    plot_type_1 = 'HF [mW]'
    
    # Renvoi des options de l'interface graphique
    plot_abscissa = str(adtc_UI.plot_abscissa.currentText())
    #
    substract_blank = adtc_UI.checkbox_substract_blank.isChecked()
    plot_blank = adtc_UI.checkbox_plot_blank.isChecked()
    #
    bool_smoothing = adtc_UI.checkbox_smoothing.isChecked()
    smoothing_value = adtc_UI.spinbox_smoothing.value()
    #
    T_spline_min = adtc_UI.spinbox_spline_min.value()
    T_spline_max = adtc_UI.spinbox_spline_max.value()
    T_int_min = adtc_UI.spinbox_int_min.value()
    T_int_max = adtc_UI.spinbox_int_max.value()
    
    # Renvoi de l'abscisse X en fonction de l'option "plot_abscissa"
    X = x_db.Ts
    if plot_abscissa == 'T [K]':    # Cas T Kelvin
        X = x_db.Ts + 273.15
    elif plot_abscissa == 'T [degC]':   # Cas T degC
        X = x_db.Ts
    else:                               # Sinon probleme
        statusbar.showMessage('Oops : Problem with abscissa option => You must choose a temperature')
    
    # Prise en compte de la ligne de base
    if substract_blank and x_db.blank_id:
        m = x_db.m - x_db.m_blank
        HF = x_db.HF - x_db.HF_blank
    else:
        m = x_db.m
        HF = x_db.HF
    
    
    
    # Labels X et Y
    adtc_UI.ax1.hold(True)
    adtc_UI.ax1.set_xlabel(plot_abscissa)
    adtc_UI.ax1.set_ylabel(plot_type_1)
    
    # Evaluation de Y puis plot X,Y
    Y = get_Y_value(plot_type_1, x_db, m, HF, adtc_UI.PRM, statusbar)
    Y = smooth_value(Y, bool_smoothing, smoothing_value)

    T_spline = np.concatenate( (X[ X <= T_spline_min ], X[ X >= T_spline_max ]) )
    HF_spline = np.concatenate( (Y[ X <= T_spline_min ], Y[ X >= T_spline_max ]) )
    
    S =  interpolate.UnivariateSpline( T_spline , HF_spline , s=np.log10(adtc_UI.spinbox_dsc_smoothing.value()) )
    HF_new = S(X)
        
    ID_int = (X >= T_int_min) & (X <= T_int_max)
    T_int = X[ID_int]
    HF_int = Y[ID_int]
    HF_int_base = HF_new[ID_int]
    tps_int = x_db.t[ID_int]
    
    int_Q = np.trapz(HF_int, x=tps_int)
    int_Q_base = np.trapz(HF_int_base, x=tps_int)
    Qreac = (int_Q - int_Q_base) / x_db.m[0]
    
    # Legende
    leg_label = 'Exp %d : %s - %dK/min - %dmL/min - Qreac = %d J/g' % (x_db.ID, x_db.method, int(x_db.heat_velocity), int(x_db.gas_rate), Qreac)
    
    Y_line, = adtc_UI.ax1.plot(X, Y, label=leg_label)
    
    # Modification de la couleur si demandee
    change_predefined_colors(Y_line, adtc_UI.PRM, x_db)
    col_line = Y_line.get_color()
    
    adtc_UI.ax1.plot(X, HF_new,'--',color=col_line)
    adtc_UI.ax1.plot(T_spline, HF_spline, 'x',color=col_line)
    adtc_UI.ax1.plot(T_int_min, S(T_int_min), 'og', T_int_max, S(T_int_max), 'og')
    adtc_UI.ax1.fill_between(T_int, HF_int, HF_int_base, where=HF_int < HF_int_base, facecolor=col_line, alpha=0.3, interpolate=True)
    adtc_UI.ax1.fill_between(T_int, HF_int, HF_int_base, where=HF_int > HF_int_base, facecolor=col_line, alpha=0.3, interpolate=True)

    
    adtc_UI.ax1.legend(loc=adtc_UI.legend_location)                     # MAJ legende
    


#----------------------------------------------------------------------#
# Tracage de plot pour une reconstruction TGA donnee
def evalXY_fitting(adtc_UI, x_db, col_line, linestyle='-', statusbar=None):
    
    # Renvoi des options de l'interface graphique
    plot_type_1 = str(adtc_UI.plot_type_1.currentText())
    plot_type_2 = str(adtc_UI.plot_type_2.currentText())
    plot_abscissa = str(adtc_UI.plot_abscissa.currentText())
    mm0_scaled = adtc_UI.checkBox_mm0_scaled.isChecked()
    mm0_scaling_min = float(adtc_UI.line_scaling_min.text())
    
    # Renvoi de l'abscisse X en fonction de l'option "plot_abscissa"
    X = x_db.Ts
    if plot_abscissa == 'T [K]':    # Cas T Kelvin
        X = x_db.Ts + 273.15
    elif plot_abscissa == 'T [degC]':   # Cas T degC
        X = x_db.Ts
    elif plot_abscissa == 't [min]':    # Cas temps min
        X = x_db.t / 60.0
    elif plot_abscissa == 't [s]':      # Cas teps secondes
        X = x_db.t
    else:                               # Sinon probleme
        statusbar.showMessage('Oops : Problem with abscissa option')
    
    m = x_db.m[0] * x_db.Y_fit
    HF = x_db.HF # Faux : a modifier
        
    #--- PLOT N°1 -----------------------------------------------------#
    # Legende
    leg_label = 'Num ' + str(x_db.ID)+' : ' + str(x_db.chi2)
    leg_label = 'Num %d : Fitting error = %.2f%%' % ( x_db.ID, x_db.chi2 * 100.0 )
    
    # Evaluation de Y puis plot X,Y
    Y = get_Y_value(plot_type_1, x_db, m, HF, adtc_UI.PRM, mm0_scaled, mm0_scaling_min, statusbar)
    adtc_UI.ax1.plot(X, Y, linestyle, color=col_line, label=leg_label)
    
    adtc_UI.ax1.legend(loc=adtc_UI.legend_location)                     # MAJ legende
    
    #--- PLOT N°2 -----------------------------------------------------#
    if 'None' not in plot_type_2:
        
        # Evaluation de Y puis plot X,Y
        Y = get_Y_value(plot_type_2, x_db, m, HF, adtc_UI.PRM, mm0_scaled, mm0_scaling_min, statusbar)
        adtc_UI.ax2.plot(X, Y, linestyle, color=col_line, label=leg_label)
    
        adtc_UI.ax2.legend(loc=adtc_UI.legend_location)                     # MAJ legende
        
        
#----------------------------------------------------------------------#
# Test de comportement
if __name__=='__main__':
    pass
