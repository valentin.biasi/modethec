#­* ­coding: utf­8 ­*
#----------------------------------------------------------------------#
# Script de generation des widgets de l'interface graphique de MODETHEC
#
# Débuté le 06/02/2015
# Auteur : Valentin Biasi
#
#~ from PyQt4.QtGui import *
#~ from PyQt4.QtCore import *
from pyface.qt.QtGui import *
from pyface.qt.QtCore import *

import mdtc_str

# Variables globales pour tous les widgets
lab_size_min = 120
h_ligne = 30


# Widget modifié pour ignorer le scroll de la souris : QSpinBox
class customQSpinBox(QSpinBox):
    def __init__(self):
        QSpinBox.__init__(self)
    def wheelEvent(self, event):
        if event.type() == QEvent.Wheel:
            event.ignore()

# Widget modifié pour ignorer le scroll de la souris : QComboBox
class customQComboBox(QComboBox):
    def __init__(self):
        QComboBox.__init__(self)
    def wheelEvent(self, event):
        if event.type() == QEvent.Wheel:
            event.ignore()

#----------------------------------------------------------------------#
#--- WIDGETS BLOBAUX --------------------------------------------------#

# Barre d'outil de choix des solveurso
def check_button_ico(FRAME,PRM,texte,icone,check,out_value,pos):
    
    def set_value(BUTTON,PRM,out_value):
        
        mdtc_str.need_to_save = True
        
        if BUTTON.isChecked():
            PRM.v = out_value[0]
        else:
            PRM.v = out_value[1]

    def get_value(BUTTON,PRM,out_value):
        if PRM.v == out_value[0]:
            BUTTON.setChecked(True)
        else:
            BUTTON.setChecked(False)
            
    BUTTON = QToolButton()
    BUTTON.setToolTip(unicode(PRM.d))
    BUTTON.setText(unicode(texte))
    BUTTON.setCheckable(True)
    BUTTON.setChecked( check )
    FRAME.addWidget(BUTTON, pos[0], pos[1], pos[2], pos[3])
    PRM.w = BUTTON

    get_value(BUTTON,PRM,out_value)
    
    ICO = QIcon()
    sizePolicy = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)

    ICO.addPixmap(QPixmap(icone))
    BUTTON.setIcon(ICO)
    BUTTON.setIconSize(QSize(100, 100))
    BUTTON.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
    BUTTON.setSizePolicy(sizePolicy)

    BUTTON.released.connect(lambda: set_value(BUTTON,PRM,out_value))


# Paramètres sous forme de ligne standard
def prm_edit(FRAME,PRM,descriptif,unite,iligne):
        label_left(FRAME,descriptif,(iligne[0],0,1,1))
        param_ligne(FRAME,PRM,(iligne[0],1,1,1))
        label_right(FRAME,unite,(iligne[0],2,1,1))
        horiz_spacer(FRAME,(iligne[0],3,1,1))
        iligne[0] = iligne[0] + 1
        
# Paramètres sous forme de ligne standard multiples
def prm_edit_multi(FRAME,PRM,descriptif,unite,iligne):
    if (PRM.n>=1):
         for ip in range(PRM.n):
            label_left(FRAME,descriptif[ip],(iligne[0]+ip,0,1,1))
            param_ligne_multi(FRAME,PRM,ip,(iligne[0]+ip,1,1,1))
            label_right(FRAME,unite[ip],(iligne[0]+ip,2,1,1))
            horiz_spacer(FRAME,(iligne[0]+ip,3,1,1))
    iligne[0] += PRM.n
    
# Paramètres sous forme de choix binaire
def prm_check(FRAME,PRM,descriptif,unite,option,iligne):
    param_checkbox(FRAME,PRM,descriptif,option,(iligne[0],0,1,2))
    label_right(FRAME,unite,(iligne[0],2,1,1))
    horiz_spacer(FRAME,(iligne[0],3,1,1))
    iligne[0] += 1
    
# Paramètres sous forme de ligne d'entier
def prm_edit_int(FRAME,PRM,descriptif,unite,iligne):
    label_left(FRAME,descriptif,(iligne[0],0,1,1))
    param_spinbox(FRAME,PRM,(iligne[0],1,1,1))
    label_right(FRAME,unite,(iligne[0],2,1,1))
    horiz_spacer(FRAME,(iligne[0],3,1,1))
    iligne[0] += 1
    
# Paramètres sous forme de tableau
def prm_table(FRAME,PRM,size,descriptif,unite,iligne):
    label_left(FRAME,descriptif,(iligne[0],0,1,1))
    param_table(FRAME,PRM,size,(iligne[0],1,1,1))
    label_right(FRAME,unite,(iligne[0],2,1,1))
    horiz_spacer(FRAME,(iligne[0],3,1,1))
    iligne[0] += 1

# Paramètre sous forme de choix en liste
def prm_list(FRAME,PRM,descriptif,option_aff,option,iligne):
    label_left(FRAME,descriptif,(iligne[0],0,1,1))
    param_combobox(FRAME,PRM,option_aff,option,(iligne[0],1,1,1))
    horiz_spacer(FRAME,(iligne[0],2,1,2))
    iligne[0] += 1

# Paramètres sous forme de tableau multi-colonnes
def prm_table_multi(FRAME,PRM,size,descriptif,unite,header,list_var,iligne):
    label_left(FRAME,descriptif,(iligne[0],0,1,1))
    param_table_multi(FRAME,PRM,size,header, list_var, (iligne[0],1,1,2))
    horiz_spacer(FRAME,(iligne[0],3,1,1))
    iligne[0] += 1

#----------------------------------------------------------------------#
#--- WIDGETS ELEMENTAIRES ---------------------------------------------#

# Tableau simple 1 colonne
def param_table(FRAME,PRM,size,pos):
    TABLE = QTableWidget(size[0],size[1])
    TABLE.setToolTip(unicode(PRM.d))
    FRAME.addWidget(TABLE, pos[0], pos[1], pos[2], pos[3])
    TABLE.horizontalHeader().setStretchLastSection(True)
    TABLE.horizontalHeader().hide()
    TABLE.setFixedHeight(TABLE.verticalHeader().length()+2)
    PRM.w = TABLE
    
    for i in range(size[0]):
        item = QTableWidgetItem()
        TABLE.setItem(i,0,item)
        TABLE.item(i,0).setText( str(PRM.v[i]) )
        
    TABLE.cellChanged.connect(lambda: set_value(TABLE,PRM))
    
    def set_value(TABLE,PRM):
        for j in range( TABLE.rowCount() ):
            PRM.v[j] = str(TABLE.item(j,0).text())
        mdtc_str.need_to_save = True

# Tableau multi-colonne (utilisation uniquement pour variables export, modifié pour être compatible avec les sensors)
def param_table_multi(FRAME,PRM,size,header,liste_var,pos):
    TABLE = QTableWidget(size[0],size[1])
    TABLE.setToolTip(unicode(PRM.d))
    FRAME.addWidget(TABLE, pos[0], pos[1], pos[2], pos[3])
    for ih in range(len(header)):
        TABLE.setHorizontalHeaderItem(ih, QTableWidgetItem())
        TABLE.horizontalHeaderItem(ih).setText(header[ih])
    TABLE.resizeColumnsToContents()
    TABLE.horizontalHeader().setStretchLastSection(True)
    TABLE.setFixedHeight(TABLE.verticalHeader().length()+25)
        
    PRM.w = TABLE
    
    for i in range(size[0]):
        for j in range(size[1]):
            item = QTableWidgetItem()
            TABLE.setItem(i,j,item)
            list_i = PRM.v[i]
            TABLE.item(i,j).setText( str(list_i[j]) )
                
            if j==0 and liste_var != '':
                item = customQComboBox()
                item.addItems(liste_var)
                item.setEditable(True)
                TABLE.setCellWidget(i,j,item)
                TABLE.cellWidget(i,j).setEditText( str(list_i[j]) )
                item.editTextChanged.connect(lambda: set_value(TABLE,PRM,liste_var))
                
    TABLE.cellChanged.connect(lambda: set_value(TABLE,PRM,liste_var))
    
    def set_value(TABLE,PRM,liste_var):
        for i in range(size[0]):
            for j in range(size[1]):
                if j==0 and liste_var != '':
                    data = str(TABLE.cellWidget(i,j).currentText())
                else:
                    data = str(TABLE.item(i,j).text())
                
                (PRM.v[i])[j] = data

        mdtc_str.need_to_save = True
         
# Ligne de texte 'String' ou 'Float'
def param_ligne(FRAME,PRM,pos):
    LIGNE = QLineEdit()
    FRAME.addWidget(LIGNE, pos[0], pos[1], pos[2], pos[3])
    LIGNE.setText(str(PRM.v))
    LIGNE.setToolTip(unicode(PRM.d))
    LIGNE.textChanged.connect(lambda: set_value(LIGNE,PRM))
    PRM.w = LIGNE

    def set_value(LIGNE,PRM):
        PRM.v = LIGNE.text()
        mdtc_str.need_to_save = True

# Multi-lignes de texte en vertical 'String' ou 'Float'
def param_ligne_multi(FRAME,PRM,ip,pos):
    LIGNE = QLineEdit()
    LIGNE.setToolTip(unicode(PRM.d))
    FRAME.addWidget(LIGNE, pos[0], pos[1], pos[2], pos[3])
    LIGNE.setText(str(PRM.v[ip]))
    LIGNE.textChanged.connect(lambda: set_value(LIGNE,PRM,ip))
    PRM.w = LIGNE

    def set_value(LIGNE,PRM,ip):
        PRM.v[ip] = str(LIGNE.text())
        mdtc_str.need_to_save = True

# Checkbox simple
def param_checkbox(FRAME,PRM,texte,option,pos):
    def get_value(CHECK,PRM,option):
        if PRM.v == option[0]:
            CHECK.setChecked(True)
        else:
            CHECK.setChecked(False)
            
    CHECK = QCheckBox(unicode(texte))
    CHECK.setToolTip(unicode(PRM.d))
    FRAME.addWidget(CHECK, pos[0], pos[1], pos[2], pos[3])
    get_value(CHECK,PRM,option)
    CHECK.toggled.connect(lambda: set_value(CHECK,PRM,option))
    PRM.w = CHECK
    
    def set_value(CHECK,PRM,option):
        if CHECK.isChecked():
            PRM.v = option[0]
        else:
            PRM.v = option[1]
        mdtc_str.need_to_save = True

# Ligne spinbox pour entiers
def param_spinbox(FRAME,PRM,pos):
    LIGNE = customQSpinBox()
    LIGNE.setToolTip(unicode(PRM.d))
    FRAME.addWidget(LIGNE, pos[0], pos[1], pos[2], pos[3])
    LIGNE.setValue(int(PRM.v))
    LIGNE.valueChanged.connect(lambda: set_value(LIGNE,PRM))
    PRM.w = LIGNE

    def set_value(LIGNE,PRM):
        PRM.v = int(LIGNE.value())
        mdtc_str.need_to_save = True

# Liste de choix multiples : combobox
def param_combobox(FRAME,PRM,option_aff,option,pos):
    COMBO = customQComboBox()
    COMBO.setToolTip(unicode(PRM.d))
    COMBO.addItems(option_aff)
    FRAME.addWidget(COMBO, pos[0], pos[1], pos[2], pos[3])
    for i in range(len(option)):
        if option[i]==PRM.v:
            COMBO.setCurrentIndex(i)            

    COMBO.currentIndexChanged.connect(lambda: set_value(COMBO,PRM,option))
    PRM.w = COMBO

    def set_value(COMBO,PRM,option):
        PRM.v = option[COMBO.currentIndex()]
        mdtc_str.need_to_save = True

# Label simple à gauche
def label_left(FRAME,texte,pos):
    Label_L = QLabel(unicode(texte))
    Label_L.setMinimumSize(QSize(lab_size_min, 0))
    FRAME.addWidget(Label_L, pos[0], pos[1], pos[2], pos[3])

# Label simple à droite
def label_right(FRAME,texte,pos):
    Label_R = QLabel(unicode(texte))
    FRAME.addWidget(Label_R, pos[0], pos[1], pos[2], pos[3])

# Spacer horizontal
def horiz_spacer(FRAME,pos):
    spacer_H = QSpacerItem(10, h_ligne, QSizePolicy.Expanding, QSizePolicy.Minimum)
    FRAME.addItem(spacer_H, pos[0], pos[1], pos[2], pos[3])

# Spacer vertical
def vert_spacer(FRAME,iligne):
    pos = [ iligne[0],0,1,1 ]
    spacer_V = QSpacerItem(10, h_ligne, QSizePolicy.Minimum, QSizePolicy.Expanding)
    FRAME.addItem(spacer_V, pos[0], pos[1], pos[2], pos[3])
    iligne[0] += 1

# Ligne de séparation horizontale
def horiz_line(FRAME,iligne):
    pos = [ iligne[0],0,1,4 ]
    line = QFrame()
    line.setFrameShape(QFrame.HLine)
    line.setFrameShadow(QFrame.Sunken)
    FRAME.addWidget(line, pos[0], pos[1], pos[2], pos[3])
    iligne[0] += 1

